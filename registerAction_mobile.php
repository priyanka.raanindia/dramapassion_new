<?php session_start();
if(isset($_POST['register'])){
	//require_once($_SESSION['langfile']);
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("langue/fr.php");
	
	$_SESSION['erreur_inscription'] = "" ;
	
	$valid = true;
	$_SESSION['error'] = array();
	if(!isset($_POST['regusername']) || strlen(cleanup($_POST['regusername'])) == 0){
		$valid = false;
		$_SESSION['erreur_inscription'] = _errEnterUsername_;
		header("Location: register_mobile.php");
		exit();
	}else{
		$resuser = mysql_query("SELECT * FROM t_dp_user WHERE UserName = '".cleanup($_POST['regusername'])."'");
		if(mysql_num_rows($resuser) > 0){
			$valid = false;
			$_SESSION['erreur_inscription'] = _errUsernameTaken_;
			header("Location: register_mobile.php");
			exit();
		}else{
			$_SESSION['regusername'] = cleanup($_POST['regusername']);
		}
	}
	
	if(!isset($_POST['regpassword']) || strlen($_POST['regpassword']) == 0){
		$valid = false;
		$_SESSION['erreur_inscription'] = _errEnterPassword_;
		header("Location: register_mobile.php");
		exit();
	}
	if(!isset($_POST['regpasswordc']) || strlen($_POST['regpasswordc']) == 0){
		$valid = false;
		$_SESSION['erreur_inscription'] = _errEnterPasswordc_;
		header("Location: register_mobile.php");
		exit();
	}
	if($_POST['regpassword'] != $_POST['regpasswordc']){
		$valid = false;
		$_SESSION['erreur_inscription'] = _errPasswordNoMatch_;
		header("Location: register_mobile.php");
		exit();
	}else{
			$_SESSION['regpassword'] = cleanup($_POST['regpassword']);
			$_SESSION['regpasswordc'] = cleanup($_POST['regpasswordc']);
	}
	
	if(!isset($_POST['regemail']) || strlen($_POST['regemail']) == 0 || !valid_email($_POST['regemail'])){
		$valid = false;
		$_SESSION['erreur_inscription'] = _errValidEmail_;
		header("Location: register_mobile.php");
		exit();
	}
	if(!isset($_POST['mailmail2']) || strlen($_POST['mailmail2']) == 0 || !valid_email($_POST['mailmail2'])){
		$valid = false;
		$_SESSION['erreur_inscription'] = _errValidEmail2_;
		header("Location: register_mobile.php");
		exit();
	}
	if($_POST['regemail'] != $_POST['mailmail2']){
		$valid = false;
		$_SESSION['erreur_inscription'] = _errEmailNoMatch_;
		header("Location: register_mobile.php");
		exit();
	}else{
			if(stripos($_POST['regemail'],"jetable")){
			$valid = false;
			$_SESSION['erreur_inscription'] =  _errValidEmail_;
			header("Location: register_mobile.php");
			exit();
		}else{
			$resemail = mysql_query("SELECT * FROM t_dp_user WHERE UserEmail = '".cleanup($_POST['regemail'])."'");
			if(mysql_num_rows($resemail) > 0){
				$valid = false;
				$_SESSION['erreur_inscription'] = _errEmailTaken_;
				header("Location: register_mobile.php");
				exit();
			}else{
				$_SESSION['regemail'] = cleanup($_POST['regemail']);
				$_SESSION['mailmail2'] = cleanup($_POST['mailmail2']);
			}
		}
	}
	
	
	if(isset($_POST['regconfirm']) && $_POST['regconfirm'] != 1){
		if(!isset($_POST['regseccode']) || $_POST['regseccode'] != $_SESSION['sec_code']){
			$valid = false;
			$_SESSION['erreur_inscription'] = _errValidSecCode_;
			header("Location: register_mobile.php");
			exit();
		}
		if(!isset($_POST['regAgree']) || !$_POST['regAgree']){
			$valid = false;
			$_SESSION['erreur_inscription'] = _errAgree_;
			header("Location: register_mobile.php");
			exit();
		}
	}
	
	$_SESSION['regcountry'] = cleanup($_POST['regcountry']);
	/*
	$referral = "";
	if(isset($_POST['regrefusername']) && strlen($_POST['regrefusername']) > 0){
		$resref = mysql_query("SELECT * FROM t_dp_user WHERE UserName = '".cleanup($_POST['regrefusername'])."'");
		if(mysql_num_rows($resref) > 0){
			$referral = mysql_result($resref,0,"UserName");
			if(mysql_result($resref,0,"IP") == $_SERVER['REMOTE_ADDR'] && $_POST['regconfirm'] != 1){
				$valid = false;
				$_SESSION['sameip'] = true;
				$_SESSION['regrefusername'] = $referral;
			}else{
				$_SESSION['regrefusername'] = $referral;
			}
		}else{
			$valid = false;
			$_SESSION['error']['regrefusername'] = _errValidReferral_;
		}
	}*/
	
	if($valid){	
		$password = md5(cleanup($_POST['regpassword']));
		//echo $_POST['regcountry'];
		
		$rescountry = mysql_query("SELECT CountryName FROM t_dp_country WHERE CountryCode = '".cleanup($_POST['regcountry'])."'");
		$country_acro = mysql_result($rescountry,0,"CountryName");
		$req_country_ship = "SELECT * FROM t_dp_countryship WHERE CountryNameEng ='".$country_acro."'" ;
		$sql_country_ship = mysql_query($req_country_ship);
		
		$countryID = mysql_result($sql_country_ship,0,"CountryID");
		
		
		//$countryID = cleanup($_POST['regcountry']);
		
			$weblang = 2;
			$sublang = 2;
			$emailtemp = "emails/email_registration.htm";
		
		/* TODO: CHANGE TO NORMAL USER (2) INSTEAD OF TEST (3) */
		$userlevel = 2;
		$balance = 0;
		$isblocked = false;
		$blocked = mysql_query("SELECT * FROM t_dp_blacklist");
		$blocks = mysql_num_rows($blocked);
		for($i=0;$i<$blocks;$i++){
			if($_SERVER['REMOTE_ADDR'] == mysql_result($blocked,$i,"ip")){
				$userlevel = 5;
				$isblocked = true;
			}
		}
		
		$now = date("Y-m-d H:i:s");
		$insertuser = "INSERT INTO t_dp_user (LevelID, UserName, UserPassword, UserEmail, UserBalance, CountryID, UserAdded,WebLangID,SubLangID,IP) VALUES (";
		$insertuser.= $userlevel.",'".cleanup($_POST['regusername'])."','".$password."','".cleanup($_POST['regemail'])."',".$balance.",".$countryID.",'".$now."',".$weblang.",".$sublang.",'".$_SERVER["REMOTE_ADDR"]."')";
		//echo $insertuser;
		mysql_query($insertuser) or die(mysql_error());
		//echo $insertuser;
		
		if(!$isblocked){
				$activationlink = $http."activate.php?uh=".$password."&us=".base64_encode($now)."";
				$array_content[]=array("dpurl", $activationlink);  
				$array_content[]=array("username", cleanup($_POST['regusername'])); 
				$array_content[]=array("useremail", cleanup($_POST['regemail'])); 
				$array_content[]=array("usercountry", mysql_result($rescountry,0,"CountryName")); 
				$array_content[]=array("userreferrer", $referral); 
		 
				mailsending::sendingemail_phpmailer($array_content, $emailtemp,"includes/class.phpmailer.php","DramaPassion.com",$emailfrom,cleanup($_POST['regemail']),"Validation de votre adresse email"); 
		}
		
		$_SESSION['useremail'] = cleanup($_POST['regemail']);
		unset($_SESSION['sec_code']);
		$_SESSION['regusername'] = "";
		$_SESSION['regpassword'] = "";
		$_SESSION['regpasswordc'] = "";
		$_SESSION['regemail'] = "";
		$_SESSION['regrefusername'] = "";
		$_SESSION['regseccode'] = "";
		header("Location: registrationcomplete_mobile.php?email=".cleanup($_POST['regemail']));
		exit();
	}else{
		unset($_SESSION['sec_code']);
		header("Location: register_mobile.php");
		exit();
	}
}
?>