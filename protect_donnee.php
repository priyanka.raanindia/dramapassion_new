<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0">
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Politique de protection des données personnelles</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="cgv">   
<h3>POLITIQUE DE PROTECTION DES DONNÉES PERSONNELLES (ci-après PPDP)</h3>


<p>Vous comptez beaucoup sur Vlexhan Distribution et, comme prévu dans cette présente PPDP, Vlexhan Distribution s'engage à prendre les mesures nécessaires pour protéger les données personnelles des Utilisateurs et des Membres Enregistrés qui utilisent le Site de Vlexhan Distribution.</p>

<p>Les Utilisateurs et les Membres Enregistrés peuvent en outre consulter le registre public des traitements de la Commission Belge de la Protection de la Vie Privée : <a href="http://www.privacycommission.be/fr" class="lien_bleu">http://www.privacycommission.be/fr</a>.</p>

<p>Cette PPDP fait partie intégrante de nos Conditions Générales d'Utilisation et de Vente. Vous pouvez trouver nos Conditions Générales d'Utilisation et de Vente ainsi que les définitions des mots utilisés dans cette PPDP à <a href="<? echo $http; ?>conditions-generales/" class="lien_bleu"><? echo $http; ?>conditions-generales/</a>.</p>



<br />

<h4>Données sur l'entreprise – Responsable du Traitement des Données Personnelles</h4>

<p>Vlexhan Distribution est une société privée à responsabilité limitée (S.P.R.L.) ayant son siège social à Av. Louise 391, Bte 4, 1050 Bruxelles, Belgique et inscrite à la Banque-Carrefour des Entreprises (RPM Bruxelles) avec le numéro d'entreprise 0810.081.939. Le numéro de TVA intracommunautaire est BE 810.081.939. Cette entreprise est aussi le Responsable de Traitement des Données Personnelles en ce qui concerne les Données Personnelles traitées par ce Site.</p>



<br />

<h4>Quelles informations collectons-nous d'une manière directe ?</h4>

<p>Vlexhan Distribution collecte les Données Personnelles telles que votre pseudo (à condition d'être une Donnée Personnelle), l'adresse email (privée ou professionnelle), caractéristiques personnelles (date de naissance, sexe), les opinions subjectives concernant DramaPassion à condition que vous choisissiez volontairement de vous enregistrer et par la suite publiez volontairement des commentaires quant aux Produits qui sont proposés aux Membres Enregistrés sur le Site et finalement certaines Données Personnelles relatives aux parrainages (parrain) (c.-à-d. le pseudo ou le nom du parrain).</p>



<br />

<h4>Quelles informations collectons-nous d'une manière automatisée ?</h4>

<p>Nous collectons aussi les Données Personnelles des Utilisateurs et des Membres Enregistrés telles que les données d'identification électronique comme l'adresse IP, les moments de connexion, les données qui concernent le style de vie comme les détails concernant la consommation des Produits proposés par le Site et les données relatives à l'utilisation des médias et moyens de communication.</p>



<br />

<h4>Cookies</h4>

<p>Durant l'opération du Site, des « Cookies » peuvent être automatiquement installés sur le disque dur de votre ordinateur. Ces informations aident Vlexhan Distribution à personnaliser le Site selon les souhaits et les préférences de chaque visiteur. Consultez les instructions de la fonction d'aide de votre navigateur Internet pour plus de détails. Si vous, en tant qu'Utilisateur ou Membre Enregistré, acceptez l'utilisation de Cookies pendant une visite, ces Cookies peuvent être réutilisés lors d'une connexion ultérieure au Site. Par contre, vu que les « Cookies » permettent de profiter pleinement des avantages et des caractéristiques du Site, nous recommandons à nos Utilisateurs et nos Membres Enregistrés de les utiliser.</p>



<br />

<h4>Titulaire des Données Personnelles</h4>

<p>Toutes les Données Personnelles mentionnées sont conservées soit par Vlexhan Distribution, soit dans des bases de données détenues et administrées par Vlexhan Distribution ou ses mandataires ou des prestataires de services (p. ex., les Services d'Hébergement). Vlexhan Distribution est titulaire des droits sur ces bases de données et sur les informations qu'elles contiennent sauf s'il s'agit des bases de données administrées par des Sites Liés.</p>



<br />

<h4>Comment vos Données Personnelles et autres informations sont-elles traitées par Vlexhan Distribution ?</h4>

<p>Vlexhan Distribution utilise les Données Personnelles qui nous sont communiquées par les Utilisateurs et les Membres Enregistrés dans un but professionnel et en conformité avec la PPDP et les notifications faites à la Commission Belge de la Protection de la Vie Privée.</p>

<ul style="text-align:justify;"><li>Si vous, en tant qu'Utilisateur ou Membre Enregistré, nous fournissez des Données Personnelles pour une finalité précise, Vlexhan Distribution utilisera ces Données Personnelles en rapport avec cette finalité, comme, le cas échéant, traiter une commande que vous nous auriez passée. Par exemple, si un Membre Enregistré ou un simple Utilisateur nous contacte par courrier électronique, Vlexhan Distribution utilisera vos Données Personnelles pour répondre à l'Utilisateur ou au Membre Enregistré. Vlexhan Distribution a aussi le droit de combiner les Données Personnelles que les Utilisateurs ou les Membres Enregistrés nous fournissent avec des informations obtenues auprès des tiers.</li>

<br />

<li>Spécifiquement, les Données Personnelles des Utilisateurs et des Membres Enregistrés seront utilisées par Vlexhan Distribution pour l'administration de la clientèle, la gestion des commandes, les livraisons, la facturation des services (im-)matériels, l'enregistrement de la clientèle et le profil de cette clientèle sur base des achats. De plus, Vlexhan Distribution se réserve le droit d'employer les Données Personnelles des Utilisateurs et des Membres Enregistrés pour la lutte contre la fraude (il s'agit d'activités permettant de prévenir et de détecter de tels agissements qui portent atteinte aux Conditions Générales d'Utilisation et de Vente du Site et/ou à l'intérêt commercial de Vlexhan Distribution), la gestion de nos propres contentieux (y compris le recouvrement de créances ou le manque de gain causé par le non-respect des droits de distribution du contenu distribué par des tiers dans la zone géographique attribuée à Vlexhan Distribution), le contrôle d'accès (le traitement de données à caractère personnel en vue de garantir la sécurité des personnes ou des biens) et le marketing direct (la prospection, les activités et les services offerts par des firmes commerciales, à des segments de la population par le biais du courrier, du téléphone ou d'autres moyens directs, par ex. par e-mail). Si un Membre Enregistré ne désire un jour ne plus recevoir nos communications ou demander le retrait de votre nom de nos listes de diffusion, vous pouvez suivre la procédure de résiliation de l'inscription figurant dans notre communication et qui se trouve au bas de chaque communication destinée aux fins de marketing direct.</li>

<br />

<li>Vlexhan Distribution peut également utiliser les Données Personnelles des Utilisateurs et des Membres Enregistrés, et les autres informations collectées sur le Site pour améliorer le contenu et la fonctionnalité du Site, pour mieux comprendre les clients de Vlexhan Distribution et les marchés, pour améliorer les produits et les services de Vlexhan Distribution et pour réaliser et faciliter les ventes.</li></ul>


<br />

<h4>Est-ce que Vlexhan Distribution communique les informations que Vlexhan Distribution reçoit ?</h4>

<p>Pour Vlexhan Distribution, les informations des Membres Enregistrés constituent un élément essentiel de la relation entre Vlexhan Distribution et ses Membres Enregistrés. Vlexhan Distribution communique les informations, y compris les Données Personnelles à certaines sociétés, mais lorsque nous confions ces Données Personnelles (p. ex., lorsque nous choisissons une société non affiliée pour effectuer une prestation telle que l'envoi groupé d'informations, la maintenance des bases de données et l'hébergement), nous faisons en sorte de lui fournir uniquement les informations nécessaires à la réalisation de cette prestation et Vlexhan Distribution lui demande de protéger les Données Personnelles des Membres Enregistrés en application de cette PPDP et dans les limites fixées par Vlexhan Distribution.</p>

<p>En général, vos Données Personnelles telles que les informations d'identification directes et indirectes ne seront transmises par Vlexhan Distribution qu'à vous-même ou à des entreprises qui sont en relation directe avec le Responsable du Traitement.</p>

<p>Dans certains cas nous pouvons également communiquer vos Données Personnelles ainsi que d'autres informations en notre possession à certains tiers, dans un nombre limité de circonstances énumérées ci-dessous : </p>

<ul style="text-align:justify;"><li>Obligations légales : Vlexhan Distribution peut divulguer les Données Personnelles et autres informations appartenant à ses Membres Enregistres et les Utilisateurs en application de la loi ou en pensant de bonne foi que cette divulgation est nécessaire pour (a) se conformer à une obligation légale, (b) protéger et défendre les droits ou la propriété de Vlexhan Distribution, (c) en cas d'urgence, agir pour protéger la sécurité personnelle des utilisateurs du Site ou du public ou (d) se protéger contre des actions en responsabilité. Ceci peut impliquer le transfert des Données d'identification des Personnes Concernées ainsi que la communication des suspicions d'actes frauduleux ou qui portent atteinte aux Conditions d'Utilisation du Site aux instances compétentes telles que des Services Publics, le centre de lutte contre la discrimination et/ou les conseillers professionnels de la Personne Concernée.</li>

<br />

<li>Transfert d'activités, restructurations : à l'occasion du développement de nos affaires, nous pouvons vendre, acheter, restructurer ou réorganiser nos activités ou nos actifs. En cas de vente, fusion, réorganisation, restructuration, dissolution ou tout autre événement similaire impliquant nos activités ou nos actifs, des Données Personnelles peuvent faire partie des actifs transférés.</li></ul>

<br />

<p>Dans aucun cas les Données Personnelles (qu'il s'agisse des Utilisateurs ou des Membres Enregistrés) ne seront vendues à des tiers par Vlexhan Distribution sauf dans le cadre éventuel d'un transfert d'activité, d'une restructuration ou du développement de nos affaires.</p>



<br />

<h4>Droit d'accès et de rectification des données</h4>

<p>En ce qui concerne les informations que vous, en tant que Membres Enregistrés, avez données à Vlexhan Distribution d'une manière directe, c.-à-d. les caractéristiques personnelles (date de naissance, sexe, adresse, etc.), vous avez vous-même le droit d'accès et de rectifications en cliquant sur votre pseudo une fois connecté au Site. Pour toutes autres informations, vous pouvez adresser - par lettre recommandée – une lettre à Vlexhan Distribution à l'adresse suivante : </p>

<ul style="text-align:justify;"><li>Vlexhan Distribution (à l'attention du D.G.I.)<br />
    Av. Louise 391 Bte 4<br />
1050 Bruxelles<br />Belgique</li></ul>

<p>N'oubliez pas d'inclure une copie de votre pièce d'identification.</p>



<br /><br />

<a class="lien_bleu" href=#>Retourner au sommet de la page</a>


<br /><br /></div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>