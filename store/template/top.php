<?php
$paques = 0;
if(time() >= 1458640800 && time() <= 1459893599 ){
	$paques = 1;
		
}
?>

<!DOCTYPE html>
<html>
  <head lang="fr">
    <title>Dramapassion Store</title>
    <meta charset="utf-8">
    <!-- Bootstrap -->
    <link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css?<?php echo date('s'); ?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo $server; ?>css/font-awesome.min.css">
    <link href="<?php echo $http ; ?>store/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $http ; ?>store/css/style_index.css?<?php echo time() ;?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css?<?php echo time() ;?>" type="text/css" media="screen" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-full">
<!-- navbar  -->
	 <nav class="navbar navbar-default navbar-static-top navbar1" role="navigation">
	  	<!-- container -->
	  	<div class="container">
		  <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header pull-left navbar-droite">
	      	<a class="navbar-brand" href="<?php echo $http ; ?>"><img alt="logo DramaPassion" title="Aller à la page d'accueil" src="http://www.dramapassion.com/images/logo.png" border="0" class="img-responsive"></a>
	      	<ul class="nav navbar-nav navbar-ul1 recherche-top">
			      <li><a href="<?php echo $http ; ?>catalogue/">VIDEOS</a></li>
			      <li><a href="<?php echo $http ; ?>premium/">PREMIUM</a></li>
			      <li><a href="<?php echo $http ; ?>store/" class="store_logo">STORE</a></li>
			 </ul>
			  <form class="navbar-form navbar-right navbar-input-group recherche-top" role="search" action="<?php echo $http ;?>catalogue/" method="POST">
				  <div class="form-group recherche-top">
				  	<?php if($_SESSION['zone_recherche'] != "" && $_SESSION['zone_recherche'] != ""){?>
				  		<input type="text" name="recherche_top" id="top_recherche" value="<?php echo $_SESSION['zone_recherche'] ; ?>" class="form-control recherche-top" placeholder="Chercher un titre">
				  	<?php }else{ ?>
				  		<input type="text" name="recherche_top" id="top_recherche" value="" class="form-control recherche-top" placeholder="Chercher un titre">
				  	<?php } ?>
				  </div>
				  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			  </form>
		  </div>
	  	</div>
	  	<!-- end container -->
	</nav>
	  	<!-- end navbar  --> 
  

<!-- navbar  -->
	 <nav class="navbar navbar-default navbar-static-top navbar2" role="navigation">
	  	<!-- container -->
	  	<div class="container">
		  <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header pull-left">
		  </div>
		  <?php 
		  	if($user_tab['Subscription'] > 0){
			  ?>
			  <div class="navbar-header pull-right">
		     	  <ul class="nav navbar-nav navbar-ul2">
			      <?php if(in_array($_SESSION['subscription'], $array_dis)){ 
						$abo_statut = '[Découverte] ';     						
					}
					elseif(in_array($_SESSION['subscription'], $array_pri)){ 
						$abo_statut = '[Privilège] ';
					}else{ 
						$abo_statut = '';                   
					} ?>
			      <li class="dropdown">
				    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				      <?php echo $_SESSION['username']." ".$abo_statut ; ?><span class="caret"></span>&nbsp;&nbsp; 
				    </a>
				    <ul class="dropdown-menu">
				      <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $http ;?>compte/abonnement/">Mon Abonnement</a></li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $http ;?>compte/profil/">Mon Profil</a></li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $http ;?>compte/paiements/">Mes Paiements</a></li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $http ;?>compte/telechargements/">Mes Téléchargements</a></li>
				    </ul>
				  </li>
			      <li><a href="#">|</a></li>
			      <li><a href="<?php echo $http ; ?>compte/playlist/" title="Voir ma playlist">Ma playlist</a></li>
			      <li><a href="#">|</a></li>
			      <li><a href="<?php echo $http ; ?>logoutAction.php" title="Me déconnecter">Déconnexion</a></li>
			    </ul>
			 </div>
			 <?php	
		  	}else{
			  ?>
			  <div class="navbar-header pull-right">
		     	  <ul class="nav navbar-nav navbar-ul2">
			      <li><a <?php if(isset($_SESSION['userid']) && $_SESSION['userid'] != ""){?><?php }else{ ?>href="<?php echo $http;?>connexion_store.php" class='iframe log'<?php } ?>><span class="glyphicon glyphicon-user"></span> S'identifier</a></li>
			      <li><a href="#">|</a></li>
			      <li><a href="#">Créer un compte</a></li>
			    </ul>
			 </div>
			 <?php
			}
			?>
		</div>
	  	<!-- end container -->
		</nav>