<?
require_once('includes/db_settings.php');
require_once('includes/inc.settings.php');
require_once('includes/inc.general.php');
require_once('includes/inc.index.php');
require_once('includes/inc.user.php');
require_once('template/top.php');






$type_1 = 0;

?>
<div class="container" id="container_princ" >
	<div class="row">
		<div class="col-xs-12">
			<h3 class="souligner h3_title">Mon panier</h3>
				<table class="tab_panier" style="margin:auto;">
					<tr>
						<td></td>
						<td class="txt-tab th" width="300">Article</td>
						<td class="txt-tab th center" width="150">Prix</td>
						<td class="txt-tab th" width="150" style="text-align:center;">Supprimer</td>
					</tr>
			<?php
								$tab_panier = unserialize($_SESSION['cont_panier']);
								$nb_panier = count($tab_panier);
								if($nb_panier == 0){
									header('Location: '.$http.'store/');
								}
								foreach($tab_panier as $key => $value){
								
								if($value['type'] == 1){
									$req_dvd = "SELECT * FROM t_dp_dvd WHERE dramaID_actor = ".$value['id'];
									$sql_dvd = mysql_query($req_dvd);
									$prix = mysql_result($sql_dvd,0, 'PriceTvac');
									$txt_panier = 'Coffret DVD';
								}elseif($value['type'] == 2){
									$req_tel = "SELECT * FROM store WHERE dramaID =".$value['id'];
									$sql_tel = mysql_query($req_tel);
									$txt_panier = 'Téléchargement définitif';
									if($user_tab['SubscriptionID'] > 0){
										$prix = mysql_result($sql_tel,0, 'prix_abo');
									}else{
										$prix = mysql_result($sql_tel,0, 'prix');
									}
									
								}
								$prix = str_replace('.', ',', $prix);
								
								?>
										<tr>
											<td class="center"><img src="<?php echo $value['img'] ; ?>"  alt="" style="max-height:70px;" /></td>
											<td class="txt-tab"><?php echo $value['txt'] ; ?><br /><span class="txt-sub-tab"><?php echo $txt_panier ; ?></span></td>
											<td class="txt-tab center"><?php echo $prix ; ?> €</td>
											<td style="text-align:center;vertical-align:middle;"><span class="glyphicon glyphicon-remove-circle btn_delete" style="top:0px !important;" id="btn_<?php echo $key ; ?>" data-type="<?php echo htmlentities($value['type']) ; ?>" data-id="<?php echo htmlentities($value['id']) ; ?>" data-idpanier="<?php echo $key ; ?>"></span></td>
										</tr>
									
									
								<?php
									
									if($value['type'] == 1){
										$type_1++;
									}
								}
							?>
							
							<tr>
							<td></td>
							<td class="txt-tab th">total</td>
							<td class="txt-tab center" style="font-weight:bold;">
								<?php
										if($user_tab['SubscriptionID'] > 0){
											$prix_tot = $_SESSION['prix_abo'];
										}else{
											$prix_tot = $_SESSION['prix'];
										}
										$prix_tot = str_replace('.',',', $prix_tot);
									?>
									<?php echo $prix_tot ; ?> €
							</td>
							<td></td>
								
								</table>
								</div>
							</div>
							<form class="form-horizontal" role="form" method="post" action="panier_check.php">
							<div style="margin-top:70px;">
					<h3 class="souligner h3_title" >Adresse de Facturation</h3>
					  <div class="form-group <?php if(isset($_SESSION['error']['oflname'])){ echo 'has-error';} ?>">
					    <label for="oflname" class="col-xs-3 control-label">Nom</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="oflname" name="oflname" placeholder="Nom" value="<?php echo $user_tab["UserLname"]; ?>">
					      <?php if(isset($_SESSION['error']['oflname'])){ echo '<label class="control-label" for="oflname">'.$_SESSION['error']['oflname'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['offname'])){ echo 'has-error';} ?>">
					    <label for="offname" class="col-xs-3 control-label">Prénom</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="offname" name="offname" placeholder="Prénom" value="<?php echo $user_tab["UserFname"]; ?>">
					      <?php if(isset($_SESSION['error']['offname'])){ echo '<label class="control-label" for="offname">'.$_SESSION['error']['offname'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['ofaddress1'])){ echo 'has-error';} ?>">
					    <label for="ofaddress1" class="col-xs-3 control-label">Adresse (ligne 1) :</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="ofaddress1" name="ofaddress1" placeholder="Adresse" value="<?php echo $user_tab["UserAddress1"]; ?>">
					      <?php if(isset($_SESSION['error']['ofaddress1'])){ echo '<label class="control-label" for="ofaddress1">'.$_SESSION['error']['ofaddress1'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['ofaddress2'])){ echo 'has-error';} ?>">
					    <label for="inputAdresse" class="col-xs-3 control-label">Adresse (ligne 2) :</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="inputAdresse2" name="ofaddress2" placeholder="Adresse" value="<?php echo $user_tab["UserAddress2"]; ?>">
					      <?php if(isset($_SESSION['error']['inputAdresse2'])){ echo '<label class="control-label" for="inputAdresse2">'.$_SESSION['error']['inputAdresse2'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['ofzip'])){ echo 'has-error';} ?>">
					    <label for="ofzip" class="col-xs-3 control-label">Code postal</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="ofzip" name="ofzip" placeholder="Code postal" value="<?php echo $user_tab["UserZip"]; ?>">
					      <?php if(isset($_SESSION['error']['ofzip'])){ echo '<label class="control-label" for="ofzip">'.$_SESSION['error']['ofzip'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['ofcity'])){ echo 'has-error';} ?>">
					    <label for="ofcity" class="col-xs-3 control-label">Ville</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="ofcity" name="ofcity" placeholder="Ville" value="<?php echo $user_tab["UserCity"]; ?>">
					      <?php if(isset($_SESSION['error']['ofcity'])){ echo '<label class="control-label" for="ofcity">'.$_SESSION['error']['ofcity'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['ofcountry'])){ echo 'has-error';} ?>">
					    <label for="ofcountry" class="col-xs-3 control-label">Pays</label>
					    <div class="col-xs-8">
					      <select name="ofcountry" id="ofcountry" class="form-control">
		                    <?php 
		                    $countries = mysql_query("SELECT * FROM t_dp_countryship ");
		                    for($i=0;$i<mysql_num_rows($countries);$i++){ 
							$selected = "";
							if($user_tab["CountryID"] > 0){
								if($user_tab["CountryID"] == mysql_result($countries,$i,"CountryID")){
									$selected = "SELECTED";
								}
							}else if(mysql_result($countries,$i,"CountryID") == 66){
								$selected = "SELECTED";
							}
							?>
		                    <option value=<?php echo "\"".mysql_result($countries,$i,"CountryID")."\" ".$selected; ?>><?php echo mysql_result($countries,$i,"CountryNameFre"); ?></option>
		                    <?php } ?>
		                  </select>
		                  <?php if(isset($_SESSION['error']['ofcountry'])){ echo '<label class="control-label" for="ofcountry">'.$_SESSION['error']['ofcountry'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['ofmobile'])){ echo 'has-error';} ?>">
					    <label for="ofmobile" class="col-xs-3 control-label">Numéro de téléphone</label>
					    <div class="col-xs-8">
					      <input type="tel" class="form-control" id="ofmobile" name="ofmobile" placeholder="Numéro de téléphone" value="<?php echo $user_tab["UserMobile"]; ?>">
					      <?php if(isset($_SESSION['error']['ofmobile'])){ echo '<label class="control-label" for="ofmobile">'.$_SESSION['error']['ofmobile'].'</label>';} ?>
					    </div>
					  </div>
			<?php 
			if($type_1 > 0){
				?>
				<div style="margin-top:70px;">
					<h3 class="souligner h3_title" >Adresse de Livraison</h3>
					<div class="form-group">
					    <div class="col-xs-offset-3 col-xs-8">
					      <div class="checkbox">
					        <label>
					          <input type="checkbox" id="livraison" name="livraison" value="1" <?php if(isset($_SESSION['data']['livraison']) && $_SESSION['data']['livraison']==1){ echo checked;} ?> /> L'adresse de livraison est différente de l'adresse de facturation.
					        </label>
					      </div>
					    </div>
					  </div>
					  <div style="display:none;" id="livraison_dif">
					  <div class="form-group <?php if(isset($_SESSION['error']['ollname'])){ echo 'has-error';} ?>">
					    <label for="ollname" class="col-xs-3 control-label">Nom</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="ollname" name="ollname" placeholder="Nom" value="<?php if(isset($_SESSION['data']['ollname'])){echo $_SESSION['data']['ollname'];} ?>" />
					      <?php if(isset($_SESSION['error']['ollname'])){ echo '<label class="control-label" for="ollname">'.$_SESSION['error']['ollname'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['olfname'])){ echo 'has-error';} ?>">
					    <label for="olfname" class="col-xs-3 control-label">Prénom</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="olfname" name="olfname" placeholder="Prénom" value="<?php if(isset($_SESSION['data']['olfname'])){echo $_SESSION['data']['olfname'];} ?>" />
					      <?php if(isset($_SESSION['error']['olfname'])){ echo '<label class="control-label" for="olfname">'.$_SESSION['error']['olfname'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['oladdress1'])){ echo 'has-error';} ?>">
					    <label for="oladdress1" class="col-xs-3 control-label">Adresse (ligne 1) :</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="oladdress1" name="oladdress1" placeholder="Adresse" value="<?php if(isset($_SESSION['data']['oladdress1'])){echo $_SESSION['data']['oladdress1'];} ?>" />
					      <?php if(isset($_SESSION['error']['oladdress1'])){ echo '<label class="control-label" for="oladdress1">'.$_SESSION['error']['oladdress1'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['oladdress2'])){ echo 'has-error';} ?>">
					    <label for="oladdress2" class="col-xs-3 control-label">Adresse (ligne 2) :</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="oladdress2" name="oladdress2" placeholder="Adresse" value="<?php if(isset($_SESSION['data']['oladdress2'])){echo $_SESSION['data']['oladdress2'];} ?>" />
					      <?php if(isset($_SESSION['error']['oladdress2'])){ echo '<label class="control-label" for="oladdress2">'.$_SESSION['error']['oladdress2'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['olzip'])){ echo 'has-error';} ?>">
					    <label for="olzip" class="col-xs-3 control-label">Code postal</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="olzip" name="olzip" placeholder="Code postal" value="<?php if(isset($_SESSION['data']['olzip'])){echo $_SESSION['data']['olzip'];} ?>" />
					      <?php if(isset($_SESSION['error']['olzip'])){ echo '<label class="control-label" for="olzip">'.$_SESSION['error']['olzip'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['olcity'])){ echo 'has-error';} ?>">
					    <label for="olcity" class="col-xs-3 control-label">Ville</label>
					    <div class="col-xs-8">
					      <input type="text" class="form-control" id="olcity" name="olcity" placeholder="Ville" value="<?php if(isset($_SESSION['data']['olcity'])){echo $_SESSION['data']['olcity'];} ?>" />
					      <?php if(isset($_SESSION['error']['olcity'])){ echo '<label class="control-label" for="olcity">'.$_SESSION['error']['olcity'].'</label>';} ?>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['olcountry'])){ echo 'has-error';} ?>">
					    <label for="olcountry" class="col-xs-3 control-label">Pays</label>
					    <div class="col-xs-8">
					      <select name="olcountry" id="olcountry" class="form-control">
		                    <?php for($i=0;$i<mysql_num_rows($countries);$i++){ 
								$selected = "";
								if(isset($_SESSION['data']['olcountry'])){
									if($_SESSION['data']['olcountry'] == mysql_result($countries,$i,"CountryID")){
										$selected = "SELECTED";
									}
								}else{
									if(mysql_result($user,0,"CountryID") > 0){
										if(mysql_result($user,0,"CountryID") == mysql_result($countries,$i,"CountryID")){
											$selected = "SELECTED";
										}
									}else{
										if(mysql_result($countries,$i,"CountryID") == 66){
											$selected = "SELECTED";	
										}
									}
								}
								?>
			                    <option value=<?php echo "\"".mysql_result($countries,$i,"CountryID")."\" ".$selected; ?>><?php echo mysql_result($countries,$i,"CountryNameFre"); ?></option>
			                    <?php } ?>
			                    <?php if(isset($_SESSION['error']['olcountry'])){ echo '<label class="control-label" for="olcountry">'.$_SESSION['error']['olcountry'].'</label>';} ?>
		                  </select>
					    </div>
					  </div>
					  <div class="form-group <?php if(isset($_SESSION['error']['olmobile'])){ echo 'has-error';} ?>">
					    <label for="olmobile" class="col-xs-3 control-label">Numéro de téléphone</label>
					    <div class="col-xs-8">
					      <input type="tel" class="form-control" id="olmobile" name="olmobile" placeholder="Numéro de téléphone" value="<?php if(isset($_SESSION['data']['olmobile'])){echo $_SESSION['data']['olmobile'];} ?>" />
					      <?php if(isset($_SESSION['error']['olmobile'])){ echo '<label class="control-label" for="olmobile">'.$_SESSION['error']['olmobile'].'</label>';} ?>
					    </div>
					  </div>
					  </div>
			<?php
			}
			?>
					  <div class="center " style="margin-top:30px;">
					  	<input type="hidden" name="order" value="1" />
					      <button type="submit" class="btn btn-success div-valide-panier">Commander</button>
					  </div>
					</form>
				</div>	
				</div>		
		</div>
	</div>
</div>
<br /><br /><br /><br />
<?php require_once('template/footer.php') ; ?>
<script>
    $( document ).ready(function() {
		if($("#livraison").is(':checked')){
			$("#livraison_dif").show();  // checked
		}
		
		
  	});
  	$(window).load(function() {
  		hauteur = $( window ).height() - 570;
		$('#container_princ').css('minHeight',hauteur+'px');
		//$('#container_princ').css('height',hauteur+'px');
     	$('.label-tooltip').tooltip('show');
     	largeur_fenetre = $(document).width();
     	largeur_ecran = $(window).width();
     	if(largeur_ecran < 1000){
     		$('.navbar1').css('width',largeur_fenetre+'px');
	 		$('.navbar2').css('width',largeur_fenetre+'px');
     	}else{
	     	$('.navbar1').css('width','100%');
		 	$('.navbar2').css('width','100%');
     	}
     	oqp = 0;
	 });
  	
	 $( window ).resize(function() {
		 largeur_fenetre = $(document).width();
		 largeur_ecran = $(window).width();
		 if(largeur_ecran < 1000){
			 $('.navbar1').css('width',largeur_fenetre+'px');
			 $('.navbar2').css('width',largeur_fenetre+'px');
     	}else{
	     	$('.navbar1').css('width','100%');
		 	$('.navbar2').css('width','100%');
     	}
  		});
  	$(".btn_delete").click(function() {
	  	type = $(this).data("type");
	  	id = $(this).data("id");
	  	idpanier = $(this).data("idpanier");
	  	url = "<?php echo $http ; ?>store/remove_panier.php?type="+type+"&id="+id+"&id_panier="+idpanier;
	  	$.getJSON(url,function(result){
		    if(result.error == 0){
		    	
			    location.reload();
			    
		    }
		    
		  });
		  
	});
	$("#livraison").click(function() {
	  	$('#livraison_dif').toggle();
		  
	});
	
</script>
    </div>
  </body>
</html>
