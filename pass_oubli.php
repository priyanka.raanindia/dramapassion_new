<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	if(isset($_SESSION['email_perdu']) && $_SESSION['email_perdu'] != ""){ 
		$email_perdu = $_SESSION['email_perdu'];
	}
	
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (Sans titre-1) -->
<?php 
?>
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Retrouver mes identifiants</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                            
                    </td>
                </tr>
			
			<tr>
			<form method="post" action="<?php echo $http ; ?>pass_oubli_action.php">
				<table border="0" cellpadding="1" cellspacing="1">
			
				
			
			<?php 
			if(isset($_SESSION['error']['email']) && $_SESSION['error']['email'] != ""){ 
			$erreur = $_SESSION['error']['email'];
			?>
                    	<tr>
							<td></td>
                        	<td colspan="2"><span class="rouge"><?php echo $erreur ; ?></span></td>
                            
						</tr> 
			<?php 
			$_SESSION['error']['email'] = "";
			$_SESSION['error']['secure'] = "";
			}
			
			elseif(isset($_SESSION['error']['secure']) && $_SESSION['error']['secure'] != ""){ 
			$erreur = $_SESSION['error']['secure'];
			?>
                    	<tr>
							<td></td>
                        	<td colspan="2"><span class="rouge"><?php echo $erreur ; ?></span></td>
                            
						</tr> 
			<?php 
			$_SESSION['error']['secure'] = "";
			$_SESSION['error']['email'] = "";
			
			}else{
			?>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2"></td>
				<tr>
			<?php } ?>
				<tr>
				<td width="136">&nbsp;</td>
				<td>Votre adresse email :</td>
				<td><input type="text" name="email" class="form" style="width:175px; height:27" value="<?php echo $email_perdu ; ?>"/></td>
				</tr>
			
				<tr>
				<td></td>
				<td></td>
				<td><img src="<?php echo $http ; ?>images/captchaCreate.php" ></td>
				<tr>
			
				<tr>
				<td></td>
				<td>Code de sécurité :</td>
				<td><input type="text" name="secure" class="form" style="width:175px; height:27" /></td>
				</tr>
			
				<tr>
				<td></td>
				<td></td>
				<td><input src="<?php echo $http ; ?>images/submit_send.png" type="image"></td>
				</tr>
				</table>
				<input type="hidden" name="register" />
			</form>
			
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>

 <script type="text/javascript">
 <?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
	
	if (w > 0) window.parent.$('.iframe_fancy').css({ width: w+"px"});
    if (h > 0) window.parent.$('.iframe_fancy').css({ height: h+"px"});
	
    if (w > 0) window.parent.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php }else{ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php } ?>
fb_resize(600,240);

function fermer(){
	window.parent.$.fancybox.close();
}

</script>                 
</body>
</html>