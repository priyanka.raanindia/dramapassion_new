<?php
function DramaInfo($id){
	global $http;
	$sql = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$id."'");
	$drama['titre'] = mysql_result($sql,0,"DramaTitle");
	$drama['titre_org'] = mysql_result($sql,0,"DramaTitle2");
	$drama['anne_sortie'] = mysql_result($sql,0,"DramaYear");
	$drama['nb_epi'] = mysql_result($sql,0,"DramaEpisodes");
	$drama['synopsis']= mysql_result($sql,0,"DramaSynopsisFre");
	$drama['shortcut'] = mysql_result($sql,0,"DramaShortcut");
	$img_clean = clean_img($drama['titre']);
	$drama['img_big'] = $http."content/dramas/".$img_clean."_Big.jpg";
	$drama['img_thumb'] = $http."content/dramas/".$img_clean."_Thumb.jpg";
	$drama['img_detail'] = $http."content/dramas/".$img_clean."_Detail.jpg";
	
	$drama['cate'] = mysql_result($sql,0,"Categorie");
	$drama['release'] = mysql_result($sql,0,"ReleaseDate");
	$drama['HD'] = mysql_result($sql,0,"HD");
	$drama['nb_photo'] = mysql_result($sql,0,"nb_photo");
	$drama['photo_car'] = mysql_result($sql,0,"photo_car");
	$drama['LicensorID'] = mysql_result($sql,0, "LicensorID");
	$drama['mineur'] = mysql_result($sql,0,"mineur");
	$drama['link'] = $http.'drama/'.$id.'/'.clean_link($drama['titre']);
	$drama['id'] = $id ;

	
		$drama['group'] = mysql_result($sql,0,"group");

	
	$drama['chaine_temp'] = mysql_result($sql,0,"ProducerID");
	$sql_chaine = mysql_query("SELECT * FROM t_dp_producer WHERE ProducerID ='".$drama['chaine_temp']."'");
	$drama['chaine'] = mysql_result($sql_chaine,0,"ProducerName");
	$drama['type_stream'] = mysql_result($sql, 0, 'type_stream');
	$drama['keyword'] = mysql_result($sql, 0, 'keyword');
	$drama['StatusID'] = mysql_result($sql,0, 'StatusID');
	
	
	$lg_max = 180; //nombre de caractère autoriser
	$chaine = $drama['synopsis'];
	if (strlen($chaine) > $lg_max){
		$chaine = substr($chaine, 0, $lg_max);
		$last_space = strrpos($chaine, " ");
		$chaine = substr($chaine, 0, $last_space)." ...";
	}
	$drama['synopsis_mini'] = $chaine;
	
	
	$type_dispo = mysql_query("SELECT * FROM t_dp_episode WHERE (DramaID ='".$id."' AND  EpisodeNumber=1)");
	$time_type_dispo = mysql_result($type_dispo ,0,"ReleaseDateFree");
	list($year1, $month1, $day1) = split('-', $time_type_dispo);
	$time_type_dispo = mktime(0, 0, 0, $month1, $day1, $year1);
	
	
	if($time_type_dispo > time()){
		$drama['logo_sortie'] = 'img/abonnement_1_mini.png';
	}else{
		$drama['logo_sortie'] = 'img/abonnement_2_mini.png';
	}
	
	
	
	return $drama ;
}
function clean_img($name){
	//$name = str_replace('\'','',$name);
	//$name = str_replace(',','',$name);
	//$name = str_replace('.','',$name);
	//$name = str_replace('!','',$name);
	$name = str_replace('?','',$name);
	//$name = str_replace('à','a',$name);
	$name = str_replace(':','',$name);
	
	return $name;
}

function clean_link($name){
	$name = str_replace(' ', '-' ,$name);
	$name = str_replace('\'','',$name);
	$name = str_replace(',','',$name);
	$name = str_replace('.','',$name);
	$name = str_replace('!','',$name);
	$name = str_replace('?','',$name);
	$name = str_replace('à','a',$name);
	$name = str_replace(':','-',$name);
	
	return $name;
}

function infoUser($userID){
	global $array_dis;
	global $array_pri;
	$req = "SELECT * FROM t_dp_user WHERE userID =".$userID;
	$sql = mysql_query($req);
	$nb_user = mysql_num_rows($sql);
	$user_tab = array();
	
	$user_tab['Subscription'] = 0;
	if($nb_user > 0){
		$user_tab['UserID'] = mysql_result($sql,0, 'UserID');
		$user_tab['LevelID'] = mysql_result($sql,0, 'LevelID');
		$user_tab['UserName'] = mysql_result($sql,0, 'UserName');
		$user_tab['UserFname'] = mysql_result($sql,0, 'UserFname');
		$user_tab['UserLname'] = mysql_result($sql,0, 'UserLname');
		$user_tab['UserPassword'] = mysql_result($sql,0, 'UserPassword');
		$user_tab['UserEmail'] = mysql_result($sql,0, 'UserEmail');
		$user_tab['UserBalance'] = mysql_result($sql,0, 'UserBalance');
		$user_tab['UserReferrer'] = mysql_result($sql,0, 'UserReferrer');
		$user_tab['SexID'] = mysql_result($sql,0, 'SexID');
		$user_tab['UserBirthdate'] = mysql_result($sql,0, 'UserBirthdate');
		$user_tab['CountryID'] = mysql_result($sql,0, 'CountryID');
		$user_tab['UserProvince'] = mysql_result($sql,0, 'UserProvince');
		$user_tab['UserCity'] = mysql_result($sql,0, 'UserCity');
		$user_tab['UserAddress1'] = mysql_result($sql,0, 'UserAddress1');
		$user_tab['UserAddress2'] = mysql_result($sql,0, 'UserAddress2');
		$user_tab['UserZip'] = mysql_result($sql,0, 'UserZip');
		$user_tab['UserMobile'] = mysql_result($sql,0, 'UserMobile');
		$user_tab['Active'] = mysql_result($sql,0, 'Active');
		$user_tab['Alias'] = mysql_result($sql,0, 'Alias');
		$user_tab['SubscriptionID'] = mysql_result($sql,0, 'SubscriptionID');
		$user_tab['Next'] = mysql_result($sql,0, 'Next');
		$user_tab['Expiry'] = mysql_result($sql,0, 'Expiry');
		$user_tab['autorenewal'] = mysql_result($sql,0, 'autorenewal');
		$user_tab['userIDfacebook'] = mysql_result($sql,0, 'userIDfacebook');
		$user_tab['userIDfacebook'] = mysql_result($sql,0, 'userIDfacebook');
		
	
		if(in_array($user_tab['SubscriptionID'], $array_dis)){
			$user_tab['SubscriptionTxt'] = 'Découverte';
			$user_tab['Subscription'] = 2;
		}elseif(in_array($user_tab['SubscriptionID'], $array_pri)){
			$user_tab['SubscriptionTxt'] = 'Privilège';
			$user_tab['Subscription'] = 3;
		}else{
			$user_tab['SubscriptionTxt'] = '';
			$user_tab['Subscription'] = 1;
		}
		
		
	}
	return $user_tab ;
}
function cleanup($data) {
   $data = mysql_real_escape_string(trim(strip_tags($data)));
   return $data;
}
function getIp2Location($ip){
	$ipa = array();
	$ipa = explode(".",$ip);
	$ipno = ($ipa[0] * (256*256*256)) + ($ipa[1] * (256*256)) + ($ipa[2] * (256)) + $ipa[3];
	$rescountry = mysql_query("SELECT * FROM ipcountry WHERE IPFROM <= ".$ipno." AND IPTO >= ".$ipno."");
	$countrycode = mysql_result($rescountry,0,"COUNTRYSHORT");
	return $countrycode;
}


?>