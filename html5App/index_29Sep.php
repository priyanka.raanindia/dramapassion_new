<?php 
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect_free.php");

// Protégé page !!!!!

	$dramaID = $_GET['d'];
	$epinb = $_GET['e'];
	$epiurl = $epinb;
	if($epinb < 10){
		$epinb = "0".$epinb;
	}
	
	if(isset($_GET['p']) && $_GET['p'] > 0){
		$part = $_GET['p'];
	}else{
		$part = 1;
	}
	
	$drama_tab = DramaInfo($dramaID);
	$epiInfo = EpiInfo($dramaID,$epiurl);
	$partnb = $epiInfo['Part'];
	$short = $drama_tab['shortcut'];
	$file = $drama_tab['shortcut'].$epinb."-fd".$part.'.mp4';
	
	$today = gmdate("n/j/Y g:i:s A");
	$initial_url = "http://nf09.dramapassion.com:8081/pdl/".$short."/".$file."";
	$ip = $_SERVER['REMOTE_ADDR'];
	$key = "HoYkdy83dG"; //this is also set up in WMSPanel rule
	$validminutes = 120;
	
	$str2hash = $ip . $key . $today . $validminutes;
	$md5raw = md5($str2hash, true);
	$base64hash = base64_encode($md5raw);
	$urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=$validminutes";
	$base64urlsignature = base64_encode($urlsignature);
	
	$signedurlwithvalidinterval = "$initial_url?wmsAuthSign=$base64urlsignature";
	$url_thumb = "http://nf09.dramapassion.com/thumb/".$short."/".$short.$epinb."-".$part."_thumb.jpg";
	
	
	$npart = $part +1;
	$bpart = $part -1;
	$next = "http://www.dramapassion.com/html5App/index.php?d=".$dramaID."&e=".$epiurl."&p=".$npart ; 
	$before = "http://www.dramapassion.com/html5App/index.php?d=".$dramaID."&e=".$epiurl."&p=".$bpart ; 
	
	
	
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name ='viewport' content ="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>Dramapassion</title>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <style>
	body{
		background-color: black; 
		width: 100%;
		height: 100%;
		padding : 0;
		margin : 0;
	}
  #my_video{
	  width: 100%;
	  height: 80%; 
  }
  #cont_my_video{
	  width: 80%;
	  height : 100%;
	  float: left;

	  
  }
  #cont_btnD{
	  width: 10%;
	  float: left;
	  color: white;
	  text-align: right;
	  
  }
  #cont_btnG{
	  width: 10%;
	  float: left;
	  color: white;
	 
	  
  }
  #btnG{
	  margin-left: 50%;
	  margin-top: 200%;
  }
  #btnD{
	  margin-right: 50%;
	  margin-top: 200%;
  }
  .fa{
	  font-size: 40px;
	  color: white;
  }
  .fa:hover{
	  color: white;
  }
  .fa:visited{
	  color: white;
  }
  .fa:focus{
	  color: white;
  }
  #cont_info_video{
	  color: white;
	  text-align: center;
	  font-size: 20px;
	  padding-top: 10px;
  }
  #pub{
	  margin-top: 40px;
	  background-color: red;
	  width: 100%;
  }
   
  
  </style>
</head>
<body>
<div id="cont_btnG">
	<div id="btnG">
	<?php
		if($part > 1){
	?>
	<a href="<?php echo $before ; ?>"><i class="fa fa-angle-left"></i></a>
	<?php
		}
	?>
	</div>
</div>
<div id="cont_my_video">
	<video id="my_video" poster="<?php echo $url_thumb ; ?>" controls preload='none' autoplay="autoplay">
	<source id='mp4' src="<?php echo $signedurlwithvalidinterval ; ?>" controls="controls" type='video/mp4'>
	</video>
</div>
<div id="cont_btnD">
	<div id="btnD">
	<?php
		if($part < $partnb){
	?>
	<a href="<?php echo $next ; ?>"><i class="fa fa-angle-right"></i></a>
	<?php
		}	
	?>
</div>
</div>
<div style="clear: both;"></div>
<div id="cont_info_video">
	<?php
		echo 	'Partie '.$part.' sur '.$partnb;
	?>
</div>
<div id="pub">
			<script type='text/javascript'>
			<!--//<![CDATA[
				var div = document.getElementById('PutPub');
				var contentdiv = "";
			   document.MAX_ct0 ='';
			   var m3_u = (location.protocol=='https:'?'https://cas.criteo.com/delivery/ajs.php?':'http://cas.criteo.com/delivery/ajs.php?');
			   var m3_r = Math.floor(Math.random()*99999999999);
			   var url = m3_u+"zoneid=158911"+"&nodis=1"+"&cb="+ m3_r;
			   if (document.MAX_used != ',') url = url+"&exclude="+document.MAX_used;
			   url = url + (document.charset ? '&charset='+document.charset : (document.characterSet ? '&charset='+document.characterSet : ''));
			   url = url+"&loc=" + escape(window.location);
			   if (document.referrer) url = url+"&referer=" + escape(document.referrer);
			   if (document.context) url = url+"&context=" + escape(document.context);
			   if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
			       url = url+"&ct0=" + escape(document.MAX_ct0);
			   }
			   if (document.mmm_fo) url = url+"&mmm_fo=1";

			   url = "<div style='margin:auto;width:728px;'><scr"+"ipt type='text/javascript' src='"+url;
			   url = url+"'></scr"+"ipt></div>";
			   
			   var print_pub = function(){
				   document.write(url);
			   }
			   

			   
			//]]>-->
		</script>
</div>
<script src="videoplaza.js"></script>
<script>
	var hauteur_video = $("body").height();
	var hauteur_fenetre = $(window).height();
	if((hauteur_fenetre - hauteur_video) > 150){
		print_pub();
		//$("#pub").hide();
	}else{
		$("#pub").hide();
	}
(function(){
	
	$('#my_video').bind('click',function() {
		$('#my_video').get(0).play();
	});
	nbOfAdd = 2;
	var myPlayer = document.getElementById("my_video"),
	//myOverlay = document.getElementById("my_video_overlay"),
	adCallModule = new videoplaza.core.AdCallModule("http://be-dramapassion.videoplaza.tv"),
	standarAdsArr = [],
	currentAd,
	originalSrc,
	tracker = new videoplaza.core.Tracker(),
	
	contentMetadata = {
        category : 'site',
        tags     : ['specpost'],
    },
    
    requestSettings = {
        height : myPlayer.height,
        width : myPlayer.width,
        insertionPointType : 'onBeforeContent', //prerolls
        maxBitRate : 800
    };
    //make new ad request
	//adCallModule.requestAds(contentMetadata, requestSettings, onSuccess, onFail);
	
	// get ads array 0~N
	function onSuccess(ads){
	    filterAds(ads);
	}
	
	function onFail(erroMsg) {
	    console.log(erroMsg);
	}
	
	function filterAds(ads) {
	   ads.forEach(function(ad, index, array) {
	       switch (ad.type)
	       {
	       case 'standard_spot':
	           standarAdsArr.push(ad);
	           break;
	       case 'inventory':
	           // track as inventory
	           tracker.track(ad, videoplaza.core.Tracker.trackingEvents.ad.impression);
	           break;
	       default:
	           // track as error, not supported ads
	           tracker.reportError(ad, videoplaza.core.Tracker.errorEvents.creative.invalidCreative);
	           break;
	       }
	   });
	   standarAdsFilter(standarAdsArr);
	}
	function standarAdsFilter(ad) {
		originalSrc = myPlayer.currentSrc;
		adsvideo = [];
		creativeTab = [];
		timer = 0;
		console.log(ad);
	   //sort the Creative(s), it can be of two types: video ads or companion banners
	   for(var k = 0; k < ad.length;k++){
	   for (var i = ad[k].creatives.length - 1; i >= 0; i--) {
	   	  
	      creative = ad[k].creatives[i];
	      creativeTab[k] = ad[k].creatives[i];
		  if(typeof creative.mediaFiles !== "undefined"){
		  for(var j = creative.mediaFiles.length -1; j>=0; j--){
		  	if(creative.mediaFiles[j].mimeType == 'video/mp4'){
		  		if(typeof creative.mediaFiles[j].uri !== "undefined"){
		  			adsvideo[k] = creative.mediaFiles[j].uri ;
		  		}
		  		//myPlayer.src = creative.mediaFiles[j].uri;
		  	}
		  }
		  }
	   }
	   }
	   console.log(adsvideo);
	   console.log(creativeTab);
	   if(adsvideo.length > 0){
	   	   if(adsvideo[1] !== "undefined"){
		   	   onEnded3();
		   	   console.log('2 pub');
	   	   }else{
		   	   onEnded2();
		   	   console.log('1 pub');
	   	   }
		   
	   }else{
		   pubDrama();
		   //onEnded();
		   
	   }
	   
	    
	   
	}

	myPlayer.addEventListener('play', onPlay);
	
	// #1 when the user click play, we should interrupt the playback and make a new ad request
	function onPlay(){
		//$("#cont_my_video").css({ 'background-color': 'black' });
		//$("#cont_my_video").css('background-image', 'none');
		myPlayer.removeEventListener('play', onPlay);
		myPlayer.addEventListener('play', onAdPlay);
		myPlayer.addEventListener('error', onError);
		myPlayer.pause();
		adCallModule.requestAds(contentMetadata, requestSettings, onSuccess, onFail);
	}
	
	function onEnded() {
		myPlayer.removeEventListener('ended', onEnded);
		myPlayer.src = originalSrc;
	
		myPlayer.addEventListener('loadstart', function playerPlay() {
			this.removeEventListener('loadstart', playerPlay);
			myPlayer.controls = "controls";
			myPlayer.play();
		}, false);
		//track completed
        //tracker.track(currentAd, videoplaza.core.Tracker.trackingEvents.creative.complete);
	}
	function pubDrama() { 
		
		/*
		myPlayer.addEventListener('ended', onEnded);
		myPlayer.src = "http://r6---sn-u2oxu-f5fs.c.2mdn.net/videoplayback/id/efb6f6ad99de353e/itag/18/source/doubleclick_dmm/ratebypass/yes/ip/37.187.49.197/ipbits/0/expire/3570956340/sparams/expire,id,ip,ipbits,itag,mm,ms,mv,pl,ratebypass,source/signature/135798D4EC8CE4A9BBA84A575886CD8966F8B1EF.76B2BABC5B9812F420F230E23BE6DEEE93B99F15/key/cms1/cms_redirect/yes/mm/28/ms/nvh/mt/1426692200/mv/u/pl/24/file/file.mp4";
		myPlayer.addEventListener('loadstart', function playerPlay() {
		         this.removeEventListener('loadstart', playerPlay);
		         myPlayer.removeAttribute("controls");
		         myPlayer.play();
		         //TimerOverlay(timer);
		      }, false);
		*/

	}
	function onEnded2() { 
		console.log('affichage pub 0');
		console.log(creativeTab[0].clickThroughUri);
		if(creativeTab[0].clickThroughUri !== "undefined"){
			click = 0;
			$('#my_video').attr('href', creativeTab[0].clickThroughUri);
			$('#my_video').click(function(){
				
				if(click == 0){
			    	window.open(creativeTab[0].clickThroughUri);
			    	click = 1; 
			    }else if(click == 1){
				    myPlayer.play();
			    }
			});
			
		}
		myPlayer.removeEventListener('ended', onEnded2);
		myPlayer.addEventListener('ended', onEnded);
		myPlayer.src = adsvideo[0]+"<?php echo "?".time() ;?>";
	
		
		myPlayer.addEventListener('loadstart', function playerPlay() {
		         this.removeEventListener('loadstart', playerPlay);
		         myPlayer.removeAttribute("controls");
		         myPlayer.play();
		         //TimerOverlay(timer);
		      }, false);
		//track completed
        tracker.track(creativeTab[0], videoplaza.core.Tracker.trackingEvents.creative.complete);
	}
	function onEnded3() {
		console.log('affichage pub 1');
		console.log(creativeTab[1].clickThroughUri);
		if(creativeTab[1].clickThroughUri !== "undefined"){
			click = 0;
			$('#my_video').attr('href', creativeTab[1].clickThroughUri);
			$('#my_video').click(function(){
				
			    if(click == 0){
			    	window.open(creativeTab[1].clickThroughUri);
			    	click = 1;
			    }else if(click == 1){
				    myPlayer.play();
			    }
			});
		}
		myPlayer.removeEventListener('ended', onEnded3);
		myPlayer.addEventListener('ended', onEnded2);
		myPlayer.src = adsvideo[1]+"<?php echo "?".time() ;?>";
	
		myPlayer.addEventListener('loadstart', function playerPlay() {
		         this.removeEventListener('loadstart', playerPlay);
		         myPlayer.removeAttribute("controls");
		         myPlayer.play();
		         //TimerOverlay(timer);
		      }, false);
		//track completed
        tracker.track(creativeTab[1], videoplaza.core.Tracker.trackingEvents.creative.complete);
	}
	function onAdPlay() {
		myPlayer.removeEventListener('play', onAdPlay);
	
	        //track impression and ad started
	        //tracker.track(standarAdsArr[0], videoplaza.core.Tracker.trackingEvents.ad.impression);
	        //tracker.track(currentAd, videoplaza.core.Tracker.trackingEvents.creative.start);
	}
	function onError() {
		myPlayer.removeEventListener('error', onError);
	
		//track error
	    //tracker.reportError(currentAd, videoplaza.core.Tracker.errorEvents.creative.invalidCreative);
	}
	/*
	function TimerOverlay(time){
		myOverlay.style.display = "block";
		var myTime = setInterval(function(){
			myOverlay.innerHTML = "Publicité : 1/1 : Votre vidéo commence dans "+time+" secondes";
			if(time == 0){
				myOverlay.style.display = "none";
				clearInterval(myTime);
			}
			time = time - 1;
		},1000);
	}
	*/
		
	
})();
</script>
</body>
</html>