/* 
* !html5-sdk v2.0.15.19.0 | client-side HTML5 SDK 2.x
* Copyright (c) 2015 by Ooyala, www.ooyala.com 
* email: info@ooyala.com 
*/

!function(window,a){"use strict";var b={adrequest:{},adresponse:{},tracking:{}};!function(a){a.adresponse.Session=function(){return this instanceof a.adresponse.Session?(this.id="",this.language="",this.trackingEvents={},void(this.insertionPoints=[])):new a.adresponse.Session}}(b),function(a){a.adresponse.InsertionPoint=function(){return this instanceof a.adresponse.InsertionPoint?(this.parentSession=null,this.conditions=[],void(this.slots=[])):new a.adresponse.InsertionPoint},a.adresponse.InsertionPoint.isReady=function(a){for(var b,c=0;c<a.slots.length;++c){b=a.slots[c];for(var d=0;d<b.ads.length;++d)if(!b.ads[d].ready)return!1}return!0},a.adresponse.InsertionPoint.maximumPreparationTime=function(a){for(var b,c,d=0,e=0;e<a.slots.length;++e){b=a.slots[e];for(var f=0;f<b.ads.length;++f)c=b.ads[f],d+=c.maximumPreparationTime}return d}}(b),function(a){a.adresponse.Slot=function(){return this instanceof a.adresponse.Slot?(this.parentInsertionPoint=null,this.trackingEvents={},void(this.ads=[])):new a.adresponse.Slot}}(b),function(b){b.adresponse.EventCondition=function(){return this instanceof b.adresponse.EventCondition?(this.type=a,void(this.conditions=[])):new b.adresponse.EventCondition},b.adresponse.EventCondition.ConditionName={ON_BEFORE_CONTENT:"onBeforeContent",ON_CONTENT_END:"onContentEnd",ON_PAUSE:"onPause"}}(b),function(b){b.adresponse.PropertyCondition=function(){return this instanceof b.adresponse.PropertyCondition?(this.type=a,this.operator=a,this.value=a,void(this.conditions=[])):new b.adresponse.PropertyCondition},b.adresponse.PropertyCondition.Operator={EQ:"eq",GEQ:"geq"},b.adresponse.PropertyCondition.ConditionName={PLAYBACK_POSITION:"playbackPosition",TIME_SINCE_LINEAR:"timeSinceLinear",PLAYBACK_TIME:"playbackTime"}}(b),function(a){a.adresponse.Companion=function(){return this instanceof a.adresponse.Companion?(this.id="",this.customId="",this.parentAd=null,this.sequence=0,this.width=0,this.height=0,this.trackingEvents={},this.resources=[],this.required=a.adresponse.Companion.RequiredRule.NO,void(this.zone="")):new a.adresponse.Companion},a.adresponse.Companion.RequiredRule={YES:"yes",AT_LEAST_ONE_FOR_THIS_SEQUENCE:"atLeastOne",NO:"no"}}(b),function(b){b.adresponse.Ad=function(){return this instanceof b.adresponse.Ad?(this.id="",this.customId="",this.goalId="",this.customGoalId="",this.campaignId="",this.customCampaignId="",this.parentSlot=null,this.allowLinearityToChange=!1,this.partOfAnExclusiveCampaign=!1,this.showCountdown=!1,this.type=a,this.variant=a,this.maximumPreparationTime=0,this.startTimeout=0,this.thirdPartyURL="",this.thirdPartyChain=[],this.trackingEvents={},this.creatives=[],this.companions=[],this.ads=[],this.ready=!1,void(this.labels={})):new b.adresponse.Ad},b.adresponse.Ad.hasPassback=function(a){return a.type!==b.adresponse.Ad.AdType.SPOT_SELECTOR&&a.ads.length>0},b.adresponse.Ad.AdType={INVENTORY:"inventory",SPOT_STANDARD:"spot_standard",SPOT_INTERACTIVE:"spot_interactive",SPOT_SELECTOR:"spot_selector",SPOT_TAKEOVER:"spot_takeover",OVERLAY_STANDARD:"overlay_standard",OVERLAY_VIDEO:"overlay_video",OVERLAY_IMAGESET:"overlay_imageset",OVERLAY_SPLASH:"overlay_splash",SPLASH_STANDARD:"splash_standard",SKIN_INSKIN:"skin_inskin"},b.adresponse.Ad.Variant={NORMAL:"normal",SPONSOR:"sponsor"}}(b),function(b){b.adresponse.LinearCreative=function(){var c=function(){return this instanceof b.adresponse.LinearCreative?(this.parentAd=null,this.clickThroughUrl="",this.duration=a,this.sequence=0,this.type="",this.trackingEvents={},this.skipOffset=a,this.skipButtonMode=a,this.lastCompletion=a,this.skipResetTime=a,void(this.mediaFiles=[])):new b.adresponse.LinearCreative};return c}(),b.adresponse.LinearCreative.SkipButtonMode={ALWAYS:"always",NEVER:"never",AFTER_FIRST_COMPLETION:"after_first_completion"}}(b),function(b){b.adresponse.NonLinearCreative=function(){var c=function(){return this instanceof b.adresponse.NonLinearCreative?(this.parentAd=null,this.duration=a,this.sequence=0,this.width=0,this.height=0,this.type="",this.trackingEvents={},void(this.resources=[])):new b.adresponse.NonLinearCreative};return c}()}(b),function(b){b.adresponse.MediaFile=function(){return this instanceof b.adresponse.MediaFile?(this.apiFramework=a,this.adParameters=a,this.bitRate=0,this.deliveryMethod=a,this.height=0,this.mimeType="",this.width=0,void(this.url="")):new b.adresponse.MediaFile},b.adresponse.MediaFile.DeliveryMethod={PROGRESSIVE:"progressive",STREAMING:"streaming"},b.adresponse.MediaFile.ApiFramework={VPAID:"VPAID"}}(b),function(a){a.adresponse.HtmlResource=function(){this.source=""}}(b),function(a){a.adresponse.IFrameResource=function(){this.url=""}}(b),function(b){b.adresponse.StaticResource=function(){this.adParameters=a,this.adParametersXmlEncoded=a,this.apiFramework=a,this.assetWidth=a,this.assetHeight=a,this.expandedWidth=a,this.expandedHeight=a,this.clickThroughUrl=a,this.mimeType=a,this.url=""},b.adresponse.StaticResource.ApiFramework={VPAID:"VPAID",CLICK_TAG:"clickTAG"}}(b),function(a){a.LogItem=function(){var b=function(c){return this instanceof a.LogItem?(this.source="",this.event=a.LogItem.EventType.GENERAL_ERROR,this.message="",void(this.thirdPartySourceURLs=[])):new b};return b}(),a.LogItem.SourceType={AD:"ad",SESSION:"session",TRACKER:"tracker"},a.LogItem.EventType={NO_AD_RESPONSE:"noAd",INVALID_ARGUMENT:"invalidArgument",INVALID_RESPONSE:"invalidResponse",GENERAL_ERROR:"generalError",REQUEST_FAILED:"requestFailed",REQUEST_TIMEOUT:"requestTimeout",REQUEST_CANCELED:"requestCanceled",WARNING:"warning",ILLEGAL_OPERATION:"illegalOperation"}}(b),function(a){a.HTTPRequester=function(){var b=function(){return this instanceof a.HTTPRequester?void 0:new b},c={503:"serviceUnavailableCode503",500:"internalServerErrorCode500",404:"requestedResourceNotFoundCode404",400:"badRequestToServerCode400",0:"requestCouldNotBeSentToServerCode0",ieError:"requestFailedError"};return b.prototype.request=function(a,b,d,e){var f,g,h="GET";if(e=e||12e3,b||(b=function(){}),d||(d=function(){}),window.XMLHttpRequest)if(f=new window.XMLHttpRequest,"withCredentials"in f){try{f.withCredentials=!0}catch(i){}try{f.open(h,a,!0),f.timeout=e,g=!1}catch(i){return void d(c[0])}f.onreadystatechange=function(){4===f.readyState&&(200===f.status?b(f.responseText):g||d(c[f.status]||"unknownError"))},f.send()}else if(window.XDomainRequest){f=new window.XDomainRequest,f.onload=function(){b(f.responseText)},f.onerror=function(){d(c.ieError||"unknownError")},f.onprogress=function(){},f.ontimeout=function(){};try{f.open(h,a),f.timeout=e,f.send()}catch(i){d(c.ieError||"unknownError")}}else d("corsNotSupportedInBrowser");var j=function(a){g=!0,f.abort(),f=null,b=function(){},d(a)};return j},b.prototype.postRequest=function(a,b,d,e,f){var g,h,i="POST";f=f||12e3,d||(d=function(){}),e||(e=function(){});var j=[];for(var k in b)j.push(k+"="+b[k]);if(j=j.join("&"),window.XMLHttpRequest)if(g=new window.XMLHttpRequest,"withCredentials"in g){try{g.withCredentials=!0}catch(l){}try{g.open(i,a,!0),g.timeout=f,h=!1}catch(l){return void e(c[0])}g.onreadystatechange=function(){4===g.readyState&&(200===g.status?d(g.responseText):h||e(c[g.status]||"unknownError"))},g.send(j)}else if(window.XDomainRequest){g=new window.XDomainRequest,g.onload=function(){d(g.responseText)},g.onerror=function(){e(c.ieError||"unknownError")},g.onprogress=function(){},g.ontimeout=function(){};try{g.open(i,a),g.timeout=f,g.send(j)}catch(l){e(c.ieError||"unknownError")}}else e("corsNotSupportedInBrowser");var m=function(a){h=!0,g.abort(),g=null,d=function(){},e(a)};return m},b}()}(b),function(b){b.adrequest.AdRequester=function(){function d(a){return/(^(http|https)\:\/\/)?([a-z0-9\-]+)\.([a-z0-9\-]+)\.[a-z]+/i.test(a)}function e(a){switch(a){case"longForm":return"long_form";case"shortForm":return"short_form"}}function f(a){switch(a){case b.adrequest.AdRequester.InsertionPointType.ON_BEFORE_CONTENT:return"p";case b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION:return"m";case b.adrequest.AdRequester.InsertionPointType.ON_CONTENT_END:return"po";case b.adrequest.AdRequester.InsertionPointType.ON_PAUSE:return"pa";case b.adrequest.AdRequester.InsertionPointType.PLAYBACK_TIME:return"o";default:throw new Error("InvalidInsertionPointType passed")}}function g(a){var b="",c="[\\?&]"+a+"=([^&#]*)",d=new RegExp(c),e=d.exec(window.location.href.toString());return null!==e&&(b=e[1]),b}function h(c,d){if(c===a||null===c)return!1;if(!c.hasOwnProperty("linearPlaybackPositions")||"[object Array]"!==Object.prototype.toString.call(c.linearPlaybackPositions)||0===c.linearPlaybackPositions.length)return!1;if(!c.hasOwnProperty("nonlinearPlaybackPositions")||"[object Array]"!==Object.prototype.toString.call(c.nonlinearPlaybackPositions)||0===c.nonlinearPlaybackPositions.length)return!1;for(var e=0;e<c.linearPlaybackPositions.length;++e)if(-1!==c.nonlinearPlaybackPositions.indexOf(c.linearPlaybackPositions[e])){var f=new b.LogItem;return f.source=b.LogItem.SourceType.SESSION,f.event=b.LogItem.EventType.ILLEGAL_OPERATION,f.message="Both linear and nonlinear ads requested for playback position "+c.linearPlaybackPositions[e]+", but only one type per position is allowed; request canceled",i(d,f),!0}return!1}function i(a,b){for(var c=0;c<a._logCallbacks.length;++c)a._logCallbacks[c](b)}function j(a,b,c,d,e){var f={operation:a,list:b,context:c,step:function(){0===f.list.length?f.finish():f.operation(f.list.shift(),f.context,f.step,f.finish,f.cancel)},finish:d,cancel:e};f.step()}function k(a,c,d,e){if(c.cancelled!==!0&&c.adContext.cancelled!==!0&&c.adContext.sessionContext.sessionCancelled!==!0){var f=function(f){if(c.cancelled!==!0&&c.adContext.cancelled!==!0&&c.adContext.sessionContext.sessionCancelled!==!0){var g,h,j=new b.LogItem;try{g=c.adContext.sessionContext.vastParser.parse(f)}catch(k){return h=k.hasOwnProperty("errorCode")?k.errorCode:"100",j.message="Could not parse third party ad: "+k.message,j.event=b.LogItem.EventType.INVALID_RESPONSE,j.thirdPartySourceURLs=a.thirdPartyChain.toString(),j.source=b.LogItem.SourceType.AD,i(c.adContext.sessionContext.target,j),e(a,h),void(c.cancelled=!0)}g?("inventory"===g.type?(v(g,a),j.message="No ads VAST response.",j.event=b.LogItem.EventType.NO_AD_RESPONSE,j.thirdPartySourceURLs=g.thirdPartyChain.toString(),j.source=b.LogItem.SourceType.AD,i(c.adContext.sessionContext.target,j),e(g,"303"),c.cancelled=!0):(v(g,a),setTimeout(function(){d(g)},0)),c.currentNode=g,c.adContext.currentChain=g):(setTimeout(function(){e(a,"900")},0),c.cancelled=!0)}},g=new b.HTTPRequester;a.thirdPartyChain.push(a.thirdPartyURL),g.request(a.thirdPartyURL,f,function(d){if(c.cancelled!==!0&&c.adContext.cancelled!==!0&&c.adContext.sessionContext.sessionCancelled!==!0){var f=new b.LogItem;f.message="Could not retrieve third party ad: "+d,f.event=b.LogItem.EventType.REQUEST_FAILED,f.thirdPartySourceURLs=a.thirdPartyChain.toString(),f.source=b.LogItem.SourceType.AD,i(c.adContext.sessionContext.target,f),e(a,"300"),c.cancelled=!0}})}}function l(a,b,c,d){var e=function(a){l(a,b,c,d)};a.thirdPartyURL.length>0?k(a,b,e,d):(b.completed=!0,c(a))}function m(c,d,e,f){var g,h={adContext:d,cancelled:!1,completed:!1,currentNode:c},j=Date.now(),m=function(){if(h.completed!==!0&&h.cancelled!==!0&&h.adContext.cancelled!==!0&&h.adContext.sessionContext.sessionCancelled!==!0){h.cancelled=!0;var a=(Date.now()-j)/1e3,e=new b.LogItem;e.message="Could not retrieve third party ad: chain timed out after "+a+"s",e.event=b.LogItem.EventType.REQUEST_TIMEOUT,e.thirdPartySourceURLs=c.thirdPartyChain.toString(),e.source=b.LogItem.SourceType.AD,i(d.sessionContext.target,e);for(var g=0;g<h.currentNode.creatives.length;++g)h.currentNode.creatives[g].trackingEvents={};f(h.currentNode,"301")}},n=function(a){clearTimeout(g),e(a)},o=function(a){l(a,h,n,f)};c.thirdPartyURL.length>0?(c.maximumPreparationTime!==a&&(g=setTimeout(m,1e3*c.maximumPreparationTime)),k(c,h,o,f)):(h.completed=!0,e(c))}function n(c,d,e,f,g){var h,j,k,l={sessionContext:d,cancelled:!1,completed:!1,currentChain:null},n=function(a,f,h){if(l.completed!==!0&&l.cancelled!==!0&&d.sessionCancelled!==!0){var j=new b.LogItem;if(h||(j.message=f,"301"===a?j.event=b.LogItem.EventType.REQUEST_TIMEOUT:j.event=b.LogItem.EventType.REQUEST_CANCELED,j.thirdPartySourceURLs=c.thirdPartyChain.toString(),j.source=b.LogItem.SourceType.AD,i(d.target,j)),c.ready=!0,c.ads.length>=2){var k=c.ads[c.ads.length-1];c.ads=[],null!==l.currentChain&&x(l.currentChain,c,"error"),z(c,a);for(var m=0;m<c.creatives.length;++m)c.creatives[m].trackingEvents.urls=[];v(c,k,!1,!0),s(c,k),"inventory"===k.type&&(c.companions=[]),e()}else g("No passback candidates remaining");l.cancelled=!0}},o=function(a,b){n("900",a,b)},p=function(){var a=(Date.now()-j)/1e3;n("301","Could not retrieve third party ad: passback timed out after "+a+"s")},q=function(a){clearTimeout(k),l.completed=!0,v(c,a,!1,!0),s(c,h),c.ads.shift(),c.ready=!0,"inventory"===c.type&&(c.companions=[]),e()},r=function(a,b){c.ads.shift();for(var d=0;d<a.creatives.length;++d)a.creatives[d].trackingEvents={};a.thirdPartyChain.length=0,a.trackingEvents.hasOwnProperty("impression")&&(a.trackingEvents.impression.urls.length=0),c.ads.length>0?(v(c.ads[0],a,!0),z(c.ads[0],b),h=c.ads[0],m(h,l,q,r)):g("No passback candidates remaining")};return c.ready&&d.skipLazyAds===!1||!c.ready&&d.skipLazyAds===!0?e():b.adresponse.Ad.hasPassback(c)?(c.maximumPreparationTime!==a&&(j=Date.now(),k=setTimeout(p,1e3*c.maximumPreparationTime)),l.currentChain=c.ads[0],h=c.ads[0],m(h,l,q,r)):e(),o}function o(a,b,c,d,e){var f=a.ads.slice(0);j(n,f,b,c,e)}function p(a,b,c,d,e){var f=a.slots.slice(0);j(o,f,b,c,e)}function q(a,c,d,e,f){var g=new b.adrequest.AdResponseParser,h={target:a,vastParser:g,sessionCancelled:!1,skipLazyAds:f},i=function(){h.sessionCancelled===!1&&d(c)},k=function(){h.sessionCancelled===!1&&e(c)},l=c.insertionPoints.slice(0);j(p,l,h,i,k);var m=function(error){h.sessionCancelled===!1&&(h.sessionCancelled=!0,e(error))};return m}function r(a,d,e){for(var f,g,h,j,k=0,l=[],m=[],n=[],o=0;o<d.insertionPoints.length;++o)g=d.insertionPoints[o],g.conditions[0].type===b.adresponse.PropertyCondition.PLAYBACK_POSITION&&l.push(g.conditions[0].value);for(var p=0;p<a.insertionPoints.length;++p){if(h=a.insertionPoints[p],j=!1,h.conditions[0].type===b.adresponse.PropertyCondition.ConditionName.PLAYBACK_POSITION)++k,-1!==l.indexOf(h.conditions[0].value)&&(j=!0,m.push(h.conditions[0].value));else for(var o=0;o<d.insertionPoints.length;++o)if(g=d.insertionPoints[o],g.conditions[0].type===h.conditions[0].type){if(h.conditions[0].type===b.adresponse.EventCondition.ConditionName.ON_PAUSE){d.insertionPoints.splice(o,1);break}j=!0,n.push(h.conditions[0].type);break}j||d.insertionPoints.push(h)}if(k>0&&k===m.length&&n.push(b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION),n.length>0){for(var p=0;p<n.length;++p)c[n[p]]&&(n[p]=c[n[p]]);var q=n.join(", ");f=new b.LogItem,f.source=b.LogItem.SourceType.SESSION,f.event=b.LogItem.EventType.WARNING,f.message="Some insertion point types were received, but not merged, as they already exist: "+q,i(e,f)}if(m.length>0){var q=m.join(", ");f=new b.LogItem,f.source=b.LogItem.SourceType.SESSION,f.event=b.LogItem.EventType.WARNING,f.message="Some playback positions were received, but not merged, as they already exist: "+q,i(e,f)}}function s(a,b){a.id=b.id,a.type=b.type,a.customId=b.customId,a.campaignId=b.campaignId,a.customCampaignId=b.customCampaignId,a.goalId=b.goalId,a.customGoalId=b.customGoalId,a.allowLinearityToChange=b.allowLinearityToChange,a.partOfAnExclusiveCampaign=b.partOfAnExclusiveCampaign,a.showCountdown=b.showCountdown;for(var c=0;c<b.creatives.length;++c)t(a.creatives[c],b.creatives[c])}function t(a,b){a.type=b.type,a.sequence=b.sequence,a.skipButtonMode=b.skipButtonMode,a.lastCompletion=b.lastCompletion,a.skipResetTime=b.skipResetTime}function u(a,b){a.duration=b.duration,a.clickThroughUrl=b.clickThroughUrl,a.skipOffset=b.skipOffset}function v(a,b,c,d){if(a!==b){for(var e=0;e<b.thirdPartyChain.length;++e)a.thirdPartyChain.push(b.thirdPartyChain[e]);if(c?(w(b,"error"),w(b,"impression")):(w(a,"error"),w(a,"impression")),x(b,a),"inventory"!==a.type){for(var f=0;f<b.creatives.length;++f){var g=a.creatives[f],h=b.creatives[f];x(h,g);for(var i=0;i<h.mediaFiles.length;++i)g.mediaFiles.push(y(h.mediaFiles[i]));d&&u(g,h)}for(var i=0;i<b.companions.length;++i)b.companions[i].targetAd=a,a.companions.push(b.companions[i])}}}function w(a,b){if(a.trackingEvents.hasOwnProperty(b)){for(var c,d=[],e=0;e<a.trackingEvents[b].urls.length;++e)c=a.trackingEvents[b].urls[e],c.thirdParty&&d.push(c);a.trackingEvents[b].urls=d}}function x(a,b,c){for(var d,e=Object.keys(a.trackingEvents),f=0;f<e.length;++f)if(d=e[f],(!c||d===c)&&a.trackingEvents.hasOwnProperty(d)){b.trackingEvents.hasOwnProperty(d)||(b.trackingEvents[d]={urls:[],blocked:!1});for(var g=0;g<a.trackingEvents[d].urls.length;++g)b.trackingEvents[d].urls.push(a.trackingEvents[d].urls[g])}}function y(a){var c=new b.adresponse.MediaFile;return c.apiFramework=a.apiFramework,c.bitRate=a.bitRate,c.deliveryMethod=a.deliveryMethod,c.height=a.height,c.id=a.id,c.mimeType=a.mimeType,c.url=a.url,c.width=a.width,c}function z(a,b){a.trackingEvents.hasOwnProperty("error")||(a.trackingEvents.error={urls:[],blocked:!1});for(var c=a.trackingEvents.error.urls,d=0;d<c.length;++d)c[d].thirdParty&&(b&&(c[d].url=c[d].url.replace(E,encodeURIComponent(b))),a.trackingEvents.hasOwnProperty("impression")||(a.trackingEvents.impression={urls:[],blocked:!1}),a.trackingEvents.impression.urls.push({url:c[d].url,thirdParty:!0}))}function A(a,c,d,h,j){try{B(d),C(h)}catch(k){throw k}var l=a;if(-1===l.indexOf("http://")&&-1===l.indexOf("https://")&&(l="http://"+l),l.lastIndexOf("/")!==l.length-1&&(l+="/"),l+="proxy/distributor/v2?",l+="rt=vp_3.0",l+="&pf=html5",l+="&cv=h5_"+window.videoplaza.versionNumber,d){d.hasOwnProperty("flags")&&d.flags.length>0&&(l+="&f="+encodeURIComponent(d.flags.join(","))),d.hasOwnProperty("tags")&&d.tags.length>0&&(l+="&t="+encodeURIComponent(d.tags.join(",")));var m="";if(d.hasOwnProperty("category")&&d.category.length>0&&(m+=d.category),d.hasOwnProperty("contentPartner")&&d.contentPartner&&(m=m?m+=","+d.contentPartner:d.contentPartner),m&&(l+="&s="+encodeURIComponent(m)),d.hasOwnProperty("contentForm")&&d.contentForm&&(l+="&cf="+e(d.contentForm)),d.hasOwnProperty("id")&&d.id&&(l+="&ci="+encodeURIComponent(d.id)),d.hasOwnProperty("duration")&&d.duration&&(l+="&cd="+d.duration),d.hasOwnProperty("customParameters")&&Object.keys(d.customParameters).length>0){var n=/[^A-Za-z0-9_~\-.]/;for(var o in d.customParameters)d.customParameters.hasOwnProperty(o)&&!n.test(o)&&(l+="&cp."+o+"="+encodeURIComponent(d.customParameters[o]))}}if(h){h.hasOwnProperty("width")&&h.width&&(l+="&vwt="+h.width),h.hasOwnProperty("height")&&h.height&&(l+="&vht="+h.height),h.hasOwnProperty("maxBitRate")&&h.maxBitRate&&(l+="&vbw="+h.maxBitRate),h.hasOwnProperty("linearSlotSize")&&h.linearSlotSize&&(l+="&pp="+h.linearSlotSize,l+="&mp="+h.linearSlotSize,l+="&pop="+h.linearSlotSize);var p=!1,q=!1;if(h.hasOwnProperty("insertionPointFilter")&&h.insertionPointFilter.length>0){for(var r=[],s=0;s<h.insertionPointFilter.length;s++)try{r.push(f(h.insertionPointFilter[s])),h.insertionPointFilter[s]===b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION?(p=!0,h.hasOwnProperty("linearPlaybackPositions")&&h.linearPlaybackPositions.length>0&&(q=!0),h.hasOwnProperty("nonlinearPlaybackPositions")&&h.nonlinearPlaybackPositions.length>0&&r.push("o")):h.insertionPointFilter[s]===b.adrequest.AdRequester.InsertionPointType.ON_BEFORE_CONTENT&&(q=!0)}catch(k){}q&&r.push("s"),r=r.filter(function(a,b,c){return c.indexOf(a)==b}),l+="&tt="+r.join(",")}else p=!0;if(p&&(h.hasOwnProperty("linearPlaybackPositions")&&h.linearPlaybackPositions.length>0&&(l+="&bp="+encodeURIComponent(h.linearPlaybackPositions.join(","))),h.hasOwnProperty("nonlinearPlaybackPositions")&&h.nonlinearPlaybackPositions.length>0&&(l+="&obp="+encodeURIComponent(h.nonlinearPlaybackPositions.join(",")))),!p&&h.hasOwnProperty("linearPlaybackPositions")&&h.linearPlaybackPositions.length>0){var t=new b.LogItem;t.message="linearPlaybackPositions provided, but insertionPointFilter excludes PLAYBACK_POSITION. No linear insertion points will be requested for the given playback positions.",t.source=b.LogItem.SourceType.SESSION,t.event=b.LogItem.EventType.WARNING,i(j,t)}if(!p&&h.hasOwnProperty("nonlinearPlaybackPositions")&&h.nonlinearPlaybackPositions.length>0){var t=new b.LogItem;t.message="nonlinearPlaybackPositions provided, but insertionPointFilter excludes PLAYBACK_POSITION. No nonlinear insertion points will be requested for the given playback positions.",t.source=b.LogItem.SourceType.SESSION,t.event=b.LogItem.EventType.WARNING,i(j,t)}h.hasOwnProperty("referrerUrl")&&h.referrerUrl&&(l+="&ru="+encodeURIComponent(h.referrerUrl))}c&&c.hasOwnProperty("deviceContainer")&&c.deviceContainer&&(l+="&dcid="+encodeURIComponent(c.deviceContainer)),c&&c.hasOwnProperty("persistentId")&&c.persistentId&&(l+="&pid="+encodeURIComponent(c.persistentId));var u=g("vppreview");return u&&(l+="&vppreview="+u),l+="&xpb=1",l+="&st=20:1",l+="&rnd="+Math.floor(1e16*Math.random())}function B(a){var c={id:"",category:"",contentForm:"",contentPartner:"",duration:0,flags:[],tags:[],customParameters:{}};if(a){if(a.hasOwnProperty("id")){if("string"!=typeof a.id)throw new Error("InvalidType of contentMetadata.id, should be string but is "+typeof a.id);a.id&&(c.id=a.id)}if(a.hasOwnProperty("category")&&a.category){if("string"!=typeof a.category)throw new Error("InvalidType of property contentMetadata.category, should be string but is "+typeof a.category);c.category=a.category}if(a.hasOwnProperty("contentForm")){if("string"!=typeof a.contentForm)throw new Error("InvalidType of property contentMetadata.contentForm, should be string but is "+typeof a.contentForm);if(a.contentForm!==b.adrequest.AdRequester.ContentForm.SHORT_FORM&&a.contentForm!==b.adrequest.AdRequester.ContentForm.LONG_FORM)throw new Error("Invalid contentMetadata.contentForm Value");c.contentForm=a.contentForm}if(a.hasOwnProperty("contentPartner")&&a.contentPartner){if("string"!=typeof a.contentPartner)throw new Error("InvalidType of property contentMetadata.contentPartner, should be string but is "+typeof a.contentPartner);c.contentPartner=a.contentPartner}if(a.hasOwnProperty("duration")){if("number"!=typeof a.duration)throw new Error("InvalidType of property contentMetadata.duration, should be number but is "+typeof a.duration);if(!(a.duration>=0))throw new Error("contentMetadata.duration value must be 0 or greater, but is "+a.duration);c.duration=a.duration}if(a.hasOwnProperty("flags")){if("[object Array]"!==Object.prototype.toString.call(a.flags))throw new Error("InvalidType of contentMetadata.flags, should be Array but is "+Object.prototype.toString.call(a.flags));c.flags=a.flags}if(a.hasOwnProperty("tags")){if("[object Array]"!==Object.prototype.toString.call(a.tags))throw new Error("InvalidType of contentMetadata.tags, should be Array but is "+Object.prototype.toString.call(a.tags));c.tags=a.tags}if(a.hasOwnProperty("customParameters")){if("[object Object]"!==Object.prototype.toString.call(a.customParameters))throw new Error("InvalidType of contentMetadata.customParameters, should be Object but is "+Object.prototype.toString.call(a.customParameters));for(var d in a.customParameters){if(!d)throw new Error("Empty contentMetadata.customParameters key");if("string"!=typeof a.customParameters[d])throw new Error("InvalidType of contentMetadata.customParameters property "+d+". Should be string but is "+typeof a.customParameters[d])}c.customParameters=a.customParameters}}return c}function C(a){var c,d,e={height:0,maxBitRate:0,referrerUrl:"",width:0,linearPlaybackPositions:[],nonlinearPlaybackPositions:[],insertionPointFilter:[]};if(a.hasOwnProperty("height")){if("number"!=typeof a.height)throw new Error("InvalidType of requestSettings.height, should be number but is "+typeof a.height);if(!(a.height>=0))throw new Error("Invalid value of requestSettings.height, must be larger than or equal to 0");e.height=a.height}if(a.hasOwnProperty("width")){if("number"!=typeof a.width)throw new Error("InvalidType of requestSettings.width, should be number but is "+typeof a.width);if(!(a.width>=0))throw new Error("Invalid value of requestSettings.width, must be larger than or equal to 0");e.width=a.width}if(a.hasOwnProperty("maxBitRate")){if("number"!=typeof a.maxBitRate)throw new Error("InvalidType of requestSettings.maxBitRate, should be number but is "+typeof a.maxBitRate);if(!(a.maxBitRate>=0))throw new Error("Invalid value of requestSettings.maxBitRate, must be larger than or equal to 0");e.maxBitRate=a.maxBitRate}if(a.hasOwnProperty("linearSlotSize")){if("number"!=typeof a.linearSlotSize)throw new Error("InvalidType of requestSettings.linearSlotSize, should be number but is "+typeof a.linearSlotSize);if(!(a.linearSlotSize>=0))throw new Error("Invalid value of requestSettings.linearSlotSize, must be larger than or equal to 0");e.linearSlotSize=a.linearSlotSize}if(a.hasOwnProperty("referrerUrl")){if("string"!=typeof a.referrerUrl)throw new Error("InvalidType of requestSettings.referrerUrl, should be string but is "+typeof a.referrerUrl);a.referrerUrl&&(e.referrerUrl=a.referrerUrl)}if(a.hasOwnProperty("linearPlaybackPositions")){if("[object Array]"!==Object.prototype.toString.call(a.linearPlaybackPositions))throw new Error("InvalidType of requestSettings.linearPlaybackPositions, should be [object Array] but is "+Object.prototype.toString.call(a.linearPlaybackPositions));if(a.linearPlaybackPositions.length>0)for(var f=0;f<a.linearPlaybackPositions.length;f++){if(c=a.linearPlaybackPositions[f],"number"!=typeof c||c!==c)throw new Error("InvalidType of requestSettings.linearPlaybackPositions at index "+f+". Should be of type number but is "+typeof a.linearPlaybackPositions[f]);e.linearPlaybackPositions.push(a.linearPlaybackPositions[f])}}if(a.hasOwnProperty("nonlinearPlaybackPositions")){if("[object Array]"!==Object.prototype.toString.call(a.nonlinearPlaybackPositions))throw new Error("InvalidType of requestSettings.nonlinearPlaybackPositions, should be [object Array] but is "+Object.prototype.toString.call(a.nonlinearPlaybackPositions));if(a.nonlinearPlaybackPositions.length>0)for(var g=0;g<a.nonlinearPlaybackPositions.length;g++){if(d=a.nonlinearPlaybackPositions[g],"number"!=typeof d||d!==d)throw new Error("InvalidType of requestSettings.nonlinearPlaybackPositions at index "+g+". Should be of type number but is "+typeof a.nonlinearPlaybackPositions[g]);e.nonlinearPlaybackPositions.push(a.nonlinearPlaybackPositions[g])}}if(a.hasOwnProperty("insertionPointFilter")){if("[object Array]"!==Object.prototype.toString.call(a.insertionPointFilter))throw new Error("InvalidType of requestSettings.insertionPointFilter, should be [object Array] but is "+Object.prototype.toString.call(a.insertionPointFilter));if(a.insertionPointFilter.length>0)for(var h=0;h<a.insertionPointFilter.length;h++){if("string"!=typeof a.insertionPointFilter[h])throw new Error("InvalidType of requestSettings.insertionPointFilter at index "+h+". Should be of type string but is "+typeof a.insertionPointFilter[h]);if(a.insertionPointFilter[h]!==b.adrequest.AdRequester.InsertionPointType.ON_BEFORE_CONTENT&&a.insertionPointFilter[h]!==b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION&&a.insertionPointFilter[h]!==b.adrequest.AdRequester.InsertionPointType.ON_CONTENT_END&&a.insertionPointFilter[h]!==b.adrequest.AdRequester.InsertionPointType.ON_PAUSE&&a.insertionPointFilter[h]!==b.adrequest.AdRequester.InsertionPointType.PLAYBACK_TIME)throw new Error("Invalid Value of requestSettings.insertionPointFilter at index "+h+". Accepted values are onBeforeContent, playbackPosition, onContentEnd, onPause or playbackTime, but is "+a.insertionPointFilter[h]);e.insertionPointFilter.push(a.insertionPointFilter[h])}}return e}var D=function(e,f){if(!(this instanceof b.adrequest.AdRequester))return new D(e,f);if(!d(e)){var g=new Error("Argument vpHost invalid or missing");throw g.name="Invalid_Arguments",g}if(!(f&&f instanceof Object))throw new Error("Argument adRequesterSettings invalid or missing");this._logCallbacks=[],f=JSON.parse(JSON.stringify(f));var j=this,k=function(a,c,d,g,k){if(!d){var l=new b.LogItem;return l.message="Required parameter missing: 'onSuccess' callback",l.event=b.LogItem.EventType.INVALID_ARGUMENT,l.source=b.LogItem.SourceType.SESSION,i(this,l),function(){}}if(!g){var l=new b.LogItem;return l.message="Required parameter missing: 'onFail' callback",l.event=b.LogItem.EventType.INVALID_ARGUMENT,l.source=b.LogItem.SourceType.SESSION,i(this,l),function(){}}var m=!1,n=new b.HTTPRequester,o=function(){},p=function(){},r=!1,s=function(a){k&&delete k._partOfOngoingRequest;var c=new b.LogItem;c.source=b.LogItem.SourceType.SESSION,m?(c.message="Session request canceled: "+a,c.event=b.LogItem.EventType.REQUEST_CANCELED):(c.message="Session request failed: "+a+" URL: "+v,c.event=b.LogItem.EventType.REQUEST_FAILED),i(j,c),g(k?k:a)},t=function(a){k&&delete k._partOfOngoingRequest,r=!0;var c,e=new b.adrequest.AdResponseParser;try{c=e.parse(a)}catch(f){var h=new b.LogItem;return h.message=f.message,h.event=b.LogItem.EventType.INVALID_RESPONSE,h.source=b.LogItem.SourceType.SESSION,i(j,h),void g(f.message)}o=q(j,c,d,s,!0)},u=function(error){m=!0,r===!1?p(error):o(error)};if(h(c,this))return setTimeout(function(){g(k?k:"Incompatible request settings")},0),function(){};try{var v=A(e,f,a,c,this);k&&(v+="&tid="+encodeURIComponent(k.id),k._partOfOngoingRequest=!0),p=n.request(v,t,s)}catch(w){var l=new b.LogItem;return l.message=w.message,l.event=b.LogItem.EventType.INVALID_ARGUMENT,l.source=b.LogItem.SourceType.SESSION,i(this,l),g(w.message),void(k&&delete k._partOfOngoingRequest)}return u};this.requestSession=function(a,b,c,d){return k.call(this,a,b,c,d)},this.requestSessionExtension=function(d,e,f,g){var l;if(null===d||d===a||!(d instanceof b.adresponse.Session))return l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.INVALID_ARGUMENT,d?l.message="Invalid type: only Session accepted for argument 'session'":l.message="Required parameter missing: 'session'",i(this,l),function(){};if(d._partOfOngoingRequest)return l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.ILLEGAL_OPERATION,l.message="The supplied session is already waiting for a request response",i(this,l),function(){};if(null===f||f===a)return l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.INVALID_ARGUMENT,l.message="Required parameter missing: 'requestSettings'",i(this,l),function(){};if(null===g||g===a)return l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.INVALID_ARGUMENT,l.message="Required parameter missing: 'onComplete' callback",i(this,l),function(){};var m=[],n=[],o=[],p=[];if(!f.hasOwnProperty("insertionPointFilter")||f.hasOwnProperty("insertionPointFilter")&&0===f.insertionPointFilter.length){for(var q in b.adrequest.AdRequester.InsertionPointType)m.push(b.adrequest.AdRequester.InsertionPointType[q]);n.push(b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION)}else{m=m.concat(f.insertionPointFilter);for(var s=0;s<m.length;++s)if(m[s]===b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION){n.push(b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION);break}}var t,u=[],v=[];if(f.linearPlaybackPositions)for(var w=0;w<f.linearPlaybackPositions.length;++w){t=!0;for(var s=0;s<d.insertionPoints.length;++s)if(B=d.insertionPoints[s],B.conditions[0].type===b.adresponse.PropertyCondition.ConditionName.PLAYBACK_POSITION&&B.conditions[0].value===f.linearPlaybackPositions[w]){t=!1,v.push(f.linearPlaybackPositions[w]);break}t&&o.push(f.linearPlaybackPositions[w])}if(f.nonlinearPlaybackPositions)for(var w=0;w<f.nonlinearPlaybackPositions.length;++w){t=!0;for(var s=0;s<d.insertionPoints.length;++s)if(B=d.insertionPoints[s],
B.conditions[0].type===b.adresponse.PropertyCondition.ConditionName.PLAYBACK_POSITION&&B.conditions[0].value===f.nonlinearPlaybackPositions[w]){t=!1,v.push(f.nonlinearPlaybackPositions[w]);break}t&&p.push(f.nonlinearPlaybackPositions[w])}if(h(f,this))return setTimeout(function(){g(d)},0),function(){};for(var x=0;x<m.length;++x){var y=m[x],z=!0;if(y!==b.adrequest.AdRequester.InsertionPointType.ON_PAUSE){for(var A=0;A<d.insertionPoints.length;A++){var B=d.insertionPoints[A];if(B.conditions[0].type===y&&y!==b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION){u.push(y),z=!1;break}}z&&y!==b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION&&n.push(y)}else n.push(y)}if(u.length>0){for(var s=0;s<u.length;++s)c[u[s]]&&(u[s]=c[u[s]]);var C=u.join(", ");l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.WARNING,l.message="Some insertion point types were excluded from the request, as they already exist: "+C,i(this,l)}if(v.length>0){var C=v.join(", ");l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.WARNING,l.message="Some playback positions were excluded from the request, as they already exist: "+C,i(this,l)}if(0===o.length&&0===p.length&&(n=n.filter(function(a,c,d){return a!==b.adrequest.AdRequester.InsertionPointType.PLAYBACK_POSITION})),n.length>0){var D=function(a){return a.id!==d.id?(l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.ILLEGAL_OPERATION,l.message="The request response's session id does not match; unmodified session returned",i(j,l),void g(d)):(r(a,d,j),void g(d))},E=JSON.parse(JSON.stringify(f));return E.insertionPointFilter=n,f&&(E.linearPlaybackPositions=o,E.nonlinearPlaybackPositions=p),k.call(this,e,E,D,function(){g(d)},d)}return l=new b.LogItem,l.source=b.LogItem.SourceType.SESSION,l.event=b.LogItem.EventType.ILLEGAL_OPERATION,l.message="None of the provided insertion point types and/or playback positions can be requested again; unmodified session returned",i(this,l),setTimeout(function(){g(d)},0),function(){}}};D.prototype.addLogListener=function(a){a&&"[object Function]"==Object.prototype.toString.call(a)&&this._logCallbacks.push(a)},D.prototype.requestThirdParty=function(c,d){var e;if(c instanceof b.adresponse.InsertionPoint)e=!0;else{if(!(c instanceof b.adresponse.Ad))throw new Error("The supplied third party container is not of a valid type: only InsertionPoints and Ads are accepted");e=!1}if(c._partOfOngoingRequest)throw new Error("The supplied container is already waiting for a request response");if(e&&!c._partOfOngoingRequest)for(var f=0;f<c.slots.length;f++)for(var g=c.slots[f],h=0;h<g.ads.length;h++){var j=g.ads[h];if(j._partOfOngoingRequest)throw new Error("The supplied insertionPoint has ads already waiting for a request response")}if(!e&&c.ready)return void d(c);var k=function(a){if(a?c._partOfOngoingRequest=a:delete c._partOfOngoingRequest,e)for(var b=0;b<c.slots.length;b++)for(var d=c.slots[b],f=0;f<d.ads.length;f++){var g=d.ads[f];a?g._partOfOngoingRequest=a:delete g._partOfOngoingRequest}};k(!0);var l,m=!1,o=function(a){throw k(!1),new Error("Third party request failed unexpectedly: "+a)},q=function(){k(!1),m=!0,d(c)},r=new b.adrequest.AdResponseParser,s={target:this,vastParser:r,sessionCancelled:!1,skipLazyAds:!1},t=!1;e?(l=function(error){if(!m&&s.sessionCancelled===!1){for(var d,e,f,g=function(){},h=0;h<c.slots.length;++h)if(d=c.slots[h],h===c.slots.length-1)for(var j=0;j<d.ads.length;++j)e=d.ads[j],(f=n(e,s,g,a,g))("Third party ad request cancelled",!0);var k=new b.LogItem;k.message="Third party request for insertion point canceled: "+error,k.source=b.LogItem.SourceType.AD,k.event=b.LogItem.EventType.REQUEST_CANCELED,i(s.target,k),s.sessionCancelled=!0,q()}},p(c,s,q,a,o)):l=n(c,s,q,a,o);var u=function(a){if(t)throw new Error("Request has already been cancelled");t=!0,l(a)};return u},D.prototype.requestPassback=function(c,d,e){var f,g=!1;if(f=b.tracking.Tracker.resolveErrorCode(c,d),c instanceof b.adresponse.LinearCreative)c=c.parentAd,g=!0;else if(!(c instanceof b.adresponse.Ad))throw new Error("Invalid type: only Creative or Ad accepted");if(c.hasOwnProperty("trackingEvents")&&c.trackingEvents.hasOwnProperty("error")&&c.trackingEvents.error.hasOwnProperty("blocked")&&c.trackingEvents.error.blocked===!0)throw new Error("Cannot request passback after tracking error");if(c.hasOwnProperty("trackingEvents")&&c.trackingEvents.hasOwnProperty("impression")&&c.trackingEvents.impression.hasOwnProperty("blocked")&&c.trackingEvents.impression.blocked===!0)throw new Error("Cannot request passback after tracking impression");if(c._partOfOngoingRequest)throw g?new Error("The supplied creative's parent ad is already waiting for a request response"):new Error("The supplied ad is already waiting for a request response");if(c._partOfOngoingRequest=!0,!b.adresponse.Ad.hasPassback(c))throw g?new Error("The supplied creative's parent ad has no passback candidates available"):new Error("The supplied ad has no passback candidates available");var h,i=function(a){throw delete c._partOfOngoingRequest,new Error("Passback loading failed unexpectedly: "+a)},j=function(){for(var a=0;a<h.length;++a)h[a].thirdParty&&c.trackingEvents.error.urls.push(h[a]);delete c._partOfOngoingRequest,e(c)};c.trackingEvents.impression.urls.length=0,z(c,f),h=c.trackingEvents.error.urls,c.trackingEvents.error.urls=[];for(var k=0;k<c.creatives.length;++k)c.creatives[k].trackingEvents={},c.creatives[k].mediaFiles=[];c.companions=[];var l=new b.adrequest.AdResponseParser,m={target:this,vastParser:l,sessionCancelled:!1,skipLazyAds:!0},o=!1,p=n(c,m,j,a,i),q=function(a){if(o)throw new Error("Request has already been cancelled");o=!0,p(a)};return q};var E=/\[ERRORCODE\]|%5BERRORCODE%5D/g;return D}(),b.adrequest.AdRequester.InsertionPointType={ON_BEFORE_CONTENT:"onBeforeContent",PLAYBACK_POSITION:"playbackPosition",ON_CONTENT_END:"onContentEnd",ON_PAUSE:"onPause",PLAYBACK_TIME:"playbackTime"};var c={};for(var d in b.adrequest.AdRequester.InsertionPointType){var e=b.adrequest.AdRequester.InsertionPointType[d];c[e]=d}b.adrequest.AdRequester.ContentForm={SHORT_FORM:"shortForm",LONG_FORM:"longForm"}}(b),function(a){a.adrequest.AdResponseParser=function(){function b(a){return/^{.*}$/.test(a.trim())}var c,d,e=function(){return this instanceof a.adrequest.AdResponseParser?(c=new a.adrequest.VPTP3Parser,void(d=new a.adrequest.VAST3Parser)):new e};return e.prototype.parse=function(a){if(b(a)){var e;try{e=JSON.parse(a)}catch(f){throw new Error("Unable to parse VPTP Ticket, malformed JSON data: "+f.message)}try{return c.parse(e)}catch(g){throw new Error("Unable to parse VPTP Ticket: "+g.message)}}else try{return d.parse(a)}catch(g){throw g}},e}()}(b),function(b){b.adrequest.VPTP3Parser=function(){function c(a){var b=[];if("[object Object]"!==Object.prototype.toString.call(a))throw new Error("VPTP Ticket trackingEvents should be object but is "+Object.prototype.toString.call(a));if(!(a.tracking.length>0))throw new Error("VPTP Ticket trackingEvents.tracking can not be empty");for(var c=0;c<a.tracking.length;c++)b[a.tracking[c].event]?b[a.tracking[c].event].urls.push({url:a.tracking[c].value}):b[a.tracking[c].event]={urls:[{url:a.tracking[c].value}],blocked:!1};return b}function d(a,c){var d=[];if("[object Array]"!==Object.prototype.toString.call(a))throw new Error("VPTP Ticket insertionPoint should be array but is "+Object.prototype.toString.call(a));for(var f=0;f<a.length;f++){var g=new b.adresponse.InsertionPoint;if(g.parentSession=c,!p(a[f],"conditions"))throw new Error("VPTP Ticket insertionPoint is missing conditions!");if(g.conditions=n(a[f].conditions.condition),!p(a[f],"slot"))throw new Error("VPTP Ticket insertionPoint is missing slots!");g.slots=e(a[f].slot,g,c),d.push(g)}return d}function e(a,c,d){var e=[];if("[object Array]"!==Object.prototype.toString.call(a))throw new Error("VPTP Ticket insertionPoint.slot should be array but is "+Object.prototype.toString.call(a));if(!(a.length>0))throw new Error("VPTP Ticket insertionPoint.slot cannot be empty!");for(var g=0;g<a.length;g++){var h=a[g],i=new b.adresponse.Slot;if(i.parentInsertionPoint=c,p(h,"trackingEvents")){if("[object Object]"!==Object.prototype.toString.call(h.trackingEvents))throw new Error("VPTP Ticket insertionPoint.slot.trackingEvents should be object but is "+Object.prototype.toString.call(h.trackingEvents));if(!p(h.trackingEvents,"tracking"))throw new Error("VPTP Ticket slot.trackingEvents is missing tracking object!");if("[object Array]"!==Object.prototype.toString.call(h.trackingEvents.tracking))throw new Error("VPTP Ticket insertionPoint.slot.trackingEvents.tracking should be array but is "+Object.prototype.toString.call(h.trackingEvents.tracking));for(var j in h.trackingEvents.tracking){var l=h.trackingEvents.tracking[j];if(!p(l,"event"))throw new Error("VPTP Ticket slot.trackingEvent object is missing event property!");if("string"!=typeof l.event)throw new Error("VPTP Ticket slot.trackingEvent object event property should be string but is "+typeof l.event);if(!p(l,"value"))throw new Error("VPTP Ticket slot.trackingEvent object is missing value property!");if("string"!=typeof l.value)throw new Error("VPTP Ticket slot.trackingEvent object value property should be string but is "+typeof l.value);i.trackingEvents[l.event]?i.trackingEvents[l.event].urls.push({url:l.value}):i.trackingEvents[l.event]={urls:[{url:l.value}],blocked:!1}}}if(!p(h,"vast"))throw new Error("VPTP Ticket insertionPoint.slot missing VAST property!");if(!p(h.vast,"ad"))throw new Error("VPTP Ticket insertionPoint.slot.vast missing 'ad' property!");if("[object Array]"!==Object.prototype.toString.call(h.vast.ad))throw new Error("VPTP Ticket insertionPoint.slot.vast.ad property should be array but is "+Object.prototype.toString.call(h.vast.ad));if(!(h.vast.ad.length>0))throw new Error("VPTP Ticket insertionPoint.slot.vast.ad array is empty!");for(var n=h.vast.ad,o=n.length,q=[],r=0;o>r;r++){var s=null;if(!p(n[r],"inLine"))throw new Error("VPTP Ticket ad missing 'inLine' property!");if("[object Object]"!==Object.prototype.toString.call(n[r].inLine))throw new Error("VPTP Ticket ad.inLine property should be object but is "+Object.prototype.toString.call(n[r].inLine));if(p(n[r].inLine,"creatives")){if("[object Object]"!==Object.prototype.toString.call(n[r].inLine.creatives))throw new Error("VPTP Ticket ad.inLine.creatives property should be object but is "+Object.prototype.toString.call(n[r].inLine.creatives));if(!p(n[r].inLine.creatives,"creative"))throw new Error("VPTP Ticket ad.inLine.creatives missing 'creative' property!");if("[object Array]"!==Object.prototype.toString.call(n[r].inLine.creatives.creative))throw new Error("VPTP Ticket ad.inLine.creatives.creative property should be array but is "+Object.prototype.toString.call(n[r].inLine.creatives.creative));for(var t=0;t<n[r].inLine.creatives.creative.length;t++){var u=n[r].inLine.creatives.creative[t];if(p(u,"linear")){if("[object Object]"!==Object.prototype.toString.call(u.linear))throw new Error("Creative.linear should be object but is "+Object.prototype.toString.call(u.linear));if(!p(u.linear,"adParameters"))throw new Error("Linear is missing AdParameters!");if("[object Object]"!==Object.prototype.toString.call(u.linear.adParameters))throw new Error("Linear AdParameters should be object but is "+Object.prototype.toString.call(u.linear.adParameters));if(!p(u.linear.adParameters,"value"))throw new Error("Linear AdParameters is missing 'value' property!");if("[object String]"!==Object.prototype.toString.call(u.linear.adParameters.value))throw new Error("Unparsed AdParameters.value should be string but is "+Object.prototype.toString.call(u.linear.adParameters.value));u.linear.adParameters.value=JSON.parse(u.linear.adParameters.value)}else{if(!p(u,"nonLinearAds"))throw new Error("Creative is missing linear or nonLinearAds property!");if("[object Object]"!==Object.prototype.toString.call(u.nonLinearAds))throw new Error("Creative.nonLinearAds should be object but is "+Object.prototype.toString.call(u.nonLinearAds));if(!p(u.nonLinearAds,"nonLinear"))throw new Error("Creative.nonLinearAds is missing nonLinear!");if("[object Array]"!==Object.prototype.toString.call(u.nonLinearAds.nonLinear))throw new Error("Creative.nonLinearAds.nonLinear should be array but is "+Object.prototype.toString.call(u.nonLinearAds.nonLinear));if(0===u.nonLinearAds.nonLinear.length)throw new Error("Creative.nonLinearAds.nonLinear is empty!");if(!p(u.nonLinearAds.nonLinear[0],"adParameters"))throw new Error("Creative.nonLinearAds.nonLinear is missing AdParameters!");if("[object Object]"!==Object.prototype.toString.call(u.nonLinearAds.nonLinear[0].adParameters))throw new Error("Creative.nonLinearAds.nonLinear.adParameters should be object but is "+Object.prototype.toString.call(u.nonLinearAds.nonLinear[0].adParameters));if(!p(u.nonLinearAds.nonLinear[0].adParameters,"value"))throw new Error("Creative.nonLinearAds.nonLinear.adParameters is missing 'value' property!");if("[object String]"!==Object.prototype.toString.call(u.nonLinearAds.nonLinear[0].adParameters.value))throw new Error("Unparsed AdParameters.value should be string but is "+Object.prototype.toString.call(u.nonLinearAds.nonLinear[0].adParameters.value));u.nonLinearAds.nonLinear[0].adParameters.value=JSON.parse(u.nonLinearAds.nonLinear[0].adParameters.value)}if(0===t)if(p(u,"linear")){if(p(u.linear,"mediaFiles")&&p(u.linear.mediaFiles,"mediaFile")){if("[object Array]"!==Object.prototype.toString.call(u.linear.mediaFiles.mediaFile))throw new Error("VPTP Linear Creative.mediaFiles.mediaFile should be Array but is "+Object.prototype.toString.call(u.linear.mediaFiles.mediaFile));if(0===u.linear.mediaFiles.mediaFile.length)s=f(n[r],i);else{if(!p(u.linear.mediaFiles.mediaFile[0],"value"))throw new Error("VPTP MediaFile missing value property!");if("string"!=typeof u.linear.mediaFiles.mediaFile[0].value)throw new Error("VPTP MediaFile value property should be string but is "+typeof u.linear.mediaFiles.mediaFile[0].value);-1!=u.linear.mediaFiles.mediaFile[0].value.indexOf("spot_selector")&&(s=f(n[r],i),s.type=b.adresponse.Ad.AdType.SPOT_SELECTOR)}}s||(s=f(n[r].inLine.creatives.creative[0].linear.adParameters.value.ad[0],i),s=k(s,n[r]))}else p(u,"nonLinearAds")&&(s=f(n[r].inLine.creatives.creative[0].nonLinearAds.nonLinear[0].adParameters.value.ad[0],i),s=k(s,n[r]))}}else s=f(n[r],i),s.type="inventory";if(null!=s){if("spot_selector"!==s.type&&s.ads.length>0){for(var v=0,w=0;w<s.ads.length;++w)v+=s.ads[w].maximumPreparationTime;s.maximumPreparationTime=Math.min(s.maximumPreparationTime,v)}switch(s.type){case b.adresponse.Ad.AdType.SKIN_INSKIN:case b.adresponse.Ad.AdType.SPOT_INTERACTIVE:case b.adresponse.Ad.AdType.SPOT_SELECTOR:case b.adresponse.Ad.AdType.SPOT_TAKEOVER:s=m(s);default:q.push(s)}}}i.ads=q,e.push(i)}return e}function f(a,c){var d=new b.adresponse.Ad;if(d.parentSlot=c,d.ready=!0,p(a,"id")&&(d.id=a.id),p(a,"inLine")||p(a,"wrapper")){var e="inLine";if(p(a,"wrapper")){if(e="wrapper",!p(a[e],"vastadTagURI"))throw new Error("VPTP Wrapper ad missing vastadTagURI!");if(!(a[e].vastadTagURI.length>0))throw new Error("VPTP Wrapper ad vastadTagURI is empty!");d.thirdPartyURL=a[e].vastadTagURI}if(!p(a[e],"impression"))throw new Error("VPTP ad "+e+" is missing impression property!");if("[object Array]"!==Object.prototype.toString.call(a[e].impression))throw new Error("VPTP ad "+e+" impression property should be array but is "+Object.prototype.toString.call(a[e].impression));if(a[e].impression.length>0)for(var i=a[e].impression,l=i.length,m=0;l>m;m++){var n=i[m];p(d.trackingEvents,"impression")||(d.trackingEvents.impression={urls:[],blocked:!1}),"inLine"===e&&p(n,"value")?d.trackingEvents.impression.urls.push({url:n.value.toString()}):"wrapper"===e&&d.trackingEvents.impression.urls.push({url:n.toString()})}if(p(a[e],"error")){if("string"!=typeof a[e].error)throw new Error("VPTP Ad error property should be string but is "+typeof a[e]);a[e].error.length>0&&(p(d.trackingEvents,"error")?d.trackingEvents.error.urls.push({url:a[e].error}):d.trackingEvents.error={urls:[{url:a[e].error}],blocked:!1})}if(p(a[e],"creatives")){if("[object Object]"===Object.prototype.toString.call(a[e].creatives)&&p(a[e].creatives,"creative")&&"[object Array]"===Object.prototype.toString.call(a[e].creatives.creative)&&a[e].creatives.creative.length>0)for(var q,r=a[e].creatives.creative,s=r.length,t=0;s>t;t++){var u=r[t];if(q=0,p(u,"sequence")&&(q=parseInt(u.sequence),NaN===q&&(q=0)),p(u,"linear")){var v=new b.adresponse.LinearCreative;if(v.parentAd=d,v.sequence=q,p(u,"id")&&(v.type=u.id),p(u.linear,"duration")&&("string"==typeof u.linear.duration?v.duration=o(u.linear.duration):"number"==typeof u.linear.duration&&(v.duration=u.linear.duration)),p(u.linear,"mediaFiles")){if(!p(u.linear.mediaFiles,"mediaFile"))throw new Error("VPTP Linear Creative.mediaFiles missing mediaFile property!");if("[object Array]"===Object.prototype.toString.call(u.linear.mediaFiles.mediaFile)&&u.linear.mediaFiles.mediaFile.length>0)for(var w=u.linear.mediaFiles.mediaFile,x=w.length,y=0;x>y;y++){var z=w[y],A=new b.adresponse.MediaFile;if(!p(z,"value"))throw new Error("VPTP MediaFile missing value property!");if("string"!=typeof z.value)throw new Error("VPTP MediaFile value property should be string but is "+typeof z.value);if(A.url=z.value,!p(z,"type"))throw new Error("VPTP MediaFile missing type property!");if("string"!=typeof z.type)throw new Error("VPTP MediaFile type property should be string but is "+typeof z.type);if(A.mimeType=z.type,!p(z,"width"))throw new Error("VPTP MediaFile missing width property!");if("number"!=typeof z.width)throw new Error("VPTP MediaFile width property should be number but is "+typeof z.width);if(A.width=parseFloat(z.width),!p(z,"height"))throw new Error("VPTP MediaFile missing height property!");if("number"!=typeof z.height)throw new Error("VPTP MediaFile height property should be number but is "+typeof z.height);if(A.height=parseFloat(z.height),!p(z,"bitrate"))throw new Error("VPTP MediaFile missing bitrate property!");if("number"!=typeof z.bitrate)throw new Error("VPTP MediaFile bitrate property should be number but is "+typeof z.bitrate);if(A.bitRate=parseFloat(z.bitrate),!p(z,"delivery"))throw new Error("VPTP MediaFile missing delivery property!");if("string"!=typeof z.delivery)throw new Error("VPTP MediaFile delivery property should be string but is "+typeof z.delivery);if(A.deliveryMethod=z.delivery,p(z,"apiFramework")){if("string"!=typeof z.apiFramework)throw new Error("VPTP MediaFile apiFramework property should be string but is "+typeof z.apiFramework);A.apiFramework=z.apiFramework}p(v,"mediaFiles")||(v.mediaFiles=[]),v.mediaFiles.push(A)}}else if("wrapper"!==e)throw new Error("VPTP Linear Creative missing mediaFiles property!");if(p(u.linear,"videoClicks")){if("[object Object]"!==Object.prototype.toString.call(u.linear.videoClicks))throw new Error("VPTP Linear Creative.videoClicks should be Object but is "+Object.prototype.toString.call(u.linear.videoClicks));if(!p(u.linear.videoClicks,"clickTracking"))throw new Error("VPTP Linear Creative videoClicks missing clickTracking property!");if("[object Array]"!==Object.prototype.toString.call(u.linear.videoClicks.clickTracking))throw new Error("VPTP Linear Creative.videoClicks.clickTracking should be Array but is "+Object.prototype.toString.call(u.linear.videoClicks.clickTracking));if(u.linear.videoClicks.clickTracking.length>0){p(v.trackingEvents,"clickthrough")||(v.trackingEvents.clickthrough={urls:[],blocked:!1});for(var B in u.linear.videoClicks.clickTracking){var C=u.linear.videoClicks.clickTracking[B];if(!p(C,"value"))throw new Error("VPTP ClickTracking object missing value property!");if("string"!=typeof C.value)throw new Error("VPTP ClickTracking value property should be string but is "+typeof C.value);v.trackingEvents.clickthrough.urls.push({url:C.value})}}if(p(u.linear.videoClicks,"clickThrough")){if("[object Object]"!==Object.prototype.toString.call(u.linear.videoClicks.clickThrough))throw new Error("VPTP Linear Creative.videoClicks clickThrough property should be object but is "+Object.prototype.toString.call(u.linear.videoClicks.clickThrough));if(!p(u.linear.videoClicks.clickThrough,"value"))throw new Error("VPTP Linear Creative.videoClicks.clickThrough missing value property!");if("string"!=typeof u.linear.videoClicks.clickThrough.value)throw new Error("VPTP Linear Creative.videoClicks.clickThrough value property should be string but is "+typeof u.linear.videoClicks.clickThrough.value);v.clickThroughUrl=u.linear.videoClicks.clickThrough.value}else if("wrapper"!==e)throw new Error("VPTP Linear Creative.videoClicks missing clickThrough property!")}if(p(u.linear,"trackingEvents")){if("[object Object]"!==Object.prototype.toString.call(u.linear.trackingEvents))throw new Error("VPTP Linear Creative.trackingEvents should be Object but is "+Object.prototype.toString.call(u.linear.trackingEvents));if(!p(u.linear.trackingEvents,"tracking"))throw new Error("VPTP Linear Creative missing trackingEvents.tracking property!");if("[object Array]"!==Object.prototype.toString.call(u.linear.trackingEvents.tracking))throw new Error("VPTP Linear Creative.trackingEvents.tracking should be Array but is "+Object.prototype.toString.call(u.linear.trackingEvents.tracking));if(!(u.linear.trackingEvents.tracking.length>0))throw new Error("VPTP Linear Creative.trackingEvents.tracking array is empty!");v.trackingEvents=j(v.trackingEvents,u.linear.trackingEvents.tracking)}if(d.creatives.push(v),p(u.linear,"adParameters")&&"[object Object]"===Object.prototype.toString.call(u.linear.adParameters)&&p(u.linear.adParameters,"value")&&"[object Object]"===Object.prototype.toString.call(u.linear.adParameters.value)&&p(u.linear.adParameters.value,"ad")){if("[object Array]"!==Object.prototype.toString.call(u.linear.adParameters.value.ad))throw new Error("VPTP Linear Creative adParameters.value.ad should be Array but is "+Object.prototype.toString.call(u.linear.adParameters.value.ad));if(!(u.linear.adParameters.value.ad.length>0))throw new Error("VPTP Linear Creative adParameters.value.ad is empty!");for(var D=u.linear.adParameters.value.ad,E=D.length,F=0;E>F;F++){var G=f(D[F],c);G&&(p(G,"lazyPrepared")&&(d.ready=!1),d.ads.push(G))}}}else if(p(u,"nonLinearAds"))p(u.nonLinearAds,"nonLinear")&&h(d,u.nonLinearAds,q,u.id);else if(p(u,"companionAds")&&p(u.companionAds,"companion")){var H=b.adresponse.Companion.RequiredRule.NO;if(p(u.companionAds,"required"))switch(u.companionAds.required){case"all":H=b.adresponse.Companion.RequiredRule.YES;break;case"any":H=b.adresponse.Companion.RequiredRule.AT_LEAST_ONE_FOR_THIS_SEQUENCE;break;case"none":H=b.adresponse.Companion.RequiredRule.NO}g(d,u.companionAds.companion,q,H)}}}else d.type="inventory",d.trackingEvents.error=d.trackingEvents.impression,d.trackingEvents.impression={urls:[],blocked:!1};d=k(d,a)}return d}function g(a,c,d,e){for(var f,g,h=0;h<c.length;++h){if(f=c[h],g=new b.adresponse.Companion,g.parentAd=a,g.sequence=d,g.required=e,!p(f,"width"))throw new Error("VPTP Companion missing required attribute width");if(g.width=parseInt(f.width),isNaN(g.width))throw new Error("VPTP Companion width is not a number");if(!p(f,"height"))throw new Error("VPTP Companion missing required attribute height");if(g.height=parseInt(f.height),isNaN(g.height))throw new Error("VPTP Companion height is not a number");if(p(f,"id")&&(g.id=f.id),i(f,g,!0),p(f,"trackingEvents")){if("[object Object]"!==Object.prototype.toString.call(f.trackingEvents))throw new Error("VPTP Linear Companion.trackingEvents should be Object but is "+Object.prototype.toString.call(f.trackingEvents));if(!p(f.trackingEvents,"tracking"))throw new Error("VPTP Linear Companion missing trackingEvents.tracking property!");if("[object Array]"!==Object.prototype.toString.call(f.trackingEvents.tracking))throw new Error("VPTP Linear Companion.trackingEvents.tracking should be Array but is "+Object.prototype.toString.call(f.trackingEvents.tracking));if(!(f.trackingEvents.tracking.length>0))throw new Error("VPTP Linear Companion.trackingEvents.tracking array is empty!");g.trackingEvents=j(g.trackingEvents,f.trackingEvents.tracking)}if(p(f,"companionClickTracking")){p(g.trackingEvents,"clickThrough")||(g.trackingEvents.clickThrough={urls:[],blocked:!1});for(var k,l,h=0;h<f.companionClickTracking.length;++h){if(k=f.companionClickTracking[h],!p(k,"value"))throw new Error("Companion has ClickTracking with no URL value");l={url:k.value,thirdPartyURL:!0},p(k,"id")||(l.id=k.id),g.trackingEvents.clickThrough.urls.push(l)}}a.companions.push(g)}}function h(a,c,d,e){for(var f,g,h=0;h<c.nonLinear.length;++h){if(f=c.nonLinear[h],g=new b.adresponse.NonLinearCreative,g.parentAd=a,g.sequence=d,p(f,"minSuggestedDuration")&&("string"==typeof f.minSuggestedDuration?g.duration=o(f.minSuggestedDuration):"number"==typeof f.minSuggestedDuration&&(g.duration=f.minSuggestedDuration),isNaN(g.duration)))throw new Error("VPTP NonLinearCreative duration is not a number");if(p(f,"width")&&(g.width=parseInt(f.width),isNaN(g.width)))throw new Error("VPTP NonLinearCreative width is not a number");if(p(f,"height")&&(g.height=parseInt(f.height),isNaN(g.height)))throw new Error("VPTP NonLinearCreative height is not a number");if(e&&(g.type=e),i(f,g,!1),p(c,"trackingEvents")){if("[object Object]"!==Object.prototype.toString.call(c.trackingEvents))throw new Error("VPTP nonLinearAds trackingEvents should be Object but is "+Object.prototype.toString.call(c.trackingEvents));if(!p(c.trackingEvents,"tracking"))throw new Error("VPTP nonLinearAds missing trackingEvents.tracking property!");if("[object Array]"!==Object.prototype.toString.call(c.trackingEvents.tracking))throw new Error("VPTP nonLinearAds trackingEvents.tracking should be Array but is "+Object.prototype.toString.call(c.trackingEvents.tracking));if(!(c.trackingEvents.tracking.length>0))throw new Error("VPTP nonLinearAds trackingEvents.tracking array is empty!");g.trackingEvents=j(g.trackingEvents,c.trackingEvents.tracking)}if(p(f,"nonLinearClickTracking")){p(g.trackingEvents,"clickThrough")||(g.trackingEvents.clickThrough={urls:[],blocked:!1});for(var k,l,h=0;h<f.nonLinearClickTracking.length;++h){if(k=f.nonLinearClickTracking[h],!p(k,"value"))throw new Error("NonLinearCreative has ClickTracking with no URL value");l={url:k.value,thirdPartyURL:!0},p(k,"id")||(l.id=k.id),g.trackingEvents.clickThrough.urls.push(l)}}a.creatives.push(g)}}function i(a,c,d){var e,f=d?"Companion":"NonLinearCreative";if(p(a,"staticResource")){if(!p(a.staticResource,"value"))throw new Error("VPTP "+f+" has StaticResource with no URL value");if(e=new b.adresponse.StaticResource,e.url=a.staticResource.value,p(a.staticResource,"creativeType")&&(e.mimeType=a.staticResource.creativeType),p(a,"apiFramework")&&(e.apiFramework=a.apiFramework),p(a,"adParameters")&&p(a.adParameters,"value")&&(e.adParameters=a.adParameters.value,p(a.adParameters,"xmlEncoded"))){if("boolean"!=typeof a.adParameters.xmlEncoded)throw new Error("VPTP "+f+" has adParameters.xmlEncoded with invalid type");e.adParametersXmlEncoded=a.adParameters.xmlEncoded}if(d?p(a,"companionClickThrough")&&(e.clickThroughUrl=a.companionClickThrough):p(a,"nonLinearClickThrough")&&(e.clickThroughUrl=a.nonLinearClickThrough),p(a,"assetWidth")&&(e.assetWidth=parseInt(a.assetWidth),isNaN(e.assetWidth)))throw new Error("VPTP "+f+" assetWidth is not a number");if(p(a,"assetHeight")&&(e.assetHeight=parseInt(a.assetHeight),isNaN(e.assetHeight)))throw new Error("VPTP "+f+" assetHeight is not a number");if(p(a,"expandedWidth")&&(e.expandedWidth=parseInt(a.expandedWidth),isNaN(e.expandedWidth)))throw new Error("VPTP "+f+" expandedWidth is not a number");if(p(a,"expandedHeight")&&(e.expandedHeight=parseInt(a.expandedHeight),isNaN(e.expandedWidth)))throw new Error("VPTP "+f+" expandedHeight is not a number");c.resources.push(e)}p(a,"iframeResource")&&(e=new b.adresponse.IFrameResource,e.url=a.iframeResource,c.resources.push(e)),a.hasOwnProperty("htmlResource")?(e=new b.adresponse.HtmlResource,e.source=a.htmlResource.value,c.resources.push(e)):a.hasOwnProperty("htmlresource")&&(e=new b.adresponse.HtmlResource,e.source=a.htmlresource.value,c.resources.push(e))}function j(a,c){a||(a={});for(var d=0;d<c.length;d++){var e=c[d];if(!p(e,"value"))throw new Error("VPTP Tracking object missing value property!");if(!p(e,"event"))throw new Error("VPTP Tracking object missing event property!");var f="";switch(e.event){case"3":case"4":case"91":f="";break;case"10":f="interaction";break;case"100":f="timeSpent";break;default:f=e.event}"acceptinvitationlinear"===f.toLowerCase()?f=b.tracking.Tracker.CreativeEventType.ACCEPT_INVITATION:"closelinear"===f.toLowerCase()&&(f=b.tracking.Tracker.CreativeEventType.CLOSE),f.length>0&&(p(a,f)||(a[f]={urls:[],blocked:!1}),a[f].urls.push({url:e.value.toString()}))}return a}function k(c,d){var e=a,f=null;if(p(d,"inLine"))f=d.inLine;else{if(!p(d,"wrapper"))return c;f=d.wrapper}var g=null;if(p(f,"extensions")){if(!p(f.extensions,"extension"))throw new Error("VPTP Extensions missing extension property!");if("[object Array]"!==Object.prototype.toString.call(f.extensions.extension))throw new Error("VPTP Extensions.extension should be array but is "+Object.prototype.toString.call(f.extensions.extension));if(f.extensions.extension.length>0)for(var h=f.extensions.extension,i=h.length,j=0;i>j;j++){var k=h[j];p(k,"adInfo")&&(g=k.adInfo)}}if(!c||!g)return c;if(p(g,"format")&&(c.type=g.format),p(g,"requestType")&&"lazy"===g.requestType&&null!==c.thirdPartyURL&&(c.lazyPrepared=!0),p(g,"timeout"))c.maximumPreparationTime=o(g.timeout);else if(b.adresponse.Ad.hasPassback(c))throw new Error("VPTP Extensions missing timeout value");if(p(g,"startAdTimeout")&&(c.startTimeout=o(g.startAdTimeout)),p(g,"variant")?c.variant=g.variant&&"normal"!==g.variant.toLowerCase()?"sponsor":"normal":c.variant="normal",p(g,"gid")&&(c.goalId=g.gid),p(g,"cid")&&(c.campaignId=g.cid),p(g,"customaid")&&(c.customId=g.customaid),p(g,"customgid")&&(c.customGoalId=g.customgid),p(g,"customcid")&&(c.customCampaignId=g.customcid),p(g,"allowLinearModeChange")&&(c.allowLinearModeChange="true"==g.allowLinearModeChange||"TRUE"==g.allowLinearModeChange),p(g,"countdown")&&(c.showCountdown="true"==g.countdown||"TRUE"==g.countdown),p(g,"exclusive")&&(c.partOfExclusiveCampaign="true"==g.exclusive||"TRUE"==g.exclusive),g.hasOwnProperty("labels")&&g.labels.hasOwnProperty("label")&&"[object Array]"===Object.prototype.toString.call(g.labels.label)&&g.labels.label.length>0)for(var m=0;m<g.labels.label.length;m++){var n=g.labels.label[m];n.hasOwnProperty("name")&&n.hasOwnProperty("value")&&(c.labels[n.name]=n.value)}p(g,"lastImpression")&&(e=Number(g.lastImpression)),p(g,"lastCompletion")&&(e=Number(g.lastCompletion));for(var q=c.creatives.filter(function(a,c,d){return a instanceof b.adresponse.LinearCreative||a instanceof b.adresponse.NonLinearCreative}),r=0;r<q.length;r++){var s=q[r];if(g.hasOwnProperty("skipOffset")||g.hasOwnProperty("skipoffset")){var t,u=g.hasOwnProperty("skipoffset")?g.skipoffset:g.skipOffset;if(u.indexOf("%")>-1){var v=parseFloat(String(u).substring(0,u.indexOf("%")));t=(s.duration||0)*(v/100)}else t=o(u);s.skipOffset=t}p(g,"showSkipButton")&&(g.showSkipButton===b.adresponse.LinearCreative.SkipButtonMode.ALWAYS?s.skipButtonMode=b.adresponse.LinearCreative.SkipButtonMode.ALWAYS:g.showSkipButton===b.adresponse.LinearCreative.SkipButtonMode.NEVER?s.skipButtonMode=b.adresponse.LinearCreative.SkipButtonMode.NEVER:g.showSkipButton===b.adresponse.LinearCreative.SkipButtonMode.AFTER_FIRST_COMPLETION&&(s.skipButtonMode=b.adresponse.LinearCreative.SkipButtonMode.AFTER_FIRST_COMPLETION),"after_first_impression"===g.showSkipButton&&(s.skipButtonMode=b.adresponse.LinearCreative.SkipButtonMode.AFTER_FIRST_COMPLETION)),p(g,"skipReset")&&(s.skipResetTime=o(g.skipReset)),s.lastCompletion=e}if(p(g,"companions")&&p(g.companions,"companion"))for(var w=g.companions.companion,m=0;m<w.length;++m)l(c,w[m]);
return c}function l(a,b){var c=null;if(!p(b,"id"))throw new Error("VPTP Companion AdInfo is missing id");for(var d=0;d<a.companions.length;++d)if(a.companions[d].id===b.id){c=a.companions[d];break}if(null!==c){if(p(b,"customaid")&&(c.customId=b.customaid),!p(b,"zone"))throw new Error("VPTP Companion AdInfo is missing zone");c.zone=b.zone}}function m(a){var c=new b.adresponse.Ad;return c.type=b.adresponse.Ad.AdType.INVENTORY,a.type==b.adresponse.Ad.AdType.SPOT_SELECTOR?c.trackingEvents.error=a.ads[0].trackingEvents.error:c.trackingEvents.error=a.trackingEvents.error,c.parentSlot=a.parentSlot,c}function n(a){var c=[];if("[object Array]"!==Object.prototype.toString.call(a))throw new Error("VPTP Ticket conditions should be array but is "+Object.prototype.toString.call(a));for(var d=0;d<a.length;d++){var e,f=a[d];if(!p(f,"type"))throw new Error("VPTP condition is missing type property!");if("string"!=typeof f.type)throw new Error("VPTP condition type property should be string but is "+typeof f.type);if("EVENT"===f.type)e=new b.adresponse.EventCondition;else{if("PROPERTY"!==f.type)throw new Error("VPTP condition with unknown type: "+f.type);e=new b.adresponse.PropertyCondition}if(!p(f,"name"))throw new Error("VPTP condition is missing name property!");if("string"!=typeof f.name)throw new Error("VPTP condition name property should be string but is "+typeof f.name);if(e.type=f.name.toLowerCase().substr(0,1)+f.name.substr(1),e instanceof b.adresponse.PropertyCondition){if(p(f,"value")){if("string"!=typeof f.value)throw new Error("VPTP condition value property should be string but is "+typeof f.value);e.value=o(f.value)}if(p(f,"operator")){if("string"!=typeof f.operator)throw new Error("VPTP condition operator property should be string but is "+typeof f.operator);e.operator=f.operator.toLowerCase()}}if(p(f,"condition")){if("[object Array]"!==Object.prototype.toString.call(f.condition))throw new Error("VPTP condition condition property should be Array but is "+Object.prototype.toString.call(f.condition));e.conditions=n(f.condition)}c.push(e)}return c}function o(a){if(!a)return Number.NaN;var b=a.split(":");if(!b||0===b.length)return Number.NaN;var c=0;b.length>=1&&isNaN(parseFloat(b[b.length-1]))===!1&&(c=parseFloat(b[b.length-1]));var d=0;b.length>=2&&isNaN(parseFloat(b[b.length-2]))===!1&&(d=parseInt(b[b.length-2]));var e=0;return b.length>=3&&isNaN(parseFloat(b[b.length-3]))===!1&&(e=parseInt(b[b.length-3])),c+60*d+3600*e}function p(a,b){var c=[];for(var d in a)c.push({actual:d,lowerCase:d.toLowerCase()});for(var e=0;e<c.length;e++)if(c[e].lowerCase===b.toLowerCase()){if(c[e].actual===b)return!0;throw new Error("Invalid object property key! Expected: "+b+" but was: "+c[e].actual)}return!1}var q=function(){return this instanceof b.adrequest.VPTP3Parser?void 0:new q};return q.prototype.parse=function(a){var e=new b.adresponse.Session;if(e.id=a.tid||"",e.language=a.lang||"",p(a,"trackingEvents")&&(e.trackingEvents=c(a.trackingEvents)),!p(a,"insertionPoint"))throw new Error("VPTP Ticket is missing insertionPoints!");return e.insertionPoints=d(a.insertionPoint,e),e},q}()}(b),function(b){b.adrequest.VAST3Parser=function(){function c(a,c){var e,f,g,l,n,o,p,q,error,r,s=!1;if(e=new b.adresponse.Ad,!(a.getElementsByTagName("AdSystem").length>0))throw error=new Error,error.message='Invalid VAST. VAST Ad missing "AdSystem" element.',error.errorCode="101",error;var t=a.getElementsByTagName("AdSystem")[0].textContent;if(t&&t.toLowerCase().indexOf("videoplaza karbon")>-1&&(s=!0),"Wrapper"===a.firstChild.nodeName){if(f=a.getElementsByTagName("VASTAdTagURI"),!(f.length>0))throw error=new Error,error.message="Invalid VAST. VAST Wrapper Ad missing VASTAdTagURI element.",error.errorCode="101",error;e.thirdPartyURL=f[0].textContent}else f=null;for(e.sequence=j(a,"sequence")?parseInt(j(a,"sequence")):0,e.id=j(a,"id"),null===e.id&&(e.id="0",e.type="inventory"),l=a.getElementsByTagName("Extension"),g=0;g<l.length;g++)"AdServer"===j(l[g],"type")&&"Videoplaza"===j(l[g],"name")&&(n=l[g].getElementsByTagName("AdInfo")[0],e.campaignId=j(n,"cid"),e.customId=j(n,"customaid"),e.customGoalId=j(n,"customgid"),e.customCampaignId=j(n,"customcid"),e.goalId=j(n,"gid"),e.type=k(j(n,"format")),e.variant=i(j(n,"variant")),e.partOfAnExclusiveCampaign="true"===j(n,"exclusive"),o=n.getElementsByTagName("Companion"));if(p=a.getElementsByTagName("Impression"),p.length<1)throw error=new Error,error.message="Invalid VAST. VAST Ad missing Impression elements!",error.errorCode="101",error;for(g=0;g<p.length;g++){e.trackingEvents.hasOwnProperty("impression")||(e.trackingEvents.impression={urls:[],blocked:!1});var u=j(p[g],"id");u?e.trackingEvents.impression.urls.push({id:u,url:h(m(p[g].textContent),c),thirdParty:!0}):e.trackingEvents.impression.urls.push({url:h(m(p[g].textContent),c),thirdParty:!0})}for(q=a.getElementsByTagName("Error"),null===f&&s&&0===q.length&&"inventory"===e.type&&(q=p),g=0;g<q.length;g++)e.trackingEvents.hasOwnProperty("error")||(e.trackingEvents.error={urls:[],blocked:!1}),e.trackingEvents.error.urls.push({url:h(m(q[g].textContent),c),thirdParty:!0});if(r=a.getElementsByTagName("Creative"),!s&&0===r.length&&null===f)throw error=new Error,error.message="Invalid VAST. VAST InLine Ad missing creatives!",error.errorCode="101",error;return e.creatives=d(r,e,c),e}function d(c,d,f){for(var i,k,l,error,n,o,p,q,r,s,t,u,v,w=[],x={Linear:"linear",NonLinearAds:"nonLinear",CompanionAds:"companion"},y=0;y<c.length;y++){i=j(c[y],"id");var z;if(k=x[c[y].firstChild.nodeName],"linear"===k){for(z=new b.adresponse.LinearCreative,z.parentAd=d,z.type=k,z.id=i,z.sequence=j(c[y],"sequence")?parseInt(j(c[y],"sequence")):0,n=c[y].getElementsByTagName("TrackingEvents"),n.length>0&&(z.trackingEvents=e(n[0],f,k)),p=c[y].getElementsByTagName("ClickTracking"),o=0;o<p.length;o++)z.trackingEvents.hasOwnProperty("clickthrough")||(z.trackingEvents.clickthrough={urls:[],blocked:!1}),z.trackingEvents.clickthrough.urls.push({url:h(m(p[o].textContent),f),thirdParty:!0});for(var A=c[y].getElementsByTagName("CustomClick"),B=0;B<A.length;B++)z.trackingEvents.hasOwnProperty("customClick")||(z.trackingEvents.customClick={urls:[],blocked:!1}),z.trackingEvents.customClick.urls.push({url:h(m(A[B].textContent),f),thirdParty:!0});if(q=c[y].getElementsByTagName("ClickThrough"),q.length>0&&(z.clickThroughUrl=m(q[0].textContent)),r=c[y].getElementsByTagName("MediaFile"),0===d.thirdPartyURL.length){if(!r||0===r.length)throw error=new Error,error.message="Invalid VAST. Creative element has no MediaFiles.",error.errorCode="101",error;for(o=0;o<r.length;o++){if(s=new b.adresponse.MediaFile,!j(r[o],"delivery"))throw error=new Error,error.message='Invalid VAST. MediaFile missing property "delivery".',error.errorCode="101",error;if(!j(r[o],"width"))throw error=new Error,error.message='Invalid VAST. MediaFile missing property "width".',error.errorCode="101",error;if(!j(r[o],"height"))throw error=new Error,error.message='Invalid VAST. MediaFile missing property "height".',error.errorCode="101",error;if(!j(r[o],"type"))throw error=new Error,error.message='Invalid VAST. MediaFile missing property "type".',error.errorCode="101",error;s.apiFramework=j(r[o],"apiFramework"),s.bitRate=parseFloat(j(r[o],"bitrate")),s.deliveryMethod=j(r[o],"delivery"),s.height=parseFloat(j(r[o],"height")),s.id=j(r[o],"id"),s.mimeType=j(r[o],"type"),s.url=r[o].textContent,s.width=parseFloat(j(r[o],"width")),z.mediaFiles.push(s)}}else if(0!==r.length)throw error=new Error,error.message="Invalid VAST. MediaFile elements not allowed in Wrapper Linear.",error.errorCode="101",error;if(l=c[y].getElementsByTagName("Duration"),0===d.thirdPartyURL.length){if(!l||0===l.length)throw error=new Error,error.message="Invalid VAST. No Duration element.",error.errorCode="101",error;try{z.duration=g(l[0].textContent)}catch(C){z.duration=a}}else if(0!==l.length)throw error=new Error,error.message="Invalid VAST. Duration element not allowed in Wrapper Linear.",error.errorCode="101",error;if(v=j(c[y].firstChild,"skipoffset")){var D;if(v.indexOf("%")>-1)if(z.duration){var E=parseFloat(String(v).substring(0,v.indexOf("%")));D=isNaN(E)?a:(z.duration||0)*(E/100)}else D=v;else D=g(v);z.skipOffset=D}z.adParameters=c[y].getElementsByTagName("AdParameters"),z.adParameters.length>0&&(z.adParameters=m(z.adParameters[0].textContent)),w.push(z)}else{if("nonLinear"===k)throw error=new Error,error.message="Invalid VAST. VAST nonlinear creatives not accepted.",error.errorCode="101",error;if("companion"===k&&"3.0"===f&&(t=c[y].firstChild,u=j(t,"required"),null===u||"none"!==u))throw error=new Error,error.message='Invalid VAST. VAST 3.0 companions not accepted unless "required" attribute is "none".',error.errorCode="101",error}}return w}function e(a,c,d){var e,g,i,k,l;e={},g=a.getElementsByTagName("Tracking");for(var n=0;n<g.length;n++)if(i=g.item(n),k=j(i,"event"),"acceptinvitationlinear"===k.toLowerCase()?k=b.tracking.Tracker.CreativeEventType.ACCEPT_INVITATION:"closelinear"===k.toLowerCase()&&(k=b.tracking.Tracker.CreativeEventType.CLOSE),"progress"===k){if(l=j(i,"offset"),!f(l)){var error=new Error;throw error.message="Invalid VAST. Progress event with missing or invalid offset value.",error.errorCode="101",error}e.hasOwnProperty(k)||(e[k]={}),e[k].hasOwnProperty(l)||(e[k][l]={urls:[],blocked:!1}),e[k][l].urls.push({url:h(m(i.textContent),c),thirdParty:!0})}else e.hasOwnProperty(k)||(e[k]={urls:[],blocked:!1}),e[k].urls.push({url:h(m(i.textContent),c),thirdParty:!0});return e}function f(a){return a&&null!==a.match(n)}function g(a){var b=a.split(":"),c=0;if(!(b.length>0&&b.length<=3))throw new Error("Invalid VAST. Invalid timestamp format");for(var d=0;d<b.length;d++)c+=Number(b[b.length-d-1]*(d?Math.pow(60,d):1));if(isNaN(c))throw new Error("Invalid VAST. Invalid timestamp format");return c}function h(a,b){return"2.0"===b&&-1===a.indexOf("[CACHEBUSTING]")&&-1===a.indexOf("%5BCACHEBUSTING%5D")&&a.length>0&&(a+=a.indexOf("?")>-1?"&rnd=[CACHEBUSTING]":"?rnd=[CACHEBUSTING]"),a}function i(a){return a&&(a=a.toLowerCase(),"bumper"===a||"sponsor"===a)?"sponsor":"normal"}function j(a,b){var c;return a&&a.attributes&&(c=a.attributes.getNamedItem(b))?c.value:null}function k(a){return"spot_standard"===a?"standard_spot":a}function l(a){var b,c,d=/^\s*$/;if(3===a.nodeType)d.test(a.nodeValue)&&a.parentNode.removeChild(a);else if(1===a.nodeType||9===a.nodeType)for(b=a.firstChild;b;)c=b.nextSibling,l(b),b=c}function m(a){return a.replace(/^\s+|\s+$/g,"")}var n=/^(\d\d:\d\d:\d\d(.\d\d\d)?)$|^((100)|(\d\d?)|(1\d\d?\.0*)|(\d\d?\.\d?\d?)|(\.\d\d?))%$/,o=function(){return this.parser=new window.DOMParser,this instanceof b.adrequest.VAST3Parser?void 0:new o};return o.prototype.parse=function(a){var d,e,f,g,error,i;try{d=this.parser.parseFromString(a,"text/xml")}catch(k){throw error=new Error,error.message="Could not parse VAST. "+k.message,error.errorCode="100",error}if(e=d.documentElement,l(e),!e)throw error=new Error,error.message="Could not parse VAST. No root element",error.errorCode="100",error;if("VAST"!==e.nodeName)throw error=new Error,error.message='Could not parse VAST. Root element is not "VAST".',error.errorCode="100",error;i=j(e,"version");var n;if(e.hasChildNodes()){if(f=e.getElementsByTagName("Ad"),g=e.getElementsByTagName("Error"),"Ad"===e.firstChild.nodeName){if(f.length>1)throw error=new Error,error.message="Invalid VAST. Found more than one ad.",error.errorCode="101",error;n=c(f[0],i)}else if("Error"===e.firstChild.nodeName){n=new b.adresponse.Ad,n.id="0",n.type="inventory";for(var o=0;o<g.length;o++)n.trackingEvents.hasOwnProperty("error")||(n.trackingEvents.error={urls:[],blocked:!1}),n.trackingEvents.error.urls.push({url:h(m(g[o].textContent),i),thirdParty:!0})}else if("parsererror"===e.firstChild.nodeName)throw error=new Error,error.message="Could not parse VAST. XML bad form: "+m(e.firstChild.textContent),error.errorCode="100",error}else n=new b.adresponse.Ad,n.id="0",n.type="inventory";return n},o}()}(b),function(b){b.tracking.Tracker=function(){function c(a){var b;for(var c in a.trackingEvents)a.trackingEvents[c].blocked=!0;for(var d=0;d<a.creatives.length;++d){b=a.creatives[d];for(var c in b.trackingEvents)if("progress"===c)for(var e in b.trackingEvents.progress)b.trackingEvents.progress[e].blocked=!0;else b.trackingEvents[c].blocked=!0}}function d(a,c){if(!a.hasOwnProperty("trackingEvents")){var d=new b.LogItem;throw d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.INVALID_ARGUMENT,d.message="Missing trackingEvents property on the passed Session Object. Can not track event '"+c+"'.",p(this,d),new Error("Abort execution")}if(!a.trackingEvents.hasOwnProperty(c)){var d=new b.LogItem;throw d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.REQUEST_FAILED,d.message="Event '"+c+"' does not exist on the passed Session Object.",p(this,d),new Error("Abort execution")}if(a.trackingEvents[c].blocked){var d=new b.LogItem;throw d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.ILLEGAL_OPERATION,d.message="Can not track '"+c+"' more than once on the passed Session Object.",p(this,d),new Error("Abort execution")}a.trackingEvents[c].blocked=!0,f.apply(this,[a.trackingEvents[c].urls])}function e(a,c){return function(error){if(!c){var d=new b.LogItem;d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.REQUEST_FAILED,d.message=error+". Failed to track URL : "+a.url,p(this,d)}}}function f(a,c,d){for(var f=this,g=0;g<a.length;g++){var h=a[g];if(h){var i=new b.HTTPRequester,j=e(h,d).bind(f),k=h.url.replace(x,encodeURIComponent(c));k=k.replace(z,Math.floor(1e16*Math.random())),i.request(k,null,j)}}}function g(a,b,c){for(var d=a;d<v.length;++d)h.apply(this,[b,v[d],c,!0])}function h(a,c,d,e){var g=a;"error"===c&&(a instanceof b.adresponse.LinearCreative||a instanceof b.adresponse.NonLinearCreative)&&(a=a.parentAd);for(var h=Object.keys(a.trackingEvents),i=!1,j=0;j<h.length;j++)if(h[j].toLowerCase()===c.toLowerCase())if(a.trackingEvents[h[j]].blocked){if(!e){var k=new b.LogItem;throw k.source=b.LogItem.SourceType.TRACKER,k.event=b.LogItem.EventType.ILLEGAL_OPERATION,k.message="Can not track '"+c+"' more than once"+r(g),p(this,k),new Error("Abort execution")}}else w[c.toLowerCase()]&&(a.trackingEvents[h[j]].blocked=!0),f.apply(this,[a.trackingEvents[h[j]].urls,d,e]),i=!0;if(!i&&!e){var k=new b.LogItem;k.source=b.LogItem.SourceType.TRACKER,k.event=b.LogItem.EventType.WARNING,k.message="Event '"+c+"' does not exist"+r(g),p(this,k)}}function i(a,c){var d,e=!1;if(a.trackingEvents.hasOwnProperty("progress")&&a.trackingEvents.progress.hasOwnProperty(c)){if(d=a.trackingEvents.progress[c],d.blocked){var g=new b.LogItem;throw g.source=b.LogItem.SourceType.TRACKER,g.event=b.LogItem.EventType.ILLEGAL_OPERATION,g.message="Can not track progress '"+c+"' more than once"+r(a),p(this,g),new Error("Abort execution")}d.blocked=!0,f.apply(this,[d.urls,0]),e=!0}if(!e){var g=new b.LogItem;g.source=b.LogItem.SourceType.TRACKER,g.event=b.LogItem.EventType.WARNING,g.message="Event 'progress' does not exist or offset is invalid"+r(a),p(this,g)}}function j(c,d,e,m,n){if(!c.hasOwnProperty("trackingEvents")){var o=new b.LogItem;throw o.source=b.LogItem.SourceType.TRACKER,o.event=b.LogItem.EventType.INVALID_ARGUMENT,o.message="Missing trackingEvents property. Can not track event '"+d+"'"+r(c),p(this,o),Error("Abort execution")}if(l(d)&&j.apply(this,[c,"interaction",a,a,!0]),k(d)||"error"===d){"progress"===d.toLowerCase()?i.apply(this,[c,m]):h.apply(this,[c,d,e]);var q;switch(d.toLowerCase()){case"complete":q=1;break;case"thirdquartile":q=2;break;case"midpoint":q=3;break;case"firstquartile":q=4;break;default:q=999}q<v.length&&g.apply(this,[q,c,e])}else if(c.trackingEvents.hasOwnProperty(d))if(w[d])if(c.trackingEvents[d].blocked){if("interaction"!==d&&!n){var o=new b.LogItem;throw o.source=b.LogItem.SourceType.TRACKER,o.event=b.LogItem.EventType.ILLEGAL_OPERATION,o.message="Can not track '"+d+"' more than once"+r(c),p(this,o),Error("Abort execution")}}else c.trackingEvents[d].blocked=!0,f.apply(this,[c.trackingEvents[d].urls,e]);else f.apply(this,[c.trackingEvents[d].urls,e]);else if(!n){var o=new b.LogItem;throw o.source=b.LogItem.SourceType.TRACKER,o.event=b.LogItem.EventType.INVALID_ARGUMENT,o.message="Custom tracking event '"+d+"' does not exist"+r(c),p(this,o),Error("Abort execution")}}function k(a){for(var c in b.tracking.Tracker.AdEventType)if(b.tracking.Tracker.AdEventType[c].toLowerCase()===a.toLowerCase())return!0;for(var c in b.tracking.Tracker.CreativeEventType)if(b.tracking.Tracker.CreativeEventType[c].toLowerCase()===a.toLowerCase())return!0;return!1}function l(a){for(var b in t)if(t[b].toLowerCase()===a.toLowerCase())return!0;return!1}function m(a){a.hasOwnProperty("parentSlot")&&a.parentSlot instanceof b.adresponse.Slot&&a.parentSlot.hasOwnProperty("trackingEvents")&&a.parentSlot.trackingEvents.hasOwnProperty("slotStart")&&!a.parentSlot.trackingEvents.slotStart.blocked&&(a.parentSlot.trackingEvents.slotStart.blocked=!0,f.apply(this,[a.parentSlot.trackingEvents.slotStart.urls]))}function n(a,error,d){var e=a,f=!1;if((a instanceof b.adresponse.LinearCreative||a instanceof b.adresponse.NonLinearCreative)&&(f=!0,e=a.parentAd),d)if(o("error",e)===!0){var g=new b.LogItem;g.source=b.LogItem.SourceType.TRACKER,g.event=b.LogItem.EventType.ILLEGAL_OPERATION,g.message="Can not track 'error' more than once"+r(a),f&&(g.message=g.message.substring(0,g.message.length-1)+"'s parent ad."),p(this,g)}else if(o("impression",e)===!0){var g=new b.LogItem;g.source=b.LogItem.SourceType.TRACKER,g.event=b.LogItem.EventType.ILLEGAL_OPERATION,g.message="Can't track error since 'impression' has already been tracked for this item.",p(this,g)}else m.apply(this,[e]),j.apply(this,[a,"error",d]),c(e);else if(s.resolveErrorCode(new b.adresponse.Ad,error)||s.resolveErrorCode(new b.adresponse.LinearCreative,error)||s.resolveErrorCode(new b.adresponse.NonLinearCreative,error)){var g=new b.LogItem;g.source=b.LogItem.SourceType.TRACKER,g.event=b.LogItem.EventType.INVALID_ARGUMENT,g.message="Error type not supported for "+q(a)+".",p(this,g)}else{var g=new b.LogItem;g.source=b.LogItem.SourceType.TRACKER,g.event=b.LogItem.EventType.INVALID_ARGUMENT,g.message="Error type not supported.",p(this,g)}}function o(a,b){return b&&b.hasOwnProperty("trackingEvents")&&b.trackingEvents.hasOwnProperty(a)&&b.trackingEvents[a].hasOwnProperty("blocked")&&b.trackingEvents[a].blocked===!0}function p(a,b){for(var c=0;c<a._logCallbacks.length;++c)a._logCallbacks[c](b)}function q(a){return a instanceof b.adresponse.Ad?"Ad":a instanceof b.adresponse.LinearCreative?"LinearCreative":a instanceof b.adresponse.NonLinearCreative?"NonLinearCreative":a instanceof b.adresponse.Companion?"Companion":""}function r(a){return a instanceof b.adresponse.Ad?" on the passed Ad Object.":a instanceof b.adresponse.LinearCreative?" on the passed LinearCreative Object.":a instanceof b.adresponse.NonLinearCreative?" on the passed NonLinearCreative Object.":a instanceof b.adresponse.Companion?" on the passed Companion Object.":""}var s=function(){return this instanceof b.tracking.Tracker?void(this._logCallbacks=[]):new s},t={MUTE:"mute",UNMUTE:"unmute",PAUSE:"pause",REWIND:"rewind",RESUME:"resume",ACCEPT_INVITATION:"acceptinvitation",EXPAND:"expand",FULLSCREEN:"fullscreen",COLLAPSE:"collapse",EXIT_FULLSCREEN:"exitfullscreen",CLICKTHROUGH:"clickthrough",CLOSE:"close"},u={xmlParsingError:100,vastValidationError:101,vastResponseError:102,adTypeNotSupportedError:200,adLinearityError:201,wrapperError:300,wrapperTimeoutError:301,noVASTResponseError:303,generalLinearError:400,linearMediaFileNotFoundError:401,mediaFileTimeoutError:402,noSupportedMediaFileFoundError:403,mediaFileDisplayError:405,generalNonlinearError:500,nonlinearMediaFileNotFoundError:502,noSupportedNonLinearResourceFoundError:503,undefinedError:900},v=["complete","thirdQuartile","midpoint","firstQuartile","start"],w={startcontent:"startcontent",impression:"impression",complete:"complete",thirdquartile:"thirdquartile",midpoint:"midpoint",firstquartile:"firstquartile",start:"start",interaction:"interaction",creativeview:"creativeview"};s.prototype.reportError=function(a,error){var c=s.resolveErrorCode(a,error);if(a instanceof b.adresponse.Ad||a instanceof b.adresponse.LinearCreative||a instanceof b.adresponse.NonLinearCreative)n.apply(this,[a,error,c]);else if(a instanceof b.adresponse.Companion){var d=new b.LogItem;d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.INVALID_ARGUMENT,d.message="Cannot report errors on Companion ads.",p(this,d)}else{var d=new b.LogItem;d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.INVALID_ARGUMENT,d.message="Not a trackable object.",p(this,d)}},s.prototype.trackEvent=function(a,c,e){if(!c||""===c){var f=new b.LogItem;return f.source=b.LogItem.SourceType.TRACKER,f.event=b.LogItem.EventType.INVALID_ARGUMENT,f.message="Event passed can not be an empty string.",void p(this,f)}if("error"===c){var f=new b.LogItem;return f.source=b.LogItem.SourceType.TRACKER,f.event=b.LogItem.EventType.INVALID_ARGUMENT,f.message='"error" must be tracked using reportError()',void p(this,f)}if(a instanceof b.adresponse.Session)try{d.apply(this,[a,c])}catch(g){}else if(a instanceof b.adresponse.Ad)if(o("error",a)===!0){var f=new b.LogItem;f.source=b.LogItem.SourceType.TRACKER,f.event=b.LogItem.EventType.ILLEGAL_OPERATION,f.message="Can not track any more events after reporting error on the passed Ad Object.",p(this,f)}else try{m.apply(this,[a]),j.apply(this,[a,c])}catch(g){}else if(a instanceof b.adresponse.LinearCreative||a instanceof b.adresponse.NonLinearCreative||a instanceof b.adresponse.Companion)if(null!=a.parentAd&&o("error",a.parentAd)===!0){var f=new b.LogItem;f.source=b.LogItem.SourceType.TRACKER,f.event=b.LogItem.EventType.ILLEGAL_OPERATION,f.message="Can not track any more events after reporting error on the passed Ad Object.",p(this,f)}else try{j.apply(this,[a,c,0,e])}catch(g){}else{var f=new b.LogItem;f.source=b.LogItem.SourceType.TRACKER,f.event=b.LogItem.EventType.INVALID_ARGUMENT,f.message="Invalid object passed. Can not track event '"+c+"'. Object passed should be of type Session, Ad, Creative or Companion.",p(this,f)}},s.prototype.addLogListener=function(a){a&&"function"==typeof a&&this._logCallbacks.push(a)};var x=/\[ERRORCODE\]|%5BERRORCODE%5D/g,y=/\[c\]|%5Bc%5D/g,z=/\[CACHEBUSTING\]|%5BCACHEBUSTING%5D/g;return s.resolveErrorCode=function(a,error){var c;if(a instanceof b.adresponse.Ad)switch(error){case b.tracking.Tracker.AdError.TYPE_NOT_SUPPORTED:c=u.adTypeNotSupportedError;break;case b.tracking.Tracker.AdError.NO_AD:c=u.noVASTResponseError;break;case b.tracking.Tracker.AdError.GENERAL_ERROR:c=u.generalLinearError}else if(a instanceof b.adresponse.LinearCreative)switch(error){case b.tracking.Tracker.CreativeError.MEDIA_FILE_NOT_FOUND:c=u.linearMediaFileNotFoundError;break;case b.tracking.Tracker.CreativeError.MEDIA_FILE_TIMEOUT:c=u.mediaFileTimeoutError;break;case b.tracking.Tracker.CreativeError.NO_SUPPORTED_MEDIA_FILE_FOUND:c=u.noSupportedMediaFileFoundError;break;case b.tracking.Tracker.CreativeError.MEDIA_FILE_DISPLAY_ERROR:c=u.mediaFileDisplayError}else if(a instanceof b.adresponse.NonLinearCreative)switch(error){case b.tracking.Tracker.CreativeError.MEDIA_FILE_NOT_FOUND:c=u.nonlinearMediaFileNotFoundError;break;case b.tracking.Tracker.CreativeError.MEDIA_FILE_TIMEOUT:c=u.mediaFileTimeoutError;break;case b.tracking.Tracker.CreativeError.NO_SUPPORTED_MEDIA_FILE_FOUND:c=u.noSupportedNonLinearResourceFoundError;break;case b.tracking.Tracker.CreativeError.MEDIA_FILE_DISPLAY_ERROR:c=u.mediaFileDisplayError}return c},s.prototype.trackTimeSpent=function(a,c){if(!(c instanceof b.adresponse.LinearCreative||c instanceof b.adresponse.NonLinearCreative)){var d=new b.LogItem;return d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.INVALID_ARGUMENT,d.message="'timeSpent' can only be tracked on LinearCreative and NonLinearCreative objects.",void p(this,d)}if(o("error",c)===!0){var d=new b.LogItem;return d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.ILLEGAL_OPERATION,d.message="Can not track any more events after reporting error on the passed creative.",void p(this,d)}if(!c.trackingEvents.hasOwnProperty("timeSpent")){var d=new b.LogItem;return d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.INVALID_ARGUMENT,d.message="Event 'timeSpent' does not exist on the passed object.",void p(this,d)}if(isNaN(a)||!isFinite(a)||0>a){var d=new b.LogItem;return d.source=b.LogItem.SourceType.TRACKER,d.event=b.LogItem.EventType.INVALID_ARGUMENT,d.message="'timeSpent' must be tracked with a positive number.",void p(this,d)}var e=new b.HTTPRequester,f="v2",g=c.trackingEvents.timeSpent.urls[0].url,h=g.slice(0,g.indexOf(f))+f,i=g.replace(y,a);i=i.replace(z,Math.floor(1e16*Math.random())),e.postRequest(h,{value:i},null,null)},s}(),b.tracking.Tracker.SessionEventType={CONTENT_START:"contentStart"},b.tracking.Tracker.AdEventType={IMPRESSION:"impression"},b.tracking.Tracker.CreativeEventType={CREATIVE_VIEW:"creativeview",START:"start",FIRST_QUARTILE:"firstquartile",MIDPOINT:"midpoint",THIRD_QUARTILE:"thirdquartile",COMPLETE:"complete",PROGRESS:"progress",CLICKTHROUGH:"clickthrough",MUTE:"mute",UNMUTE:"unmute",PAUSE:"pause",REWIND:"rewind",RESUME:"resume",FULLSCREEN:"fullscreen",EXIT_FULLSCREEN:"exitfullscreen",EXPAND:"expand",COLLAPSE:"collapse",ACCEPT_INVITATION:"acceptinvitation",CLOSE:"close",SKIP:"skip"},b.tracking.Tracker.AdError={TYPE_NOT_SUPPORTED:"adTypeNotSupportedError",NO_AD:"noAdError",GENERAL_ERROR:"generalAdError"},b.tracking.Tracker.CreativeError={MEDIA_FILE_NOT_FOUND:"mediaFileNotFoundError",MEDIA_FILE_TIMEOUT:"mediaFileTimeoutError",NO_SUPPORTED_MEDIA_FILE_FOUND:"noSupportedMediaFileFoundError",MEDIA_FILE_DISPLAY_ERROR:"mediaFileDisplayError"}}(b),window.videoplaza=b,window.videoplaza.buildDate="20151014",window.videoplaza.versionNumber="2.0.15.19.0"}(window);
//# sourceMappingURL=html5-sdk-2.0.15.19.0.min.js.map

// code pulled from:
// https://github.com/d4tocchini/customevent-polyfill
// https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent#Polyfill

if (!window.CustomEvent || typeof window.CustomEvent !== 'function') {
    var CustomEvent = function(event, params) {
        var evt;
        params = params || {
            bubbles: false,
            cancelable: false,
            detail: undefined
        };

        evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    };

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent; // expose definition to window
}

(function(window, undefined) {
	'use strict';
    var main, api,
    	doc = document,
    	CustomEvent = window.CustomEvent,
    	clickEvent = (navigator.userAgent.match(/iPad/i)) ? 'touchstart' : 'click',
    	utils = {},
    	core = {},
    	model = {},
    	view = {};
utils.helpers = (function () {
	"use strict";
	return {
	    /**
		 * query URI for params foo=bar&baz=foo
		 * return Object {foo:'bar', baz:'foo'}
		 */
		queryURI : function (qs) {
			qs = decodeURIComponent(qs.split('+').join(' '));

			var params = {}, 
				tokens,
				re = /[?&]?([^=]+)=([^&]*)/g;

			while ((tokens = re.exec(qs)) !== null) {
				params[tokens[1]] = tokens[2];
			}
			return params;
		},
		objectToLowerCase : function (obj) {
			if (!obj) {
				return;
			}
			var key, keys = Object.keys(obj),
				n = keys.length,
				newobj = {};

			while (n--) {
			 	key = keys[n];
			 	newobj[key.toLowerCase()] = obj[key];
			}
			return newobj;
		},
		findSupportedVideo : function (player, mediaFiles) {
			for(var i=0; i < mediaFiles.length; i++) {
				if (player.canPlayType(mediaFiles[i].mimeType) === 'probably' ||
					player.canPlayType(mediaFiles[i].mimeType) === 'maybe') {
					return mediaFiles[i].url;
				}
			}
			return false;
		},
		isVideo : function (domElem) {
			try {
				return domElem.tagName.toLowerCase() === 'video';
			}
			catch(e) {
				return false;
			}
		},
		isIE : function () {
			try {
				return navigator.userAgent.search(/Trident|MSIE/g) !== -1;
			}
			catch(e) {
				return false;
			}
		},
		/**
		 * validate if obj has a property
		 * @param  {[type]} obj   [description]
		 * @param  {[type]} value [description]
		 */
		inObject : function (obj, property) {
			if (!obj) {
				return false;
			}
			else if (!obj.hasOwnProperty(property)) {
				return false;
			}
			return true;
		}
	};
}());

utils.logger = (function () {
    'use strict';
    
    var isEnabbled = (utils.helpers.queryURI(document.location.search).vplog === 'true');
    //isEnabbled = true;

    function output(type, moduleName, msg, color) {
        if(!isEnabbled) {
            return;
        }

        // I want log a Object
        if (msg === undefined) {
            console.log(moduleName);
            return;
        }
        // Internet Explore
        else if (utils.helpers.isIE()) {
            console.log(type + " [module " + moduleName + "] : " + msg);
        }
        // Chrome, Safari & FF + firebug
        else {
            console.log("%c "+ type +" %c "+ moduleName, "color:#FFFFFF; background:"+ color +";", "color:#000000; background:#C8C8C8;", msg);
        }
    }

    /* ==========================================================================
     Public API
     ========================================================================== */
    return {
        log : function (moduleName, msg, color) {
            output('LOG',moduleName, msg, (color ? color : '#FF33CC'));
        },

        error : function (moduleName, msg, color) {
            output('ERROR',moduleName, msg, (color ? color : '#FF0000'));
        },

        event : function (moduleName, msg, color) {
            output('EVENT',moduleName, msg, (color ? color : '#0066FF'));
        },

        info : function (moduleName, msg, color) {
            output('INFO',moduleName, msg, (color ? color : '#009933'));
        }
    };
}());
model.events = (function () {
	'use strict';
    return {
		AD_PLUGIN_INIT 					: 'adPluginInit',
		AD_PLUGIN_READY 				: 'adPluginReady',
		AD_PLUGIN_ERROR 				: 'adPluginError',
		AD_PLUGIN_LOADED 				: 'adPluginLoaded',
		AD_REQUEST_SUCCESS 				: 'adRequestSuccess',
		AD_REQUEST_FAIL 				: 'adRequestFail',
		AD_REQUEST_THIRD_PARTY_SUCCESS 	: 'adRequestThirdPartySuccess',
		AD_REQUEST_THIRD_PARTY_FAIL 	: 'adRequestThirdPartyFail',
		AD_SKIP_BUTTON_CLICK 			: 'adSkipButtonClick'
	};
}());
model.ui = (function () {
	'use strict';
    return {
    	//top menu
    	CLOSE_BTN_IMAGE : '../../resources/skipBtn.png',
    	TOP_MENU_HEIGHT : 22,
		AD : 'Ad',
		SKIP_BUTTON : 'Skip',
		SPOT_COUNT_DOWN : 'Your video starts in',
		SPOT_COUNT_DOWN_END : 'The ad ends in',
		SINGULAR_UNIT : 'second',
		PLURAL_UNIT : 'seconds'
	};
}());
view.topMenu = (function () {
	"use strict";

	var count = 0,
		evt = model.events,
		ui = model.ui,
		secUnit = ui.PLURAL_UNIT,
		player,
		topMenu,
		countdown,
		skipBtn,
		currentAd;


	function buildTopMenu (width, height) {
		var menuDiv = doc.createElement('div');
		menuDiv.id = 'topMenu';
		menuDiv.style.backgroundColor = 'rgba(000,0,0,0.6)';
        menuDiv.style.width = '100%';
        menuDiv.style.height = ui.TOP_MENU_HEIGHT + 'px';
        menuDiv.style.position = 'absolute';
        menuDiv.style.zIndex = '10005';
        return menuDiv;
	}

	function buildCountDown () {
		var countDiv = doc.createElement('div');
        countDiv.style.cssFloat = 'left';
        countDiv.style.width = (topMenu.width - (100 + 20)) + 'px';
        countDiv.style.height = ui.TOP_MENU_HEIGHT + 'px';
		countDiv.style.padding = '5px';
        countDiv.style.color = '#FFFFFF';
        countDiv.style.font = 'normal 14px Arial, Helvetica, sans-serif';
        return countDiv;
	}

	function buildSkipBtn () {
		var btn = doc.createElement('div');
		btn.style.cssFloat = 'right';
        btn.style.width = ui.TOP_MENU_HEIGHT + 'px';
        btn.style.height = ui.TOP_MENU_HEIGHT + 'px';
        btn.style.background = 'url('+ ui.CLOSE_BTN_IMAGE +') no-repeat right top ';
        btn.style.visibility = 'hidden';
		return btn;
	}

	function onSkipBtnClick () {
		doc.dispatchEvent(new CustomEvent(evt.AD_SKIP_BUTTON_CLICK));
	}
	
	/* ==========================================================================
    	Public
    ========================================================================== */
	return {
		build : function (playerElem, ad, width, height) {
			//only create once
			if (topMenu) {
				return;
			}

			currentAd = ad.creatives[0];
			player = playerElem;
			topMenu = buildTopMenu(width, height);
            countdown = buildCountDown();
            
            if (currentAd.skipButtonMode !== "never") {
            	skipBtn = buildSkipBtn();
            	topMenu.appendChild(skipBtn);
            }

            topMenu.appendChild(countdown);
            player.appendChild(topMenu);

            var css = document.createElement("style");
			css.type = "text/css";
			css.innerHTML = "*:fullscreen {width:100%;}";
			document.body.appendChild(css);
		},
		show : function (isVisible) {
			if (isVisible) {
				player.parentNode.insertBefore(topMenu, player);
				//skipBtn.addEventListener(clickEvent, onSkipBtnClick);
			}
			else {
				topMenu.parentElement.removeChild(topMenu);
				//skipBtn.removeEventListener(clickEvent, onSkipBtnClick);
				//skipBtn.style.visibility = 'hidden';
			}
		},
		setSize : function (width, height) {
			if (width) {
				topMenu.style.width = width + 'px';
			}

			if (height) {
				topMenu.style.height = height + 'px';
			}
		},
		countdown : function (evt) {
			count = Math.floor(evt.target.duration - evt.target.currentTime) + 1;
	        secUnit = (count <= 1) ? ui.SINGULAR_UNIT : ui.PLURAL_UNIT;
	        countdown.innerHTML = ui.SPOT_COUNT_DOWN_END + ' ' + count + ' ' + secUnit;

	        //display close btn
	        if (Math.floor(evt.target.currentTime) === Math.floor(currentAd.skipOffset)) {
	            //skipBtn.style.visibility = 'visible';
	        }
		}
	};
}());
core.adRequest = (function () {
    'use strict';

	var MAX_BIT_RATE = 800, //bitrate is capped to 800, change to increase quality (and size)
		l = utils.logger,
		evt = model.events,
		adRequesterSettings,
		adSession,
		adRequester;

	function configAdRequest (config) {
		adRequesterSettings = {
			deviceContainer : (config.devicecontainer) ? config.devicecontainer : '',
			persistentId : (config.persistentid) ? config.persistentid : ''
		};

		adRequester = new videoplaza.adrequest.AdRequester(config.host, adRequesterSettings);

		adRequester.addLogListener(function(logObj){
			l.error('adRequest', logObj.message);
		});
	}

	function onAdRequestFail (msg) {
		l.error('adRequest', msg);
		doc.dispatchEvent(new CustomEvent(evt.AD_REQUEST_FAIL));
	}

	function onAdRequestSuccess (session) {
		l.log('adRequest', 'ad request success');
		l.info('adRequest', 'ad session: ');
		l.log(session);

		adSession = session;
		doc.dispatchEvent(new CustomEvent(evt.AD_REQUEST_SUCCESS));
	}

	/* ==========================================================================
		Public
	========================================================================== */
	return {
		newAdRequest : function (config, duration) {
			l.log('adRequest', 'start new ad request');

			configAdRequest(config);

			config.duration = duration || 0;

			//exclude unsupported formats
			if (config.flags) {
				config.flags.push('noskins','nooverlays');
			}
			else {
				config.flags = ['noskins','nooverlays'];
			}

			var cues = config.cuepoints || [];
			if (typeof cues ==='string') {
				config.cuepoints = cues.split(",").map(Number);
			}
			

			var contentMetadata = config,
				requestSettings = {
					height : config.height || 0,
					width : config.width || 0,
					maxBitRate : config.maxbitrate || MAX_BIT_RATE,
					referrerUrl : config.referrerurl || '',
					//linearPlaybackPositions : cues,
					insertionPointFilter : [
						videoplaza.adrequest.AdRequester.InsertionPointType.ON_BEFORE_CONTENT
					]
				};
			l.info('adRequest', 'contentMetadata: ');
			l.info(contentMetadata);
			l.info('adRequest', 'requestSettings: ');
			l.info(requestSettings);

			adRequester.requestSession(contentMetadata, requestSettings, onAdRequestSuccess, onAdRequestFail);
		},
		loadThirdPartyAd : function  (ad) {
			adRequester.requestThirdParty(ad, function(loadedAd) {
				if(loadedAd.ready){
					l.log('adRequest', 'Third party ad loaded');
					doc.dispatchEvent(new CustomEvent(evt.AD_REQUEST_THIRD_PARTY_SUCCESS));
				} else {
					l.error('adRequest', 'Third party ad fail to load');
					doc.dispatchEvent(new CustomEvent(evt.AD_REQUEST_THIRD_PARTY_FAIL));
				}
			});
		},
		getAdSession: function () {
			return adSession;
		}
	};
}());
core.adResponse = (function () {
    'use strict';

    var l = utils.logger;

    function isTimeCapped (insertionPoint) {
        if (insertionPoint.conditions[0].conditions.length > 0) {
            for (var k = 0, len = insertionPoint.conditions[0].conditions.length; k < len; k++) {
                var childCondition = insertionPoint.conditions[0].conditions[k];

                if (childCondition.name === videoplaza.adresponse.PropertyCondition.ConditionName.TIME_SINCE_LINEAR &&
                    childCondition.value > 0) {

                    var timeSinceLinearInMilliseconds = childCondition.value * 1000,
                    	now = new Date().getTime();

                    l.log("adSession", now + " < " + (linearAdLastPlayedTimestamp() + timeSinceLinearInMilliseconds));

                    if (now < linearAdLastPlayedTimestamp() + timeSinceLinearInMilliseconds) {
                        return true;
                    }
                }
            }
        }
    	return false;
    }

    function linearAdLastPlayedTimestamp () {
    	try{
    		var c = document.cookie;
    		return parseInt(c.match(/timeSinceLinear=\d{13}/)[0].substr(16,13));
    	} catch(e) {
    		l.error("could not read cookie property [timeSinceLinear]. Last played time set to 0");
    		return 0;
    	}
    }			

    /* ==========================================================================
    	Public
    ========================================================================== */
    return {
        getAds : function (session, type) {
            l.log("adSession", "start insertion point: " + type);

            var ads = [];
            for (var i = 0, len = session.insertionPoints.length; i < len; i++) {
                var insertionPoint = session.insertionPoints[i];

                // Check to see if this insertion point has the ads we are looking for
                if (insertionPoint.conditions.length === 1 &&
                    insertionPoint.conditions[0].type === type) {

                    // Look for the child condition 'timeSinceLinear' to add support for
                    // time-based frequency capping.
                    var isCapped = isTimeCapped(insertionPoint);
                    l.log("adSession", "is time capped? " + isCapped);

                    if (!isCapped) {
                        for (var j = 0, leng = insertionPoint.slots.length; j < leng; j++) {
                            ads = ads.concat(insertionPoint.slots[j].ads);
                        }
                    }
                }
            }
            return ads;
        }
    };
}());
core.adTracker = (function () {
    'use strict';

    var l = utils.logger,
        session, ad, firstQuartile, midpoint, thirdQuartile,
        duration, creative, tracker;

    function trackEvent (elm, evt) {
        l.event('adTracker','track event: ' + evt, '#FF9933');

        if (!elm || !tracker) {
            l.error('adTracker','failt to track event: ' + evt);
            return;
        }
        tracker.trackEvent(elm, evt);
    }

    function reportError (elm, evt) {
        l.event('adTracker','track error: ' + evt, '#FF9933');

        if (!elm || !tracker) {
            l.error('adTracker','fail to report error event: ' + evt);
            return;
        }
        tracker.reportError(elm, evt);
    }

    /* ==========================================================================
    	Public
    ========================================================================== */
    return {
        init : function () {
            tracker = new videoplaza.tracking.Tracker();
            tracker.addLogListener(function(logObj){
                l.error('adTracker', logObj.message);
            });
        },
        setTrackingElements : function (adSession, currentAd) {
            session = adSession;
            ad = currentAd;

            //inventory ads have no duration
            if (!ad.creatives[0]) {
                return;
            }

            creative = ad.creatives[0];
            duration = creative.duration; 
            firstQuartile = Math.round(duration * 0.25);
            midpoint = Math.round(duration * 0.50);
            thirdQuartile = Math.round(duration * 0.75);
        },
        quartile : function (evt) {
            switch (Math.round(evt.target.currentTime)) {
                case firstQuartile:
                    trackEvent(creative, videoplaza.tracking.Tracker.CreativeEventType.FIRST_QUARTILE);
                    firstQuartile = NaN; //stop tracking it twice
                    return;
                case midpoint:
                    trackEvent(creative, videoplaza.tracking.Tracker.CreativeEventType.MIDPOINT);
                    midpoint = NaN;
                    return;
                case thirdQuartile:
                    trackEvent(creative, videoplaza.tracking.Tracker.CreativeEventType.THIRD_QUARTILE);
                    thirdQuartile = NaN;
                    return;
            }
        },
        impression : function () {
            trackEvent(ad, videoplaza.tracking.Tracker.AdEventType.IMPRESSION);
        },
        start : function () {
            trackEvent(creative, videoplaza.tracking.Tracker.CreativeEventType.START);
        },
        complete : function () {
            trackEvent(creative, videoplaza.tracking.Tracker.CreativeEventType.COMPLETE);
        },
        close : function () {
            trackEvent(creative, videoplaza.tracking.Tracker.CreativeEventType.CLOSE);
        },
        content_start : function () {
            trackEvent(session, videoplaza.tracking.Tracker.SessionEventType.CONTENT_START);
        },
        clickthrough : function () {
            trackEvent(creative, videoplaza.tracking.Tracker.CreativeEventType.CLICKTHROUGH);
        },

        // error events

        no_ad : function () {
            reportError(ad, videoplaza.tracking.Tracker.AdError.NO_AD);
        },
        type_not_supported : function () {
            reportError(ad, videoplaza.tracking.Tracker.AdError.TYPE_NOT_SUPPORTED);
        },
        general_error : function () {
            reportError(ad, videoplaza.tracking.Tracker.AdError.GENERAL_ERROR);
        },
        media_file_not_found : function () {
           reportError(creative, videoplaza.tracking.Tracker.CreativeError.MEDIA_FILE_NOT_FOUND);
        },
        no_supported_media_file_found : function () {
            reportError(creative, videoplaza.tracking.Tracker.CreativeError.NO_SUPPORTED_MEDIA_FILE_FOUND);
        },
        media_file_display_error : function () {
           reportError(creative, videoplaza.tracking.Tracker.CreativeError.MEDIA_FILE_DISPLAY_ERROR);
        }
    };
}());
api = (function () {
    'use strict';
 
    var evt = model.events,
        apiCallback = {},
        config, 
        player, 
        errorMsg,
        isReady,
        isInitiated;
        
    doc.addEventListener(evt.AD_PLUGIN_ERROR, function (e) {
        errorMsg = e.detail.message;
    });
    doc.addEventListener(evt.AD_PLUGIN_READY, function (e) {
        isReady = true;
    });

    /* ==========================================================================
        PUBLIC
    ========================================================================== */
    return {
        init : function (elem, conf) {
            if (isInitiated) {
                return this;
            }
            isInitiated = true;
            player = elem;
            config = conf;
            doc.dispatchEvent(new CustomEvent(model.events.AD_PLUGIN_INIT));
            return this; 
        },
        utils : function () {
            return utils.helpers;
        },
        getConfig : function () {
            return config;
        },
        getVideoElem : function () {
            return player;
        },
        trigger : function (evt, param) {
            if (apiCallback[evt]) {
                apiCallback[evt](param);
            }
        },
        onError : function (callback) {
            apiCallback.onError = callback;
            if(!isReady) {
                callback(errorMsg);
            }
            return this;
        },
        onReady : function (callback) {
            apiCallback.onReady = callback;
            if(isReady) {
                callback();
            }
            return this;
        },
        onAdStart : function (callback) {
            apiCallback.onAdStart = callback;
            return this;
        },
        onAdCompleted : function (callback) {
            apiCallback.onAdCompleted = callback;
            return this;
        },
        onAdSlotStart : function (callback) {
            apiCallback.onAdSlotStart = callback;
            return this;
        },
        onAdSlotCompleted : function (callback) {
            apiCallback.onAdSlotCompleted = callback;
            return this;
        },
        onAdClickThrough : function (callback) {
            apiCallback.onAdClickThrough = callback;
            return this;
        },
        onAdSkipButtonClick : function(callback) {
            apiCallback.onContentCompleted = callback;
            return this;
        },
        onContentStart : function (callback) {
            apiCallback.onContentStart = callback;
            return this;
        },
        onContentCompleted : function (callback) {
            apiCallback.onContentCompleted = callback;
            return this;
        }
    };
}());
/**
 * @author Nelson Diotto
 * @date 2015.03.20
 */
main = (function () {
    'use strict';
    
    var l = utils.logger,
    	help = utils.helpers,
		evt = model.events,
		adRequest = core.adRequest,
		adResponse = core.adResponse,
		adTracker = core.adTracker,
		topMenu = view.topMenu,
		adsArr = [],
		adSlotArr = [],
		pluginInterface,
		player,
		config,
		playerContentSrc,
		playerPoster,
		adSession,
		clickThroughUrl,
		adType,
		adCreative;

	l.info('main','load HTML5 SDK: ' + videoplaza.versionNumber + ' release: ' + videoplaza.buildDate);

	function displayPreroll() {
		l.log('main', 'start preroll');
		selectAdType(videoplaza.adrequest.AdRequester.InsertionPointType.ON_BEFORE_CONTENT);
	}

	/*
		2. get specific ad type (pre, mid or post-roll) from the ad response
	 */
	function selectAdType (type) {
		adType = type;
		adsArr = adResponse.getAds(adSession, adType);
		adSlotArr = adsArr.slice(); //clone array
		checkAvailableAds(true);
	}

	/*
		3. check if there available ads to play in the ad response
	 */
	function checkAvailableAds (isFirstAd) {
		if (!isFirstAd) {
			adsArr.shift(); //remove previous ad from array
		}

		l.info('main', 'available ads: ' + adsArr.length + ' type: ' + adType);

		if(adsArr.length > 0 ){
			checkAdReady();
		} else {
			onAdSlotCompleted();
		}
	}

	/*
		4. check if ad is ready
	 */
	function checkAdReady () {
		var isReady = adsArr[0].ready;
		l.log('main', 'ad is ready? ' + isReady);

		if(isReady) {
			ckeckAdType();
		} else {
			adRequest.loadThirdPartyAd(adsArr[0]);
		}
	}

	/*
		5. check if the ad is the correct type
	 */
	function ckeckAdType () {
		l.log('main', 'ad type: ' + adsArr[0].type);

		adTracker.setTrackingElements(adSession, adsArr[0]);

		if (adsArr[0].type === 'inventory') {
			adTracker.no_ad();
			checkAvailableAds();
		}
		else {
			startAd();
		}
	}

	/*
		6. If ad is the correct type, start ad playback 
	 */
	function startAd () {
		l.log('main', 'ad id: ' + adsArr[0].id);

		adCreative = adsArr[0].creatives[0];
		var videoAd = help.findSupportedVideo(player, adCreative.mediaFiles);

		if (videoAd) {
			topMenu.build(player, adsArr[0], config.width, config.height);
			playVideoAsset(videoAd, onAdStart);
			onAdSlotStart();
		}
		else {
			l.error('main', 'not supported media file found');
			adTracker.no_supported_media_file_found();
			checkAvailableAds();
		}
	}

	function startContent() {
		playVideoAsset(playerContentSrc, onContentStart);
	}

	function resetPlayer () {
		l.log('main', 'reset player ' + playerContentSrc +" | "+ playerPoster);
		player.src = playerContentSrc;
		player.poster = playerPoster;

		//stop player restarting after postroll if autoplay
		if (player.autoplay) {
			player.addEventListener('loadstart', function onLoadstart() {
				this.removeEventListener('loadstart', onLoadstart);
				player.pause();
			});
		}

		// comment below to stop showing ads on re-play
		player.addEventListener('play', onPlay);
	}

	function playVideoAsset (src, callback) {
		l.log('main', 'play video: ' + src);

		if (!src) {
			l.error('main', 'undefined video src');
			return;
		}
		player.src = src;

		player.addEventListener('error', onAdStartError);
		player.addEventListener('loadstart', function onLoadstart() {
			this.removeEventListener('loadstart', onLoadstart);

			player.addEventListener('canplaythrough', callback);
			player.poster = '';
			player.play();
		});
	}

	function enableClickThrough (enable) {
		if (!enable) {
			player.removeEventListener(clickEvent, onAdClickThrough);
			return;
		}
		clickThroughUrl = adCreative.clickThroughUrl;

		if (clickThroughUrl !== '') {
			player.addEventListener(clickEvent, onAdClickThrough);
		}
		l.log('main', 'enable  clickThrough: ' + enable + ' url: ' + clickThroughUrl);
	}
	/* ==========================================================================
		Callbacks
	========================================================================== */
	/*
		1. as soon the content start playing, pause and make a new
		ad request
	 */
	function onPlay() {
		l.event('main', 'onPlay');
		player.removeEventListener('play', onPlay);
		player.pause();

		//fix for IE returning currentSrc null
		if (help.isIE()) {
			playerContentSrc = player.getAttribute("src") || player.children[0].getAttribute("src");
		}
		else {
			playerContentSrc = player.currentSrc;
		}

		playerPoster = player.poster;
		adRequest.newAdRequest(config, player.duration, player.height, player.width);
	}

	function onAdPluginInit () {
		l.event('main', 'onAdPluginInit');
		config = help.objectToLowerCase(api.getConfig());
		player = api.getVideoElem();

		//FAIL: client host is missing
        if(!help.inObject(config, 'host')) {
        	onAdPluginError('invalid plugin config Object');
            return;
        }
        //FAIL: <video> element is not correct
        else if(!help.isVideo(player)) {
        	onAdPluginError('invalid <video> DOM element');
            return;
        }
        //SUCCESS: start plugin
        onAdPluginReady();
	}

	function onAdPluginError(message) {
		l.error('main', 'onAdPluginError');
        doc.dispatchEvent(new CustomEvent(evt.AD_PLUGIN_ERROR, { 
            'detail': {
                'message' : message
            }
        }));
	}

	function onAdPluginReady () {
		l.event('main', 'onAdPluginReady');
        doc.dispatchEvent(new CustomEvent(evt.AD_PLUGIN_READY));
		player.addEventListener('play', onPlay);
	}

	function onAdRequestFail () {
		
		l.error('main', 'onAdRequestFail');
		api.trigger('onError','Ad Request Fail');
		//resetPlayer();
		startContent();
		
	}

	function onAdRequestSuccess () {
		l.event('main', 'onAdRequestSuccess');
		adSession = adRequest.getAdSession();
		adTracker.init();
		if (adSession) {
			displayPreroll();
		}
		else {
			l.error('main', 'session undefined');
		}
	}

	function onAdStart () {
		l.event('main', 'onAdStart');
		player.removeEventListener('canplaythrough', onAdStart);
		player.addEventListener('ended', onAdCompleted);
		player.addEventListener('timeupdate', onAdTimeUpdate);
		doc.addEventListener(evt.AD_SKIP_BUTTON_CLICK, onAdSkipButtonClick);
		enableClickThrough(true);
		topMenu.show(true);
		adTracker.impression(adsArr[0]);
		adTracker.start();
		api.trigger('onAdStart',adsArr[0]);
	}

	function onAdStartError (evt) {
		// fix for FF firing error even when media is working
		if (!evt.target.error) {
			return;
		}

		l.error('main', 'onAdStartError ' + evt.target.error.code);
		api.trigger('onError','Fail to Start Video Ad');
		player.removeEventListener('error', onAdStartError);
		adTracker.media_file_display_error();
		player.pause();
		checkAvailableAds();
	}

	function onAdCompleted () {
		l.event('main', 'onAdCompleted');
		player.removeEventListener('ended', onAdCompleted);
		player.removeEventListener('timeupdate', onAdTimeUpdate);
		enableClickThrough(false);
		topMenu.show(false);
		adTracker.complete();
		api.trigger('onAdCompleted',adsArr[0]);
		checkAvailableAds();
	}

	function onAdSlotStart () {
		if (adSlotArr.length === adsArr.length) {
			l.event('main', 'onAdSlotStart');
			api.trigger('onAdSlotStart', adSlotArr);
		}
	}

	function onAdSlotCompleted () {
		l.event('main', 'onAdSlotCompleted');
		api.trigger('onAdSlotCompleted', adSlotArr);

		if (adType == videoplaza.adrequest.AdRequester.InsertionPointType.ON_CONTENT_END) {
			resetPlayer();
		}
		else {
			startContent();
		}
	}

	function onAdSkipButtonClick () {
		l.event('main', 'onAdSkipButtonClick');
		player.removeEventListener('ended', onAdCompleted);
		player.removeEventListener('timeupdate', onAdTimeUpdate);
		enableClickThrough(false);
		topMenu.show(false);
		adTracker.close();
		api.trigger('onAdSkipButtonClick',adsArr[0]);
		checkAvailableAds();
	}

	function onAdTimeUpdate (evt) {
		adTracker.quartile(evt);
		topMenu.countdown(evt);
	}

	function onAdClickThrough () {
		l.event('main', 'onAdClickThrough');
		enableClickThrough(false);
		window.open(clickThroughUrl);
		adTracker.clickthrough();
		api.trigger('onAdClickThrough',adsArr[0]);
	}


	function onContentStart () {
		l.event('main', 'onContentStart');
		player.removeEventListener('canplaythrough', onContentStart);
		player.addEventListener('ended', onContentCompleted);
		adTracker.content_start();
		api.trigger('onContentStart');
	}

	function onContentCompleted () {
		l.event('main', 'onContentCompleted');
		player.removeEventListener('ended', onContentCompleted);
		api.trigger('onContentCompleted');
	}

	doc.addEventListener(evt.AD_PLUGIN_INIT, onAdPluginInit);
	doc.addEventListener(evt.AD_REQUEST_SUCCESS, onAdRequestSuccess);
	doc.addEventListener(evt.AD_REQUEST_FAIL, onAdRequestFail);
	doc.addEventListener(evt.AD_REQUEST_THIRD_PARTY_SUCCESS, onAdRequestSuccess);
	doc.addEventListener(evt.AD_REQUEST_THIRD_PARTY_FAIL, onAdRequestFail);

	doc.dispatchEvent(new CustomEvent(evt.AD_PLUGIN_LOADED));
}());
    //make API public
    window.adPlugin = api;

    //broadcast API
    document.dispatchEvent(new CustomEvent(model.events.AD_PLUGIN_LOADED, { 
    	'detail': {
    		'adPlugin': api 
    	}
    }));
}(window));