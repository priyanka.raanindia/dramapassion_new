<?php 
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect_free.php");

// Protégé page !!!!!

	$dramaID = $_GET['d'];
	$epinb = $_GET['e'];
	$epiurl = $epinb;
	if($epinb < 10){
		$epinb = "0".$epinb;
	}
	
	if(isset($_GET['p']) && $_GET['p'] > 0){
		$part = $_GET['p'];
	}else{
		$part = 1;
	}
	
	$drama_tab = DramaInfo($dramaID);
	$epiInfo = EpiInfo($dramaID,$epiurl);
	$partnb = $epiInfo['Part'];
	$short = $drama_tab['shortcut'];
	$file = $drama_tab['shortcut'].$epinb."-fd".$part.'.mp4';
	
	$today = gmdate("n/j/Y g:i:s A");
	$initial_url = "http://nf09.dramapassion.com:8081/pdl/".$short."/".$file."";
	$ip = $_SERVER['REMOTE_ADDR'];
	$key = "HoYkdy83dG"; //this is also set up in WMSPanel rule
	$validminutes = 120;
	
	$str2hash = $ip . $key . $today . $validminutes;
	$md5raw = md5($str2hash, true);
	$base64hash = base64_encode($md5raw);
	$urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=$validminutes";
	$base64urlsignature = base64_encode($urlsignature);
	
	$signedurlwithvalidinterval = "$initial_url?wmsAuthSign=$base64urlsignature";
	$url_thumb = "http://nf09.dramapassion.com/thumb/".$short."/".$short.$epinb."-".$part."_thumb.jpg";
	
	
	$npart = $part +1;
	$bpart = $part -1;
	$next = "http://www.dramapassion.com/html5App/index.php?d=".$dramaID."&e=".$epiurl."&p=".$npart ; 
	$before = "http://www.dramapassion.com/html5App/index.php?d=".$dramaID."&e=".$epiurl."&p=".$bpart ; 
	
	
	
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name ='viewport' content ="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>Dramapassion</title>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="adPlugin2.js"></script>
  <style>
	body{
		background-color: black; 
		width: 100%;
		height: 100%;
		padding : 0;
		margin : 0;
	}
  #my_video{
	  width: 100%;
	  height: 80%; 
  }
  #cont_my_video{
	  width: 80%;
	  height : 100%;
	  float: left;

	  
  }
  #cont_btnD{
	  width: 10%;
	  float: left;
	  color: white;
	  text-align: right;
	  
  }
  #cont_btnG{
	  width: 10%;
	  float: left;
	  color: white;
	 
	  
  }
  #btnG{
	  margin-left: 50%;
	  margin-top: 200%;
  }
  #btnD{
	  margin-right: 50%;
	  margin-top: 200%;
  }
  .fa{
	  font-size: 40px;
	  color: white;
  }
  .fa:hover{
	  color: white;
  }
  .fa:visited{
	  color: white;
  }
  .fa:focus{
	  color: white;
  }
  #cont_info_video{
	  color: white;
	  text-align: center;
	  font-size: 20px;
	  padding-top: 10px;
  }
  #pub{
	  margin-top: 40px;
	  background-color: red;
	  width: 100%;
  }
   
  
  </style>
</head>
<body>
<div id="cont_btnG">
	<div id="btnG">
	<?php
		if($part > 1){
	?>
	<a href="<?php echo $before ; ?>"><i class="fa fa-angle-left"></i></a>
	<?php
		}
	?>
	</div>
</div>
<div id="cont_my_video">
	<video id="my_video" poster="<?php echo $url_thumb ; ?>" controls preload='none' autoplay="autoplay">
	<source id='mp4' src="<?php echo $signedurlwithvalidinterval ; ?>" controls="controls" type='video/mp4'>
	</video>
</div>
<div id="cont_btnD">
	<div id="btnD">
	<?php
		if($part < $partnb){
	?>
	<a href="<?php echo $next ; ?>"><i class="fa fa-angle-right"></i></a>
	<?php
		}	
	?>
</div>
</div>
<div style="clear: both;"></div>
<div id="cont_info_video">
	<?php
		echo 	$drama_tab['titre'].' : Partie '.$part.' sur '.$partnb;
	?>
</div>
<div id="pub">
			<script type='text/javascript'>
			<!--//<![CDATA[
				var div = document.getElementById('PutPub');
				var contentdiv = "";
			   document.MAX_ct0 ='';
			   var m3_u = (location.protocol=='https:'?'https://cas.criteo.com/delivery/ajs.php?':'http://cas.criteo.com/delivery/ajs.php?');
			   var m3_r = Math.floor(Math.random()*99999999999);
			   var url = m3_u+"zoneid=158911"+"&nodis=1"+"&cb="+ m3_r;
			   if (document.MAX_used != ',') url = url+"&exclude="+document.MAX_used;
			   url = url + (document.charset ? '&charset='+document.charset : (document.characterSet ? '&charset='+document.characterSet : ''));
			   url = url+"&loc=" + escape(window.location);
			   if (document.referrer) url = url+"&referer=" + escape(document.referrer);
			   if (document.context) url = url+"&context=" + escape(document.context);
			   if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
			       url = url+"&ct0=" + escape(document.MAX_ct0);
			   }
			   if (document.mmm_fo) url = url+"&mmm_fo=1";

			   url = "<div style='margin:auto;width:728px;'><scr"+"ipt type='text/javascript' src='"+url;
			   url = url+"'></scr"+"ipt></div>";
			   
			   var print_pub = function(){
				   document.write(url);
			   }
			   

			   
			//]]>-->
		</script>
</div>
<script>
	var hauteur_video = $("body").height();
	var hauteur_fenetre = $(window).height();
	if((hauteur_fenetre - hauteur_video) > 150){
		print_pub();
		//$("#pub").hide();
	}else{
		$("#pub").hide();
	}
	var myPlayer = document.getElementById("my_video");
	'use strict';
	var init = function (player, config) {			
    	//prevent start playback before plugin is ready
		player.controls = false;

		/**
		 * init() start and configure the ad plugin
		 * @player = the player DOM element
		 * @config = ad request configurations
		 */
		adPlugin.init(player, config)
		.onError(function (errorMsg) {
			console.log(errorMsg);
			player.controls = true;
		})
		.onReady(function () {
			console.log('ready');
			player.controls = true;
		})
		.onAdStart(function (ad) {
			console.log(ad);
			player.controls = false;
		})
		.onAdCompleted(function (ad) {
			console.log(ad);
		})
		.onContentStart(function () {
			player.controls = true;
		})
		.onAdClickThrough(function (ad) {
			player.controls = true;
		});
	};

	(function () {
		var player = document.getElementById('my_video'),
			config = {
				host: 'http://be-dramapassion.videoplaza.tv',
				tags: [''],
				category: 'HTML5',
				height : myPlayer.height,
				width : myPlayer.width
			};

		//if ad plugin not in the DOM, wait for loaded event
		if (typeof adPlugin === 'undefined') {
			document.addEventListener('adPluginLoaded', function(evt) {
				if (evt.detail) {
					init(player, config);
				}
			});
			return;
		}
		init(player, config);
	}());
</script>
</body>
</html>