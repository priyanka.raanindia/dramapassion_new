<?php 
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect_free.php");

// Protégé page !!!!!

	$dramaID = $_GET['d'];
	$epinb = $_GET['e'];
	$epiurl = $epinb;
	if($epinb < 10){
		$epinb = "0".$epinb;
	}
	
	if(isset($_GET['p']) && $_GET['p'] > 0){
		$part = $_GET['p'];
	}else{
		$part = 1;
	}
	
	$drama_tab = DramaInfo($dramaID);
	$epiInfo = EpiInfo($dramaID,$epiurl);
	$partnb = $epiInfo['Part'];
	$short = $drama_tab['shortcut'];
	$file = $drama_tab['shortcut'].$epinb."-fd".$part.'.mp4';
	
	$today = gmdate("n/j/Y g:i:s A");
	$initial_url = "http://nf09.dramapassion.com:8081/pdl/".$short."/".$file."";
	$ip = $_SERVER['REMOTE_ADDR'];
	$key = "HoYkdy83dG"; //this is also set up in WMSPanel rule
	$validminutes = 120;
	
	$str2hash = $ip . $key . $today . $validminutes;
	$md5raw = md5($str2hash, true);
	$base64hash = base64_encode($md5raw);
	$urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=$validminutes";
	$base64urlsignature = base64_encode($urlsignature);
	
	$signedurlwithvalidinterval = "$initial_url?wmsAuthSign=$base64urlsignature";
	$url_thumb = "http://nf09.dramapassion.com/thumb/".$short."/".$short.$epinb."-".$part."_thumb.jpg";
	
	
	$npart = $part +1;
	$bpart = $part -1;
	$next = "http://www.dramapassion.com/html5App/index.php?d=".$dramaID."&e=".$epiurl."&p=".$npart ; 
	$before = "http://www.dramapassion.com/html5App/index.php?d=".$dramaID."&e=".$epiurl."&p=".$bpart ; 
	
	
	
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name ='viewport' content ="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>Dramapassion</title>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="//releases.flowplayer.org/5.5.2/commercial/flowplayer.min.js"></script>
  <link rel="stylesheet" href="skin/playful.css">
  <script src="videoplaza.js"></script>
<style>
	body{
		background-color: black; 
		width: 100%;
		height: 100%;
		padding : 0;
		margin : 0;
	}
  .flowplayer{
	  width: 100%;
	  height: 80%; 
  }
  #cont_my_video{
	  width: 80%;
	  height : 100%;
	  float: left;
	  background-image: url("<?php echo $url_thumb ; ?>");
	  
  }
  #cont_btnD{
	  width: 10%;
	  float: left;
	  color: white;
	  text-align: right;
	  
  }
  #cont_btnG{
	  width: 10%;
	  float: left;
	  color: white;
	 
	  
  }
  #btnG{
	  margin-left: 50%;
	  margin-top: 200%;
  }
  #btnD{
	  margin-right: 50%;
	  margin-top: 200%;
  }
  .fa{
	  font-size: 40px;
	  color: white;
  }
  .fa:hover{
	  color: white;
  }
  .fa:visited{
	  color: white;
  }
  .fa:focus{
	  color: white;
  }
  #cont_info_video{
	  color: white;
	  text-align: center;
	  font-size: 20px;
	  padding-top: 10px;
  }
  #pub{
	  margin-top: 40px;
	  background-color: red;
	  width: 100%;
  }
   /* custom player skin */
   .flowplayer { background-color: black ;background-size: cover;}
   flowplayer .fp-controls { background-color: rgba(17, 17, 17, 1)}
   .flowplayer .fp-timeline { background-color: rgba(204, 204, 204, 1)}
   .flowplayer .fp-progress { background-color: rgba(229, 28, 99, 1)}
   .flowplayer .fp-buffer { background-color: rgba(249, 249, 249, 1)}
   .flowplayer.is-playing {background-color: black}
  
  </style>
</head>
<body>
<div class="flowplayer" id="player">
</div>


<script type="text/javascript">
$(function () { // ensure DOM is ready
 
   // this will install flowplayer into an element with id="player"
   $("#player").flowplayer({
      // one video: a one-member playlist
      playlist: [
         // a list of type-url mappings in picking order
         [
            { mp4:     "<?php echo $signedurlwithvalidinterval ; ?>" }
         ]
      ],
      ratio: 3/4,   // video with 4:3 aspect ratio
      splash: true  // a splash setup
   });
   $("#player").flowplayer().bind("load", function (e, api, video) {
	   api.pause();
 
 	});
 
});	
</script>
</body>
</html>