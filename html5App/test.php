<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("../includes/functions.php");
require_once("../includes/serverselect_free.php");
	
$dramaID = 183;
$epinb = 1;
$epiurl = $epinb;
if($epinb < 10){
	$epinb = "0".$epinb;
}

if(isset($_GET['p']) && $_GET['p'] > 0){
	$part = $_GET['p'];
}else{
	$part = 1;
}	
	
$hashEpi = Hash_free($epinb,$dramaID,$part);	
$drama_tab = DramaInfo($dramaID);
$epiInfo = EpiInfo($dramaID,$epiurl);
$partnb = $epiInfo['Part'];
$short = $drama_tab['shortcut'];
$file = $drama_tab['shortcut'].$epinb;
$url_free_epi = "http://nf04.dramapassion.com/".$hashEpi;
$url_thumb = "http://nf04.dramapassion.com/thumb/".$short."/".$file."-".$part."_thumb.jpg";
		
	
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name ='viewport' content ="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>Dramapassion</title>
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="//releases.flowplayer.org/5.5.2/commercial/flowplayer.min.js"></script>
  <script src="http://cdn.stickyadstv.com/plugins/flowplayer/flowplayer-plugin.js"></script>  
  
</head>
<body>
	<div id="cont_my_video" class="flowplayer no-mute no-volume" data-key="$491419316258115" data-embed="false"  data-keyboard="false" >
	<video id="my_video" class=" no-mute no-volume" >
		<source type="video/mp4"  src="<?php echo $url_free_epi ; ?>">
	</video>
	<script type="application/json" class="sticky-flowplayer-config">
      {
        "ads":{
            "zones":{
                "web":{
                    "preroll":"1845"
                },
                "mobile":{
                    "preroll":"1845"
                }            
            }
        }
      }
      </script>
</div>
</body>
</html>