flowplayer(function (player, root) 
{
    var playerReadyArgs, playerReadyCallbacks = [];
    
    function onPlayerReady(callback)
    {
        if (playerReadyArgs)
        {
            callback.apply(null,playerReadyArgs);     
        }
        else
        {
            playerReadyCallbacks.push(callback);
        }
    }
    
    root.bind('ready',function(e, player, video)
    {
        root.unbind("ready",arguments.callee);
        
        playerReadyArgs = arguments;
        
        for(var i = 0; i < playerReadyCallbacks.length; ++ i)
        {
            playerReadyCallbacks[i].apply(null,playerReadyArgs);
        }
    });

    //Permit to be first to receive events on the listener list
    $.fn.bindFirst = function(/*String*/ eventType, /*[Object])*/ eventData, /*Function*/ handler) {
        var indexOfDot = eventType.indexOf(".");
        var eventNameSpace = indexOfDot > 0 ? eventType.substring(indexOfDot) : "";

        eventType = indexOfDot > 0 ? eventType.substring(0, indexOfDot) : eventType;
        handler = handler == undefined ? eventData : handler;
        eventData = typeof eventData == "function" ? {} : eventData;

        return this.each(function() {
            var $this = $(this);
            var currentAttrListener = this["on" + eventType];

            if (currentAttrListener) {
                $this.bind(eventType, function(e) {
                    return currentAttrListener(e.originalEvent); 
                });

                this["on" + eventType] = null;
            }

            $this.bind(eventType + eventNameSpace, eventData, handler);

            var allEvents = $this.data("events") || $._data($this[0], "events");
            var typeEvents = allEvents[eventType];
            var newEvent = typeEvents.pop();
            typeEvents.unshift(newEvent);
        });
    };
    
    var protocol = ("https:" == document.location.protocol) ? "https:" : "http:";
    
    //exec init() when the JS file is loaded.
    addScriptOnHead(protocol + "//cdn.stickyadstv.com/mustang/mustang.min.js",setupMustang);
    
    function getConfig()
    {
        //Flowplayer is implemented by jQuery
        if(typeof(player.conf.stickyFlowplayerConfig) != "undefined")
        {
            return player.conf.stickyFlowplayerConfig;
        }
        //Flowplayer is implemented by html5.
        else
        {
          //The sticky script tag with config
            var configJSON = root.find(".sticky-flowplayer-config").text();
            
            //Return JS object
            return (configJSON) ? JSON.parse(configJSON) : null ;
        }
        
    }
    
    function getVideoTag()
    {
        if(player.engine == "flash")
        {
            return root[0];
        }
        else
        {
            //HTML video tag
            return root.find("video").get(0);
        }
        
    }
    
    function displayControls(isDisplay)
    {
        if(player.engine == "flash")
        {
            if(isDisplay)
            {
                root.find(".fp-ratio").show();
                $(".fp-engine").css("width","");
                $(".fp-engine").css("height","");
                root.css("height","");
            }
            else
            {
                root.css("height",root.find(".fp-engine").get(0).clientHeight + "px");
                root.find(".fp-ratio").hide();
                $(".fp-engine").css("width","1px");
                $(".fp-engine").css("height","1px");
            }
        }

        // the flowplayer UI HTMLelement
        var videoTag = root.find(".fp-ui");
        
        if(isDisplay)
        {
            videoTag.show();
        }
        else
        {
            videoTag.hide();
        }
    }
    
    //add the script tag to the dom
    function addScriptOnHead(url,callback)
    {
        var script = document.createElement('script');
         
        script.type = "text/javascript";

        script.addEventListener("load",function(e)
        {
            callback();
        });
        
        script.src = url;
        
        var firstDomScript = document.getElementsByTagName('script')[0];
         
        firstDomScript.parentNode.insertBefore(script, firstDomScript);
    }

    function setupMustang()
    {
        var playingAd = false;
        
        //Ad config
        var config = getConfig();
        
        if (!config)
        {
            throw new Error("config is not defined.");
        }
        else if(typeof config.ads != "object")
        {
            throw new Error("'ads' property missing.");
        }
        else if(typeof config.ads.zones != "object")
        {
            throw new Error("'zones' property missing.");
        }
        else if(typeof config.ads.zones.web != "object")
        {
            throw new Error("'web' property missing.");
        }
        
        config = buildMustangConfig(config);
        
        adsComponent = new com.stickyadstv.AdsComponent(config);
        
        var setupDone = false;
        
        onPlayerReady(function(e, player, video)
        {
            if (setupDone)
            {
                return;
            }
            
            setupDone = true;

            var play = player.play;
            
            player.play = function()
            {
                player.play = play;
            }
            
            var videoTag = getVideoTag();

            if (!videoTag)
            {
                throw new Error("Unable to find flowplayer video tag");
            }
            
            adsComponent.play(videoTag);
        });
        
        //cancel error dispatching to the player if it concern mustang.
        function cancelError(e)
        {
            e.stopImmediatePropagation();
            
            e.preventDefault();
        }
        
        if(player.engine == "html5")
        {
            adsComponent.on(com.stickyadstv.AdsComponent.AD_START,function(e)
            {
                displayControls(false);
            });
        }
        
        adsComponent.on(com.stickyadstv.AdsComponent.AD_CREATE,function(e)
        {
            root.bindFirst("error",cancelError);
        });
        
        adsComponent.on(com.stickyadstv.AdsComponent.AD_LOADED,function(e)
        {
            playingAd = true;
        });
        
        adsComponent.on(com.stickyadstv.AdsComponent.AD_COMPLETE,function(e)
        {
            displayControls(true);
            
            playingAd = false;
            
            root.unbind("error",cancelError);
        });    
        
        adsComponent.on(com.stickyadstv.AdsComponent.REQUEST_PLAY,function(e)
        {
            setTimeout(function() {
                player.resume();
            },300);
        });
        
        adsComponent.on(com.stickyadstv.AdsComponent.NOTIFY_ADS_COMPLETE ,function(e)
        {
            player.unbind("finish");
        });
        
        adsComponent.on(com.stickyadstv.AdsComponent.REQUEST_PAUSE,function(e)
        {
            if (!player.loading)
            {
                player.pause();
                
                displayControls(false);
            }
            else
            {
                adsComponent.onPlayerStateChanged(com.stickyadstv.AdsComponent.PLAYER_PAUSE);
            }
        });
        
        player.bind("resume", function(e, player, video) 
        {
            if (playingAd) { return; }
            
            adsComponent.onPlayerStateChanged(com.stickyadstv.AdsComponent.PLAYER_PLAY);
        });
        
        player.bind("pause", function(e, player, video) 
        {
	        
            if (playingAd) { return; }
            
            adsComponent.onPlayerStateChanged(com.stickyadstv.AdsComponent.PLAYER_PAUSE);
        });
        
        player.bind("finish", function(e, player, video) 
        {
            if (playingAd) { return; }
            
            if((typeof(config.ads.zones.postroll) != "undefined") && (config.ads.zones.postroll != -1))
                displayControls(false);
            
            adsComponent.onPlayerStateChanged(com.stickyadstv.AdsComponent.PLAYER_STOP);
        });
        
        player.bind("volume", function(e, player, video) 
        {
            if (playingAd) { return; }
            
            adsComponent.onVolumeLevelChanged((player.engine == "flash") ? player.volumeLevel : player.getVolume());
        });
    }
    
    function buildMustangConfig(config)
    {
        var userAgent = navigator.userAgent;
        var isIpad = /iPad|MeeGo/.test(userAgent);
        var isIphone = /iP(hone|od)/i.test(userAgent) && !/iPad/.test(userAgent);
        var isAndroid = /Android/.test(userAgent);
        var isMobile = isIpad || isIphone || isAndroid;
        
        var zones = config.ads.zones.web;
        
        if((typeof(config.ads.zones.mobile) != "undefined") && isMobile)
        {
            //Replace web zones by mobile.
            zones = config.ads.zones.mobile;
        }
        
        //don't display midroll and postroll on IOS
        if(isIphone || isIpad)
        {
            delete zones["midroll"];
            delete zones["postroll"];
        }
        
        config.ads.zones = zones;
        
        var ads = config.ads;
        
        ads.environment = ads.environment || {};
        
        ads.environment.flashEnabled = ads.environment.flashEnabled || false;
        
        return config;
    }
});