<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$dramaID = cleanup($_GET['dramaID']);
	$userConnect = UserIsConnect();
	$com_id = cleanup($_GET['com_id']);
	$erreur = cleanup($_GET['erreur']);
	
	if($erreur == 1){
		$erreur = "Vous devez sélectionner une catégorie pour signaler ce commentaire.";
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_coms.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />


</head>
<body>
	<div id="cont_iframe_signaler">
	
		<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
			<div id="cont_iframe_tot">
			<div id="titre_iframe_signaler">
			<span>Signaler</span>
			</div>
			<div id="logo_iframe_signaler">
			<img src="<?php echo $http ;?>images/pop_logo.png">
			</div>
			</div>
		</div>
	</div>
<?php if($userConnect == 0){  
$header = 'Location: '.$http.'connexion.php?variable=1' ;
header($header);

}else{?>
<div id="cont_cont_iframe">
	<div id="cont_coms" style="width:600px;">
	<div class="div_center" style="width:400px;">
	<span class="fancy_erreur"><?php echo $erreur ; ?></span>
	Veuillez séléctionner l'objet du signalement : <br /><br />
	<form method="POST" action="<?php echo $http ; ?>signaler_coms_traitement.php">
	<div class="cont_liste" style="display:block;width:400px;clear:both;">
		<?php 
		$type = "tab_signaler";
						echo'<div class="all_input" onclick="showHideSelect(\'selectID_'.$type.'\')" style="float:left;">';
							echo'<div class="inputsSelect"   >';
								$type_lien = "lien_".$type;
								echo'<p class="selects"  id="select_'.$type.'" >Choisissez</p>';
								echo'<ul id="selectID_'.$type.'"  >';
								echo '<li><a href="javascript:void(0)"  onclick="validAndHide(\'1\', this, \''.$type_lien.'\', \'select_'.$type.'\')">Propos violents et agressifs</a></li>';
								echo '<li><a href="javascript:void(0)"  onclick="validAndHide(\'2\', this, \''.$type_lien.'\', \'select_'.$type.'\')">Spoiler</a></li>';
								echo '<li><a href="javascript:void(0)"  onclick="validAndHide(\'3\', this, \''.$type_lien.'\', \'select_'.$type.'\')">Liens vers vidéos illégales</a></li>';
								echo '<li><a href="javascript:void(0)"  onclick="validAndHide(\'4\', this, \''.$type_lien.'\', \'select_'.$type.'\')">Autre</a></li>';

								echo'</ul>';
		
							echo'</div><img src="'.$http.'images/submit_liste.png" style="cursor:pointer;">';
							echo'<input type="hidden" name="'.$type_lien.'" id="'.$type_lien.'" />';
						echo'</div>';
		
		?>
		</div>
		<br /><br /><br />
		<input type="hidden" name="dramaID" value="<?php echo $dramaID ; ?>" />
		<input type="hidden" name="com_id" value="<?php echo $com_id ; ?>" />
		<div class="cont_send_bt" style="display:block;width:400px;clear:both;">
		<input src="<?php echo $http ; ?>images/envoyer.png" type="image" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/annuler.png" ></a>
		</div>
	</form>
	</div>
	</div>
</div>	
<?php } ?>      
            
		

</form>
<script>
function showHideSelect(select)
{
    var objSelect = document.getElementById(select);
    objSelect.style.display = (objSelect.style.display == 'block') ? 'none' : 'block';
}

function validAndHide(txt, obj, input, select)
{
    
	document.getElementById(input).value = txt;
	var parent = obj.parentNode.parentNode;
	document.getElementById(select).innerHTML = obj.innerHTML;
	
	
	
	showHideSelect(parent);
	
}
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
function fermer(){
	window.parent.$.fancybox.close();
}

</script>
</body>
</html>