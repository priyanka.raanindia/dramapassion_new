<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	
	require_once("mama_top.php");
	$idfp = "$491419316258115";
	
	 $service='hls://stream150300/playlist.m3u8';
	 $serviceID='hls://stream150400/playlist.m3u8';
	 
	 
	 
	 function cdnapicall($serviceid) {
        $apiurl='http://sstv00.api.customized-cdn.net/api/?url='.$serviceid.'&ip='.$_SERVER['REMOTE_ADDR'];
        $jsonapi=file_get_contents($apiurl);
        $jsonarr=json_decode($jsonapi, true);
        
        return($jsonarr);
    }
    
     $aRET=cdnapicall($service);
	
?>
	
	<tr>
		<td valign="top">  
			<div id="">
				<div style="width:1000px;margin:auto;" >
					<div data-live="true" data-ratio="0.5625" class="flowplayer fixed-controls">
					   <video data-title="Live stream">
					   		<source type="application/x-mpegurl" src="<?php echo $aRET['url'] ; ?>">
					   </video>
					</div>
				</div>
			</div><br />
			<div id="free_pub_head" style="width:728px;margin:auto;margin-top:15px;margin-bottom:15px;">
			<?php //AffPub(5) ;?>
			<!-- criteo leaderboard -->
			<script type='text/javascript'>
			<!--//<![CDATA[
			   document.MAX_ct0 ='';
			   var m3_u = (location.protocol=='https:'?'https://cas.criteo.com/delivery/ajs.php?':'http://cas.criteo.com/delivery/ajs.php?');
			   var m3_r = Math.floor(Math.random()*99999999999);
			   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
			   document.write ("zoneid=158911");document.write("&amp;nodis=1");
			   document.write ('&amp;cb=' + m3_r);
			   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
			   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
			   document.write ("&amp;loc=" + escape(window.location));
			   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
			   if (document.context) document.write ("&context=" + escape(document.context));
			   if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
			       document.write ("&amp;ct0=" + escape(document.MAX_ct0));
			   }
			   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
			   document.write ("'></scr"+"ipt>");
			//]]>--></script>
		</div>
			<div id="mama_nomine">
				<h2>Nominés</h2>
				<br />
				<div id="nomines">
					<h3><span class="nom_number">01</span><span class="nom_title">Révélation masculine de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/1.jpg"/>
								NCT 127
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/2.jpg"/>
								SF9
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/3.jpg"/>
								ASTRO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/4.jpg"/>
								KNK
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/5.jpg"/>
								PENTAGON
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">02</span><span class="nom_title">Révélation féminine de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/6.jpg"/>
								BLACKPINK
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/7.jpg"/>
								gugudan
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/8.jpg"/>
								Bolbbalgan4
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/9.jpg"/>
								I.O.I
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/10.jpg"/>
								WJSN
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">03</span><span class="nom_title">Groupe masculin de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/11.jpg"/>
								EXO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/12.jpg"/>
								iKON
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/13.jpg"/>
								BTS
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/14.jpg"/>
								Block B
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/15.jpg"/>
								SHINee
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/16.jpg"/>
								INFINITE
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">04</span><span class="nom_title">Groupe féminin de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/17.jpg"/>
								TWICE
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/18.jpg"/>
								Red Velvet
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/19.jpg"/>
								MAMAMOO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/20.jpg"/>
								GFRIEND
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/21.jpg"/>
								Wonder Girls
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">05</span><span class="nom_title">Artiste masculin de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/22.jpg"/>
								PARK HYO SHIN
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/23.jpg"/>
								PSY
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/24.jpg"/>
								Lim Chang Jung
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/25.jpg"/>
								ZICO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/26.jpg"/>
								CRUSH
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">06</span><span class="nom_title">Artiste féminine de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/27.jpg"/>
								Baek A Yeon
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/28.jpg"/>
								Ailee
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/29.jpg"/>
								LEE HI
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/30.jpg"/>
								Jeong Eun Ji
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/31.jpg"/>
								TAEYEON
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">07</span><span class="nom_title">Chorégraphie solo de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/32.jpg"/>
								Free Somebody
								<br /><span class="nom_art_sous">LUNA</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/33.jpg"/>
								Find Me
								<br /><span class="nom_art_sous">Jun Hyo Seong</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/34.jpg"/>
								Press Your Number
								<br /><span class="nom_art_sous">TAEMIN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/35.jpg"/>
								I Just Wanna Dance
								<br /><span class="nom_art_sous">TIFFANY</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/36.jpg"/>
								How's this?
								<br /><span class="nom_art_sous">HyunA</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">08</span><span class="nom_title">Chorégraphie de groupe masculin de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/37.jpg"/>
								Monster
								<br /><span class="nom_art_sous">EXO</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/38.jpg"/>
								Hard Carry
								<br /><span class="nom_art_sous">GOT7</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/39.jpg"/>
								All in
								<br /><span class="nom_art_sous">MONSTA X</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/40.jpg"/>
								Blood Sweat & Tears
								<br /><span class="nom_art_sous">BTS</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/41.jpg"/>
								Fantasy
								<br /><span class="nom_art_sous">VIXX</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/42.jpg"/>
								Pretty U
								<br /><span class="nom_art_sous">SEVENTEEN</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">09</span><span class="nom_title">Chorégraphie de groupe féminin de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/43.jpg"/>
								Good Luck
								<br /><span class="nom_art_sous">AOA</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/44.jpg"/>
								CHEER UP
								<br /><span class="nom_art_sous">TWICE</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/45.jpg"/>
								Russian Roulette
								<br /><span class="nom_art_sous">Red Velvet</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/46.jpg"/>
								I Like That
								<br /><span class="nom_art_sous">SISTAR</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/47.jpg"/>
								Rough
								<br /><span class="nom_art_sous">GFRIEND</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">10</span><span class="nom_title">Performance vocale masculine de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/48.jpg"/>
								D (half moon)
								<br /><span class="nom_art_sous">DEAN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/49.jpg"/>
								Good For You
								<br /><span class="nom_art_sous">Eric Nam</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/50.jpg"/>
								The Love That I Committed
								<br /><span class="nom_art_sous">Lim Chang Jung</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/51.jpg"/>
								Fallen in Love (Only with You)
								<br /><span class="nom_art_sous">JANG BEOM JUNE</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/52.jpg"/>
								Don't Forget
								<br /><span class="nom_art_sous">CRUSH</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">11</span><span class="nom_title">Performance vocale féminine de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/53.jpg"/>
								So-So
								<br /><span class="nom_art_sous">Baek A Yeon</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/54.jpg"/>
								Across The Universe
								<br /><span class="nom_art_sous">Yerin Baek</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/55.jpg"/>
								if You
								<br /><span class="nom_art_sous">Ailee</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/56.jpg"/>
								Hopefully sky
								<br /><span class="nom_art_sous">Jeong Eun Ji</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/57.jpg"/>
								Rain
								<br /><span class="nom_art_sous">TAEYEON</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">12</span><span class="nom_title">Performance vocale de groupe de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/58.jpg"/>
								Beside me
								<br /><span class="nom_art_sous">DAVICHI</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/59.jpg"/>
								You're the best
								<br /><span class="nom_art_sous">MAMAMOO</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/60.jpg"/>
								Ribbon
								<br /><span class="nom_art_sous">BEAST</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/61.jpg"/>
								Remember that
								<br /><span class="nom_art_sous">BTOB</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/62.jpg"/>
								I Don't Love You
								<br /><span class="nom_art_sous">Urban Zakapa</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">13</span><span class="nom_title">Performance de groupe de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/63.jpg"/>
								What The Spring??
								<br /><span class="nom_art_sous">10cm</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/64.jpg"/>
								YOU'RE SO FINE
								<br /><span class="nom_art_sous">CNBLUE</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/65.jpg"/>
								Letting Go
								<br /><span class="nom_art_sous">DAY6</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/66.jpg"/>
								Take Me Now
								<br /><span class="nom_art_sous">FTISLAND</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/67.jpg"/>
								Pulse
								<br /><span class="nom_art_sous">Guckkasten</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">14</span><span class="nom_title">Performance rap de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/68.jpg"/>
								Lonely Night
								<br /><span class="nom_art_sous">GARY</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/69.jpg"/>
								1llusion
								<br /><span class="nom_art_sous">DOK2</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/70.jpg"/>
								Sour Grapes
								<br /><span class="nom_art_sous">San E, Mad Clown</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/71.jpg"/>
								puzzle
								<br /><span class="nom_art_sous">Cjamm, BewhY</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/72.jpg"/>
								I Am You, You Are me
								<br /><span class="nom_art_sous">ZICO</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">15</span><span class="nom_title">Collaboration de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/73.jpg"/>
								HIT ME
								<br /><span class="nom_art_sous">MOBB</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/74.jpg"/>
								Inferiority Complex
								<br /><span class="nom_art_sous">Park Kyung, Eunha</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/75.jpg"/>
								No Matter What
								<br /><span class="nom_art_sous">BoA, Beenzino</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/76.jpg"/>
								Dream
								<br /><span class="nom_art_sous">Suzy, BAEKHYUN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/77.jpg"/>
								Spring Love
								<br /><span class="nom_art_sous">Eric Nam, WENDY</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">16</span><span class="nom_title">Clip de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/78.jpg"/>
								WHISTLE
								<br /><span class="nom_art_sous">BLACKPINK</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/79.jpg"/>
								bonnie & clyde
								<br /><span class="nom_art_sous">DEAN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/80.jpg"/>
								Carnival (The Last Day)
								<br /><span class="nom_art_sous">GAIN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/81.jpg"/>
								Blood Sweat & Tears
								<br /><span class="nom_art_sous">BTS</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/82.jpg"/>
								Why So Lonely
								<br /><span class="nom_art_sous">Wonder Girls</span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">17</span><span class="nom_title">Bande originale de l'année</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/83.jpg"/>
								Moonlight Drawn by Clouds
								<br /><span class="nom_art_sous">Moonlight Drawn by Clouds</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/84.jpg"/>
								This Love
								<br /><span class="nom_art_sous">Descendants of the Sun</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/85.jpg"/>
								Like a Dream
								<br /><span class="nom_art_sous"><a class="link_drama" href="http://www.dramapassion.com/drama/228/Another-Miss-Oh">Another Miss Oh</a></span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/86.jpg"/>
								Don't Worry
								<br /><span class="nom_art_sous">Reply 1988</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/87.jpg"/>
								Reminiscence
								<br /><span class="nom_art_sous"><a class="link_drama" href="http://www.dramapassion.com/drama/216/Signal">Signal</a></span>
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">18</span><span class="nom_title">Artiste de l'année (HotelsCombined)</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/88.jpg"/>
								BLACKPINK
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/89.jpg"/>
								EXO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/90.jpg"/>
								iKON
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/91.jpg"/>
								NCT 127
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/92.jpg"/>
								SF9
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/93.jpg"/>
								TWICE
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/94.jpg"/>
								gugudan
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/95.jpg"/>
								Red Velvet
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/96.jpg"/>
								MAMAMOO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/97.jpg"/>
								PARK HYO SHIN
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/98.jpg"/>
								BTS
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/99.jpg"/>
								Baek A Yeon
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/100.jpg"/>
								Bolbbalgan4
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/101.jpg"/>
								Block B
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/102.jpg"/>
								SHINee
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/103.jpg"/>
								PSY
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/104.jpg"/>
								ASTRO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/105.jpg"/>
								I.O.I
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/106.jpg"/>
								Ailee
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/107.jpg"/>
								GFRIEND
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/108.jpg"/>
								WJSN
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/109.jpg"/>
								Wonder Girls
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/110.jpg"/>
								LEE HI
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/111.jpg"/>
								INFINITE
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/112.jpg"/>
								Lim Chang Jung
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/113.jpg"/>
								Jeong Eun Ji
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/114.jpg"/>
								ZICO
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/115.jpg"/>
								KNK
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/116.jpg"/>
								CRUSH
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/117.jpg"/>
								TAEYEON
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/118.jpg"/>
								PENTAGON
							</div>
							<div class="clear"></div>
						</div>
					<h3><span class="nom_number">19</span><span class="nom_title">Chanson de l'année (HotelsCombined)</span></h3>
						<div class="nom_content">
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/119.jpg"/>
								What The Spring??
								<br /><span class="nom_art_sous">10cm</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/120.jpg"/>
								Good Luck
								<br /><span class="nom_art_sous">AOA</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/121.jpg"/>
								YOU'RE SO FINE
								<br /><span class="nom_art_sous">CNBLUE</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/122.jpg"/>
								Letting Go
								<br /><span class="nom_art_sous">DAY6</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/123.jpg"/>
								D (half moon)
								<br /><span class="nom_art_sous">DEAN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/124.jpg"/>
								Monster
								<br /><span class="nom_art_sous">EXO</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/125.jpg"/>
								Take Me Now
								<br /><span class="nom_art_sous">FTISLAND</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/126.jpg"/>
								Hard Carry
								<br /><span class="nom_art_sous">GOT7</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/127.jpg"/>
								HIT ME
								<br /><span class="nom_art_sous">MOBB</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/128.jpg"/>
								CHEER UP
								<br /><span class="nom_art_sous">TWICE</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/129.jpg"/>
								Lonely Night
								<br /><span class="nom_art_sous">GARY</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/130.jpg"/>
								Pulse
								<br /><span class="nom_art_sous">Guckkasten</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/131.jpg"/>
								Beside Me
								<br /><span class="nom_art_sous">DAVICHI</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/132.jpg"/>
								1llusion
								<br /><span class="nom_art_sous">DOK2</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/133.jpg"/>
								Russian Roulette
								<br /><span class="nom_art_sous">Red Velvet</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/134.jpg"/>
								Free Somebody
								<br /><span class="nom_art_sous">LUNA</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/135.jpg"/>
								You're the best
								<br /><span class="nom_art_sous">MAMAMOO</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/136.jpg"/>
								All in
								<br /><span class="nom_art_sous">MONSTA X</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/137.jpg"/>
								Inferiority Complex
								<br /><span class="nom_art_sous">Park Kyung, Eunha</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/138.jpg"/>
								Blood Sweat & Tears
								<br /><span class="nom_art_sous">BTS</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/139.jpg"/>
								So-So
								<br /><span class="nom_art_sous">Baek A Yeon</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/140.jpg"/>
								Across The Universe
								<br /><span class="nom_art_sous">Yerin Baek</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/141.jpg"/>
								No Matter What
								<br /><span class="nom_art_sous">BoA, Beenzino</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/142.jpg"/>
								Ribbon
								<br /><span class="nom_art_sous">BEAST</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/143.jpg"/>
								Remember that
								<br /><span class="nom_art_sous">BTOB</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/144.jpg"/>
								Fantasy
								<br /><span class="nom_art_sous">VIXX</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/145.jpg"/>
								Sour Grapes
								<br /><span class="nom_art_sous">San E, Mad Clown</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/146.jpg"/>
								Pretty U
								<br /><span class="nom_art_sous">SEVENTEEN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/147.jpg"/>
								Dream
								<br /><span class="nom_art_sous">Suzy, BAEKHYUN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/148.jpg"/>
								I Like That
								<br /><span class="nom_art_sous">SISTAR</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/149.jpg"/>
								puzzle
								<br /><span class="nom_art_sous">Cjamm, BewhY</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/150.jpg"/>
								I Don't Love You
								<br /><span class="nom_art_sous">Urban Zakapa</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/151.jpg"/>
								Good For You
								<br /><span class="nom_art_sous">Eric Nam</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/152.jpg"/>
								Spring Love
								<br /><span class="nom_art_sous">Eric Nam, WENDY</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/153.jpg"/>
								If You
								<br /><span class="nom_art_sous">Ailee</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/154.jpg"/>
								Rough
								<br /><span class="nom_art_sous">GFRIEND</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/155.jpg"/>
								The Love That I Committed
								<br /><span class="nom_art_sous">Lim Chang Jung</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/156.jpg"/>
								Fallen in Love (Only with You)
								<br /><span class="nom_art_sous">Jang BEOM JUNE</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/157.jpg"/>
								Find Me
								<br /><span class="nom_art_sous">Jun Hyo Seong</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/158.jpg"/>
								Hopefully sky
								<br /><span class="nom_art_sous">Jeong Eun Ji</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/159.jpg"/>
								I Am You, You Are me
								<br /><span class="nom_art_sous">ZICO</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/160.jpg"/>
								Don't Forget
								<br /><span class="nom_art_sous">CRUSH</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/161.jpg"/>
								Press Your Number
								<br /><span class="nom_art_sous">TAEMIN</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/162.jpg"/>
								Rain
								<br /><span class="nom_art_sous">TAEYEON</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/163.jpg"/>
								I Just Wanna Dance
								<br /><span class="nom_art_sous">TIFFANY</span>
							</div>
							<div class="nom_art">
								<img class="img_art" src="<?php echo $http ; ?>content/mama2016/164.jpg"/>
								How's this?
								<br /><span class="nom_art_sous">HyunA</span>
							</div>
							<div class="clear"></div>
						</div>
				</div>
				<br /><br />
			</div>
		</td>
	</tr>
	
	
	
<?php	
	require_once("mama_bottom.php"); 
?>