<?php
require 'includes/class.phpmailer2.php';


$message = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Dramapassion - Renouvellement automatique désactivé</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #DADADA; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#0E7693; font-size:22px; }

    </style>
   
</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;">

    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#DADADA" >
        <tbody>
            <tr>
                <td align="center" bgcolor="#DADADA">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>                            
                            <tr>
                                <td class="w640"  width="640" height="10"></td>
                            </tr>

                            <tr>
                                <td align="center" class="w640"  width="640" height="20"></td>
                            </tr>
                            <tr>
                                <td class="w640"  width="640" height="10"></td>
                            </tr>


                            <!-- entete -->
                            <tr class="pagetoplogo">
                                <td class="w640"  width="640">
                                    <table  class="w640"  width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#F2F0F0" >
                                        <tbody>
                                            <tr>
                                                <td class="w30"  width="30"></td>
                                                <td  class="w580"  width="580" valign="middle" align="left">
                                                    <div class="pagetoplogo-content">
                                                        <img class="w580" style="text-decoration: none; display: block; color:#476688; font-size:30px;" src="http://www.dramapassion.com/images/logo.png" alt="Logo Dramapassion" />
                                                    </div>
                                                </td> 
                                                <td class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!-- separateur horizontal -->
                            <tr>
                                <td  class="w640"  width="640" height="1" bgcolor="#ffffff"></td>
                            </tr>
                             <!-- contenu -->
                            <tr class="content">
                                <td class="w640" class="w640"  width="640" bgcolor="#ffffff">
                                    <table class="w640"  width="640" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td  class="w30"  width="30"></td>
                                                <td  class="w580"  width="580">
                                                    <!-- une zone de contenu -->
                                                    <table class="w580"  width="580" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody> 
                                                        	<tr>
                                                                <td class="w580"  width="580" height="20" ></td>
                                                            </tr>                                                           
                                                            <tr>
                                                                <td class="w580"  width="580">
                                                                    <div align="left" class="article-content">
                                                                        <p><em style="font-size:12px;">Attention : ceci est un message généré automatiquement. Ne répondez pas directement à cet email car votre message ne sera pas lu. Si vous voulez nous contacter, veuillez utiliser le formulaire de contact disponible sur notre site.</em></p><br />
                                                                        <p>WOOK,<br /><br />
                                                                        	Vous venez de désactiver le renouvellement automatique de votre compte et votre demande a bien été prise en compte.<br /><br />
                                                                        	Vous pouvez vérifier le statut du renouvellement automatique dans votre espace personnel à la page suivante : <a href="http://www.dramapassion.com/compte/abonnement/">http://www.dramapassion.com/compte/abonnement/</a><br /><br />
                                                                        	Veuillez converser précieusement cet email car il sert de preuve de votre demande de désactivation du renouvellement automatique.<br /><br />
                                                                        	Si vous avez des questions, veuillez nous contacter en utilisant le formulaire de contact disponible à la page suivante : <a href="http://www.dramapassion.com/contact/">http://www.dramapassion.com/contact/</a><br /><br /><br />
                                                                        	Nous vous remercions de la confiance que vous nous témoignez.<br /><br />

																			Cordialement,<br />
																			L’équipe Dramapassion.
                                                                        
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!-- fin zone -->                                                   

                                                   
                                                </td>
                                                <td class="w30" class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!--  separateur horizontal de 15px de  haut-->
                            <tr>
                                <td class="w640"  width="640" height="15" bgcolor="#ffffff"></td>
                            </tr>
                            <tr>
                                <td class="w640"  width="640" height="60"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
';


$mail = new PHPMailer;
$mail->Hostname = 'mail.dramapassion.com';
$mail->CharSet = "UTF-8";

$mail->From = 'noreply@dramapassion.com';
$mail->FromName = 'Dramapassion';
$mail->AddAddress('drama.passion@yahoo.com');  // Add a recipient


$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
$mail->IsHTML(true);   	                              // Set email format to HTML


$subject = 'Confirmation de la désactivation du renouvellement automatique';
$subject = utf8_decode($subject);
$subject = '=?iso8859-1?B?'.base64_encode($subject).'?=';


$mail->Subject = $subject;
$mail->Body    = $message;
$mail->AltBody = 'Attention : ceci est un message généré automatiquement. Ne répondez pas directement à cet email car votre message ne sera pas lu. Si vous voulez nous contacter, veuillez utiliser le formulaire de contact disponible sur notre site.\n\n



'.$pseudo.',\n\n
Vous venez de désactiver le renouvellement automatique de votre compte et votre demande a bien été prise en compte.\n\n

Vous pouvez vérifier le statut du renouvellement automatique dans votre espace personnel à la page suivante :
http://www.dramapassion.com/compte/abonnement/\n\n

Veuillez converser précieusement cet email car il sert de preuve de votre demande de désactivation du renouvellement automatique.\n\n

Si vous avez des questions, veuillez nous contacter en utilisant le formulaire de contact disponible à la page suivante :
http://www.dramapassion.com/contact/\n\n


Nous vous remercions de la confiance que vous nous témoignez.\n\n

Cordialement,\n
L’équipe Dramapassion.
';

if(!$mail->Send()) {
   echo 'Mailer error: ' . $mail->ErrorInfo;
   echo Finished;
   //exit;
}
echo OK;
?>