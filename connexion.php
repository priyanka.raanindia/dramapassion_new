<?php
if(!session_id()) session_start();
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	

	
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script>
function setFocus()
{
document.getElementById("dpuser").focus();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="setFocus()" style="background-color: white;">
<!-- ImageReady Slices (Sans titre-1) -->
<div id="fb-root"></div>
  <script type="text/javascript">
// This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    //console.log('statusChangeCallback');
    //console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      UserConnect();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
  
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.

    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '344619609607',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.


  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/fr_FR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function UserConnect() {
    FB.api('/me', function(response) {
	    facebookID = response.id;
	    FacebookEmail = response.email;
	    FacebookUserName = response.first_name;
		urlJson = "<?php echo $http ; ?>_tempFacebook.php?i="+facebookID+"&u="+FacebookUserName+"&e="+FacebookEmail ; 
		
		var jqxhr = $.getJSON( urlJson, function() {
		  
		})
		  .done(function(data) {
		    	window.location.href = "<?php echo $http ; ?>loginFacebook.php?userID="+data.userid;

		  })
		  .fail(function() {
		    
		  });
		  

	}); 
      
      
  }

</script>
<?php 
?>
<form action="<?php echo $http ;?>loginAction.php" METHOD="POST">
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="600" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Connexion</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                            
                    </td>
                </tr>
			<?php if(isset($_GET['active']) && $_GET['active'] == 1){ ?>
			<tr><td>
				<div style="width:570px;text-align:center;">
				<p><br />
					Le compte n'est pas encore activé. Veuillez cliquer sur le lien d'activation qui se trouve dans l'email de validation qui vous a été envoyé.<br />
					<br /><a href="<?php echo $http ; ?>resend_confirmation.php" class="lien_bleu" style="font-size:medium;">Cliquez ici pour recevoir un nouvel email de validation.</a><br /><br />
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			<?php }elseif(isset($_GET['email_ok']) && $_GET['email_ok'] == 1){ ?>
				<tr><td>
				<div style="width:570px;text-align:center;margin:auto;">
				<p><br />
					Un nouvel email de validation a été envoyé.<br /><br />
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			
			
			<?php
			}elseif(isset($_SESSION['blocked']) && $_SESSION['blocked'] == true){ 
			$block = 1;
			?>
			<tr><td>
				<div style="width:570px;text-align:center;margin:auto;">
				<p><br />
					Votre compte a été bloqué pour non respect des conditions générales d'utilisation.<br /><br />
					Veuillez contacter le support pour plus d'informations.<br /><br />
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			
			
			<?php
			$_SESSION['blocked'] = "";
			}elseif(isset($_SESSION['userZero']) && $_SESSION['userZero'] == true){ 
			$block = 1;
			?>
			<tr><td>
				<div style="width:570px;text-align:center;margin:auto;">
				<p><br />
					Ce compte n'existe plus.<br /><br />
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			
			
			<?php
			$_SESSION['userZero'] = "";
			}else{ ?>
                <tr>
                	<td>
                    <table width="600" class="texte">
			<?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){?>
                    	<tr>
                        	<td colspan="2"><?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?><span class="rouge">Vous devez être connecté pour <?php if($_GET['variable'] == 1){ echo "signaler" ;}elseif($_GET['variable'] == 2){ echo "ecrire" ;} ?> un commentaire !</span><?php } ?></td>
                                                
						<td><br /></td>
						</tr>           
			<?php }
			if($_GET['message_pass'] == 1 || $_GET['message_pass'] == 2){?>
                    	<tr>
                        	<td colspan="2"><?php if($_GET['message_pass'] == 1 ){ ?><span class="rouge">Vos identifiants ont été envoyés à votre adresse email.</span><?php } ?></td>
                        	<td><br /></td>
						</tr>       
			<?php }
			if($_SESSION['error']['dpusername'] != ""){ 
			$erreur = $_SESSION['error']['dpusername'];
			?>
                    	<tr>
                        	<td colspan="2"><span class="rouge"><?php echo $erreur ; ?></span></td>
                            <td><br /></td>
						</tr> 
			<?php 
			$_SESSION['error']['dpusername'] = "";
			$_SESSION['error']['dppassword'] = "";
			}elseif($_SESSION['error']['dppassword'] != ""){ ?>
						<tr>
                        	<td colspan="2"><span class="rouge"><?php echo $_SESSION['error']['dppassword'] ; ?></span></td>
                            <td><br /></td>
						</tr> 
			<?php 
			$_SESSION['error']['dpusername'] = "";
			$_SESSION['error']['dppassword'] = "";
			}else{ ?>			
							<tr>
                        	<td colspan="2">&nbsp;</td>
                            <td><br /></td>
						</tr> 		
			<?php } ?>
                    	<tr>
                        	<td width="340" valign="top" colspan="2"><b style="text-transform:uppercase">Compte DRamaPassion</b></td>
                        	<td width="40"></td>
                        	<td width="220" valign="top"><b style="text-transform:uppercase">Connectez-vous via facebook</b></td>
						</tr>
						<tr>
                        	<td width="340" valign="top" colspan="2">&nbsp;</td>
                        	<td width="40">&nbsp;</td>
                        	<td width="220" valign="top">&nbsp;</td>
						</tr>
                        <tr>
                        	<td>Pseudo</td>
                            <td><input type="text" name="dpuser" id="dpuser" class="form" style="width:175px; height:27" tabindex="1"></td>
                        	<td width="50" valign="top" style="background-image:url(<?php echo $http ;?>images/pop_barre.jpg); background-repeat:repeat-y" rowspan="7"></td>                            
                            <td style="text-align: center;"><fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button></td>
						</tr>   
                        <tr>
                        	<td>Mot de passe</td>
                            <td><input type="password" name="dppassw" id="dppassw" class="form" style="width:175px; height:27" tabindex="2"></td>
                            
                            <td></td>
						</tr> 
                    	<tr>
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	<td></td>
                            <td><input src="<?php echo $http ;?>images/pop_connecter.jpg" type="image"></td>
                            
                            <td></td>
						</tr>           
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" class="form" name="rememberme" id="rememberme" tabindex="3"> Garder ma session ouverte</td>
                            <td></td>
						</tr>
						
                    	<tr>
                        	<td colspan="2">
<a href="<?php echo $http ; ?>pass_oubli.php" class="lien_bleu"><br />Pseudo/mot de passe oublié ?</a><br>
<?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?>
Pas encore de compte? <a href="<?php echo $http ; ?>register.php?variable=1" class="lien_bleu">Créer un compte maintenant</a><br>
<?php }else{ ?>
Pas encore de compte? <a href="<?php echo $http ; ?>register.php" class="lien_bleu">Créer un compte maintenant</a><br>
<?php } ?>
                            </td>
                            <td><br></td>
						</tr> 
					</table>                                                                                                                                        
                    <br><br>
                    </td>
                </tr>
			<?php } ?>
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>
</form> 
 <script>
      
 <?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
	
	if (w > 0) window.parent.$('.iframe_fancy').css({ width: w+"px"});
    if (h > 0) window.parent.$('.iframe_fancy').css({ height: h+"px"});
	
    if (w > 0) window.parent.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php }else{ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php } ?>
<?php if(isset($_GET['active']) && $_GET['active'] == 1){ ?>
fb_resize(600,250);
<?php }elseif((isset($_GET['email_ok']) && $_GET['email_ok'] == 1) || ($block == 1)){ ?>
fb_resize(600,200);

<?php }elseif((isset($_GET['variable']) && ($_GET['variable'] == 1 || $_GET['variable'] == 2 )) || (isset($_GET['message_pass']) && $_GET['message_pass'] == 1)){ ?>
fb_resize(600,350);

<?php }else{ ?>
fb_resize(600,350);
<?php } ?>

function fermer(){
	window.parent.$.fancybox.close();
}

</script>                 
</body>
</html>