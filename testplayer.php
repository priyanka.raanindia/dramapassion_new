<?php
require_once("includes/settings.inc.php");
require_once("includes/dbinfo.inc.php");
require_once("includes/functions.php");
require_once("logincheck.php");
require_once("langue/fr.php");
//require_once("locationcheck.php");
require_once("includes/serverselect.php");
$dir = "rns";
$file = "rns01";

$today = gmdate("n/j/Y g:i:s A");
$initial_url = "rtmp://ns01.dramapassion.com/vods/_definst_/"; //enter your rtmp server here
$ip = $_SERVER['REMOTE_ADDR'];
$key = "daHD!75H%x"; //enter your key here
$validminutes = 180;
$str2hash = $ip . $key . $today . $validminutes;
$md5raw = md5($str2hash, true);
$base64hash = base64_encode($md5raw);
$urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=$validminutes";
$base64urlsignature = base64_encode($urlsignature);
$signedurlwithvalidinterval = $initial_url . "?wmsAuthSign=$base64urlsignature";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- css -->
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_free.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_bottom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />

<link rel="shortcut icon" type="image/png" href="<?php echo $http ; ?>logo.png" /> 
<link rel="icon" type="image/png" href="<?php echo $http ; ?>logo.png" /> 

<!-- meta -->
<title>test</title>

<meta http-equiv="content-language" content="fr-fr" />

<!-- script java -->
<script src="<?php echo $server; ?>/js/scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>/js/AC_OETags.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?php echo $http ; ?>js/flowplayer-3.2.11.min.js"></script>		

<script type="text/javascript" src="<?php echo $http ; ?>js/flash_detect_min.js"></script>
<script type="text/javascript" src="<?php echo $http ; ?>js/swfobject.js"></script>
<style type="text/css">
        <!--
        	body{
	        	background-color: black;
        	}
			.white {
				color: white;
			}
			
			.white:link{text-decoration:none;color: white;}
			.white:visited{text-decoration:none;color: white;}
			.white:hover{text-decoration:none;color: white;}
			.white:active{text-decoration:none;color: white;}
        	.normal {
				color: #969696;
			}
			.normal:link {text-decoration:none;color: #464646;}
			.normal:visited {text-decoration:none;color: #464646;}
			.normal:hover {text-decoration:none;color:white;}
			.normal:active {text-decoration:none;color:white;}			
			.waiting {
				color: white;
				text-decoration: blink;
			}					
			.waiting:link {text-decoration:none;color: white;text-decoration: blink;}
			.waiting:visited {text-decoration:none;color: white;text-decoration: blink;}
			.waiting:hover {text-decoration:none;color: white;text-decoration: blink;}
			.waiting:active {text-decoration:none;color: white;text-decoration: blink;}
			.hold {
				color: #454545;
				cursor:  default;
			}
			.hold:link {text-decoration:none;color: #454545;cursor:  default;}	
			.hold:visited {text-decoration:none;color: #454545;cursor:  default;}	
			.hold:hover {text-decoration:none;color: #454545;cursor:  default;}	
			.hold:active {text-decoration:none;color: #454545;cursor:  default;}			
			.playing {
				color: #E1D275;
			}
			.playing:link {text-decoration:none;color: #E1D275;}
			.playing:visited {text-decoration:none;color: #E1D275;}
			.playing:hover {text-decoration:none;color: #E1D275;}
			.playing:active {text-decoration:none;color: #E1D275;}		
			
			.bt_on:link {text-decoration:none;color: #E1D275;}
			.bt_on:visited {text-decoration:none;color: #E1D275;}
			.bt_on:hover {text-decoration:none;color:white;}
			.bt_on:active {text-decoration:none;color: #E1D275;}	
			
			.bt_off:link {text-decoration:none;color: #464646;}
			.bt_off:visited {text-decoration:none;color: #464646;}
			.bt_off:hover {text-decoration:none;color: #E1D275;}
			.bt_off:active {text-decoration:none;color: #464646;}	
        -->
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<div style="width:1000px;margin:auto;border:none;">
		
		<div style="margin-bottom:6px;margin-left:74px;margin-top:15px;border:none;">
			<span class="blanc" style="text-transform:uppercase;margin-left:1px;margin-top:15px;font-size:medium;">Test :</span> <span class="rose" style="text-transform:uppercase;font-size:medium;">Episode test</span>
		</div>
		<div style="width:852;height:480px;margin-left:74px;display:block;">
			<div id="free_video">
				<div style="display:block;width:852px;height:480px;cursor:default;" id="free"></div>
			</div>
		</div>
		<div style="width:852px;margin-left:74px;">
		<div id="dssc" style="display:block;margin-top:10px;margin-left:20px;width:980px;height:5px;font-size:12px;">
			<div style="float:left;" class="white"><a target="_blank" href="<?php echo $http ; ?>guide_lecteur" title="Plus d'info" class="white" >Qualité (info) :</a></div>
			<div id="dssc-items" style="float:left;">
				<a href="javascript:void();" class="bt_on" onclick="Switch_auto()" id="switch_auto" ><div id="switch_txt" style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (ON)</div></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_364" onclick="">224p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_574" onclick="">288p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_896" onclick="">360p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_1196" onclick="">432p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_1728" onclick="">540p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_2728" onclick="">720p</a>
			</div>
		</div>	
</div>
				

<script>
switch_var = 0;
pl = flowplayer("free", "http://www.dramapassion.com/swf/flowplayer.commercial-3.2.15.swf", {
	key: '#$8d427e6f20cb64d82ba',
	clip: {
	   url: 'http://ns01.dramapassion.com/vod/_definst_/smil:<?php echo $dir; ?>/<?php echo $file ; ?>-hd.smil/medialist.smil',
       provider: 'rtmp',
       urlResolvers: [ 'smil', 'bwcheck' ],
       autoPlay: false,
       scaling: 'fit'
    },
    canvas: {
	    backgroundColor: "#000000",
	    backgroundGradient: "none"
	},
    plugins: {
        // RTMP streaming plugin
        rtmp: {
            url: "http://www.dramapassion.com/swf/flowplayer.rtmp-3.2.11.swf",
            netConnectionUrl: '<?PHP echo $signedurlwithvalidinterval ; ?>'
        },
        bwcheck: {
            url: "http://www.dramapassion.com/swf/flowplayer.bwcheck-3.2.11.swf",
 
            // CloudFront uses Adobe FMS servers
            serverType: 'wowza',
            menu: true,
            // we use dynamic switching, the appropriate bitrate is switched on the fly
            dynamic: true,
            dynamicBuffer: true,
            checkOnStart: false,
            qos:{bwUp: true, bwDown: true, frames: false, buffer: true, screen: true, bitrateSafety: 1.70},
 
            netConnectionUrl: '<?PHP echo $signedurlwithvalidinterval ; ?>',
            onStreamSwitchBegin: function (newItem, currentItem) {
            	
               //alert(newItem.bitrate);
            },
            onStreamSwitch: function (newItem) {
               if(switch_var == 0){
	               $('.playing').attr('class','hold');
	               $('.waiting').attr('class','hold');
	               
               }else if(switch_var == 1){
	               $('.playing').attr('class','normal');
	               $('.waiting').attr('class','normal');
               }
               var id_bw = '#qal_'+newItem.bitrate;
               $(id_bw).attr('class','playing');
            }
 
        },
        menu: {
            url: "http://www.dramapassion.com/swf/flowplayer.menu-3.2.11.swf",
            items: [
                // you can have an optional label as the first item
                // the bitrate specific items are filled here based on the clip's bitrates
                { label: "Qualitée :", enabled: false, index: 0 },
                { label: "Auto", toggle: true, group: 'select', selected: true , index: 1 },
                { label: "720p", toggle: true, group: 'select', index: 2728 },
                { label: "540p", toggle: true, group: 'select', index: 1728 },
                { label: "432p", toggle: true, group: 'select', index: 1196 },
                { label: "360p", toggle: true, group: 'select', index: 896 },
                { label: "288p", toggle: true, group: 'select', index: 574 },
                { label: "224p", toggle: true, group: 'select', index: 364 }
            ],
            menuItem: {
	            'color': '#222323',
	            'fontColor': '#FFFFFF',
	            'enabled': true,
	            'overColor': '#525252',
	            'border' : '0',
	             
            },
            width: 80,
            autoHide: true,
            onSelect: function(item) {
	            // play the video corresponding to the selected menu item's index
	            // the 'index' is a custom property set to the menu item
	            if (item.index == 1) {
		            $f('free').getPlugin("bwcheck").enableDynamic('enables');
		        }else{
		        	$f('free').getPlugin("bwcheck").enableDynamic('disables');
			        $f('free').getPlugin("bwcheck").setBitrate(item.index);
		        }
		    }
        },
        controls: {
        	url: 'http://www.dramapassion.com/swf/flowplayer.controls-3.2.14.swf',
        		tooltips: {
	        		buttons: true,
	        		menu: 'Qualitée vidéo'
	        	}
        
        },
         smil: { 
         	url: "http://www.dramapassion.com/swf/flowplayer.smil-3.2.8.swf" 
         }
     }
});
function Switch_auto(){
	
	if(switch_var == 0){
		var bitrate_cour = '#qal_'+$f('free').getPlugin("bwcheck").getBitrate();
		$f('free').getPlugin("bwcheck").enableDynamic('disables');
		$('#switch_auto').attr('class', 'bt_off');
		$('#switch_txt').html('&nbsp;&nbsp;&nbsp;&nbsp;Auto (OFF)');
		$('#qal_364').attr('class','normal');
		$('#qal_574').attr('class','normal');
		$('#qal_896').attr('class','normal');
		$('#qal_1196').attr('class','normal');
		$('#qal_1728').attr('class','normal');
		$('#qal_2728').attr('class','normal');
		
		$('#qal_364').attr('onclick','Switch_bw(364)');
		$('#qal_574').attr('onclick','Switch_bw(574)');
		$('#qal_896').attr('onclick','Switch_bw(896)');
		$('#qal_1196').attr('onclick','Switch_bw(1196)');
		$('#qal_1728').attr('onclick','Switch_bw(1728)');
		$('#qal_2728').attr('onclick','Switch_bw(2728)');
		
		$(bitrate_cour).attr('class','playing');
		
		
		switch_var = 1;
	}else if(switch_var == 1){
		var bitrate_cour = '#qal_'+$f('free').getPlugin("bwcheck").getBitrate();
		$f('free').getPlugin("bwcheck").enableDynamic('enables');
		$('#switch_auto').attr('class', 'bt_on');
		$('#switch_txt').html('&nbsp;&nbsp;&nbsp;&nbsp;Auto (ON)');
		$('#qal_364').attr('class','hold');
		$('#qal_574').attr('class','hold');
		$('#qal_896').attr('class','hold');
		$('#qal_1196').attr('class','hold');
		$('#qal_1728').attr('class','hold');
		$('#qal_2728').attr('class','hold');
		
		$('#qal_364').attr('onclick','');
		$('#qal_574').attr('onclick','');
		$('#qal_896').attr('onclick','');
		$('#qal_1196').attr('onclick','');
		$('#qal_1728').attr('onclick','');
		$('#qal_2728').attr('onclick','');
		
		$(bitrate_cour).attr('class','playing');
		
		switch_var = 0;
	}
	
	
	
}
function Switch_bw(bw){
	var id_link = '#qal_'+bw;
	$(id_link).attr('class','waiting');
	$f('free').getPlugin("bwcheck").setBitrate(bw);
}
</script>
</body>
</html>