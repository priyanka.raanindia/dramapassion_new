<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
	
	$id = cleanup($_GET['id_dvd']);
	$req = "SELECT * FROM t_dp_dvd WHERE DvdID =".$id;
	$sql = mysql_query($req);
	$dramaInfo = dramaInfo($id);
	
	$nom =  mysql_result($sql,0,"TitleFre");
	$statut = mysql_result($sql,0,"statut");
	$prix = mysql_result($sql,0,"PriceTvac");
	$delai_livr = "4";
	$acteur = mysql_result($sql,0,"Actors");
	$realisateur = mysql_result($sql,0,"Director");
	$produit = mysql_result($sql,0,"Producer");
	$editeur = mysql_result($sql,0,"Distributor");
	$version = mysql_result($sql,0,"VersionFre");
	$format = mysql_result($sql,0,"FormatFre");
	$langue = mysql_result($sql,0,"LanguageFre");
	$sous_titre = mysql_result($sql,0,"SubtitlesFre");
	$region = mysql_result($sql,0,"Region");;
	$nb_dvd = mysql_result($sql,0,"Discnumber");
	$emballage = mysql_result($sql,0,"PackagingFre");
	$date_sortie = mysql_result($sql,0,"ReleasedateFre");
	$duree_tot = mysql_result($sql,0,"DurationFre");
	$synopsis = mysql_result($sql,0,"SynopsisFre");
	$img = mysql_result($sql,0,"img");
	
	
	$dramaID_actor =  mysql_result($sql,0,"dramaID_actor");
	
?>
<tr>
        <td valign="top">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="800">
                <tr>
                	<td width="1000" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir"><?php echo $nom; ?></h1><br />
<img src="<?php echo $http ; ?>images/ligne1000.jpg">      
<br><br>
        
<table width="1000" border="0" cellpadding="0" cellspacing="0">
	<tr>
    	<td width="300" valign="top">
        <img src="<?php echo $http."content/dvd/dvd_".$img."01.jpg" ; ?>" width="300"></td>
        <td width="40">&nbsp;</td>
		<td width="660" valign="top" class="texte">
        <h2><?php echo $nom ; ?></h2>
        
        <table width="660" class="texte">
        	<tr>
            	<td>Statut : </td>
                <td class="rose"><?php 
                if($statut == 'Rupture de stock'){
                echo '<span style="color:#7b1302;">'.$statut.'</span>' ;  
                }else{
	              echo $statut ;   
                }
                ?></td>
			</tr>
            <tr>
            	<td>Prix :</td>
                <td class="rose"><?php echo $prix ; ?> € (livraison incluse en France et en Belgique)</td>
			</tr>
            <tr>
            	<td>Délai de livraison : </td>
                <td class="rose"><?php echo $delai_livr ; ?> jours ouvrables <?php if($id == 10){ echo ' - après la sortie officielle' ;} ?></td>
			</tr>
            <tr>
            	<td>Acteurs : </td>
                <td class="rose"><?php echo $acteur ; ?></td>
			</tr>            
            <tr>
            	<td>Réalisateur : </td>
                <td class="rose"><?php echo $realisateur ; ?></td>
			</tr>
            <tr>
            	<td>Produit par :</td>
                <td class="rose"><?php echo $produit ; ?></td>
			</tr>
            <tr>
            	<td>Editeur :</td>
                <td class="rose"><?php echo $editeur ; ?></td>
			</tr>
            <tr>
            	<td>Version : </td>
                <td class="rose"><?php echo $version ; ?></td>
			</tr>
            <tr>
            	<td>Format : </td>
                <td class="rose"><?php echo $format ; ?></td>
			</tr>
            <tr>
            	<td>Langue : </td>
                <td class="rose"><?php echo $langue ; ?></td>
			</tr>
            <tr>
            	<td>Sous-titres :</td>
                <td class="rose"><?php echo $sous_titre ; ?></td>
			</tr>
            <tr>
            	<td>Région : </td>
                <td class="rose"><?php echo $region ; ?></td>
			</tr>
            <tr>
            	<td>Nombre de disques : </td>
                <td class="rose"><?php echo $nb_dvd ; ?></td>
			</tr>
            <tr>
            	<td>Emballage : </td>
                <td class="rose"><?php echo $emballage ; ?></td>
			</tr>
            <tr>
            	<td>Date de sortie :</td>
                <td class="rose"><?php echo $date_sortie ; ?></td>
			</tr>
            <tr>
            	<td>Durée totale : </td>
                <td class="rose"><?php echo $duree_tot ; ?></td>
			</tr>
        </table>                
		<br>
        <img src="<?php echo $http ; ?>images/ligne660.jpg" >

<h2>Synopsis</h2>
<div id="dvd_synopsis" style="width:660px;text-align:justify;">
<?php echo $synopsis ; ?>	
</div>
<br><br>
<img src="<?php echo $http ; ?>images/ligne660.jpg" width="660">
        
<h2>Casting</h2>
						<table width="660" class="noir">
                        	<?php ActreurDrama($dramaID_actor) ; ?>
                                                         
                        	                           
                        </table>
<br>
<img src="<?php echo $http ; ?>images/ligne660.jpg" >
<br><br>
<?php if($statut == 'Rupture de stock'){
        	?>
        	<a href="http://www.amazon.fr/Boys-over-flowers-Int%C3%A9grale/dp/B004RIOBYM/ref=sr_1_1?ie=UTF8&qid=1367228059&sr=8-1&keywords=boys+over+flowers" target="_blank" style="text-decoration:underline ;color:blue !important;font-weight:bold;">Vous pouvez encore vous le procurer en cliquant ici !</a>
        	<?php
        }else{
        ?>
<a <?php if($_SESSION['userid']){ ?>href="<?php echo $http; ?>orderdetail.php?dvd=<?php echo $id; ?>" <?php }else{echo 'href="'.$http.'connexion.php" class="iframe log_dvd"'; } ?> ><?php if($id == 10){ ?><img src="<?php echo $http ; ?>images/preco.jpg" border="0"><?php }else{ ?><img src="<?php echo $http ; ?>images/pics033.jpg" border="0"><?php } ?></a>
<?php } ?>
<br><br>

<!-- 
<div id="dvd_mag" style="width:590px;text-align:justify;">
Vous pouvez également vous procurer le coffret DVD "The 1st Shop of Coffee Prince" auprès des magasins suivants.
</div>
<br>
<a href="#" class="lien_fushia">Voir les magasins</a>
-->
<br />
        </td>
	</tr>
</table>    
        
                              
                                                                                         
                    
                	</td>                                                             
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>
<script>
<?php
if($id == 1){
?>

<?php
}elseif($id == 2){
?>
$('.rose').css('color','#835731');
<?php	
}elseif($id == 3){
?>
$('.rose').css('color','#1ba8fe');
<?php	
}elseif($id == 4){
?>
$('.rose').css('color','#d31415');
<?php	
}elseif($id == 5){
?>
$('.rose').css('color','#05028f');
<?php	
}

?>
$(".log_dvd").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		parent.location.reload(true);
	}
});
</script>
<?php require_once("bottom.php"); 


?>