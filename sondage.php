<?php

	//include générale
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
		
	
	?>
	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="setFocus()">	
<form action="<?php echo $http ;?>sondage_action.php" METHOD="POST">
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Sondage</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                            
                    </td>
                </tr>
                <tr>
                	<td width="600" style="padding-left:30px;padding-right:30px;">
                	<br />
                		<span style="font-size:20px;">Merci de nous accorder 15 secondes de votre temps </span><img src="<?php echo $http ; ?>images/800px-Smiley.png" height="20" /> <br /><br />
                		Quel est l'avantage du service payant que vous jugez le plus important par rapport au service gratuit ?<br /><br />
                		<input type="radio" name="option" value="1">&nbsp;&nbsp; Pas de publicité<br />
                		<input type="radio" name="option" value="2">&nbsp;&nbsp; Meilleure qualité de la vidéo<br /> 
                		<input type="radio" name="option" value="3">&nbsp;&nbsp; Accès tablette et mobile<br />
                		<input type="radio" name="option" value="4">&nbsp;&nbsp; Téléchargement temporaire<br />
                		<input type="radio" name="option" value="5">&nbsp;&nbsp; Ne pas devoir attendre 3 mois pour les nouveautés<br /><br /><br />
                	</td>
                </tr>
                <tr>
                	<td width="600" style="padding-left:30px;padding-right:30px;">
                		Commentaires éventuels (optionnel) :<br /><br />
                		<textarea name="coms" style="width:540px;height:50px;margin:auto;"></textarea><br /><br />
                	</td>
                </tr>
                <tr width="600">
                	<td style="padding-left:30px;padding-right:30px;"><input src="<?php echo $http ;?>images/pop_up_annonceur.jpg" type="image"><span style="font-size:12px;float:right;margin-top:10px;"><a href="<?php echo $http ; ?>sondage_action2.php" style="color:blue !important;">Ne pas répondre et ne plus afficher</a></span></td>
                </tr>
            </table>
        </td>
	</tr>
</table>
</form>
</body>
</html>
