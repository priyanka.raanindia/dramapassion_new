<!DOCTYPE html>

<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />

  <title>brselect fms smil</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style type="text/css">
/* <![CDATA[ */
    #player {
      display: block;
      width: 640px;
      height: 360px;
      margin: 0;
    }
    :focus {
      outline-style: none;
    }
  /* ]]> */
  </style>
  <script type="text/javascript" src="http://www.dramapassion.com/js/flowplayer-3.2.12.min.js">
</script>
  <script type="text/javascript">
//<![CDATA[
  window.onload = function () {
    $f("player", "http://www.dramapassion.com/swf/flowplayer.commercial-3.2.16.swf", {
      //log: {level: "debug", filter: "org.flowplayer.bitrateselect.*"},
      key: '#$8d427e6f20cb64d82ba',
      clip: {
        provider: "rtmp",
        scaling: "fit",
        urlResolvers: ["smil", "brselect"],
        url: 'http://devel.dramapassion.com/wmstest/wowza/elephantsdream.smil',
        autoPlay: false
      },
      plugins: {
        smil: {
          url: "http://www.dramapassion.com/swf/flowplayer.smil-3.2.8.swf"
        },
        rtmp: {
          url: "http://www.dramapassion.com/swf/flowplayer.rtmp-3.2.12.swf",
          netConnectionUrl: 'rtmp://devel.dramapassion.com:1935/vod/_definst_/'
        },
        brselect: {
          url: "http://www.dramapassion.com/swf/flowplayer.bitrateselect-3.2.13.swf",
          onStreamSwitchBegin: function (newItem, currentItem) {
            this.getPlayer().getPlugin("content").setHtml("Switch from " + currentItem.bitrate +
                " kbps to " + newItem.bitrate + " kbps ...");
          },
          onStreamSwitch: function (newItem) {
            this.getPlayer().getPlugin("content").setHtml("Switched to " + newItem.bitrate + " kbps.");
          }
        },
        content: {
          url: "http://www.dramapassion.com/swf/flowplayer.content-3.2.8.swf",
          width: 300,
          left: 10
        }
      }
    });
  };
  //]]>
  </script>
</head>

<body>
  <h1>brselect WOWZA smil</h1>

  <div id="player"></div>
</body>
