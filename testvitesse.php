<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
	
	$urlsec = Hash_st_HD(10,17,'hd');
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Testez la vitesse de votre connexion internet</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="contenu" style="text-align:justify;">
<h3>A qui s'adresse ce test ?</h3>
<p>Ce test permet de voir si la vitesse de votre connexion est suffisante pour regarder les vidéos en streaming SD et/ou HD.
Si la vidéo se met souvent en "buffering" pendant que vous regardez une vidéo en streaming, ce test peut vous être utile.</p>
<br />
<p>Si vous utilisez principalement le téléchargement, vous pouvez toujours utiliser ce test pour faire un test rapide de votre connexion.
Nous vous invitons par ailleurs à consulter la page suivante qui explique comment vous pouvez mieux profiter du téléchargement :<br />
<a href="<?php echo $http; ?>astuce-telechargement/" class="lien_bleu"><?php echo $http; ?>astuce-telechargement/</a></p>
<br />

<h3>Test de la vitesse de connexion</h3>
<p>La vitesse de votre connexion peut varier avec le temps.
<br />Nous vous recommandons de faire ce test à différents moments de la journée.</p>
<br />
<p><b>Avant de procéder au test, veuillez vérifier :</b></p>
<ul>
<li><b>qu'aucun autre programme n'utilise internet en même temps</b></li>
<li><b>qu'aucun autre ordinateur n'utilise la même connexion internet en même temps</b></li>
<li><b>que votre boitier de la télévision numérique soit éteint (si vous utilisez la télévision et l'internet sur la même ligne)</b></li>
</ul>
<p>Cliquez sur le lien suivant pour lancer le téléchargement. Ensuite, observez à quelle vitesse le téléchargement s'effectue.
La vitesse s'exprime en ko/sec ou Mo/sec.</p>
<br />
<p style="text-align:center;"><a href="<?php echo $server_hd.$urlsec; ?>" class="lien_bleu" style="font-size:14px;"><b>Cliquez ici pour lancer le téléchargement du fichier test</b></a></p>
<br />

<h3>Comment interpréter le résultat</h3>
<ul>
<li>Si la vitesse est supérieure à 400 ko/sec (0,4 Mo/sec), la vitesse est suffisante pour la qualité HD.</li>
<li>Si la vitesse se situe entre 200 ko/sec (0,2 Mo/sec), la vitesse est suffisante pour la qualité SD.</li>
<li>Si la vitesse est inférieure à 200 ko/sec (0,2 Mo/sec), la vitesse n'est pas suffisante pour le streaming et le téléchargement peut être très long.</li>
</ul>
<br />

<h3>Pourquoi ma connexion est-elle aussi lente ?</h3>
<p>Différentes raisons peuvent expliquer la lenteur de votre connexion :</p>
<ul>
<li>Si la vitesse reste lente et constante peu importe l'heure de la journée, la lenteur peut être due à des causes physiques et/ou techniques.
Nous vous invitons à vous renseigner auprès de votre FAI.</li>
<li>Si la variation de la vitesse est importante, et si la vitesse est particulièrement lente le soir, il est probable que le réseau de votre FAI
soit saturé à cause d'une trop grande utilisation d'internet par les abonnés.</li>
<li>Si vous utilisez la même ligne pour la télévision et l'internet, le fait d'avoir le boitier de la télévision allumé peut diminuer la vitesse de votre connexion internet.</li>
<li>Enfin, dans des particuliers, il est aussi possible que votre FAI bride la connexion pour certaines activités dont le téléchargement direct.</li>
</ul>
<br />

<h3>Comment améliorer la situation ?</h3>
<p>Dans tous cas, nous vous invitons à remplir le formulaire au lien suivant : <a href="<?php echo $http; ?>formulaire-connexion/" class="lien_bleu"><?php echo $http; ?>formulaire-connexion/</a></p>
<p>Les informations que vous nous fournissez peuvent nous aider à mieux adapter notre service dans le futur.</p>
<br />
<p>Si vous utilisez principalement le téléchargement, l'utilisation d'un gestionnaire de téléchargement peut vous grandement faciliter la tâche.
Veuillez consulter la page au lien suivant :<br />
<a href="<?php echo $http; ?>astuce-telechargement/" class="lien_bleu"><?php echo $http; ?>astuce-telechargement/</a></p>
<br />
<p>Si vous utilisez le streaming et si vous avez un abonnement Privilège, nous vous invitons à essayer le téléchargement.
Veuillez consulter les deux pages suivantes qui expliquent comment profiter au mieux de notre service de téléchargement.</p>
<p>- Guide pour commencer : <a href="<?php echo $http; ?>guide-telechargement/" class="lien_bleu"><?php echo $http; ?>guide-telechargement/</a></p>
<p>- Utilisation d'un gestionnaire de téléchargement : <a href="<?php echo $http; ?>astuce-telechargement/" class="lien_bleu"><?php echo $http; ?>astuce-telechargement/</a></p>
<br />
<p>Enfin, si vous avez un abonnement Découverte et si vous désirez tester le téléchargement, veuillez nous contacter en utilisant le formulaire au lien suivant :
<a href="<?php echo $http; ?>contactez-nous/" class="lien_bleu"><?php echo $http; ?>contactez-nous/</a></p>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>