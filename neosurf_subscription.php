<?php
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("includes/settings.inc.php");
	require_once("includes/promocheck.php");
		
	require_once "NeosurfMerchant.php";
	//Récupération de la chaîne cryptée envoyée en paramètre par socket
	$data = $_POST['rep'];
	
	//Test de l'option magic_quotes_gpc sur le serveur du marchand
	if (get_magic_quotes_gpc()) $data=stripslashes($data);

	//Récupération des données à partir de la chaîne $data
	$trsdata = parseResponse($data);
	
	//Est-ce que la chaîne reçue a pu être décryptée ?
	if ($trsdata['Errno'] != 0) {
		//Option : Enregistrez ici l'erreur en base de donnée
		//Utilisez pour cela le paramètre $trsdata['Errno']
		print 'KO';
		print "Erreur dans l'appel de parseResponse. Code erreur = " . $trsdata['Errno'];
	} else {
		//Est-ce que la transaction a pu avoir lieu sur le serveur de Neosurf ?
		if ($trsdata['ReponseNeosurf'] == 1) {			
			
			//Le paiement a pu être effectué
			//Validez définitivement la transaction du client 
			//Utilisez pour cela le paramètre $trsdata['IDTransaction']
			if(isset($_REQUEST['id']) && isset($_REQUEST['price'])){
				print 'OK';
				$ip = $_SERVER['REMOTE_ADDR'];
				$userid = substr($trsdata['IDTransaction'],16,strlen($trsdata['IDTransaction']));
			
				$subid = cleanup($_REQUEST['id']);
				$price = cleanup($_REQUEST['price']);
				
				$user = mysql_query("SELECT SubscriptionID,Expiry FROM t_dp_user WHERE UserID = ".$userid);
				$cursubscription = mysql_result($user,0,"SubscriptionID");
				$subsc = mysql_query("SELECT Title, Duration FROM t_dp_subscription WHERE SubscriptionID = ".$subid);
				$product = mysql_result($subsc,0,"Title");
				$transdatetime = date("Y-m-d H:i:s");
				
				$expiry = date("Y-m-d H:i:s");
				$promocheck = checkpromologged($userid);	
				$TimeStampJanv = 1454281199;
				$TimeStampDec = 1450998000;
				$TimeStampDecouverte = 1452293999;	
				
					
				if($subid == 9){
					if($promocheck == 1){
						$now = time();
						if($now >= $TimeStampDec){
							$expiry = $TimeStampDecouverte;
						}else{
							$expiry = strtotime(date("Y-m-d H:i:s", strtotime($expiry)) . " +14 days");
						}
						mysql_query("UPDATE stat_promo SET nb = nb + 1 WHERE id = 0");
					}else{
						$expiry = strtotime(date("Y-m-d H:i:s", strtotime($expiry)) . " +7 days");
					}
				} elseif ($subid == 1 || $subid == 3){
					if($promocheck == 1){
						$expiry = $TimeStampJanv;	
						if($subid == 1){
							mysql_query("UPDATE stat_promo SET nb = nb + 1 WHERE id = 1");
						}else{
							mysql_query("UPDATE stat_promo SET nb = nb + 1 WHERE id = 2");
						}						
					}else{
						$expiry = strtotime(date("Y-m-d H:i:s", strtotime($expiry)) . " +1 month");														
					}
				} elseif ($subid == 2 || $subid == 4){
					if($promocheck == 1){
						$expiry = $TimeStampJanv;	
						if($subid == 2){
							mysql_query("UPDATE stat_promo SET nb = nb + 1 WHERE id = 1");
						}else{
							mysql_query("UPDATE stat_promo SET nb = nb + 1 WHERE id = 2");
						}						
					}else{
						$expiry = strtotime(date("Y-m-d H:i:s", strtotime($expiry)) . " +1 month");														
					}
				} elseif ($subid == 10){
					if($promocheck == 1){
						$expiry = $TimeStampJanv;	
						mysql_query("UPDATE stat_promo SET nb = nb + 1 WHERE id = 2");						
					}else{
						$expiry = strtotime(date("Y-m-d H:i:s", strtotime($expiry)) . " +1 month");														
					}
				} else {
				}
				$parrainageActive = 1;
					if($parrainageActive == 1){
						if($subid == 3){
							$lookForP = mysql_query("SELECT * FROM parrainageTemp WHERE userIDF = ".$userid." AND used = 0 ORDER BY id DESC LIMIT 0,1");
							$checkForP = mysql_num_rows($lookForP);
							if($checkForP > 0){
								$userP = mysql_result($lookForP,0,"userIDP" );
								$userPAbo = abo_user($userP);
								if($userPAbo == 'privilege'){
									$expiry = date("Y-m-d H:i:s");
									$expiry = strtotime(date("Y-m-d H:i:s", strtotime($expiry)) . " +14 days");
									mysql_query("INSERT INTO parrainage (userIDP,userIDF,date) VALUES(".$userP.",".$userid.",NOW())");
									$checkUserP = mysql_query("SELECT * FROM parrainage WHERE userIDP = ".$userP);
									$checkUserP2 = mysql_query("SELECT * FROM parrainage WHERE userIDF = ".$userP);
									$numberP = mysql_num_rows($checkUserP);
									$numberP2 = mysql_num_rows($checkUserP2);
									$numberP = $numberP + $numberP2;
									if($numberP <= 4){
										$updateUserP = mysql_query("SELECT * FROM t_dp_user WHERE UserID = ".$userP);
										$dateUserPTemp = mysql_result($updateUserP,0,'Expiry');
										$dateUserP = strtotime($dateUserPTemp) + 60*60*24*14;
										$newExpP = date("Y-m-d H:i:s", $dateUserP);
										mysql_query("UPDATE t_dp_user SET Expiry = '".$newExpP."' WHERE UserID = ".$userP." LIMIT 1");
										mysql_query("INSERT INTO t_dp_transaction (TransDateTime,UserID,TransTypeID,TransProductID,TransProductName,TransEuro,TransAmount,OgoneRefID,UserIP,Method) VALUES ('".$transdatetime."','".$userP."',6,3,'PROMO PARRAINAGE','0',0,'0','".$ip."','PROMO')");
									}
									mysql_query("UPDATE parrainageTemp SET used = 1 WHERE userIDF = ".$userid." ");
									mysql_query("INSERT INTO t_dp_transaction (TransDateTime,UserID,TransTypeID,TransProductID,TransProductName,TransEuro,TransAmount,OgoneRefID,UserIP,Method) VALUES ('".$transdatetime."','".$userid."',6,3,'PROMO PARRAINAGE','0',0,'0','".$ip."','PROMO')");
								}
							}
						}
					}	
				$expiry = date("Y-m-d H:i:s",$expiry);				
				
				//ADD TRANSACTIONS
				$query = "INSERT INTO t_dp_transaction (TransDateTime,UserID,TransTypeID,TransProductID,TransProductName,TransAmount,TransEuro,OgoneRefID,UserIP,Method) VALUES ('".$transdatetime."','".$userid."',6,".$subid.",'".$product."',0,'".$price."','".$trsdata['IDTransaction']."','".$ip."','NEOSURF')";
				mysql_query($query);

				//ADD PROMO STAT
				$quepromo = "INSERT INTO promonov15 (userid,date,subid) VALUES (".$userid.",".$transdatetime.",".$subid.")";
				mysql_query($quepromo);				

				//SET SUBSCRIPTION
				if ($subid == 7 || $subid == 8){
					$subid = 1;
				}
				mysql_query("UPDATE t_dp_user SET Next = ".$subid.", SubscriptionID = ".$subid.", Expiry = '".$expiry."', autorenewal = 0 WHERE UserID = ".$userid."");
				$_SESSION['subscription'] = $subid;
				
				
				if(isset($_REQUEST['coupon_code'])){
				$coupon = cleanup($_REQUEST['coupon_code']);
				
				
				$coupon_req = "SELECT * FROM coupon WHERE code ='".$coupon."'";
				$coupon_sql = mysql_query($coupon_req);
				$id_coupon = mysql_result($coupon_sql,0,'id');
					
					
				$coupon_req_user = "SELECT * FROM coupon_user WHERE id_coupon ='".$id_coupon."' AND id_user =".$userid;
				$coupon_sql_user = mysql_query($coupon_req_user);
				$nb_utilisation = mysql_result($coupon_sql_user,0,'utiliser');
						
				$nb_utilisation = $nb_utilisation +1;
					
					
				$req_update_coupon = "UPDATE coupon_user SET utiliser = ".$nb_utilisation." , utiliser_date = NOW() WHERE (id_user = ".$userid." AND id_coupon = ".$id_coupon.")" ;
				mysql_query($req_update_coupon);
				}
				
				
					
						
						$req_user_info = "SELECT * FROM t_dp_user WHERE UserID = ".$userid;
						$sql_user_info = mysql_query($req_user_info);
						$sex = mysql_result($sql_user_info,0,'SexID');
						
						if($sex == 1){
							$message_p = 'Cher ';
						}elseif($sex == 2){
							$message_p = 'Chère ';
						}else{
							$message_p = 'Cher/Chère ';
						}
						
						$req_type_abo = "SELECT * FROM t_dp_subscription WHERE SubscriptionID =".$subid ;
						$sql_type_abo = mysql_query($req_type_abo);
						
						$pseudo = $message_p.mysql_result($sql_user_info,0,'UserName');
						$num_com = $trsdata['IDTransaction'];
						$time = $transdatetime;
						$methode_pai = 'NEOSURF';
						$montant = $price;
						if($promocheck == 1){
							$abo = mysql_result($sql_type_abo,0,'TitleFre').' - '.$promo_titre;
						}else{
							$abo = mysql_result($sql_type_abo,0,'TitleFre');
						}						
						$date_expi = $expiry;
						$mail_client = mysql_result($sql_user_info,0, 'UserEmail');
						
						require 'mail_mes2.php';
						
						
					
				
							
			}else{
				print 'KO';
			}
		} else {
			//Option : Enregistrez ici l'erreur en base de donnée. Annulez la transaction.
			//Utilisez pour cela les paramètres $trsdata['IDTransaction'] et $trsdata['Errno'] 
			print 'KO';
			print "Erreur dans l'appel de parseResponse. Code erreur = " . $trsdata['Errno']." - Trans ID: ".$trsdata['IDTransaction'];
		}
	}
?>