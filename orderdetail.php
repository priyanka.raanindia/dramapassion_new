<?php session_start();
require_once("includes/settings.inc.php");
require_once("includes/dbinfo.inc.php");

if($_REQUEST['dvd'] != 1){

if(((isset($_REQUEST['dvd']) && $_REQUEST['dvd'] != 0) || isset($_SESSION['order']['quantity'])) && isset($_SESSION['userid'])){
	require_once("top.php");

	$dvd = mysql_query("SELECT * FROM t_dp_dvd WHERE DvdID = ".$_REQUEST['dvd']);
	$user = mysql_query("SELECT * FROM t_dp_user WHERE UserID = ".$_SESSION['userid']);
	$countries = mysql_query("SELECT * FROM t_dp_countryship");
	
	if(isset($_POST['quantity'])){
		$quantity = cleanup($_POST['quantity']);
		$_SESSION['order']['quantity'] = $quantity;
	}elseif(isset($_SESSION['order']['quantity'])){
		$quantity = $_SESSION['order']['quantity'];
	}else{
		$quantity = 1;
		$_SESSION['order']['quantity'] = $quantity;
	}
	
	$totalprice = mysql_result($dvd,0,"PriceTvac") * $quantity;
	$_SESSION['dvd'] = $_REQUEST['dvd'];
	
?>
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
	<tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Votre commande</h1>
<img src="images/ligne720.jpg">      
<br><br>
<b style="text-transform:uppercase">Details de votre commande</b>
<br><br>
          <table cellpadding="0" cellspacing="5" border="0" style="padding-left:30px;">
            <tr>
              <td width="85" align="left">Produit : </td>
              <td align="left"><?php echo mysql_result($dvd,0,"TitleFre"); ?></td>
            </tr>
            <tr>
              <td align="left">Prix unitaire : </td>
              <td align="left"><?php echo mysql_result($dvd,0,"PriceTvac"); ?> &euro;</td>
            </tr>
            <tr>
              <td align="left">Quantité : </td>
              <td align="left"><form method="post" action="orderdetail.php?dvd=<?php echo $_REQUEST['dvd']; ?>">
                  <select name="quantity" onchange="this.form.submit();" style="width:50px;">
                    <?php for($i=1;$i<=5;$i++){ ?>
                    <option value="<?php echo $i; ?>" <?php if($quantity == $i){ echo "selected"; }?>><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
                </form></td>
            </tr>
            <tr>
              <td align="left">Montant total : </td>
              <td align="left"><?php echo number_format($totalprice,2); ?> &euro;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">Les frais de livraison éventuels seront calculés à l'étape suivante.</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table>
<img src="images/ligne720.jpg">      
<br><br>
<b style="text-transform:uppercase">Adresse de facturation</b>
<br><br>
          <form method="post" action="orderdetailAction.php">
            <table cellpadding="0" cellspacing="5" border="0" style="padding-left:30px;">
              <tr>
                <td align="left">Nom :</td>
                <td align="left"><input type="text" name="oflname" value="<?php echo mysql_result($user,0,"UserLname"); ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['oflname'])){ echo $_SESSION['error']['oflname'];} ?></td>
              </tr>
              <tr>
                <td align="left">Prénom :</td>
                <td align="left"><input type="text" name="offname" value="<?php echo mysql_result($user,0,"UserFname"); ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['offname'])){ echo $_SESSION['error']['offname'];} ?></td>
              </tr>
              <tr>
                <td align="left">Adresse (ligne 1) :</td>
                <td align="left"><input type="text" name="ofaddress1" value="<?php echo mysql_result($user,0,"UserAddress1"); ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['ofaddress1'])){ echo $_SESSION['error']['ofaddress1'];} ?></td>
              </tr>
              <tr>
                <td align="left">Adresse (ligne 2) :</td>
                <td align="left"><input type="text" name="ofaddress2" value="<?php echo mysql_result($user,0,"UserAddress2"); ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['ofaddress2'])){ echo $_SESSION['error']['ofaddress2'];} ?></td>
              </tr>
              <tr>
                <td align="left">Code postal :</td>
                <td align="left"><input type="text" name="ofzip" value="<?php echo mysql_result($user,0,"UserZip"); ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['ofzip'])){ echo $_SESSION['error']['ofzip'];} ?></td>
              </tr>
              <tr>
                <td align="left">Ville :</td>
                <td align="left"><input type="text" name="ofcity" value="<?php echo mysql_result($user,0,"UserCity"); ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['ofcity'])){ echo $_SESSION['error']['ofcity'];} ?></td>
              </tr>
              <tr>
                <td align="left">Pays :</td>
                <td align="left"><select name="ofcountry">
                    <?php for($i=0;$i<mysql_num_rows($countries);$i++){ 
					$selected = "";
					if(mysql_result($user,0,CountryID) > 0){
						if(mysql_result($user,0,CountryID) == mysql_result($countries,$i,"CountryID")){
							$selected = "SELECTED";
						}
					}else if(mysql_result($countries,$i,"CountryID") == 66){
						$selected = "SELECTED";
					}
					?>
                    <option value=<?php echo "\"".mysql_result($countries,$i,"CountryID")."\" ".$selected; ?>><?php echo mysql_result($countries,$i,"CountryNameFre"); ?></option>
                    <?php } ?>
                  </select></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['ofcountry'])){ echo $_SESSION['error']['ofcountry'];} ?></td>
              </tr>
              <tr>
                <td align="left">Numéro de téléphone :</td>
                <td align="left"><input type="tel" name="ofmobile" value="<?php echo mysql_result($user,0,"UserMobile"); ?>"  /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['ofmobile'])){ echo $_SESSION['error']['ofmobile'];} ?></td>
              </tr>
			<tr>
              <td colspan="3">&nbsp;</td>
            </tr>
            </table>
<img src="images/ligne720.jpg">      
<br><br>
<b style="text-transform:uppercase">Adresse de livraison</b>
<br><br>
<input type="checkbox" name="livraison" value="1" <?php if(isset($_SESSION['data']['livraison']) && $_SESSION['data']['livraison']==1){ echo checked;} ?> />&nbsp;&nbsp;L'adresse de livraison est différente de l'adresse de facturation.
<br><br>
			<table cellpadding="0" cellspacing="5" border="0" style="padding-left:30px;">
              <tr>
                <td align="left">Nom :</td>
                <td align="left"><input type="text" name="ollname" value="<?php if(isset($_SESSION['data']['ollname'])){echo $_SESSION['data']['ollname'];} ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['ollname'])){ echo $_SESSION['error']['ollname'];} ?></td>
              </tr>
              <tr>
                <td align="left">Prénom :</td>
                <td align="left"><input type="text" name="olfname" value="<?php if(isset($_SESSION['data']['olfname'])){echo $_SESSION['data']['olfname'];} ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['olfname'])){ echo $_SESSION['error']['olfname'];} ?></td>
              </tr>
              <tr>
                <td align="left">Adresse (ligne 1) :</td>
                <td align="left"><input type="text" name="oladdress1" value="<?php if(isset($_SESSION['data']['oladdress1'])){echo $_SESSION['data']['oladdress1'];} ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['oladdress1'])){ echo $_SESSION['error']['oladdress1'];} ?></td>
              </tr>
              <tr>
                <td align="left">Adresse (ligne 2) :</td>
                <td align="left"><input type="text" name="oladdress2" value="<?php if(isset($_SESSION['data']['oladdress2'])){echo $_SESSION['data']['oladdress2'];} ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['oladdress2'])){ echo $_SESSION['error']['oladdress2'];} ?></td>
              </tr>
              <tr>
                <td align="left">Code postal :</td>
                <td align="left"><input type="text" name="olzip" value="<?php if(isset($_SESSION['data']['olzip'])){echo $_SESSION['data']['olzip'];} ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['olzip'])){ echo $_SESSION['error']['olzip'];} ?></td>
              </tr>
              <tr>
                <td align="left">Ville :</td>
                <td align="left"><input type="text" name="olcity" value="<?php if(isset($_SESSION['data']['olcity'])){echo $_SESSION['data']['olcity'];} ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['olcity'])){ echo $_SESSION['error']['olcity'];} ?></td>
              </tr>
              <tr>
                <td align="left">Pays :</td>
                <td align="left"><select name="olcountry">
                    <?php for($i=0;$i<mysql_num_rows($countries);$i++){ 
					$selected = "";
					if(isset($_SESSION['data']['olcountry'])){
						if($_SESSION['data']['olcountry'] == mysql_result($countries,$i,"CountryID")){
							$selected = "SELECTED";
						}
					}else{
						if(mysql_result($user,0,"CountryID") > 0){
							if(mysql_result($user,0,"CountryID") == mysql_result($countries,$i,"CountryID")){
								$selected = "SELECTED";
							}
						}else{
							if(mysql_result($countries,$i,"CountryID") == 66){
								$selected = "SELECTED";	
							}
						}
					}
					?>
                    <option value=<?php echo "\"".mysql_result($countries,$i,"CountryID")."\" ".$selected; ?>><?php echo mysql_result($countries,$i,"CountryNameFre"); ?></option>
                    <?php } ?>
                  </select></td>
                <td class="error"><?php if(isset($_SESSION['error']['olcountry'])){ echo $_SESSION['error']['olcountry'];} ?></td>
              </tr>
              <tr>
                <td align="left">Numéro de téléphone :</td>
                <td align="left"><input type="text" name="olmobile" value="<?php if(isset($_SESSION['data']['olmobile'])){echo $_SESSION['data']['olmobile'];} ?>" /></td>
                <td class="error" align="left"><?php if(isset($_SESSION['error']['olmobile'])){ echo $_SESSION['error']['olmobile'];} ?></td>
              </tr>
              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td></td>
                <td colspan="3" align="left"><input type="hidden" name="order" value="1" />
                  <input type="image" src="<?php echo $http ; ?>images/confirmer.png" /></td>
              </tr>
              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>
            </table>
          </form>
               	</td>                                                             
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>
<?php
	require_once("bottom.php");
	$_SESSION['error'] = array();
	$_SESSION['data'] = array();
}else{
	header("Location: ".$http);
	exit();
}
}else{
	header("Location: ".$http);
	exit();
}
?>
