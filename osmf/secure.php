<?php
$today = gmdate("n/j/Y g:i:s A");
$initial_url = "http://ns01.dramapassion.com/vods/_definst_/rns/smil:rns01-hd.smil/manifest.f4m";
$ip = $_SERVER['REMOTE_ADDR'];
$key = "daHD!75H%x";
$validminutes = 180;
$str2hash = $ip.$key.$today.$validminutes;
$md5raw = md5($str2hash, true);
$base64hash = base64_encode($md5raw);
$urlsignature = "server_time=".$today."&hash_value=".$base64hash."&validminutes=$validminutes";
$base64urlsignature = base64_encode($urlsignature);
$signedurlwithvalidinterval = $initial_url . "?wmsAuthSign=$base64urlsignature"; 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
<link rel="StyleSheet" href="http://www.dramapassion.com/css/my_style.css" type="text/css" media="screen" />
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery-ui.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/swfobject.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/ParsedQueryString.js"></script>
<script type="text/javascript">
        	function loadStrobeMediaPlayback()
        	{
	            // Collect query parameters in an object that we can
	            // forward to SWFObject:
            
	            var pqs = new ParsedQueryString();
	            var parameterNames = pqs.params(false);
	            
	            var parameters = {
	                src: "http://ns01.dramapassion.com/vods/_definst_/rns/smil:rns01-hd.smil/manifest.f4m?wmsAuthSign=<?php echo $base64urlsignature; ?>",
	                autoPlay: false,
					dynamicStreamBufferTime: 12,
					dvrBufferTime: 12,
					liveDynamicStreamingBufferTime: 12,
	                controlBarAutoHide: true,
	                playButtonOverlay: true,
	                showVideoInfoOverlayOnStartUp: false,
	                javascriptCallbackFunction: "onJavaScriptBridgeCreated"	
	            };
	            
	            for (var i = 0; i < parameterNames.length; i++) {
	                var parameterName = parameterNames[i];
	                parameters[parameterName] = pqs.param(parameterName) ||
	                parameters[parameterName];
	            }
	            
	          
	            
	            var wmodeValue = "direct";
	            var wmodeOptions = ["direct", "opaque", "transparent", "window"];
	            if (parameters.hasOwnProperty("wmode"))
	            {
	            	if (wmodeOptions.indexOf(parameters.wmode) >= 0)
	            	{
	            		wmodeValue = parameters.wmode;
	            	}	            	
	            	delete parameters.wmode;
	            }
	            
	            // Embed the player SWF:	            
	            swfobject.embedSWF(
					"http://www.dramapassion.com/swf/StrobeMediaPlayback.swf"
					, "StrobeMediaPlayback"
					, 852
					, 480
					, "10.1.0"
					, "swf/expressInstall.swf"
					, parameters
					, {
		                allowFullScreen: "true",
		                wmode: wmodeValue
		            }
					, {
		                name: "StrobeMediaPlayback"
		            }
				);
				
				
			}
			window.onload = loadStrobeMediaPlayback;
			
			var player = null;
			function onJavaScriptBridgeCreated(playerId)
			{
				if (player == null) {
					player = document.getElementById(playerId);
					
					// Add event listeners that will update the 
					player.addEventListener("isDynamicStreamChange", "updateDynamicStreamItems");
					player.addEventListener("switchingChange", "updateDynamicStreamItems");
					player.addEventListener("autoSwitchChange", "updateDynamicStreamItems");
					player.addEventListener("mediaSizeChange", "updateDynamicStreamItems");
				}
			}
			
			var next = 10;
			function updateDynamicStreamItems()
			{
				document.getElementById("dssc").style.display = "block";
				var dynamicStreams = player.getStreamItems();
				var ds = document.getElementById("dssc-items");
				var switchMode = player.getAutoDynamicStreamSwitch() ? "ON" : "OFF"; 
				var currentStreamIndex = player.getCurrentDynamicStreamIndex();
				var bitrates = ["224p","288p","360p","432p","540p","720p"];
				var playing = "";				
				if (currentStreamIndex == next){
					next = 10;
				}

				if (switchMode == "ON"){
					var dsItems = '<a href="#" class="bt_on" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;"><div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</div></a>';
					for (var idx = 0; idx < dynamicStreams.length; idx ++)
					{
						if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "hold"; }					
						dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
					}
					<?php if($drama_tab['HD'] == 1){?>
					if(dynamicStreams.length < 5 ){
					dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
					dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
					}
					<?php } ?>
				}
				else {
					var dsItems = '<a href="#" class="bt_off" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;"><div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</div></a>';
					if (next == 10)
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "normal"; }
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="' + playing + '" onclick="switchDynamicStreamIndex(' + idx + '); return false;">' + bitrates[idx] + '</a>';				
						}
						<?php if($drama_tab['HD'] == 1){?>
						if(dynamicStreams.length < 5 ){
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
						}
						<?php } ?>
					}
					else
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else if (next == idx) { playing = "waiting"; } else { playing = "hold"; }					
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
						}
						<?php if($drama_tab['HD'] == 1){?>
						if(dynamicStreams.length < 5 ){
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
						}
						<?php } ?>
					}
				}
				ds.innerHTML = dsItems;
			}
			
			function switchDynamicStreamIndex(index)
			{
				if (player.getAutoDynamicStreamSwitch())
				{
					player.setAutoDynamicStreamSwitch(false);	
				}
				player.switchDynamicStreamIndex(index);
				next = index;
			}
</script>
<style type="text/css">
        <!--
			.white {
				color: white;
			}
			
			.white:link{text-decoration:none;color: white;}
			.white:visited{text-decoration:none;color: white;}
			.white:hover{text-decoration:none;color: white;}
			.white:active{text-decoration:none;color: white;}
        	.normal {
				color: #969696;
			}
			.normal:link {text-decoration:none;color: #464646;}
			.normal:visited {text-decoration:none;color: #464646;}
			.normal:hover {text-decoration:none;color:white;}
			.normal:active {text-decoration:none;color:white;}			
			.waiting {
				color: white;
				text-decoration: blink;
			}					
			.waiting:hover {text-decoration:none;}
			.hold {
				color: #464646;
			}
			
			
			
			.hold:hover {text-decoration:none;}			
			.playing {
				color: #E1D275;
			}
			.playing:link {text-decoration:none;color: #E1D275;}
			.playing:visited {text-decoration:none;color: #E1D275;}
			.playing:hover {text-decoration:none;color: #E1D275;}
			.playing:active {text-decoration:none;color: #E1D275;}		
			
			.bt_on:link {text-decoration:none;color: #E1D275;}
			.bt_on:visited {text-decoration:none;color: #E1D275;}
			.bt_on:hover {text-decoration:none;color:white;}
			.bt_on:active {text-decoration:none;color: #E1D275;}	
			
			.bt_off:link {text-decoration:none;color: #464646;}
			.bt_off:visited {text-decoration:none;color: #464646;}
			.bt_off:hover {text-decoration:none;color: #E1D275;}
			.bt_off:active {text-decoration:none;color: #464646;}	
        -->
        </style>
</head>
<body>
<div width="1000" height="505">
<div style="margin-bottom:6px;margin-left:74px;margin-top:15px;border:none;">
<span class="blanc" style="text-transform:uppercase;margin-left:1px;margin-top:15px;font-size:medium;">TEST :</span> <span class="rose" style="text-transform:uppercase;font-size:medium;">Episode XX</span>
</div>
<div style="width:852px;margin-left:74px;">
<div id="StrobeMediaPlayback">
	Alternative content
</div>
<div id="dssc" style="display:none;margin-top:10px;margin-left:20px;width:980px;height:5px;font-size:12px;">
	<div style="float:left;" class="white"><a target="_blank" href="http://www.dramapassion.com/guide_lecteur" title="Plus d'info" class="white" >Qualité (info) :</a></div>
	<div id="dssc-items" style="float:left;">
		The available qualities will be loaded once the playback starts...
	</div>
</div>	
</div>
</div>
</body>
</html>
