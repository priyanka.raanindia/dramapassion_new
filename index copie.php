<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
	$new_epi_free = New_epi_drama("Free");
	$new_epi_pay = New_epi_drama("Pay");
	
	$tab_info_pay = DramaInfo($new_epi_pay[0]['dramaID']);
	$tab_info_free = DramaInfo($new_epi_free[0]['dramaID']);
	
	$chaine = $tab_info_pay['synopsis'];
	$chaine2 = $tab_info_free['synopsis'];
	
	$lg_max = 200; //nombre de caractère autoriser

	if (strlen($chaine) > $lg_max){
		$chaine = substr($chaine, 0, $lg_max);
		$last_space = strrpos($chaine, " ");
		$chaine = substr($chaine, 0, $last_space)."...";
	}
	$synopsis = $chaine;
	if (strlen($chaine2) > $lg_max){
		$chaine2 = substr($chaine2, 0, $lg_max);
		$last_space = strrpos($chaine2, " ");
		$chaine2 = substr($chaine2, 0, $last_space)."...";
	}
	$synopsis2 = $chaine2;
	//print_r($_SESSION);
	$os = getOs();

?>
<?php // print_r($_SESSION); ?>

<tr>
        <td valign="top">  
        <!-- BLOC JQUERY -->
		<?php if($os == 'IOS' || $os == 'Android'){
		?><div style="width:1000px;margin:auto;">
		<?php
		}else{ ?>
        <div style="width:1070px;margin:auto;">
		<?php } ?>
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
                    <td colspan="3" height="10" bgcolor="#FFFFFF"><!-- ZONE BLANCHE --></td>
                </tr>
                
                <tr>
				<?php if($os == 'IOS' || $os == 'Android'){
				
				}else{ ?>
                    <td width="10" bgcolor="#FFFFFF" height="320"><a href="javascript:"><img src="images/fleche_gauche.png" class="carrousel-prev" id="carrousel-prev"></a></td>
				<?php } ?>
					<td>
					<ul class="carrousel" id="carrousel_feed">
					
					<?php 
					if($_SESSION['userid'] == 3){
					Car_aff3(); 	
					//print_r($tab_trailer);
					}else{
					Car_aff3(); 
					}
					
					?>
					
					</ul>
					</td>
				<?php if($os == 'IOS' || $os == 'Android'){
				}else{ ?>
                    <td width="10" bgcolor="#FFFFFF"><a href="javascript:"><img src="images/fleche_droite.png" class="carrousel-next" id="carrousel-next"></a></td>
				<?php } ?>
			   </tr>
                
                <tr>
                    <td colspan="4" bgcolor="#FFFFFF" width="1000" height="10"><!-- ZONE BLANCHE --></td>
                </tr>

			</table>
		</div>
<!-- BLOC JQUERY -->     
        </td>
	</tr>
    
	<tr>
        <td valign="top" height="100">  
        <!-- BLOC PUB-->
        <div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="109">
                <tr>
                	<td>
                    <div style="width:728px;margin:auto;">
                    <!-- <a href="<?php echo $http ; ?>premium/"><img src="images/pub_728par90.jpg" width="728" height="90" alt="Publicité" title="Promotion" border="0"></a> -->
                    <!-- home_leaderboard -->
                    <div id='div-gpt-ad-1346334604979-0' style='width:728px; height:90px;'>
                    <script type='text/javascript'>
	                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1346334604979-0'); });
	                </script>
	                </div>
                    </div>
                    </td>
                </tr>
                
                <tr>
                    <td bgcolor="#FFFFFF" width="1000" height="19"><img src="images/ligne.jpg" width="1000" height="19" alt=""></td>
                </tr>
                                
			</table>
		</div>
        <!-- FIN BLOC PUB -->        
        </td>
	</tr>
        
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
		
        <div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0">
                <tr>
                	<td width="720" valign="top">
                    <!-- CADRE DE GAUCHE -->
					<table width="720" cellpadding="0" cellspacing="0" border="0">
						<tr>
                        	<td colspan="3">
                            <h1 class="menu_noir">Dernières sorties</h1>
                            <img src="images/ligne720.jpg" width="720">
                            <br>
                            </td>
						</tr>
						<tr>
                        	<td width="340" valign="top">
							<div class="a_premium_free"><img src="images/abonnement_1.png" alt="Premium" title="Premium"   border="0"></div>
                            <br />
							<a href="drama/<?php echo $new_epi_pay[0]['dramaID']."/".$new_epi_pay[0]['link'] ; ?>" class="img_play_detail" ><div style="width:340px;height:253px;background-image:url(<?php echo $http.$new_epi_pay[0]['img'] ; ?>);background-size:340px 253px;position:relative;">
							<?php
							
								$date = $new_epi_pay[0]['date'].' 12:00:00';
								$date = strtotime($date);
								$date = $array_jour_fr_p[date('l',$date)];
								?>
								<div class="date_sortie_premium"><?php echo $date ; ?></div>
								
							</div></a>
							</td>
                        	<td width="40"></td>
                        	<td width="340" valign="top">
							<div class="a_premium_free"><img src="images/abonnement_2.png" alt="Gratuit" title="Gratuit"   border="0"></div>
							<br />
							<a href="drama/<?php echo $new_epi_free[0]['dramaID']."/".$new_epi_free[0]['link'] ; ?>" class="img_play_detail" ><div style="width:340px;height:253px;background-image:url(<?php echo $http.$new_epi_free[0]['img'] ; ?>);background-size:340px 253px;position:relative;">
							<?php
							
								$date = $new_epi_free[0]['date'].' 12:00:00';
								$date = strtotime($date);
								$date = $array_jour_fr_p[date('l',$date)];
								?>
								<div class="date_sortie_free"><?php echo $date ; ?></div>
								
							</div></a>
							</td>
						</tr>
						<tr>
                        	<td class="noir" align="justify" valign="top">
                            <div style="padding-top:8px;"></div>
                            <h2 class="h2_index"><a href="drama/<?php echo $new_epi_pay[0]['dramaID']."/".$new_epi_pay[0]['link'] ; ?>" class="lien_menu_noir"><?php echo $new_epi_pay[0]['drama'] ; ?></a></h2></span><span class="plus_index"><?php playlist_bt($new_epi_pay[0]['dramaID']) ; ?></span>
							<div class="supprimer_float" style="clear:both;"></div>
							<?php echo $new_epi_pay[0]['char'] ; ?><br />
                            <?php echo $synopsis ; ?>
                            <br /><br />
                            </td>
                            <td></td>
							<td class="noir" align="justify" valign="top">
                            <div style="padding-top:8px;"></div>
                             <h2 class="h2_index"><a href="drama/<?php echo $new_epi_free[0]['dramaID']."/".$new_epi_free[0]['link'] ; ?>" class="lien_menu_noir"><?php echo $new_epi_free[0]['drama'] ; ?></a></h2></span><span class="plus_index"><?php playlist_bt($new_epi_free[0]['dramaID']) ; ?></span></h2>
							 <div class="supprimer_float" style="clear:both;"></div>
							 <?php echo $new_epi_free[0]['char'] ; ?><br />
                            <?php echo $synopsis2 ; ?>
                            <br /><br />
							</td>                            
						</tr>
						<tr>
                        	<td colspan="3">
                            <br />
                            <table width="720" cellpadding="0" cellspacing="0" class="noir">
                            	<tr>
                                	<td width="160">
                                	<a href="drama/<?php echo $new_epi_pay[1]['dramaID']."/".$new_epi_pay[1]['link'] ; ?>" class="img_play_detail" ><div style="width:144px;height:85px;background-image:url(<?php echo $http.$new_epi_pay[1]['img'] ; ?>);position:relative;background-size:144px 85px;float:left;margin-bottom:5px;">
                                	<?php
							
								$date = $new_epi_pay[1]['date'].' 12:00:00';
								$date = strtotime($date);
								$date = $array_jour_fr_p[date('l',$date)];
								?>
								<div class="date_sortie_premium_mini"><?php echo $date ; ?></div>
								
							</div></a>
                                	<?php //playlist_bt($new_epi_pay[1]['dramaID']) ; ?>
                                	
                                	
                                	</td>
                                    <td width="20"></td>
									<td width="160">
									<a href="drama/<?php echo $new_epi_pay[2]['dramaID']."/".$new_epi_pay[2]['link'] ; ?>" class="img_play_detail" ><div style="width:144px;height:85px;background-image:url(<?php echo $http.$new_epi_pay[2]['img'] ; ?>);position:relative;background-size:144px 85px;float:left;margin-bottom:5px;">
									<?php
							
								$date = $new_epi_pay[2]['date'].' 12:00:00';
								$date = strtotime($date);
								$date = $array_jour_fr_p[date('l',$date)];
								?>
								<div class="date_sortie_premium_mini"><?php echo $date ; ?></div>
								
							</div></a>
									<?php //playlist_bt($new_epi_pay[2]['dramaID']) ; ?>
									
									</td>
                                    <td width="40"></td>
									<td width="160">
									<a href="drama/<?php echo $new_epi_free[1]['dramaID']."/".$new_epi_free[1]['link'] ; ?>" class="img_play_detail" ><div style="width:144px;height:85px;background-image:url(<?php echo $http.$new_epi_free[1]['img'] ; ?>);position:relative;background-size:144px 85px;float:left;margin-bottom:5px;">
									
									<?php
							
								$date = $new_epi_free[1]['date'].' 12:00:00';
								$date = strtotime($date);
								$date = $array_jour_fr_p[date('l',$date)];
								?>
								<div class="date_sortie_free_mini"><?php echo $date ; ?></div>
								
							</div></a>
							<?php //playlist_bt($new_epi_free[1]['dramaID']) ; ?>
									</td>
                                    <td width="20"></td>
									<td width="160">
									<a href="drama/<?php echo $new_epi_free[2]['dramaID']."/".$new_epi_free[2]['link'] ; ?>" class="img_play_detail" ><div style="width:144px;height:85px;background-image:url(<?php echo $http.$new_epi_free[2]['img'] ; ?>);position:relative;background-size:144px 85px;float:left;margin-bottom:5px;">
									
									<?php
							
								$date = $new_epi_free[2]['date'].' 12:00:00';
								$date = strtotime($date);
								$date = $array_jour_fr_p[date('l',$date)];
								?>
								<div class="date_sortie_free_mini"><?php echo $date ; ?></div>
								
							</div></a>
							<?php //playlist_bt($new_epi_free[2]['dramaID']) ; ?>
									</td>
								</tr>
								<tr>
                                	<td valign="top"><a href="drama/<?php echo $new_epi_pay[1]['dramaID']."/".$new_epi_pay[1]['link'] ; ?>" class="lien_noir"><?php echo $new_epi_pay[1]['drama'] ; ?></a></td>
									<td width="20"></td>
									<td valign="top"><a href="drama/<?php echo $new_epi_pay[2]['dramaID']."/".$new_epi_pay[2]['link'] ; ?>" class="lien_noir"><?php echo $new_epi_pay[2]['drama'] ; ?></a></td>
                                    <td valign="top"></td>
                                    <td valign="top"><a href="drama/<?php echo $new_epi_free[1]['dramaID']."/".$new_epi_free[1]['link'] ; ?>" class="lien_noir"><?php echo $new_epi_free[1]['drama'] ; ?></a></td>
                                    <td width="20"></td>
									<td valign="top"><a href="drama/<?php echo $new_epi_free[2]['dramaID']."/".$new_epi_free[2]['link'] ; ?>" class="lien_noir"><?php echo $new_epi_free[2]['drama'] ; ?></a></td>
								</tr> 
                            	<tr>
                                	<td valign="top"><div class="cont_align_logo"><div class="align_logo"><img  src="images/abonnement_1_mini.png" style="width:17px"/></div><div class="titre_logo_align"><?php echo $new_epi_pay[1]['char'] ; ?></div><div style="clear:both;"></div></div></td>
                                    <td width="20"></td>
									<td valign="top"><div class="cont_align_logo"><div class="align_logo"><img  src="images/abonnement_1_mini.png" style="width:17px"/></div><div class="titre_logo_align"><?php echo $new_epi_pay[2]['char'] ; ?></div><div style="clear:both;"></div></div></td>
                                    <td valign="top"> </td>
                                    <td valign="top"><div class="cont_align_logo"><div class="align_logo"><img  src="images/abonnement_2_mini.png" style="width:17px"/></div><div class="titre_logo_align"><?php echo $new_epi_free[1]['char'] ; ?></div><div style="clear:both;"></div></div></td>
                                    <td width="20"></td>
									<td valign="top"><div class="cont_align_logo"><div class="align_logo"><img  src="images/abonnement_2_mini.png" style="width:17px"/></div><div class="titre_logo_align"><?php echo $new_epi_free[2]['char'] ; ?></div><div style="clear:both;"></div></div></td>
								</tr>      
                            	                                                         
							</table>                                                                    
                            </td>
						</tr>
						<tr>
                        	<td colspan="3" height="45" valign="middle" width="720">
                            &nbsp; <br> &nbsp;
                            </td>
						</tr> 
					</table>
	<!--ICI du php -->				
	<?php
	$com = CatalogueAff('CataCom') ;
	$melo = CatalogueAff('CataMelo');
	$act = CatalogueAff('CataAct');
	$hist = CatalogueAff('CataHist');
	
	$imgCom = CatalogueImg($com);
	$imgMelo = CatalogueImg($melo);
	$imgAct = CatalogueImg($act);
	$imgHist = CatalogueImg($hist);
	
	$allCom = NomTypeDrama($com);
	$allMelo = NomTypeDrama($melo);
	$allAct = NomTypeDrama($act);
	$allHist = NomTypeDrama($hist);
	
	$lienCom[1] = "drama/".$com[1].'/'.dramaLinkClean($allCom[10]) ;
	$lienCom[2] = "drama/".$com[2].'/'.dramaLinkClean($allCom[20]) ;
	$lienCom[3] = "drama/".$com[3].'/'.dramaLinkClean($allCom[30]) ;
	$lienMelo[1] = "drama/".$melo[1].'/'.dramaLinkClean($allMelo[10]) ;
	$lienMelo[2] = "drama/".$melo[2].'/'.dramaLinkClean($allMelo[20]) ;
	$lienMelo[3] = "drama/".$melo[3].'/'.dramaLinkClean($allMelo[30]) ;
	$lienAct[1] = "drama/".$act[1].'/'.dramaLinkClean($allAct[10]) ;
	$lienAct[2] = "drama/".$act[2].'/'.dramaLinkClean($allAct[20]) ;
	$lienAct[3] = "drama/".$act[3].'/'.dramaLinkClean($allAct[30]) ;
	$lienHist[1] = "drama/".$hist[1].'/'.dramaLinkClean($allHist[10]) ;
	$lienHist[2] = "drama/".$hist[2].'/'.dramaLinkClean($allHist[20]) ;
	$lienHist[3] = "drama/".$hist[3].'/'.dramaLinkClean($allHist[30]) ;
	
	?>
					<table width="720" cellpadding="0" cellspacing="0" class="noir">
                    	<tr>
                        	<td colspan="4">
                            <h1 class="menu_noir">Catalogue VOD</h1>
							</td>
							<td align="right">
							<a href="<?php $http ;?>/catalogue/" class="lien_noir plus_up">voir tout le catalogue<img src="images/plus.jpg" border="0" align="absmiddle" class="plus_up_img"></a>
                            </td>
						</tr>
						<tr>
							<td colspan="5">
							<img src="images/ligne720.jpg" width="720">
                            <br>
							</td>
						</tr>
                        <tr>
                        	<td colspan="4"><h2 class="sous_menu_noir">Comédie / Comédie Romantique</h2></td>
                            <td align="right">
                            <a href="<?php $http ;?>/catalogue/Comedies" class="lien_noir plus_up">voir tous <img src="images/plus.jpg" border="0" align="absmiddle" class="plus_up_img"></a>
                            </td>
						</tr>                            
                        <tr>
                        	<td valign="top" width="200"><a href="<?php echo $lienCom[1] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allCom[10] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allCom[10] ;?>&quot; en vostfr" id="<?php echo $allCom[15] ; ?>" src="<?php echo $http.$imgCom[1] ;?>" class="img_thumb"></a><?php playlist_bt($com[1]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienCom[2] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allCom[20] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allCom[20] ;?>&quot; en vostfr" id="<?php echo $allCom[25] ; ?>" src="<?php echo $http.$imgCom[2] ;?>" class="img_thumb"></a><?php playlist_bt($com[2]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienCom[3] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allCom[30] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allCom[30] ;?>&quot; en vostfr" id="<?php echo $allCom[35] ; ?>" src="<?php echo $http.$imgCom[3] ;?>" class="img_thumb"></a><?php playlist_bt($com[3]) ; ?></td>
						</tr>   
                        <tr>
                        	<td valign="top"><a href="<?php echo $lienCom[1] ; ?>" class="lien_noir"><?php echo $allCom[10] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienCom[2] ; ?>" class="lien_noir"><?php echo $allCom[20] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienCom[3] ; ?>" class="lien_noir"><?php echo $allCom[30] ;?></a></td>
						</tr>      
						<tr>
                        	<td valign="top"><?php echo $allCom[11] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allCom[21] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allCom[31] ;?></td>
						</tr>  
                        <tr>
                        	<td colspan="5" height="45" valign="middle">
                            <center><img src="images/ligne4.jpg"></center>
                            </td>
						</tr> 
                        
                        <tr>
                        	<td colspan="4"><h2 class="sous_menu_noir">Drame</h2></td>
                            <td align="right">
                            <a href="<?php $http ;?>/catalogue/Drame" class="lien_noir plus_up">voir tous <img src="images/plus.jpg" border="0" align="absmiddle" class="plus_up_img"></a>
                            </td>
						</tr>                            
                        <tr>
                        	<td valign="top" width="200"><a href="<?php echo $lienMelo[1] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allMelo[10] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allMelo[10] ;?>&quot; en vostfr" id="<?php echo $allMelo[15] ; ?>" src="<?php echo $http.$imgMelo[1] ;?>" class="img_thumb"></a><?php playlist_bt($melo[1]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienMelo[2] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allMelo[20] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allMelo[20] ;?>&quot; en vostfr" id="<?php echo $allMelo[25] ; ?>" src="<?php echo $http.$imgMelo[2] ;?>" class="img_thumb"></a><?php playlist_bt($melo[2]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienMelo[3] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allMelo[30] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allMelo[30] ;?>&quot; en vostfr" id="<?php echo $allMelo[35] ; ?>" src="<?php echo $http.$imgMelo[3] ;?>" class="img_thumb"></a><?php playlist_bt($melo[3]) ; ?></td>
						</tr>   
                        <tr>
                        	<td valign="top"><a href="<?php echo $lienMelo[1] ; ?>" class="lien_noir"><?php echo $allMelo[10] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienMelo[2] ; ?>" class="lien_noir"><?php echo $allMelo[20] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienMelo[3] ; ?>" class="lien_noir"><?php echo $allMelo[30] ;?></a></td>
						</tr>  
                        <tr>
                        	<td valign="top"><?php echo $allMelo[11] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allMelo[21] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allMelo[31] ;?></td>
						</tr>                                                          
                        <tr>
                        	<td colspan="5" height="45" valign="middle">
                            <center><img src="images/ligne4.jpg"></center>
                            </td>
						</tr>  
                        
                        <tr>
                        	<td colspan="4"><h2 class="sous_menu_noir">Historique</h2></td>
                            <td align="right">
                            <a href="<?php $http ;?>/catalogue/Historique" class="lien_noir plus_up">voir tous <img src="images/plus.jpg" border="0" align="absmiddle" class="plus_up_img"></a>
                            </td>
						</tr>                            
                          
						<tr>
                        	<td valign="top" width="200"><a href="<?php echo $lienHist[1] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allHist[10] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allHist[10] ;?>&quot; en vostfr" id="<?php echo $allHist[15] ; ?>" src="<?php echo $http.$imgHist[1] ;?>" class="img_thumb"></a><?php playlist_bt($hist[1]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienHist[2] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allHist[20] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allHist[20] ;?>&quot; en vostfr" id="<?php echo $allHist[25] ; ?>" src="<?php echo $http.$imgHist[2] ;?>" class="img_thumb"></a><?php playlist_bt($hist[2]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienHist[3] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allHist[30] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allHist[30] ;?>&quot; en vostfr" id="<?php echo $allHist[35] ; ?>" src="<?php echo $http.$imgHist[3] ;?>" class="img_thumb"></a><?php playlist_bt($hist[3]) ; ?></td>
						</tr>  
                        <tr>
                        	<td valign="top"><a href="<?php echo $lienHist[1] ; ?>" class="lien_noir"><?php echo $allHist[10] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienHist[2] ; ?>" class="lien_noir"><?php echo $allHist[20] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienHist[3] ; ?>" class="lien_noir"><?php echo $allHist[30] ;?></a></td>
						</tr>  
                        <tr>
                        	<td valign="top"><?php echo $allHist[11] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allHist[21] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allHist[31] ;?></td>
						</tr>                                                         
                        <tr>
                        	<td colspan="5" height="45" valign="middle">
                            <center><img src="images/ligne4.jpg"></center>
                            </td>
						</tr> 	
                        
                        <tr>
                        	<td colspan="4"><h2 class="sous_menu_noir">Action / Thriller / Fantastique</h2></td>
                            <td align="right">
                            <a href="<?php $http ;?>/catalogue/Action-Thriller-Fantastique" class="lien_noir plus_up">voir tous <img src="images/plus.jpg" border="0" align="absmiddle" class="plus_up_img"></a>
                            </td>
						</tr>  
						
						<tr>
                        	<td valign="top" width="200"><a href="<?php echo $lienAct[1] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allAct[10] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allAct[10] ;?>&quot; en vostfr" id="<?php echo $allAct[15] ; ?>" src="<?php echo $http.$imgAct[1] ;?>" class="img_thumb"></a><?php playlist_bt($act[1]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienAct[2] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allAct[20] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allAct[20] ;?>&quot; en vostfr" id="<?php echo $allAct[25] ; ?>" src="<?php echo $http.$imgAct[2] ;?>" class="img_thumb"></a><?php playlist_bt($act[2]) ; ?></td>
                        	<td valign="top" width="60"></td>
                        	<td valign="top" width="200"><a href="<?php echo $lienAct[3] ; ?>" class="img_play_thumb" ><img class="img_index_thumb" title="drama coréen &quot;<?php echo $allAct[30] ;?>&quot; en vostfr" alt="série coréenne &quot;<?php echo $allAct[30] ;?>&quot; en vostfr" id="<?php echo $allAct[35] ; ?>" src="<?php echo $http.$imgAct[3] ;?>" class="img_thumb"></a><?php playlist_bt($act[3]) ; ?></td>
						</tr>   
                        <tr>
                        	<td valign="top"><a href="<?php echo $lienAct[1] ; ?>" class="lien_noir"><?php echo $allAct[10] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienAct[2] ; ?>" class="lien_noir"><?php echo $allAct[20] ;?></a></td>
                            <td valign="top"></td>
                            <td valign="top"><a href="<?php echo $lienAct[3] ; ?>" class="lien_noir"><?php echo $allAct[30] ;?></a></td>
						</tr>   
                        <tr>
                        	<td valign="top"><?php echo $allAct[11] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allAct[21] ;?></td>
                            <td valign="top"></td>
                            <td valign="top"><?php echo $allAct[31] ;?></td>
						</tr>                                                        
					</table>
					<br /><br />
                    </td>
                    
                    
                    
                    <td width="30"></td>
                    
                    <td width="250" valign="top" >
					<!-- CADRE DE DROITE -->
						<!-- <div id='div-gpt-ad-1346334604979-2' style='width:250px; height:250px;'>
						<script type='text/javascript'>
							googletag.cmd.push(function() { googletag.display('div-gpt-ad-1346334604979-2'); });
						</script>
						</div><br /> -->
						<?php comming_soon() ; ?><br />
						<?php //AffPub("2") ; ?>   
						<table cellpadding="0" cellspacing="0" width="250">
							
						<?php 
						AffTop10();
						?>
                                                      
						</table>   <br /><br />         
						<?php AffPub("10") ; ?>
			
						
						
						  
                    
						<br />
						<div class="fb-like-box" data-href="https://www.facebook.com/dramapassion" data-width="250" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
                   
						<br /><br />
						<?php AffPub("9") ; ?>
						<br />
						<br />
						
						<table cellpadding="0" cellspacing="0" width="250">
							
						<?php //AffPub(3); ?>
						<!-- small_rectangle_2
						<div id='div-gpt-ad-1346334604979-1' style='width:250px; height:250px;'>
						<script type='text/javascript'>
							//googletag.cmd.push(function() { googletag.display('div-gpt-ad-1346334604979-1'); });
						</script>
						</div> -->
						<!-- small_rectangle_all_pages -->  
                                                      
						</table>  
                   
                    </td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>
<script type="text/javascript" language="javascript" src="<?php echo $http ; ?>js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script>
$(document).ready(function() {
    // Using default configuration
    $('#carrousel_feed').carouFredSel({
    	direction           : "left",
    	auto: {
	    	timeoutDuration : 5000,
	    	pauseOnHover: 'resume'
    	},
	     scroll : {
		            duration        : 1000
		            
		        },
		 prev : "#carrousel-prev",
		 next : "#carrousel-next"
    });
    
    <?php
	    
	    
	   
	   
	 foreach($tab_trailer as $key => $value){
		 
			 
		echo "
                     	
						    jwplayer('player_trailer_".$value."').setup({
						        file: '".$tab_trailer_url[$value]."',
						        title: 'Trailer - ".$tab_trailer_name[$value]."',
						        width: '100%',
						        aspectratio: '16:9',
						        skin: 'five',
						        width: 0,
						        height: 0,
						        tracks: [{ 
							            file: '".$tab_trailer_shortcut[$value]."', 
							            label: 'Francais',
							            kind: 'captions',
							            'default': true 
							    }]
						    });
						    $('#btn_trailer_".$value."').click(function(){
								drama_id = $( this ).data('id');
								eta = $( this ).data('eta');
								btn_trailer = '".$http."images/btn_trailer.png';
								btn_image = '".$http."images/btn_affiche.png';
								
								if(eta == 0){
									$('#img_drama_'+drama_id).hide();
									$( this ).data('eta',1);
									$( this ).attr('src', btn_image);
									jwplayer('player_trailer_".$value."').resize(640,320);
									jwplayer('player_trailer_".$value."').play();
									$('#carrousel_feed').trigger('stop');
									jwplayer('player_trailer_".$value."').onComplete(function(){
		   
									   setTimeout(function(){ 
										   	$('#img_drama_'+drama_id).show();
										   	$( '#btn_trailer_".$value."' ).data('eta',0);
										   	$( '#btn_trailer_".$value."' ).attr('src', btn_trailer);
										   	jwplayer('player_trailer_".$value."').resize(0,0);
										   	jwplayer('player_trailer_".$value."').stop();
									   		$('#carrousel_feed').trigger('play',true); 
									   }, 2000);
									   
								    });
									
								}else if(eta = 1){
									$('#img_drama_'+drama_id).show();
									$( this ).data('eta',0);
									$( this ).attr('src', btn_trailer);
									jwplayer('player_trailer_".$value."').resize(0,0);
									jwplayer('player_trailer_".$value."').stop();
									$('#carrousel_feed').trigger('play',true);
									
								}
								
								
								 
								
							});
						
                     	";	 
			 
			 
	 }   
	    
	    
	 ?>   
    
   

});
/*
var Global_src = "test" ;
$(".img_play_detail").mouseover(function() {
	var srcIMG = $(this).find(".img_detail").attr("id");
	Global_src = $(this).find(".img_detail").attr("src");
	var src = "image_play.php?img="+srcIMG+"&type=detail";
    $(this).find(".img_detail").attr("src", src);
  }).mouseout(function(){
    var src2 = Global_src;
	
    $(this).find(".img_detail").attr("src", src2);
  });
  $(".img_play_thumb").mouseover(function() {
	var srcIMG = $(this).find(".img_thumb").attr("id");
	Global_src = $(this).find(".img_thumb").attr("src");
	var src = "image_play.php?img="+srcIMG+"&type=thumb";
    $(this).find(".img_thumb").attr("src", src);
  }).mouseout(function(){
    var src2 = Global_src;
	
    $(this).find(".img_thumb").attr("src", src2);
  });
 */
$(".plus_up").mouseover(function(){
	var src = "images/plus_up.jpg";
    $(this).find(".plus_up_img").attr("src", src);
}).mouseout(function(){
	var src2 = "images/plus.jpg";
    $(this).find(".plus_up_img").attr("src", src2);
});
</script>

<?php require_once("bottom.php");?>