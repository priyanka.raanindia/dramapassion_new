<?php

if(!isset($_COOKIE['__trk'])){
	$unique = uniqid();
	$cookie = md5($unique.$_SERVER['REMOTE_ADDR']);
	setcookie("__trk", $cookie, time()+15552000, "/");
}else{
	setcookie("__trk", $_COOKIE['__trk'], time()+15552000, "/");
}

$cookiesIp = $_COOKIE['dpip'];
if($cookiesIp != 'ok'){
	setcookie("dpip", 'ok',  time() + (3600 * 24 * 90), "/");
}


require_once('includes/Mobile_Detect.php');
require_once("includes/settings.inc.php");
require_once("includes/dbinfo.inc.php");
require_once("includes/functions.php");
require_once("logincheck.php");
require_once("langue/fr.php");
//require_once("locationcheck.php");

$detect_os = new Mobile_Detect();

$user_lvl = $_SESSION['subscription'];

$os = getOs();


if(isset($_SESSION['userid']) && $_SESSION['userid'] > 0){
	$_SESSION['balance'] = mysql_result(mysql_query("SELECT UserBalance FROM t_dp_user WHERE UserID = ".$_SESSION['userid']),0,"UserBalance");	
}

$page = str_replace(".php","",basename($_SERVER['PHP_SELF']));

$title = _ttlindex_;
$description = "";
$keywords = "";
$footer = "";
$sociallinks = false;
$og_title = _ttlindex_ ;
$og_img = $http."images/logo_facebook.jpg";
$og_desc = _dscindex_;


if($page == "index"){
	$title = _ttlindex_;
	$description = _dscindex_;
	$keywords = _keyindex_;
	$sociallinks = true ;
	
	$og_title = _ttlindex_ ;
	$og_img = $http."images/logo_facebook.jpg";
	$og_desc = _dscindex_;
	if($detect_os->isTablet()){
	
	if(isset($_COOKIE['__AppliDramapassion'])){
	$cookiez = $_COOKIE['__AppliDramapassion'];
	if($cookiez == 1){
			$verif_app = 0;
		}else{
			$verif_app = 1;
		}
	
	}else{
		$verif_app = 1;
	
	}
	}
	if($os == 'RT' || $os == 'Android'){
		$verif_app = 0;
	}
	
}elseif($page == "drama2"){
	$dramaID = cleanup($_GET['dramaID']);
	$drama_info_meta = DramaInfo($dramaID);
	
	$title = _ttldrama2_;
	$title = str_replace("%%title%%",$drama_info_meta['titre'],$title);
	
	$description = _dscdrama2_;
	$synopsis = substr($drama_info_meta['synopsis'],0,200);
		if(strlen($drama_info_meta['synopsis']) > 200){
			$synopsis.= "...";	
		}
	$description =  str_replace("%%title%%",$drama_info_meta['titre'],$description);
	$description =  str_replace("%%synopsis%%",$synopsis,$description);
	
	$keywords = _keydrama2_;
	$keyword_titre = $drama_info_meta['titre'].', '.$drama_info_meta['keyword'];
	$keywords = str_replace("%%title%%",$keyword_titre,$keywords);
	
	$sociallinks = true;
	$og_title = $drama_info_meta['titre'];
	$og_img = $drama_info_meta['img_facebook'];
	$og_desc = $synopsis;
	
}elseif($page == "player"){
	$dramaID = cleanup($_GET['dramaID']);
	$drama_info_meta = DramaInfo($dramaID);
	$epiNB = cleanup($_GET['epiNB']);
	$type = cleanup($_POST['hidden_type']);
	
	if($epiNB < 10){
		$epiNB2 = $chaine = substr($epiNB,1); 
	}
	
	$title = _ttlplayer_;
	$title = str_replace("%%title%%",$drama_info_meta['titre'],$title);
	$title = str_replace("%%type%%",$type,$title);
	$title = str_replace("%%numEpi%%",$epiNB2,$title);
	$title = str_replace("free","Gratuit",$title);
	$title = str_replace("sd","SD",$title);
	$title = str_replace("hd","HD",$title);
	
	
	$description = _dscplayer_;
	$synopsis = substr($drama_info_meta['synopsis'],0,200);
		if(strlen($drama_info_meta['synopsis']) > 200){
			$synopsis.= "...";	
		}
	$description =  str_replace("%%title%%",$drama_info_meta['titre'],$description);
	$description =  str_replace("%%synopsis%%",$synopsis,$description);
	
	$keywords = _keyplayer_;
	$keyword_titre = $drama_info_meta['titre'].', '.$drama_info_meta['keyword'];
	$keywords = str_replace("%%title%%",$keyword_titre,$keywords);
	
	$sociallinks = true;
	
	$sociallinks = true;
	$og_title = $drama_info_meta['titre']." - ".$type." Episode ".$epiNB;
	$og_title = str_replace("free","Gratuit",$og_title);
	$og_title = str_replace("sd","SD",$og_title);
	$og_title = str_replace("hd","HD",$og_title);
	$og_img = $drama_info_meta['img_detail'];
	$og_desc = $synopsis;
	
}elseif($page == "premium2"){
	$title = _ttlpremium2_;
	$description = _dscpremium2_;
	$keywords = _keypremium2_;
}elseif($page == "catalogue2"){
	$title = _ttlcatalogue2_;
	$description = _dsccatalogue2_;
	$keywords = _keycatalogue2_;
}elseif($page == "dvd2"){
	$title = _ttldvd2_;
	$description = _dscdvd2_;
	$keywords = _keydvd2_;
}elseif($page == "dvd_detail"){
	$title = _ttldvdDetail_;
	$description = _dscdvdDetail_;
	$keywords = _keydvdDetail_;
}elseif($page == "qui_somme_nous"){
	$title = _ttlquiSommeNous_;
	$description = _dscquiSommeNous_;
	$keywords = _keyquiSommeNous_;
}elseif($page == "condi_general"){
	$title = _ttlcondiGeneral_;
	$description = _dsccondiGeneral_;
	$keywords = _keycondiGeneral_;
}elseif($page == "protect_donnee"){
	$title = _ttlprotectDonnee_;
	$description = _dscprotectDonnee_;
	$keywords = _keyprotectDonnee_;
}elseif($page == "faq"){
	$title = _ttlfaq_;
	$description = _dscfaq_;
	$keywords = _keyfaq_;
}elseif($page == "contact"){
	$title = _ttlcontact_;
	$description = _dsccontact_;
	$keywords = _keycontact_;
}elseif($page == "mon_compte"){
	$title = _ttlmonCompte_;
	$description = _dscmonCompte_;
	$keywords = _keymonCompte_;
}elseif($page == "mon_abo"){
	$title = _ttlmonAbo_;
	$description = _dscmonAbo_;
	$keywords = _keymonAbo_;
}elseif($page == "playlist_aff"){
	$title = _ttlplaylistAff_;
	$description = _dscplaylistAff_;
	$keywords = _keyplaylistAff_;
}elseif($page == "parrainage2"){
	$title = "Dramapassion - Parrainage";
	$description = "Offre parrainage";
	$keywords = "dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger";
}

$url_serv = $http3.$_SERVER['REQUEST_URI'];
$url_replace = str_replace('/', '%2F' ,$url_serv);
$url_req = "https://www.facebook.com/sharer.php?u=http%3A%2F%2F".$url_replace;


if($page == "player"){
$info = DramaInfo($dramaID);
$titre = $info['titre'];
$titre_clean = dramaLinkClean($titre);
$url_face = $http."drama/".$dramaID."/".$titre_clean;
$facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count" data-href="'.$url_face.'"></div></td>';

}elseif($page == 'index'){
$facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/dramapassion" ></div></td>';

}else{
$facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div></td>';
$url_face = $http2.$_SERVER['REQUEST_URI'];

}


?>
<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<meta name="theme-color" content="#FF0066">


<!-- Bootstrap -->
<link href="<?php echo $server; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $server; ?>css/bt_style.css?<?php echo time() ; ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $server; ?>/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $server; ?>js/bootstrap.min.js"></script>  
    <script src="<?php echo $server; ?>js/jquery.twbsPagination.js"></script>  
<!-- 1. skin -->
<link rel="stylesheet" href="//releases.flowplayer.org/6.0.5/skin/functional.css">

 
<!-- 3. flowplayer -->
<script src="//releases.flowplayer.org/6.0.5/flowplayer.min.js"></script>    
    
    <!-- optional: the quality selector stylesheet -->
<link rel="stylesheet" href="//releases.flowplayer.org/quality-selector/flowplayer.quality-selector.css">
 
 
<!-- load the Flowplayer hlsjs engine, including hls.js -->
<script src="//releases.flowplayer.org/hlsjs/flowplayer.hlsjs.min.js"></script>
 
<!-- optional: the quality selector plugin for quality selection when hlsjs is not supported -->
<script src="//releases.flowplayer.org/quality-selector/flowplayer.quality-selector.min.js"></script>

<script>
	flowplayer.conf.embed = false;
	</script>

<link rel="shortcut icon" type="image/png" href="<?php echo $http ; ?>logo.png" /> 
<link rel="icon" type="image/png" href="<?php echo $http ; ?>logo.png" /> 

<!-- meta -->
<title><?php echo $title;?></title>
<meta name="description" lang="fr" content="<?php echo $description;?>">
<meta name="keywords" lang="fr" content="<?php echo $keywords;?>">
<meta name="google-site-verification" content="SbdBgJew61e-YYEHq3Mb5FNdsqAOOO5eY3EmTTutQr0" />
<meta name="author" content="Vlexhan">
<meta name="date-creation-ddmmyyyy" content="21022012" />
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="3 days" />
<meta http-equiv="content-language" content="fr-fr" />

<meta property="fb:admins" content="100000544554843" />

<?php if($sociallinks == true || $page == "index"){ ?>
<meta property="og:url" content="<?php 

if($page == "index"){
echo "http://www.facebook.com/dramapassion" ;
}else{
echo $url_face ; 
}

?>" />
<meta property="og:title" content="<?php echo $og_title ; ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:image" content="<?php echo $og_img ; ?>"/>
<meta property="og:site_name" content="Dramapassion"/>
<meta property="og:description" content="<?php echo $og_desc ; ?>"/>
<?php }elseif($page == "parrainage2"){
		

?>
<meta property="og:title" content="Dramapassion - parrainage"/>
<meta property="og:type" content="tv_show"/>
<meta property="og:image" content="http://www.dramapassion.com/images/page_parrainageFB.jpg"/>
<meta property="og:site_name" content="Dramapassion"/>
<meta property="og:description" content="Deviens mon filleul sur Dramapassion en prenant un abonnement Privilège et bénéficie de deux semaines offertes !"/>
<?php	
}

	
if($page == "player"){
	$crawler = 0;
	if ( preg_match('/(bot|spider|yahoo)/i', $_SERVER[ "HTTP_USER_AGENT" ] )) $crawler = 1 ;
	
}	
	
?>



<!-- Begin Cookie Consent plugin by Silktide -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"Pour rendre votre visite plus agréable Dramapassion utilise des cookies.","dismiss":"J'ai compris","learnMore":"En savoir plus","link":"http://www.dramapassion.com/cookies","theme":"light-top"};
    
	
</script>

<script type="text/javascript" src="<?php echo $http ; ?>js/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->



</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo $http ; ?>"><img style="max-height:50px;" src="<?php echo $http ; ?>images/logo.png"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
	    <ul class="nav navbar-nav">
	        <li><a href="<?php echo $http ; ?>catalogue/">VIDEOS</a></li>
	        <li><a href="<?php echo $http ; ?>premium/">PREMIUM</a></li>
	        <li><a href="<?php echo $http ; ?>store/">STORE</a></li>
	    </ul>
	     <form class="navbar-form navbar-right">
	        <div class="form-group">
	          <input type="text" placeholder="Chercher un titre" class="form-control">
	        </div>
	        <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
	      </form>
	    <ul class="nav navbar-nav navbar-right">
	        <li><a href="#">S'identifier</a></li>
	        <li><a href="#">Créer un compte</a></li>
	    </ul>
     
    </div><!--/.navbar-collapse -->
  </div>
</nav>	
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '344619609607', // App ID
      channelUrl : 'http://www.dramapassion.com', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/fr_FR/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "15796249" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=15796249&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- ImageReady Slices (Sans titre-1) -->

<div class="container container-top">


