<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");

	$dramaID = (int)cleanup($_GET['dramaID']);
	$dramaInfo = DramaInfo($dramaID);
	$connect = UserIsConnect() ;
	$verif = 1 ;
	
	if($connect == 1){ 
		$req_verif = "SELECT * FROM t_dp_playlist WHERE (UserID = ".$_SESSION['userid']." AND DramaID = ".$dramaID.")";
		$sql_verif = mysql_query($req_verif);
		$verif = mysql_num_rows($sql_verif);
		$actif = mysql_result($sql_verif,0,"actif");
		
		
		if($verif == 0){
			$req = "INSERT INTO t_dp_playlist (DramaID,DramaEpiTotal,UserID,DateTime) VALUES(".$dramaID.",".$dramaInfo['nb_epi'].",".$_SESSION['userid'].",now())";
			mysql_query($req);
		}
		if($verif == 1 && $actif == 0){
			$req = "UPDATE t_dp_playlist SET actif = 1 WHERE (UserID =".$_SESSION['userid']." AND DramaID=".$dramaID.")";
			$sql = mysql_query($req);
		}
	}
	
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (Sans titre-1) -->

<form action="<?php echo $http ;?>loginAction.php" METHOD="POST">
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Ma Playlist</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                                
                    </td>
                </tr>
				<tr><td height="10">&nbsp;</td></tr>
				<tr>
					<td width="600" height="100">
					<div style="width:570px;height:100px;margin:auto;">
						<?php if(($connect == 1 && $verif == 0) || ($connect == 1 && $verif == 1 && $actif ==0)){ 
							
						?>	
						<div class="playlist_img_thumb" style="float:left;width:180px;height:106px;">
						
						<img src="<?php echo $dramaInfo['img_thumb'] ; ?>" />
						</div>
						<div class="playlist_titre" style="float:left;width:370px;margin-left:20px;height:71px;" >
							"<?php echo $dramaInfo['titre'] ; ?>" a été ajouté à votre playlist.
						</div>
						
						<div class="playlist_bt_fermer" style="float:left;margin-left:20px;" >
							<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
						</div>
							
						<?php }elseif($connect == 1 && $verif != 0){ ?>
							<div class="playlist_img_thumb" style="float:left;width:180px;height:106px;">
						
						<img src="<?php echo $dramaInfo['img_thumb'] ; ?>" />
						</div>
						<div class="playlist_titre" style="float:left;width:370px;margin-left:20px;height:71px;" >
							"<?php echo $dramaInfo['titre'] ; ?>" se trouve déjà dans votre playlist.
							</div>
						
						<div class="playlist_bt_fermer" style="float:left;margin-left:20px;" >
							<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" /></a>
						</div>
						<?php }else{ ?>
							<div style="text-align:center;">
							Vous devez vous connecter pour ajouter un drama à votre playlist.<br /><br /><br />
							<div style="width:130px;margin:auto;" ><a href="<?php echo $http ; ?>connexion.php"><img src="<?php echo $http ; ?>images/pop_connecter.jpg" /></a></div>
							</div>
						<?php } ?>
						
						
					</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>
function fermer(){
	window.parent.$.fancybox.close();
}

</script>
</body>
</html>