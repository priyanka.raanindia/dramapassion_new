<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Guide d'utilisation pour le téléchargement</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="contenu" style="text-align:justify;">
<h3>1. Installation des logiciels</h3>
<p>Les vidéos en téléchargement sont au format f4v et sont protégées par le système de protection Flash Access 2.0. Les vidéos sont compatibles avec les systèmes Windows, MacOS et Linux.
Pour lire ces fichiers, vous avez besoin de trois logiciels : le lecteur Flash (version 10.1 ou plus), Adobe AIR et le lecteur Adobe Media Player.</p>
<br /><p>Ces logiciels peuvent être téléchargés gratuitement aux liens suivants :</p>
<ul>
<li>Lecteur Flash : <a href="http://get.adobe.com/fr/flashplayer/" class="lien_bleu" target="_blank">http://get.adobe.com/fr/flashplayer/</a></li>
<li>Adobe AIR (à installer avant le lecteur Adobe Media Player) : <a href="http://get.adobe.com/air/" class="lien_bleu" target="_blank">http://get.adobe.com/air/</a></li>
<li>Adobe Media Player : <a href="<?php echo $http;?>software/adobe_media_player.air" class="lien_bleu" target="_blank">http://www.dramapassion.com/software/adobe_media_player.air</a> (Si vous utilisez Internet Explorer, il est possible que l'extension du fichier soit changé en ".zip". Il faut alors rechanger manuellement l'extension du fichier téléchargé en ".air")</li>
</ul>
<br />
<h3>2. Première ouverture de Adobe Media Player</h3>
<p>Lorsque vous ouvrez Adobe Media Player pour la première fois, la fenêtre suivante s'affiche automatiquement.<br />
Si vous cliquez sur "Yes", toutes les vidéos avec l'extension f4v et flv seront dorénavant lancées avec Adobe Media Player par défaut. Nous vous recommandons de cliquer sur "Yes" à moins que vous n'utilisiez les formats f4v et flv de manière régulière avec un autre logiciel.
Si vous cliquez sur "No", les associations des extensions existantes ne seront pas modifiées.</p>
<br />
<img src="<?php echo $http; ?>images/ampstart.jpg" width="400">
<br />
<br />
<br />
<h3>3. Ouverture du fichier vidéo f4v avec Adobe Media Player</h3>

<h4>Méthode 1</h4>
<p>Si vous avez cliqué sur "Yes" à l'étape 2, il suffit de faire un double-clic sur le fichier f4v et Adobe Media Player se lance automatiquement.</p>
<br />
<h4>Méthode 2</h4>
<p>Ouvrez Adobe Media Player, puis glissez simplement le fichier f4v sur la fenêtre d'Adobe Media Player. Pour glisser le fichier, faites un clic-gauche sur l'icône du fichier et bougez la souris tout en gardant le bouton gauche appuyé.</p>
<br />
<img src="<?php echo $http; ?>images/methode2.jpg" width="600">
<br />
<br />
<br />

<h4>Méthode 3</h4>
<p>Faites un clic-droit sur l'icône du fichier f4v. Dans le menu déroulant, sélectionnez "Ouvrir avec" et ensuite sélectionnez "Adobe Media Player".</p>
<br />
<div style="overflow:hidden;position: relative;">
<img src="<?php echo $http; ?>images/methode3.jpg" width="400" style="position: relative;top: 0; left: 0;">
<?php
	if($paques == 1){
		$dbQpaques = "SELECT * FROM paques WHERE id = 36";
		$sqlQpaques = mysql_query($dbQpaques);
		$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
		$idPaques = mysql_result($sqlQpaques,0,"name");
		$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
		if(time()>$dateshow || $_SESSION["userid"] == 3){	
			$oeuf = '<img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:18px;position: absolute;top:203px;left:87px;">';
			echo $oeuf;
		}
	}
?>
</div>
<br />
<br />
<br />

<h4>Méthode 4</h4>
<p>Ouvrez Adobe Media Player, puis cliquez sur le lien "My Favorites" dans la barre de menu.</p>
<br />
<img src="<?php echo $http; ?>images/methode4a.jpg" width="600">
<br />
<br />
<p>Ensuite, cliquez sur l'onglet "My Personal Videos".</p>
<br />
<img src="<?php echo $http; ?>images/methode4b.jpg" width="600">
<br />
<br />
<p>Enfin, cliquez sur le bouton "+ ADD VIDEO" dans le coin inférieur gauche de la fenêtre et sélectionnez le fichier f4v.</p>
<br />
<img src="<?php echo $http; ?>images/methode4c.jpg" width="600">
<br />
<br />
<br />

<h3>4. Identification</h3>
<p>Lorsque vous ouvrez un fichier téléchargé pour la première fois, une fenêtre d'identification apparaît. Il faut obligatoirement être connecté à internet pour procéder à l'identification.
Introduisez vos identifiants Dramapassion dans les cases (pseudo dans première et mot de passe dans la deuxième) et cliquez sur "OK".
Cela peut prendre plusieurs secondes avant que la vidéo ne se lance.</p>
<br />
<img src="<?php echo $http; ?>images/amplogin.jpg">
<br /><br />
<p>Une fois identifié, vous pouvez regarder la vidéo en question sans être connecté à internet jusqu'à l'expiration de votre abonnement en cours sur l'ordinateur où l'identification a été faite.<br />
Il faudra vous identifier à nouveau après chaque renouvellement d'abonnement (automatique ou manuel). Par exemple, si vous avez un abonnement de trois mois, il faudra vous identifier à nouveau lorsque ces trois mois seront écoulés, même si vous êtes en renouvellement automatique.</p>
<br />
<br />
<h3>5. Utilisation d'un gestionnaire de téléchargement</h3>
<p>Veuillez consulter la page suivante pour profiter au maximum du service de téléchargement !</p>
<br />
<p>- Pour les utilisateurs Windows :
<a href="<?php echo $http; ?>astuce-telechargement/" class="lien_bleu"><?php echo $http; ?>astuce-telechargement/</a></p>
<p>- Pour les utilisateurs Mac :
<a href="<?php echo $http; ?>astuce-telechargement-mac/" class="lien_bleu"><?php echo $http; ?>astuce-telechargement-mac/</a></p>
<br />
<br />
<br />
<br />
<br />
</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>