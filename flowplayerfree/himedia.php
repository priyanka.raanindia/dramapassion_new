<?php
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect_free.php");

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

$content = "$_SERVER[REQUEST_URI]";
?>
<html>
<head>
<script src="http://www.dramapassion.com/js/flowplayer-3.2.11.min.js"></script>
</head>
<body>		
<?php 
	$hashEpi = Hash_free(10,35,1);	
	$url_free_epi = $server_free.$hashEpi;
?>
<div id="free_video">
	<div style="display:block;width:600px;height:340px;cursor:default;" id="free"></div>
</div>
<script>
$f("free", "<?php echo $http; ?>swf/flowplayer.commercial-3.2.15.swf", {
	key: '#$8d427e6f20cb64d82ba',
	id:'adswizz1',
	name:'adswizz1',
	wmode:'transparent',
	clip: {
		url: '<?php echo $url_free_epi ; ?>',		
        provider: 'pseudo',
        autoPlay: true,
		autoBuffering: true,
		scaling: 'fit'
		},
	canvas: {
		 backgroundColor:'#000000'
		},
	plugins: {
		pseudo: { 
			url: 'flowplayer.pseudostreaming-3.2.7.swf'
			},
		
        videoplaza: { 
            url: 'http://be-dramapassion.cdn.videoplaza.tv/resources/flash/flowplayer-3.2.6/latest/vp_plugin.swf',
            vpDomain:'http://be-dramapassion.videoplaza.tv',
            vpShares:'test',
            vpTags: 'himedia',
            vpContentid: '10351-Prosecutor-Princess-Ep1-Part1',
            vpEnvironmentURL: ''
            },

		controls: {
			stop: false
			}		
	}
});
</script>
</body>
</html>