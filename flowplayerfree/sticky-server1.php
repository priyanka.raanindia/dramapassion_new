<?php
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect.php");
?>
<html>
<head>
<script src="http://www.dramapassion.com/js/flowplayer-3.2.11.min.js"></script>
</head>
<body>		
<?php 
	$hashEpi = Hash_free(10,35,1);	
	$url_free_epi = $server_free.$hashEpi;
?>
<div id="free_video">
	<div style="display:block;width:600px;height:340px;cursor:default;" id="free"></div>
</div>
<script>
$f("free", "<?php echo $http; ?>swf/flowplayer.commercial-3.2.15.swf", {
	key: '#$8d427e6f20cb64d82ba',
	id:'adswizz1',
	name:'adswizz1',
	wmode:'transparent',
	clip: {
		url: '<?php echo $url_free_epi ; ?>',		
        provider: 'pseudo',
        autoPlay: true,
		autoBuffering: true,
		scaling: 'fit',
		},
	canvas: {
		 backgroundColor:'#000000'
		},
	plugins: {
		pseudo: { 
			url: 'flowplayer.pseudostreaming-3.2.7.swf'
			},
		
		adswizz : {
            url: 'AdswizzVIP-Flowplayer-3.2.0.swf',
            server: "ads.stickyadstv.com",
            loadTimeout: 7000, // default 3000
            ads: {
				prerollZoneId: 1845, // default -1
                midrollZoneId: 1846, // default -1
                postrollZoneId: 1847, // default -1
                midrollInterval: -1, // default -1
                midrollDelay: 15 // default -1
                },
            tags:{ // defaults to none
                // keyword: "value",
                // tag: "tag value",
                //"tag with spaces": "some value"
				},                          
            adTimeDisplay:{ 	           
                text: "                            CLIQUEZ SUR LA PUB POUR SOUTENIR DRAMAPASSION ([time] secondes)                                 ",
                align: "TL", // one of BL, BR, TL, TR, defaults to BR
                x: 0, // pixel offset from the above alignment point, defaults to 0
                y: 0,// pixel offset from the above alignment point, defaults to 0
                textColor: "0xffffff", // hex value for colour styling, defaults to 0xffffff, i.e. white
                <?php
                $random = rand(1,5);
                if($random == 1){
	                echo 'backgroundColor: "0xFEG7EB"';
                }elseif($random == 2){
	                echo 'backgroundColor: "0xFF4848"';
                }elseif($random == 3){
	                echo 'backgroundColor: "0x800080"';
                }elseif($random == 4){
	                echo 'backgroundColor: "0x23819C"';
                }elseif($random == 5){
	                echo 'backgroundColor: "0x3923D6"';
                }
                ?>
				}
			},

		controls: {
			stop: false
			}		
	}
});
</script>
</body>
</html>