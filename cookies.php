<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<?php
if($paques == 1){
	$dbQpaques = "SELECT * FROM paques WHERE id = 34";
	$sqlQpaques = mysql_query($dbQpaques);
	$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
	$idPaques = mysql_result($sqlQpaques,0,"name");
	$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
	if(time()>$dateshow || $_SESSION["userid"] == 3){	
		$oeuf = '<img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:16px">';
		//echo $oeuf;
	}
	?>
	<h2>Qu’est ce que les co<?php echo $oeuf;?>kies ?</h2>
	<?php
}else{
?>
<h2>Qu’est ce que les cookies ?</h2>
<?php	
}	
?>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br /><br />
<div class="cgv">   
<p>Un cookie est une information déposée sur le disque dur d’un internaute par le serveur du site qu'il visite. Il contient plusieurs données : le nom du serveur qui l'a déposé, un identifiant sous forme de numéro unique, éventuellement une date d'expiration. Ces informations sont parfois stockées sur l’ordinateur dans un simple fichier texte auquel un serveur accède pour lire et enregistrer des informations.</p>
<br /><br /><br />

<h1>Les différents types de cookies utilisés</h1>
   
<br /><br /><br />

<h2 >Les Cookies de session d'utilisateur</h2>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br /><br />
<div class="cgv">   
<p>Les Cookies de session d'utilisateur enregistrent les informations que vous avez dûment remplies à l'aide de nos formulaires lors de votre inscription.<br />
Ces technologies nous permettent de vous reconnaitre dès que vous vous connectez sur notre site dramapassion.com.</p>
<br /><br />

<h2 >Les Cookies permettant l'affichage de contenus et publicité ciblée</h2>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br /><br />
<div class="cgv">   
<p>Les Cookies permettant l'affichage de contenus et publicité ciblés sont déposés par nous ou par des tiers (annonceur, régie publicitaire). 
Ils sont utilisés pour connaitre vos habitudes de visite. Ces Cookies permettent ainsi d'adapter les contenus et les publicités qui vous sont proposés sur nos Services en fonction de vos centres d'intérêts. Ces derniers sont déterminés par la reconnaissance de votre Terminal et votre navigation antérieure ou ultérieure sur nos Services mais également sur d'autres sites Internet déposant des cookies similaires. L'intérêt est de proposer des publicités qui seraient pertinentes pour vous et pourraient ainsi vous intéresser. </p>
<br /><br />

<h2 >Les Cookies de mesure d'audience</h2>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br /><br />
<div class="cgv">   
<p>Les Cookies de mesure d'audience établissent des statistiques sur le nombre de visites et l'utilisation de nos Services. Des statistiques de fréquentation, d'affichage, de publicités sur nos espaces publicitaires et d'interaction des Utilisateurs avec ces espaces pourront ainsi être réalisées. Ils permettent également d'assurer le suivi des facturations des annonceurs tiers sur nos Services.</p>
<br /><br />

<h2>Les Cookies de partage via les réseaux sociaux</h2>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br /><br />
<div class="cgv">   
<p>Des fichiers cookies sont susceptibles d'être déposés sur votre Terminal par des sites de réseaux sociaux permettant le partage de contenus de nos Services. Ces sites de réseaux sociaux sont matérialisés sur nos Services par des boutons qui permettent à ces réseaux sociaux de vous identifier au cours de votre navigation sur nos Services.
Nous ne pouvons contrôler ces Cookies et les données récoltées par les réseaux sociaux. Pour plus d'informations sur ces applications, nous vous invitons à consulter les politiques de confidentialité propres à chacun de ces sites de réseaux sociaux :<br />
<a href="https://www.facebook.com/help/cookies">Facebook</a><br />
<a href="https://twitter.com/privacy?lang=fr">Twitter</a><br />



</p>
<br /><br />


<br />
<br />
<br />
</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>