<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
                    
<?php
if($paques == 1){
	$dbQpaques = "SELECT * FROM paques WHERE id = 35";
	$sqlQpaques = mysql_query($dbQpaques);
	$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
	$idPaques = mysql_result($sqlQpaques,0,"name");
	$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
	if(time()>$dateshow || $_SESSION["userid"] == 3){	
		$oeuf = '<img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:16px">';
		//echo $oeuf;
	}
	?>
	<h1 class="menu_noir">F<?php echo $oeuf ; ?>nctionnement du lecteur streaming</h1>
	<?php
}else{
?>
<h1 class="menu_noir">Fonctionnement du lecteur streaming</h1>
<?php	
}	
?>                    
                    

<br>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="contenu" style="text-align:justify;">
<h3>1. Player 1</h3>
<br>
<p>Par défaut, le mode automatique est activé. Le lecteur choisira et adaptera la qualité de la vidéo automatiquement en fonction de la qualité de votre connexion internet.</p>
<br>
<p>Si vous souhaitez fixer la qualité de façon manuelle, cliquez sur le bouton "HD" dans la barre de contrôle du lecteur et sélectionnez la qualité souhaitée.</p>
<br>
<br>
<h3>2. Player 2</h3>
<br>
<p>Par défaut, le mode automatique est activé. Le lecteur choisira et adaptera la qualité de la vidéo automatiquement en fonction de la qualité de votre connexion internet.</p>
<br>
<p>Pour modifier la qualité manuellement, cliquez d'abord sur le témoin "Auto(ON)". Le témoin s'éteint et change en "Auto(OFF)". Les témoins chiffrés s'éclaircissent pour montrer que la sélection est possible. Attendez environ 10 secondes pour s'assurer que le mode automatique ait bien terminé ses opérations en cours. Cliquez ensuite sur la qualité que vous désirez fixer.<br />
La qualité enregistrée devient blanche (et clignote sur Firefox) en l'attente du basculement. Le changement du flux vidéo peut prendre quelques secondes avant de s'effectuer
et vous ne pouvez pas modifier la qualité pendant ce temps. Une fois le basculement terminé, la qualité que vous avez choisie change du blanc au jaune.</p><br />
<p>Pour retourner en mode automatique, cliquez simplement sur le témoin "Auto(OFF)".</p>
<br />
<br />
<br />
<br /></div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>