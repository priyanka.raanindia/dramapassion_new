<?php	
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect_free.php");

	$hashEpi = Hash_free(10,35,1);	
	$url_free_epi = "http://nf04.dramapassion.com/".$hashEpi;
	
?>

<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
		<style type="text/css">
			#my_video {height:320px; width:567px;}
		</style>

    	<script src="adPlugin.js"></script>
	</head>
	<body>
		<video id='my_video' controls preload='none' autoplay="autoplay">
			<source src="<?php echo $url_free_epi ; ?>" type='video/mp4'>
		</video>

		<script>
			'use strict';
			var init = function (player, config) {			
	        	//prevent start playback before plugin is ready
				player.controls = false;

				/**
				 * init() start and configure the ad plugin
				 * @player = the player DOM element
				 * @config = ad request configurations
				 */
				adPlugin.init(player, config)
				.onError(function (errorMsg) {
					console.log(errorMsg);
					player.controls = true;
				})
				.onReady(function () {
					console.log('ready');
					player.controls = true;
				})
				.onAdStart(function (ad) {
					console.log(ad);
					player.controls = false;
				})
				.onAdCompleted(function (ad) {
					console.log(ad);
				})
				.onContentStart(function () {
					player.controls = true;
				})
				.onAdClickThrough(function (ad) {
					player.controls = true;
				});
			};

			(function () {
				var player = document.getElementById('my_video'),
					config = {
        				host: 'http://be-dramapassion.videoplaza.tv',
        				tags: [''],
        				category: 'HTML5',
        				height : 320,
						width : 567
        			};

				//if ad plugin not in the DOM, wait for loaded event
				if (typeof adPlugin === 'undefined') {
					document.addEventListener('adPluginLoaded', function(evt) {
						if (evt.detail) {
							init(player, config);
						}
					});
					return;
				}
				init(player, config);
			}());

		</script>

	</body>
</html>