<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("bt_header.php");
	

	$dramaID = (int)cleanup($_GET['dramaID']);
	$drama_tab = DramaInfo($dramaID);
	$drama_name = $drama_tab['titre'];
	$drama_name = str_replace(' ', '-' ,$drama_name);
	$drama_name_lien = str_replace('\'', '' ,$drama_name);
	$type_stream = $drama_tab['type_stream'];
	
	$verif_drama_status = $drama_tab['StatusID'];
	
	$top10 = AffTop10New();
	
	
	$req_coms_tot = "SELECT * FROM t_dp_review WHERE (Visible=1 AND WebLangID=2 AND ProductID='".$dramaID."') ORDER BY ReviewAdded ";
	$sql_coms_tot = mysql_query($req_coms_tot);
	
	$nb_coms_tot = mysql_num_rows($sql_coms_tot);
	
	if(($nb_coms_tot % 10) == 0){
		$nb_tot_page = intval($nb_coms_tot/10);
	}else{
		$nb_tot_page = intval($nb_coms_tot/10)+1;
	}
	

	if($verif_drama_status == 1 || $verif_drama_status == 10 || $verif_drama_status == 30){
		require_once("bt_top.php");
		$getAbo = abo_user($_SESSION['userid']);
		
		echo '<div class="row"><div class="col-xs-12"><div class="img_drama" ><img id="img_drama_'.$dramaID.'"  src="'.$drama_tab['img_big'].'" width="100%"  alt="série coréenne &quot;'.$drama_tab['titre'].'&quot; en vostfr" title="drama coréen &quot;'.$drama_tab['titre'].'&quot; en vostfr" border="0"></div></div></div>'; 
		
		
		?>
		<div class="row right like-facebook">
			<div class="col-xs-12">
				<div class="inblock block-fb"><td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="<?php echo $url_face ; ?>"></div></td></div>
				<div class="inblock block-tweet"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></div>
			</div>
		</div>
		<div class="row row-drama-info">
			<div class="col-xs-12 col-sm-8">
				<div>
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#episodes" aria-controls="home" role="tab" data-toggle="tab">Épisodes</a></li>
				    <li role="presentation"><a href="#infos" aria-controls="profile" role="tab" data-toggle="tab">Infos</a></li>
				    <li role="presentation"><a href="#commentaires" aria-controls="messages" role="tab" data-toggle="tab">Commentaires</a></li>
				  </ul>
				
				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="episodes">
					    <?php AffTapEpiNew($dramaID) ; ?>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="infos">
					    <p>
							<?php
								$req_dl = "SELECT * FROM store WHERE dramaID = ".$dramaID;
			                	$sql_dl = mysql_query($req_dl);
			                	
			                	$nb_dl = mysql_num_rows($sql_dl);
			                	
			                	if($nb_dl > 0){
				                	echo '<a href="'.$http.'store/detail.php?id=1'.$dramaID.'"><img src="'.$http.'images/drama_pub_tel.jpg" width="100%" /></a><br /><br />';
			                	}	
							?>
					    </p>
					    <p>
					    <table width="330" border="0" cellpadding="0" cellspacing="0" >
						    <tr>
                            	<td width="40%">Titre</td>
                                <td width="60%"><?php echo $drama_tab['titre']; ?></td>
                                
                            </tr>
                            <tr>
                            	<td width="40%">Épisodes</td>
                                <td width="60%"><?php echo $drama_tab['nb_epi']; ?></td>
                                
                            </tr>
                        	<tr>
                            	<td width="40%">Titre original</td>
                                <td width="60%"><?php echo $drama_tab['titre_org']; ?></td>
                                
                            </tr>
							<tr>
                            	<td>Catégorie</td>
                                <td><?php AffCateDrama($dramaID); ?></td>
                            </tr>
                            <?php if(GetGenreDrama($dramaID) == 'ok'){ ?>
                        	<tr>
                            	<td>Genre</td>
                                <td><?php AffGenreDrama($dramaID); ?></td>
                            </tr>
                            <?php } ?>
                        	<tr>
                            	<td>Chaîne de diffusion</td>
                                <td><?php echo $drama_tab['chaine'] ; ?></td>
                            </tr>
                        	<tr>
                            	<td>Année de diffusion</td>
                                <td><?php echo $drama_tab['anne_sortie']; ?></td>
                            </tr>
                            <?php if($drama_tab['StatusID'] == 10){?>
                            <tr>
                            	<td>&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                            <tr>
                            	<td>Date de sortie</td>
                                <td><?php echo $drama_tab['releaseFR']; ?></td>
                            </tr>
                            <?php } ?>
							<tr>
                            	<td>&nbsp;&nbsp;&nbsp;</td>
                            </tr>
							<tr>
                            	<td><?php playlist2($dramaID) ; ?></td>
                                
                            </tr>
                            <?php if($drama_tab['mineur'] > 0 || $drama_tab['trailer'] >= 1 ){
                            ?>
                            <tr>
                            	<td>&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                            
                            
                            <?php 
	                            $pays = getIp2Location($_SERVER["REMOTE_ADDR"]);
	                            if($drama_tab['mineur'] > 0){ ?>
	                            <?php
		                            $oeuf10 = "0";
	                            if($paques == 1 && $dramaID == 208){
									$dbQpaques = "SELECT * FROM paques WHERE id = 11";
									$sqlQpaques = mysql_query($dbQpaques);
									$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
									$idPaques = mysql_result($sqlQpaques,0,"name");
									$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
									if(time()>$dateshow || $_SESSION["userid"] == 3){	
										$oeuf10 = '<img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:14px;">';
										
									}
								}    
	                               ?>
                            <tr>
                            	<td COLSPAN=2><div><div style="float:left;font-size: 16px;">Série déconseillée aux moins de 1<?php echo $oeuf10 ?> ans </div><div style="margin-top:0px;margin-left:10px;float:left;"></div></div></td>
                                
                            </tr>
                            <tr>
                            	<td>&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                            <?php
	                            }
                            }
                            ?>
						</table>
					    </p>
						<h4>Synopsis</h4>
						<p style="text-align:justify;"><?php echo $drama_tab['synopsis'] ; ?></p>
					    <h4>Acteurs</h4>
						<?php ActeurDramaNew($dramaID) ; ?>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="commentaires">
					    
					    <div id="com_add">
							    <h3>Ajouter un commentaire</h3>
							    <form>
								    <div class="form-group">
								    	<input type="text" class="form-control" placeholder="Titre">
								    </div>
								    <div>
									    <p style="font-size: 12px;">
										    Cette case est exclusivement réservée aux commentaires sur le drama mis en ligne par Dramapassion. Tout renvoi vers d’autres sites internet sera automatiquement supprimé.
En cas de liens répétitifs, l'accès à votre compte Dramapassion pourra être bloqué.
Merci de votre compréhension.
									    </p>
								    </div>
								    <div class="form-group">
								    	<textarea class="form-control" rows="3" placeholder="Commentaire"></textarea>
								    </div>
								    <div class="form-group">
									    <button type="submit" class="btn btn-default">Envoyer</button>
								    </div>
							    </form>
					    </div>
					    <div id="top_com">
						    <div class="row">
							    <div class="col-xs-12 right"><?php echo $nb_coms_tot ; ?> commentaires</div>
						    </div>
					    </div>
					    <div id="com">
						    <div class="row"><div class="col-xs-12">
							    <table class="table" id="tablecom">
							    </table>
						    </div></div>
					    </div>
					    <nav aria-label="Page navigation">
					        <ul class="pagination" id="pagination"></ul>
					    </nav>
				    </div>
				  </div>
				
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 aside">
				<?php if($getAbo != "privilege" || $getAbo != "decouverte"){ ?>
				<div>
					<a href="<?php echo $http ; ?>premium/"><img src="<?php echo $http;?>images/abonnement.jpg" class="img-aside" /></a>
				</div>
				<?php } ?>
				<?php recommander_drama_new($dramaID) ; ?>
				<table class="table table-aside-top">
			<thead> 
				<tr> 
					<th colspan="2" class="top-table top-10" ><p class="p-top-10">TOP 10</p></th>  
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach($top10 as $k => $v){
						echo "<tr>";
							echo "<td>";
								echo $k;
							echo "</td>";
							echo '<td class="noir">';
								echo '<a href="'.$v['link'].'" class="lien_noir">'.$v['name'].'</a>';
							echo "</td>";
						echo "</tr>";
						
						
					}
				?>
			</tbody>
		</table>
			</div>
		</div>
		<script type="text/javascript">
		    $(function () {
		        window.pagObj = $('#pagination').twbsPagination({
		            totalPages: <?php echo $nb_tot_page ; ?>,
		            visiblePages: 5,
		            onPageClick: function (event, page) {
		                //console.info(page + ' (from options)');
		                $.ajax({
					       url : '<?php echo $http ; ?>bt_com.php?dramaID=<?php echo $dramaID; ?>&limit_min='+page,
					       type : 'GET',
					       dataType : 'json',
					       success : function(json, statut){
						       temp = '';
					           $.each(json, function (key, data) {
								    temp += '<tr>';
								    temp += '<td class="pseudoReview">'+data.pseudo+'</td>';
								    temp += '<td><p class="titeReview">'+data.reviewTitel+'</p><p>'+data.review+'</p></td>';
								    temp += '<td></td>';
								    temp += '</tr>';
								})
								$('#tablecom').html(temp);
					       },
					
					       error : function(resultat, statut, erreur){
					         
					       },
					
					       complete : function(resultat, statut){
					
					       }
					
					    });

		                
		            }
		        }).on('page', function (event, page) {
		            //console.info(page + ' (from event listening)');
		        });
		    });
		</script>
		<?php
		
	}else{
	$url = "Location: ".$http ;

		header($url);
	}

?>
 

</div>

<?php require_once("bt_bottom.php");?>