<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Que faire en cas de lenteur de la connexion internet ?</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="contenu" style="text-align:justify;">
<h3>1. Symptômes</h3>
<p>Ce guide s'applique à vous si vous expérimentez une lenteur anormale de votre connexion internet sur le site de dramapassion.com.
En streaming, cela se traduit par une longue durée d'attente au démarrage de la vidéo et un buffering fréquent pendant le visionnage.
En téléchargement, cela se traduit par une vitesse de téléchargement anormalement faible malgré une connexion haut débit.</p>
<p>Veuillez noter que Dramapassion ne fixe aucune limite de vitesse que ce soit en streaming ou en téléchargement.</p>
<br />
<br />
<h3>2. Test de la vitesse réelle de votre connexion internet</h3>
<p>Tout d'abord, faites un premier test de votre connexion internet pour voir où se situe la vitesse réelle de connexion entre votre ordinateur et le serveur dramapassion.com.
Pour cela, cliquez sur le lien suivant pour télécharger un fichier test. Observez ensuite la vitesse à la quelle le fichier est téléchargé. La vitesse est exprimée en XXX ko/sec ou XXX Mo/sec.
<b>Attendez au moins 30 secondes avant d'arrêter le téléchargement.</b></p>
<br />
<p style="text-align:center;"><a href="http://ns01.dramapassion.com/test-vitesse.dat" class="lien_bleu"><b>Cliquez ici pour lancer le téléchargement du fichier test</b></a></p>
<br />
<h4>Comment interpréter le résultat :</h4>
<ul>
<li>Si la vitesse est supérieure à 500 ko/sec (0,5 Mo/sec), la vitesse est suffisante pour regarder de manière confortable les vidéos en streaming (SD et HD) et en téléchargement (SD et HD).</li>
<li>Si la vitesse se situe entre 250 et 500 ko/sec (0,25 à 0,5 Mo/sec), la vitesse est suffisante pour la qualité SD mais probablement pas pour la qualité HD.</li>
<li>Si la vitesse est inférieure à 250 ko/sec (0,25 Mo/sec), la vitesse n'est pas suffisante pour regarder les vidéos, même en qualité SD, sans buffering et le téléchargement peut être très long.</li>
</ul>
<p>Si la vitesse constatée est beaucoup plus lente que lorsque vous téléchargez ou regardez des vidéos sur des sites de streaming de référence tels que Youtube, il y a de fortes chances que votre FAI bride votre connexion.
L'étape suivante explique comment vous pouvez accélérer la vitesse de téléchargement sur une connexion bridée.</p>
<br />
<br />
<h3>3. Test de l'accélération de la vitesse de téléchargement</h3>
<p>Grâce à un logiciel d'accélération de téléchargement, vous pouvez augmenter la vitesse de téléchargement sur une connexion bridée.</p>
<p>Téléchargez et installez le logiciel correspondant à votre système :</p>
<ul>
<li>Pour les PC Windows : <a href="http://www.dramapassion.com/software/fdminst-Dramapassion.exe" class="lien_bleu">cliquez ici pour télécharger Free Download Manager</a>.</li>
<li>Pour les Mac OSX : <a href="http://www.dramapassion.com/software/PSD.dmg" class="lien_bleu">cliquez ici pour télécharger Progressive Download</a>.</li>
</ul>
<p>Une fois le logiciel installé, faites un test de téléchargement du fichier test au lien suivant. Pour télécharger avec le logiciel d'accélération, faites un clic droit sur le lien ci-dessous, sélectionnez "copier l'adresse du lien".
Ensuite, dans le logiciel "Free Download Manager" (ou "Progressive download" pour les utilisateurs Mac), cliquez sur le bouton "+" dans la barre de menu.
L'adresse du lien est alors automatiquement copiée. Enfin, cliquez sur "OK" pour lancer le téléchargement. <b>Attendez au moins 30 secondes avant d'arrêter le téléchargement.</b></p>
<br />
<p style="text-align:center;"><a href="http://ns01.dramapassion.com/test-vitesse.dat" class="lien_bleu"><b>Lien pour le téléchargement du fichier test</b></a></p>
<br />
<h4>Comment interpréter le résultat</h4>
<p>Vous devriez normalement obtenir une vitesse de téléchargement sensiblement supérieure au test de l'étape 2 (un multiple de la vitesse initiale).
Cela veut dire que vous pouvez effectivement contourner le problème de votre connexion bridée.</p>
<p>Si la vitesse est identique au test de l'étape 2, cela veut dire que la vitesse globale de votre connexion est trop faible. Dans ce cas de figure, il n'y a pas d'autres solutions que de vous renseigner auprès de votre FAI ou changer de FAI.</p>
<br />
<br />
<h3>4. Qu'est-ce que cela veut dire concrètement pour vous</h3>
<p>Si vous avez un abonnement Privilège et que vous utilisez principalement le téléchargement, la solution proposée à l'étape 3 peut accélérer vos téléchargements.</p>
<br />
<p>Si vous avez un abonnement Privilège et que vous utilisez principalement le streaming et que le chargement est très lent (buffering fréquent) :</p>
<ul>
<li>Testez d'abord la qualité SD pour voir si la lecture est plus fluide.</li>
<li>Si vous rencontrez des problèmes de lenteur même pour la qualité SD, nous vous recommandons d'utiliser le téléchargement en appliquant la solution proposée à l'étape 3.</li>
</ul>
<p>Si vous avez un abonnement Découverte et que vous rencontrez des problèmes de lenteur avec le streaming, nous vous recommandons de prendre un abonnement Privilège et utiliser le téléchargement
en appliquant la solution proposée à l'étape 3. Vous pouvez bien sûr contacter votre FAI et essayer de résoudre le problème de lenteur.</p>
<br />
<h4>Rappel :</h4>
<ul>
<li>Pour télécharger un fichier avec le logiciel d'accélération, faites un clic droit sur le lien, puis sélectionnez "Copier l'adresse du lien", et ensuite cliquez sur le bouton "+" dans le logiciel d'accélération.</li>
<li>Consultez la page suivante pour les instructions relatives à la lecture des fichiers téléchargés : <a href="http://www.dramapassion.com/guide-telechargement/" class="lien_bleu"><b>Guide d'utilisation pour le téléchargement</b></a>.</li>
</ul>
<br />
<br />
<br />
<br />
<!--Free Download Manager code//-->
<div align="right">
<a href="http://www.freedownloadmanager.org/"title=" Free Download Manager">
<img src="http://www.freedownloadmanager.org/fdm_03.gif" width="88" height="31" border="0" alt = "Free Download Manager"><br>Free Download Manager</A> <!--End of Free Download Manager code//-->
<br />
<br />
</div>
</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>