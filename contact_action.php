<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$os = getOs();

	$erreur = 0;

	if(!isset($_POST['lien_tab_contact']) || $_POST['lien_tab_contact'] == ""){
		$erreur = 1;
	}else{
		$type_q = $_POST['lien_tab_contact'];
		$tab_type = array("Problème Technique","Proposition de Drama","Gestion de mon Compte","Autre");
		$type = $tab_type[$type_q];
		$_SESSION['contact']['lien_tab_contact']= $type_q;
	}
	if(!isset($_POST['pseudo']) || $_POST['pseudo'] == ""){
		$erreur = 1;
	}else{
		$pseudo = $_POST['pseudo'];
		$_SESSION['contact']['pseudo']= $pseudo;
	}
	if(!isset($_POST['mail']) || $_POST['mail'] == ""){
		$erreur = 1;
	}else{
		$mail = $_POST['mail'];
		$_SESSION['contact']['mail']= $mail;
		$verif_email = valid_email($mail);
		 if($verif_email == 0){
			$erreur = 1;
			$_SESSION['contact']['mail']= "";
		 }
	}
	if(!isset($_POST['text']) || $_POST['text'] == ""){
		$erreur = 1;
	}else{
		$text = $_POST['text'];
		$_SESSION['contact']['text']= $text;
	}
	
	

	
	
	
	if($erreur == 1){
	$location = "Location: ".$http."contactez-nous/erreur";

	header($location);
	}else{
	
	
	
	$valid = 1;
	if($valid){
		require_once("top.php");
		$categoryname = $type;
		$contactname = cleanup($pseudo);
		$contactemail = cleanup($mail);
		$contactmessage = cleanup($text);
		$contactmessage = str_replace("\\r\\n","<br />",$contactmessage);
		$contactmessage = stripslashes($contactmessage);
		$browser = get_browser(null, true);
		
		$message = "Username: $contactname <br>";
		$message.= "Email: $contactemail <br>";
		$message.= "User IP: ".$_SERVER['REMOTE_ADDR']." | ".getIp2Location($_SERVER['REMOTE_ADDR'])."<br><br>";
		$message.= "Category: $categoryname <br>";
		$message.= "OS: $os <br>";
		$message.= "Explorer: ".$_SERVER['HTTP_USER_AGENT']." <br><br>";
		$message.= "Message: <br>".$contactmessage;
		
		
		$headers = "From: $contactemail\r\n";
		$headers .= "Reply-To: $contactemail\r\n";
		$headers .= "Return-Path: $contactemail\r\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-type: text/html; charset=UTF-8\n";
		
		if(mail($supportemail,"Dramapassion.com - ".$categoryname,$message,$headers)){
			$message = 'Merci. Votre message a été envoyé correctement à notre équipe de support. <br><a class="lien_noir" style="font-size:16px;" href="'.$http.'">Cliquez ici</a> pour retourner à la page d’accueil.';
			unset($_SESSION['contact']['text']);
		}else{
			$message = 'Une erreur est survenue !<br /><a href="'.$http.'contactez-nous/">Cliquez ici</a> pour revenir à la page précédente.';
		}
		?>
		<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <center>
		<div class="tab_min_height">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0"  >
                <tr>
                	<td width="1000" valign="top">
                    <!-- CADRE DE GAUCHE -->
                    
                    <table width="1000" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td style="text-align:center;"><p><?php echo $message ;?></p></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
		</div>
		</center>
		</td>
		</tr>
		<?php
		//header("Location: ".$http."messagesent.php");
		//exit();
		
	}
	

require_once("bottom.php"); 
}
?>