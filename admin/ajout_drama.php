<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("top.php");


$gris = 0;
?>
<tr>
        <td valign="top" height="37" style="background-image:url(<?php echo $http ; ?>images/header.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/index.php" class="lien_noir" style="text-align: right">Sortie Drama</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/aff.php" class="lien_noir" style="text-align: right">Slider Drama / Page index</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/drama.php" class="lien_noir" style="text-align: right">Liste Drama</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				</tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>
<tr><td>
<div style="width:1000px;margin:auto;">
<h2>Ajout Drama</h2>
<a href="index.php">Retour</a>
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
<form method="post" action="ajout_drama_action.php" name="form" id="form" enctype="multipart/form-data">
<table>
<tr><td>Titre :</td><td><input type="text" id="titre" name="titre" /></td></tr>
<tr><td>Date de sortie :</td><td><input type="text" id="datepicker" name="datepicker" /></td></tr>
<tr><td>Image Thumb : (180 x 106)</td><td><input type="file" name="thumb"  id="thumb"/></td></tr>
</table>
</form>
<br />
<input type="button" value="Ajouter" id="envoyer" />
</div>
</td></tr>	
<script>
	jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
});
$(document).ready(function() {

                $('#datepicker').datepicker({
                	changeYear: true,
                	changeMonth: true
                });
                $('#envoyer').click(function() {
                	if($('#titre').val() != '' && $('#datepicker').val() != '' && $('#thumb').val() != ''){
	                	$('#form').submit();
                	}else{
                		error = '';
	                	if($('#titre').val() == ''){
		                	error = 'Titre manquant \n';
	                	}
	                	if($('#datepicker').val() == ''){
		                	error = error+'Date de sortie manquante \n';
	                	}
	                	if($('#thumb').val() == ''){
		                	error = error+'Image Thumb manquante \n';
	                	}
	                	alert(error);
                	}
                });

            });
</script> 
<?php
//echo '<a href="http://dramapassion.com/admin/genre_drama.php">genre</a>';
require_once("bottom.php");
?>