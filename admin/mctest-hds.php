<?php
$file = $_REQUEST['f'];
$dir0 = substr_replace($file ,"",-1);
$dir0 = substr_replace($dir0 ,"",-1);
$dir = $dir0."/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <title>Strobe Media Playback</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <script type="text/javascript" src="../js/swfobject.js">
        </script>
        <script type="text/javascript" src="../js/ParsedQueryString.js">
        </script>
        <script type="text/javascript" src="../js/debug.js">
        </script>
        <script type="text/javascript">
        	function loadStrobeMediaPlayback()
        	{
	            // Collect query parameters in an object that we can
	            // forward to SWFObject:
            
	            var pqs = new ParsedQueryString();
	            var parameterNames = pqs.params(false);
	            
	            var parameters = {
	                src: "http://np03.dramapassion.com/mediacache/_definst_/smil:http/<?php echo $dir0; ?>/<?php echo $file; ?>-hd.smil/manifest.f4m",
	                //"http://np01.dramapassion.com/vod/_definst_/<?php echo $dir0; ?>/smil:<?php echo $file; ?>-hd.smil/manifest.f4m"
	                //"http://np03.dramapassion.com:1935/mediacache/_definst_/mp4:http/np01.dramapassion.com/video/sha/sha01-300.mp4/manifest.f4m"
	                autoPlay: false,
					dynamicStreamBufferTime: 6,
	                controlBarAutoHide: false,
	                scaleMode : "letterbox",
	                playButtonOverlay: true,
	                showVideoInfoOverlayOnStartUp: true,
	                javascriptCallbackFunction: "onJavaScriptBridgeCreated"	
	            };
	            
	            for (var i = 0; i < parameterNames.length; i++) {
	                var parameterName = parameterNames[i];
	                parameters[parameterName] = pqs.param(parameterName) ||
	                parameters[parameterName];
	            }
	            
	            if (parameters.hasOwnProperty("logFilter"))
	            {
	            	org.osmf.player.debug.filter = parameters.logFilter;
	            	delete parameters.logFilter;
	            	document.getElementById("logFilter").value = org.osmf.player.debug.filter;
	            }
	            
	            var wmodeValue = "direct";
	            var wmodeOptions = ["direct", "opaque", "transparent", "window"];
	            if (parameters.hasOwnProperty("wmode"))
	            {
	            	if (wmodeOptions.indexOf(parameters.wmode) >= 0)
	            	{
	            		wmodeValue = parameters.wmode;
	            	}	            	
	            	delete parameters.wmode;
	            }
	            
	            // Embed the player SWF:	            
	            swfobject.embedSWF(
					"../swf/StrobeMediaPlayback.swf"
					, "StrobeMediaPlayback"
					, 640
					, 480
					, "10.1.0"
					, "../swf/expressInstall.swf"
					, parameters
					, {
		                allowFullScreen: "true",
		                wmode: wmodeValue
		            }
					, {
		                name: "StrobeMediaPlayback"
		            }
				);
				
				
			}
			window.onload = loadStrobeMediaPlayback;
			
			var player = null;
			function onJavaScriptBridgeCreated(playerId)
			{
				if (player == null) {
					player = document.getElementById(playerId);
					
					// Add event listeners that will update the 
					player.addEventListener("isDynamicStreamChange", "updateDynamicStreamItems");
					player.addEventListener("switchingChange", "updateDynamicStreamItems");
					player.addEventListener("autoSwitchChange", "updateDynamicStreamItems");
					player.addEventListener("mediaSizeChange", "updateDynamicStreamItems");
				}
			}
			
			var next = 10;
			function updateDynamicStreamItems()
			{
				document.getElementById("dssc").style.display = "block";
				var dynamicStreams = player.getStreamItems();
				var ds = document.getElementById("dssc-items");
				var switchMode = player.getAutoDynamicStreamSwitch() ? "ON" : "OFF"; 
				var currentStreamIndex = player.getCurrentDynamicStreamIndex();
				var bitrates = ["300","500","800","1100","1600","2600"];
				var playing = "";				
				if (currentStreamIndex == next){
					next = 10;
				}

				if (switchMode == "ON"){
					var dsItems = '<a href="#" class="playing" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;">&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</a>';
					for (var idx = 0; idx < dynamicStreams.length; idx ++)
					{
						if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "hold"; }					
						dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
					}
				}
				else {
					var dsItems = '<a href="#" class="normal" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;">&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</a>';
					if (next == 10)
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "normal"; }
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="' + playing + '" onclick="switchDynamicStreamIndex(' + idx + '); return false;">' + bitrates[idx] + '</a>';				
						}
					}
					else
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else if (next == idx) { playing = "waiting"; } else { playing = "hold"; }					
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
						}
					}
				}
				ds.innerHTML = dsItems;
			}
			
			function switchDynamicStreamIndex(index)
			{
				if (player.getAutoDynamicStreamSwitch())
				{
					player.setAutoDynamicStreamSwitch(false);	
				}
				player.switchDynamicStreamIndex(index);
				next = index;
			}
        </script>
        <style type="text/css">
        <!--
			.white {
				font-family: Verdana, Geneva, sans-serif;
				color: white;
			}
        	.normal {
				font-family: Verdana, Geneva, sans-serif;
				color: #969696;
			}
			.normal:link {text-decoration:none;}
			.normal:visited {text-decoration:none;}
			.normal:hover {text-decoration:none;color:white;}
			.normal:active {text-decoration:none;color:white;}			
			.waiting {
				font-family: Verdana, Geneva, sans-serif;
				color: white;
				text-decoration: blink;
			}					
			.waiting:hover {text-decoration:none;}
			.hold {
				font-family: Verdana, Geneva, sans-serif;
				color: #464646;
			}
			.hold:hover {text-decoration:none;}			
			.playing {
				font-family: Verdana, Geneva, sans-serif;
				color: #E1D275;
			}
			.playing:link {text-decoration:none;}
			.playing:visited {text-decoration:none;}
			.playing:hover {text-decoration:none;}
			.playing:active {text-decoration:none;}			
        -->
        </style>
    </head>
    <body>
    <?php
    $taille = strlen($file);
    $taille = $taille - 2;
    $file_next = substr($file ,$taille,2);
    $file_next= $file_next+1;
    $file_before = $file_next - 2;
    
    if($file_next < 10){
	    $file_next = "0".$file_next;
    }
    if($file_before < 1){
	    $file_before = 1;
    }

    if($file_before < 10){
	    $file_before = "0".$file_before;
    }
        
    echo "<a href=smptest.php?f=".$dir0.$file_before.">précédent</a> ## ".$file." ## <a href=smptest.php?f=".$dir0.$file_next.">suivant</a>";
    ?>
    
        <table width="100%" border="0" cellspacing="5">
            <tr>
                <td valign="top">
                    <table width="640" border="0" cellspacing="5" style="background-color:#000000;">
                        <tr>
                            <td valign="top" width="640" colspan="3">
                                <div id="StrobeMediaPlayback">
                                    <p>
                                        Alternative content
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">  
                                <div id="dssc" style="display:none;margin:auto;width:640px">
									<div style="float:left;" class="white">&nbsp;&nbsp;&nbsp;Quality (kbps) :</div>
									<div id="dssc-items" style="float:left;">
										The available qualities will be loaded once the playback starts...
									</div>
								</div>								
                            </td>
                        </tr>						
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
