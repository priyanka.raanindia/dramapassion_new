<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- css -->
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_free.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_bottom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<link type="text/css" href="<?php echo $server ;?>css/custom-theme/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo $server; ?>admin/datatable/css/jquery.dataTables.css" type="text/css" media="screen" />


<!-- meta -->
<title>Admin</title>

<!-- script java -->
<script src="<?php echo $server; ?>/js/scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>/js/AC_OETags.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>admin/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/js/flowplayer-3.2.4.min.js"></script>
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $server; ?>admin/datatable/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script type="text/javascript">

</script>
<style>
.iframe{
	font-size: 12px;
	color: blue;
}
.iframe:hover{
	font-size: 12px;
	color: red;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="conteneur_all_page">
<div class="contenant_all_page">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top" height="93" style="background-image:url(<?php echo $http ; ?>images/header.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER GRIS-->
        
        <div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" height="93" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="368" height="93"><a href="<?php echo $http ; ?>admin/" ><img src="<?php echo $http ; ?>images/logo.png" width="368" height="93" alt="Logo Dramapassion" title="Logo Dramapassion" border="0"></a></td>
                   <td width="368" height="93"><h1>ADMIN</h1></td>
               
                </tr>
            </table>
		</div>
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
    
	<tr>
        <td valign="top" height="37" style="background-image:url(<?php echo $http ; ?>images/header2.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/index.php" class="lien_blanc" style="text-align: right">Drama</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/user.php" class="lien_blanc" style="text-align: right">User</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="old/orderlist.php" target="_blank" class="lien_blanc" style="text-align: right">DVD</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/stats.php" class="lien_blanc" style="text-align: right">Stats</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/serveradmin.php" class="lien_blanc" style="text-align: right">Servers</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/presse.php" class="lien_blanc" style="text-align: right">Presse</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin_acteurs" class="lien_blanc" style="text-align: right">Acteurs</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/track_select" class="lien_blanc" style="text-align: right">Track</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				

				</tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>