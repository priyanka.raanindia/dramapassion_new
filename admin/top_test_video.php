<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- css -->
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_free.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_bottom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<link type="text/css" href="<?php echo $server ;?>css/custom-theme/jquery-ui-1.8.19.custom.css" rel="stylesheet" />


<!-- meta -->
<title>Admin</title>

<script src="<?php echo $server; ?>/js/scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>/js/AC_OETags.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<?php if ($os == 'Ps' || $os == 'Android'){ ?>
	<script type="text/javascript" src="<?php echo $http ; ?>js/flowplayer-3.2.4.min.js"></script>		
<?php } else { ?>
	<script type="text/javascript" src="<?php echo $http ; ?>js/flowplayer-3.2.12.min.js"></script>	
<?php } ?>
<script type="text/javascript" src="<?php echo $http ; ?>js/flash_detect_min.js"></script>
<script type="text/javascript" src="<?php echo $http ; ?>js/swfobject.js"></script>
<script src="<?php echo $http ; ?>js/jquery.dropkick-1.0.0.js" type="text/javascript" charset="utf-8"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
