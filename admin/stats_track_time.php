<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");


$date = $_GET['date'];
$nb_caract = strlen($date);


if($nb_caract < 12){
	$date = $date.' 00:00:00';
	$time_1 = strtotime($date);
	$time_2 = $time_1+(60*60*24);
}else{
	$date_exp = explode(" - ", $date);
	$date_1 = $date_exp[0];
	$date_2 = $date_exp[1];
	
	$date_1 = $date_1.' 00:00:00';
	$date_2 = $date_2.' 00:00:00';
	
	$time_1 = strtotime($date_1);
	$time_2 = strtotime($date_2)+(60*60*24);
}



$req = "SELECT * FROM user_track.time_track WHERE time_1 >= $time_1 AND time_1 <= $time_2 AND id_cookies !='' ORDER BY url";
$sql = mysql_query($req);


$tab_sortie = array();

$tab_sortie['tot_5'] = 0;
$tab_sortie['tot_6_30'] = 0;
$tab_sortie['tot_31_300'] = 0;
$tab_sortie['tot_301'] = 0;

$tab_sortie['index_5'] = 0;
$tab_sortie['index_6_30'] = 0;
$tab_sortie['index_31_300'] = 0;
$tab_sortie['index_301'] = 0;

$tab_sortie['epi_5'] = 0;
$tab_sortie['epi_6_30'] = 0;
$tab_sortie['epi_31_300'] = 0;
$tab_sortie['epi_301'] = 0;

$tab_sortie['drama_5'] = 0;
$tab_sortie['drama_6_30'] = 0;
$tab_sortie['drama_31_300'] = 0;
$tab_sortie['drama_301'] = 0;

$tab_sortie['cata_5'] = 0;
$tab_sortie['cata_6_30'] = 0;
$tab_sortie['cata_31_300'] = 0;
$tab_sortie['cata_301'] = 0;

$tab_sortie['dl_5'] = 0;
$tab_sortie['dl_6_30'] = 0;
$tab_sortie['dl_31_300'] = 0;
$tab_sortie['dl_301'] = 0;

$tab_sortie['premium_5'] = 0;
$tab_sortie['premium_6_30'] = 0;
$tab_sortie['premium_31_300'] = 0;
$tab_sortie['premium_301'] = 0;


$tab_sortie['dvd_5'] = 0;
$tab_sortie['dvd_6_30'] = 0;
$tab_sortie['dvd_31_300'] = 0;
$tab_sortie['dvd_301'] = 0;

$tab_sortie['tot_moy'] = 0;
$tab_sortie['index_moy'] = 0;
$tab_sortie['epi_moy'] = 0;
$tab_sortie['drama_moy'] = 0;
$tab_sortie['cata_moy'] = 0;
$tab_sortie['dl_moy'] = 0;
$tab_sortie['premium_moy'] = 0;
$tab_sortie['dvd_moy'] = 0;

$tab_sortie_temp['tot_moy'] = 0;
$tab_sortie_temp['index_moy'] = 0;
$tab_sortie_temp['epi_moy'] = 0;
$tab_sortie_temp['drama_moy'] = 0;
$tab_sortie_temp['cata_moy'] = 0;
$tab_sortie_temp['dl_moy'] = 0;
$tab_sortie_temp['premium_moy'] = 0;
$tab_sortie_temp['dvd_moy'] = 0;


while($row=mysql_fetch_assoc($sql)){
	$verif = 0;
	if($row['url'] == '/'){
		if($row['time_2'] > 0){
			$time_diff = $row['time_2'] - $row['time_1'];
			$verif = 1;
		}else{
			if($row['time_3'] > 0){
				$time_diff = $row['time_3'] - $row['time_1'];
				$verif = 1;
			}
		}
		if($verif == 1){
		$tab_sortie['index_moy'] = $tab_sortie['index_moy'] + $time_diff;
		$tab_sortie_temp['index_moy'] = $tab_sortie_temp['index_moy'] + 1;

		
		$tab_sortie['tot_moy'] = $tab_sortie['tot_moy'] + $time_diff;
		$tab_sortie_temp['tot_moy'] = $tab_sortie_temp['tot_moy'] +1;

		
		if($time_diff <= 5){
			$tab_sortie['index_5'] = $tab_sortie['index_5']+1;
			$tab_sortie['tot_5'] = $tab_sortie['tot_5']+1;
		}else if($time_diff > 5 && $time_diff <= 30){
			$tab_sortie['index_6_30'] = $tab_sortie['index_6_30']+1;
			$tab_sortie['tot_6_30'] = $tab_sortie['tot_6_30']+1;
		}else if($time_diff > 30 && $time_diff <= 300){
			$tab_sortie['index_31_300'] = $tab_sortie['index_31_300']+1;
			$tab_sortie['tot_31_300'] = $tab_sortie['tot_31_300']+1;
		}else{
			$tab_sortie['index_301'] = $tab_sortie['index_301']+1;
			$tab_sortie['tot_301'] = $tab_sortie['tot_301']+1;
		}
		}
	}
	else if(preg_match("#^/drama/#", $row['url'])){
		$url_sans = substr($row['url'], 0, -1);
		
		if(preg_match("#[0-9]{2}$#", $url_sans)){
			if($row['time_2'] > 0){
				$time_diff = $row['time_2'] - $row['time_1'];
				$verif = 1;
			}else{
				if($row['time_3'] > 0){
					$time_diff = $row['time_3'] - $row['time_1'];
					$verif = 1;
				}
			}
			if($verif == 1){
			
			$tab_sortie['epi_moy'] = $tab_sortie['epi_moy'] + $time_diff;
			$tab_sortie_temp['epi_moy'] = $tab_sortie_temp['epi_moy'] + 1;

			
			$tab_sortie['tot_moy'] = $tab_sortie['tot_moy'] + $time_diff;
			$tab_sortie_temp['tot_moy'] = $tab_sortie_temp['tot_moy'] +1;

			
			if($time_diff <= 5){
				$tab_sortie['epi_5'] = $tab_sortie['epi_5']+1;
				$tab_sortie['tot_5'] = $tab_sortie['tot_5']+1;
			}else if($time_diff > 5 && $time_diff <= 30){
				$tab_sortie['epi_6_30'] = $tab_sortie['epi_6_30']+1;
				$tab_sortie['tot_6_30'] = $tab_sortie['tot_6_30']+1;
			}else if($time_diff > 30 && $time_diff <= 300){
				$tab_sortie['epi_31_300'] = $tab_sortie['epi_31_300']+1;
				$tab_sortie['tot_31_300'] = $tab_sortie['tot_31_300']+1;
			}else{
				$tab_sortie['epi_301'] = $tab_sortie['epi_301']+1;
				$tab_sortie['tot_301'] = $tab_sortie['tot_301']+1;
			}
			}
		}else{
			if($row['time_2'] > 0){
				$time_diff = $row['time_2'] - $row['time_1'];
				$verif = 1;
			}else{
				if($row['time_3'] > 0){
					$time_diff = $row['time_3'] - $row['time_1'];
					$verif = 1;
				}
			}
			if($verif == 1){
			
			$tab_sortie['drama_moy'] = $tab_sortie['drama_moy'] + $time_diff;
			$tab_sortie_temp['drama_moy'] = $tab_sortie_temp['drama_moy'] + 1;

			
			$tab_sortie['tot_moy'] = $tab_sortie['tot_moy'] + $time_diff;
			$tab_sortie_temp['tot_moy'] = $tab_sortie_temp['tot_moy'] +1;

			
			if($time_diff <= 5){
				$tab_sortie['drama_5'] = $tab_sortie['drama_5']+1;
				$tab_sortie['tot_5'] = $tab_sortie['tot_5']+1;
			}else if($time_diff > 5 && $time_diff <= 30){
				$tab_sortie['drama_6_30'] = $tab_sortie['drama_6_30']+1;
				$tab_sortie['tot_6_30'] = $tab_sortie['tot_6_30']+1;
			}else if($time_diff > 30 && $time_diff <= 300){
				$tab_sortie['drama_31_300'] = $tab_sortie['drama_31_300']+1;
				$tab_sortie['tot_31_300'] = $tab_sortie['tot_31_300']+1;
			}else{
				$tab_sortie['drama_301'] = $tab_sortie['drama_301']+1;
				$tab_sortie['tot_301'] = $tab_sortie['tot_301']+1;
			}
			}
		}
	}
	else if(preg_match("#^/catalogue/#", $row['url'])){
		if($row['time_2'] > 0){
			$time_diff = $row['time_2'] - $row['time_1'];
			$verif = 1;
		}else{
			if($row['time_3'] > 0){
				$time_diff = $row['time_3'] - $row['time_1'];
				$verif = 1;
			}
		}
		if($verif == 1){
		
		$tab_sortie['cata_moy'] = $tab_sortie['cata_moy'] + $time_diff;
		$tab_sortie_temp['cata_moy'] = $tab_sortie_temp['cata_moy'] + 1;

			
		$tab_sortie['tot_moy'] = $tab_sortie['tot_moy'] + $time_diff;
		$tab_sortie_temp['tot_moy'] = $tab_sortie_temp['tot_moy'] +1;

		
		if($time_diff <= 5){
			$tab_sortie['cata_5'] = $tab_sortie['cata_5']+1;
			$tab_sortie['tot_5'] = $tab_sortie['tot_5']+1;
		}else if($time_diff > 5 && $time_diff <= 30){
			$tab_sortie['cata_6_30'] = $tab_sortie['cata_6_30']+1;
			$tab_sortie['tot_6_30'] = $tab_sortie['tot_6_30']+1;
		}else if($time_diff > 30 && $time_diff <= 300){
			$tab_sortie['cata_31_300'] = $tab_sortie['cata_31_300']+1;
			$tab_sortie['tot_31_300'] = $tab_sortie['tot_31_300']+1;
		}else{
			$tab_sortie['cata_301'] = $tab_sortie['cata_301']+1;
			$tab_sortie['tot_301'] = $tab_sortie['tot_301']+1;
		}
		}
	}
	else if(preg_match("#^/dl.php#", $row['url'])){
		if($row['time_2'] > 0){
			$time_diff = $row['time_2'] - $row['time_1'];
			$verif = 1;
		}else{
			if($row['time_3'] > 0){
				$time_diff = $row['time_3'] - $row['time_1'];
				$verif = 1;
			}
		}
		if($verif == 1){
		
		$tab_sortie['dl_moy'] = $tab_sortie['dl_moy'] + $time_diff;
		$tab_sortie_temp['dl_moy'] = $tab_sortie_temp['dl_moy'] + 1;

			
		$tab_sortie['tot_moy'] = $tab_sortie['tot_moy'] + $time_diff;
		$tab_sortie_temp['tot_moy'] = $tab_sortie_temp['tot_moy'] +1;

		
		if($time_diff <= 5){
			$tab_sortie['dl_5'] = $tab_sortie['dl_5']+1;
			$tab_sortie['tot_5'] = $tab_sortie['tot_5']+1;
		}else if($time_diff > 5 && $time_diff <= 30){
			$tab_sortie['dl_6_30'] =$tab_sortie['dl_6_30']+1;
			$tab_sortie['tot_6_30'] = $tab_sortie['tot_6_30']+1;
		}else if($time_diff > 30 && $time_diff <= 300){
			$tab_sortie['dl_31_300'] = $tab_sortie['dl_31_300']+1;
			$tab_sortie['tot_31_300'] = $tab_sortie['tot_31_300']+1;
		}else{
			$tab_sortie['dl_301'] = $tab_sortie['dl_301']+1;
			$tab_sortie['tot_301'] = $tab_sortie['tot_301']+1;
		}
		}
	}
	else if(preg_match("#^/premium/#", $row['url'])){
		if($row['time_2'] > 0){
			$time_diff = $row['time_2'] - $row['time_1'];
			$verif = 1;
		}else{
			if($row['time_3'] > 0){
				$time_diff = $row['time_3'] - $row['time_1'];
				$verif = 1;
			}
		}
		if($verif == 1){
		
		$tab_sortie['premium_moy'] = $tab_sortie['premium_moy'] + $time_diff;
		$tab_sortie_temp['premium_moy'] = $tab_sortie_temp['premium_moy'] + 1;

			
		$tab_sortie['tot_moy'] = $tab_sortie['tot_moy'] + $time_diff;
		$tab_sortie_temp['tot_moy'] = $tab_sortie_temp['tot_moy'] +1;

		
		if($time_diff <= 5){
			$tab_sortie['premium_5'] = $tab_sortie['premium_5']+1;
			$tab_sortie['tot_5'] = $tab_sortie['tot_5']+1;
		}else if($time_diff > 5 && $time_diff <= 30){
			$tab_sortie['premium_6_30'] = $tab_sortie['premium_6_30']+1;
			$tab_sortie['tot_6_30'] = $tab_sortie['tot_6_30']+1;
		}else if($time_diff > 30 && $time_diff <= 300){
			$tab_sortie['premium_31_300'] = $tab_sortie['premium_31_300']+1;
			$tab_sortie['tot_31_300'] = $tab_sortie['tot_31_300']+1;
		}else{
			$tab_sortie['premium_301'] = $tab_sortie['premium_301']+1;
			$tab_sortie['tot_301'] = $tab_sortie['tot_301']+1;
		}
		}
	}
	else if(preg_match("#^/dvd/#", $row['url'])){
		if($row['time_2'] > 0){
			$time_diff = $row['time_2'] - $row['time_1'];
			$verif = 1;
		}else{
			if($row['time_3'] > 0){
				$time_diff = $row['time_3'] - $row['time_1'];
				$verif = 1;
			}
		}
		if($verif == 1){
		
		$tab_sortie['dvd_moy'] = $tab_sortie['dvd_moy'] + $time_diff;
		$tab_sortie_temp['dvd_moy'] = $tab_sortie_temp['dvd_moy'] + 1;

			
		$tab_sortie['tot_moy'] = $tab_sortie['tot_moy'] + $time_diff;
		$tab_sortie_temp['tot_moy'] = $tab_sortie_temp['tot_moy'] +1;

		
		
		if($time_diff <= 5){
			$tab_sortie['dvd_5'] = $tab_sortie['dvd_5']+1;
			$tab_sortie['tot_5'] = $tab_sortie['tot_5']+1;
		}else if($time_diff > 5 && $time_diff <= 30){
			$tab_sortie['dvd_6_30'] = $tab_sortie['dvd_6_30']+1;
			$tab_sortie['tot_6_30'] = $tab_sortie['tot_6_30']+1;
		}else if($time_diff > 30 && $time_diff <= 300){
			$tab_sortie['dvd_31_300'] = $tab_sortie['dvd_31_300']+1;
			$tab_sortie['tot_31_300'] = $tab_sortie['tot_31_300']+1;
		}else{
			$tab_sortie['dvd_301'] = $tab_sortie['dvd_301']+1;
			$tab_sortie['tot_301'] = $tab_sortie['tot_301']+1;
		}
		}
	}
	
}

$tab_sortie['tot_moy'] = ($tab_sortie['tot_moy']/$tab_sortie_temp['tot_moy'])/60;
$tab_sortie['index_moy'] = ($tab_sortie['index_moy']/$tab_sortie_temp['index_moy'])/60;
$tab_sortie['epi_moy'] = ($tab_sortie['epi_moy']/$tab_sortie_temp['epi_moy'])/60;
$tab_sortie['drama_moy'] = ($tab_sortie['drama_moy']/$tab_sortie_temp['drama_moy'])/60;
$tab_sortie['cata_moy'] = ($tab_sortie['cata_moy']/$tab_sortie_temp['cata_moy'])/60;
$tab_sortie['dl_moy'] = ($tab_sortie['dl_moy']/$tab_sortie_temp['dl_moy'])/60;
$tab_sortie['premium_moy'] = ($tab_sortie['premium_moy']/$tab_sortie_temp['premium_moy'])/60;
$tab_sortie['dvd_moy'] = ($tab_sortie['dvd_moy']/$tab_sortie_temp['dvd_moy'])/60;




ksort($tab_sortie);
// le header pour informer le navigateur du format des données
header( 'Content-type: application/json' );
 
// la sortie de texte : mon tableau encodé au format json
echo json_encode( $tab_sortie );
?>