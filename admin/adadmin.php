<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("../includes/functions.php");
require_once("top.php");

//get server balancing data
$sql_adserver = "SELECT * FROM ad_adserver";
$que_adserver = mysql_query($sql_adserver);
$pc_sticky = mysql_result($que_adserver, 0, 'Sticky');
$pc_advideum = mysql_result($que_adserver, 0, 'Advideum');
$pc_fourmedia = mysql_result($que_adserver, 0, 'Fourmedia');
$pc_adsense = mysql_result($que_adserver, 0, 'Adsense');
$pc_himedia = mysql_result($que_adserver, 0, 'Himedia');
$max_sticky = mysql_result($que_adserver, 1, 'Sticky');
$max_advideum = mysql_result($que_adserver, 1, 'Advideum');
$max_fourmedia = mysql_result($que_adserver, 1, 'Fourmedia');
$max_adsense = mysql_result($que_adserver, 1, 'Adsense');
$max_himedia = mysql_result($que_adserver, 1, 'Himedia');

//get statistics
$sql_stat = "SELECT * FROM ad_pubstat WHERE Date >= DATE_SUB(CURDATE(),INTERVAL 30 DAY) ORDER BY Date DESC";
$que_stat = mysql_query($sql_stat);
$rows = mysql_num_rows($que_stat);
?>
<tr>
        <td valign="top" height="37" style="background-image:url(<?php echo $http ; ?>images/header.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/stats.php" class="lien_noir" style="text-align: right">Stats Abo / Vue</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/stats_click.php" class="lien_noir" style="text-align: right">Stats Click</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/adadmin.php" class="lien_noir" style="text-align: right">Ads repartition</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/stats_coupon.php" class="lien_noir" style="text-align: right">Stats coupon</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/stats_widget.php" class="lien_noir" style="text-align: right">Stats widget</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				<td class="blanc bloc_center" width="70"><a href="<?php echo $http ;?>admin/facebook_like_aff.php" class="lien_noir" style="text-align: right">Facebook track</a><td>
				<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
				</tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>
<tr><td>
<tr>
	<td>  
		<div style="width:1000px;margin:auto;">
			<h1>Volume allocation</h1>
			<form name="volume" method="post" action="adadminaction.php">
			<table>
			<tr>
				<td>StickyAds :&nbsp;</td>
				<td><input size="3" type="text" name="sticky" value="<?php echo $pc_sticky; ?>">&nbsp;%</td>
				<td><input size="7" type="text" name="sticky_max" value="<?php echo $max_sticky; ?>">&nbsp;par jour</td>
			</tr>
			<tr>
				<td>Advideum :&nbsp;</td>
				<td><input size="3" type="text" name="advideum" value="<?php echo $pc_advideum; ?>">&nbsp;%</td>
				<td><input size="7" type="text" name="advideum_max" value="<?php echo $max_advideum; ?>">&nbsp;par jour</td>
			</tr>
			<tr>
				<td>Fourmedia :&nbsp;</td>
				<td><input size="3" type="text" name="fourmedia" value="<?php echo $pc_fourmedia; ?>">&nbsp;%</td>
				<td><input size="7" type="text" name="fourmedia_max" value="<?php echo $max_fourmedia; ?>">&nbsp;par jour</td>
			</tr>
			<tr>
				<td>Adsense :&nbsp;</td>
				<td><input size="3" type="text" name="adsense" value="<?php echo $pc_adsense; ?>">&nbsp;%</td>
				<td><input size="7" type="text" name="adsense_max" value="<?php echo $max_adsense; ?>">&nbsp;par jour</td>
			</tr>
			<tr>
				<td>Hi-media :&nbsp;</td>
				<td><input size="3" type="text" name="himedia" value="<?php echo $pc_himedia; ?>">&nbsp;%</td>
				<td><input size="7" type="text" name="himedia_max" value="<?php echo $max_himedia; ?>">&nbsp;par jour</td>
			</tr>
			</table>
			<br />	
			<input type="submit" value="UPDATE">
			</form>

			<br />
			<h1>Statistics</h1>
			<table border="1" width="1000">
				<tr>
					<td align="left">Date</td>
					<td align="right">Total #</td>
					<td align="right">Sticky #</td>
					<td align="right">Advideum #</td>
					<td align="right">FourMedia #</td>
					<td align="right">Adsense #</td>
					<td align="right">Himedia #</td>
					<td align="right">Sticky %</td>
					<td align="right">Advideum %</td>
					<td align="right">FourMedia %</td>
					<td align="right">Adsense %</td>
					<td align="right">Himedia %</td>			
				</tr>
<?php
	for($i=0; $i<$rows; $i++){
		$jour = mysql_result($que_stat, $i, 'Date');
		//$totcnt = mysql_result($que_stat, $i, 'Total');
		$sticnt = mysql_result($que_stat, $i, 'Sticky');
		$advcnt = mysql_result($que_stat, $i, 'Advideum');
		$himcnt = mysql_result($que_stat, $i, 'Himedia');
		$fmecnt = mysql_result($que_stat, $i, 'Fourmedia');
		$adscnt = mysql_result($que_stat, $i, 'Adsense');
		$totcnt = $sticnt + $advcnt + $himcnt + $fmecnt + $adscnt;
		$stipc = $sticnt / $totcnt * 100;	
		$advpc = $advcnt / $totcnt * 100;	
		$himpc = $himcnt / $totcnt * 100;	
		$fmepc = $fmecnt / $totcnt * 100;	
		$adspc = $adscnt / $totcnt * 100;	
		$stipc = round($stipc,1);
		$advpc = round($advpc,1);
		$himpc = round($himpc,1);
		$fmepc = round($fmepc,1);
		$adspc = round($adspc,1);
?>
<tr>
	<td align="left"><?php echo $jour; ?></td>
	<td align="right"><?php echo $totcnt; ?></td>
	<td align="right"><?php echo $sticnt; ?></td>
	<td align="right"><?php echo $advcnt; ?></td>
	<td align="right"><?php echo $fmecnt; ?></td>
	<td align="right"><?php echo $adscnt; ?></td>
	<td align="right"><?php echo $himcnt; ?></td>
	<td align="right"><?php echo $stipc; ?>&nbsp;%</td>
	<td align="right"><?php echo $advpc; ?>&nbsp;%</td>
	<td align="right"><?php echo $fmepc; ?>&nbsp;%</td>
	<td align="right"><?php echo $adspc; ?>&nbsp;%</td>
	<td align="right"><?php echo $himpc; ?>&nbsp;%</td>
</tr>
<?php		
	}	
?>			
			</table>
		</div>
    </td>
</tr>
<?php
require_once("bottom.php");
?>