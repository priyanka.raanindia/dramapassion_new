<?php
require_once("../includes/dbinfo.inc.php");

//format du CSV
$csv_terminated = "\n";
$csv_separator = ",";
$csv_enclosed = '';
$csv_escaped = "\\";
 
// requête MySQL
$sql_query = "SELECT * FROM newsletter WHERE newsletter = 1";
  
// exécute la commande
$result = mysql_query($sql_query);
$fields_cnt = mysql_num_fields($result);
 
 // fin for
 
//$out = trim(substr($schema_insert, 0, -1));
//$out .= $csv_terminated;
 
// Format des données

while ($row = mysql_fetch_array($result))
{
    $schema_insert = '';
    for ($j = 0; $j < $fields_cnt; $j++)
    {
	if ($row[$j] == '0' || $row[$j] != '')
	{
 
	    if ($csv_enclosed == '')
	    {
		$schema_insert .= $row[$j];
	    } else
	    {
		$schema_insert .= $csv_enclosed .
				    str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row[$j]) . $csv_enclosed;
	    }
	} else
	{
	    $schema_insert .= '';
	}
 
	if ($j < $fields_cnt - 1)
	{
	    $schema_insert .= $csv_separator;
	}
    } // fin for
 
    $out .= $schema_insert;
    $out .= $csv_terminated;
} // fin while
$date = date("Y-m-d");
$nom_fichier = "liste_newsletter_".$date.".csv";
// Envoie au fureteur pour le téléchargement
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=".$nom_fichier);
echo $out;
exit; ?>