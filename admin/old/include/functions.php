<?php
function getImage($filename,$folder,$w,$h){
	$imagepath = "";
	if(file_exists("../content/".$folder."/".$filename.".jpg")){
		$imagepath = "<img src=\"../content/".$folder."/".$filename.".jpg\" width=$w height=$h alt='".substr(str_replace("'"," ",$filename),0,strpos($filename,"_"))."' title='".substr(str_replace("'"," ",$filename),0,strpos($filename,"_"))."'>";
	}else{
		$imagepath = "";
	}
	echo $imagepath;
}
function getRanking($totals,$dramas){
	$ranks = array();
	for($i=0;$i<mysql_num_rows($dramas);$i++){
		$ranks[$i]['id'] = mysql_result($dramas,$i,"ProductID");
		$score = mysql_result($dramas,$i,"Totals")/$totals*100;	
		if($score < 12)    { $score = 8.0; }
		elseif($score < 13){ $score = 8.1; }
		elseif($score < 14){ $score = 8.2; }
		elseif($score < 15){ $score = 8.3; }
		elseif($score < 16){ $score = 8.4; }
		elseif($score < 17){ $score = 8.5; }
		elseif($score < 18){ $score = 8.6; }
		elseif($score < 19){ $score = 8.7; }
		elseif($score < 20){ $score = 8.8; }
		elseif($score < 21){ $score = 8.9; }
		elseif($score < 22){ $score = 9.0; }
		elseif($score < 23){ $score = 9.1; }
		elseif($score < 24){ $score = 9.2; }
		elseif($score < 25){ $score = 9.3; }
		elseif($score < 26){ $score = 9.4; }
		elseif($score < 27){ $score = 9.5; }
		elseif($score < 28){ $score = 9.6; }
		elseif($score < 29){ $score = 9.7; }
		elseif($score < 30){ $score = 9.8; }
		elseif($score > 30){ $score = 9.9; }
		
		$ranks[$i]['score'] = $score;
	}
	return $ranks;
}
function getexpirydate($expiry){
	$expirydate = array();
	$expirydif = date_diff(date("Y-m-d H:i:s"),$expiry);
	if($expirydif <= (48*60*60) && $expirydif >= 0){
		$expirydate[0] = sec2hms($expirydif);
		$expirydate[1] = true;
	}else if($expirydif < 0){
		$expirydate[0] = $expiry;
		$expirydate[1] = false;
	}else{
		$expirydate[0] = round($expirydif / 60 / 60 / 24) ." days";
		$expirydate[1] = false;
	}
	return $expirydate;
}
function cleanup($data) {
   $data = mysql_escape_string(trim(strip_tags($data)));
   return $data;
}
//Calculates the difference in seconds between to timestamps
/*
function date_diff($start,$end){
	$s = strtotime($start); // Convert datetime to UNIX seconds
	$e = strtotime($end);	// Convert datetime to UNIX seconds
	$difference = $e - $s;	// Subtract endtime with starttime
	return $difference;		// Return amount of seconds between two times
}
*/
//Converts a number of seconds in H:m:s format
function sec2hms ($sec, $padHours = false) {
    $hms = "";
    $hours = intval(intval($sec) / 3600);					// Calculate hours
    $hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'		// Add divider 'h ' if leading 0 is set true
          : $hours. ':';									// Add divider 'h ' if leading 0 is set false
    $minutes = intval(($sec / 60) % 60); 					// Calculate remaining minutes
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ':';	// Add divider 'm '
    $seconds = intval($sec % 60); 							// Calculate remaining seconds
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT). '';	// Add divider 's '
    return $hms; 											// Return #h #m #s
}

//Converts a number of seconds in d u m format
function sec2dhms ($sec, $padHours = false) {
    $hms = "";
	$days = intval(intval($sec) / (3600 * 24));
	$sec = $sec - ($days * 24 * 3600);
	$hms .= str_pad($days, 2, "0", STR_PAD_LEFT). 'd ';
    $hours = intval(intval($sec) / 3600);					// Calculate hours
    $hms .= str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ';	// Add divider 'h ' if leading 0 is set true
    $minutes = intval(($sec / 60) % 60); 					// Calculate remaining minutes
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). 'm ';	// Add divider 'm '
    $seconds = intval($sec % 60); 							// Calculate remaining seconds
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT). 's';	// Add divider 's '
    return $hms; 											// Return #h #m #s
}

function valid_email($email){
	// First, we check that there's one @ symbol, and that the lengths are right
	if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
		return false;
	}
	// Split it into sections to make life easier
	$email_array = explode("@", $email);
	$local_array = explode(".", $email_array[0]);
	for ($i = 0; $i < sizeof($local_array); $i++) {
		if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
			return false;
		}
	}
	if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
		$domain_array = explode(".", $email_array[1]);
		if (sizeof($domain_array) < 2) {
			return false; // Not enough parts to domain
		}
		for ($i = 0; $i < sizeof($domain_array); $i++) {
			if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
				return false;
			}
		}
	}
	return true;
}
function valid_url($uri){
	if($uri == "" || $uri == "http://"){
		return true;
	}else{
		if( preg_match('/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}'.'((:[0-9]{1,5})?\/.*)?$/i' ,$uri)){
			return true;
		} else {
			return false;
		}
	}
}
function browser_detection( $which_test ) {
	// initialize the variables
	$browser = '';
	$dom_browser = '';

	// set to lower case to avoid errors, check to see if http_user_agent is set
	$navigator_user_agent = ( isset( $_SERVER['HTTP_USER_AGENT'] ) ) ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) : '';

	// run through the main browser possibilities, assign them to the main $browser variable
	if (stristr($navigator_user_agent, "opera")){
		$browser = 'opera';
		$dom_browser = true;
	}elseif (stristr($navigator_user_agent, "msie 4")){
		$browser = 'msie4'; 
		$dom_browser = false;
	}elseif (stristr($navigator_user_agent, "msie")){
		$browser = 'msie'; 
		$dom_browser = true;
	}elseif ((stristr($navigator_user_agent, "konqueror")) || (stristr($navigator_user_agent, "safari"))) 	{
		$browser = 'safari'; 
		$dom_browser = true;
	}elseif (stristr($navigator_user_agent, "gecko")) 	{
		$browser = 'mozilla';
		$dom_browser = true;
	}elseif (stristr($navigator_user_agent, "mozilla/4")) 	{
		$browser = 'ns4';
		$dom_browser = false;
	}else 	{
		$dom_browser = false;
		$browser = false;
	}

	// return the test result you want
	if ( $which_test == 'browser' ){
		return $browser;
	}elseif ( $which_test == 'dom' ){
		return $dom_browser;
		//  note: $dom_browser is a boolean value, true/false, so you can just test if
		// it's true or not.
	}
}
function os_detection($p_sAgent = NULL){
    ($p_sAgent===NULL ?    $sAgent = strtolower($_SERVER['HTTP_USER_AGENT']) :    $sAgent = strtolower($p_sAgent));
    $sOs = '';
        switch(true){
			case strpos($sAgent,'windows nt 6.0') : $sOs = 'Windows Vista';
			break;
			case strpos($sAgent,'windows nt 5.2') : $sOs = 'Windows 2003 server';
			break;
			case strpos($sAgent,'windows nt 5.1') : $sOs = 'Windows XP';
			break;
			case strpos($sAgent,'windows nt 5.0') : $sOs = 'Windows 2000';
			break;
			case strpos($sAgent,'windows nt') : $sOs = 'Windows NT';
			break; 
			case strpos($sAgent,'windows 98') : $sOs = 'Windows 98';
			break;
			case strpos($sAgent,'win 9x 4.90') : $sOs = 'Windows ME';
			break;
			case strpos($sAgent,'win me') :    $sOs = 'Windows ME';
			break;
			case strpos($sAgent,'win ce') : $sOs = 'Windows CE';
			break;
			case strpos($sAgent,'ubuntu') : $sOs = 'Ubuntu';
			break;
			case strpos($sAgent,'freebsd') : $sOs = 'Free BSD';
			break;
			case strpos($sAgent,'symbian') : $sOs = 'Symbian';
			break;
			case strpos($sAgent,'mac os x') : $sOs = 'Mac OS X';
			break;
			case strpos($sAgent,'macintosh') : $sOs = 'Macintosh';
			break;
			case strpos($sAgent,'linux') : $sOs = 'Linux';
			break;
			default: $sOs = 'Onbekend';
        }
	    return $sOs;
}

function getIp2Location($ip){
	$ipa = array();
	$ipa = explode(".",$ip);
	$ipno = ($ipa[0] * (256*256*256)) + ($ipa[1] * (256*256)) + ($ipa[2] * (256)) + $ipa[3];
	$rescountry = mysql_query("SELECT * FROM ipcountry WHERE IPFROM <= ".$ipno." AND IPTO >= ".$ipno."");
	$countrycode = mysql_result($rescountry,0,"COUNTRYSHORT");
	return $countrycode;
}
function generateReferral($length=8){
  $referral = "";
  $possible = "0123456789abcdfghjkmnpqrstvwxyzABCDFGHJKMNPQRSTVWXYZ";  
  $i = 0; 
  while ($i < $length) { 
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
    if (!strstr($referral, $char)) { 
      $referral .= $char;
      $i++;
    }
  }
  return $referral;
}
function rewriteCountry($country){
	if(strpos($country,',')){
		$parts = explode(',',$country);
		$country = $parts[1]." ".$parts[0];
	}
	return $country;
}
function yearOptions($endYear=0,$startYear=1900,$selection=1900){
	if($endYear == 0) $endYear = date('Y');
	for ( $i = $endYear; $i >= $startYear; $i-- ){
		$selected = "";
		if($selection == $i){
			$selected = "SELECTED";
		}
		echo "<option value='".$i."' $selected>".$i."</option>/n";
	}
}
function monthOptions($lang,$selection=0){
	if($lang == 1){
		$months = array( 1 => "Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre" );
	}
	if($lang == 2){
		$months = array( 1 => "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" );
	}
	
	foreach ( $months as $monthNo => $month ){
		$selected = "";
		if($selection == $monthNo){
			$selected = " SELECTED";
		}
		echo '<option value="'.$monthNo.'"'.$selected.'>'.$month.'</option>'."\n";
	}
}
function dayOptions($selection=0){
	for ( $i = 1; $i <= 31; $i++ ){
		$selected = "";
		if($selection == $i){
			$selected = " SELECTED";
		}
		echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n";
	}
}

class mailsending {
	function sendingemail_phpmailer ($var_array,$template,$phpmailer,$FromName,$From,$to,$Subject,$Bcc){
		if (!is_array($var_array)){
			echo "first variable should be an array. ITS NOT !";
			exit;
		}
	
		require_once($phpmailer);
		// I changed this to require_once because i found that when i trued to look the class for multiple emails, the phpmailer class was recelared and hence caused issue. SO MADE THIS as require_once.
	
		$mail = new PHPMailer();

		$mail->FromName = $FromName;
		$mail->From = $From;
		$mail->AddAddress($to);
		if($Bcc != ''){
			$mail->AddBCC($Bcc);
		}
		$mail->Subject = $Subject;
		$mail->IsHTML(true); 
		
		$filename = $template;
		$fd = fopen ($filename, "r");
		$mailcontent = fread ($fd, filesize ($filename));
								
		foreach ($var_array as $key=>$value){
			$mailcontent = str_replace("%%$value[0]%%", $value[1],$mailcontent );
		}
								
		$mailcontent = stripslashes($mailcontent);
		
		fclose ($fd);
		$mail->Body=$mailcontent;
		if(!$mail->Send()){
		   echo $mail->ErrorInfo;
		   exit;
		}
	}
}
?>
