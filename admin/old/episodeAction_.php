<?php session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1); 
if(isset($_POST['action']) && isset($_POST['episode'])){
	include("include/dbinfo.inc.php");
	include("include/functions.php");
	switch($_POST['action']){
		case 'insert':
			$valid = true;
			$error = "";
			$subeng = "";
			$subfre = "";
			if(!isset($_POST['episodenumber']) || strlen($_POST['episodenumber']) == 0){
				$valid = false;
				$error.= "<li>Please enter the episode number.</li>";
			}
			if(!isset($_POST['episodevideo']) || strlen($_POST['episodevideo']) == 0){
				$valid = false;
				$error.= "<li>Please select the videofile.</li>";
			}
			if(!isset($_POST['episodeprice']) || strlen($_POST['episodeprice']) == 0){
				$valid = false;
				$error.= "<li>Please enter the price.</li>";
			}
			
			$subeng = "";
			$subfre = "";
			if(isset($_FILES["episodesubeng"]) && strlen($_FILES["episodesubeng"]['name']) > 0){
				if (end(explode(".",strtolower($_FILES["episodesubeng"]["name"]))) == "srt"){				
					if (file_exists("D:/subtitles/" . $_FILES["episodesubeng"]["name"])){
						unlink("D:/subtitles/" . $_FILES["episodesubeng"]["name"]);
					}
					move_uploaded_file($_FILES["episodesubeng"]["tmp_name"],"D:/subtitles/" . $_FILES["episodesubeng"]["name"]);
					$subeng = $_FILES["episodesubeng"]["name"];
				}else{
					$valid = false;
					$error .= "Invalid english subtitle, only .srt-files are allowed.";
				}
			}
			if(isset($_FILES["episodesubfre"]) && strlen($_FILES["episodesubfre"]['name']) > 0){
				if (end(explode(".",strtolower($_FILES["episodesubfre"]["name"]))) == "srt"){				
					if (file_exists("D:/subtitles/" . $_FILES["episodesubfre"]["name"])){
						unlink("D:/subtitles/" . $_FILES["episodesubeng"]["name"]);
					}
					move_uploaded_file($_FILES["episodesubfre"]["tmp_name"],"D:/subtitles/" . $_FILES["episodesubfre"]["name"]);
					$subfre = $_FILES["episodesubfre"]["name"];
				}else{
					$valid = false;
					$error .= "Invalid french subtitle, only .srt-files are allowed.";
				}
			}
			
			if($valid){
				$drmeng = cleanup($_POST['episodedrmeng']);
				$drmfre = cleanup($_POST['episodedrmfre']);
				$keyeng = cleanup($_POST['episodekeyeng']);
				$keyfre = cleanup($_POST['episodekeyfre']);
				
				$filename = cleanup($_POST['episodevideo']);
				$filename = end(split("/",$filename));
				
				$insertEpisode = "INSERT INTO t_dp_episode (DramaID, EpisodeNumber, EpisodeVideo, EpisodeSubEng, EpisodeSubFre, EpisodePrice, LicensorID, EpisodeAdded, DownFileEng, DownFileFre, KeyIDEng, KeyIDFre) VALUES (";
				$insertEpisode .= cleanup($_POST['drama']).",'".cleanup($_POST['episodenumber'])."','".$filename."','".$subeng."','".$subfre."','".cleanup($_POST['episodeprice'])."','".cleanup($_POST['licensor'])."',now(),'".$drmeng."','".$drmfre."','".$keyeng."','".$keyfre."')";
				mysql_query($insertEpisode) or die(mysql_error());
			}
		break;
		case 'update':
			$valid = true;
			$error = "";
			if(!isset($_POST['episodenumber']) && strlen($_POST['episodenumber']) == 0){
				$valid = false;
				$error.= "<li>Please enter the episode number.</li>";
			}
			if(!isset($_POST['episodevideo']) && strlen($_POST['episodevideo']) == 0){
				$valid = false;
				$error.= "<li>Please enter the videolink.</li>";
			}
			if(!isset($_POST['episodeprice']) && strlen($_POST['episodeprice']) == 0){
				$valid = false;
				$error.= "<li>Please enter the price.</li>";
			}
			$subeng = "";
			$subfre = "";
			if(isset($_FILES["episodesubeng"]) && strlen($_FILES["episodesubeng"]['name']) > 0){
				if (end(explode(".",strtolower($_FILES["episodesubeng"]["name"]))) == "srt"){		
					if (file_exists("D:/subtitles/" . $_FILES["episodesubeng"]["name"])){
						unlink("D:/subtitles/" . $_FILES["episodesubeng"]["name"]);
					}
					move_uploaded_file($_FILES["episodesubeng"]["tmp_name"],"D:/subtitles/" . $_FILES["episodesubeng"]["name"]);
					$subeng = $_FILES["episodesubeng"]["name"];
					$subeng = ", EpisodeSubEng = '".$subeng."'";
				}else{
					$valid = false;
					$error .= "Invalid english subtitle, only .srt-files are allowed.";
				}
			}
			if(isset($_FILES["episodesubfre"]) && strlen($_FILES["episodesubfre"]['name']) > 0){
				if (end(explode(".",strtolower($_FILES["episodesubfre"]["name"]))) == "srt"){				
					if (file_exists("D:/subtitles/" . $_FILES["episodesubfre"]["name"])){
						unlink("D:/subtitles/" . $_FILES["episodesubeng"]["name"]);
					}
					move_uploaded_file($_FILES["episodesubfre"]["tmp_name"],"D:/subtitles/" . $_FILES["episodesubfre"]["name"]);
					$subfre = $_FILES["episodesubfre"]["name"];
					$subfre = ", EpisodeSubFre = '".$subfre."'";
				}else{
					$valid = false;
					$error .= "Invalid french subtitle, only .srt-files are allowed.";
				}
			}
				
			if($valid){
				$drmeng = cleanup($_POST['episodedrmeng']);
				$drmfre = cleanup($_POST['episodedrmfre']);
				$keyeng = cleanup($_POST['episodekeyeng']);
				$keyfre = cleanup($_POST['episodekeyfre']);
				
				$filename = cleanup($_POST['episodevideo']);
				$filename = end(split("/",$filename));
				$updateEpisode = "UPDATE t_dp_episode SET DramaID=".cleanup($_POST['drama']).", EpisodeNumber='".cleanup($_POST['episodenumber'])."', EpisodeVideo='".$filename."' $subeng $subfre, EpisodePrice='".cleanup($_POST['episodeprice'])."', LicensorID='".cleanup($_POST['licensor'])."', DownFileEng = '".$drmeng."', DownFileFre = '".$drmfre."', KeyIDEng = '".$keyeng."', KeyIDFre = '".$keyfre."' WHERE EpisodeID = ".cleanup($_POST['episodeid'])." LIMIT 1";
				//echo $updateEpisode;
				mysql_query($updateEpisode) or die(mysql_error());
				header("Location: dramalist.php?id=".cleanup($_POST['drama']));
				exit();
			}
		break;
		case 'delete':
			$deleteEpisode = "DELETE FROM t_dp_episode WHERE EpisodeID = ".cleanup($_POST['episodeid'])." LIMIT 1";
			//echo $deleteEpisode;
			mysql_query($deleteEpisode) or die(mysql_error());
		break;
	}
}
$_SESSION['error'] = $error;
header("Location: episodeForm.php?do=".cleanup($_POST['action'])."&id=".cleanup($_POST['drama']));
?>