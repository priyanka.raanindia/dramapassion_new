<?php
require_once("top.php");
require_once("header.php");
$content = "drama";
$resultstatus = mysql_query("SELECT * FROM t_dp_status");
$resultproducer = mysql_query("SELECT * FROM t_dp_producer");
$resultcountry = mysql_query("SELECT * FROM t_dp_countryship");
$resultgenre = mysql_query("SELECT * FROM t_dp_genrevideo");
$resultactor = mysql_query("SELECT * FROM t_dp_actor");
$status = mysql_num_rows($resultstatus);
$producers = mysql_num_rows($resultproducer);
$countries = mysql_num_rows($resultcountry);
$genres = mysql_num_rows($resultgenre);
$actors = mysql_num_rows($resultactor);
?>
<tr>
  <td>
  
  <tr>
  <td style="background:url(images/v1_slice_admin_03.jpg);" width="980" height="58" valign="top" align="left" class="pagetitle">Drama</td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_admin_04.jpg);" width="980" height="563" valign="top"><?php
switch($_REQUEST['do']){
	case 'insert':
	?>

<form method="post" ENCTYPE="multipart/form-data" action="<?php echo $content; ?>Action.php" name="insert<?php echo $content; ?>">
  <table id=dramas  align="left">
   <tr><th colspan="2"> | <a href="javascript:history.go(-1)">Go Back</a></th></tr>
    <tr>
      <td align="left">Title:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>title" /></td>
    </tr>
    <tr>
      <td align="left">Original Title:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>title2" /></td>
    </tr>
    <tr>
      <td align="left">Episodes:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>episodes" /></td>
    </tr>
    <tr>
      <td align="left">Length:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>length" /></td>
    </tr>
    <tr>
      <td align="left">Year:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>year" /></td>
    </tr>
    <tr>
      <td align="left">Genre:</td>
      <td align="left"><select name="genre[]" multiple size=5>
          <?php
        for($i=0;$i<$genres;$i++){
            echo "<option value=".mysql_result($resultgenre,$i,"GenreVideoID").">".mysql_result($resultgenre,$i,"GenreVideoDesc")."</option>";
        }
        ?>
        </select></td>
    </tr>
    <tr>
      <td align="left">Country:</td>
      <td align="left"><select name="country">
          <?php
        for($i=0;$i<$countries;$i++){
            echo "<option value=".mysql_result($resultcountry,$i,"CountryID").">".rewriteCountry(mysql_result($resultcountry,$i,"CountryNameEng"))."</option>";
        }
        ?>
        </select></td>
    </tr>
    <tr>
      <td align="left">Synopsis ENG:</td>
      <td align="left"><textarea name="<?php echo $content; ?>synopsis"></textarea></td>
    </tr>
        <tr>
      <td align="left">Synopsis FRE:</td>
      <td align="left"><textarea name="<?php echo $content; ?>synopsisfre"></textarea></td>
    </tr>
    <tr>
      <td align="left">Producer:</td>
      <td align="left"><select name="producer">
          <?php
        for($i=0;$i<$producers;$i++){
            echo "<option value=".mysql_result($resultproducer,$i,"ProducerID").">".mysql_result($resultproducer,$i,"ProducerName")."</option>";
        }
        ?>
        </select></td>
    </tr>
    <tr>
      <td align="left">Trailer:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>trailer" /></td>
    </tr>
    <tr>
      <td align="left">Sub Eng:</td>
      <td align="left"><input type="checkbox" name="<?php echo $content; ?>subeng" value="1" /></td>
    </tr>
    <tr>
      <td align="left">Sub Fre:</td>
      <td align="left"><input type="checkbox" name="<?php echo $content; ?>subfre" value="1" /></td>
    </tr>
    <tr>
      <td align="left">Pack Price:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>price" /></td>
    </tr>
    <tr>
      <td align="left">Pack Bonus:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>bonus" /></td>
    </tr>
    <tr>
      <td align="left">Status:</td>
      <td align="left"><select name="status">
          <?php
        for($i=0;$i<$status;$i++){
            echo "<option value=".mysql_result($resultstatus,$i,"StatusID").">".mysql_result($resultstatus,$i,"StatusDesc")."</option>";
        }
        ?>
        </select></td>
    </tr>
    
    <tr>
      <td align="left">Scroller Position:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>position" /></td>
    </tr>
    
    <tr>
      <td align="left">Resolution HQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>hq" /> [## kbs: ## x ##]</td>
    </tr>
    <tr>
      <td align="left">Resolution LQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>lq" /> [## kbs: ## x ##]</td>
    </tr>
    
    <tr>
      <td align="left">Display Size HQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>displayhq" /> [### x ###]</td>
    </tr>
    <tr>
      <td align="left">Display Size LQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>displaylq" /> [### x ###]</td>
    </tr>
    
    <tr>
      <td align="left"><input type="hidden" name="action" value="insert" />
        <input type="hidden" name="<?php echo $content; ?>" value="1" /></td>
      <td align="left"><input type="submit" value="Add" name="submit"/></td>
    </tr>
  </table>
</form>
<?php
	break;
	case 'update':
		$resultdrama = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID = ".$_REQUEST['id']);
		$resultdramaactors = mysql_query("SELECT ActorID FROM t_dp_dramaactorlink WHERE DramaID = ".$_REQUEST['id']);
		$resultdramagenres = mysql_query("SELECT GenreVideoID From t_dp_dramagenrelink WHERE DramaID = ".$_REQUEST['id']);
		$dramagenres = mysql_num_rows($resultdramagenres);
		$dramaactors = mysql_num_rows($resultdramaactors);
?>
<form method="post" ENCTYPE="multipart/form-data" action="<?php echo $content; ?>Action.php" name="update<?php echo $content; ?>">
  <input type="hidden" name="<?php echo $content; ?>id" value="<?php echo $_REQUEST['id']; ?>" />
  <table id=dramas>
  <tr><th colspan="2"><?php echo mysql_result($resultdrama,0,"DramaTitle");?> | <a href="javascript:history.go(-1)">Go Back</a></th></tr>
    <tr>
      <td align="left">Title:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaTitle");?>" name="<?php echo $content; ?>title" /></td>
    </tr>
    <tr>
      <td align="left">Original Title:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaTitle2");?>" name="<?php echo $content; ?>title2" /></td>
    </tr>
    <tr>
      <td align="left">Episodes:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaEpisodes");?>" name="<?php echo $content; ?>episodes" /></td>
    </tr>
    <tr>
      <td align="left">Length:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaLength");?>" name="<?php echo $content; ?>length" /></td>
    </tr>
    <tr>
      <td align="left">Year:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaYear");?>" name="<?php echo $content; ?>year" /></td>
    </tr>
    <tr>
      <td align="left">Genre:</td>
      <td align="left"><select name="genre[]" multiple size=5>
          <?php
		for($i=0;$i<$genres;$i++){
			echo "<option value=".mysql_result($resultgenre,$i,"GenreVideoID")." ";
			for($j=0;$j<$dramagenres;$j++){
				if(mysql_result($resultgenre,$i,"GenreVideoID") == mysql_result($resultdramagenres,$j,"GenreVideoID")){
					echo " SELECTED";
					$j = $dramagenres;
				}
			}
			echo ">".mysql_result($resultgenre,$i,"GenreVideoDesc")."</option>";
        }
        ?>
        </select></td>
    </tr>
    <tr>
      <td align="left">Country:</td>
      <td align="left"><select name="country">
          <?php
        for($i=0;$i<$countries;$i++){
			$selected = "";
			if(mysql_result($resultdrama,0,"CountryID") == mysql_result($resultcountry,$i,"CountryID")){
				$selected = " SELECTED";
			}
            echo "<option value=".mysql_result($resultcountry,$i,"CountryID")." $selected>".rewriteCountry(mysql_result($resultcountry,$i,"CountryNameEng"))."</option>";
        }
        ?>
        </select></td>
    </tr>
    <tr>
      <td align="left">Synopsis ENG:</td>
      <td align="left"><textarea name="<?php echo $content; ?>synopsis" rows="5" cols="40"><?php echo mysql_result($resultdrama,0,"DramaSynopsis");?></textarea></td>
    </tr>
    <tr>
      <td align="left">Synopsis FRE:</td>
      <td align="left"><textarea name="<?php echo $content; ?>synopsisfre" rows="5" cols="40"><?php echo mysql_result($resultdrama,0,"DramaSynopsisFre");?></textarea></td>
    </tr>
    <tr>
      <td align="left">Producer:</td>
      <td align="left"><select name="producer">
          <?php
        for($i=0;$i<$producers;$i++){
			$selected = "";
			if(mysql_result($resultdrama,0,"ProducerID") == mysql_result($resultproducer,$i,"ProducerID")){
				$selected = " SELECTED";
			}
            echo "<option value=".mysql_result($resultproducer,$i,"ProducerID")." $selected>".mysql_result($resultproducer,$i,"ProducerName")."</option>";
        }
        ?>
        </select></td>
    </tr>
    <tr>
      <td align="left">Trailer:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaTrailer");?>" name="<?php echo $content; ?>trailer" /></td>
    </tr>
    <tr>
      <td align="left">Sub Eng:</td>
      <td align="left"><input type="checkbox" <?php if(mysql_result($resultdrama,0,"DramaSubEng")==1) echo "checked"; ?> value="<?php echo mysql_result($resultdrama,0,"DramaSubEng");?>" name="<?php echo $content; ?>subeng" /></td>
    </tr>
    <tr>
      <td align="left">Sub Fre:</td>
      <td align="left"><input type="checkbox" <?php if(mysql_result($resultdrama,0,"DramaSubFre")==1) echo "checked"; ?> value="<?php echo mysql_result($resultdrama,0,"DramaSubFre");?>" name="<?php echo $content; ?>subfre" /></td>
    </tr>
    <tr>
      <td align="left">Pack Price:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaPackPrice");?>" name="<?php echo $content; ?>price" /></td>
    </tr>
    <tr>
      <td align="left">Pack Bonus:</td>
      <td align="left"><input type="text" value="<?php echo mysql_result($resultdrama,0,"DramaPackBonus");?>" name="<?php echo $content; ?>bonus" /></td>
    </tr>
    <tr>
      <td align="left">Status:</td>
      <td align="left"><select name="status">
          <?php
        for($i=0;$i<$status;$i++){
			$selected = "";
			if(mysql_result($resultdrama,0,"StatusID") == mysql_result($resultstatus,$i,"StatusID")){
				$selected = " SELECTED";
			}
            echo "<option value=".mysql_result($resultstatus,$i,"StatusID")." $selected>".mysql_result($resultstatus,$i,"StatusDesc")."</option>";
        }
        ?>
        </select></td>
    </tr>
    <tr>
      <td align="left">Scroller Position:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>position"  value="<?php if(mysql_result($resultdrama,0,"ScrollerPosition") >= 0) echo mysql_result($resultdrama,0,"ScrollerPosition");?>" /></td>
    </tr>
    
        <tr>
      <td align="left">Resolution HQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>hq" value="<?php echo mysql_result($resultdrama,0,"ResolutionHQ");?>" /> [## kbs: ## x ##]</td>
    </tr>
    <tr>
      <td align="left">Resolution LQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>lq" value="<?php echo mysql_result($resultdrama,0,"ResolutionLQ");?>" /> [## kbs: ## x ##]</td>
    </tr>
    
    <tr>
      <td align="left">Display Size HQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>displayhq" value="<?php echo mysql_result($resultdrama,0,"DisplaySizeHQ");?>" /> [### x ###]</td>
    </tr>
    <tr>
      <td align="left">Display Size LQ:</td>
      <td align="left"><input type="text" name="<?php echo $content; ?>displaylq" value="<?php echo mysql_result($resultdrama,0,"DisplaySizeLQ");?>" /> [### x ###]</td>
    </tr>
    
    <tr>
      <td align="left"><input type="hidden" name="action" value="update" />
        <input type="hidden" name="<?php echo $content; ?>" value="1" /></td>
      <td align="left"><input type="submit" value="Update" name="submit"/></td>
    </tr>
  </table>
</form>
<?php
	break;
	case 'delete':
		$resultdrama = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID = ".$_REQUEST['id']);
?>
<form method="post" action="<?php echo $content; ?>Action.php" name="delete<?php echo $content; ?>">
  <input type="hidden" name="<?php echo $content; ?>id" value="<?php echo $_REQUEST['id']; ?>" />
  <table id=dramas>
  <tr><th colspan="2"><?php echo mysql_result($resultdrama,0,"DramaTitle");?> | <a href="javascript:history.go(-1)">Go Back</a></th></tr>
    <tr>
      <td align="left">Title:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaTitle");?></td>
    </tr>
    <tr>
      <td align="left">Original Title:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaTitle2");?></td>
    </tr>
    <tr>
      <td align="left">Episodes:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaEpisodes");?></td>
    </tr>
    <tr>
      <td align="left">Length:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaLength");?></td>
    </tr>
    <tr>
      <td align="left">Year:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaYear");?></td>
    </tr>
    <tr>
      <td align="left">Country:</td>
      <td align="left"><?php
        for($i=0;$i<$countries;$i++){
			$selected = "";
			if(mysql_result($resultdrama,0,"CountryID") == mysql_result($resultcountry,$i,"CountryID")){
				echo rewriteCountry(mysql_result($resultcountry,$i,"CountryNameEng"));
			}
        }
        ?></td>
    </tr>
    <tr>
      <td align="left">Synopsis ENG:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaSynopsis");?></td>
    </tr>
        <tr>
      <td align="left">Synopsis FRE:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaSynopsisFre");?></td>
    </tr>
    <tr>
      <td align="left">Producer:</td>
      <td align="left"><?php
        for($i=0;$i<$producers;$i++){
			$selected = "";
			if(mysql_result($resultdrama,0,"ProducerID") == mysql_result($resultproducer,$i,"ProducerID")){
				echo mysql_result($resultproducer,$i,"ProducerName");
			}
        }
        ?></td>
    </tr>
    <tr>
      <td align="left">Trailer:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaTrailer");?></td>
    </tr>
    <tr>
      <td align="left">Sub Eng:</td>
      <td align="left"><input type="checkbox" disabled <?php if(mysql_result($resultdrama,0,"DramaSubEng")==1) echo "checked"; ?> value="<?php echo mysql_result($resultdrama,0,"DramaSubEng");?>"/></td>
    </tr>
    <tr>
      <td align="left">Sub Fre:</td>
      <td align="left"><input type="checkbox" disabled <?php if(mysql_result($resultdrama,0,"DramaSubFre")==1) echo "checked"; ?> value="<?php echo mysql_result($resultdrama,0,"DramaSubFre");?>"/></td>
    </tr>
    <tr>
      <td align="left">Pack Price:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaPackPrice");?></td>
    </tr>
    <tr>
      <td align="left">Pack Bonus:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"DramaPackBonus");?></td>
    </tr>
    <tr>
      <td align="left">Status:</td>
      <td align="left"><?php
        for($i=0;$i<$status;$i++){
			$selected = "";
			if(mysql_result($resultdrama,0,"StatusID") == mysql_result($resultstatus,$i,"StatusID")){
				echo mysql_result($resultstatus,$i,"StatusDesc");
			}
        }
        ?></td>
    </tr>
     <tr>
      <td align="left">Scroller Position:</td>
      <td align="left"><?php if(mysql_result($resultdrama,0,"ScrollerPosition") >= 0) echo mysql_result($resultdrama,0,"ScrollerPosition");?></td>
    </tr>
    
        <tr>
      <td align="left">Resolution HQ:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"ResolutionHQ");?></td>
    </tr>
    <tr>
      <td align="left">Resolution LQ:</td>
      <td align="left"><?php echo mysql_result($resultdrama,0,"ResolutionLQ");?></td>
    </tr>
    
    <tr>
      <td align="left"><input type="hidden" name="action" value="delete" />
        <input type="hidden" name="<?php echo $content; ?>" value="1" /></td>
      <td align="left"><input type="submit" value="Delete" name="submit"/></td>
    </tr>
  </table>
</form>
<?php
	break;
}
?></td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_thankyou_04.jpg);" width="980" height="10"></td>
</tr>
  
  </td>
</tr>
<?php
require_once("bottom.php");
?>