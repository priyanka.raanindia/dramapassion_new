<?php session_start();
include("include/dbinfo.inc.php");
include("include/functions.php");
/* ACTOR */
if(isset($_POST['actor']) && $_POST['actor'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['actorname']) || strlen($_POST['actorname']) == 0){
		$valid = false;
		$error.= "Please enter the actor's name.";
	}
	if(!isset($_POST['actororiginalname']) || strlen($_POST['actororiginalname']) == 0){
		$valid = false;
		$error.= "Please enter the actor's original name.";
	}
	
	if($valid)
		mysql_query("INSERT INTO t_dp_actor (ActorName,ActorOriginalName,CountryID) VALUES ('".cleanup($_POST['actorname'])."','".cleanup($_POST['actororiginalname'])."','".cleanup($_POST['actorcountry'])."')") or die(mysql_error());
		$_SESSION['error'] = $error;
		header("Location: actors.php");
		die();
}

if(isset($_POST['actor']) && $_POST['actor'] == "update"){
	$valid = true;
	$error = "";
	if(!isset($_POST['actorname']) || strlen($_POST['actorname']) == 0){
		$valid = false;
		$error.= "Please enter the actor's name.";
	}
	if(!isset($_POST['actororiginalname']) || strlen($_POST['actororiginalname']) == 0){
		$valid = false;
		$error.= "Please enter the actor's original name.";
	}
	
	if($valid)
		mysql_query("UPDATE t_dp_actor SET ActorName = '".cleanup($_POST['actorname'])."',ActorOriginalName='".$_POST['actororiginalname']."',CountryID='".cleanup($_POST['actorcountry'])." WHERE ActorID = ".cleanup($_POST['actorid'])." LIMIT 1") or die(mysql_error());
		$_SESSION['error'] = $error;
		header("Location: actors.php");
		die();
}

if(isset($_POST['actor']) && $_POST['actor'] == "delete"){
	mysql_query("DELETE FROM t_dp_actor WHERE ActorID = ".cleanup($_POST['actorid'])." LIMIT 1");
	header("Location: actors.php");
	die();
}

/* ARTIST */

if(isset($_POST['artist']) && $_POST['artist'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['artistname']) || strlen($_POST['artistname']) == 0){
		$valid = false;
		$error.= "Please enter the artist's name.";
	}
	if(!isset($_POST['artistoriginalname']) || strlen($_POST['artistoriginalname']) == 0){
		$valid = false;
		$error.= "Please enter the artist's original name.";
	}
	
	if($valid)
		mysql_query("INSERT INTO t_dp_artist (artistName,artistOriginalName,CountryID) VALUES ('".cleanup($_POST['artistname'])."','".cleanup($_POST['artistoriginalname'])."','".cleanup($_POST['artistcountry'])."')") or die(mysql_error());
		$_SESSION['error'] = $error;
		header("Location: artists.php");
		die();
}

/* PRODUCER */

if(isset($_POST['producer']) && $_POST['producer'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['producername']) || strlen($_POST['producername']) == 0){
		$valid = false;
		$error.= "Please enter the producer's name.";
	}
	if($valid)
		mysql_query("INSERT INTO t_dp_producer (ProducerName) VALUES ('".cleanup($_POST['producername'])."')") or die(mysql_error());
		$_SESSION['error'] = $error;
		header("Location: producers.php");
		die();
}

if(isset($_POST['producer']) && $_POST['producer'] == "update"){
	$valid = true;
	$error = "";
	if(!isset($_POST['producername']) || strlen($_POST['producername']) == 0){
		$valid = false;
		$error.= "Please enter the producer's name.";
	}
	if($valid)
		mysql_query("UPDATE t_dp_producer SET ProducerName='".cleanup($_POST['producername'])."' WHERE ProducerID = ".cleanup($_POST['producerid'])." LIMIT 1") or die(mysql_error());
		$_SESSION['error'] = $error;
		header("Location: producers.php");
		die();
}

if(isset($_POST['producer']) && $_POST['producer'] == "delete"){
	mysql_query("DELETE FROM t_dp_producer WHERE ProducerID = ".cleanup($_POST['producerid'])." LIMIT 1") or die(mysql_error());
	header("Location: producers.php");
	die();
}
/* LICENSOR */

if(isset($_POST['licensor']) && $_POST['licensor'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['licensorname']) || strlen($_POST['licensorname']) == 0){
		$valid = false;
		$error.= "Please enter the licensor's name.";
	}
	if($valid)
		mysql_query("INSERT INTO t_dp_licensor (LicensorName) VALUES ('".cleanup($_POST['licensorname'])."')") or die(mysql_error());
		$_SESSION['error'] = $error;
		header("Location: licensors.php");
		die();
}
?>