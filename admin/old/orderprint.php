<?php

include("include/dbinfo.inc.php");

$order = mysql_query("SELECT * FROM t_dp_order WHERE OrderID = ".$_REQUEST['id']);
$user  = mysql_query("SELECT * FROM t_dp_user WHERE UserID = ".mysql_result($order,0,"UserID"));
$dvd  = mysql_query("SELECT * FROM t_dp_dvd WHERE DvdID = ".mysql_result($order,0,"DvdID"));

$olcountry = mysql_result(mysql_query("SELECT * FROM t_dp_countryship WHERE CountryID = ".mysql_result($order,0,"Deliverycountry")),0,"CountryNameEng");
$ofcountry = mysql_result(mysql_query("SELECT * FROM t_dp_countryship WHERE CountryID = ".mysql_result($user,0,"CountryID")),0,"CountryNameEng");
$sex = mysql_result(mysql_query("SELECT Sex FROM t_dp_sex WHERE SexID = ".mysql_result($user,0,"SexID")),0,"Sex");
$shippingtype = mysql_result(mysql_query("SELECT DescEng FROM t_dp_shiptype WHERE ShipTypeID = ".mysql_result($order,0,"ShippingType")),0,"DescEng");
$product = mysql_result(mysql_query("SELECT TitleEng FROM t_dp_dvd WHERE DvdID = ".mysql_result($order,0,"DvdID")),0,"TitleEng");

?>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css" media="print">
.printbutton {
  visibility: hidden;
  display: none;
}
</style>
<style type="text/css">
BODY {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
}
.pagetitle {
	font-size: 20px;
	font-weight: bold;
	text-align: center;
}
.title {
	font-size: 13px;
	font-weight: bold
}
.tabletitle {
	font-size: 13px;
	font-weight: bold;
	line-height: 25px;
}
.indent10 {
	text-indent: 10px;
}
.size10 {
	font-size: 12px;
}
TD {
	font-size: 12px;	
}
</style>
<body>
<center>
<script>
document.write("<input type='button' " +
"onClick='window.print()' " +
"class='printbutton' " +
"value='Print This Page'/>");
</script>
  <table width="900" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td colspan="2"><hr width="100%" size="1" /></td>
    </tr>
    <tr>
      <td class="pagetitle" colspan="2">D&eacute;tails de votre commande</td>
    </tr>
    <tr>
      <td colspan="2"><hr width="100%" size="1" /></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td class="title indent10" width="200">Num&eacute;ro de commande :</td>
      <td><?php echo mysql_result($order,0,"Ordernumber"); ?></td>
    </tr>
    <tr>
      <td class="title indent10" width="200">Date de commande :</td>
      <td><?php echo mysql_result($order,0,"Datetime"); ?></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="padding-left:10px;"><table cellpadding="0" cellspacing="5" border="0">
          <tr>
            <td><table cellpadding="0" cellspacing="3" border="0" width="500">
                <tr>
                  <td class="title">Addresse de facturation : </td>
                </tr>
                <tr>
                  <td><?php echo strtoupper(mysql_result($user,0,"UserLname")); ?> <?php echo mysql_result($user,0,"UserFname"); ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($user,0,"UserAddress1"); ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($user,0,"UserAddress2"); ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($user,0,"UserZip")." ".mysql_result($user,0,"UserCity"); ?></td>
                </tr>
                <tr>
                  <td><?php echo $ofcountry; ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($user,0,"UserMobile"); ?></td>
                </tr>
              </table></td>
            <td><table cellpadding="0" cellspacing="3" border="0">
                <tr>
                  <td class="title">Addresse de livraison : </td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($order,0,"Deliveryname"); ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($order,0,"Deliveryaddress1"); ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($order,0,"Deliveryaddress2"); ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($order,0,"Deliveryzip")." ".mysql_result($order,0,"Deliverycity"); ?></td>
                </tr>
                <tr>
                  <td><?php echo $olcountry; ?></td>
                </tr>
                <tr>
                  <td><?php echo mysql_result($order,0,"Deliveryphone"); ?></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
  </table>
  <table width="900" cellpadding="0" cellspacing="0" style="border:1px #000 solid;">
    <tr>
      <td width="20"></td>
      <td width="400" class="tabletitle">PRODUIT</td>
      <td width="10">
      <td class="tabletitle">Prix unitaire</td>
      <td width="10">
      <td class="tabletitle">Quantité</td>
      <td width="10">
      <td class="tabletitle">Prix total</td>
      <td width="10">
    </tr>
    <tr>
      <td colspan="9"><hr width="100%" size="1" /></td>
    </tr>
    <tr>
      <td colspan="9">&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td><?php echo $product; ?></td>
      <td></td>
      <td><?php echo mysql_result($dvd,0,"PriceTvac"); ?> &euro;</td>
      <td></td>
      <td><?php echo mysql_result($order,0,"Quantity"); ?></td>
      <td></td>
      <td><?php echo mysql_result($order,0,"PriceTvac") * mysql_result($order,0,"Quantity"); ?> &euro;</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>Frais de livraison</td>
      <td colspan="5"></td>
      <td><?php echo mysql_result($order,0,"ShippingTvac"); ?> &euro;</td>
      <td></td>
    </tr>
    <tr>
      <td colspan="9">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="5"></td>
      <td class="title">Montant total :</td>
      <td></td>
      <td class="title"><?php echo mysql_result($order,0,"TotalTvac")+mysql_result($order,0,"ShippingTvac"); ?> &euro;</td>
    </tr>
    <tr>
      <td colspan="5"></td>
      <td>Montant HTVA :</td>
      <td></td>
      <td><?php echo mysql_result($order,0,"TotalHtva")+mysql_result($order,0,"ShippingHtva"); ?> &euro;</td>
    </tr>
    <tr>
      <td colspan="5"></td>
      <td>TVA :</td>
      <td></td>
      <td><?php echo mysql_result($order,0,"TotalTva"); ?> &euro;</td>
    </tr>
    <tr>
      <td colspan="9">&nbsp;</td>
    </tr>
  </table>
  <table width="900" cellpadding="0" cellspacing="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="title indent10">Paiement effectué sur internet.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="title indent10">Fournisseur :</td>
    </tr>
    <tr>
      <td class="indent10 size10">Vlexhan Distribution SPRL</td>
    </tr>
    <tr>
      <td class="indent10 size10">Place des Maïeurs, 2, B3</td>
    </tr>
    <tr>
      <td class="indent10 size10">1150 Bruxelles</td>
    </tr>
    <tr>
      <td class="indent10 size10">Belgique</td>
    </tr>
    <tr>
      <td class="indent10 size10">TVA: BE0810.081.939</td>
    </tr>
    <tr>
      <td class="indent10 size10">Contact: support@dramapassion.com</td>
    </tr>
  </table>
</center>
</body>
</html>