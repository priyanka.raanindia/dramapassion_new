<?php session_start();
include("include/dbinfo.inc.php");
include("include/functions.php");
if(isset($_POST['news']) && $_POST['news'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['titleeng']) || strlen($_POST['titleeng']) == 0){
		$valid = false;
		$error.= "Please enter the english news title.";
	}
	if(!isset($_POST['newseng']) || strlen($_POST['newseng']) == 0){
		$valid = false;
		$error.= "Please enter the english news.";
	}
	if(!isset($_POST['titlefre']) || strlen($_POST['titlefre']) == 0){
		$valid = false;
		$error.= "Please enter the french news title.";
	}
	if(!isset($_POST['newsfre']) || strlen($_POST['newsfre']) == 0){
		$valid = false;
		$error.= "Please enter the french news.";
	}
	
	if($valid)
		mysql_query("INSERT INTO t_dp_news (NewsTitle,NewsContent,NewsTitleFre,NewsContentFre,NewsCategoryID,NewsAdded) VALUES ('".cleanup($_POST['titleeng'])."','".cleanup($_POST['newseng'])."','".cleanup($_POST['titlefre'])."','".cleanup($_POST['newsfre'])."',".cleanup($_POST['newscategory']).",NOW())") or die(mysql_error());
		
		header("Location: news.php");
		die();
}

if(isset($_POST['news']) && $_POST['news'] == "update"){
	$valid = true;
	$error = "";
	if(!isset($_POST['titleeng']) || strlen($_POST['titleeng']) == 0){
		$valid = false;
		$error.= "Please enter the english news title.";
	}
	if(!isset($_POST['newseng']) || strlen($_POST['newseng']) == 0){
		$valid = false;
		$error.= "Please enter the english news.";
	}
	if(!isset($_POST['titlefre']) || strlen($_POST['titlefre']) == 0){
		$valid = false;
		$error.= "Please enter the french news title.";
	}
	if(!isset($_POST['newsfre']) || strlen($_POST['newsfre']) == 0){
		$valid = false;
		$error.= "Please enter the french news.";
	}
	
	if($valid)
		mysql_query("UPDATE t_dp_news SET NewsTitle='".cleanup($_POST['titleeng'])."',NewsContent='".cleanup($_POST['newseng'])."',NewsTitleFre='".cleanup($_POST['titlefre'])."',NewsContentFre='".cleanup($_POST['newsfre'])."',NewsCategoryID = ".cleanup($_POST['newscategory'])." WHERE newsID = ".cleanup($_POST['newsid'])." LIMIT 1") or die(mysql_error());
		
		header("Location: news.php");
		die();
}

if(isset($_POST['news']) && $_POST['news'] == "delete"){
	mysql_query("DELETE FROM t_dp_news WHERE newsID = ".cleanup($_POST['newsid'])." LIMIT 1") or die(mysql_error());
	header("Location: news.php");
	die();
}
?>