<?php session_start();
include("include/dbinfo.inc.php");
include("include/functions.php");
if(isset($_POST['faqcat']) && $_POST['faqcat'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['faqcatname']) || strlen($_POST['faqcatname']) == 0){
		$valid = false;
		$error.= "Please enter the category's english name.";
	}
	if(!isset($_POST['faqcatnamefre']) || strlen($_POST['faqcatnamefre']) == 0){
		$valid = false;
		$error.= "Please enter the category's french name.";
	}
	if($valid)
		mysql_query("INSERT INTO t_dp_faqcategory (FaqCategoryDesc,FaqCategoryDescFre) VALUES ('".cleanup($_POST['faqcatname'])."','".cleanup($_POST['faqcatnamefre'])."')") or die(mysql_error());
		header("Location: faqs.php");
		die();
}

if(isset($_POST['faqcat']) && $_POST['faqcat'] == "update"){
	$valid = true;
	$error = "";
	if(!isset($_POST['faqcatname']) || strlen($_POST['faqcatname']) == 0){
		$valid = false;
		$error.= "Please enter the category's english name.";
	}
	if(!isset($_POST['faqcatnamefre']) || strlen($_POST['faqcatnamefre']) == 0){
		$valid = false;
		$error.= "Please enter the category's french name.";
	}
	if($valid)
		mysql_query("UPDATE t_dp_faqcategory SET FaqCategoryDesc='".cleanup($_POST['faqcatname'])."',FaqCategoryDescFre='".cleanup($_POST['faqcatnamefre'])."' WHERE FaqCategoryID = ".cleanup($_POST['faqcatid'])." LIMIT 1") or die(mysql_error());
		header("Location: faqs.php");
		die();
}

if(isset($_POST['faqcat']) && $_POST['faqcat'] == "delete"){
	mysql_query("DELETE FROM t_dp_faqcategory WHERE FaqCategoryID = ".cleanup($_POST['faqcatid'])." LIMIT 1") or die(mysql_error());
	header("Location: faqs.php");
	die();
}
?>