<?php
require_once("top.php");
require_once("header.php");

$intArevtoday = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND DATE(TransDateTime) = CURDATE()"),0) / 1.21;
$intBrevtoday = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND DATE(TransDateTime) = CURDATE()"),0) * 0.4;
$intCrevtoday = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND DATE(TransDateTime) = CURDATE()"),0) / 1.21;
$intTrevtoday = $intArevtoday + $intBrevtoday + $intCrevtoday;
$intAcnttoday = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND DATE(TransDateTime) = CURDATE()"),0);
$intBcnttoday = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND DATE(TransDateTime) = CURDATE()"),0);
$intCcnttoday = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND TransEuro > 0 AND DATE(TransDateTime) = CURDATE()"),0);
$intTcnttoday = $intAcnttoday + $intBcnttoday + $intCcnttoday;
$dvdrevtoday = mysql_result(mysql_query("SELECT SUM(TotalTvac + ShippingTvac) FROM t_dp_order WHERE DATE(Datetime) = CURDATE()"),0) / 1.21;
$dvdcnttoday = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_order WHERE DATE(Datetime) = CURDATE()"),0);
$totrevtoday = $intTrevtoday + $dvdrevtoday;
$totcnttoday = $intTcnttoday + $dvdcnttoday;

$intArevyest = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND DATE(TransDateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0) / 1.21;
$intBrevyest = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND DATE(TransDateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0) * 0.4;
$intCrevyest = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND DATE(TransDateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0) / 1.21;
$intTrevyest = $intArevyest + $intBrevyest + $intCrevyest;
$intAcntyest = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND DATE(TransDateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0);
$intBcntyest = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND DATE(TransDateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0);
$intCcntyest = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND DATE(TransDateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0);
$intTcntyest = $intAcntyest + $intBcntyest + $intCcntyest;
$dvdrevyest = mysql_result(mysql_query("SELECT SUM(TotalTvac + ShippingTvac) FROM t_dp_order WHERE DATE(Datetime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0) / 1.21;
$dvdcntyest = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_order WHERE DATE(Datetime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0);
$totrevyest = $intTrevyest + $dvdrevyest;
$totcntyest = $intTcntyest + $dvdcntyest;

$intArevmtd = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) / 1.21;
$intBrevmtd = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) * 0.4;
$intCrevmtd = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) / 1.21;
$intTrevmtd = $intArevmtd + $intBrevmtd + $intCrevmtd;
$intAcntmtd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intBcntmtd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intCcntmtd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intTcntmtd = $intAcntmtd + $intBcntmtd + $intCcntmtd;
$dvdrevmtd = mysql_result(mysql_query("SELECT SUM(TotalTvac + ShippingTvac) FROM t_dp_order WHERE MONTH(DATE(Datetime)) = MONTH(CURDATE()) AND YEAR(DATE(DateTime)) = YEAR(CURDATE())"),0) / 1.21;
$dvdcntmtd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_order WHERE MONTH(DATE(Datetime)) = MONTH(CURDATE()) AND YEAR(DATE(DateTime)) = YEAR(CURDATE())"),0);
$totrevmtd = $intTrevmtd + $dvdrevmtd;
$totcntmtd = $intTcntmtd + $dvdcntmtd;

$intArevlm = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) / 1.21;
$intBrevlm = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method = 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) * 0.4;
$intCrevlm = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) / 1.21;
$intTrevlm = $intArevlm + $intBrevlm + $intCrevlm;
$intAcntlm = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intBcntlm = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND MONTH(DATE(TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intCcntlm = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND TransEuro > 0 AND MONTH(DATE(TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intTcntlm = $intAcntlm + $intBcntlm + $intCcntlm;
$dvdrevlm = mysql_result(mysql_query("SELECT SUM(TotalTvac + ShippingTvac) FROM t_dp_order WHERE MONTH(DATE(Datetime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(DateTime)) = YEAR(CURDATE())"),0) / 1.21;
$dvdcntlm = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_order WHERE MONTH(DATE(Datetime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(DateTime)) = YEAR(CURDATE())"),0);
$totrevlm = $intTrevlm + $dvdrevlm;
$totcntlm = $intTcntlm + $dvdcntlm;

$intArevytd = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) / 1.21;
$intBrevytd = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) * 0.4;
$intCrevytd = mysql_result(mysql_query("SELECT SUM(TransEuro) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0) / 1.21;
$intTrevytd = $intArevytd + $intBrevytd + $intCrevytd;
$intAcntytd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '2' AND Method != 'ALLOPASS' AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intBcntytd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE (TransTypeID = '2' OR TransTypeID = '6') AND Method = 'ALLOPASS' AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intCcntytd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_transaction WHERE TransTypeID = '6' AND Method != 'ALLOPASS' AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE())"),0);
$intTcntytd = $intAcntytd + $intBcntytd + $intCcntytd;
$dvdrevytd = mysql_result(mysql_query("SELECT SUM(TotalTvac + ShippingTvac) FROM t_dp_order WHERE YEAR(DATE(Datetime)) = YEAR(CURDATE())"),0) / 1.21;
$dvdcntytd = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_order WHERE YEAR(DATE(Datetime)) = YEAR(CURDATE())"),0);
$totrevytd = $intTrevytd + $dvdrevytd;
$totcntytd = $intTcntytd + $dvdcntytd;

$subsreq = mysql_query("SELECT COUNT(UserID) AS tot_rec,SubscriptionID FROM t_dp_user WHERE (LevelID = '2' OR LevelID = '4') AND SubscriptionID <> 0 AND Expiry > NOW() GROUP BY SubscriptionID ORDER BY SubscriptionID");
$num_line = mysql_num_rows($subsreq);


$subsd1w = 0;
$subsd1m = 0;
$subsd3m = 0;
$subsp1m = 0;
$subsp3m = 0;
$subsp6m = 0;
$subsdfl = 0;
$subspfl = 0;

for ($i = 0; $i < $num_line; $i++){
	$subs_id = mysql_result($subsreq,$i,"SubscriptionID");
	switch ($subs_id) {
		case 1:
			$subsd1m = mysql_result($subsreq,$i,"tot_rec");
			break;
		case 2:
			$subsd3m = mysql_result($subsreq,$i,"tot_rec");
			break;
		case 3:
			$subsp1m = mysql_result($subsreq,$i,"tot_rec");
			break;
		case 4:
			$subsp3m = mysql_result($subsreq,$i,"tot_rec");
			break;
		case 5:
			$subsdfl = mysql_result($subsreq,$i,"tot_rec");
			break;
		case 6:
			$subspfl = mysql_result($subsreq,$i,"tot_rec");
			break;
		case 9:
			$subsd1w = mysql_result($subsreq,$i,"tot_rec");
			break;
		case 10:
			$subsp6m = mysql_result($subsreq,$i,"tot_rec");
			break;
	}
}

$substot = $subsd1w + $subsd1m + $subsd3m + $subsp1m + $subsp3m + $subsp6m + $subsdfl + $subspfl;

$payercremtd = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND t_dp_transaction.TransTypeID = '2' AND MONTH(DATE(t_dp_transaction.TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(t_dp_transaction.TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));
$payercrelm = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND t_dp_transaction.TransTypeID = '2' AND MONTH(DATE(t_dp_transaction.TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));
$payercreytd = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND t_dp_transaction.TransTypeID = '2' AND YEAR(DATE(t_dp_transaction.TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));

$payersubmtd = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND t_dp_transaction.TransTypeID = '6' AND MONTH(DATE(t_dp_transaction.TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(t_dp_transaction.TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));
$payersublm = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND t_dp_transaction.TransTypeID = '6' AND MONTH(DATE(t_dp_transaction.TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));
$payersubytd = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND t_dp_transaction.TransTypeID = '6' AND YEAR(DATE(t_dp_transaction.TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));

$payerintmtd = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND (t_dp_transaction.TransTypeID = '2' OR t_dp_transaction.TransTypeID = '6') AND MONTH(DATE(t_dp_transaction.TransDateTime)) = MONTH(CURDATE()) AND YEAR(DATE(t_dp_transaction.TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));
$payerintlm = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND (t_dp_transaction.TransTypeID = '2' OR t_dp_transaction.TransTypeID = '6') AND MONTH(DATE(t_dp_transaction.TransDateTime)) = MONTH(DATE_SUB(CURDATE(),INTERVAL 1 MONTH)) AND YEAR(DATE(TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));
$payerintytd = mysql_num_rows(mysql_query("SELECT t_dp_transaction.UserID FROM t_dp_transaction INNER JOIN t_dp_user ON t_dp_transaction.UserID = t_dp_user.UserID WHERE (t_dp_user.LevelID = '2' OR t_dp_user.LevelID = '4') AND (t_dp_transaction.TransTypeID = '2' OR t_dp_transaction.TransTypeID = '6') AND YEAR(DATE(t_dp_transaction.TransDateTime)) = YEAR(CURDATE()) GROUP BY t_dp_transaction.UserID"));

$num_view_free_tod = mysql_result(mysql_query("SELECT SUM(Pays_total) FROM t_dp_freestat WHERE Date_j = CURDATE()"),0);
$num_view_free_yes = mysql_result(mysql_query("SELECT SUM(Pays_total) FROM t_dp_freestat WHERE Date_j = DATE_SUB(CURDATE(),INTERVAL 1 DAY)"),0);

$num_view_pay_tod_dl = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_substat WHERE DATE(DateTime) = CURDATE() AND Streaming = 0"),0);
$num_view_pay_tod_st = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_substat WHERE DATE(DateTime) = CURDATE() AND Streaming = 1"),0);
$num_view_pay_tod = $num_view_pay_tod_dl + $num_view_pay_tod_st;

$num_view_pay_yes_dl = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_substat WHERE DATE(DateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY) AND Streaming = 0"),0);
$num_view_pay_yes_st = mysql_result(mysql_query("SELECT COUNT(UserID) FROM t_dp_substat WHERE DATE(DateTime) = DATE_SUB(CURDATE(),INTERVAL 1 DAY) AND Streaming = 1"),0);
$num_view_pay_yes = $num_view_pay_yes_dl + $num_view_pay_yes_st;


?>
<tr>
  <td>
  
  <tr>
  <td style="background:url(images/v1_slice_admin_03.jpg);" width="980" height="58" valign="top" class="pagetitle">Dashboard
  </td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_admin_04.jpg);" width="980" height="563" valign="top">
  
 <table id=genres border="0" cellpadding="0" cellspacing="2">
  <tr>
    <th align="left" width="100">&nbsp;</th>
    <th align="center" width="78">Today</th>
    <th align="left" width="78">&nbsp;</th>
    <th align="left" width="20">&nbsp;</th>
    <th align="center" width="78">N-1</th>
    <th align="left" width="78">&nbsp;</th>
    <th align="left" width="20">&nbsp;</th>
    <th align="center" width="78">MTD</th>
    <th align="left" width="78">&nbsp;</th>
    <th align="left" width="20">&nbsp;</th>
    <th align="center" width="78">LM</th>
    <th align="left" width="78">&nbsp;</th>
    <th align="left" width="20">&nbsp;</th>
    <th align="center" width="78">YTD</th>
    <th align="left" width="78">&nbsp;</th>
    <th align="left" width="20">&nbsp;</th>
  </tr>
  <tr>
    <td align="left">&nbsp;</td>
    <td align="center"><b>#</b></td>
    <td align="center"><b>EUR</b></td>
    <td align="left">&nbsp;</td>
    <td align="center"><b>#</b></td>
    <td align="center"><b>EUR</b></td>
    <td align="left">&nbsp;</td>
    <td align="center"><b>#</b></td>
    <td align="center"><b>EUR</b></td>
    <td align="left">&nbsp;</td>
    <td align="center"><b>#</b></td>
    <td align="center"><b>EUR</b></td>
    <td align="left">&nbsp;</td>
    <td align="center"><b>#</b></td>
    <td align="center"><b>EUR</b></td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">Credit CC</td>
    <td align="right"><?php echo $intAcnttoday; ?></td>
    <td align="right"><?php echo round($intArevtoday); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intAcntyest; ?></td>
    <td align="right"><?php echo round($intArevyest); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intAcntmtd; ?></td>
    <td align="right"><?php echo round($intArevmtd); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intAcntlm; ?></td>
    <td align="right"><?php echo round($intArevlm); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intAcntytd; ?></td>
    <td align="right"><?php echo round($intArevytd); ?></td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">Credit SMS</td>
    <td align="right"><?php echo $intBcnttoday; ?></td>
    <td align="right"><?php echo round($intBrevtoday); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intBcntyest; ?></td>
    <td align="right"><?php echo round($intBrevyest); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intBcntmtd; ?></td>
    <td align="right"><?php echo round($intBrevmtd); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intBcntlm; ?></td>
    <td align="right"><?php echo round($intBrevlm); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intBcntytd; ?></td>
    <td align="right"><?php echo round($intBrevytd); ?></td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">Subscription</td>
    <td align="right"><?php echo $intCcnttoday; ?></td>
    <td align="right"><?php echo round($intCrevtoday); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intCcntyest; ?></td>
    <td align="right"><?php echo round($intCrevyest); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intCcntmtd; ?></td>
    <td align="right"><?php echo round($intCrevmtd); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intCcntlm; ?></td>
    <td align="right"><?php echo round($intCrevlm); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intCcntytd; ?></td>
    <td align="right"><?php echo round($intCrevytd); ?></td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">Total internet</td>
    <td align="right"><?php echo $intTcnttoday; ?></td>
    <td align="right"><?php echo round($intTrevtoday); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intTcntyest; ?></td>
    <td align="right"><?php echo round($intTrevyest); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intTcntmtd; ?></td>
    <td align="right"><?php echo round($intTrevmtd); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intTcntlm; ?></td>
    <td align="right"><?php echo round($intTrevlm); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $intTcntytd; ?></td>
    <td align="right"><?php echo round($intTrevytd); ?></td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" colspan="11">&nbsp;<td>
  </tr>
  <tr>
    <td align="left">DVD</td>
    <td align="right"><?php echo $dvdcnttoday; ?></td>
    <td align="right"><?php echo round($dvdrevtoday); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $dvdcntyest; ?></td>
    <td align="right"><?php echo round($dvdrevyest); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $dvdcntmtd; ?></td>
    <td align="right"><?php echo round($dvdrevmtd); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $dvdcntlm; ?></td>
    <td align="right"><?php echo round($dvdrevlm); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $dvdcntytd; ?></td>
    <td align="right"><?php echo round($dvdrevytd); ?></td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" colspan="11">&nbsp;<td>
  </tr>
  <tr>
    <td align="left">Grand total</td>
    <td align="right"><?php echo $totcnttoday; ?></td>
    <td align="right"><?php echo round($totrevtoday); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $totcntyest; ?></td>
    <td align="right"><?php echo round($totrevyest); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $totcntmtd; ?></td>
    <td align="right"><?php echo round($totrevmtd); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $totcntlm; ?></td>
    <td align="right"><?php echo round($totrevlm); ?></td>
    <td align="left">&nbsp;</td>
    <td align="right"><?php echo $totcntytd; ?></td>
    <td align="right"><?php echo round($totrevytd); ?></td>
    <td align="left">&nbsp;</td>
  </tr>
  </table>
<br><br>
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td>
  <table id=user border="0" cellpadding="0" cellspacing="2">
  <tr>
    <th align="center" width="40">&nbsp;</th>
    <th align="left" width="100">&nbsp;</th>
    <th align="center" width="80">Today</th>
    <th align="center" width="40">&nbsp;</th>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Discovery 1 week</td>
    <td align="right"><?php echo $subsd1w; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Discovery 1 month</td>
    <td align="right"><?php echo $subsd1m; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Discovery 3 month</td>
    <td align="right"><?php echo $subsd3m; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Discovery Flex</td>
    <td align="right"><?php echo $subsdfl; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Privilege 1 month</td>
    <td align="right"><?php echo $subsp1m; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Privilege 3 month</td>
    <td align="right"><?php echo $subsp3m; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Privilege 6 month</td>
    <td align="right"><?php echo $subsp6m; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Privilege Flex</td>
    <td align="right"><?php echo $subspfl; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Total</td>
    <td align="right"><?php echo $substot; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
</table>
</td>
<td>
  <table id=payer border="0" cellpadding="0" cellspacing="2">
  <tr>
    <th align="left" width="100">&nbsp;</th>
    <th align="center" width="80">MTD</th>
    <th align="center" width="80">LM</th>
    <th align="center" width="80">YTD</th>
    <th align="center">&nbsp;</th>
  </tr>
  <tr>
    <td align="left">Credits</td>
    <td align="right"><?php echo $payercremtd; ?></td>
    <td align="right"><?php echo $payercrelm; ?></td>
    <td align="right"><?php echo $payercreytd; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">Subscriptions</td>
    <td align="right"><?php echo $payersubmtd; ?></td>
    <td align="right"><?php echo $payersublm; ?></td>
    <td align="right"><?php echo $payersubytd; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">Total internet</td>
    <td align="right"><?php echo $payerintmtd; ?></td>
    <td align="right"><?php echo $payerintlm; ?></td>
    <td align="right"><?php echo $payerintytd; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  <br><br>
  <table id=user border="0" cellpadding="0" cellspacing="2">
   <tr>
    <th align="center" width="40">&nbsp;</th>
    <th align="left" width="100">FREE VIEWS</th>
    <th align="center" width="80">&nbsp;</th>
    <th align="center" width="40">&nbsp;</th>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Today</td>
    <td align="right"><?php echo $num_view_free_tod; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Yesterday</td>
    <td align="right"><?php echo $num_view_free_yes; ?></td>
    <td align="right">&nbsp;</td>
  </tr>
 </table>
   <br><br>
  <table id=user border="0" cellpadding="0" cellspacing="2">
   <tr>
    <th align="center" width="40">&nbsp;</th>
    <th align="left" width="100">PAY VIEWS</th>
    <th align="center" width="80">TOTAL</th>
    <th align="center" width="80">STREAMING</th>
    <th align="center" width="80">DOWNLOAD</th>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Today</td>
    <td align="right"><?php echo $num_view_pay_tod; ?></td>
    <td align="right"><?php echo $num_view_pay_tod_st; ?></td>
    <td align="right"><?php echo $num_view_pay_tod_dl; ?></td>
  </tr>
  <tr>
    <th align="center">&nbsp;</th>
    <td align="left">Yesterday</td>
    <td align="right"><?php echo $num_view_pay_yes; ?></td>
    <td align="right"><?php echo $num_view_pay_yes_st; ?></td>
    <td align="right"><?php echo $num_view_pay_yes_dl; ?></td>
  </tr>
 </table>
 
  </td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_thankyou_04.jpg);" width="980" height="10"></td>
</tr>
  
  </td>
</tr>
<?php
require_once("bottom.php");
?>

