<?php session_start();
include("include/dbinfo.inc.php");
include("include/functions.php");
if(isset($_POST['kdict']) && $_POST['kdict'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['kdictexp']) || strlen($_POST['kdictexp']) == 0){
		$valid = false;
		$error.= "Please enter the kdict's expression.";
	}
	
	if(!isset($_POST['kdicteng']) || strlen($_POST['kdicteng']) == 0){
		$valid = false;
		$error.= "Please enter the kdict's english definition.";
	}
	if(!isset($_POST['kdictfre']) || strlen($_POST['kdictfre']) == 0){
		$valid = false;
		$error.= "Please enter the kdict's french definition.";
	}
	if($valid)
		if(isset($_POST['kdictexpfre']) && strlen($_POST['kdictexpfre']) != 0){
			$kdictfre = cleanup($_POST['kdictexpfre']);
		}else{
			$kdictfre = cleanup($_POST['kdictexp']);
		}
		mysql_query("INSERT INTO t_dp_kdict (KdictExpression,KdictExpressionFre,Kdict,KdictFre) VALUES ('".cleanup($_POST['kdictexp'])."','".$kdictfre."','".cleanup($_POST['kdicteng'])."','".cleanup($_POST['kdictfre'])."')") or die(mysql_error());

		if(isset($_POST['dramas'])){
			$kdictid = mysql_result(mysql_query("SELECT KdictID FROM t_dp_kdict ORDER BY KdictID DESC LIMIT 1"),0,"KdictID");
			for($i=0;$i<sizeof($_POST['dramas']);$i++){
				$insertdramakdictlink = "INSERT INTO t_dp_kdictdramalink (KdictID,DramaID) VALUES ('".$kdictid."','".$_POST['dramas'][$i]."')";
				mysql_query($insertdramakdictlink) or die(mysql_error());
			}
		}
		
		header("Location: kdicts.php");
		die();
}

if(isset($_POST['kdict']) && $_POST['kdict'] == "update"){
	$valid = true;
	$error = "";
	if(!isset($_POST['kdictexp']) || strlen($_POST['kdictexp']) == 0){
		$valid = false;
		$error.= "Please enter the kdict's expression.";
	}
	if(!isset($_POST['kdicteng']) || strlen($_POST['kdicteng']) == 0){
		$valid = false;
		$error.= "Please enter the kdict's english definition.";
	}
	if(!isset($_POST['kdictfre']) || strlen($_POST['kdictfre']) == 0){
		$valid = false;
		$error.= "Please enter the kdict's french definition.";
	}
	if($valid)
		if(isset($_POST['kdictexpfre']) && strlen($_POST['kdictexpfre']) != 0){
			$kdictfre = cleanup($_POST['kdictexpfre']);
		}else{
			$kdictfre = cleanup($_POST['kdictexp']);
		}
		mysql_query("UPDATE t_dp_kdict SET kdictExpression='".cleanup($_POST['kdictexp'])."',kdictExpressionFre='".cleanup($_POST['kdictexpfre'])."',kdict='".cleanup($_POST['kdicteng'])."',kdictFre='".cleanup($_POST['kdictfre'])."' WHERE kdictID = ".cleanup($_POST['kdictid'])." LIMIT 1") or die(mysql_error());
		
		if(isset($_POST['dramas'])){
			for($i=0;$i<sizeof($_POST['dramas']);$i++){
				$insertdramakdictlink = "INSERT INTO t_dp_kdictdramalink (KdictID,DramaID) VALUES ('".cleanup($_POST['kdictid'])."','".$_POST['dramas'][$i]."')";
				mysql_query($insertdramakdictlink) or die(mysql_error());
			}
		}
		
		header("Location: kdicts.php");
		die();
}

if(isset($_POST['kdict']) && $_POST['kdict'] == "delete"){
	mysql_query("DELETE FROM t_dp_kdict WHERE kdictID = ".cleanup($_POST['kdictid'])." LIMIT 1") or die(mysql_error());
	header("Location: kdicts.php");
	die();
}
?>