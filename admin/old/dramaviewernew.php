<?php session_start(); 
require_once("top.php");
require_once("header.php");

if(isset($_SESSION['language']) && $_SESSION['language'] == "Fre"){
	$lang = 2;
}else{
	$lang = 1;
}
if(isset($_POST['media'])){
	$media = substr(cleanup($_POST['media']),1);
	$_SESSION['media'] = $media;
}else{
	$media = cleanup($_SESSION['media']);	
}
if(isset($_POST['userid'])){
 	$_SESSION['userid'] = $_POST['userid'];	
}

if(isset($_REQUEST['sublng'])){
	if($_REQUEST['sublng'] == "Fre"){
		$sub = 2;
		$subfre = true; 
	}else{
		$sub = 1;
		$subfre = false;
	}
	$updatesublang = mysql_query("UPDATE t_dp_user SET SubLangID = ".$sub." WHERE UserID = ".$_SESSION['userid']." LIMIT 1");	
}else{
	if(mysql_result(mysql_query("SELECT SubLangID FROM t_dp_user WHERE UserID = ".$_SESSION['userid']." LIMIT 1"),0,"SubLangID") == 2){
		$subfre = true;	
	}else{
		$subfre = false;
	}
}
?>
<!-- Include CSS to eliminate any default margins/padding and set the height of the html element and 
		     the body element to 100%, because Firefox, or any Gecko based browser, interprets percentage as 
			 the percentage of the height of its parent container, which has to be set explicitly.  Initially, 
			 don't display flashContent div so it won't show if JavaScript disabled.
		-->
        <style type="text/css" media="screen"> 
			html, body	{ height:100%; }
			body { margin:0; padding:0; overflow:auto; text-align:center; 
			       background-color: #000000; }   
			#flashContent { display:none; }
        </style>
		
		<!-- Enable Browser History by replacing useBrowserHistory tokens with two hyphens -->
        <!-- BEGIN Browser History required section -->
        <link rel="stylesheet" type="text/css" href="history/history.css" />
        <script type="text/javascript" src="history/history.js"></script>
        <!-- END Browser History required section -->  
		    
       
        <script type="text/javascript">
            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
            var swfVersionStr = "10.0.0";
            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
            var xiSwfUrlStr = "playerProductInstall.swf";
            var flashvars = {};
			flashvars.media = "<?php echo $media; ?>";
			flashvars.user = "<?php echo $_SESSION['userid']; ?>";
            var params = {};
            params.quality = "high";
            params.bgcolor = "#000000";
            params.allowscriptaccess = "sameDomain";
            params.allowfullscreen = "true";
			
            var attributes = {};
            attributes.id = "DramaViewer2";
            attributes.name = "DramaViewer2";
            attributes.align = "middle";
            swfobject.embedSWF(
                "DramaViewer2.swf", "flashContent", 
                "852", "480", 
                swfVersionStr, xiSwfUrlStr, 
                flashvars, params, attributes);
			<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
			swfobject.createCSS("#flashContent", "display:block;text-align:left;");
</script>

<tr>
  <td>
  
  <tr>
  <td style="background:url(images/v1_slice_admin_03.jpg);" width="980" height="58" valign="top" class="pagetitle">Dramaviewer</td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_admin_04.jpg);" width="980" height="563" valign="top" align="center">
 <table border="0" cellpadding="0" cellspacing="0">
 <tr><td align="left" width="60">Subtitles:</td><td align="left"><a href="dramaviewer.php?sublng=Eng">English</a> | <a href="dramaviewer.php?sublng=Fre">French</a></td><td align="right"><a href=<?php echo $_SERVER['HTTP_REFERER']; ?>>Go Back</a></td></tr>
 <tr><td colspan="3">&nbsp;</td></tr>
 <tr><td colspan="3"><!-- PLAYER -->
         <!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough 
			 JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
			 when JavaScript is disabled.
		-->
        <div id="flashContent">
        	<p>
	        	To view this page ensure that Adobe Flash Player version 
				10.0.0 or greater is installed. 
			</p>
			<script type="text/javascript"> 
				var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
				document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
								+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
			</script> 
        </div>
	   	
       	<noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="852" height="480" id="DramaViewer2">
                <param name="movie" value="DramaViewer2.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#000000" />
                <param name="allowScriptAccess" value="sameDomain" />
                <param name="allowFullScreen" value="true" />
                <param name="media" value="<?php echo $media; ?>"/>
                <param name="user" value="<?php echo $_SESSION['userid']; ?>"/>
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="DramaViewer2.swf" width="852" height="480">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#000000" />
                    <param name="allowScriptAccess" value="sameDomain" />
                    <param name="allowFullScreen" value="true" />
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                	<p> 
                		Either scripts and active content are not permitted to run or Adobe Flash Player version
                		10.0.0 or greater is not installed.
                	</p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
	    </noscript>	
  </td></tr>
  </table>
  </td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_thankyou_04.jpg);" width="980" height="10"></td>
</tr>
  
  </td>
</tr>
<?php
require_once("bottom.php");
?>