<?php session_start();
if(isset($_POST['action']) && isset($_POST['drama'])){
	unset($_POST['drama']);
	include("include/dbinfo.inc.php");
	include("include/functions.php");
	switch($_POST['action']){
		case 'insert':
			$valid = true;
			$error = "";
			if(!isset($_POST['dramatitle']) || strlen($_POST['dramatitle']) == 0){
				$valid = false;
				$error.= "<li>Please enter the title.</li>";
			}
			if(!isset($_POST['dramatitle2']) || strlen($_POST['dramatitle2']) == 0){
				$valid = false;
				$error.= "<li>Please enter the original title.</li>";
			}
			if(!isset($_POST['dramayear']) || strlen($_POST['dramayear']) == 0){
				$valid = false;
				$error.= "<li>Please enter the release year.</li>";
			}
			if(!isset($_POST['dramaepisodes']) || strlen($_POST['dramaepisodes']) == 0){
				$valid = false;
				$error.= "<li>Please enter the number of episodes.</li>";
			}
			if(!isset($_POST['dramalength']) || strlen($_POST['dramalength']) == 0){
				$valid = false;
				$error.= "<li>Please enter the length.</li>";
			}
			if(!isset($_POST['dramasynopsis']) || strlen($_POST['dramasynopsis']) == 0){
				$valid = false;
				$error.= "<li>Please enter the English synopsis.</li>";
			}
			if(!isset($_POST['dramasynopsisfre']) || strlen($_POST['dramasynopsisfre']) == 0){
				$valid = false;
				$error.= "<li>Please enter the French synopsis.</li>";
			}
			if(!isset($_POST['dramaprice']) || strlen($_POST['dramaprice']) == 0){
				$valid = false;
				$error.= "<li>Please enter the pack price.</li>";
			}
			if(!isset($_POST['dramabonus']) || strlen($_POST['dramabonus']) == 0){
				$valid = false;
				$error.= "<li>Please enter the pack bonus.</li>";
			}
			if(!isset($_POST['genre'])){
				$valid = false;
				$error.= "<li>Please select at least one genre.</li>";
			}
			if(!isset($_POST['dramadisplayhq']) || strlen($_POST['dramadisplayhq']) == 0){
				$valid = false;
				$error.= "<li>Please enter the HQ display size.</li>";
			}
			if(!isset($_POST['dramadisplaylq']) || strlen($_POST['dramadisplaylq']) == 0){
				$valid = false;
				$error.= "<li>Please enter the LQ display size.</li>";
			}
			
			if(!isset($_POST['dramasubeng']))
				$_POST['dramasubeng'] = 0;
			if(!isset($_POST['dramasubfre']))
				$_POST['dramasubfre'] = 0;
				
			if(!isset($_POST['dramatrailer']))
				$_POST['dramatrailer'] = "";
			if(isset($_POST['dramaposition'])){
				if(cleanup($_POST['dramaposition']) == ''){
					$position = -1;
				}else{
					$position = cleanup($_POST['dramaposition']);
				}
			}
			
			if($valid){
				$insertdrama = "INSERT INTO t_dp_drama (DramaTitle, DramaTitle2, DramaYear, DramaEpisodes, DramaLength, CountryID, ProducerID, DramaSynopsis,DramaSynopsisFre, DramaSubEng, DramaSubFre, DramaTrailer, StatusID, DramaPackPrice, DramaPackBonus, DramaAdded, ScrollerPosition, ResolutionHQ, ResolutionLQ,DisplaySizeHQ,DisplaySizeLQ) VALUES (";
				$insertdrama.= "'".cleanup($_POST['dramatitle'])."','".$_POST['dramatitle2']."','".cleanup($_POST['dramayear'])."','".cleanup($_POST['dramaepisodes'])."','".cleanup($_POST['dramalength'])."','".cleanup($_POST['country'])."','".cleanup($_POST['producer'])."','".cleanup($_POST['dramasynopsis'])."','".utf8_encode(cleanup($_POST['dramasynopsisfre']))."','".cleanup($_POST['dramasubeng'])."','".cleanup($_POST['dramasubfre'])."','".cleanup($_POST['dramatrailer'])."','".cleanup($_POST['status'])."','".cleanup($_POST['dramaprice'])."','".cleanup($_POST['dramabonus'])."',now(),'".$position."','".cleanup($_POST['dramahq'])."','".cleanup($_POST['dramalq'])."','".cleanup($_POST['dramadisplayhq'])."','".cleanup($_POST['dramadisplaylq'])."')";
				//echo $insertdrama;
				mysql_query($insertdrama) or die(mysql_error());
				$dramaid = mysql_result(mysql_query("SELECT DramaID FROM t_dp_drama ORDER BY DramaAdded DESC LIMIT 1"),0,"DramaID");
				for($i=0;$i<sizeof($_POST['genre']);$i++){
					$insertdramagenrelink = "INSERT INTO t_dp_dramagenrelink (DramaID,GenreVideoID) VALUES ('".$dramaid."','".$_POST['genre'][$i]."')";
					mysql_query($insertdramagenrelink) or die(mysql_error());
				}
			}
		break;
		case 'update':
			$valid = true;
			$error = "";
			if(!isset($_POST['dramatitle']) || strlen($_POST['dramatitle']) == 0){
				$valid = false;
				$error.= "<li>Please enter the title.</li>";
			}
			if(!isset($_POST['dramatitle2']) || strlen($_POST['dramatitle2']) == 0){
				$valid = false;
				$error.= "<li>Please enter the original title.</li>";
			}
			if(!isset($_POST['dramayear']) || strlen($_POST['dramayear']) == 0){
				$valid = false;
				$error.= "<li>Please enter the release year.</li>";
			}
			if(!isset($_POST['dramaepisodes']) || strlen($_POST['dramaepisodes']) == 0){
				$valid = false;
				$error.= "<li>Please enter the number of episodes.</li>";
			}
			if(!isset($_POST['dramalength']) || strlen($_POST['dramalength']) == 0){
				$valid = false;
				$error.= "<li>Please enter the length.</li>";
			}
			if(!isset($_POST['dramasynopsis']) || strlen($_POST['dramasynopsis']) == 0){
				$valid = false;
				$error.= "<li>Please enter the English synopsis.</li>";
			}
			if(!isset($_POST['dramasynopsisfre']) || strlen($_POST['dramasynopsisfre']) == 0){
				$valid = false;
				$error.= "<li>Please enter the French synopsis.</li>";
			}
			if(!isset($_POST['dramaprice']) || strlen($_POST['dramaprice']) == 0){
				$valid = false;
				$error.= "<li>Please enter the pack price.</li>";
			}
			if(!isset($_POST['dramabonus']) || strlen($_POST['dramabonus']) == 0){
				$valid = false;
				$error.= "<li>Please enter the pack bonus.</li>";
			}
			if(!isset($_POST['genre'])){
				$valid = false;
				$error.= "<li>Please select at least one genre.</li>";
			}
			if(!isset($_POST['dramadisplayhq']) || strlen($_POST['dramadisplayhq']) == 0){
				$valid = false;
				$error.= "<li>Please enter the HQ display size.</li>";
			}
			if(!isset($_POST['dramadisplaylq']) || strlen($_POST['dramadisplaylq']) == 0){
				$valid = false;
				$error.= "<li>Please enter the LQ display size.</li>";
			}
			
			if(!isset($_POST['dramasubeng']))
				$_POST['dramasubeng'] = 0;
			if(!isset($_POST['dramasubfre']))
				$_POST['dramasubfre'] = 0;
			$thumb = "";
			if(!isset($_POST['dramatrailer']))
				$_POST['dramatrailer'] = "";
			if(isset($_POST['dramaposition'])){
				if(cleanup($_POST['dramaposition']) == ''){
					$position = -1;
				}else{
					$position = cleanup($_POST['dramaposition']);
				}
			}
			
			if($valid){
				$updatedrama = "UPDATE t_dp_drama SET ";
				$updatedrama.= "DramaTitle='".cleanup($_POST['dramatitle'])."',DramaTitle2='".$_POST['dramatitle2']."',DramaYear='".cleanup($_POST['dramayear'])."',DramaEpisodes='".cleanup($_POST['dramaepisodes'])."',DramaLength='".cleanup($_POST['dramalength'])."',CountryID='".cleanup($_POST['country'])."',ProducerID='".cleanup($_POST['producer'])."',DramaSynopsis='".cleanup($_POST['dramasynopsis'])."',DramaSynopsisFre='".utf8_encode(cleanup($_POST['dramasynopsisfre']))."',DramaSubEng='".cleanup($_POST['dramasubeng'])."',DramaSubFre='".cleanup($_POST['dramasubfre'])."',DramaTrailer='".cleanup($_POST['dramatrailer'])."',StatusID='".cleanup($_POST['status'])."',DramaPackPrice='".cleanup($_POST['dramaprice'])."',DramaPackBonus='".cleanup($_POST['dramabonus'])."',ScrollerPosition = '".$position."', ResolutionHQ = '".cleanup($_POST['dramahq'])."', ResolutionLQ = '".cleanup($_POST['dramalq'])."', DisplaySizeHQ = '".cleanup($_POST['dramadisplayhq'])."',DisplaySizeLQ = '".cleanup($_POST['dramadisplaylq'])."' WHERE DramaID = ".cleanup($_POST['dramaid'])." LIMIT 1";
				mysql_query($updatedrama) or die($updatedrama."<br><br>".mysql_error());
				
				//TODO CHECK FOR CHANGES IN GENRE
				if(sizeof($_POST['genre'])>0){
					$deletegenrelinks = "DELETE FROM t_dp_dramagenrelink WHERE DramaID = ".cleanup($_POST['dramaid']);
					mysql_query($deletegenrelinks) or die(mysql_error());
					for($i=0;$i<sizeof($_POST['genre']);$i++){
						$insertdramagenrelink = "INSERT INTO t_dp_dramagenrelink (DramaID,GenreVideoID) VALUES ('".cleanup($_POST['dramaid'])."','".cleanup($_POST['genre'][$i])."')";
						mysql_query($insertdramagenrelink) or die(mysql_error());
					}
				}
			}
		break;
		case 'delete':
			$deleteepisodes = "DELETE FROM t_dp_episode WHERE DramaID = ".cleanup($_POST['dramaid']);
			$deletedrama 	= "DELETE FROM t_dp_drama WHERE DramaID = ".cleanup($_POST['dramaid'])." LIMIT 1";
			$deleteactorlinks = "DELETE FROM t_dp_dramaactorlink WHERE DramaID = ".cleanup($_POST['dramaid']);
			$deletegenrelinks = "DELETE FROM t_dp_dramagenrelink WHERE DramaID = ".cleanup($_POST['dramaid']);
			mysql_query($deletedrama) or die(mysql_error());
			mysql_query($deleteepisodes) or die(mysql_error());
			mysql_query($deleteactorlinks) or die(mysql_error());
			mysql_query($deletegenrelinks) or die(mysql_error());
		break;
	}
}
mysql_close();
$_SESSION['error'] = $error;
header("Location: dramalist.php");
?>