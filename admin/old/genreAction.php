<?php session_start();
include("include/dbinfo.inc.php");
include("include/functions.php");
if(isset($_POST['genre']) && $_POST['genre'] == "insert"){
	$valid = true;
	$error = "";
	if(!isset($_POST['genrename']) || strlen($_POST['genrename']) == 0){
		$valid = false;
		$error.= "Please enter the genre's name.";
	}
	if($valid)
		mysql_query("INSERT INTO t_dp_genrevideo (GenreVideoDesc) VALUES ('".cleanup($_POST['genrename'])."')") or die(mysql_error());
		header("Location: genres.php");
		die();
}

if(isset($_POST['genre']) && $_POST['genre'] == "update"){
	$valid = true;
	$error = "";
	if(!isset($_POST['genrename']) || strlen($_POST['genrename']) == 0){
		$valid = false;
		$error.= "Please enter the genre's name.";
	}
	if($valid)
		mysql_query("UPDATE t_dp_genrevideo SET GenreVideoDesc='".cleanup($_POST['genrename'])."' WHERE GenreVideoID = ".cleanup($_POST['genreid'])." LIMIT 1") or die(mysql_error());
		header("Location: genres.php");
		die();
}

if(isset($_POST['genre']) && $_POST['genre'] == "delete"){
	mysql_query("DELETE FROM t_dp_genrevideo WHERE GenreVideoID = ".cleanup($_POST['genreid'])." LIMIT 1") or die(mysql_error());
	header("Location: genres.php");
	die();
}
?>