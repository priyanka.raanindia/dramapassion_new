<?php session_start();
include("include/dbinfo.inc.php");
include("include/functions.php");

if(isset($_REQUEST['do']) && strlen($_REQUEST['do']) > 0){
	switch($_REQUEST['do']){
		case 'hide': if(is_numeric($_REQUEST['id'])) mysql_query("UPDATE t_dp_review SET Visible = 0 WHERE ReviewID = ".cleanup($_REQUEST['id'])." LIMIT 1") or die(mysql_error());
			break;	
		case 'show': if(is_numeric($_REQUEST['id'])) mysql_query("UPDATE t_dp_review SET Visible = 1 WHERE ReviewID = ".cleanup($_REQUEST['id'])." LIMIT 1") or die(mysql_error());
				break;
	}
	header("Location: reviews.php");
	exit();
}else{
	if(isset($_POST['review']) && $_POST['review'] == "update"){
		$valid = true;
		$error = "";
		if(!isset($_POST['reviewtitle']) || strlen($_POST['reviewtitle']) == 0){
			$valid = false;
			$error.= "Please enter the review title.";
		}
		if(!isset($_POST['reviewcontent']) || strlen($_POST['reviewcontent']) == 0){
			$valid = false;
			$error.= "Please enter the review.";
		}
		
		if($valid)
			mysql_query("UPDATE t_dp_review SET ReviewTitle='".cleanup($_POST['reviewtitle'])."',ReviewContent='".cleanup($_POST['reviewcontent'])."',WebLangID='".cleanup($_POST['reviewlanguage'])."' WHERE reviewID = ".cleanup($_POST['reviewid'])." LIMIT 1") or die(mysql_error());
			
			header("Location: reviews.php");
			die();
	}
	
	if(isset($_POST['review']) && $_POST['review'] == "delete"){
		mysql_query("DELETE FROM t_dp_review WHERE reviewID = ".cleanup($_POST['reviewid'])." LIMIT 1") or die(mysql_error());
		header("Location: reviews.php");
		die();
	}
}
?>