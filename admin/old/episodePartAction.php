<?php session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1); 
if(isset($_POST['action']) && isset($_POST['episodePart'])){
	include("include/dbinfo.inc.php");
	include("include/functions.php");
	
	switch($_POST['action']){
		case 'insert':
			$valid = true;
			$error = "";
			
			if(!isset($_POST['episodePartnumber']) || strlen($_POST['episodePartnumber']) == 0){
				$valid = false;
				$error.= "<li>Please enter the part number.</li>";
			}
			if(!isset($_POST['episodePartfile']) || strlen($_POST['episodePartfile']) == 0){
				$valid = false;
				$error.= "<li>Please enter the videofile.</li>";
			}
			if(!isset($_POST['episodePartfileFre']) || strlen($_POST['episodePartfileFre']) == 0){
				$valid = false;
				$error.= "<li>Please enter the French videofile.</li>";
			}
			
			if($valid){					
				$insertEpisode = "INSERT INTO t_dp_parttable (EpisodeID, PartNumber, VideoPath, VideoPathFre) VALUES (";
				$insertEpisode .= cleanup($_POST['episodeID']).",".cleanup($_POST['episodePartnumber']).",'".cleanup($_POST['episodePartfile'])."','".cleanup($_POST['episodePartfileFre'])."')";
				mysql_query($insertEpisode) or die(mysql_error());
			}
		break;
		case 'update':
			$valid = true;
			$error = "";
			
			if(!isset($_POST['episodePartnumber']) || strlen($_POST['episodePartnumber']) == 0){
				$valid = false;
				$error.= "<li>Please enter the part number.</li>";
			}
			if(!isset($_POST['episodePartfile']) || strlen($_POST['episodePartfile']) == 0){
				$valid = false;
				$error.= "<li>Please enter the videofile.</li>";
			}
			if(!isset($_POST['episodePartfileFre']) || strlen($_POST['episodePartfileFre']) == 0){
				$valid = false;
				$error.= "<li>Please enter the French videofile.</li>";
			}
				
			if($valid){
				$updateEpisode = "UPDATE t_dp_parttable SET EpisodeID=".cleanup($_POST['episodeID']).", PartNumber='".cleanup($_POST['episodePartnumber'])."', VideoPath='".cleanup($_POST['episodePartfile'])."', VideoPathFre ='".cleanup($_POST['episodePartfileFre'])."' WHERE PartID = ".cleanup($_POST['episodePartid'])." LIMIT 1";
				//echo $updateEpisode;
				mysql_query($updateEpisode) or die(mysql_error());
				header("Location: episodePart.php?id=".cleanup($_POST['episodeID']));
				exit();
			}
		break;
		case 'delete':
			$deleteEpisode = "DELETE FROM t_dp_parttable WHERE PartID = ".cleanup($_POST['episodePartid'])." LIMIT 1";
			//echo $deleteEpisode;
			mysql_query($deleteEpisode) or die(mysql_error());
		break;
	}
}
$_SESSION['error'] = $error;
header("Location: episodePart.php?id=".cleanup($_POST['episodeID']));
?>