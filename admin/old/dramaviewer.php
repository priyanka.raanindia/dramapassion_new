<?php session_start(); 
require_once("top.php");
require_once("header.php");

if(isset($_SESSION['language']) && $_SESSION['language'] == "Fre"){
	$lang = 2;
}else{
	$lang = 1;
}
if(isset($_POST['media'])){
	$media = cleanup($_POST['media']);
	$_SESSION['media'] = $media;
}else{
	$media = cleanup($_SESSION['media']);	
}
if(isset($_POST['userid'])){
 	$_SESSION['userid'] = $_POST['userid'];	
}

if(isset($_REQUEST['sublng'])){
	if($_REQUEST['sublng'] == "Fre"){
		$sub = 2;
		$subfre = true; 
	}else{
		$sub = 1;
		$subfre = false;
	}
	$updatesublang = mysql_query("UPDATE t_dp_user SET SubLangID = ".$sub." WHERE UserID = ".$_SESSION['userid']." LIMIT 1");	
}else{
	if(mysql_result(mysql_query("SELECT SubLangID FROM t_dp_user WHERE UserID = ".$_SESSION['userid']." LIMIT 1"),0,"SubLangID") == 2){
		$subfre = true;	
	}else{
		$subfre = false;
	}
}

$mediastring = $_SESSION['userid'].$media;
?>
<script language="JavaScript" type="text/javascript">
<!--
// -----------------------------------------------------------------------------
// Globals
// Major version of Flash required
var requiredMajorVersion = 9;
// Minor version of Flash required
var requiredMinorVersion = 0;
// Minor version of Flash required
var requiredRevision = 124;
// -----------------------------------------------------------------------------
// -->
</script>

<tr>
  <td>
  
  <tr>
  <td style="background:url(images/v1_slice_admin_03.jpg);" width="980" height="58" valign="top" class="pagetitle">Dramaviewer</td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_admin_04.jpg);" width="980" height="563" valign="top" align="center">
 <table border="0" cellpadding="0" cellspacing="0">
 <tr><td align="left" width="60">Subtitles:</td><td align="left"><a href="dramaviewer.php?sublng=Eng">English</a> | <a href="dramaviewer.php?sublng=Fre">French</a></td><td align="right"><a href=<?php echo $_SERVER['HTTP_REFERER']; ?>>Go Back</a></td></tr>
 <tr><td colspan="3">&nbsp;</td></tr>
 <tr><td colspan="3">
 <script language="JavaScript" type="text/javascript">
<!--
// Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
var hasProductInstall = DetectFlashVer(6, 0, 65);

// Version check based upon the values defined in globals
var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

if ( hasProductInstall && !hasRequestedVersion ) {
	// DO NOT MODIFY THE FOLLOWING FOUR LINES
	// Location visited after installation is complete if installation is required
	var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
	var MMredirectURL = window.location;
    document.title = document.title.slice(0, 47) + " - Flash Player Installation";
    var MMdoctitle = document.title;

	AC_FL_RunContent(
		"src", "playerProductInstall",
		"FlashVars", "MMredirectURL="+MMredirectURL+'&MMplayerType='+MMPlayerType+'&MMdoctitle='+MMdoctitle+'&media=<?php echo $mediastring; ?>'+"",
		"width", "852",
		"height", "480",
		"align", "middle",
		"id", "DramaViewerAdmin",
		"quality", "high",
		"bgcolor", "#444444",
		"name", "DramaViewerAdmin",
		"allowScriptAccess","sameDomain",
		"allowFullScreen","true",
		"type", "application/x-shockwave-flash",
		"pluginspage", "http://www.adobe.com/go/getflashplayer"
	);
} else if (hasRequestedVersion) {
	// if we've detected an acceptable version
	// embed the Flash Content SWF when all tests are passed
	AC_FL_RunContent(
			"src", "DramaViewerAdmin",
			"FlashVars", "MMredirectURL="+MMredirectURL+'&MMplayerType='+MMPlayerType+'&MMdoctitle='+MMdoctitle+'&media=<?php echo $mediastring; ?>'+"",
			"width", "852",
			"height", "480",
			"align", "middle",
			"id", "DramaViewerAdmin",
			"quality", "high",
			"bgcolor", "#444444",
			"name", "DramaViewerAdmin",
			"allowScriptAccess","sameDomain",
			"allowFullScreen","true",
			"type", "application/x-shockwave-flash",
			"pluginspage", "http://www.adobe.com/go/getflashplayer"
	);
  } else {  // flash is too old or we can't detect the plugin
    var alternateContent = 'Alternate HTML content should be placed here. '
  	+ 'This content requires the Adobe Flash Player. '
   	+ '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
    document.write(alternateContent);  // insert non-flash content
  }
// -->
</script>
          <noscript>
          <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
			id="DramaViewerAdmin" width="850" height="480"
			codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
            <param name="movie" value="DramaViewerAdmin.swf">
            <param name="quality" value="high">
            <param name="bgcolor" value="#444444">
            <param name="flashvars" value="media=<?php echo $mediastring; ?>">
            <param name="allowScriptAccess" value="sameDomain">
            <param name="allowFullScreen" value="true">
            <embed src="DramaViewerAdmin.swf"
            	quality="high"
                bgcolor="#444444"
                width="852"
                height="480"
                name="DramaViewerAdmin"
                align="middle"
                play="true"
				loop="false"
                quality="high"
                allowScriptAccess="sameDomain"
                type="application/x-shockwave-flash"
                pluginspage="http://www.adobe.com/go/getflashplayer">
            </embed>
          </object>
          </noscript>
  </td></tr>
  </table>
  </td>
</tr>
<tr>
  <td style="background:url(images/v1_slice_thankyou_04.jpg);" width="980" height="10"></td>
</tr>
  
  </td>
</tr>
<?php
require_once("bottom.php");
?>