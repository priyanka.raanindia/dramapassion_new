<?php

$np01_wowza = simplexml_load_file('http://np01.dramapassion.com:8086/serverinfo');
$np02_wowza = simplexml_load_file('http://np02.dramapassion.com:8086/serverinfo');
$np01_w_all = $np01_wowza->VHost->ConnectionsCurrent;
//$np01_w_flash = $np01_wowza->VHost->Application->ApplicationInstance->SanJoseConnectionCount;
//$np01_w_hls = $np01_wowza->VHost->Application->ApplicationInstance->CupertinoConnectionCount;
//$np01_w_rtmp = $np01_wowza->VHost->Application->ApplicationInstance->RTMPConnectionCount;
$np02_w_all = $np02_wowza->VHost->ConnectionsCurrent;
//$np02_w_flash = $np02_wowza->VHost->Application->ApplicationInstance->SanJoseConnectionCount;
//$np02_w_hls = $np02_wowza->VHost->Application->ApplicationInstance->CupertinoConnectionCount;
//$np02_w_rtmp = $np02_wowza->VHost->Application->ApplicationInstance->RTMPConnectionCount;
$tot_w_all = $np01_w_all + $np02_w_all;
//$tot_w_flash = $np01_w_flash + $np02_w_flash;
//$tot_w_hls = $np01_w_hls + $np02_w_hls;
//$tot_w_rtmp = $np01_w_rtmp + $np02_w_rtmp;

$np01_data = file_get_contents('http://np01.dramapassion.com:8080/connstat?auto');
preg_match('/.*BusyWorkers: ([0-9]+).*/', $np01_data, $np01_output);
$np01_conn = (int)$np01_output[1];
$np01_out = file_get_contents('http://np01.dramapassion.com:8080/speedout.php');

$np02_data = file_get_contents('http://np02.dramapassion.com:8080/connstat?auto');
preg_match('/.*BusyWorkers: ([0-9]+).*/', $np02_data, $np02_output);
$np02_conn = (int)$np02_output[1];
$np02_out = file_get_contents('http://np02.dramapassion.com:8080/speedout.php');

$tot_pay_conn = $np01_conn + $np02_conn;
$tot_pay_out = $np01_out + $np02_out;

$nf01_data = file_get_contents('http://nf01.dramapassion.com/connstat?auto');
preg_match('/.*BusyWorkers: ([0-9]+).*/', $nf01_data, $nf01_output);
$nf01_conn = (int)$nf01_output[1];
$nf01_out = file_get_contents('http://nf01.dramapassion.com/speedout.php');

$nf02_data = file_get_contents('http://nf02.dramapassion.com/connstat?auto');
preg_match('/.*BusyWorkers: ([0-9]+).*/', $nf02_data, $nf02_output);
$nf02_conn = (int)$nf02_output[1];
$nf02_out = file_get_contents('http://nf02.dramapassion.com/speedout.php');

$tot_free_conn = $nf01_conn + $nf02_conn;
$tot_free_out = $nf01_out + $nf02_out;

?>
<HTML>
<head>
</head>
<body>
<table width="1000">
	<tr>
		<td width="300">PAY STREAMING</td>
		<td width="30">&nbsp;</td>
		<td width="300">PAY DOWNLOAD</td>
		<td width="30">&nbsp;</td>
		<td width="300">FREE STREAMING</td>	
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td width="75">&nbsp;</td>
					<td width="75">np01</td>
					<td width="75">np02</td>
					<td width="75">Total</td>
				</tr>
				<tr>
					<td>Total</td>
					<td><?php echo $np01_w_all; ?></td>
					<td><?php echo $np02_w_all; ?></td>
					<td><?php echo $tot_w_all; ?></td>
				</tr>
			</table>
		</td>
		<td>&nbsp;</td>
		<td>
			<table>
				<tr>
					<td width="75">&nbsp;</td>
					<td width="75">np01</td>
					<td width="75">np02</td>
					<td width="75">Total</td>
				</tr>
				<tr>
					<td>Conn.</td>
					<td><?php echo $np01_conn; ?></td>
					<td><?php echo $np02_conn; ?></td>
					<td><?php echo $tot_pay_conn; ?></td>
				</tr>
				<tr>
					<td>B/W</td>
					<td><?php echo $np01_out; ?></td>
					<td><?php echo $np02_out; ?></td>
					<td><?php echo $tot_pay_out; ?></td>
				</tr>				
			</table>
		</td>
		<td>&nbsp;</td>
		<td>
			<table>
				<tr>
					<td width="75">&nbsp;</td>
					<td width="75">nf01</td>
					<td width="75">nf02</td>
					<td width="75">Total</td>
				</tr>
				<tr>
					<td>Conn.</td>
					<td><?php echo $nf01_conn; ?></td>
					<td><?php echo $nf02_conn; ?></td>
					<td><?php echo $tot_free_conn; ?></td>
				</tr>
				<tr>
					<td>B/W</td>
					<td><?php echo $nf01_out; ?></td>
					<td><?php echo $nf02_out; ?></td>
					<td><?php echo $tot_free_out; ?></td>
				</tr>				
			</table>
		</td>
	</tr>
</table>
</body>
</HTML>