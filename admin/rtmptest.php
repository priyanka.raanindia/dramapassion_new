<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.4,minimum-scale=0.5">

<!-- css -->
<link rel="StyleSheet" href="http://www.dramapassion.com/css/my_style.css?17" type="text/css" media="screen" />
<link rel="StyleSheet" href="http://www.dramapassion.com/css/style_page_free.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="http://www.dramapassion.com/css/style_page_bottom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://www.dramapassion.com/fancybox/jquery.fancybox-1.3.4.css?1382621357" type="text/css" media="screen" />

<link rel="stylesheet" href="http://www.dramapassion.com/css/example/example.css" type="text/css">
<link rel="stylesheet" href="http://www.dramapassion.com/css/dropkick.css" type="text/css">
<link rel="stylesheet" href="http://www.dramapassion.com/css/swipebox.css">


<link href='http://fonts.googleapis.com/css?family=Carter+One&v1' rel='stylesheet' type='text/css'>
  
  
  <style type="text/css">
    .dk_theme_black {
	  background-image: url('http://www.dramapassion.com/images/fond_menu_player.png');
	  background-repeat: repeat-x;
  
  );
}
  .dk_theme_black .dk_toggle,
  .dk_theme_black.dk_open .dk_toggle {
    background-color: transparent;
    color: #fff;
    text-shadow: none;
    text-align: right;
  }
  .dk_theme_black .dk_options a {
    background-color: #333;
    color: #fff;
    text-shadow: none;
  }
    .dk_theme_black .dk_options a:hover,
    .dk_theme_black .dk_option_current a {
      background-color: #969695;
      color: #fff;
      text-shadow: #604A42 0 1px 0;
    }
  </style>

<link rel="shortcut icon" type="image/png" href="http://www.dramapassion.com/logo.png" /> 
<link rel="icon" type="image/png" href="http://www.dramapassion.com/logo.png" /> 

<!-- meta -->
<title>Level 7 Civil Servant -  en VOSTFR - Episode 5</title>
<meta name="description" lang="fr" content="Tous les épisodes en VOSTFR gratuits - Level 7 Civil Servant - Rien ne prédestinait Kim Kyung-Ja et Han Pil-Hoon à se rencontrer : il est de bonne famille, elle collectionne les petits boulots pour joindre les deux bouts. Mais quand ils sont tous les deux embau...">
<meta name="keywords" lang="fr" content="Level 7 Civil Servant, 7th Grade Civil Servant, 7th Level Civil Servant, My Girlfriend is an Agent, drama coréen, korean drama, k drama, VOSTFR, gratuit, streaming, épisode, télécharger">
<meta name="google-site-verification" content="SbdBgJew61e-YYEHq3Mb5FNdsqAOOO5eY3EmTTutQr0" />
<meta name="author" content="Vlexhan">
<meta name="Publisher" content="Weaby.Be by G1 sprl" />
<meta name="date-creation-ddmmyyyy" content="21022012" />
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="3 days" />
<meta http-equiv="content-language" content="fr-fr" />

<meta property="fb:admins" content="100000544554843" />

<meta property="og:url" content="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant" />
<meta property="og:title" content="Level 7 Civil Servant -  Episode 05"/>
<meta property="og:type" content="tv_show"/>
<meta property="og:image" content="http://www.dramapassion.com/content/dramas/Level%207%20Civil%20Servant_Detail.jpg"/>
<meta property="og:site_name" content="DramaPassion"/>
<meta property="og:description" content="Rien ne prédestinait Kim Kyung-Ja et Han Pil-Hoon à se rencontrer : il est de bonne famille, elle collectionne les petits boulots pour joindre les deux bouts. Mais quand ils sont tous les deux embau..."/>

<!-- script java -->
<script src="http://www.dramapassion.com//js/scrollbar.js" type="text/javascript"></script>
<script src="http://www.dramapassion.com//js/AC_OETags.js" type="text/javascript"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery-ui.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/functions.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="http://www.dramapassion.com/js/flowplayer-3.2.12.min.js"></script>	
<script type="text/javascript" src="http://www.dramapassion.com/js/flash_detect_min.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/swfobject.js"></script>
<script src="http://www.dramapassion.com/js/jquery.dropkick-1.0.0.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
$(function () {

	$('.custom_dropbox').dropkick({
        theme : 'black',
        change: function (value, label) {
          var url_ajax = "http://www.dramapassion.com/change_player.php?player="+value ;
          
          $.ajax({
	          url: url_ajax
	      }).done(function() {
		      location.reload();
		//window.location = url_location ;
		});
          
        }
    });
        

});
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12509314-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
googletag.cmd.push(function() {
googletag.defineSlot('/20746576/home_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-0').addService(googletag.pubads());
googletag.defineSlot('/20746576/small_rectangle_2', [250, 250], 'div-gpt-ad-1346334604979-1').addService(googletag.pubads());
googletag.defineSlot('/20746576/small_rectangle_all_pages', [250, 250], 'div-gpt-ad-1346334604979-2').addService(googletag.pubads());
googletag.defineSlot('/20746576/video_viewer_right_rectangle', [300, 250], 'div-gpt-ad-1346334604979-3').addService(googletag.pubads());
googletag.defineSlot('/20746576/video_viewer_top_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-4').addService(googletag.pubads());
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script>
<link type="text/css" rel="stylesheet" href="http://www.dramapassion.com/css/jquery.dropdown.css" />
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery.dropdown.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="conteneur_all_page">
<div class="contenant_all_page">
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '344619609607', // App ID
      channelUrl : 'http://www.dramapassion.com', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/fr_FR/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "15796249" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=15796249&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- ImageReady Slices (Sans titre-1) -->


<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	
        <td valign="top" height="93" style="background-image:url(http://www.dramapassion.com/images/header.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER GRIS-->
                <div style="width:960px;margin:auto;height:93px;overflow:hidden;" >
            <table id="Tableau_01" width="960" height="93" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="368" height="93"><a href="http://www.dramapassion.com/" ><img alt="logo DramaPassion" title="Aller à la page d'accueil" src="http://www.dramapassion.com/images/logo.png" width="368" height="93"  border="0"></a></td>
                    <td width="124" height="93"><a href="http://www.dramapassion.com/catalogue/" ><img alt="catalogue des séries" title="Voir tout le catalogue" src="http://www.dramapassion.com/images/video.png" width="124" height="93" alt="Vidéo on Dramapassion" title="video" border="0" onMouseOver="this.src='http://www.dramapassion.com/images/video_up.png'" onMouseOut="this.src='http://www.dramapassion.com/images/video.png'"></a></td>
                    <td width="141" height="93"><a href="http://www.dramapassion.com/premium/" ><img alt="premium" title="Voir les abonnements" src="http://www.dramapassion.com/images/premium.png" width="141" height="93" alt="Premium account" title="premium" border="0" onMouseOver="this.src='http://www.dramapassion.com/images/premium_up.png'" onMouseOut="this.src='http://www.dramapassion.com/images/premium.png'"></a></td>
                    <td width="90" height="93"><a href="http://www.dramapassion.com/dvd/" ><img alt="DVD" title="Voir les coffrets DVD" src="http://www.dramapassion.com/images/dvd.png" width="90" height="93" alt="DVD" title="dvd" border="0" onMouseOver="this.src='http://www.dramapassion.com/images/dvd_up.png'" onMouseOut="this.src='http://www.dramapassion.com/images/dvd.png'"></a></td>
                    <td width="237" height="93" valign="middle" align="right">
                    	<form action="http://www.dramapassion.com/catalogue/" method="POST">
                        <table height="93" cellpadding="0" cellspacing="0" width="180">
                        <!-- MOTEUR DE RECHERCHE -->
							
                        	<tr>
						
							<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="form_recherche2 recherche_gris" value="Chercher un titre" /></td>							
                                <td><input src="http://www.dramapassion.com/images/submit_search.png" type="image" /></td>
							
							</tr>
							
		                                     
                        <!-- FIN MOTEUR DE RECHERCHE -->                              
						</table>  
                        </form>                                                          
                    </td>
                </tr>
            </table>
		</div>
                <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
    	    	<tr>
        <td valign="top" height="37" style="background-image:url(http://www.dramapassion.com/images/header2.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:960px;margin:auto;">
            <table id="Tableau_01" width="960" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				                    					<td class="blanc" width="130">
					<div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div></td>
                    					<td class="blanc" width="122"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
										<td class="blanc" width="763" align="right"><a href="#" data-dropdown="#dropdown-1" id="dropdown_menu" title="Gérer mon compte" class="lien_blanc" style="text-align: right">Oya [Privilège]&nbsp;&nbsp;</a>
					<div id="dropdown-1" class="dropdown dropdown-tip">
					    <ul class="dropdown-menu">
					    	<li><a href="http://www.dramapassion.com/compte/abonnement/">Mon Abonnement</a></li>
					        <li><a href="http://www.dramapassion.com/compte/profil/">Mon Profil</a></li>
					        <li><a href="http://www.dramapassion.com/compte/paiements/">Mes Paiements</a></li>
					    </ul>
					</div>
					<td>
					<td class="blanc" width="5"><img src="http://www.dramapassion.com/images/barre.png" align="absmiddle"></td>
					<td class="blanc bloc_center" width="70"><a href="http://www.dramapassion.com/compte/playlist/" title="Voir ma playlist" class="lien_blanc">Ma playlist</a><td>
					<td class="blanc" width="5"><img src="http://www.dramapassion.com/images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="70" align="right"><a href="http://www.dramapassion.com/logoutAction.php" title="Me déconnecter" class="lien_blanc" style="text-align: right">Déconnexion</a><td>
					
					
				                                      
                                       
                </tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>
    	
	<tr>
        <td valign="top" height="545" style="background-image:url(http://www.dramapassion.com/images/player_bg.png); background-repeat:repeat-x;">  
		<script type="text/javascript"> 
	/*
    if(FlashDetect.versionAtLeast(10, 1)){
		
        
    }else{
        (function($){

                $(function(){
						
                        $(".player_flash_alert").click();
						$(".player_afficher").css({ display: "none"})

                });

        })(jQuery);
    }   
    */
   </script> 
  
<script type="text/javascript" src="http://www.dramapassion.com/js/flowplayer-3.2.11.min.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/functions.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/swfobject.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/ParsedQueryString.js"></script>
<script>
$(function () {
$('.custom_dropbox').change(function() {
	var player = $('.custom_select option:selected').val();
	var url_ajax = "http://www.dramapassion.com/change_player.php?player="+player ;
  	//var url_location = "http://www.dramapassion.com/player_HD2.php?player_type="+player+"&dramaID=122&epiNB=05&type=&user=86";
	$.ajax({
		url: url_ajax
	}).done(function() {
		location.reload();
		//window.location = url_location ;
	});
	
});
});

</script>	<style type="text/css">
        <!--
			.white {
				color: white;
			}
			
			.white:link{text-decoration:none;color: white;}
			.white:visited{text-decoration:none;color: white;}
			.white:hover{text-decoration:none;color: white;}
			.white:active{text-decoration:none;color: white;}
        	.normal {
				color: #969696;
			}
			.normal:link {text-decoration:none;color: #adadad;}
			.normal:visited {text-decoration:none;color: #adadad;}
			.normal:hover {text-decoration:none;color:white;}
			.normal:active {text-decoration:none;color:white;}			
			.waiting {
				color: white;
				text-decoration: blink;
			}					
			.waiting:link {text-decoration:none;color: white;text-decoration: blink;}
			.waiting:visited {text-decoration:none;color: white;text-decoration: blink;}
			.waiting:hover {text-decoration:none;color: white;text-decoration: blink;}
			.waiting:active {text-decoration:none;color: white;text-decoration: blink;}
			.hold {
				color: #454545;
				cursor:  default;
			}
			.hold:link {text-decoration:none;color: #454545;cursor:  default;}	
			.hold:visited {text-decoration:none;color: #454545;cursor:  default;}	
			.hold:hover {text-decoration:none;color: #454545;cursor:  default;}	
			.hold:active {text-decoration:none;color: #454545;cursor:  default;}			
			.playing {
				color: #E1D275;
			}
			.playing{
				cursor: default;
			}
			.playing:link {text-decoration:none;color: #E1D275;}
			.playing:visited {text-decoration:none;color: #E1D275;}
			.playing:hover {text-decoration:none;color: #E1D275;}
			.playing:active {text-decoration:none;color: #E1D275;}		
			
			.bt_on:link {text-decoration:none;color: #E1D275;}
			.bt_on:visited {text-decoration:none;color: #E1D275;}
			.bt_on:hover {text-decoration:none;color:white;}
			.bt_on:active {text-decoration:none;color: #E1D275;}	
			
			.bt_off:link {text-decoration:none;color: #464646;}
			.bt_off:visited {text-decoration:none;color: #464646;}
			.bt_off:hover {text-decoration:none;color: #E1D275;}
			.bt_off:active {text-decoration:none;color: #464646;}	
        -->
</style>


<div style="width:1000px;margin:auto;border:none;">
		
		<div style="margin-bottom:6px;margin-left:74px;margin-top:15px;border:none;">
			<span class="blanc" style="text-transform:uppercase;margin-left:1px;margin-top:15px;font-size:medium;">Level 7 Civil Servant :</span> <span class="rose" style="text-transform:uppercase;font-size:medium;">Episode 5</span>
		</div>
		<div style="width:852;height:480px;margin-left:74px;display:block;">
			<div id="free_video">
				<div style="display:block;width:852px;height:480px;cursor:default;" id="free">
				</div>
			</div>
		</div>
		<div style="width:852px;margin-left:74px;">
		<div id="dssc" style="display:block;margin-top:10px;margin-left:20px;width:980px;height:5px;font-size:12px;">
			<div style="float:left;" class="white"><a target="_blank" href="http://www.dramapassion.com/guide_lecteur" title="Plus d'info" class="white" >Qualité (info) :</a></div>
			<div id="dssc-items" style="float:left;">
				<a href="javascript:void();" class="bt_on" onclick="Switch_auto()" id="switch_auto" ><div id="switch_txt" style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (ON)</div></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_364" onclick="">224p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_564" onclick="">288p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_896" onclick="">360p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_1196" onclick="">432p</a>&nbsp;&nbsp;&nbsp;&nbsp;
				
								<a href="javascript:void();" class="hold" id="qal_1728" onclick="">540p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="hold" id="qal_2728" onclick="">720p</a>
							</div>
		</div>	
		<div style="float:right;margin-top:-10px;font-size:12px;width:300px;" class="white">
			<div style="float:right;">
				<select class="custom_dropbox" title="Choix du player">
					<option value="1" >Flash 1</option>
					<option value="2" selected>Flash 2</option>
					</select>
			</div>
			<div style="float:right;margin-right:5px;margin-top:3px;"><a target="_blank" href="http://www.dramapassion.com/guide_lecteur" title="Plus d'info" class="white" >Choix du lecteur : </a></div>
			</div>
			<div style="clear:booth;">
			</div>	
		</div>
				

<script>
switch_var = 0;
scount = 0;
pl = flowplayer("free", "http://www.dramapassion.com/swf/flowplayer.commercial-3.2.15.swf", {
	key: '#$8d427e6f20cb64d82ba',
	clip: {
	   url: 'http://np02.dramapassion.com/vod/_definst_/smil:lscs/lscs05-hd.smil/medialist.smil',
       provider: 'rtmp',
       urlResolvers: [ 'smil', 'bwcheck' ],
       autoPlay: false,
       scaling: 'fit',
       autoBuffering: true,
       onStart: function(clip){
       		var id_bw = '#qal_'+$f('free').getPlugin("bwcheck").getBitrate();
               $(id_bw).attr('class','playing');
       }
    },
    canvas: {
	    backgroundColor: "#000000",
	    					
	    backgroundGradient: "none"
	},
    plugins: {
        // RTMP streaming plugin
        rtmp: {
            url: "http://www.dramapassion.com/swf/flowplayer.rtmp-3.2.11.swf",
            netConnectionUrl: 'rtmp://np02.dramapassion.com/vod/_definst_/'
        },
        bwcheck: {
            url: "http://www.dramapassion.com/swf/flowplayer.bwcheck-3.2.11.swf",
 
            // CloudFront uses Adobe FMS servers
            serverType: 'wowza',
            //menu: true,
            // we use dynamic switching, the appropriate bitrate is switched on the fly
            dynamic: true,
            dynamicBuffer: true,
            checkOnStart: true,
            rememberBitrate: true,	
            qos:{bwUp: true, bwDown: true, frames: false, buffer: true, screen: false, bitrateSafety: 1.2},
 
            netConnectionUrl: 'rtmp://np02.dramapassion.com/bwcheck',
            onStreamSwitchBegin: function (newItem, currentItem) {
            	
               //alert(newItem.bitrate);
            },
            onStreamSwitch: function (newItem) {
               if(switch_var == 0){
	               $('.playing').attr('class','hold');
	               $('.waiting').attr('class','hold');
	               
               }else if(switch_var == 1){
	               $('.playing').attr('class','normal');
	               $('.waiting').attr('class','normal');
               }
               var id_bw = '#qal_'+newItem.bitrate;
               $(id_bw).attr('class','playing');
               if(scount == 10){
               		var ctime = $f('free').getTime();               
               		ctime = ctime + 0.3;
               		$f('free').seek(ctime);
               		scount = 0;
               	}else{
               		scount = scount + 1;
               	}	
            } 
        },
       
         smil: { 
         	url: "http://www.dramapassion.com/swf/flowplayer.smil-3.2.8.swf" 
         }
     }
});
function Switch_auto(){
	var bitrate_cour = '#'+$('.playing').attr('id');
	if(switch_var == 0){
		//var bitrate_cour = '#qal_'+$f('free').getPlugin("bwcheck").getBitrate();
		
		
		$f('free').getPlugin("bwcheck").enableDynamic(0);
		$('#switch_auto').attr('class', 'bt_off');
		$('#switch_txt').html('&nbsp;&nbsp;&nbsp;&nbsp;Auto (OFF)');
		$('#qal_364').attr('class','normal');
		$('#qal_564').attr('class','normal');
		$('#qal_896').attr('class','normal');
		$('#qal_1196').attr('class','normal');
		$('#qal_1728').attr('class','normal');
		$('#qal_2728').attr('class','normal');
		
		$('#qal_364').attr('onclick','Switch_bw(364)');
		$('#qal_564').attr('onclick','Switch_bw(564)');
		$('#qal_896').attr('onclick','Switch_bw(896)');
		$('#qal_1196').attr('onclick','Switch_bw(1196)');
		$('#qal_1728').attr('onclick','Switch_bw(1728)');
		$('#qal_2728').attr('onclick','Switch_bw(2728)');
		
		$(bitrate_cour).attr('class','playing');
		
		
		switch_var = 1;
	}else if(switch_var == 1){
		//var bitrate_cour = '#qal_'+$f('free').getPlugin("bwcheck").getBitrate();
		$f('free').getPlugin("bwcheck").enableDynamic(1);
		$('#switch_auto').attr('class', 'bt_on');
		$('#switch_txt').html('&nbsp;&nbsp;&nbsp;&nbsp;Auto (ON)');
		$('#qal_364').attr('class','hold');
		$('#qal_564').attr('class','hold');
		$('#qal_896').attr('class','hold');
		$('#qal_1196').attr('class','hold');
		$('#qal_1728').attr('class','hold');
		$('#qal_2728').attr('class','hold');
		
		$('#qal_364').attr('onclick','');
		$('#qal_564').attr('onclick','');
		$('#qal_896').attr('onclick','');
		$('#qal_1196').attr('onclick','');
		$('#qal_1728').attr('onclick','');
		$('#qal_2728').attr('onclick','');
		
		$(bitrate_cour).attr('class','playing');
		
		switch_var = 0;
	}
	
	
	
}
function Switch_bw(bw){
	var id_link = '#qal_'+bw;
	$(id_link).attr('class','waiting');
	$f('free').getPlugin("bwcheck").setBitrate(bw);
}
</script>
     

	
			</td>
	</tr>
		
		
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:960px;margin:auto;">
            <table id="Tableau_01" width="960" border="0" cellpadding="0" cellspacing="0">
                <tr>
                	<td width="680" valign="top">
                    <!-- CADRE DE GAUCHE -->
                    
                    
                          
                          
                        <table width="680" cellpadding="0" cellspacing="0" class="noir" border="0">
<tr><td valign="middle" width="10">&nbsp;</td>
<td valign="middle" width="110"><b>EPISODE</b></td>
<td valign="middle" width="200"><center><b>DATE DE SORTIE</b></td>
<td valign="middle" width="200" class="or"><center><b>STREAMING</b></center></td>
<td valign="middle" width="160" class="or"><center><b>TELECHARGEMENT</b></center></td>
</tr>
<tr>
<td colspan="8"><img src="http://www.dramapassion.com/images/ligne680.jpg" ></td>
</tr>
<tr class="tab_blanc" id="tab_01"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 1</td>
<td height="25" valign="middle"><center>26-07-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/01/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2350&t=sd&i_norm=01&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_02" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 2</td>
<td height="25" valign="middle"><center>26-07-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/02/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2351&t=sd&i_norm=02&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_03"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 3</td>
<td height="25" valign="middle"><center>26-07-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/03/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2352&t=sd&i_norm=03&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_04" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 4</td>
<td height="25" valign="middle"><center>26-07-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/04/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2353&t=sd&i_norm=04&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_05"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 5</td>
<td height="25" valign="middle"><center>02-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/05/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2354&t=sd&i_norm=05&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_06" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 6</td>
<td height="25" valign="middle"><center>02-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/06/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2355&t=sd&i_norm=06&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_07"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 7</td>
<td height="25" valign="middle"><center>03-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/07/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2356&t=sd&i_norm=07&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_08" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 8</td>
<td height="25" valign="middle"><center>03-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/08/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2357&t=sd&i_norm=08&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_09"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 9</td>
<td height="25" valign="middle"><center>09-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/09/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2358&t=sd&i_norm=09&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_10" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 10</td>
<td height="25" valign="middle"><center>09-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/10/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2359&t=sd&i_norm=10&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_11"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 11</td>
<td height="25" valign="middle"><center>09-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/11/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2360&t=sd&i_norm=11&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_12" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 12</td>
<td height="25" valign="middle"><center>09-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/12/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2361&t=sd&i_norm=12&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_13"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 13</td>
<td height="25" valign="middle"><center>16-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/13/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2362&t=sd&i_norm=13&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_14" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 14</td>
<td height="25" valign="middle"><center>16-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/14/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2363&t=sd&i_norm=14&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_15"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 15</td>
<td height="25" valign="middle"><center>16-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/15/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2364&t=sd&i_norm=15&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_16" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 16</td>
<td height="25" valign="middle"><center>16-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/16/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2365&t=sd&i_norm=16&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_17"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 17</td>
<td height="25" valign="middle"><center>23-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/17/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2366&t=sd&i_norm=17&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_18" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 18</td>
<td height="25" valign="middle"><center>23-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/18/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2367&t=sd&i_norm=18&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc" id="tab_19"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 19</td>
<td height="25" valign="middle"><center>23-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/19/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2368&t=sd&i_norm=19&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris" id="tab_20" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 20</td>
<td height="25" valign="middle"><center>23-08-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant/20/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNNE1qWXlNVE0xT1E9PQ==&e=2369&t=sd&i_norm=20&DramaID=122"><img src="http://www.dramapassion.com/images/bt_dl.png" border="0"></a></center></td>
</tr>
</table>
                                                                                                                                        
  

<br><br>

					
                   
 
                    <iframe src="http://www.dramapassion.com/drama_coms.php?dramaID=122&type=0" width="680"  frameborder="0" style="border:none;margin-bottom:40px;" scrolling="no" id="id_iframe" class="noir" border="0" bgcolor="#ecebeb"></iframe>                   
                    
                    <br><br>    
                                        
                    
                    
                   
                     <!-- FIN CADRE DE GAUCHE -->
                    </td>
                    
                    <td width="30">&nbsp;</td>
                    
                    <td width="250" valign="top">
                    <!-- CADRE DE DROITE -->
					
                    <div class="fb-like" data-href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant" data-send="false" data-width="250" data-show-faces="true"></div>					
                    <br /><br />
                    <div style="border: solid 1px #d8bb43;width: 248px;height: 80px;margin-bottom: 20px;"><p style="text-align:center;margin-top:15px;"><span class="noir" style="font-size:14px;"><b>Comment modifier la qualité ?</b></span><br /><br /><a target="_blank" href="http://www.dramapassion.com/guide_lecteur" style="font-size:14px;" class="lien_bleu"><b>CLIQUEZ ICI</b></a></p></div>					
                    <table cellpadding="0" cellspacing="0" width="250" style="border: solid 1px #f6045b;padding : 5px;padding-bottom: 10px;"><tr><td colspan="2" class="menu_noir" style="font-size:14px;text-align:center;"><div style="height:20px;margin-top:5px;"><div style="display: inline-block;heihgt:15px;margin-top:1px;margin-right:7px;margin-left: -7px;"><img src="http://www.dramapassion.com/images/coeur.png" /></div><div style="display:inline-block;">Avec les mêmes acteurs...</div><div style="display: inline-block;margin-top:1px;margin-right : -7px;margin-left: 7px;"><img src="http://www.dramapassion.com/images/coeur.png" /></div></div></td></tr><tr colspan="2" height="10" ><td> </td></tr><tr height="38"><td width="70" align="left" style="padding-left:8px;"><a href="http://www.dramapassion.com/drama/81/The-Moon-That-Embraces-the-Sun"><img title="drama coréen &quot;The Moon That Embraces the Sun&quot; en vostfr" alt="série coréenne &quot;The Moon That Embraces the Sun&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/The%20Moon%20That%20Embraces%20the%20Sun_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/81/The-Moon-That-Embraces-the-Sun" class="lien_noir">The Moon That Embraces the Sun</a><br /><p style="color:grey;font-size:12px;"><form method="post" name="acteur_567_0" action="http://www.dramapassion.com/catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="567"><a href="javascript:void();" title="Voir toutes les séries avec Ahn Nae-Sang" class="lien_recommander" onclick="document.forms['acteur_567_0'].submit();">Ahn Nae-Sang    </a></form></p></td></tr><tr height="38"><td width="70" align="left" style="padding-left:8px;"><a href="http://www.dramapassion.com/drama/31/Baker-King-Kim-Tak-Gu"><img title="drama coréen &quot;Baker King, Kim Tak-Gu&quot; en vostfr" alt="série coréenne &quot;Baker King, Kim Tak-Gu&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/Baker%20King,%20Kim%20Tak-Gu_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/31/Baker-King-Kim-Tak-Gu" class="lien_noir">Baker King, Kim Tak-Gu</a><br /><p style="color:grey;font-size:12px;"><form method="post" name="acteur_172_1" action="http://www.dramapassion.com/catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="172"><a href="javascript:void();" title="Voir toutes les séries avec Joo Won" class="lien_recommander" onclick="document.forms['acteur_172_1'].submit();">Joo Won    </a></form></p></td></tr><tr height="38"><td width="70" align="left" style="padding-left:8px;"><a href="http://www.dramapassion.com/drama/98/Bridal-Mask"><img title="drama coréen &quot;Bridal Mask&quot; en vostfr" alt="série coréenne &quot;Bridal Mask&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/Bridal%20Mask_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/98/Bridal-Mask" class="lien_noir">Bridal Mask</a><br /><p style="color:grey;font-size:12px;"><form method="post" name="acteur_172_2" action="http://www.dramapassion.com/catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="172"><a href="javascript:void();" title="Voir toutes les séries avec Joo Won" class="lien_recommander" onclick="document.forms['acteur_172_2'].submit();">Joo Won    </a></form></p></td></tr><tr height="38"><td width="70" align="left" style="padding-left:8px;"><a href="http://www.dramapassion.com/drama/54/Royal-Family"><img title="drama coréen &quot;Royal Family&quot; en vostfr" alt="série coréenne &quot;Royal Family&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/Royal%20Family_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/54/Royal-Family" class="lien_noir">Royal Family</a><br /><p style="color:grey;font-size:12px;"><form method="post" name="acteur_567_3" action="http://www.dramapassion.com/catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="567"><a href="javascript:void();" title="Voir toutes les séries avec Ahn Nae-Sang" class="lien_recommander" onclick="document.forms['acteur_567_3'].submit();">Ahn Nae-Sang    </a></form></p></td></tr><tr height="38"><td width="70" align="left" style="padding-left:8px;"><a href="http://www.dramapassion.com/drama/133/Monstar"><img title="drama coréen &quot;Monstar&quot; en vostfr" alt="série coréenne &quot;Monstar&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/Monstar_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/133/Monstar" class="lien_noir">Monstar</a><br /><p style="color:grey;font-size:12px;"><form method="post" name="acteur_567_4" action="http://www.dramapassion.com/catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="567"><a href="javascript:void();" title="Voir toutes les séries avec Ahn Nae-Sang" class="lien_recommander" onclick="document.forms['acteur_567_4'].submit();">Ahn Nae-Sang    </a></form></p></td></tr></table>                    <br /><br />
					
                    						<table cellpadding="0" cellspacing="0" width="250">
						
	<tr>
		<td colspan="3" class="menu_noir">Top 10 du moment</td>
	</tr>
	<tr>
		<td colspan="3"><img src="http://www.dramapassion.com/images/ligne250.jpg" width="250" ></td>
	</tr><tr height="20px"><td valign="bottom" class="rose" align="left">1</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/124/Queen-of-the-Office" class="lien_noir">Queen of the Office</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">5<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">2</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/126/Gu-Family-Book" class="lien_noir">Gu Family Book</a></td><td valign="bottom" class="postop10egal" align="left"><img src="http://www.dramapassion.com/images/top10_egal.png" height=9 width=8/></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">3</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/2/Boys-Over-Flowers" class="lien_noir">Boys Over Flowers</a></td><td valign="bottom" class="postop10egal" align="left"><img src="http://www.dramapassion.com/images/top10_egal.png" height=9 width=8/></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">4</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/125/Can-We-Get-Married" class="lien_noir">Can We Get Married</a></td><td valign="bottom" class="postop10neg" align="left"><img src="http://www.dramapassion.com/images/top10_neg.png" height=9 width=8/><span class="noir">3<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">5</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/127/When-a-Man-Falls-in-Love" class="lien_noir">When a Man Falls in Love</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">2<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">6</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/122/Level-7-Civil-Servant" class="lien_noir">Level 7 Civil Servant</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">11<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">7</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/65/Heartstrings" class="lien_noir">Heartstrings</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">1<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">8</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/103/Faith" class="lien_noir">Faith</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">4<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">9</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/29/Personal-Taste" class="lien_noir">Personal Taste</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">19<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">10</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/113/My-Flower-Boy-Next-Door" class="lien_noir">My Flower Boy Next Door</a></td><td valign="bottom" class="postop10neg" align="left"><img src="http://www.dramapassion.com/images/top10_neg.png" height=9 width=8/><span class="noir">6<span class="noir"></td></tr>						<tr><td colspan="3"><br />
												<!-- small_rectangle_all_pages
						<div id='div-gpt-ad-1346273097508-1' style='width:250px; height:250px;'>
						<script type='text/javascript'>
							googletag.cmd.push(function() { googletag.display('div-gpt-ad-1346273097508-1'); });
						</script>
						</div>
						</td>
					</tr>
					</table>  -->
					  
					<a href="http://www.dramapassion.com/dvd/detail/4/Coffret-DVD-Vampire-Prosecutor"><img width="250" title="Le coffret DVD &quot;Vampire Prosecutor &quot;" alt="DVD &quot;Vampire Prosecutor&quot;"  src="http://www.dramapassion.com/content/dvd/vpdvdsmall.png" border="0"></a><br /><br /><br />                       
                    </td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>
<tr><td>
    
<!-- Google Code for conversion_2 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1026690034;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "N_WgCKTOyQcQ8pfI6QM";
var google_conversion_value = 0;
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:none;">
<img height="0" width="0" style="border-style:none;display:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1026690034/?value=0&amp;label=N_WgCKTOyQcQ8pfI6QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</td></tr>
<script>


$(document).ready(function() {
	 //setTimeout('addphoto()',4000);
	 //$('iframe').iframeAutoHeight();
	
$('#tab_05').css('background-color','#aa8b33');
}) 
 </script>
  <a href="http://www.dramapassion.com/version_flash.php" class="player_flash_alert iframe" style="display:none;" ></a>
</table>
  
<div class="margepied_all_page"><!-- ne pas enlever cette marge et laisser en dernier  --></div>
</div>  
<div id="cont_cont_tab_bottom">
<div style="width:1000px;margin:auto;margin-top: 25px;">
	<div >
		<div style="width:250px;float:left;margin-right:125px;">
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/a-propos-de-DramaPassion/" class="lien_footer">À propos de Dramapassion</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/conditions-generales/" class="lien_footer">Conditions d'utilisation et de vente</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/protection-des-donnees-personnelles/" class="lien_footer">Protection des données personnelles</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
		</div>
		<div style="width:250px;float:left;margin-right:125px;">
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/aide-faq/1" class="lien_footer">Aide - FAQ</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/contactez-nous/" class="lien_footer">Contactez-nous</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
		</div>
		<div style="width:250px;float:left;">
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/annonceurs" class="lien_footer">Publicités/Annonceurs</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/partenaires" class="lien_footer">Partenaires</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
</div>
<div id="cont_cont_tab_bottom2">
<div style="width:1000px;margin:auto;margin-top : 25px;">	
	<div >
		<span style="display:block;width:320px;margin:auto;">
			<img src="http://www.dramapassion.com/images/pics008.png">
			<img src="http://www.dramapassion.com/images/pics009.png">
			<img src="http://www.dramapassion.com/images/pics010.png">
			<img src="http://www.dramapassion.com/images/pics011.png">
			<img src="http://www.dramapassion.com/images/pics012.png">
			<img src="http://www.dramapassion.com/images/pics013.png"> 
		</span>
		<div style="margin-top : 25px;">
			<span style="text-align:center;font-style:italic;font-size:11px;color: #888888;display:block;" >Dramapassion.com est le premier site de VOD en ligne spécialisé dans les séries coréennes en version originale avec sous-titres français (VOSTFR).<br />Dramapassion.com propose un service gratuit et premium, en streaming et en téléchargement temporaire.</span>
			<span style="color: #ffffff;text-align:center;font-size:11px;display:block;margin-top:15px;">&copy; Vlexhan Distribution SPRL, 2013, tous droits réservés.</span>
		</div>
	</div>
</div>
</div>  
<!-- Code Google de la balise de remarketing -->
<!--------------------------------------------------
Les balises de remarketing ne peuvent pas être associées aux informations personnelles ou placées sur des pages liées aux catégories à caractère sensible. Pour comprendre et savoir comment configurer la balise, rendez-vous sur la page http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1026690034;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:none;">
<img height="1" width="1" style="border-style:none;display:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1026690034/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
 
<script>

$(window).unload( function() {
	//alert("http://www.dramapassion.com/user_track_ajax.php?i=MTQwNjQyNDA=");
	$.ajax({
        type: 'POST',
        url: "http://www.dramapassion.com/user_track_ajax.php?i=MTQwNjQyNDA=",
        async:false
    });
});
$(".tab_blanc").mouseover(function() {
	if($(this).css("background-color") == 'rgb(170, 139, 51)'){
		
	}else{
    $(this).css("background-color", "#d6d6d6");
    }
  }).mouseout(function(){
  if($(this).css("background-color") == 'rgb(170, 139, 51)'){
		
	}else{
	$(this).css("background-color", "#FFFFFF");
	}
  });
$(".tab_gris").mouseover(function() {
if($(this).css("background-color") == 'rgb(170, 139, 51)'){
		
	}else{
    $(this).css("background-color", "#d6d6d6");
    }
  }).mouseout(function(){
  if($(this).css("background-color") == 'rgb(170, 139, 51)'){
		
	}else{
	$(this).css("background-color", "#ecebeb");
	}
  });
  
  $(".identifier").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 400,
	'onClosed': function() {
		parent.location.reload(true);
	}
	
});
 $("#identifier2").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 300,
	'onClosed': function() {
		//parent.location.reload(true);
		parent.location = "http://www.dramapassion.com/";
	}
	
});

$("#register").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
  $(".playlist_a").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	
	
});
$(".playlist_a2").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	
	
});
$("#rec_facebook").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
$("#rec_tweet").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
$(".download").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
$("#test_HD2").fancybox({
	'href'			: 'http://www.dramapassion.com/test_video2.php?type=HD',
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 852,
	'height'		: 480,
	'padding'		: 0
	
	
});
$("#test_SD2").fancybox({
	'href'			: 'http://www.dramapassion.com/test_video2.php?type=SD',
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 852,
	'height'		: 480,
	'padding'		: 0
	
	
});
$(".player_flash_alert").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	'onClosed': function() {
		//parent.location.reload(true);
	}
	
});
  $(".click_pub").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 500,
	'height'		: 200,
	'overlayShow'	:	false
	
	
});
$("#pop_up_annonceurs").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 500,
	'height'		: 600
	
	
});
$(".playlist_a").mouseover(function(){
	var src = "http://www.dramapassion.com/images/playlist_over.png";
    $(this).find(".playlist_img").attr("src", src);
}).mouseout(function(){
	var src2 = "http://www.dramapassion.com/images/playlist.png";
    $(this).find(".playlist_img").attr("src", src2);
});
$(".playlist_a2").mouseover(function(){
	var src = "http://www.dramapassion.com/images/playlist_over.png";
    $(this).find(".playlist_img").attr("src", src);
}).mouseout(function(){
	var src2 = "http://www.dramapassion.com/images/playlist.png";
    $(this).find(".playlist_img").attr("src", src2);
}); 
function supprimer_gris_recherche(){
	
	document.getElementById('top_recherche').value = "";
	document.getElementById('top_recherche').style.color = "black";
}
 function valide(type,nb){
	
	if(nb < 10){
		nb = "0"+nb;
	}
	if(type == 'sd' || type == 'auto_hd'  || type == 'auto_sd'){
		type2 = 'hd';
	}else{
		type2 = type;
	}
	
	var hidden = "hidden_"+type2+"_"+nb;
	var form = "form_"+type2+"_"+nb;
	document.getElementById(hidden).value = type;
	
	document.forms[form].submit(); 
	
 }  
 function valide2(type,nb,dramaID){
	
	if(nb < 10){
		nb = "0"+nb;
	}
	type2 = 'hd';
	
	var hidden = "hidden_"+type2+"_"+dramaID+"_"+nb;
	
	var form = "form_"+type2+"_"+dramaID+"_"+nb;
	
	document.getElementById(hidden).value = type;
	
	
	document.forms[form].submit(); 
	
 } 
</script>

</body>
</html>