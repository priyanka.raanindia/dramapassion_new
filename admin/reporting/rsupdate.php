<?php
require_once("../../includes/settings.inc.php");
require_once("../../includes/dbinfo.inc.php");

$year = $_GET['year'];
$month = $_GET['month'];

//DELETE EXISTING LINE
$sql = "SELECT * FROM ad_stat_rs WHERE Year = ".$year." AND Month = ".$month;
$que = mysql_query($sql);
$line = mysql_num_rows($que);
if($line > 0){
	$del = "DELETE FROM ad_stat_rs WHERE Year = ".$year." AND Month = ".$month;
	$exec_del = mysql_query($del);	
}

//TOTAL NUMBERS
$sql_count_pay_tot = "SELECT SUM(Pays_total) FROM t_dp_paystat_tot WHERE YEAR(Date_j) = ".$year." AND MONTH(Date_j) = ".$month;
$que_count_pay_tot = mysql_query($sql_count_pay_tot);
$count_tot_pay = mysql_result($que_count_pay_tot,0,'SUM(Pays_total)');

$sql_count_free_tot = "SELECT SUM(Pays_total) FROM t_dp_freestat WHERE YEAR(Date_j) = ".$year." AND MONTH(Date_j) = ".$month;
$que_count_free_tot = mysql_query($sql_count_free_tot);
$count_tot_free = mysql_result($que_count_free_tot,0,'SUM(Pays_total)');

$sql_rs_tot = "SELECT Source,SUM(Amount) FROM ad_stat_rev WHERE Year = ".$year." AND Month = ".$month." GROUP BY Source";
$que_rs_tot = mysql_query($sql_rs_tot);
$rs_line = mysql_num_rows($que_rs_tot);
$rs_tot_pay = 0;
$rs_tot_free = 0;
for($i=0; $i<$rs_line; $i++){
	$src = mysql_result($que_rs_tot,$i,'Source');
	$add = mysql_result($que_rs_tot,$i,'SUM(Amount)');
	if($src == 'ogo' || $src == 'allo'){
		$rs_tot_pay = $rs_tot_pay + $add;
	} else {
		$rs_tot_free = $rs_tot_free + $add;
	}
}
$rs_tot_pay = $rs_tot_pay * 0.25;
$rs_tot_free = $rs_tot_free * 0.25;

//PAY VOD
$sql_pay = "SELECT DramaID,SUM(Pays_total) FROM t_dp_paystat_tot WHERE YEAR(Date_j) = ".$year." AND MONTH(Date_j) = ".$month." GROUP BY DramaID";
$que_pay = mysql_query($sql_pay);
$line_pay = mysql_num_rows($que_pay);
for($i=0; $i<$line_pay; $i++){
	$dramaid = mysql_result($que_pay,$i,'DramaID');
	$count = mysql_result($que_pay,$i,'SUM(Pays_total)');
	$pc = $count / $count_tot_pay * 100;
	$amount = $pc / 100 * $rs_tot_pay;
	$pc = round($pc,2);
	$amount = round($amount,2);
		
	$sql_lic = "SELECT LicensorID FROM t_dp_drama WHERE DramaID = ".$dramaid;
	$que_lic = mysql_query($sql_lic);
	$licid = mysql_result($que_lic,0,'LicensorID');
	
	$ins = "INSERT INTO ad_stat_rs VALUES (".$year.",".$month.",1,".$dramaid.",".$licid.",".$count.",".$pc.",".$amount.")";
	$exec_ins = mysql_query($ins);
}

//FREE VOD
$sql_free = "SELECT DramaID,SUM(Pays_total) FROM t_dp_freestat WHERE YEAR(Date_j) = ".$year." AND MONTH(Date_j) = ".$month." GROUP BY DramaID";
$que_free = mysql_query($sql_free);
$line_free = mysql_num_rows($que_free);
for($i=0; $i<$line_free; $i++){
	$dramaid = mysql_result($que_free,$i,'DramaID');
	$count = mysql_result($que_free,$i,'SUM(Pays_total)');
	$pc = $count / $count_tot_free * 100;
	$amount = $pc / 100 * $rs_tot_free;
	$pc = round($pc,2);
	$amount = round($amount,2);
	$sql_lic = "SELECT LicensorID FROM t_dp_drama WHERE DramaID = ".$dramaid;
	$que_lic = mysql_query($sql_lic);
	$licid = mysql_result($que_lic,0,'LicensorID');
	
	$ins = "INSERT INTO ad_stat_rs VALUES (".$year.",".$month.",2,".$dramaid.",".$licid.",".$count.",".$pc.",".$amount.")";
	$exec_ins = mysql_query($ins);
}

$url = "index.php?year=".$year."&month=".$month;
header("Location: ".$url);

?>
