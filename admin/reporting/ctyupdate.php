<?php
require_once("../../includes/settings.inc.php");
require_once("../../includes/dbinfo.inc.php");

$year = $_GET['year'];
$month = $_GET['month'];

//DELETE EXISTING LINES
$sql_chk = "SELECT * FROM ad_stat_cty WHERE Year = ".$year." AND Month = ".$month;
$que_chk = mysql_query($sql_chk);
$chk_line = mysql_num_rows($que_chk);
if($chk_line > 0){
	$del = "DELETE FROM ad_stat_cty WHERE Year = ".$year." AND Month = ".$month;
	$exec_del = mysql_query($del);
}

//PAY STATISTICS
$sql_pay = "SELECT SUM(Pays_BE),SUM(Pays_FR),SUM(Pays_UK),SUM(Pays_NL),SUM(Pays_LU),SUM(Pays_CH),SUM(Pays_total) FROM t_dp_paystat_tot WHERE YEAR(Date_j) = ".$year." AND MONTH(Date_j) = ".$month;
$que_pay = mysql_query($sql_pay);
$numline = mysql_num_rows($que_pay);
$pay_be = mysql_result($que_pay,0,'SUM(Pays_BE)');
$pay_fr = mysql_result($que_pay,0,'SUM(Pays_FR)');
$pay_uk = mysql_result($que_pay,0,'SUM(Pays_UK)');
$pay_nl = mysql_result($que_pay,0,'SUM(Pays_NL)');
$pay_lu = mysql_result($que_pay,0,'SUM(Pays_LU)');
$pay_ch = mysql_result($que_pay,0,'SUM(Pays_CH)');
$pay_tot = mysql_result($que_pay,0,'SUM(Pays_total)');

$pay_be = $pay_be / $pay_tot * 100;
$pay_fr = $pay_fr / $pay_tot * 100;
$pay_uk = $pay_uk / $pay_tot * 100;
$pay_nl = $pay_nl / $pay_tot * 100;
$pay_lu = $pay_lu / $pay_tot * 100;
$pay_ch = $pay_ch / $pay_tot * 100;

$pay_be = round($pay_be,2);
$pay_fr = round($pay_fr,2);
$pay_uk = round($pay_uk,2);
$pay_nl = round($pay_nl,2);
$pay_lu = round($pay_lu,2);
$pay_ch = round($pay_ch,2);

$ins_pay_be = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",1,'Belgium',".$pay_be.")";
$ins_pay_fr = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",1,'France',".$pay_fr.")";
$ins_pay_uk = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",1,'United Kingdom',".$pay_uk.")";
$ins_pay_nl = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",1,'Netherlands',".$pay_nl.")";
$ins_pay_lu = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",1,'Luxembourg',".$pay_lu.")";
$ins_pay_ch = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",1,'Switzerland',".$pay_ch.")";
$exe_pay_be = mysql_query($ins_pay_be);
$exe_pay_fr = mysql_query($ins_pay_fr);
$exe_pay_uk = mysql_query($ins_pay_uk);
$exe_pay_nl = mysql_query($ins_pay_nl);
$exe_pay_lu = mysql_query($ins_pay_lu);
$exe_pay_ch = mysql_query($ins_pay_ch);

//FREE STATISTICS
$sql_free = "SELECT SUM(Pays_BE),SUM(Pays_FR),SUM(Pays_UK),SUM(Pays_NL),SUM(Pays_LU),SUM(Pays_CH),SUM(Pays_total) FROM t_dp_freestat WHERE YEAR(Date_j) = ".$year." AND MONTH(Date_j) = ".$month;
$que_free = mysql_query($sql_free);
$free_be = mysql_result($que_free,0,'SUM(Pays_BE)');
$free_fr = mysql_result($que_free,0,'SUM(Pays_FR)');
$free_uk = mysql_result($que_free,0,'SUM(Pays_UK)');
$free_nl = mysql_result($que_free,0,'SUM(Pays_NL)');
$free_lu = mysql_result($que_free,0,'SUM(Pays_LU)');
$free_ch = mysql_result($que_free,0,'SUM(Pays_CH)');
$free_tot = mysql_result($que_free,0,'SUM(Pays_total)');

$free_be = $free_be / $free_tot * 100;
$free_fr = $free_fr / $free_tot * 100;
$free_uk = $free_uk / $free_tot * 100;
$free_nl = $free_nl / $free_tot * 100;
$free_lu = $free_lu / $free_tot * 100;
$free_ch = $free_ch / $free_tot * 100;

$free_be = round($free_be,2);
$free_fr = round($free_fr,2);
$free_uk = round($free_uk,2);
$free_nl = round($free_nl,2);
$free_lu = round($free_lu,2);
$free_ch = round($free_ch,2);

$ins_free_be = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",2,'Belgium',".$free_be.")";
$ins_free_fr = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",2,'France',".$free_fr.")";
$ins_free_uk = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",2,'United Kingdom',".$free_uk.")";
$ins_free_nl = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",2,'Netherlands',".$free_nl.")";
$ins_free_lu = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",2,'Luxembourg',".$free_lu.")";
$ins_free_ch = "INSERT INTO ad_stat_cty VALUES (".$year.",".$month.",2,'Switzerland',".$free_ch.")";
$exe_free_be = mysql_query($ins_free_be);
$exe_free_fr = mysql_query($ins_free_fr);

$url = "index.php?year=".$year."&month=".$month;
header("Location: ".$url);

?>