<?php
require_once("../../includes/settings.inc.php");
require_once("../../includes/dbinfo.inc.php");
require_once("top.php");

if(isset($_GET['year'])){
	$year = $_GET['year'];
} else {
	$year = 2012;
}

if(isset($_GET['month'])){
	$month = $_GET['month'];
} else {
	$month = date("n")-1;
}

$moname = array('empty','January','February','March','April','May','June','July','August','September','October','November','December');
$subname = array('empty','Disc1m','Disc3m','Priv1m','Priv3m','Discfl','Privfl','Discpm','Discpm','Disc1w','Priv6m');
$order = array(9,1,2,3,4,10,5,6);

?>

<!-- TABLE ALL REVENUES -->
<tr><td>
<div style="width:1000px;margin:auto;">
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
<p style="margin-top:5px; margin-bottom:3px; vertical-align:middle;">
<form action="index.php" method="get" style="font-size:12px;">
&nbsp;&nbsp;&nbsp;&nbsp;
YEAR : <select name="year">
<?php
for($i=2012; $i<=2015; $i++){
	if($i == $year){
		echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';	
	} else {
		echo '<option value="'.$i.'">'.$i.'</option>';	
	}
}
?>
</select>
&nbsp;&nbsp;&nbsp;&nbsp;
MONTH : <select name="month">
<?php
for($i=1; $i<=12; $i++){
	if($i == $month){
		echo '<option value="'.$i.'" selected="selected">'.$moname[$i].'</option>';	
	} else {
		echo '<option value="'.$i.'">'.$moname[$i].'</option>';	
	}
}
?>
</select>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" value="UPDATE">
</form>
</p>
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
</div>
</td></tr>

<tr><td>
<br /><br />
<div style="width:1000px;margin:auto;">
<!-- BLOC ONE -->
<table width="1000" border="0" cellpadding="0" cellspacing="0">
<!-- Revenues all -->
<tr>
<td valign="top" width="480">
<?php
$ogo = 0;
$ogo_det = array(0,0,0,0,0,0,0,0,0,0,0);
$allo = 0;
$sticky = 0;
$specm = 0;
$adsense = 0;
$fourm = 0;

$sql_ogo = "SELECT Source,SUM(Amount) FROM ad_stat_rev WHERE Source = 'ogo' AND Year = ".$year." AND Month = ".$month." GROUP BY Source";
$que_ogo = mysql_query($sql_ogo);
$ogo = mysql_result($que_ogo,0,'SUM(Amount)');

$sql_det = "SELECT Type,SUM(Amount) FROM ad_stat_rev WHERE Source = 'ogo' AND Year = ".$year." AND Month = ".$month." GROUP BY Type";
$que_det = mysql_query($sql_det);
$det_line = mysql_num_rows($que_det);
for($i = 0; $i < $det_line; $i++){
	$subid = mysql_result($que_det, $i, 'Type');
	$amount = mysql_result($que_det, $i, 'SUM(Amount)');
	$ogo_det[$subid] = $amount;
}

$sql_allo = "SELECT Amount FROM ad_stat_rev WHERE Source = 'allo' AND Year = ".$year." AND Month = ".$month;
$que_allo = mysql_query($sql_allo);
$allo = mysql_result($que_allo, 0, 'Amount');

$sql_sticky = "SELECT Amount FROM ad_stat_rev WHERE Source = 'sticky' AND Year = ".$year." AND Month = ".$month;
$que_sticky = mysql_query($sql_sticky);
$sticky = mysql_result($que_sticky, 0, 'Amount');

$sql_specm = "SELECT Amount FROM ad_stat_rev WHERE Source = 'specm' AND Year = ".$year." AND Month = ".$month;
$que_specm = mysql_query($sql_specm);
$specm = mysql_result($que_specm, 0, 'Amount');

$sql_adsense = "SELECT Amount FROM ad_stat_rev WHERE Source = 'adsense' AND Year = ".$year." AND Month = ".$month;
$que_adsense = mysql_query($sql_adsense);
$adsense = mysql_result($que_adsense, 0, 'Amount');

$sql_fourm = "SELECT Amount FROM ad_stat_rev WHERE Source = 'fourm' AND Year = ".$year." AND Month = ".$month;
$que_fourm = mysql_query($sql_fourm);
$fourm = mysql_result($que_fourm, 0, 'Amount');

$tot_pay = $ogo + $allo;
$tot_free = $sticky + $specm + $adsense + $fourm;
$tot_gen = $tot_pay + $tot_free;
$rs_pay = $tot_pay * 0.25;
$rs_free = $tot_free * 0.25;
$rs_tot = $rs_pay + $rs_free;
?>
<table width="480" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
<tr height="25">
<td width="180">
	<b>Total revenues</b>
</td>
<td width="150" align="right">
	<b><?php echo round($tot_gen,0); ?></b>
</td>
<td width="150" align="right">
	<b><?php echo round($rs_tot,0); ?></b>
</td>
<td width="20">
	&nbsp;
</td>
<td width="80" align="center">
	&nbsp;
</td>
</tr>
<tr height="8">
<td colspan="5">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="480" />
</td>
</tr>
<tr height="25">
<td>
	<b>Subscriptions (pay)</b>
</td>
<td align="right">
	<b><?php echo round($tot_pay,0); ?></b>
</td>
<td align="right">
	<b><?php echo round($rs_pay,0); ?></b>
</td>
<td>
	&nbsp;
</td>
<td align="center">
	&nbsp;
</td>
</tr>
<tr height="8">
<td colspan="5">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="480" />
</td>
</tr>
<tr height="25">
<td>
	<b>Regular payments</b>
</td>
<td align="right">
	<b><?php echo round($ogo,0); ?></b>
</td>
<td align="right">
	&nbsp;
</td>
<td>
	&nbsp;
</td>
<td align="center">
	<a href="ogoupdate.php?year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
<?php
for($i=0; $i<=7; $i++){
	$s = $order[$i];
?>
<tr height="25">
<td>
&nbsp;&nbsp;<?php echo $subname[$s]; ?>
</td>
<td align="right">
	<?php echo round($ogo_det[$s],0); ?>
</td>
<td align="right">
	&nbsp;
</td>
<td>
	&nbsp;
</td>
<td align="center">
	&nbsp;
</td>
</tr>
<?php } ?>
<tr height="25">
<td>
	<b>Allopass</b>
</td>
<td align="right">
	<b><?php echo round($allo,0); ?></b>
</td>
<td align="right">
	&nbsp;
</td>
<td>
	&nbsp;
</td>
<td align="center">
	<a href="revupdate.php?src=allo&year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
<tr height="8">
<td colspan="5">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="480" />
</td>
</tr>
<tr height="25">
<td>
	<b>Advertisements (free)</b>
</td>
<td align="right">
	<b><?php echo round($tot_free,0); ?></b>
</td>
<td align="right">
	<b><?php echo round($rs_free,0); ?></b>
</td>
<td>
	&nbsp;
</td>
<td align="center">
	&nbsp;
</td>
</tr>
<tr height="8">
<td colspan="5">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="480" />
</td>
</tr>
<tr height="25">
<td>
	<b>Sticky Ads</b>
</td>
<td align="right">
	<b><?php echo round($sticky,0); ?></b>
</td>
<td align="right">
	&nbsp;
</td>
<td>
	&nbsp;
</td>
<td align="center">
	<a href="revupdate.php?src=sticky&year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
<tr height="25">
<td>
	<b>Specific Media</b>
</td>
<td align="right">
	<b><?php echo round($specm,0); ?></b>
</td>
<td align="right">
	&nbsp;
</td>
<td>
	&nbsp;
</td>
<td align="center">
	<a href="revupdate.php?src=specm&year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
<tr height="25">
<td>
	<b>Google Adsense</b>
</td>
<td align="right">
	<b><?php echo round($adsense,0); ?></b>
</td>
<td align="right">
	&nbsp;
</td>
<td>
	&nbsp;
</td>
<td align="center">
	<a href="revupdate.php?src=adsense&year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
<tr height="25">
<td>
	<b>Four Media</b>
</td>
<td align="right">
	<b><?php echo round($fourm,0); ?></b>
</td>
<td align="right">
	&nbsp;
</td>
<td>
	&nbsp;
</td>
<td align="center">
	<a href="revupdate.php?src=fourm&year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
</table>
</td>
<td width="40">
&nbsp;
</td>
<!-- COLUMN RIGHT -->
<td width="480" valign="top">
<table width="480" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
<!-- BY LICENSOR -->
<tr height="25">
<td width="120">
	<b>RS per licensor</b>
</td>
<td width="90" align="right">
	&nbsp;
</td>
<td width="90" align="right">
	&nbsp;
</td>
<td width="90" align="right">
	&nbsp;
</td>
<td width="90" align="center">
	<a href="rsupdate.php?year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
<tr height="8">
<td colspan="5">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="480" />
</td>
</tr>
<tr height="25">
<td>
	<b>Licensor</b>
</td>
<td align="right">
	<b>Pay VOD</b>
</td>
<td align="right">
	<b>Free VOD</b>
</td>
<td align="right">
	<b>Total</b>
</td>
<td align="right">
	<b>%</b>
</td>
</tr>
<tr height="8">
<td colspan="5">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="480" />
</td>
</tr>
<?php
$licorder = array(1,3,2,8,9,4,10,7);

$lic_tot_pay = 0;
$lic_tot_free = 0;
$lic_tot_tot = 0;
$lic_tot_pc = 0;

for($i = 0; $i < 8; $i++){
$licid = $licorder[$i];
$sql_lic_list = "SELECT LicensorName FROM t_dp_licensor WHERE LicensorID = ".$licid;
$que_lic_list = mysql_query($sql_lic_list);
$licname = mysql_result($que_lic_list, 0, 'LicensorName');

$sql_lic_pay = "SELECT SUM(Amount) FROM ad_stat_rs WHERE Type = 1 AND Licensor = ".$licid." AND Year = ".$year." AND Month = ".$month." GROUP BY Licensor";
$que_lic_pay = mysql_query($sql_lic_pay);
$lic_pay = mysql_result($que_lic_pay, 0, 'SUM(Amount)');

$sql_lic_free = "SELECT SUM(Amount) FROM ad_stat_rs WHERE Type = 2 AND Licensor = ".$licid." AND Year = ".$year." AND Month = ".$month." GROUP BY Licensor";
$que_lic_free = mysql_query($sql_lic_free);
$lic_free = mysql_result($que_lic_free, 0, 'SUM(Amount)');

$lic_tot = $lic_pay + $lic_free;
$lic_pc = $lic_tot / $rs_tot * 100;

$lic_tot_pay = $lic_tot_pay + $lic_pay;
$lic_tot_free = $lic_tot_free + $lic_free;
$lic_tot_tot = $lic_tot_tot + $lic_tot;
$lic_tot_pc = $lic_tot_pc + $lic_pc;
?>
<tr height="25">
<td>
	<?php echo $licname; ?>
</td>
<td align="right">
	<?php echo round($lic_pay,0); ?>
</td>
<td align="right">
	<?php echo round($lic_free,0); ?>
</td>
<td align="right">
	<?php echo round($lic_tot,0); ?>
</td>
<td align="right">
	<?php echo round($lic_pc,1); ?>&nbsp;%
</td>
</tr>
<?php } ?>
<tr height="8">
<td colspan="5">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="480" />
</td>
</tr>
<tr height="25">
<td>
	Total
</td>
<td align="right">
	<?php echo round($lic_tot_pay,0); ?>
</td>
<td align="right">
	<?php echo round($lic_tot_free,0); ?>
</td>
<td align="right">
	<?php echo round($lic_tot_tot,0); ?>
</td>
<td align="right">
	<?php echo round($lic_tot_pc,1); ?>&nbsp;%
</td>
</tr>
</table>
<br /><br />
<table width="300" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
<!-- BY COUNTRY -->
<tr height="25">
<td width="120">
	<b>Split per country</b>
</td>
<td width="90" align="right">
	&nbsp;
</td>
<td width="90" align="center">
	<a href="ctyupdate.php?year=<?php echo $year; ?>&month=<?php echo $month; ?>" class="lien_bleu">Update</a>
</td>
</tr>
<tr height="8">
<td colspan="3">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="300" />
</td>
</tr>
<tr height="25">
<td>
	<b>Country</b>
</td>
<td align="right">
	<b>Pay VOD</b>
</td>
<td align="right">
	<b>Free VOD</b>
</td>
</tr>
<tr height="8">
<td colspan="3">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="300" />
</td>
</tr>
<?php 
$sql_cty = "SELECT Country,SUM(Count) FROM ad_stat_cty WHERE Year = ".$year." AND Month = ".$month." GROUP BY Country ORDER BY 2 DESC";
$que_cty = mysql_query($sql_cty);
$cty_line = mysql_num_rows($que_cty);
$cty_tot_pay = 0;
$cty_tot_free = 0;

for ($i = 0; $i < $cty_line; $i++){
	$cty_name = mysql_result($que_cty, $i, 'Country');
	$sql_cty_pay = "SELECT * FROM ad_stat_cty WHERE Type = 1 AND Year = ".$year." AND Month = ".$month." AND Country = '".$cty_name."'";
	$que_cty_pay = mysql_query($sql_cty_pay);
	$sql_cty_free = "SELECT * FROM ad_stat_cty WHERE Type = 2 AND Year = ".$year." AND Month = ".$month." AND Country = '".$cty_name."'";
	$que_cty_free = mysql_query($sql_cty_free);
	
	$cty_count_pay = mysql_result($que_cty_pay, 0, 'Count');
	$cty_count_free = mysql_result($que_cty_free, 0, 'Count');
	$cty_tot_pay = $cty_tot_pay + $cty_count_pay;
	$cty_tot_free = $cty_tot_free + $cty_count_free;	
?>
<tr height="25">
<td>
	<?php echo $cty_name; ?>
</td>
<td align="right">
	<?php echo round($cty_count_pay,1); ?>&nbsp;%
</td>
<td align="right">
	<?php echo round($cty_count_free,1); ?>&nbsp;%
</td>
</tr>
<?php }  ?>
<tr height="8">
<td colspan="3">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="300" />
</td>
</tr>
<tr height="25">
<td>
	Total
</td>
<td align="right">
	<?php echo round($cty_tot_pay,1); ?>&nbsp;%
</td>
<td align="right">
	<?php echo round($cty_tot_free,1); ?>&nbsp;%
</td>
</tr>
</table>
</td>
</tr>
</table>
<br /><br /><br /><br />
<table width="850" border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
<tr height="25">
<td width="130">
	<b>Revenue details</b>
</td>
<?php
for($i = 0 ; $i <= 6 ; $i++){
	$check = $month - (6 - $i);
	if($check <= 0){
		$check = $check + 12;
	}
	$rs_mo = $moname[$check];
?>
<td width="90" align="right">
	<b><?php echo $rs_mo; ?></b>
</td>
<?php } ?>
<td width="90" align="right">
	<b>Total</b>
</td>
<tr height="8">
<td colspan="9">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="850" />
</td>
</tr>
<?php
$sql_per = "SELECT Type, Period, SUM(Amount) FROM ad_stat_rev WHERE Source = 'ogo' AND Year = ".$year." AND Month = ".$month." GROUP BY Type, Period";
$que_per = mysql_query($sql_per);
$per_line = mysql_num_rows($que_per);
$ogo_per = array();
$ogo_row_tot = array();
$ogo_col_tot = array();
$grand_tot = 0;
for($i = 0; $i < $per_line; $i++){
	$subid2 = mysql_result($que_per, $i, 'Type');
	$perid2 = mysql_result($que_per, $i, 'Period');
	$amount2 = mysql_result($que_per, $i, 'SUM(Amount)');
	$ogo_per[$subid2][$perid2] = $amount2;
	$ogo_row_tot[$subid2] = $ogo_row_tot[$subid2] + $ogo_per[$subid2][$perid2];
	$grand_tot = $grand_tot + $ogo_per[$subid2][$perid2];
}

for($i=0; $i<=6; $i++){
	for($j=0; $j<=7; $j++){
		$s = $order[$j];
		$ogo_col_tot[$i] = $ogo_col_tot[$i] + $ogo_per[$s][$i];
	}
}

for($i=0; $i<=7; $i++){
	$s = $order[$i];
?>
<tr height="25">
<td>
	<?php echo $subname[$s]; ?>
</td>
<?php
for ($j=0; $j<=6; $j++){
	$period = 6 - $j;
?>
<td align="right">
	<?php echo round($ogo_per[$s][$period],0); ?>
</td>
<?php } ?>
<td align="right">
	<?php echo round($ogo_row_tot[$s],0); ?>
</td>
</tr>
<?php } ?>
<tr height="8">
<td colspan="9">
	<img src="<?php echo $http ; ?>images/ligne1000.jpg" width="850" />
</td>
</tr>
<tr height="25">
<td>
	Total
</td>
<?php
for ($j=0; $j<=6; $j++){
	$period = 6 - $j;
?>
<td align="right">
	<?php echo round($ogo_col_tot[$period],0); ?>
</td>
<?php } ?>
<td align="right">
	<?php echo round($grand_tot,0); ?>
</td>
</tr>
</table>
</div>
</td></tr>

<?php
require_once("bottom.php");
?>