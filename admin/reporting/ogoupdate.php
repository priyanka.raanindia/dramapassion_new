<?php
require_once("../../includes/settings.inc.php");
require_once("../../includes/dbinfo.inc.php");

$year = $_GET['year'];
$month = $_GET['month'];

//CLEAN DB FIRST
$sql = "SELECT * FROM ad_stat_rev WHERE Year = ".$year." AND Month = ".$month." AND Source = 'ogo'";
$que = mysql_query($sql);
$line = mysql_num_rows($que);
if($line > 0){
	$del = "DELETE FROM ad_stat_rev WHERE Year = ".$year." AND Month = ".$month." AND Source = 'ogo'";
	$exec_del = mysql_query($del);	
}

//6 MONTHS AGO
$yr6 = $year;
$mo6 = $month - 6;
if($mo6 <= 0){
	$mo6 = $mo6 + 12;
	$yr6 = $yr6 - 1;
}
$sql6 = "SELECT * FROM t_dp_transaction WHERE TransTypeID = 6 AND YEAR(TransDateTime) = ".$yr6." AND MONTH(TransDateTime) = ".$mo6." AND Method <> 'ALLOPASS'";
$que6 = mysql_query($sql6);
$line6 = mysql_num_rows($que6);
$amount6p6m = 0;

for($i=0; $i<$line6; $i++){
	$subs = mysql_result($que6,$i,'TransProductID');
	$add = mysql_result($que6,$i,'TransEuro');
	if($subs == 10){
		$amount6p6m = $amount6p6m + $add;
	}
}

$amount6p6m = $amount6p6m / 12;
$amount6p6m = $amount6p6m / 1.21;
$amount6p6m = round($amount6p6m,2);
$ins6p6m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',10,6,".$amount6p6m.")";
$exe6p6m = mysql_query($ins6p6m);

//5 MONTHS AGO
$yr5 = $year;
$mo5 = $month - 5;
if($mo5 <= 0){
	$mo5 = $mo5 + 12;
	$yr5 = $yr5 - 1;
}
$sql5 = "SELECT * FROM t_dp_transaction WHERE TransTypeID = 6 AND YEAR(TransDateTime) = ".$yr5." AND MONTH(TransDateTime) = ".$mo5." AND Method <> 'ALLOPASS'";
$que5 = mysql_query($sql5);
$line5 = mysql_num_rows($que5);
$amount5p6m = 0;

for($i=0; $i<$line5; $i++){
	$subs = mysql_result($que5,$i,'TransProductID');
	$add = mysql_result($que5,$i,'TransEuro');
	if($subs == 10){
		$amount5p6m = $amount5p6m + $add;
	}
}

$amount5p6m = $amount5p6m / 6;
$amount5p6m = $amount5p6m / 1.21;
$amount5p6m = round($amount5p6m,2);
$ins5p6m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',10,5,".$amount5p6m.")";
$exe5p6m = mysql_query($ins5p6m);

//4 MONTHS AGO
$yr4 = $year;
$mo4 = $month - 4;
if($mo4 <= 0){
	$mo4 = $mo4 + 12;
	$yr4 = $yr4 - 1;
}
$sql4 = "SELECT * FROM t_dp_transaction WHERE TransTypeID = 6 AND YEAR(TransDateTime) = ".$yr4." AND MONTH(TransDateTime) = ".$mo4." AND Method <> 'ALLOPASS'";
$que4 = mysql_query($sql4);
$line4 = mysql_num_rows($que4);
$amount4p6m = 0;

for($i=0; $i<$line4; $i++){
	$subs = mysql_result($que4,$i,'TransProductID');
	$add = mysql_result($que4,$i,'TransEuro');
	if($subs == 10){
		$amount4p6m = $amount4p6m + $add;
	}
}

$amount4p6m = $amount4p6m / 6;
$amount4p6m = $amount4p6m / 1.21;
$amount4p6m = round($amount4p6m,2);
$ins4p6m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',10,4,".$amount4p6m.")";
$exe4p6m = mysql_query($ins4p6m);

//3 MONTHS AGO
$yr3 = $year;
$mo3 = $month - 3;
if($mo3 <= 0){
	$mo3 = $mo3 + 12;
	$yr3 = $yr3 - 1;
}
$sql3 = "SELECT * FROM t_dp_transaction WHERE TransTypeID = 6 AND YEAR(TransDateTime) = ".$yr3." AND MONTH(TransDateTime) = ".$mo3." AND Method <> 'ALLOPASS'";
$que3 = mysql_query($sql3);
$line3 = mysql_num_rows($que3);
$amount3p6m = 0;
$amount3p3m = 0;
$amount3d3m = 0;

for($i=0; $i<$line3; $i++){
	$subs = mysql_result($que3,$i,'TransProductID');
	$add = mysql_result($que3,$i,'TransEuro');
	if($subs == 10){
		$amount3p6m = $amount3p6m + $add;
	}
	elseif($subs == 4){
		$amount3p3m = $amount3p3m + $add;
	}
	elseif($subs == 2){
		$amount3d3m = $amount3d3m + $add;
	}
	else{
	}
}

$amount3p6m = $amount3p6m / 6;
$amount3p6m = $amount3p6m / 1.21;
$amount3p6m = round($amount3p6m,2);
$amount3p3m = $amount3p3m / 6;
$amount3p3m = $amount3p3m / 1.21;
$amount3p3m = round($amount3p3m,2);
$amount3d3m = $amount3d3m / 6;
$amount3d3m = $amount3d3m / 1.21;
$amount3d3m = round($amount3d3m,2);

$ins3p6m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',10,3,".$amount3p6m.")";
$exe3p6m = mysql_query($ins3p6m);
$ins3p3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',4,3,".$amount3p3m.")";
$exe3p3m = mysql_query($ins3p3m);
$ins3d3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',2,3,".$amount3d3m.")";
$exe3d3m = mysql_query($ins3d3m);

//2 MONTHS AGO
$yr2 = $year;
$mo2 = $month - 2;
if($mo2 <= 0){
	$mo2 = $mo2 + 12;
	$yr2 = $yr2 - 1;
}
$sql2 = "SELECT * FROM t_dp_transaction WHERE TransTypeID = 6 AND YEAR(TransDateTime) = ".$yr2." AND MONTH(TransDateTime) = ".$mo2." AND Method <> 'ALLOPASS'";
$que2 = mysql_query($sql2);
$line2 = mysql_num_rows($que2);
$amount2p6m = 0;
$amount2p3m = 0;
$amount2d3m = 0;

for($i=0; $i<$line2; $i++){
	$subs = mysql_result($que2,$i,'TransProductID');
	$add = mysql_result($que2,$i,'TransEuro');
	if($subs == 10){
		$amount2p6m = $amount2p6m + $add;
	}
	elseif($subs == 4){
		$amount2p3m = $amount2p3m + $add;
	}
	elseif($subs == 2){
		$amount2d3m = $amount2d3m + $add;
	}
}

$amount2p6m = $amount2p6m / 6;
$amount2p6m = $amount2p6m / 1.21;
$amount2p6m = round($amount2p6m,2);
$amount2p3m = $amount2p3m / 3;
$amount2p3m = $amount2p3m / 1.21;
$amount2p3m = round($amount2p3m,2);
$amount2d3m = $amount2d3m / 3;
$amount2d3m = $amount2d3m / 1.21;
$amount2d3m = round($amount2d3m,2);

$ins2p6m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',10,2,".$amount2p6m.")";
$exe2p6m = mysql_query($ins2p6m);
$ins2p3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',4,2,".$amount2p3m.")";
$exe2p3m = mysql_query($ins2p3m);
$ins2d3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',2,2,".$amount2d3m.")";
$exe2d3m = mysql_query($ins2d3m);

//1 MONTHS AGO
$yr1 = $year;
$mo1 = $month - 1;
if($mo1 <= 0){
	$mo1 = $mo1 + 12;
	$yr1 = $yr1 - 1;
}
$sql1 = "SELECT * FROM t_dp_transaction WHERE TransTypeID = 6 AND YEAR(TransDateTime) = ".$yr1." AND MONTH(TransDateTime) = ".$mo1." AND Method <> 'ALLOPASS'";
$que1 = mysql_query($sql1);
$line1 = mysql_num_rows($que1);
$amount1p6m = 0;
$amount1p3m = 0;
$amount1d3m = 0;
$amount1p1m = 0;
$amount1d1m = 0;

for($i=0; $i<$line1; $i++){
	$subs = mysql_result($que1,$i,'TransProductID');
	$add = mysql_result($que1,$i,'TransEuro');
	if($subs == 10){
		$amount1p6m = $amount1p6m + $add;
	}
	elseif($subs == 4){
		$amount1p3m = $amount1p3m + $add;
	}
	elseif($subs == 2){
		$amount1d3m = $amount1d3m + $add;
	}
	elseif($subs == 3){
		$amount1p1m = $amount1p1m + $add;
	}
	elseif($subs == 1){
		$amount1d1m = $amount1d1m + $add;
	}
	else{
	}
}

$amount1p6m = $amount1p6m / 6;
$amount1p6m = $amount1p6m / 1.21;
$amount1p6m = round($amount1p6m,2);
$amount1p3m = $amount1p3m / 3;
$amount1p3m = $amount1p3m / 1.21;
$amount1p3m = round($amount1p3m,2);
$amount1d3m = $amount1d3m / 3;
$amount1d3m = $amount1d3m / 1.21;
$amount1d3m = round($amount1d3m,2);
$amount1p1m = $amount1p1m / 2;
$amount1p1m = $amount1p1m / 1.21;
$amount1p1m = round($amount1p1m,2);
$amount1d1m = $amount1d1m / 2;
$amount1d1m = $amount1d1m / 1.21;
$amount1d1m = round($amount1d1m,2);


$ins1p6m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',10,1,".$amount1p6m.")";
$exe1p6m = mysql_query($ins1p6m);
$ins1p3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',4,1,".$amount1p3m.")";
$exe1p3m = mysql_query($ins1p3m);
$ins1d3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',2,1,".$amount1d3m.")";
$exe1d3m = mysql_query($ins1d3m);
$ins1p1m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',3,1,".$amount1p1m.")";
$exe1p1m = mysql_query($ins1p1m);
$ins1d1m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',1,1,".$amount1d1m.")";
$exe1d1m = mysql_query($ins1d1m);

//0 MONTHS AGO
$yr0 = $year;
$mo0 = $month;

$sql0 = "SELECT * FROM t_dp_transaction WHERE TransTypeID = 6 AND YEAR(TransDateTime) = ".$yr0." AND MONTH(TransDateTime) = ".$mo0." AND Method <> 'ALLOPASS'";
$que0 = mysql_query($sql0);
$line0 = mysql_num_rows($que0);
$amount0p6m = 0;
$amount0p3m = 0;
$amount0d3m = 0;
$amount0p1m = 0;
$amount0d1m = 0;
$amount0d1w = 0;
$amount0pfl = 0;
$amount0dfl = 0;

for($i=0; $i<$line0; $i++){
	$subs = mysql_result($que0,$i,'TransProductID');
	$add = mysql_result($que0,$i,'TransEuro');
	$cre = mysql_result($que0,$i,'TransAmount');
	if($subs == 10){
		$amount0p6m = $amount0p6m + $add;
	}
	elseif($subs == 4){
		$amount0p3m = $amount0p3m + $add;
	}
	elseif($subs == 2){
		$amount0d3m = $amount0d3m + $add;
	}
	elseif($subs == 3){
		$amount0p1m = $amount0p1m + $add;
	}
	elseif($subs == 1){
		$amount0d1m = $amount0d1m + $add;
	}
	elseif($subs == 9){
		$amount0d1w = $amount0d1w + $add;
	}
	elseif($subs == 5){
		$amount0dfl = $amount0dfl + $cre;
	}
	elseif($subs == 6){
		$amount0pfl = $amount0pfl + $cre;
	}
	else{
	}
}

$amount0p6m = $amount0p6m / 12;
$amount0p6m = $amount0p6m / 1.21;
$amount0p6m = round($amount0p6m,2);
$amount0p3m = $amount0p3m / 6;
$amount0p3m = $amount0p3m / 1.21;
$amount0p3m = round($amount0p3m,2);
$amount0d3m = $amount0d3m / 6;
$amount0d3m = $amount0d3m / 1.21;
$amount0d3m = round($amount0d3m,2);
$amount0p1m = $amount0p1m / 2;
$amount0p1m = $amount0p1m / 1.21;
$amount0p1m = round($amount0p1m,2);
$amount0d1m = $amount0d1m / 2;
$amount0d1m = $amount0d1m / 1.21;
$amount0d1m = round($amount0d1m,2);
$amount0d1w = $amount0d1w / 1.21;
$amount0d1w = round($amount0d1w,2);
$amount0dfl = abs($amount0dfl) / 10;
$amount0dfl = $amount0dfl / 1.21;
$amount0dfl = round($amount0dfl,2);
$amount0pfl = abs($amount0pfl) / 10;
$amount0pfl = $amount0pfl / 1.21;
$amount0pfl = round($amount0pfl,2);


$ins0p6m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',10,0,".$amount0p6m.")";
$exe0p6m = mysql_query($ins0p6m);
$ins0p3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',4,0,".$amount0p3m.")";
$exe0p3m = mysql_query($ins0p3m);
$ins0d3m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',2,0,".$amount0d3m.")";
$exe0d3m = mysql_query($ins0d3m);
$ins0p1m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',3,0,".$amount0p1m.")";
$exe0p1m = mysql_query($ins0p1m);
$ins0d1m = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',1,0,".$amount0d1m.")";
$exe0d1m = mysql_query($ins0d1m);
$ins0d1w = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',9,0,".$amount0d1w.")";
$exe0d1w = mysql_query($ins0d1w);
$ins0dfl = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',5,0,".$amount0dfl.")";
$exe0dfl = mysql_query($ins0dfl);
$ins0pfl = "INSERT INTO ad_stat_rev VALUES (".$year.",".$month.",'ogo',6,0,".$amount0pfl.")";
$exe0pfl = mysql_query($ins0pfl);


$url = "rsupdate.php?year=".$year."&month=".$month;
header("Location: ".$url);

?>