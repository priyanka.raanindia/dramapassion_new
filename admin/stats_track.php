<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("top.php");


$date_1 = date('d-m-Y');
$date_2 = date('d-m-Y');

?>
<style>
#jrange input {
   width: 200px;
}

#jrange div {
   font-size: 9pt;
}

.date-range-selected > .ui-state-active,
.date-range-selected > .ui-state-default {
   background: none;
   background-color: lightsteelblue;
}
</style>
<script src="js/highcharts.js"></script>
<tr><td>
<div style="width:1000px;margin:auto;">
<h2>Stats track user</h2> 
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
<div class="wrapper">
   <div id="jrange" class="dates">
    <input value="<?php echo date('m/d/Y') ; ?>"/>
    <div></div>
   </div>
</div>

<div id="container2" style="width:100%; height:600px;"></div>
<div id="container" style="width:100%; height:600px;"></div>

</div>
</td>
</tr>
<script>
/*
$(document).ready(function(){
    $("#div_track").load("stats_track_time.php?time=<?php echo $time ; ?>");
});
*/
MonTableau = new Array();
$.getJSON('stats_track_time.php?date=<?php echo date('m/d/Y') ; ?>', function(data) {
	$.each(data, function(key, val) {
	    //items.push('<li id="' + key + '">' + key + ' - ' + val + '</li>');
	    MonTableau[key] = val;
	  });


}).done(function( json ) {
  //alert(MonTableau['index_5']);
  highchats2();
  // highchats();
})
.fail(function( jqxhr, textStatus, error ) {
  
});

function maj_graph(){
date_input = $('#jrange input').val();
$.getJSON('stats_track_time.php?date='+date_input, function(data) {
	$.each(data, function(key, val) {
	    //items.push('<li id="' + key + '">' + key + ' - ' + val + '</li>');
	    MonTableau[key] = val;
	  });


}).done(function( json ) {
  //alert(MonTableau['index_5']);
  highchats2();
  // highchats();
})
.fail(function( jqxhr, textStatus, error ) {
  
});	
}

function highchats(){
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Statistique de temps passer par page'
            },
            xAxis: {
                categories: ['Index', 'Drama', 'Epi', 'Catalogue', 'Premium','DL','DVD'],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nombre de requettes',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' '
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Inférieur à 5 sec',
                data: [MonTableau['index_5'], MonTableau['drama_5'], MonTableau['epi_5'], MonTableau['cata_5'], MonTableau['premium_5'],MonTableau['dl_5'],MonTableau['dvd_5']]
            }, {
                name: 'Entre 6 et 30 sec',
                data: [MonTableau['index_6_30'], MonTableau['drama_6_30'], MonTableau['epi_6_30'], MonTableau['cata_6_30'], MonTableau['premium_6_30'],MonTableau['dl_6_30'],MonTableau['dvd_6_30']]
            }, {
                name: 'Entre 31 sec et 5 min',
                data: [MonTableau['index_31_300'], MonTableau['drama_31_300'], MonTableau['epi_31_300'], MonTableau['cata_31_300'], MonTableau['premium_31_300'],MonTableau['dl_31_300'],MonTableau['dvd_31_300']]
            }, {
	            name: 'Supérieur a 5min',
	            data: [MonTableau['index_301'], MonTableau['drama_301'], MonTableau['epi_301'], MonTableau['cata_301'], MonTableau['premium_301'],MonTableau['dl_301'],MonTableau['dvd_301']]
            }
            ]
        });
    });
}
function highchats2(){
	$(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories = ['Total', 'Index', 'Drama', 'Epi', 'Catalogue','Premium','DL','DVD'],
            name = 'Statistique de temps',
            data = [{
                    y: MonTableau['tot_moy'],
                    color: colors[0],
                    drilldown: {
                        name: 'Total',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['tot_5'], MonTableau['tot_6_30'], MonTableau['tot_31_300'], MonTableau['tot_301']],
                        color: colors[0]
                    }
                }, {
                    y: MonTableau['index_moy'],
                    color: colors[1],
                    drilldown: {
                        name: 'Index',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['index_5'], MonTableau['index_6_30'], MonTableau['index_31_300'], MonTableau['index_301']],
                        color: colors[1]
                    }
                }, {
                    y: MonTableau['drama_moy'],
                    color: colors[2],
                    drilldown: {
                        name: 'Drama',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['drama_5'], MonTableau['drama_6_30'], MonTableau['drama_31_300'], MonTableau['drama_301']],
                        color: colors[2]
                    }
                }, {
                    y: MonTableau['epi_moy'],
                    color: colors[3],
                    drilldown: {
                        name: 'Epi',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['epi_5'], MonTableau['epi_6_30'], MonTableau['epi_31_300'], MonTableau['epi_301']],
                        color: colors[3]
                    }
                }, {
                    y: MonTableau['cata_moy'],
                    color: colors[4],
                    drilldown: {
                        name: 'Catalogue',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['cata_5'], MonTableau['cata_6_30'], MonTableau['cata_31_300'], MonTableau['cata_301']],
                        color: colors[4]
                    }
                },{
	               y: MonTableau['premium_moy'],
                    color: colors[5],
                    drilldown: {
                        name: 'Premium',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['premium_5'], MonTableau['premium_6_30'], MonTableau['premium_31_300'], MonTableau['premium_301']],
                        color: colors[5] 
                    }
                },{
	               y: MonTableau['dl_moy'],
                    color: colors[6],
                    drilldown: {
                        name: 'DL',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['dl_5'], MonTableau['dl_6_30'], MonTableau['dl_31_300'], MonTableau['dl_301']],
                        color: colors[6] 
                    }
                },{
	               y: MonTableau['dvd_moy'],
                    color: colors[7],
                    drilldown: {
                        name: 'DVD',
                        categories: ['Inférieur à 5 sec', 'Entre 6 et 30 sec', 'Entre 31 sec et 5 min', 'Supérieur a 5min'],
                        data: [MonTableau['dvd_5'], MonTableau['dvd_6_30'], MonTableau['dvd_31_300'], MonTableau['dvd_301']],
                        color: colors[7] 
                    }
                }
                ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Statistique de temps par page'
            },
            subtitle: {
                text: 'Click sur une colonne pour plus de détails'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Temps en min'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                    chart.yAxis[0].axisTitle.attr({
								        text: 'Nombre de requettes'
								    });
								    chart.setTitle({text: "Statistique de requettes par page"});
                                } else { // restore
                                    setChart(name, categories, data);
                                    chart.yAxis[0].axisTitle.attr({
								        text: 'Temps en min'
								    });
								    chart.setTitle({text: "Statistique de temps par page"});
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y ;
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +' </b><br/>';
                    if (point.drilldown) {
                        s += 'Click pour voir le nombre de requette pour : '+ point.category ;
                    } else {
                        s += 'Click retourner aux moyennes';
                    }
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: true
            }
        })
        .highcharts(); // return chart
    });
}
$.datepicker._defaults.onAfterUpdate = null;

var datepicker__updateDatepicker = $.datepicker._updateDatepicker;
$.datepicker._updateDatepicker = function( inst ) {
   datepicker__updateDatepicker.call( this, inst );

   var onAfterUpdate = this._get(inst, 'onAfterUpdate');
   if (onAfterUpdate)
      onAfterUpdate.apply((inst.input ? inst.input[0] : null),
         [(inst.input ? inst.input.val() : ''), inst]);
}


$(function() {

   var cur = -1, prv = -1;
   $('#jrange div')
      .datepicker({
            numberOfMonths: 3,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,

            beforeShowDay: function ( date ) {
                  return [true, ( (date.getTime() >= Math.min(prv, cur) && date.getTime() <= Math.max(prv, cur)) ? 'date-range-selected' : '')];
               },

            onSelect: function ( dateText, inst ) {
                  var d1, d2;

                  prv = cur;
                  cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
                  if ( prv == -1 || prv == cur ) {
                     prv = cur;
                     $('#jrange input').val( dateText );
                  } else {
                     d1 = $.datepicker.formatDate( 'mm/dd/yy', new Date(Math.min(prv,cur)), {} );
                     d2 = $.datepicker.formatDate( 'mm/dd/yy', new Date(Math.max(prv,cur)), {} );
                     $('#jrange input').val( d1+' - '+d2 );
                  }
               },

            onChangeMonthYear: function ( year, month, inst ) {
                  prv = cur = -1;
               },

            onAfterUpdate: function ( inst ) {
                  $('<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" data-handler="hide" data-event="click">Done</button>')
                     .appendTo($('#jrange div .ui-datepicker-buttonpane'))
                     .on('click', function () { 
                     $('#jrange div').hide(); 
                     maj_graph();
                     
                     
                     });
               }
         })
      .position({
            my: 'left top',
            at: 'left bottom',
            of: $('#jrange input')
         })
      .hide();

   $('#jrange input').on('focus', function (e) {
         var v = this.value,
             d;

         try {
            if ( v.indexOf(' - ') > -1 ) {
               d = v.split(' - ');

               prv = $.datepicker.parseDate( 'mm/dd/yy', d[0] ).getTime();
               cur = $.datepicker.parseDate( 'mm/dd/yy', d[1] ).getTime();

            } else if ( v.length > 0 ) {
               prv = cur = $.datepicker.parseDate( 'mm/dd/yy', v ).getTime();
            }
         } catch ( e ) {
            cur = prv = -1;
         }

         if ( cur > -1 )
            $('#jrange div').datepicker('setDate', new Date(cur));

         $('#jrange div').datepicker('refresh').show();
      });

});
</script>
