<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("../includes/functions.php");
require_once("top.php");
$gris = 0;
$dramaID = $_GET['id'];
$req_drama = "SELECT * FROM t_dp_drama WHERE DramaID =".$dramaID;
$sql_drama =mysql_query($req_drama);
$dramaTitle = mysql_result($sql_drama,0,'DramaTitle');
$dramaTitle2 = mysql_result($sql_drama,0,'DramaTitle2');
$dramaYear = mysql_result($sql_drama,0,'DramaYear');
$dramaEpi = mysql_result($sql_drama,0,'DramaEpisodes');
$dramaProd = mysql_result($sql_drama,0,'ProducerID');
$dramaLicensor = mysql_result($sql_drama,0,'LicensorID');
$dramaSyno = mysql_result($sql_drama,0,'DramaSynopsisFre');
$dramaStatus = mysql_result($sql_drama,0,'DramaTrailer');
$dramaStatus = mysql_result($sql_drama,0,'StatusID');
$dramaAdded = mysql_result($sql_drama,0,'DramaAdded');
$dramaRelease = mysql_result($sql_drama,0,'ReleaseDate');
$dramaShort = mysql_result($sql_drama,0,'DramaShortcut');
$dramaCate = mysql_result($sql_drama,0,'Categorie');
$dramaHD = mysql_result($sql_drama,0,'HD');
$info_drama = DramaInfo($dramaID);
$link= "drama/".$dramaID."/".$dramaTitle;
$link = dramaLinkClean($link);
$link = $http.$link;


$req_prod = "SELECT * FROM t_dp_producer WHERE ProducerID = ".$dramaProd;
$sql_prod = mysql_query($req_prod);
$dramaProd = mysql_result($sql_prod,0,'ProducerName');

$req_prod2 = "SELECT * FROM t_dp_producer";
$sql_prod2 = mysql_query($req_prod2);

$p = 0;
while($row_prod = mysql_fetch_array($sql_prod2)){
	$tab_prod[$p] = array("id" => $row_prod['ProducerID'], "nom" => $row_prod['ProducerName']);
	$p++;
}


$req_licensor = "SELECT * FROM t_dp_licensor WHERE LicensorID = ".$dramaLicensor;
$sql_licensor = mysql_query($req_licensor);
$dramaLicensor= mysql_result($sdl_licensor,0,'LicensorName');

$req_genre = "SELECT * FROM t_dp_dramagenrelink WHERE DramaID = ".$dramaID;
$sql_genre = mysql_query($req_genre);
$i=0;
$req_genre_all = "SELECT * FROM t_dp_genrevideo";
$sql_genre_all = mysql_query($req_genre_all);


while($row = mysql_fetch_array($sql_genre)){
	$req_nom = "SELECT * FROM t_dp_genrevideo WHERE GenreVideoID = ".$row['GenreVideoID'];
	$sql_nom = mysql_query($req_nom);
	
	$tab_genre[$i] = mysql_result($sql_nom,0,'GenreVideoDescFre');
	$i++ ;
}
foreach($tab_genre as $key => $value){
	$dramaGenre = $dramaGenre.$value.", ";
	
}

$req_actor = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID = ".$dramaID." ORDER BY Rank";
$sql_actor = mysql_query($req_actor);

$j=0;
while($row_actor = mysql_fetch_array($sql_actor)){

	$req_info_actor = "SELECT * FROM t_dp_actor WHERE ActorID = ".$row_actor['ActorID'];
	$sql_info_actor = mysql_query($req_info_actor);
	$name = mysql_result($sql_info_actor,0,'ActorName');
	$role = $row_actor['RoleName'];
	$rank = $row_actor['Rank'];
	$tab_actor[$j] = array("name" => $name , "role" => $role , "rank" => $rank);
	$j++;
}



?>
<tr><td>
<form action="modif_drama_action.php?id=<?php echo $dramaID ; ?>" method="post">
<div style="width:1000px;margin:auto;">
<h2 style="width:900px;float:left;"><?php echo $dramaTitle ; ?></h2>
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br /><input type="submit" value="Modifier" />
<h4>Info</h4>
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
<table>
	<tr>
		<td width="500" colspan="2">Général</td><td width="500" colspan="2">Info</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td width="250">Titre :</td><td width="250"><input type="text" style="width:250px;"name="titre" value="<?php echo $dramaTitle ; ?>" /></td>
		<td width="250">ID :</td><td width="250"><?php echo $dramaID ; ?></td>
	</tr>
	<tr>
		<td>Catégorie :</td><td><select name="cate"><?php 
		$tab_cate = array("com" => "Comédies / Comédies Romantique","drame" => "Drame", "act" => "Action / Thriller / Fantastique","hist" => "Historique");
		foreach($tab_cate as $cate_key => $cate_value){
			if($dramaCate == $cate_key){
				echo '<option value="'.$cate_key.'" SELECTED >'.$cate_value.'</option>';
			}else{
				echo '<option value="'.$cate_key.'" >'.$cate_value.'</option>';
			}
		
		}
		
		
		?></select></td>
		<td>Status :</td><td><input type="text" style="width:250px;"name="status" value="<?php echo $dramaStatus ; ?>" /></td>
	</tr>
	<tr>
		<td>Genre :</td><td><?php AffGenreDrama($dramaID) ; ?></td>
		<td>ReleaseDate :</td><td><input type="text" id="datepicker" style="width:250px;" name="release_date" value="<?php echo $dramaRelease ; ?>" /></td>
	</tr>
	<tr>
		<td>Titre Original :</td><td><input type="text" style="width:250px;"name="titre_org" value="<?php echo $dramaTitle2 ; ?>" /></td>
		<td>Ajouté le :</td><td><?php echo $dramaAdded ; ?></td>
	</tr>
	<tr>
		<td>Année de sortie :</td><td><input type="text" style="width:250px;"name="annee_sortie" value="<?php echo $dramaYear ; ?>" /></td>
		<td>Shortcut :</td><td><input type="text" style="width:250px;"name="shortcut" value="<?php echo $dramaShort ; ?>" /></td>
	</tr>
	<tr>
		<td>Producteur :</td><td><select name="produteur"><?php 
		foreach($tab_prod as $prod_key => $prod_value){
			if($prod_value['nom'] == $dramaProd){
				echo '<option value="'.$prod_value['id'].'" SELECTED >'.$prod_value['nom'].'</option>';
			}else{
				echo '<option value="'.$prod_value['id'].'" >'.$prod_value['nom'].'</option>';
			}
		
		}
		
		?></select></td>
		<td>HD :</td><td><input type="text" style="width:250px;"name="hd" value="<?php echo $dramaHD ; ?>" /></td>
	</tr>
	<tr>
		<td>Licensor :</td><td><?php echo $dramaLicensor ; ?></td>
		<td>Nb epis :</td><td><input type="text" style="width:250px;"name="nb_epi" value="<?php echo $dramaEpi ; ?>" /></td>
	</tr>
	<tr>
		<td>Link :</td><td><?php echo $link ; ?></td>
		<td></td>
	</tr>
	
</table><br />
<h4>Synopsis</h4>

<img src="<?php echo $http ; ?>images/ligne1000.jpg" />

<br />
<div>
<textarea name="synopsis" style="width:1000px;height:200px;">
<?php echo $dramaSyno ; ?>
</textarea>
</div>
<h4>Actor</h4>
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
<div>
<?php
foreach($tab_actor as $cle => $valeur){
	$acteur_img = $http.'content/actors/'.$valeur['name'].'.jpg';
	$acteur_img = str_replace(' ', '%20' ,$acteur_img);
	echo '<div style="width:331px;height:130px;float:left;border: solid 1px black;">';
	echo 'Nom : '.$valeur['name'];
	echo '<br />';
	echo 'Role : '.$valeur['role'];
	echo '<br />';
	echo 'Rank : '.$valeur['rank'];
	echo '<br />';
	echo '<img src="'.$acteur_img.'" />';
	echo '</div>';
 

}

?>
</div><div style="clear:both;"></div>
<br /><br />

<h4>Images</h4>
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
<div>
<h5>Big</h5>
<img src="<?php echo $info_drama['img_big'] ; ?>" /><br />
<h5>Detail</h5>
<img src="<?php echo $info_drama['img_detail'] ; ?>" /><br />
<h5>Thumb</h5>
<img src="<?php echo $info_drama['img_thumb'] ; ?>" /><br />

</div>
</div>
</form>
</td></tr>	
<script>
$(function() {
	var d = new Date();
	var n = d.getFullYear();
	var dataBack = n-80;
		$( "#datepicker" ).datepicker({
            changeYear: true,
			changeMonth: true,
			yearRange: dataBack+':'+n,
			
		});
		
	});
	
	jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
});
</script>
<?php
require_once("bottom.php");
?>