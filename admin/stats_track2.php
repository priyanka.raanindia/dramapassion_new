<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("top.php");


$date_1 = date('d-m-Y');
$date_2 = date('d-m-Y');

?>
<style>
#jrange input {
   width: 200px;
}

#jrange div {
   font-size: 9pt;
}

.date-range-selected > .ui-state-active,
.date-range-selected > .ui-state-default {
   background: none;
   background-color: lightsteelblue;
}
.spinner {
    position: fixed;
    top: 50%;
    left: 50%;
    margin-left: -50px; /* half width of the spinner gif */
    margin-top: -50px; /* half height of the spinner gif */
    text-align:center;
    z-index:1234;
    overflow: auto;
    width: 100px; /* width of the spinner gif */
    height: 102px; /*hight of the spinner gif +2px to fix IE8 issue */
}
</style>
<script src="js/highcharts.js"></script>
<tr><td>
<div style="width:1000px;margin:auto;">
<h2>Stats track user</h2> 
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />
<div class="wrapper">
   <div id="jrange" class="dates">
    <input value="<?php echo date('m/d/Y') ; ?>"/>
    <div></div>
   </div>
</div>
<div id="tot_user">nombre total d'utilisateur unique : </div>
<div><input type="radio" id="tout" name="groupe" checked>Tout les utilisateurs || <input type="radio" id="abo" name="groupe">Que les utilisateurs abonné || <input type="radio" id="pas_abo" name="groupe">Que les utilisateurs free</div>
<div id="container" style="width:100%; height:600px;"></div>
<div id="tot_video">nombre total de video : </div>
<div id="container2" style="width:100%; height:600px;"></div>


</div>
</td>
</tr>
<div id="spinner" class="spinner" style="display:none;">
    <img id="img-spinner" src="spinner.gif" alt="Loading"/>
</div>
<script>
/*
$(document).ready(function(){
    $("#div_track").load("stats_track_time.php?time=<?php echo $time ; ?>");
});
*/
$(document).ready(function(){
    $("#spinner").bind("ajaxSend", function() {
        $(this).show();
    }).bind("ajaxStop", function() {
        $(this).hide();
    }).bind("ajaxError", function() {
        $(this).hide();
    });
 
     });
MonTableau = new Array();
$.getJSON('stats_track_time2.php?date=<?php echo date('m/d/Y') ; ?>', function(data) {
	$.each(data, function(key, val) {
	    //items.push('<li id="' + key + '">' + key + ' - ' + val + '</li>');
	    MonTableau[key] = val;
	  });


}).done(function( json ) {
  //alert(MonTableau['index_5']);
  highchats2();
  highchats();
  $('#tot_user').html('nombre total d\'utilisateur unique : '+MonTableau['user_tot']);
  $('#tot_video').html('nombre total de video : '+MonTableau['video_tot']);
})
.fail(function( jqxhr, textStatus, error ) {
  
});

function maj_graph(){
date_input = $('#jrange input').val();
if($('#abo').is(':checked')){
		abo_url = '&abo=1';
	}
if($('#pas_abo').is(':checked')){
		abo_url = '&abo=2';
	}
if($('#tout').is(':checked')){
		abo_url = '&abo=0';
	}	
$.getJSON('stats_track_time2.php?date='+date_input+abo_url, function(data) {
	$.each(data, function(key, val) {
	    //items.push('<li id="' + key + '">' + key + ' - ' + val + '</li>');
	    MonTableau[key] = val;
	  });


}).done(function( json ) {
  //alert(MonTableau['index_5']);
  highchats2();
  highchats();
  $('#tot_user').html('nombre total d\'utilisateur unique : '+MonTableau['user_tot']);
  $('#tot_video').html('nombre total de video : '+MonTableau['video_tot']);
})
.fail(function( jqxhr, textStatus, error ) {
  
});	
}
$('#abo').change(function () {
	maj_graph();
});
$('#pas_abo').change(function () {
	maj_graph();
});
$('#tout').change(function () {
	maj_graph();
});
function highchats2(){
	$(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories= ['0s - 5s', '6s - 30s', '31s - 5m', '5m - 10m', '10m - 20m','20m - 30m','30m - 40m','40m - 50m',' > 50m'],
            data = [{
                    y: MonTableau['epi_5'],
                    color: colors[0],
                }, {
                    y: MonTableau['epi_6_30'],
                    color: colors[1],
                }, {
                    y: MonTableau['epi_31_300'],
                    color: colors[2],
                }, {
                    y: MonTableau['epi_301_10'],
                    color: colors[3],
                }, {
                    y: MonTableau['epi_10_20'],
                    color: colors[4],
                },{
	               y: MonTableau['epi_20_30'],
                    color: colors[5],
                },{
	               y: MonTableau['epi_30_40'],
                    color: colors[6],
                },{
	               y: MonTableau['epi_40_50'],
                    color: colors[7],
                },{
	               y: MonTableau['epi_50'],
                    color: colors[8],
                }
                ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'black'
			}, false);
			chart.redraw();
        }
    
        var chart = $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'min par user unique'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Nombre users'
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'black'
            }],
            exporting: {
                enabled: false
            }
        })
        .highcharts(); // return chart
    });
}
function highchats(){
	$(function () {
    
        var colors = Highcharts.getOptions().colors,
            categories= ['1','2','3','4','5','6','7','8', '9 - 16', '17 - 24', '25 - 32', '33 - 40','41 - 80','81 - 160','161 - 240',' > 241'],
            data = [{
                    y: MonTableau['nb_epi_1'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_2'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_3'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_4'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_5'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_6'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_7'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_8'],
                    color: colors[0],
                }, {
                    y: MonTableau['nb_epi_9'],
                    color: colors[1],
                }, {
                    y: MonTableau['nb_epi_10'],
                    color: colors[2],
                }, {
                    y: MonTableau['nb_epi_11'],
                    color: colors[3],
                }, {
                    y: MonTableau['nb_epi_12'],
                    color: colors[4],
                },{
	               y: MonTableau['nb_epi_13'],
                    color: colors[5],
                },{
	               y: MonTableau['nb_epi_14'],
                    color: colors[6],
                },{
	               y: MonTableau['nb_epi_15'],
                    color: colors[7],
                },{
	               y: MonTableau['nb_epi_16'],
                    color: colors[8],
                }
                ];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'black'
			}, false);
			chart.redraw();
        }
    
        var chart = $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Nombre de vue par users'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Nombre vue'
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'black'
            }],
            exporting: {
                enabled: false
            }
        })
        .highcharts(); // return chart
    });
}
$.datepicker._defaults.onAfterUpdate = null;

var datepicker__updateDatepicker = $.datepicker._updateDatepicker;
$.datepicker._updateDatepicker = function( inst ) {
   datepicker__updateDatepicker.call( this, inst );

   var onAfterUpdate = this._get(inst, 'onAfterUpdate');
   if (onAfterUpdate)
      onAfterUpdate.apply((inst.input ? inst.input[0] : null),
         [(inst.input ? inst.input.val() : ''), inst]);
}


$(function() {

   var cur = -1, prv = -1;
   $('#jrange div')
      .datepicker({
            numberOfMonths: 3,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,

            beforeShowDay: function ( date ) {
                  return [true, ( (date.getTime() >= Math.min(prv, cur) && date.getTime() <= Math.max(prv, cur)) ? 'date-range-selected' : '')];
               },

            onSelect: function ( dateText, inst ) {
                  var d1, d2;

                  prv = cur;
                  cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
                  if ( prv == -1 || prv == cur ) {
                     prv = cur;
                     $('#jrange input').val( dateText );
                  } else {
                     d1 = $.datepicker.formatDate( 'mm/dd/yy', new Date(Math.min(prv,cur)), {} );
                     d2 = $.datepicker.formatDate( 'mm/dd/yy', new Date(Math.max(prv,cur)), {} );
                     $('#jrange input').val( d1+' - '+d2 );
                  }
               },

            onChangeMonthYear: function ( year, month, inst ) {
                  prv = cur = -1;
               },

            onAfterUpdate: function ( inst ) {
                  $('<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" data-handler="hide" data-event="click">Done</button>')
                     .appendTo($('#jrange div .ui-datepicker-buttonpane'))
                     .on('click', function () { 
                     $('#jrange div').hide(); 
                     maj_graph();
                     
                     
                     });
               }
         })
      .position({
            my: 'left top',
            at: 'left bottom',
            of: $('#jrange input')
         })
      .hide();

   $('#jrange input').on('focus', function (e) {
         var v = this.value,
             d;

         try {
            if ( v.indexOf(' - ') > -1 ) {
               d = v.split(' - ');

               prv = $.datepicker.parseDate( 'mm/dd/yy', d[0] ).getTime();
               cur = $.datepicker.parseDate( 'mm/dd/yy', d[1] ).getTime();

            } else if ( v.length > 0 ) {
               prv = cur = $.datepicker.parseDate( 'mm/dd/yy', v ).getTime();
            }
         } catch ( e ) {
            cur = prv = -1;
         }

         if ( cur > -1 )
            $('#jrange div').datepicker('setDate', new Date(cur));

         $('#jrange div').datepicker('refresh').show();
      });

});
</script>
