<?php
require_once("includes/settings.inc.php");
require_once("includes/dbinfo.inc.php");
$req = "SELECT * FROM t_dp_episode AS e,t_dp_drama AS d WHERE e.DramaID = d.DramaID AND d.StatusID = 1 AND e.ReleaseDateFree != '0000-00-00' ORDER BY d.DramaID DESC";
$sql = mysql_query($req);
$crawler = 0;
if ( preg_match('/(bot|spider|yahoo)/i', $_SERVER[ "HTTP_USER_AGENT" ] )) $crawler = 1 ;

if($crawler == 1){
header("Content-Type: text/xml;charset=utf-8");
echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"> 
	    <?php
	    while($row=mysql_fetch_assoc($sql)){
	    	$time_epi = strtotime($row['ReleaseDateFree']);
	    	$time = time();
	    	if($time_epi <= $time || $row['EpisodeNumber'] == 1){
	    
	    	
	    	$drama_link = str_replace(' ', '-' ,$row['DramaTitle']);
	    	$drama_link = str_replace('\'','',$drama_link);
	    	$drama_link = str_replace(',','',$drama_link);
	    	$drama_link = str_replace('.','',$drama_link);
	    	$drama_link = str_replace('!','',$drama_link);
	    	$drama_link = str_replace('?','',$drama_link);
	    	$drama_link = str_replace('à','a',$drama_link);
	    	$drama_link = str_replace(':','',$drama_link);
	    	
	    	
	    	
	    	
	    	$lg_max = 2000; //nombre de caractère autoriser
	    	$chaine = $row['DramaSynopsisFre'];
	    	$chaine = str_replace('<br />','',$chaine);
	    	$chaine = '(Episode '.$row['EpisodeNumber'].') '.$chaine;
	    	//$chaine = str_replace('à','a',$chaine);
	    	//$chaine = str_replace('â','a',$chaine);
	    	//$chaine = str_replace('é','e',$chaine);
	    	//$chaine = str_replace('è','e',$chaine);
			if (strlen($chaine) > $lg_max){
				$chaine = substr($chaine, 0, $lg_max);
				$last_space = strrpos($chaine, " ");
				
			}
			$synopsis = $chaine;
	    	
	    	$epinum = $row['EpisodeNumber'];
	    	if($epinum < 10){
		    	$epinum = '0'.$epinum;
	    	}
	    	$time = $row['TimeEpi'];
	    	
	    	if($time <= 0){
		    	$time = 3000;
	    	}
	    	
	    	
	    	
			$shortcut = $row["DramaShortcut"];
			
	    	
	    	$url = "http://nf04.dramapassion.com/dcvideo/".$shortcut."/".$shortcut.$epinum."-fd1.mp4";
	    	
	    	
		   echo '<url>'."\n";
		   	echo '<loc>'.$http.'drama/'.$row['DramaID'].'/'.$drama_link.'/'.$epinum.'/</loc>'."\n";
		   	echo '<video:video>'."\n";
		   		echo '<video:thumbnail_loc>http://nf04.dramapassion.com/thumb/'.$row['DramaShortcut'].'/'.$row['DramaShortcut'].$epinum.'-1_thumb.jpg</video:thumbnail_loc>'."\n";
		   		echo '<video:title>'.$row['DramaTitle'].' épisode : '.$epinum.'</video:title>'."\n";
		   		echo '<video:description>'.$synopsis.'</video:description>'."\n";
		   		echo '<video:content_loc>'.$url.'</video:content_loc>'."\n";
		   		echo '<video:duration>'.$time.'</video:duration>'."\n";
		   		echo '<video:category>Drama, Kdrama, série corréenne</video:category>'."\n";
		   		echo '<video:publication_date>'.$row['ReleaseDateFree'].'</video:publication_date>'."\n";
		   	echo '</video:video>'."\n";
		   echo '</url>'."\n"; 
	    }
	    }
	    ?>
	    </urlset>
<?php } ?>