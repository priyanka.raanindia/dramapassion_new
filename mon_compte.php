<?php
	if(!session_id()) session_start();
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");	
	require_once("top.php");
	require_once("includes/fct_inc_mon_compte.php");
	$tab_user = InfoUser($_SESSION['userid']);
?>
<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:870px;margin:auto;">
		<div class="tab_min_height">
            <table id="Tableau_01" width="870" border="0" cellpadding="0" cellspacing="0"  >
                <tr>
                	<td width="870" valign="top">
                    <!-- CADRE DE GAUCHE -->
                    
                    <table width="870" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td width="175"><h1 class="menu_noir">Mon compte </h1></td>
                            <td width="35">&nbsp;</td>
							<td width="660"></td>
						</tr>
                        <tr>
                        	<td colspan="3"><img src="<?php echo $http ; ?>images/ligne870.jpg"></td>
						</tr> 
                        <tr>
                        	<td valign="top">
                            <center>
                            <br />
<a href="<?php echo $http ; ?>compte/abonnement/" class="lien_noir" style="text-transform:uppercase"><b>mon abonnement</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/parrainage/" class="lien_noir" style="text-transform:uppercase"><b>Mon parrainage</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/profil/" class="lien_noir" style="text-transform:uppercase"><b>Mon profil</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/playlist/" class="lien_noir" style="text-transform:uppercase"><b>Ma Playlist</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/paiements/" class="lien_noir" style="text-transform:uppercase"><b>Mes paiements</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/telechargements/" class="lien_noir" style="text-transform:uppercase"><b>Mes téléchargements</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
                            </center>
                            </td>
                        	<td></td>
                <td valign="top">
                           
					<table width="660" cellpadding="0" cellspacing="0" class="noir">                  
						<tr>
                        	<td></td>
                        	<td colspan="3"> 
                           <div style="margin-top:19px;">							
                           <h2 class="rose" style="text-transform:uppercase"><b>Gestion de profil</b></h2>                           
                            </div>                         
                                                        
                            </td>
						</tr> 
						<tr>
                        	<td colspan="4">
							<br>               
                            </td>                      
						</tr>                                                    
                        <tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="middle">Pseudo :</td>
                        	<td height="25" valign="middle"><?php echo htmlentities($tab_user['name']) ; ?></td>
                       	 	<td height="25" valign="middle" align="right"> </td>                        
					   </tr>  
                       <tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="middle">Mot de passe :</td>
                        	<td height="25" valign="middle">**************</td>
                       	 	<td height="25" valign="middle" align="right"><a href="<?php echo $http ; ?>modif_compte.php?mod=pass" class="iframe lien_fushia">Modifier</a></td>                        
					   </tr> 
                        <tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="middle">Adresse e-mail :</td>
                        	<td height="25" valign="middle"><?php echo htmlentities($tab_user['email']) ; ?></td>
                       	 	<td height="25" valign="middle" align="right"><a href="<?php echo $http ; ?>modif_compte.php?mod=mail" class="iframe lien_fushia">Modifier</a></td>                        
					   </tr>
					   <?php
					   
					   		if($tab_user['newsletter'] == 1){
						   		$newsletter_inscription = 'oui';
						   		$news_type = 1;
					   		}else{
						   		$newsletter_inscription = 'non';
						   		$news_type = 0;
					   		}
						   ?>
						<tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="middle">Inscription à la newsletter :</td>
                        	<td height="25" valign="middle"><?php echo $newsletter_inscription ; ?></td>
                       	 	<td height="25" valign="middle" align="right"><a href="<?php echo $http ; ?>modif_compte.php?mod=newsletter&news_type=<?php echo $news_type ; ?>" class="iframe lien_fushia">Modifier</a></td>                        
					   </tr>
						   <?php
					   
					   ?> 
                        <tr>
                        	<td colspan="4"><img src="<?php echo $http ; ?>images/ligne660.jpg"></td>
						</tr> 
                        <tr>
                        	<td colspan="4"><br></td>
						</tr>                        
                        <tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="middle">Date de naissance :</td>
                        	<td height="25" valign="middle"><?php echo $tab_user['annif'] ; ?></td>
                       	 	<td height="25" valign="middle" align="right"><a href="<?php echo $http ; ?>modif_compte.php?mod=annif" class="iframe lien_fushia">Modifier</a></td>                        
					   </tr>  
                       <tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="middle">Sexe :</td>
                        	<td height="25" valign="middle"><?php echo $tab_user['sexe'] ; ?></td>
                       	 	<td height="25" valign="middle" align="right"><a href="<?php echo $http ; ?>modif_compte.php?mod=sexe" class="iframe lien_fushia">Modifier</a></td>                        
					   </tr> 
                        <tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="middle">Pays :</td>
                        	<td height="25" valign="middle"><?php echo $tab_user['pays'] ; ?></td>
                       	 	<td height="25" valign="middle" align="right"><a href="<?php echo $http ; ?>modif_compte.php?mod=pays" class="iframe lien_fushia">Modifier</a></td>                        
					   </tr> 
                        <tr>
                        	<td colspan="4"><img src="<?php echo $http ; ?>images/ligne660.jpg"></td>
						</tr> 
                        <tr>
                        	<td colspan="4"><br></td>
						</tr>                        
                        <tr>
                       		<td valign="top">&nbsp;</td>
                        	<td height="25" valign="top">Adresse :</td>
                        	<td height="25" valign="top">
							<?php if($tab_user['addr1'] != ""){echo htmlentities($tab_user['addr1'])."<br />";} ?>
							<?php if($tab_user['addr2'] != ""){echo htmlentities($tab_user['addr2'])."<br />" ;} ?>
							<?php if($tab_user['zip'] != ""){echo htmlentities($tab_user['zip'])." " ;} ?>
							<?php if($tab_user['city'] != ""){ echo htmlentities($tab_user['city'])." " ;} ?>
							<?php if($tab_user['province'] != ""){echo htmlentities($tab_user['province'])."<br />" ;} ?>
							<?php if($tab_user['pays_fr'] != ""){echo htmlentities($tab_user['pays_fr']) ;} ?>
                            </td>
                       	 	<td height="25" valign="top" align="right"><a href="<?php echo $http ; ?>modif_compte.php?mod=addr" class="iframe lien_fushia">Modifier</a></td>                       
							</tr>                                                                     		                                                                                                  
					</table><br /><br /><br />                                                   
                </td>
						</tr>                                                                                                                                      
					</table>                                                                                                    
                    
                	</td>                                                             
                </tr>
			</table>
		</div>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>
    

<script>
$(".iframe").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		parent.location.reload(true);
	}
});
</script>
<?php require_once("bottom.php");
?>