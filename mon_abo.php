<?php
	if(!session_id()) session_start();
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");	
	require_once("top.php");
	require_once("includes/fct_inc_mon_compte.php");
	$tab_user = InfoUser($_SESSION['userid']);
	
	
	if(!isset($_SESSION['userid'])){
		?>
		<script>
		$(document).ready(function() {
        	$("#identifier").trigger('click');
        });
		</script>
		<?php
	}else{
	
	$date_time_expiry = strtotime($tab_user['expire']);
	$date_expiry = date('j-m-Y H:m' , $date_time_expiry);
	
	if($tab_user['autorenew'] == 1){
		$abo_renew = "Activé";
	}else{
		$abo_renew = "Désactivé";
	}
	$user_abo_fct = abo_user($_SESSION['userid']);
	
	$req_type_abo = "SELECT * FROM t_dp_subscription WHERE ordre > 0 ORDER BY ordre";
	$sql_type_abo = mysql_query($req_type_abo);

	$balance = mysql_result(mysql_query("SELECT UserBalance FROM t_dp_user WHERE UserID = ".$_SESSION['userid']),0,"UserBalance");
	
	$nb = 1;
	while($sql_row_type_abo=mysql_fetch_array($sql_type_abo)){
			$tab_abo[$nb] = $sql_row_type_abo['TitleFre'];
			$tab_abo_val[$nb] = $sql_row_type_abo['SubscriptionID'];
			$nb++;

	if($user_abo_fct == "decouverte"){
		if($sql_row_type_abo['SubscriptionID'] == $tab_user['subs']){
			$user_abo = $sql_row_type_abo['TitleFre'];
		}
	}elseif($user_abo_fct == "privilege"){
		if($sql_row_type_abo['SubscriptionID'] == $tab_user['subs']){
			$user_abo = $sql_row_type_abo['TitleFre'];
		}
	}else{
		$user_abo = "Vous n'avez pas d'abonnement actif";
	}
	}
	$user_next2 =  mysql_result(mysql_query("SELECT Next FROM t_dp_user WHERE UserID = ".$_SESSION['userid']),0,"Next");
	$user_next = mysql_result(mysql_query("SELECT * FROM t_dp_subscription WHERE SubscriptionID = ".$user_next2),0,"TitleFre");
?>
<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:870px;margin:auto;">
		<div class="tab_min_height">
            <table id="Tableau_01" width="870" border="0" cellpadding="0" cellspacing="0"  >
                <tr>
                	<td width="870" valign="top">
                    <!-- CADRE DE GAUCHE -->
                    
                    <table width="870" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td width="175"><h1 class="menu_noir">Mon compte </h1></td>
                            <td width="35">&nbsp;</td>
							<td width="660"></td>
						</tr>
                        <tr>
                        	<td colspan="3"><img src="<?php echo $http ; ?>images/ligne870.jpg"></td>
						</tr> 
                        <tr>
                        	<td valign="top">
                            <center>
                            <br />
<a href="<?php echo $http ; ?>compte/abonnement/" class="lien_noir" style="text-transform:uppercase"><b>mon abonnement</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/parrainage/" class="lien_noir" style="text-transform:uppercase"><b>Mon parrainage</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/profil/" class="lien_noir" style="text-transform:uppercase"><b>Mon profil</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/playlist/" class="lien_noir" style="text-transform:uppercase"><b>Ma Playlist</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/paiements/" class="lien_noir" style="text-transform:uppercase"><b>Mes paiements</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
<a href="<?php echo $http ; ?>compte/telechargements/" class="lien_noir" style="text-transform:uppercase"><b>Mes téléchargements</b></a><br />
<div style="padding-top:5px;"></div>
<img src="<?php echo $http ; ?>images/pics030.png">
<br />
</center>
                            </td>
                        	<td></td>
                <td valign="top">
                           
					<table width="660" cellpadding="0" cellspacing="0" class="noir">                  
						<tr>
                        	<td >      
							<div style="margin-top:19px;">							
                            <h2 class="rose" style="text-transform:uppercase;"><b>Gestion de mon abonnement</b></h2>                              
                            </div>
							</td>
						</tr>
						<tr height="30px">
							<td>
							Votre abonnement actuel : <?php echo $user_abo ; ?>
							</td>
						</tr>
						<?php if($user_abo_fct == "decouverte" || $user_abo_fct == "privilege"){ ?>
						<tr height="30px">
							<td>
							Date d'expiration de votre abonnement : <?php echo $date_expiry ; ?>
							</td>
						</tr>
						<tr height="30px">
							<td>
							Renouvellement automatique : <?php echo $abo_renew ; ?>
							</td>
						</tr>
						<!-- CONVERSION CREDITS -->
						<?php }
						/*if($balance > 0){
							if($user_abo_fct == "decouverte" || $user_abo_fct == "privilege"){
								if ($user_abo_fct == "decouverte"){
									$days = $balance * 1 / 3;
								} else {
									$days = $balance * 1 / 5;
								}
								$days = round($days,1);
						?>
						<!-- <tr height="100px">
							<td>
							Information : Vous avez encore <?php echo $balance; ?> crédits disponibles.<br>
							Vous pouvez les convertir pour prolonger votre abonnement de <?php echo $days; ?> jours.<br><br>
							<FORM METHOD="post" ACTION="<?php echo $http."prolongaction"; ?>" id="prolong">
							<input type="hidden" name="subs" value="<?php echo $tab_user['subs']; ?>" >
							<input type="hidden" name="balance" value="<?php echo $balance; ?>" >
							<input type="hidden" name="days" value="<?php echo $days ; ?>" >
							<input type="image" src="<?php echo $http ; ?>images/conv_credit.png" height="30" name="orderconfirm">
							</FORM>
							</td>
						</tr> -->								
						<?php } else { ?>
						<!-- <tr height="100px">
							<td>
							Information : Vous avez encore <?php echo $balance; ?> crédits disponibles.<br>							
							Vous pouvez les convertir à tout moment en jours d'abonnement.<br><br>
							<FORM METHOD="post" ACTION="<?php echo $http."payment"; ?>" id="conversion">
							<input type="hidden" name="subs" value="1" >
							<input type="submit" value="Convertir mes crédits maintenant" name="orderconfirm">
							</FORM>
							</td>
						</tr>	-->							
						<?php }} */ ?>

						<!-- UPGRADE PRIVILEGE -->
						<?php
						if($user_abo_fct == "decouverte"){
						$enddate = mysql_result(mysql_query("SELECT Expiry FROM t_dp_user WHERE UserID = ".$_SESSION['userid'].""),0,"Expiry");				

						if($tab_user['subs'] == 9){
							$startdate = strtotime(date("Y-m-d H:i:s", strtotime($enddate)) . " -7 days");
						} elseif ($tab_user['subs'] == 1){
							$startdate = strtotime(date("Y-m-d H:i:s", strtotime($enddate)) . " -1 month");
						} elseif ($tab_user['subs'] == 2){
							$startdate = strtotime(date("Y-m-d H:i:s", strtotime($enddate)) . " -3 month");
						} else {
							$startdate = strtotime($enddate);
						}
						
						$dategap = strtotime($enddate) - strtotime(date("Y-m-d H:i:s"));
						$datefull = strtotime($enddate) - $startdate;
						
						$curprice = 0;				
						if ($tab_user['subs'] == 5){
							$curprice = mysql_result(mysql_query("SELECT AmountEur FROM t_dp_subscription WHERE SubscriptionID = 9"),0,"AmountEur");	
						} else {
							$curprice = mysql_result(mysql_query("SELECT AmountEur FROM t_dp_subscription WHERE SubscriptionID = ".$tab_user['subs'].""),0,"AmountEur");	
						}
						
						$discount = round($curprice * $dategap / $datefull, 2) ;
						$price1mo = mysql_result(mysql_query("SELECT AmountEur FROM t_dp_subscription WHERE SubscriptionID = 3"),0,"AmountEur") - $discount;
						$price3mo = mysql_result(mysql_query("SELECT AmountEur FROM t_dp_subscription WHERE SubscriptionID = 4"),0,"AmountEur") - $discount;
						$price6mo = mysql_result(mysql_query("SELECT AmountEur FROM t_dp_subscription WHERE SubscriptionID = 10"),0,"AmountEur") - $discount;					
					
						/*	
						$gapdays = $dategap / (60*60*24) ;
						$fulldays = $datefull / (60*60*24) ;						
						echo date("Y-m-d H:i:s", $startdate)."<br>";
						echo $enddate."<br>";
						echo "gap = ".$gapdays."<br>";
						echo "full = ".$fulldays."<br>";
						echo "curprice = ".$curprice."<br>";
						echo "discount = ".$discount ;						
						*/
						?>
						<?php
						if($price1mo >= 1){
						?>
                        <tr>
                        	<td><img src="<?php echo $http ; ?>images/ligne660.jpg"></td>
						</tr> 			
						<tr height="30px">
							<td><b style="font-size:13px;"><br>Souhaitez-vous passer en abonnement Privilège immédiatement ?<br/>
							Choisissez le type d'abonnement souhaité.</b><br>
							Les prix tiennent compte des jours non utilisés de votre abonnement Découverte en cours.<br>
							La durée de l'abonnement est réinitialisée à partir de la date de l'upgrade.<br><br>
							<FORM METHOD="POST" ACTION="<?php echo $http."payment"; ?>" id="upgrade">
							<input type="radio" name="subs" value="3" checked="checked">&nbsp;&nbsp;Abonnement Privilège (1 mois) : <?php echo $price1mo; ?> euros.<br><br>
							<!-- <input type="radio" name="subs" value="4">&nbsp;&nbsp;Abonnement Privilège (3 mois) : <?php echo $price3mo; ?> euros.<br><br>
							<input type="radio" name="subs" value="10">&nbsp;&nbsp;Abonnement Privilège (6 mois) : <?php echo $price6mo; ?> euros.<br><br> -->
							<input type="hidden" name="discount" value="<?php echo $discount; ?>" >
							<input type="image" src="<?php echo $http ; ?>images/pics027.png">
							</FORM>
							</td>
						</tr>
						<?php } } ?>
					
						<!-- MODIFICATION ABONNEMENT -->
						<?php
						if($tab_user['autorenew'] == 1 && ($user_abo_fct == "decouverte" || $user_abo_fct == "privilege")){
						?>
                        <tr>
                        	<td><br /><img src="<?php echo $http ; ?>images/ligne660.jpg"></td>
						</tr> 			
						<tr><td><b style="font-size:13px;"><br>Souhaitez-vous changer votre abonnement au prochain renouvellement ?<br/>
						Si oui, choisissez le type d'abonnement souhaité :</b><br><br>
						L'option affichée représente votre choix actuel.<br><br>
						
						<form method="post" action="<?php echo $http ; ?>modif_abo.php">
						<?php 
						$type = "tabAbo";
						echo'<div class="all_input" onclick="showHideSelect(\'selectID_'.$type.'\')" style="float:left;">';
							echo'<div class="inputsSelect"   >';
								$type_lien = "lien_".$type;
								echo'<p class="selects"  id="select_'.$type.'" >'.$user_next.'</p>';
								echo'<ul id="selectID_'.$type.'"  >';
									foreach($tab_abo as $key => $value){
										if ($tab_abo_val[$key] == 1 || $tab_abo_val[$key] == 3){
										echo'<li><a href="javascript:void(0)"  onclick="validAndHide(\''.$tab_abo_val[$key].'\', this, \''.$type_lien.'\', \'select_'.$type.'\')">'.$value.'</a></li>';
										}
									}
		
								echo'</ul>';
		
							echo'</div><img src="'.$http.'images/submit_liste.png" style="cursor:pointer;">';
							echo'<input type="hidden" name="'.$type_lien.'" id="'.$type_lien.'" />';
						echo'</div>';
						?>
						
						<input type="image" src="<?php echo $http ; ?>images/pics028.png" style="float:left;margin-left: 10px;" height="27px" /></td>
						</form>
						</tr>
                        <tr height="0">
                        	<td>&nbsp;</td>
						</tr> 			
						<!-- DESACTIVER RENOUVELLEMENT AUTOMATIQUE -->
                        <tr>
                        	<td><img src="<?php echo $http ; ?>images/ligne660.jpg"></td>
						</tr> 			
						<tr height="30px">
							<td><br /><b style="font-size:13px;">Souhaitez-vous désactiver le renouvellement automatique ?</b><br/><br />
							<FORM METHOD="post" ACTION="<?php echo $http."modif_abo"; ?>" id="conversion">
							<input type="hidden" name="desactiver" value="1" >
							<input type="image" src="<?php echo $http ; ?>images/pics029.png" />
							</FORM>							
							</td>
						</tr>
						<?php 
						}
						
						
							?>
							<tr height="0">
	                        	<td>&nbsp;</td>
							</tr> 	
								
							<!-- Code Abo -->
	                        <tr>
	                        	<td><img src="<?php echo $http ; ?>images/ligne660.jpg"></td>
							</tr> 			
							<tr height="30px">
								<td><br /><b style="font-size:13px;">Code Promo</b><br/><br />
								<FORM METHOD="post" ACTION="<?php echo $http."code_promo.php"; ?>">
								<input type="text" name="codeAbo" id="codeAbo" />
								<input type="submit" value="Valider" />
								</FORM>							
								</td>
							</tr>
							<?php
							if($_GET['erreur'] == 1){
								?>
								<tr height="30px">
									<td><br /><b style="font-size:13px;color: red">Le code introduit n'existe pas. </b><br/><br />						
									</td>
								</tr>
								<?php
							}else if($_GET['erreur'] == 2){
								?>
								<tr height="30px">
									<td><br /><b style="font-size:13px;color: red">Le code introduit à déjà été utilisé. </b><br/><br />						
									</td>
								</tr>
								<?php
							}else if($_GET['erreur'] == 3){
								?>
								<tr height="30px">
									<td><br /><b style="font-size:13px;color: red">Le code introduit à expiré. </b><br/><br />						
									</td>
								</tr>
								<?php
							}
						
						?>
						
						<tr height="50px">
							<td>&nbsp;
							</td>
						</tr>
					</table>                                                    
                </td>
						</tr>                                                                                                                                      
					</table>                                                                                                    
                    
                	</td>                                                             
                </tr>
			</table>
		</center>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>
<script>
function showHideSelect(select)
{
    var objSelect = document.getElementById(select);
    objSelect.style.display = (objSelect.style.display == 'block') ? 'none' : 'block';
}

function validAndHide(txt, obj, input, select)
{
    
	document.getElementById(input).value = txt;
	var parent = obj.parentNode.parentNode;
	document.getElementById(select).innerHTML = obj.innerHTML;
	showHideSelect(parent);
	
}

</script>			
<?php 
}
require_once("bottom.php"); ?>