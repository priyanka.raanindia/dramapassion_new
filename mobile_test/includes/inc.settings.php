<?php
define("_imgslide_","3000");
define("_imgslidespeed_","800");
define("_newsslide_","7000");
define("_newsslidespeed_","800");
//define('YOUR_APP_ID', '336589259687274'); //Facebook app ID
define('YOUR_APP_ID', '344619609607'); //Facebook app ID
define('YOUR_SECRET_ID', '8bacb45587099cc2873772a53b270225');

$emailfrom = "noreply@dramapassion.com";
$supportemail = "support@dramapassion.com";

$ssl = "https://www.dramapassion.com/secure/";
$http = "http://www.dramapassion.com/mobile_test/";
$http2 = "http://www.dramapassion.com";
$http3 = "www.dramapassion.com/";
$https = "https://www.dramapassion.com/";
$server_free1 = "http://nf01.dramapassion.com/";
$server_free2 = "http://nf02.dramapassion.com/";
$server_hd1 = "http://np01.dramapassion.com:8080/";
$server_hd2 = "http://np02.dramapassion.com:8080/";
$server_hd1_admin = "http://np01.dramapassion.com:8080/";
$server_hd2_admin = "http://np02.dramapassion.com:8080/";

//$server_wowza1 = "http://ns01.dramapassion.com/";
//$server_wowza1_rtmpt = "ns01.dramapassion.com/";
//$server_wowza2 = "http://ns04.dramapassion.com/";
//$server_wowza2_rtmpt = "ns04.dramapassion.com/";

$server = "http://www.dramapassion.com/";
$images = $server."images/";
$css = $server."css/";
$js = $server."js";
$includes = $server."includes/";
$content = $server."content/";
/*
	$rand = rand(1,10);
	if($rand <= 5){
		$server_free = $server_free1 ;
	}else{
		$server_free = $server_free2 ;
	}
	$rand = rand(1,10);
	if($rand <= 5){
		$server_hd = $server_hd1 ;
	}else{
		$server_hd = $server_hd2 ;
	}
	$rand = rand(1,10);
	if($rand <= 5){	
		$server_wowza = $server_wowza1;
		$server_wowza_rtmpt = $server_wowza1_rtmpt;
	}else{
		$server_wowza = $server_wowza2;
		$server_wowza_rtmpt = $server_wowza2_rtmpt;
	}
*/
//Subscriptions
$array_dis = array("1","2","5","7","8","9");
$array_pri = array("3","4","6","10");
//English countrycodes
$array_en = array("UK","GB","NL","BE","LU","FR","YT","RE","MQ","PF","CH","GP","NC","MF","GF","SX","MU");
//French countrycodes
$array_fr = array("UK","GB","NL","BE","LU","FR","YT","RE","MQ","PF","CH","GP","NC","MF","GF","SX","MU");
//User exception list (UserID)
$array_id = array("2009","2011","4508","4510","11261","12213","14449","14957","235","453","10071","86","22697","1598");

//jour fr
$array_jour_fr = array(	"Monday" => "Lundi",
						"Tuesday" => "Mardi",
						"Wednesday" => "Mercredi",
						"Thursday" => "Jeudi",
						"Friday" => "Vendredi",
						"Saturday" => "Samedi" ,
						"Sunday" => "Dimanche");
$array_jour_fr_p = array(	"Monday" => "Lundis",
						"Tuesday" => "Mardis",
						"Wednesday" => "Mercredis",
						"Thursday" => "Jeudis",
						"Friday" => "Vendredis",
						"Saturday" => "Samedis" ,
						"Sunday" => "Dimanches");
$array_mois_fr = array(	"January" => "Janvier",
						"February" => "Février",
						"March" => "Mars",
						"April" => "Avril",
						"May" => "Mai",
						"June" => "Juin",
						"July" => "Juillet",
						"August" => "Août",
						"September" => "Septembre",
						"October" => "Octobre",
						"November" => "Novembre",
						"December" => "Décembre");
?>