<?php
require_once('includes/db_settings.php');
require_once('includes/inc.settings.php');
require_once('includes/inc.general.php');
require_once('includes/inc.index.php');

require_once('template/top.php');

$dramaID = $_GET['dramaID'];
$drama = DramaInfo($dramaID);

?>
<!-- page -->
<div class="container" >
	<div class="col-lg-12">
		<div class="center_drama_top">
			<div class="drama_detail">
				<h3><?php echo $drama['titre'] ; ?></h3>
				<h6><?php echo $drama['nb_epi'] ; ?> épisodes</h6>
				
			</div>
			<img src="<?php echo $drama['img_big'] ; ?>" alt="" class="img_drama_border" />
		</div>
	</div>
</div>
<!-- page -->
<?php require_once('template/footer.php') ; ?>
<script>

</script>
    
  </body>
</html>