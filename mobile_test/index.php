<?php
require_once('includes/db_settings.php');
require_once('includes/inc.settings.php');
require_once('includes/inc.general.php');
require_once('includes/inc.index.php');

require_once('template/top.php');

?>

	<!-- slider -->
		<?php Slider() ; ?>
	<!-- slider -->
	<!-- pub abo -->
	<div class="container" style="margin-top: 35px !important;">
	<div class="row">
		<div class="col-lg-12 ">
			<div class="img_all_center shadow">
				<img src="img/pub_750par90.jpg" alt="pub abo" class="img-responsive center-block img_all_center_img" />
			</div>
		</div>
	</div>
	</div>
	<!-- pub abo -->
	<!-- page -->
	<?php
	
	$new_epi_free = New_epi_drama("Free");
	$new_epi_pay = New_epi_drama("Pay");
	
	
	$new_epi_free_1 = DramaInfo($new_epi_free[0]['dramaID']);
	$new_epi_free_2 = DramaInfo($new_epi_free[1]['dramaID']);
	$new_epi_free_3 = DramaInfo($new_epi_free[2]['dramaID']);
	
	$new_epi_pay_1 = DramaInfo($new_epi_pay[0]['dramaID']);
	$new_epi_pay_2 = DramaInfo($new_epi_pay[1]['dramaID']);
	$new_epi_pay_3 = DramaInfo($new_epi_pay[2]['dramaID']);
	
	
	
	$com = CatalogueAff('CataCom') ;
	$melo = CatalogueAff('CataMelo');
	$act = CatalogueAff('CataAct');
	$hist = CatalogueAff('CataHist');
	
	$com1 = DramaInfo($com[1]);
	$com2 = DramaInfo($com[2]);
	$com3 = DramaInfo($com[3]);
	
	$melo1 = DramaInfo($melo[1]);
	$melo2 = DramaInfo($melo[2]);
	$melo3 = DramaInfo($melo[3]);
	
	$act1 = DramaInfo($act[1]);
	$act2 = DramaInfo($act[2]);
	$act3 = DramaInfo($act[3]);
	
	$hist1 = DramaInfo($hist[1]);
	$hist2 = DramaInfo($hist[2]);
	$hist3 = DramaInfo($hist[3]);
	

	
	?>
	<div class="container" style="margin-top: 35px !important;">
		<div class="row">
				<div class="col-lg-9">
					<h3 class="souligner h3_title">Dernières nouveautés</h3>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="row">
								<div class="col-xs-3 visible-xs"></div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
									<img src="img/abonnement_1.png" alt="" class="img-responsive center-block img_index_nouv_titre" />
									<img src="<?php echo $new_epi_pay_1['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow" />
									<div class="titre_miniature mini-titre-xs"><a href="<?php echo $new_epi_pay_1['link'] ; ?>" class="lien_menu_noir"><?php echo $new_epi_pay_1['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $new_epi_pay_1['img_thumb'] ; ?>" data-modalid="<?php echo $new_epi_pay_1['id'] ; ?>" data-modaltitle="<?php echo $new_epi_pay_1['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
									<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $new_epi_pay_1['logo_sortie'] ; ?>" /> Épisodes <?php echo $new_epi_pay[0]['epi_1'].' - '.$new_epi_pay[0]['epi_2'] ; ?></div>
									<p class="noir justify mini_height hidden-xs"><?php echo $new_epi_pay_1['synopsis_mini'] ; ?></p>
								</div>
								<div class="col-xs-3 visible-xs"></div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<img src="<?php echo $new_epi_pay_2['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
									<img src="<?php echo $new_epi_pay_2['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
									<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $new_epi_pay_2['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $new_epi_pay_2['img_thumb'] ; ?>" data-modalid="<?php echo $new_epi_pay_2['id'] ; ?>" data-modaltitle="<?php echo $new_epi_pay_2['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
									<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $new_epi_pay_2['logo_sortie'] ; ?>" /> Épisodes <?php echo $new_epi_pay[1]['epi_1'].' - '.$new_epi_pay[1]['epi_2'] ; ?></div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<img src="<?php echo $new_epi_pay_3['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
									<img src="<?php echo $new_epi_pay_3['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
									<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $new_epi_pay_3['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $new_epi_pay_3['img_thumb'] ; ?>" data-modalid="<?php echo $new_epi_pay_3['id'] ; ?>" data-modaltitle="<?php echo $new_epi_pay_3['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
									<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $new_epi_pay_3['logo_sortie'] ; ?>" /> Épisodes <?php echo $new_epi_pay[2]['epi_1'].' - '.$new_epi_pay[2]['epi_2'] ; ?></div>
								</div>
							</div>
						</div>
						<div class="col-xs-2 visible-xs"></div>
						<div class="col-xs-8 visible-xs"><div class="souligner separateur_nouv "></div></div>
						<div class="col-xs-2 visible-xs"></div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="row">
								<div class="col-xs-3 visible-xs"></div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
									<img src="img/abonnement_2.png" alt="" class="img-responsive center-block img_index_nouv_titre" />
									<img src="<?php echo $new_epi_free_1['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow" />
									<div class="titre_miniature mini-titre-xs"><a href="#" class="lien_menu_noir"><?php echo $new_epi_free_1['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $new_epi_free_1['img_thumb'] ; ?>" data-modalid="<?php echo $new_epi_free_1['id'] ; ?>" data-modaltitle="<?php echo $new_epi_free_1['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
									<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $new_epi_free_1['logo_sortie'] ; ?>" /> Épisodes <?php echo $new_epi_free[0]['epi_1'].' - '.$new_epi_free[0]['epi_2'] ; ?></div>
									<p class="noir justify mini_height hidden-xs"><?php echo $new_epi_free_1['synopsis_mini'] ; ?></p>
								</div>
								<div class="col-xs-3 visible-xs"></div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<img src="<?php echo $new_epi_free_2['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
									<img src="<?php echo $new_epi_free_2['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
									<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $new_epi_free_2['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $new_epi_free_2['img_thumb'] ; ?>" data-modalid="<?php echo $new_epi_free_2['id'] ; ?>" data-modaltitle="<?php echo $new_epi_free_2['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
									<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $new_epi_free_2['logo_sortie'] ; ?>" /> Épisodes <?php echo $new_epi_free[1]['epi_1'].' - '.$new_epi_free[1]['epi_2'] ; ?></div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<img src="<?php echo $new_epi_free_3['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
									<img src="<?php echo $new_epi_free_3['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
									<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $new_epi_free_3['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $new_epi_free_3['img_thumb'] ; ?>" data-modalid="<?php echo $new_epi_free_3['id'] ; ?>" data-modaltitle="<?php echo $new_epi_free_3['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
									<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $new_epi_free_3['logo_sortie'] ; ?>" /> Épisodes <?php echo $new_epi_free[2]['epi_1'].' - '.$new_epi_free[2]['epi_2'] ; ?></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row hidden-lg">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 separateur_index_mini"></div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
					</div>
					<div class="row hidden-lg" style="margin-top:40px !important;">
						<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
							<h3 class="souligner h3_title">Top 10</h3>
							<div class="row" style="margin-top:10px;">
								<?php AffTop10() ; ?>
							</div>
						</div>
						<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
							<h3 class="souligner h3_title">Prochainement</h3>
							<div class="row">
								<?php comming_soon() ; ?>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:40px !important;">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h3 class="souligner h3_title">Catalogue VOD<span class="pull-right cata_index"><a href="#" class="lien_menu_noir">voir tout le catalogue <span class="glyphicon glyphicon-film"></span></span></a></h3>
								<div class="row padding-interne">
									<h4 class="h3_title">Comédie / Comédie Romantique<span class="pull-right cata_index"><a href="#" class="lien_menu_noir">voir tout <span class="glyphicon glyphicon-film"></span></span></a></h4>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $com1['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $com1['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $com1['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $com1['img_thumb'] ; ?>" data-modalid="<?php echo $com1['id'] ; ?>" data-modaltitle="<?php echo $com1['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $com1['logo_sortie'] ; ?>" /> <?php echo $com1['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $com2['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $com2['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $com2['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $com2['img_thumb'] ; ?>" data-modalid="<?php echo $com2['id'] ; ?>" data-modaltitle="<?php echo $com2['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $com2['logo_sortie'] ; ?>" /> <?php echo $com2['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="visible-xs" style="clear:both;"></div>
									<div class="col-xs-3 visible-xs"></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $com3['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $com3['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $com3['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $com3['img_thumb'] ; ?>" data-modalid="<?php echo $com3['id'] ; ?>" data-modaltitle="<?php echo $com3['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $com3['logo_sortie'] ; ?>" /> <?php echo $com3['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-xs-3 visible-xs"></div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 separateur_index_mini"></div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
								</div>
								<div class="row padding-interne">
									<h4 class="h3_title">Drame<span class="pull-right cata_index"><a href="#" class="lien_menu_noir">voir tout <span class="glyphicon glyphicon-film"></span></span></a></h4>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $melo1['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $melo1['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $melo1['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $melo1['img_thumb'] ; ?>" data-modalid="<?php echo $melo1['id'] ; ?>" data-modaltitle="<?php echo $melo1['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $melo1['logo_sortie'] ; ?>" /> <?php echo $melo1['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $melo2['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $melo2['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $melo2['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $melo2['img_thumb'] ; ?>" data-modalid="<?php echo $melo3['id'] ; ?>" data-modaltitle="<?php echo $melo2['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $melo2['logo_sortie'] ; ?>" /> <?php echo $melo2['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="visible-xs" style="clear:both;"></div>
									<div class="col-xs-3 visible-xs"></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $melo3['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $melo3['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $melo3['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $melo3['img_thumb'] ; ?>" data-modalid="<?php echo $melo3['id'] ; ?>" data-modaltitle="<?php echo $melo3['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $melo3['logo_sortie'] ; ?>" /> <?php echo $melo3['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-xs-3 visible-xs"></div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 separateur_index_mini"></div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
								</div>
								<div class="row padding-interne">
									<h4 class="h3_title">Historique<span class="pull-right cata_index"><a href="#" class="lien_menu_noir">voir tout <span class="glyphicon glyphicon-film"></span></span></a></h4>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $hist1['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $hist1['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $hist1['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $hist1['img_thumb'] ; ?>" data-modalid="<?php echo $hist1['id'] ; ?>" data-modaltitle="<?php echo $hist1['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $hist1['logo_sortie'] ; ?>" /> <?php echo $hist1['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $hist2['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $hist2['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $hist2['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $hist2['img_thumb'] ; ?>" data-modalid="<?php echo $hist2['id'] ; ?>" data-modaltitle="<?php echo $hist2['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $hist2['logo_sortie'] ; ?>" /> <?php echo $hist2['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="visible-xs" style="clear:both;"></div>
									<div class="col-xs-3 visible-xs"></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $hist3['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $hist3['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $hist3['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $hist3['img_thumb'] ; ?>" data-modalid="<?php echo $hist3['id'] ; ?>" data-modaltitle="<?php echo $hist3['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $hist3['logo_sortie'] ; ?>" /> <?php echo $hist3['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-xs-3 visible-xs"></div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 separateur_index_mini"></div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
								</div>
								<div class="row padding-interne">
									<h4 class="h3_title">Action / Thriller / Fantastique<span class="pull-right cata_index"><a href="#" class="lien_menu_noir">voir tout <span class="glyphicon glyphicon-film"></span></span></a></h4>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $act1['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $act1['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $act1['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $act1['img_thumb'] ; ?>" data-modalid="<?php echo $act1['id'] ; ?>" data-modaltitle="<?php echo $act1['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $act1['logo_sortie'] ; ?>" /> <?php echo $act1['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $act2['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $act2['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $act2['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $act2['img_thumb'] ; ?>" data-modalid="<?php echo $act2['id'] ; ?>" data-modaltitle="<?php echo $act2['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $act2['logo_sortie'] ; ?>" /> <?php echo $act2['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="visible-xs" style="clear:both;"></div>
									<div class="col-xs-3 visible-xs"></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<img src="<?php echo $act3['img_thumb'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow hidden-xs" />
											<img src="<?php echo $act3['img_detail'] ; ?>" alt="" class="img-responsive center-block img_index_nouv_detail img-thumbnail shadow visible-xs margin-mini-detail" />
											<div class="content-mini-index">
												<div class="titre_miniature"><a href="#" class="lien_menu_noir"><?php echo $act3['titre'] ; ?></a><span class="pull-right"><a href="#" data-modalimg="<?php echo $act3['img_thumb'] ; ?>" data-modalid="<?php echo $act3['id'] ; ?>" data-modaltitle="<?php echo $act3['titre'] ; ?>" data-toggle="modal" data-target="#myModal" class="playlist_a"><span class="glyphicon glyphicon-plus"></span></span></a></div>
												<div class="new_epi_epsiode"><img class="mini-logo-epi" src="<?php echo $act3['logo_sortie'] ; ?>" /> <?php echo $act3['nb_epi'] ; ?> Épisodes</div>
											</div>
									</div>
									<div class="col-xs-3 visible-xs"></div>
								</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 visible-lg">
					<div class="facebook-box visible-lg">
						<div class="fb-like-box" data-href="https://www.facebook.com/dramapassion" data-height="300" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
					</div>
					<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
						<h3 class="souligner h3_title">Top 10</h3>
						<div class="row" style="margin-top:10px;">
							<?php AffTop10() ; ?>
						</div>
					</div>
					<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
						<h3 class="souligner h3_title">Prochainement</h3>
						<div class="row">
							<?php comming_soon2() ; ?>
						</div>
					</div>
				</div>
		</div>
	</div>
	<!-- page -->
	<?php require_once('template/footer.php') ; ?>
    <script>
    $('.img_proch').load(function() {
		img_proch = $('.img_proch').height();
		img_proch = ((img_proch+20)/2)-10;
		$('.name-proch').css('marginTop',img_proch+'px');
	});
	$('.img_proch2').load(function() {
		img_proch = $('.img_proch2').height();
		img_proch = ((img_proch+20)/2)-10;
		$('.name-proch2').css('marginTop',img_proch+'px');
	});
	$( window ).resize(function() {
	  	img_proch = $('.img_proch').height();
		img_proch = ((img_proch+20)/2)-10;
		$('.name-proch').css('marginTop',img_proch+'px');
		img_proch = $('.img_proch2').height();
		img_proch = ((img_proch+20)/2)-10;
		$('.name-proch2').css('marginTop',img_proch+'px');
	});
    $(document).ready(function () {
	    $(".playlist_a").click(function () {
	        title = $(this).data('modaltitle');
	        id = $(this).data('modalid');
	        img = '<img src="'+$(this).data('modalimg')+'" id="myModalImgSrc" class="img-responsive" />';
	        footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>';
	        footer = footer+'<button type="button" class="btn btn-primary">Save changes</button>';
	        cont = '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" id="myModalCol1">';
	        cont = cont+img+'</div>';
	        cont = cont+'<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">';
	        cont = cont+'</div>';
	        $('#myModalLabel').html('Ma Playlist');
	        $('#myModalFooter').html(footer);   
	        $('#myModalBodyCont').html(cont);  
	    });
	});
    $('#myCarousel').carousel({
	  interval: 5000
	});
	$('#myCarousel2').carousel({
	  interval: 5000
	});

  	$("#swipe_cont2").swipe({
	  swipeLeft:function(event, direction, distance, duration, fingerCount) {
	    //This only fires when the user swipes left
	    $( "#swipe_right2" ).trigger( "click" );
	  },
	  swipeRight:function(event, direction, distance, duration, fingerCount) {
	    //This only fires when the user swipes left
	    $( "#swipe_left2" ).trigger( "click" );
	  },
	  tap:function(event, target) {
	  		var totalItems = $('.item_mini').length;
	  		var currentIndex = $('.item_mini.active').index();
	  		var id_item_mini = "#item_mini_id_"+currentIndex;
	  		var link = $(id_item_mini).data("link");
          alert(link);
      }
	  
	});
    </script>
    
  </body>
</html>