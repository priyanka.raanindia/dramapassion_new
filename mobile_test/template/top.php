<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Dramapassion</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8"> 
    
    
    <!-- Bootstrap -->
    <link href="<?php echo $http ; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $http ; ?>css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1&appId=122818257795479";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!-- navbar -->
	<nav class="navbar navbar-default navbar-fixed-top shadow2" role="navigation">
	  <div class="container">
	    <div class="navbar-header pull-left">
	      <a class="navbar-brand" href="<?php echo $http ; ?>"><img height="50" alt="logo DramaPassion" title="Aller à la page d'accueil" src="http://www.dramapassion.com/images/logo.png" style="max-height:50px;"></a>
	    </div>
	    <div class="navbar-header pull-right">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>
	    <div class="navbar-collapse collapse">
	      <ul class="nav navbar-nav navbar-left" >
	            <li class="active"><a href="#">Vidéo</a></li>
	            <li><a href="#about">Premium</a></li>
	            <li><a href="#contact">dvd</a></li>
	      </ul>  
	      <form class="navbar-form navbar-right navbar-input-group visible-lg " role="search">
				  <div class="form-group">
				    <input type="text" class="form-control" placeholder="Chercher un titre">
				  </div>
				  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
		  </form>    
		  <ul class="nav navbar-nav navbar-right">
	          	<li class="dropdown">
			        <a href="#" class="dropdown-toggle dropdown-menu-width" data-toggle="dropdown"> <span class="glyphicon glyphicon-user" style="color:#f60030;"></span> Mon compte <b class="caret"></b></a>
			        <ul class="dropdown-menu">
			          <li><a href="#">S'identifier</a></li>
			          <li><a href="#">Créer un compte</a></li>
			        </ul>
			    </li>
		  </ul>
		  <div class="col-md-12 hidden-lg h_divider"></div>  
		  <div class="col-md-12 hidden-lg s_zone">
				    <div class="input-group">
				      <input type="text" class="form-control" placeholder="Chercher un titre">
				      <span class="input-group-btn">
				        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
				      </span>
				    </div><!-- /input-group -->
		  </div><!-- /.col-lg-6 -->          
	    </div>
	  </div>
	</nav>
	<!-- navbar -->
	<!-- like -->
	<nav class="navbar navbar-default navbar-facebook" style="height: 25px !important;">
	  <div class="container" style="height: 25px !important;">
	    <div class="navbar-header pull-left">
	    </div>
	    <div class="navbar-header pull-right facebook-ul">
	      <ul class="nav navbar-nav pull-right" style="margin-top: 0px !important;margin-bottom: 0px !important;">
	        <li>
	        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	        
	          <div class="fb-like" data-href="https://www.facebook.com/dramapassion" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
	<!-- like -->