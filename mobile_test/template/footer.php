<!-- footer -->
	<footer>
		<div class="footer-fond-1">
			<div class="container">
		        <div class="row">
		        	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		        		<div class="footer-link">À propos de Dramapassion</div>
		        		<div class="footer-link">Conditions d'utilisation et de vente</div>
		        		<div class="footer-link">Protection des données personnelles</div>
		        	</div>
		        	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		        		<div class="footer-link">Aide - FAQ</div>
		        		<div class="footer-link">Contactez-nous</div>
		        	</div>
		        	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		        		<div class="footer-link">Publicités/Annonceurs</div>
		        		<div class="footer-link">Partenaires</div>
		        	</div>
		        </div>
			</div>
		</div>
		<div class="footer-fond-2">
			<div class="container">
		        <div class="row">
		        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
		        		<p>
							<img src="<?php echo $http ; ?>img/pics008.png">
							<img src="<?php echo $http ; ?>img/pics009.png">
							<img src="<?php echo $http ; ?>img/pics010.png">
							<img src="<?php echo $http ; ?>img/pics011.png">
							<img src="<?php echo $http ; ?>img/pics012.png">
							<img src="<?php echo $http ; ?>img/pics013.png"> 
		        		</p><br />
		        		<p class="footer-p-ita">Dramapassion.com est le premier site de VOD en ligne spécialisé dans les séries coréennes en version originale avec sous-titres français (VOSTFR).<br />Dramapassion.com propose un service gratuit et premium, en streaming et en téléchargement temporaire.</p>
		        		<p class="footer-p-copy">© Vlexhan Distribution SPRL, <?php echo date('Y') ; ?>, tous droits réservés.</p>
		        	</div>
		        </div>
			</div>
		</div>
    </footer>
	<!-- footer -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Ma Playlist</h4>
	      </div>
	      <div class="modal-body" id="myModalBody">
	        <div class="row" id="myModalBodyCont">
				
	        </div>
	      </div>
	      <div class="modal-footer" id="myModalFooter">
	        
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->	
	<script type='application/javascript' src='<?php echo $http ; ?>js/fastclick.js'></script>
    <script>
    window.addEventListener('load', function() {
	    new FastClick(document.body);
	}, false);
	</script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo $http ; ?>js/jquery-2.0.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $http ; ?>js/bootstrap.min.js"></script>
    <!-- swipe -->
    <script src="<?php echo $http ; ?>js/jquery.touchSwipe.min.js"></script>
    <!-- fastclick -->