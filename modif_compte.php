<?php
	if(!session_id()) session_start();
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");	
	require_once("includes/fct_inc_mon_compte.php");
	$tab_user = InfoUser($_SESSION['userid']);
	$mod = $_GET['mod'];
	
	if($mod == "pass"){
		$titre = "Modification de votre mot de passe";
	}elseif($mod == "mail"){
		$titre = "Modification de votre e-mail";
	}elseif($mod == "annif"){
		$titre = "Modification de votre date de naissance";
	}elseif($mod == "sexe"){
		$titre = "Modification de votre sexe";
	}elseif($mod == "pays"){
		$titre = "Modification de votre pays";
	}elseif($mod == "addr"){
		$titre = "Modification de votre adresse";
	}elseif($mod == "newsletter"){
		$type_news = $_GET['news_type'];
		if($type_news == 1){
			$titre = "Désinscription à notre newsletter";
		}else{
			$titre = "Inscription à notre newsletter";
		}
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<link type="text/css" href="<?php echo $http ;?>css/custom-theme/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<style type="text/css">
.ui-datepicker {
font-size:12px; 
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (Sans titre-1) -->

<form action="<?php echo $http ;?>modif_compte_action.php" METHOD="POST">
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span><?php echo $titre ; ?></span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                               
                    </td>
                </tr>
                <tr>
                	<td>
			<?php if($mod == "annif"){ ?>
				
                    <table width="500"  border="0" cellpadding="0" cellspacing="0" style="margin:auto;" class="texte">
                    	                           
                    	<tr>	
                        	<td width="240" height="40" valign="top" align="right"><b>Ancienne date de naissance</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top" ><b><?php echo $tab_user['annif'] ; ?></b></td>
						</tr>
                        <tr>
                        	<td width="240" height="40" valign="top" align="right" ><b>Nouvelle date de naissance</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="datepicker" name="annif"></td>
						</tr>
                        
                    	<tr height="100">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><input type="hidden" name="modif_type" value="annif" /><input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            
						</tr>           
                        
                    	
					</table>   
					
			      		
			<?php }elseif($mod == "newsletter"){ 
					if($type_news == 1){
						?>
							<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Voulez vous modifier votre compte pour ne plus recevoir notre newsletter ?</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><input type="hidden" name="modif_type" value="newsletter" />
								<input type="hidden" name="modif_news" value="0" />
								<input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table> 	
						<?php
					}else{
						?>
							<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Voulez vous modifier votre compte pour recevoir à nouveau notre newsletter ?</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><input type="hidden" name="modif_type" value="newsletter" />
								<input type="hidden" name="modif_news" value="1" />
								<input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table> 
						<?php
					}
				
			?>  
			<?php }elseif($mod == "pass"){ ?>
			<table width="500"  border="0" cellpadding="0" cellspacing="0" style="margin:auto;" class="texte">
			<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Ancien mot de passe</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="password" id="anc_pass" name="anc_pass"></td>
						</tr>
				<tr>
                        	<td width="240" height="40" valign="top" valign="top" align="right"><b>Nouveau mot de passe</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="password" id="new_pass" name="new_pass"></td>
						</tr>
						
                        <tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Confirmation nouveau mot de passe</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="password" id="new_pass_conf" name="new_pass_conf"></td>
						</tr>
						<tr height="10px">
                        	<td colspan="2"><br></td>
                            <td></td>
						</tr> 
						<tr>
                        	
                            <td colspan="3" align="center"><input type="hidden" name="modif_type" value="pass" /><input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            <td></td>
						</tr>     
			</table>   
			<?php }elseif($mod == "mail"){ ?>
			<table width="500"  border="0" cellpadding="0" cellspacing="0" style="margin:auto;" class="texte">
				<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Ancienne adresse e-mail</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><b><?php echo $tab_user['email'] ; ?></b></td>
						</tr>
						<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Nouvelle adresse e-mail</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="new_email" name="new_email"></td>
						</tr>
                        <tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Confirmation nouvelle adresse e-mail</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="new_email_conf" name="new_email_conf"></td>
						</tr>
						<tr height="10px">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr> 
						<tr>
                        	
                            <td colspan="3" align="center"><input type="hidden" name="modif_type" value="mail" /><input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            <td></td>
						</tr> 
			</table> 
			<?php }elseif($mod == "sexe"){ ?>
			<table width="500"  border="0" cellpadding="0" cellspacing="0" style="margin:auto;" class="texte">
                        <tr>
                    
                        	
                        	<td width="240" height="40" valign="top" align="right"><b>Sexe :</b></td>
							<td width="20"></td>
							<td width="240" valign="top"><SELECT id="new_sexe" name="new_sexe">
															<OPTION VALUE="1">Homme</OPTION>
															<OPTION VALUE="2">Femme</OPTION>
														</SELECT></td>
						</tr>
						<tr height="10px">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr> 
						<tr>
                        	
                            <td colspan="3" align="center"><input type="hidden" name="modif_type" value="sexe" /><input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            <td></td>
						</tr> 
			</table> 
			
			<?php }elseif($mod == "addr"){ 
			$req_pays = "SELECT * FROM t_dp_countryship";
			$sql_pays = mysql_query($req_pays);
			$pays_actuel = $tab_user['pays_fr'];
			
			?>
			<table width="500"  border="0" cellpadding="0" cellspacing="0" style="margin:auto;" class="texte">
                        <tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Adresse 1 :</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="addr1" name="addr1"></td>
						</tr>
						<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Adresse 2 :</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="addr1" name="addr2"></td>
						</tr>
						<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Code postal:</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="zip" name="zip"></td>
						</tr>
						<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Ville :</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="city" name="city"></td>
						</tr>
						<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Province :</b></td>
                        	<td width="20"></td>
                        	<td width="240" valign="top"><input type="text" id="province" name="province"></td>
						</tr>
						<tr>
                        	<td width="240" height="40" valign="top" align="right"><b>Pays :</b></td>
                        	<td width="20"></td>
							
							<td width="240" valign="top"><SELECT id="new_pays" name="new_pays">
														<?php
															while($sql_row_pays=mysql_fetch_array($sql_pays)){
															
																if($pays_actuel == $sql_row_pays['CountryNameFre']){
																	$pays_selected = 'selected="selected"';
																	echo'<OPTION VALUE="'.$sql_row_pays['CountryID'].'" '.$pays_selected.'>'.$sql_row_pays['CountryNameFre'].'</OPTION>';
																}else{
																	echo'<OPTION VALUE="'.$sql_row_pays['CountryID'].'">'.$sql_row_pays['CountryNameFre'].'</OPTION>';
																}
															
																
															}?>
														</SELECT></td>
                        	
						</tr>
						<tr height="10px">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr> 
						<tr>
                        	
                            <td colspan="3" align="center" ><input type="hidden" name="modif_type" value="addr" /><input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            <td></td>
						</tr> 
			</table> 
			<?php }elseif($mod == "pays"){ 
			$req_pays = "SELECT * FROM t_dp_countryship";
			$sql_pays = mysql_query($req_pays);
			
		
			$pays_actuel = $tab_user['pays_fr'];
			?>
			<table width="500"  border="0" cellpadding="0" cellspacing="0" style="margin:auto;" class="texte">
                        <tr>
                        	
                        	<td height="40" valign="top" align="right"><b>Pays: </b></td>
							<td width="20"></td>
							
							<td width="240" valign="top"><SELECT id="pays_new" name="pays_new">
														<?php
															while($sql_row_pays=mysql_fetch_array($sql_pays)){
															
																if($pays_actuel == $sql_row_pays['CountryNameFre']){
																	$pays_selected = 'selected="selected"';
																	echo'<OPTION VALUE="'.$sql_row_pays['CountryID'].'" '.$pays_selected.'>'.$sql_row_pays['CountryNameFre'].'</OPTION>';
																}else{
																	echo'<OPTION VALUE="'.$sql_row_pays['CountryID'].'">'.$sql_row_pays['CountryNameFre'].'</OPTION>';
																}
															
																
															}?>
														</SELECT></td>
						</tr>
						<tr height="10px">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr> 
						<tr>
                        	
                            <td colspan="3" align="center"><input type="hidden" name="modif_type" value="pays" /><input src="<?php echo $http ;?>images/modifier.png" type="image">&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ;?>images/fermer.jpg" /></a></td>
                            
                            <td></td>
						</tr> 
			</table> 
			<?php } ?>
                    <br><br>
                    </td>
                </tr>
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>
</form>
<?php if($mod == "annif"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,300);
	
</script>
<?php }elseif($mod == "newsletter"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,200);
	
</script>
<?php }elseif($mod == "pass"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,240);
	
</script>
<?php }elseif($mod == "mail"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,250);
	
</script>
<?php }elseif($mod == "sexe"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,150);
	
</script>
<?php }elseif($mod == "pays"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,170);
	
</script>
<?php }elseif($mod == "addr"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,370);
	
</script>
<?php } ?>
<script>
$(function() {
	var d = new Date();
	var n = d.getFullYear();
	var dataBack = n-80;
		$( "#datepicker" ).datepicker({
            changeYear: true,
			changeMonth: true,
			yearRange: dataBack+':'+n,
			
		});
		
	});
	
	jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
});
function fermer(){
	window.parent.$.fancybox.close();
}
</script>         
</body>
</html>