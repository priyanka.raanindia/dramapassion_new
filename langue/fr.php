<?php
// INDEX
define("_ttlindex_","Dramapassion - Dramas coréens gratuits en français - Kdrama en streaming VOSTFR");
define("_dscindex_","Regardez les dramas coréens gratuitement en français en France. Kdrama en streaming VOSTFR.");
define("_keyindex_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger, kdrama, français, france");

//Drama

define("_ttldrama2_","Drama coréen %%title%% gratuit en français - Kdrama en streaming VOSTFR");
define("_dscdrama2_","%%title%% - drama coréen - Tous les épisodes gratuits en français en France. Kdrama en streaming VOSTFR.");
define("_keydrama2_","%%title%%, drama coréen, korean drama, k drama, VOSTFR, gratuit, streaming, télécharger, kdrama, français, france");

//Player

define("_ttlplayer_","%%title%% - Episode %%numEpi%% - drama coréen en français - Kdrama en streaming VOSTFR");
define("_dscplayer_","%%title%% - drama coréen - Tous les épisodes gratuits en français en France. Kdrama en streaming VOSTFR.");
define("_keyplayer_","%%title%%, drama coréen, korean drama, k drama, VOSTFR, gratuit, streaming, épisode, télécharger, kdrama, français, france");

//Premium

define("_ttlpremium2_","Offre premium");
define("_dscpremium2_","Liste des dramas coréens disponibles sur Dramapassion en VOSTFR");
define("_keypremium2_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//Catalogue

define("_ttlcatalogue2_","Catalogue des dramas coréens");
define("_dsccatalogue2_","Description de l'utilisation du service VOD Dramapassion");
define("_keycatalogue2_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger, kdrama, français, france");

//DVD

define("_ttldvd2_","Catalogue des dramas coréens");
define("_dscdvd2_","Description de l'utilisation du service VOD Dramapassion");
define("_keydvd2_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//DVD détail

define("_ttldvdDetail_","Catalogue des dramas coréens");
define("_dscdvdDetail_","Description de l'utilisation du service VOD Dramapassion");
define("_keydvdDetail_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//qui somme nous

define("_ttlquiSommeNous_","Qui sommes-nous ?");
define("_dscquiSommeNous_","Présentation de la société");
define("_keyquiSommeNous_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//condition général

define("_ttlcondiGeneral_","Conditions Générales");
define("_dsccondiGeneral_","Conditions Générales pour l'utilisation du site Dramapassion.com");
define("_keycondiGeneral_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//protection des données

define("_ttlprotectDonnee_","Politique de Protection des Données Personnelles");
define("_dscprotectDonnee_","Politique de Protection des Données Personnelles sur le site de Dramapassion.com");
define("_keyprotectDonnee_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//FAQ

define("_ttlfaq_","FAQ – questions fréquemment posées");
define("_dscfaq_","Retrouvez les réponses à vos questions sur l'utilisation du service Dramapassion");
define("_keyfaq_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//Contact

define("_ttlcontact_","Comment nous contacter ?");
define("_dsccontact_","Contactez-nous en utilisant le formulaire sur cette page");
define("_keycontact_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//mon compte

define("_ttlmonCompte_","Mon compte");
define("_dscmonCompte_","Mon compte");
define("_keymonCompte_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//mon abo

define("_ttlmonAbo_","Mon abonnement");
define("_dscmonAbo_","Mon abonnement");
define("_keymonAbo_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

//playlist

define("_ttlplaylistAff_","Ma playlist");
define("_dscplaylistAff_","Ma playlist");
define("_keyplaylistAff_","dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger");

/* ERROR MESSAGES */
define("_errEnterUsername_","Veuillez saisir votre pseudo");
define("_errUsernameTaken_","Pseudo non disponible - Choisissez-en un autre ");
define("_errEnterPassword_","Veuillez saisir votre mot de passe");
define("_errPasswordNoMatch_","La confirmation de votre mot de passe ne correspond pas");
define("_errEmailNoMatch_","La confirmation de votre adresse e-mail ne correspond pas");
define("_errValidEmail_","Veuillez saisir une adresse e-mail valide");
define("_errEmailTaken_","Cette adresse e-mail n'est pas disponible");
define("_errValidSecCode_","Le code de sécurité ne correspond pas - Veuillez le saisir à nouveau");
define("_errValidReferral_","Pseudo non disponible");
define("_errAgree_","Veuillez accepter les Conditions Générales");
define("_errRequired_","Ce champ est obligé");
define("_errRemplir_","Remplir ce champ est obligatoire");
define("_errEnterPasswordc_", "Veuillez confirmer votre mot de passe");
define("_errValidEmail2_" , "Veuillez confirmer votre email");

?>