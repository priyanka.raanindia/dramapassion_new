<?php 
require_once("includes/settings.inc.php");
?>

<footer>
	<div class="footer-fond-1">
		<div class="container">
	        <div class="row">
	        	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
	        		<div class="footer-link"><a href="<?php echo $http ;?>a-propos-de-DramaPassion/">À propos de Dramapassion</a></div>
	        		<div class="footer-link"><a href="<?php echo $http ;?>conditions-generales/">Conditions d'utilisation et de vente</a></div>
	        		<div class="footer-link"><a href="<?php echo $http ;?>protection-des-donnees-personnelles/">Protection des données personnelles</a></div>
	        	</div>
	        	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
	        		<div class="footer-link"><a href="<?php echo $http ;?>aide-faq/">Aide - FAQ</a></div>
	        		<div class="footer-link"><a href="<?php echo $http ;?>contactez-nous/">Contactez-nous</a></div>
	        	</div>
	        	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
	        		<div class="footer-link"><a href="<?php echo $http ;?>annonceurs">Publicités/Annonceurs</a></div>
	        		
	        	</div>
	        </div>
		</div>
	</div>
	<div class="footer-fond-2">
		<div class="container">
	        <div class="row">
	        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
	        		<p>
						<img src="<?php echo $http ; ?>images/pics008.png">
						<img src="<?php echo $http ; ?>images/pics009.png">
						<img src="<?php echo $http ; ?>images/pics010.png">
						<img src="<?php echo $http ; ?>images/pics011.png">
						<img src="<?php echo $http ; ?>images/pics012.png">
						<img src="<?php echo $http ; ?>images/pics013.png"> 
	        		</p><br />
	        		<p class="footer-p-ita">Dramapassion.com est le premier site de VOD en ligne spécialisé dans les séries coréennes en version originale avec sous-titres français (VOSTFR).<br />Dramapassion.com propose un service gratuit et premium, en streaming et en téléchargement temporaire.</p>
	        		<p class="footer-p-copy">© Vlexhan Distribution SPRL, <?php echo date('Y') ; ?>, tous droits réservés.</p>
	        	</div>
	        </div>
		</div>
	</div>
</footer>  
  
  

    
  
<!-- Code Google de la balise de remarketing -->
<!--------------------------------------------------
Les balises de remarketing ne peuvent pas être associées aux informations personnelles ou placées sur des pages liées aux catégories à caractère sensible. Pour comprendre et savoir comment configurer la balise, rendez-vous sur la page http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1026690034;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:none;">
<img height="1" width="1" style="border-style:none;display:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1026690034/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>