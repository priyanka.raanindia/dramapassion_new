<!DOCTYPE html>
<html>
  <head>
    <title>kpop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style_index.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	  	<!-- navbar  -->
	  	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  	<!-- container -->
	  	<div class="container">
		  <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header pull-left">
	      	<a class="navbar-brand" href="#">Logo</a>
		  </div>
		  <div class="navbar-header pull-right">
	     	 <form class="navbar-form navbar-right navbar-input-group hidden-xs" role="search">
				  <div class="form-group">
				    <input type="text" class="form-control" placeholder="Chercher">
				  </div>
				  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			  </form>  
		 	 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		    </button>
		 </div>
		  <!-- Collect the nav links, forms, and other content for toggling -->
		  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		    <ul class="nav navbar-nav">
		      <li><a href="#">ARTISTES</a></li>
		      <li><a href="#">OST</a></li>
		      <li><a href="#">PLAYLIST</a></li>
		      <li><a href="#">OST</a></li>
		    </ul>
		    <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
			        <a href="#" class="dropdown-toggle toggle-login" data-toggle="dropdown"><img src="img/face.jpg" height="34" class="img-navbar" />Login <b class="caret"></b></a>
			        <ul class="dropdown-menu">
			          <li><a href="#">Action</a></li>
			          <li><a href="#">Another action</a></li>
			          <li><a href="#">Something else here</a></li>
			          <li class="divider"></li>
			          <li><a href="#">Separated link</a></li>
			          <li class="divider"></li>
			          <li><a href="#">One more separated link</a></li>
			        </ul>
		      </li>
		    </ul>
		    <div class="col-xs-12 visible-xs h_divider"></div>  
			<div class="col-xs-12 visible-xs s_zone">
					    <div class="input-group">
					      <input type="text" class="form-control" placeholder="Chercher">
					      <span class="input-group-btn">
					        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
					      </span>
					    </div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->
		  </div><!-- /.navbar-collapse -->
	  	</div>
	  	<!-- end container -->
		</nav>
	  	<!-- end navbar  -->
	  	<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-528b589501c0213b"></script>
<!-- AddThis Button END -->
	  	<!-- container -->
	  	<div class="container">
	  	<p>
	  	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet felis hendrerit, dignissim felis eu, tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer a ipsum vulputate, suscipit mi quis, imperdiet mauris. Morbi felis turpis, fermentum ac blandit posuere, dapibus sed elit. Phasellus commodo sagittis nisl, non interdum purus imperdiet eu. Integer bibendum pharetra blandit. Fusce hendrerit, nibh eu commodo condimentum, lectus ligula dignissim velit, at tempus mi urna eu ante. Aenean pulvinar dignissim fermentum.

Nam leo dolor, rutrum vel vestibulum et, rhoncus sit amet purus. Nullam ultrices venenatis elit sit amet porttitor. Nunc id augue nec dolor aliquet semper vel at eros. Duis in diam mollis, viverra justo et, lobortis tortor. Sed vitae mauris purus. Quisque viverra lectus nec metus faucibus pretium nec nec mauris. Proin mollis lectus sit amet leo pellentesque egestas. Pellentesque blandit orci eu elementum ultricies. Etiam bibendum justo ultricies neque sollicitudin, at gravida mauris vestibulum. Morbi nibh diam, varius eget convallis ut, porttitor vulputate sapien. Morbi dictum hendrerit odio. Cras interdum justo nisl, in eleifend risus faucibus vel. Pellentesque et tristique tellus. Nam eget egestas nunc. Suspendisse varius, lacus ac vehicula ullamcorper, lorem neque facilisis ligula, ut suscipit odio est nec arcu. In non vestibulum sapien, ut tristique lectus.

Aliquam sed lacus justo. Mauris adipiscing massa ac augue cursus euismod. Vestibulum eleifend nulla magna, sit amet pretium elit sollicitudin tempus. Maecenas aliquam nisl sit amet velit dictum, quis pharetra sapien congue. Vivamus sit amet nibh metus. Sed mattis cursus lorem sed facilisis. Nullam imperdiet urna ut ante varius rhoncus. Nullam facilisis suscipit libero bibendum volutpat. Proin in pellentesque leo. Aliquam pulvinar rhoncus faucibus. Nulla libero mauris, pellentesque sed luctus quis, faucibus in orci. Proin scelerisque ullamcorper orci et sollicitudin. Nam massa neque, ullamcorper a venenatis id, mattis vel urna. Integer rutrum ante sed facilisis eleifend.

Quisque rutrum eleifend luctus. Etiam mattis molestie augue, a tincidunt nibh tincidunt non. Fusce non condimentum odio. Phasellus dictum dolor eu neque pretium, vel dapibus nisl mattis. Nulla pulvinar est id sodales aliquet. Etiam elementum iaculis tellus, nec tincidunt neque pharetra in. Donec ac adipiscing elit. Morbi iaculis mi non arcu auctor accumsan. Nam arcu augue, placerat id nibh et, molestie blandit augue. Donec sit amet arcu sit amet leo molestie commodo. Cras eget nunc velit.

Etiam interdum sollicitudin justo sed tristique. Aliquam interdum, nunc vel semper convallis, nunc quam scelerisque dui, eget venenatis libero elit ac ipsum. Aenean velit dui, luctus at massa sit amet, laoreet facilisis tellus. Duis tempor gravida libero. Curabitur vitae dictum metus, vel scelerisque nisi. Duis eu egestas leo. Nullam hendrerit enim ac lectus blandit consequat. Curabitur non tellus sed sem fermentum iaculis at placerat mi. Mauris sed eros a sapien vulputate accumsan eu ac leo. Fusce vel consequat ligula, a condimentum neque. Aliquam posuere neque quam. Praesent pretium dolor eu sapien elementum, vel tristique eros fermentum. Donec ac neque fermentum eros cursus consectetur eu vel nunc. Aliquam aliquet malesuada purus at gravida.
	  	</p>
	  	<p>
	  	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet felis hendrerit, dignissim felis eu, tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer a ipsum vulputate, suscipit mi quis, imperdiet mauris. Morbi felis turpis, fermentum ac blandit posuere, dapibus sed elit. Phasellus commodo sagittis nisl, non interdum purus imperdiet eu. Integer bibendum pharetra blandit. Fusce hendrerit, nibh eu commodo condimentum, lectus ligula dignissim velit, at tempus mi urna eu ante. Aenean pulvinar dignissim fermentum.

Nam leo dolor, rutrum vel vestibulum et, rhoncus sit amet purus. Nullam ultrices venenatis elit sit amet porttitor. Nunc id augue nec dolor aliquet semper vel at eros. Duis in diam mollis, viverra justo et, lobortis tortor. Sed vitae mauris purus. Quisque viverra lectus nec metus faucibus pretium nec nec mauris. Proin mollis lectus sit amet leo pellentesque egestas. Pellentesque blandit orci eu elementum ultricies. Etiam bibendum justo ultricies neque sollicitudin, at gravida mauris vestibulum. Morbi nibh diam, varius eget convallis ut, porttitor vulputate sapien. Morbi dictum hendrerit odio. Cras interdum justo nisl, in eleifend risus faucibus vel. Pellentesque et tristique tellus. Nam eget egestas nunc. Suspendisse varius, lacus ac vehicula ullamcorper, lorem neque facilisis ligula, ut suscipit odio est nec arcu. In non vestibulum sapien, ut tristique lectus.

Aliquam sed lacus justo. Mauris adipiscing massa ac augue cursus euismod. Vestibulum eleifend nulla magna, sit amet pretium elit sollicitudin tempus. Maecenas aliquam nisl sit amet velit dictum, quis pharetra sapien congue. Vivamus sit amet nibh metus. Sed mattis cursus lorem sed facilisis. Nullam imperdiet urna ut ante varius rhoncus. Nullam facilisis suscipit libero bibendum volutpat. Proin in pellentesque leo. Aliquam pulvinar rhoncus faucibus. Nulla libero mauris, pellentesque sed luctus quis, faucibus in orci. Proin scelerisque ullamcorper orci et sollicitudin. Nam massa neque, ullamcorper a venenatis id, mattis vel urna. Integer rutrum ante sed facilisis eleifend.

Quisque rutrum eleifend luctus. Etiam mattis molestie augue, a tincidunt nibh tincidunt non. Fusce non condimentum odio. Phasellus dictum dolor eu neque pretium, vel dapibus nisl mattis. Nulla pulvinar est id sodales aliquet. Etiam elementum iaculis tellus, nec tincidunt neque pharetra in. Donec ac adipiscing elit. Morbi iaculis mi non arcu auctor accumsan. Nam arcu augue, placerat id nibh et, molestie blandit augue. Donec sit amet arcu sit amet leo molestie commodo. Cras eget nunc velit.

Etiam interdum sollicitudin justo sed tristique. Aliquam interdum, nunc vel semper convallis, nunc quam scelerisque dui, eget venenatis libero elit ac ipsum. Aenean velit dui, luctus at massa sit amet, laoreet facilisis tellus. Duis tempor gravida libero. Curabitur vitae dictum metus, vel scelerisque nisi. Duis eu egestas leo. Nullam hendrerit enim ac lectus blandit consequat. Curabitur non tellus sed sem fermentum iaculis at placerat mi. Mauris sed eros a sapien vulputate accumsan eu ac leo. Fusce vel consequat ligula, a condimentum neque. Aliquam posuere neque quam. Praesent pretium dolor eu sapien elementum, vel tristique eros fermentum. Donec ac neque fermentum eros cursus consectetur eu vel nunc. Aliquam aliquet malesuada purus at gravida.
	  	</p>
	  	<p>
	  	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet felis hendrerit, dignissim felis eu, tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer a ipsum vulputate, suscipit mi quis, imperdiet mauris. Morbi felis turpis, fermentum ac blandit posuere, dapibus sed elit. Phasellus commodo sagittis nisl, non interdum purus imperdiet eu. Integer bibendum pharetra blandit. Fusce hendrerit, nibh eu commodo condimentum, lectus ligula dignissim velit, at tempus mi urna eu ante. Aenean pulvinar dignissim fermentum.

Nam leo dolor, rutrum vel vestibulum et, rhoncus sit amet purus. Nullam ultrices venenatis elit sit amet porttitor. Nunc id augue nec dolor aliquet semper vel at eros. Duis in diam mollis, viverra justo et, lobortis tortor. Sed vitae mauris purus. Quisque viverra lectus nec metus faucibus pretium nec nec mauris. Proin mollis lectus sit amet leo pellentesque egestas. Pellentesque blandit orci eu elementum ultricies. Etiam bibendum justo ultricies neque sollicitudin, at gravida mauris vestibulum. Morbi nibh diam, varius eget convallis ut, porttitor vulputate sapien. Morbi dictum hendrerit odio. Cras interdum justo nisl, in eleifend risus faucibus vel. Pellentesque et tristique tellus. Nam eget egestas nunc. Suspendisse varius, lacus ac vehicula ullamcorper, lorem neque facilisis ligula, ut suscipit odio est nec arcu. In non vestibulum sapien, ut tristique lectus.

Aliquam sed lacus justo. Mauris adipiscing massa ac augue cursus euismod. Vestibulum eleifend nulla magna, sit amet pretium elit sollicitudin tempus. Maecenas aliquam nisl sit amet velit dictum, quis pharetra sapien congue. Vivamus sit amet nibh metus. Sed mattis cursus lorem sed facilisis. Nullam imperdiet urna ut ante varius rhoncus. Nullam facilisis suscipit libero bibendum volutpat. Proin in pellentesque leo. Aliquam pulvinar rhoncus faucibus. Nulla libero mauris, pellentesque sed luctus quis, faucibus in orci. Proin scelerisque ullamcorper orci et sollicitudin. Nam massa neque, ullamcorper a venenatis id, mattis vel urna. Integer rutrum ante sed facilisis eleifend.

Quisque rutrum eleifend luctus. Etiam mattis molestie augue, a tincidunt nibh tincidunt non. Fusce non condimentum odio. Phasellus dictum dolor eu neque pretium, vel dapibus nisl mattis. Nulla pulvinar est id sodales aliquet. Etiam elementum iaculis tellus, nec tincidunt neque pharetra in. Donec ac adipiscing elit. Morbi iaculis mi non arcu auctor accumsan. Nam arcu augue, placerat id nibh et, molestie blandit augue. Donec sit amet arcu sit amet leo molestie commodo. Cras eget nunc velit.

Etiam interdum sollicitudin justo sed tristique. Aliquam interdum, nunc vel semper convallis, nunc quam scelerisque dui, eget venenatis libero elit ac ipsum. Aenean velit dui, luctus at massa sit amet, laoreet facilisis tellus. Duis tempor gravida libero. Curabitur vitae dictum metus, vel scelerisque nisi. Duis eu egestas leo. Nullam hendrerit enim ac lectus blandit consequat. Curabitur non tellus sed sem fermentum iaculis at placerat mi. Mauris sed eros a sapien vulputate accumsan eu ac leo. Fusce vel consequat ligula, a condimentum neque. Aliquam posuere neque quam. Praesent pretium dolor eu sapien elementum, vel tristique eros fermentum. Donec ac neque fermentum eros cursus consectetur eu vel nunc. Aliquam aliquet malesuada purus at gravida.
	  	</p>
	  	<p>
	  	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet felis hendrerit, dignissim felis eu, tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer a ipsum vulputate, suscipit mi quis, imperdiet mauris. Morbi felis turpis, fermentum ac blandit posuere, dapibus sed elit. Phasellus commodo sagittis nisl, non interdum purus imperdiet eu. Integer bibendum pharetra blandit. Fusce hendrerit, nibh eu commodo condimentum, lectus ligula dignissim velit, at tempus mi urna eu ante. Aenean pulvinar dignissim fermentum.

Nam leo dolor, rutrum vel vestibulum et, rhoncus sit amet purus. Nullam ultrices venenatis elit sit amet porttitor. Nunc id augue nec dolor aliquet semper vel at eros. Duis in diam mollis, viverra justo et, lobortis tortor. Sed vitae mauris purus. Quisque viverra lectus nec metus faucibus pretium nec nec mauris. Proin mollis lectus sit amet leo pellentesque egestas. Pellentesque blandit orci eu elementum ultricies. Etiam bibendum justo ultricies neque sollicitudin, at gravida mauris vestibulum. Morbi nibh diam, varius eget convallis ut, porttitor vulputate sapien. Morbi dictum hendrerit odio. Cras interdum justo nisl, in eleifend risus faucibus vel. Pellentesque et tristique tellus. Nam eget egestas nunc. Suspendisse varius, lacus ac vehicula ullamcorper, lorem neque facilisis ligula, ut suscipit odio est nec arcu. In non vestibulum sapien, ut tristique lectus.

Aliquam sed lacus justo. Mauris adipiscing massa ac augue cursus euismod. Vestibulum eleifend nulla magna, sit amet pretium elit sollicitudin tempus. Maecenas aliquam nisl sit amet velit dictum, quis pharetra sapien congue. Vivamus sit amet nibh metus. Sed mattis cursus lorem sed facilisis. Nullam imperdiet urna ut ante varius rhoncus. Nullam facilisis suscipit libero bibendum volutpat. Proin in pellentesque leo. Aliquam pulvinar rhoncus faucibus. Nulla libero mauris, pellentesque sed luctus quis, faucibus in orci. Proin scelerisque ullamcorper orci et sollicitudin. Nam massa neque, ullamcorper a venenatis id, mattis vel urna. Integer rutrum ante sed facilisis eleifend.

Quisque rutrum eleifend luctus. Etiam mattis molestie augue, a tincidunt nibh tincidunt non. Fusce non condimentum odio. Phasellus dictum dolor eu neque pretium, vel dapibus nisl mattis. Nulla pulvinar est id sodales aliquet. Etiam elementum iaculis tellus, nec tincidunt neque pharetra in. Donec ac adipiscing elit. Morbi iaculis mi non arcu auctor accumsan. Nam arcu augue, placerat id nibh et, molestie blandit augue. Donec sit amet arcu sit amet leo molestie commodo. Cras eget nunc velit.

Etiam interdum sollicitudin justo sed tristique. Aliquam interdum, nunc vel semper convallis, nunc quam scelerisque dui, eget venenatis libero elit ac ipsum. Aenean velit dui, luctus at massa sit amet, laoreet facilisis tellus. Duis tempor gravida libero. Curabitur vitae dictum metus, vel scelerisque nisi. Duis eu egestas leo. Nullam hendrerit enim ac lectus blandit consequat. Curabitur non tellus sed sem fermentum iaculis at placerat mi. Mauris sed eros a sapien vulputate accumsan eu ac leo. Fusce vel consequat ligula, a condimentum neque. Aliquam posuere neque quam. Praesent pretium dolor eu sapien elementum, vel tristique eros fermentum. Donec ac neque fermentum eros cursus consectetur eu vel nunc. Aliquam aliquet malesuada purus at gravida.
	  	</p>
	  	<p>
	  	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet felis hendrerit, dignissim felis eu, tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer a ipsum vulputate, suscipit mi quis, imperdiet mauris. Morbi felis turpis, fermentum ac blandit posuere, dapibus sed elit. Phasellus commodo sagittis nisl, non interdum purus imperdiet eu. Integer bibendum pharetra blandit. Fusce hendrerit, nibh eu commodo condimentum, lectus ligula dignissim velit, at tempus mi urna eu ante. Aenean pulvinar dignissim fermentum.

Nam leo dolor, rutrum vel vestibulum et, rhoncus sit amet purus. Nullam ultrices venenatis elit sit amet porttitor. Nunc id augue nec dolor aliquet semper vel at eros. Duis in diam mollis, viverra justo et, lobortis tortor. Sed vitae mauris purus. Quisque viverra lectus nec metus faucibus pretium nec nec mauris. Proin mollis lectus sit amet leo pellentesque egestas. Pellentesque blandit orci eu elementum ultricies. Etiam bibendum justo ultricies neque sollicitudin, at gravida mauris vestibulum. Morbi nibh diam, varius eget convallis ut, porttitor vulputate sapien. Morbi dictum hendrerit odio. Cras interdum justo nisl, in eleifend risus faucibus vel. Pellentesque et tristique tellus. Nam eget egestas nunc. Suspendisse varius, lacus ac vehicula ullamcorper, lorem neque facilisis ligula, ut suscipit odio est nec arcu. In non vestibulum sapien, ut tristique lectus.

Aliquam sed lacus justo. Mauris adipiscing massa ac augue cursus euismod. Vestibulum eleifend nulla magna, sit amet pretium elit sollicitudin tempus. Maecenas aliquam nisl sit amet velit dictum, quis pharetra sapien congue. Vivamus sit amet nibh metus. Sed mattis cursus lorem sed facilisis. Nullam imperdiet urna ut ante varius rhoncus. Nullam facilisis suscipit libero bibendum volutpat. Proin in pellentesque leo. Aliquam pulvinar rhoncus faucibus. Nulla libero mauris, pellentesque sed luctus quis, faucibus in orci. Proin scelerisque ullamcorper orci et sollicitudin. Nam massa neque, ullamcorper a venenatis id, mattis vel urna. Integer rutrum ante sed facilisis eleifend.

Quisque rutrum eleifend luctus. Etiam mattis molestie augue, a tincidunt nibh tincidunt non. Fusce non condimentum odio. Phasellus dictum dolor eu neque pretium, vel dapibus nisl mattis. Nulla pulvinar est id sodales aliquet. Etiam elementum iaculis tellus, nec tincidunt neque pharetra in. Donec ac adipiscing elit. Morbi iaculis mi non arcu auctor accumsan. Nam arcu augue, placerat id nibh et, molestie blandit augue. Donec sit amet arcu sit amet leo molestie commodo. Cras eget nunc velit.

Etiam interdum sollicitudin justo sed tristique. Aliquam interdum, nunc vel semper convallis, nunc quam scelerisque dui, eget venenatis libero elit ac ipsum. Aenean velit dui, luctus at massa sit amet, laoreet facilisis tellus. Duis tempor gravida libero. Curabitur vitae dictum metus, vel scelerisque nisi. Duis eu egestas leo. Nullam hendrerit enim ac lectus blandit consequat. Curabitur non tellus sed sem fermentum iaculis at placerat mi. Mauris sed eros a sapien vulputate accumsan eu ac leo. Fusce vel consequat ligula, a condimentum neque. Aliquam posuere neque quam. Praesent pretium dolor eu sapien elementum, vel tristique eros fermentum. Donec ac neque fermentum eros cursus consectetur eu vel nunc. Aliquam aliquet malesuada purus at gravida.
	  	</p>
	  	<p>
	  	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet felis hendrerit, dignissim felis eu, tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer a ipsum vulputate, suscipit mi quis, imperdiet mauris. Morbi felis turpis, fermentum ac blandit posuere, dapibus sed elit. Phasellus commodo sagittis nisl, non interdum purus imperdiet eu. Integer bibendum pharetra blandit. Fusce hendrerit, nibh eu commodo condimentum, lectus ligula dignissim velit, at tempus mi urna eu ante. Aenean pulvinar dignissim fermentum.

Nam leo dolor, rutrum vel vestibulum et, rhoncus sit amet purus. Nullam ultrices venenatis elit sit amet porttitor. Nunc id augue nec dolor aliquet semper vel at eros. Duis in diam mollis, viverra justo et, lobortis tortor. Sed vitae mauris purus. Quisque viverra lectus nec metus faucibus pretium nec nec mauris. Proin mollis lectus sit amet leo pellentesque egestas. Pellentesque blandit orci eu elementum ultricies. Etiam bibendum justo ultricies neque sollicitudin, at gravida mauris vestibulum. Morbi nibh diam, varius eget convallis ut, porttitor vulputate sapien. Morbi dictum hendrerit odio. Cras interdum justo nisl, in eleifend risus faucibus vel. Pellentesque et tristique tellus. Nam eget egestas nunc. Suspendisse varius, lacus ac vehicula ullamcorper, lorem neque facilisis ligula, ut suscipit odio est nec arcu. In non vestibulum sapien, ut tristique lectus.

Aliquam sed lacus justo. Mauris adipiscing massa ac augue cursus euismod. Vestibulum eleifend nulla magna, sit amet pretium elit sollicitudin tempus. Maecenas aliquam nisl sit amet velit dictum, quis pharetra sapien congue. Vivamus sit amet nibh metus. Sed mattis cursus lorem sed facilisis. Nullam imperdiet urna ut ante varius rhoncus. Nullam facilisis suscipit libero bibendum volutpat. Proin in pellentesque leo. Aliquam pulvinar rhoncus faucibus. Nulla libero mauris, pellentesque sed luctus quis, faucibus in orci. Proin scelerisque ullamcorper orci et sollicitudin. Nam massa neque, ullamcorper a venenatis id, mattis vel urna. Integer rutrum ante sed facilisis eleifend.

Quisque rutrum eleifend luctus. Etiam mattis molestie augue, a tincidunt nibh tincidunt non. Fusce non condimentum odio. Phasellus dictum dolor eu neque pretium, vel dapibus nisl mattis. Nulla pulvinar est id sodales aliquet. Etiam elementum iaculis tellus, nec tincidunt neque pharetra in. Donec ac adipiscing elit. Morbi iaculis mi non arcu auctor accumsan. Nam arcu augue, placerat id nibh et, molestie blandit augue. Donec sit amet arcu sit amet leo molestie commodo. Cras eget nunc velit.

Etiam interdum sollicitudin justo sed tristique. Aliquam interdum, nunc vel semper convallis, nunc quam scelerisque dui, eget venenatis libero elit ac ipsum. Aenean velit dui, luctus at massa sit amet, laoreet facilisis tellus. Duis tempor gravida libero. Curabitur vitae dictum metus, vel scelerisque nisi. Duis eu egestas leo. Nullam hendrerit enim ac lectus blandit consequat. Curabitur non tellus sed sem fermentum iaculis at placerat mi. Mauris sed eros a sapien vulputate accumsan eu ac leo. Fusce vel consequat ligula, a condimentum neque. Aliquam posuere neque quam. Praesent pretium dolor eu sapien elementum, vel tristique eros fermentum. Donec ac neque fermentum eros cursus consectetur eu vel nunc. Aliquam aliquet malesuada purus at gravida.
	  	</p>
	  	<p>
	  	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet felis hendrerit, dignissim felis eu, tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer a ipsum vulputate, suscipit mi quis, imperdiet mauris. Morbi felis turpis, fermentum ac blandit posuere, dapibus sed elit. Phasellus commodo sagittis nisl, non interdum purus imperdiet eu. Integer bibendum pharetra blandit. Fusce hendrerit, nibh eu commodo condimentum, lectus ligula dignissim velit, at tempus mi urna eu ante. Aenean pulvinar dignissim fermentum.

Nam leo dolor, rutrum vel vestibulum et, rhoncus sit amet purus. Nullam ultrices venenatis elit sit amet porttitor. Nunc id augue nec dolor aliquet semper vel at eros. Duis in diam mollis, viverra justo et, lobortis tortor. Sed vitae mauris purus. Quisque viverra lectus nec metus faucibus pretium nec nec mauris. Proin mollis lectus sit amet leo pellentesque egestas. Pellentesque blandit orci eu elementum ultricies. Etiam bibendum justo ultricies neque sollicitudin, at gravida mauris vestibulum. Morbi nibh diam, varius eget convallis ut, porttitor vulputate sapien. Morbi dictum hendrerit odio. Cras interdum justo nisl, in eleifend risus faucibus vel. Pellentesque et tristique tellus. Nam eget egestas nunc. Suspendisse varius, lacus ac vehicula ullamcorper, lorem neque facilisis ligula, ut suscipit odio est nec arcu. In non vestibulum sapien, ut tristique lectus.

Aliquam sed lacus justo. Mauris adipiscing massa ac augue cursus euismod. Vestibulum eleifend nulla magna, sit amet pretium elit sollicitudin tempus. Maecenas aliquam nisl sit amet velit dictum, quis pharetra sapien congue. Vivamus sit amet nibh metus. Sed mattis cursus lorem sed facilisis. Nullam imperdiet urna ut ante varius rhoncus. Nullam facilisis suscipit libero bibendum volutpat. Proin in pellentesque leo. Aliquam pulvinar rhoncus faucibus. Nulla libero mauris, pellentesque sed luctus quis, faucibus in orci. Proin scelerisque ullamcorper orci et sollicitudin. Nam massa neque, ullamcorper a venenatis id, mattis vel urna. Integer rutrum ante sed facilisis eleifend.

Quisque rutrum eleifend luctus. Etiam mattis molestie augue, a tincidunt nibh tincidunt non. Fusce non condimentum odio. Phasellus dictum dolor eu neque pretium, vel dapibus nisl mattis. Nulla pulvinar est id sodales aliquet. Etiam elementum iaculis tellus, nec tincidunt neque pharetra in. Donec ac adipiscing elit. Morbi iaculis mi non arcu auctor accumsan. Nam arcu augue, placerat id nibh et, molestie blandit augue. Donec sit amet arcu sit amet leo molestie commodo. Cras eget nunc velit.

Etiam interdum sollicitudin justo sed tristique. Aliquam interdum, nunc vel semper convallis, nunc quam scelerisque dui, eget venenatis libero elit ac ipsum. Aenean velit dui, luctus at massa sit amet, laoreet facilisis tellus. Duis tempor gravida libero. Curabitur vitae dictum metus, vel scelerisque nisi. Duis eu egestas leo. Nullam hendrerit enim ac lectus blandit consequat. Curabitur non tellus sed sem fermentum iaculis at placerat mi. Mauris sed eros a sapien vulputate accumsan eu ac leo. Fusce vel consequat ligula, a condimentum neque. Aliquam posuere neque quam. Praesent pretium dolor eu sapien elementum, vel tristique eros fermentum. Donec ac neque fermentum eros cursus consectetur eu vel nunc. Aliquam aliquet malesuada purus at gravida.
	  	</p>
	  	
	  </div>
	  <!-- end container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>