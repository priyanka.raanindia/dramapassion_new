<?php
if (!isset($_COOKIE['__trk'])) {
    $unique = uniqid();
    $cookie = md5($unique . $_SERVER['REMOTE_ADDR']);
    setcookie("__trk", $cookie, time() + 15552000, "/");
} else {
    setcookie("__trk", $_COOKIE['__trk'], time() + 15552000, "/");
}

$cookiesIp = $_COOKIE['dpip'];
if ($cookiesIp != 'ok') {
    setcookie("dpip", 'ok', time() + (3600 * 24 * 90), "/");
}


require_once('includes/Mobile_Detect.php');
require_once("includes/settings.inc.php");
require_once("includes/dbinfo.inc.php");
require_once("includes/functions.php");
require_once("logincheck.php");
require_once("langue/fr.php");
//require_once("locationcheck.php");

$detect_os = new Mobile_Detect();

$user_lvl = $_SESSION['subscription'];

$os = getOs();


if (isset($_SESSION['userid']) && $_SESSION['userid'] > 0) {
    $_SESSION['balance'] = mysql_result(mysql_query("SELECT UserBalance FROM t_dp_user WHERE UserID = " . $_SESSION['userid']), 0, "UserBalance");
}

$page = str_replace(".php", "", basename($_SERVER['PHP_SELF']));

$title = _ttlindex_;
$description = "";
$keywords = "";
$footer = "";
$sociallinks = false;
$og_title = _ttlindex_;
$og_img = $http . "images/logo_facebook.jpg";
$og_desc = _dscindex_;


if ($page == "index") {
    $title = _ttlindex_;
    $description = _dscindex_;
    $keywords = _keyindex_;
    $sociallinks = true;

    $og_title = _ttlindex_;
    $og_img = $http . "images/logo_facebook.jpg";
    $og_desc = _dscindex_;
    if ($detect_os->isTablet()) {

        if (isset($_COOKIE['__AppliDramapassion'])) {
            $cookiez = $_COOKIE['__AppliDramapassion'];
            if ($cookiez == 1) {
                $verif_app = 0;
            } else {
                $verif_app = 1;
            }
        } else {
            $verif_app = 1;
        }
    }
    if ($os == 'RT' || $os == 'Android') {
        $verif_app = 0;
    }
} elseif ($page == "drama2") {
    $dramaID = cleanup($_GET['dramaID']);
    $drama_info_meta = DramaInfo($dramaID);

    $title = _ttldrama2_;
    $title = str_replace("%%title%%", $drama_info_meta['titre'], $title);

    $description = _dscdrama2_;
    $synopsis = substr($drama_info_meta['synopsis'], 0, 200);
    if (strlen($drama_info_meta['synopsis']) > 200) {
        $synopsis .= "...";
    }
    $description = str_replace("%%title%%", $drama_info_meta['titre'], $description);
    $description = str_replace("%%synopsis%%", $synopsis, $description);

    $keywords = _keydrama2_;
    $keyword_titre = $drama_info_meta['titre'] . ', ' . $drama_info_meta['keyword'];
    $keywords = str_replace("%%title%%", $keyword_titre, $keywords);

    $sociallinks = true;
    $og_title = $drama_info_meta['titre'];
    $og_img = $drama_info_meta['img_facebook'];
    $og_desc = $synopsis;
} elseif ($page == "player") {
    $dramaID = cleanup($_GET['dramaID']);
    $drama_info_meta = DramaInfo($dramaID);
    $epiNB = cleanup($_GET['epiNB']);
    $type = cleanup($_POST['hidden_type']);

    if ($epiNB < 10) {
        $epiNB2 = $chaine = substr($epiNB, 1);
    }

    $title = _ttlplayer_;
    $title = str_replace("%%title%%", $drama_info_meta['titre'], $title);
    $title = str_replace("%%type%%", $type, $title);
    $title = str_replace("%%numEpi%%", $epiNB2, $title);
    $title = str_replace("free", "Gratuit", $title);
    $title = str_replace("sd", "SD", $title);
    $title = str_replace("hd", "HD", $title);


    $description = _dscplayer_;
    $synopsis = substr($drama_info_meta['synopsis'], 0, 200);
    if (strlen($drama_info_meta['synopsis']) > 200) {
        $synopsis .= "...";
    }
    $description = str_replace("%%title%%", $drama_info_meta['titre'], $description);
    $description = str_replace("%%synopsis%%", $synopsis, $description);

    $keywords = _keyplayer_;
    $keyword_titre = $drama_info_meta['titre'] . ', ' . $drama_info_meta['keyword'];
    $keywords = str_replace("%%title%%", $keyword_titre, $keywords);

    $sociallinks = true;

    $sociallinks = true;
    $og_title = $drama_info_meta['titre'] . " - " . $type . " Episode " . $epiNB;
    $og_title = str_replace("free", "Gratuit", $og_title);
    $og_title = str_replace("sd", "SD", $og_title);
    $og_title = str_replace("hd", "HD", $og_title);
    $og_img = $drama_info_meta['img_detail'];
    $og_desc = $synopsis;
} elseif ($page == "premium2") {
    $title = _ttlpremium2_;
    $description = _dscpremium2_;
    $keywords = _keypremium2_;
} elseif ($page == "catalogue2") {
    $title = _ttlcatalogue2_;
    $description = _dsccatalogue2_;
    $keywords = _keycatalogue2_;
} elseif ($page == "dvd2") {
    $title = _ttldvd2_;
    $description = _dscdvd2_;
    $keywords = _keydvd2_;
} elseif ($page == "dvd_detail") {
    $title = _ttldvdDetail_;
    $description = _dscdvdDetail_;
    $keywords = _keydvdDetail_;
} elseif ($page == "qui_somme_nous") {
    $title = _ttlquiSommeNous_;
    $description = _dscquiSommeNous_;
    $keywords = _keyquiSommeNous_;
} elseif ($page == "condi_general") {
    $title = _ttlcondiGeneral_;
    $description = _dsccondiGeneral_;
    $keywords = _keycondiGeneral_;
} elseif ($page == "protect_donnee") {
    $title = _ttlprotectDonnee_;
    $description = _dscprotectDonnee_;
    $keywords = _keyprotectDonnee_;
} elseif ($page == "faq") {
    $title = _ttlfaq_;
    $description = _dscfaq_;
    $keywords = _keyfaq_;
} elseif ($page == "contact") {
    $title = _ttlcontact_;
    $description = _dsccontact_;
    $keywords = _keycontact_;
} elseif ($page == "mon_compte") {
    $title = _ttlmonCompte_;
    $description = _dscmonCompte_;
    $keywords = _keymonCompte_;
} elseif ($page == "mon_abo") {
    $title = _ttlmonAbo_;
    $description = _dscmonAbo_;
    $keywords = _keymonAbo_;
} elseif ($page == "playlist_aff") {
    $title = _ttlplaylistAff_;
    $description = _dscplaylistAff_;
    $keywords = _keyplaylistAff_;
} elseif ($page == "parrainage2") {
    $title = "Dramapassion - Parrainage";
    $description = "Offre parrainage";
    $keywords = "dramas coréens, drama coréen, korean drama, série coréenne, k drama, VOSTFR, gratuit, streaming, télécharger";
}

$url_serv = $http3 . $_SERVER['REQUEST_URI'];
$url_replace = str_replace('/', '%2F', $url_serv);
$url_req = "https://www.facebook.com/sharer.php?u=http%3A%2F%2F" . $url_replace;


if ($page == "player") {
    $info = DramaInfo($dramaID);
    $titre = $info['titre'];
    $titre_clean = dramaLinkClean($titre);
    $url_face = $http . "drama/" . $dramaID . "/" . $titre_clean;
    $facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count" data-href="' . $url_face . '"></div></td>';
} elseif ($page == 'index') {
    $facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/dramapassion" ></div></td>';
} else {
    $facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div></td>';
    $url_face = $http2 . $_SERVER['REQUEST_URI'];
}
?>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <?php
        if ($detect_os->isTablet()) {
            if ($detect_os->isiOS()) {
                echo '<meta name="viewport" content="width=device-width,target-densitydpi=high-dpi,initial-scale=1.0,maximum-scale=1.4,minimum-scale=0.5">';
            } else {
                echo '<meta name="viewport" content="width=1040,target-densitydpi=device-dpi,initial-scale=1.5,maximum-scale=1.4,minimum-scale=0.5">';
            }
        } else {
            echo '<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.4,minimum-scale=0.5">';
        }
        ?>
        <meta name="theme-color" content="#FF0066">
        <script src="http://jwpsrv.com/library/H0L0+I2UEeKL4SIACpYGxA.js"></script>
        <!--mobile responsive-->
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

        <!--custom css-->
        <!-- css -->
        <link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css?<?php echo date('s'); ?>" type="text/css" media="screen" />
        <link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_free.css" type="text/css" media="screen" />
        <link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_bottom.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css?<?php echo time(); ?>" type="text/css" media="screen" />

        <link rel="stylesheet" href="<?php echo $server; ?>css/example/example.css" type="text/css">
        <!--Sandeep Css -->
        <link rel="stylesheet" href="dist/main.css">
        <link rel="stylesheet" href="dist/owl.carousel.min.css">
        <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
        <!--Sandeep Css -->
        <?php
        if ($page == 'orderdetail') {
            
        } else {
            ?>
            <link rel="stylesheet" href="<?php echo $server; ?>css/dropkick.css" type="text/css">
        <?php } ?>
        <link rel="stylesheet" href="<?php echo $server; ?>css/swipebox.css">
        <link rel="stylesheet" href="<?php echo $server; ?>css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Carter+One&v1' rel='stylesheet' type='text/css'>
        <style type="text/css">
            .dk_theme_black {
                background-image: url('<?php echo $http; ?>images/fond_menu_player.png');
                background-repeat: repeat-x;

                );
            }
            .dk_theme_black .dk_toggle,
            .dk_theme_black.dk_open .dk_toggle {
                background-color: transparent;
                color: #fff;
                text-shadow: none;
                text-align: right;
            }
            .dk_theme_black .dk_options a {
                background-color: #333;
                color: #fff;
                text-shadow: none;
            }
            .dk_theme_black .dk_options a:hover,
            .dk_theme_black .dk_option_current a {
                background-color: #969695;
                color: #fff;
                text-shadow: #604A42 0 1px 0;
            }
        </style>
        <link rel="shortcut icon" type="image/png" href="<?php echo $http; ?>logo.png" /> 
        <link rel="icon" type="image/png" href="<?php echo $http; ?>logo.png" /> 
        <title><?php echo $title; ?></title>
        <meta name="description" lang="fr" content="<?php echo $description; ?>">
        <meta name="keywords" lang="fr" content="<?php echo $keywords; ?>">
        <meta name="google-site-verification" content="SbdBgJew61e-YYEHq3Mb5FNdsqAOOO5eY3EmTTutQr0" />
        <meta name="author" content="Vlexhan">
        <meta name="date-creation-ddmmyyyy" content="21022012" />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 days" />
        <meta http-equiv="content-language" content="fr-fr" />

        <meta property="fb:admins" content="100000544554843" />

        <?php if ($sociallinks == true || $page == "index") { ?>
            <meta property="og:url" content="<?php
            if ($page == "index") {
                echo "http://www.facebook.com/dramapassion";
            } else {
                echo $url_face;
            }
            ?>" />
            <meta property="og:title" content="<?php echo $og_title; ?>"/>
            <meta property="og:type" content="website"/>
            <meta property="og:image" content="<?php echo $og_img; ?>"/>
            <meta property="og:site_name" content="Dramapassion"/>
            <meta property="og:description" content="<?php echo $og_desc; ?>"/>
        <?php } elseif ($page == "parrainage2") {
            ?>
            <meta property="og:title" content="Dramapassion - parrainage"/>
            <meta property="og:type" content="tv_show"/>
            <meta property="og:image" content="http://www.dramapassion.com/images/page_parrainageFB.jpg"/>
            <meta property="og:site_name" content="Dramapassion"/>
            <meta property="og:description" content="Deviens mon filleul sur Dramapassion en prenant un abonnement Privilège et bénéficie de deux semaines offertes !"/>
            <?php
        }

        if ($page == "player") {
            $crawler = 0;
            if (preg_match('/(bot|spider|yahoo)/i', $_SERVER["HTTP_USER_AGENT"]))
                $crawler = 1;
        }
        ?>
        <!-- script java -->

        <script src="<?php echo $server; ?>/js/scrollbar.js" type="text/javascript"></script>
        <script src="<?php echo $server; ?>/js/AC_OETags.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
        <script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
        <script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <?php if ($os == 'Ps' || $os == 'Android') { ?>
            <script type="text/javascript" src="<?php echo $http; ?>js/flowplayer-3.2.4.min.js"></script>		
        <?php } else { ?>
            <script type="text/javascript" src="<?php echo $http; ?>js/flowplayer-3.2.12.min.js"></script>	
        <?php } ?>
        <script type="text/javascript" src="<?php echo $http; ?>js/flash_detect_min.js"></script>
        <script type="text/javascript" src="<?php echo $http; ?>js/swfobject.js"></script>
        <script src="<?php echo $http; ?>js/jquery.dropkick-1.0.0.js" type="text/javascript" charset="utf-8"></script>

        <script type="text/javascript">
            $(function () {
                $('.custom_dropbox').dropkick({
                    theme: 'black',
                    change: function (value, label) {
                        var url_ajax = "<?php echo $http; ?>change_player.php?player=" + value;

                        $.ajax({
                            url: url_ajax
                        }).done(function () {
                            location.reload();
                            //window.location = url_location ;
                        });

                    }
                });


            });
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-12509314-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <?php if ($_SESSION['screen_size_x'] == '' && $_SESSION['screen_size_y'] == '') {
            ?>
            <script language="Javascript">
                $(document).ready(function () {
                    $.ajax({
                        type: "POST",
                        url: "temp_screen_size.php",
                        data: "x=" + screen.width + "&y=" + screen.height,
                    });
                });
            </script>
        <?php } ?>
        <script type='text/javascript'>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            (function () {
                var gads = document.createElement('script');
                gads.async = true;
                gads.type = 'text/javascript';
                var useSSL = 'https:' == document.location.protocol;
                gads.src = (useSSL ? 'https:' : 'http:') +
                        '//www.googletagservices.com/tag/js/gpt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(gads, node);
            })();
        </script>

        <script type='text/javascript'>
            googletag.cmd.push(function () {
                googletag.defineSlot('/20746576/home_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-0').addService(googletag.pubads());
                googletag.defineSlot('/20746576/small_rectangle_2', [250, 250], 'div-gpt-ad-1346334604979-1').addService(googletag.pubads());
                googletag.defineSlot('/20746576/small_rectangle_all_pages', [250, 250], 'div-gpt-ad-1346334604979-2').addService(googletag.pubads());
                googletag.defineSlot('/20746576/video_viewer_right_rectangle', [300, 250], 'div-gpt-ad-1346334604979-3').addService(googletag.pubads());
                googletag.defineSlot('/20746576/video_viewer_top_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-4').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>
        <link type="text/css" rel="stylesheet" href="<?php echo $http; ?>css/jquery.dropdown.css" />
        <script type="text/javascript" src="<?php echo $http; ?>js/jquery.dropdown.js"></script>


        <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
        <script type="text/javascript">
            //    window.cookieconsent_options = {"message":"Pour rendre votre visite plus agréable Dramapassion utilise des cookies.","dismiss":"J'ai compris","learnMore":"En savoir plus","link":"http://www.dramapassion.com/cookies","theme":"light-top"};

<?php
if ($os == 'Android') {
    $linkApp = 'http://play.google.com/store/apps/details?id=com.dramapassion.dramapassion';
    $TextStore = "Gratuit - Dans le Play Store"
    ?>
                $(document).ready(function () {
                    $('.conteneur_all_page').animate({
                        marginTop: 80
                    }, 500);
                    $('#AppPromo').show();
                    $('#CloseAppTag').click(function () {
                        $('#AppPromo').hide();
                        $('.conteneur_all_page').animate({
                            marginTop: 0
                        }, 500);
                    });
                });
    <?php
} elseif ($os == 'IOS') {
    $linkApp = "http://itunes.com/apps/Dramapassion";
    $TextStore = "Gratuit - Dans l'Apple Store";
    ?>
                $(document).ready(function () {
                    $('.conteneur_all_page').animate({
                        marginTop: 80
                    }, 500);
                    $('#AppPromo').show();
                    $('#CloseAppTag').click(function () {
                        $('#AppPromo').hide();
                        $('.conteneur_all_page').animate({
                            marginTop: 0
                        }, 500);
                    });
                });
    <?php
}
?>
        </script>

        <script type="text/javascript" src="<?php echo $http; ?>js/cookieconsent.min.js"></script>
        <!-- End Cookie Consent plugin -->



    </head>
    <body>
        <!--Start Header-->
        <header id="header">
            <div class="container">
                <div class="row">
                    <!--For Desktop menu-->
                    <?php
                    if ($page == "player") {
                        
                    } else {
                        ?>
                        <!--Logo & Category Section-->
                        <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                            <div class="logo"><a href="<?php echo $http; ?>" ><img alt="logo DramaPassion" title="Aller à la page d'accueil" src="<?php echo $http; ?>images/logo.png"></a></div>
                            <nav id="nav" class="hidden-sm">
                                <ul>
                                    <li><a href="<?php echo $http; ?>catalogue/">VIDEOS</a></li>
                                    <li><a href="<?php echo $http; ?>premium/">PREMIUM</a></li>
                                    <li><a href="<?php echo $http; ?>store/">STORE</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!--For End Desktop menu-->
                        <div class="logo hidden-lg hidden-md hidden-sm"><a href="<?php echo $http; ?>" ><img alt="logo DramaPassion" title="Aller à la page d'accueil" src="<?php echo $http; ?>images/logo.png" ></a></div>
                        <!--End Logo & Category Section-->
                        <?php
                    }
                    if ($page == "player") {
                        
                    } else {
                        ?>
                        <!--For Desktop menu-->
                        <!--Login & Search Section-->
                        <div class="col-md-6 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                            <div class="top_search">
                                <form action="<?php echo $http; ?>catalogue/" method="POST">
                                    <?php
                                    if ($_SESSION['zone_recherche'] != "" && $_SESSION['zone_recherche'] != "") {

                                        echo'<td><input type="text" name="recherche_top" id="top_recherche" class="input" value="' . $_SESSION['zone_recherche'] . '" /></td>';
                                    } else {

                                        echo'<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="input recherche_gris" value="Chercher un titre" /></td>';
                                    }
                                    ?>

                                                                                                                                                            <!--        <input src="<?php echo $http; ?>images/submit_search.png" type="image" />-->
                                    <input type="submit" value="search" class="button" />
                                </form>  
                            </div>
                            <div id="reg_area">
                                <?php
                                $userConnect = UserIsConnect();
                                if ($userConnect == 0) {
                                    ?>
                                    <ul>
                                        <li><a title="Me connecter" href="javascript:void()"  id="identifier" data-toggle="modal" data-target="#myLoginModal">S'identifier</a></li>
                                        <li><a title="Créer un compte" href="javascript:void(0);" data-toggle="modal" data-target="#myRegisterModal"  id="register" style="text-align: right">Créer un compte</a></li>
                                    </ul>




                                    <?php
                                } else {
                                    if (in_array($_SESSION['subscription'], $array_dis)) {
                                        $abo_statut = '[Découverte]';
                                    } elseif (in_array($_SESSION['subscription'], $array_pri)) {
                                        $abo_statut = '[Privilège]';
                                    } else {
                                        $abo_statut = '';
                                    }
                                    ?>

                                    <ul>
                                        <li><a href="#"><?php echo $_SESSION['username'] . " " . $abo_statut; ?>&nbsp;&nbsp;</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo $http; ?>compte/abonnement/">Mon Abonnement</a></li>
                                                <li><a href="<?php echo $http; ?>compte/parrainage/">Mon Parrainage</a></li>
                                                <li><a href="<?php echo $http; ?>compte/profil/">Mon Profil</a></li>
                                                <li><a href="<?php echo $http; ?>compte/paiements/">Mes Paiements</a></li>
                                                <li><a href="<?php echo $http; ?>compte/telechargements/">Mes Téléchargements</a></li>
                                            </ul> </li>
                                        <li><a href="<?php echo $http; ?>compte/playlist/">Ma Playlist</a> </li>
                                        <li><a href="<?php echo $http; ?>logoutAction.php">D�connexion</a> </li>
                                    </ul>
                                    <?
                                }
                                ?>


                            </div>
                        </div>
                        <!--End Login & Search Section-->
                        <!--For End Desktop menu-->

                        <?php
                    }
                    ?>

                    <!--For Ipad menu-->
                    <?php
                    if ($page = "player") {
                        
                    } else {
                        ?>
                        <div class="col-md-6 col-sm-6 col-xs-6 hidden-lg hidden-md hidden-xs posinher pull-right">
                            <div class="responsive-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="open-menu">
                                <ul>
                                    <li><a href="<?php echo $server; ?>catalogue/">VIDEOS</a></li>
                                    <li><a href="<?php echo $server; ?>premium/">PREMIUM</a></li>
                                    <li><a href="<?php echo $server; ?>store/">STORE</a></li>
                                    <?php
                                    $userConnect = UserIsConnect();
                                    if ($userConnect == 0) {
                                        if ($os == 'IOS' || $os == 'Android') {
                                            ?>
                                            <li><a title="Me connecter" href="javascript:void(0);" data-toggle="modal" data-target="#myIosLoginModal"  id="identifier">S'identifier</a></li>
                                            <li><a title="Créer un compte" href="javascript:void(0);" data-toggle="modal" data-target="#myIosRegisterModal"  id="registerrr" style="text-align: right">Créer un compte</a></li>
                                        <?php } else { ?>
                                            <li><a title="Me connecter" href="javascript:void(0);" data-toggle="modal" data-target="#myLoginModal"  id="identifier">S'identifier</a></li>
                                            <li><a title="Créer un compte" href="javascript:void(0);" data-toggle="modal" data-target="#myRegisterModal"  id="register" style="text-align: right">Créer un compte</a></li>
                                            <?php
                                        }
                                    } else {
                                        if (in_array($_SESSION['subscription'], $array_dis)) {
                                            $abo_statut = '[Découverte]';
                                        } elseif (in_array($_SESSION['subscription'], $array_pri)) {
                                            $abo_statut = '[Privilège]';
                                        } else {
                                            $abo_statut = '';
                                        }
                                        ?>

                                        <ul>
                                            <li><a href="#"><?php echo $_SESSION['username'] . " " . $abo_statut; ?>&nbsp;&nbsp;</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo $http; ?>compte/abonnement/">Mon Abonnement</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/parrainage/">Mon Parrainage</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/profil/">Mon Profil</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/paiements/">Mes Paiements</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/telechargements/">Mes Téléchargements</a></li>
                                                </ul> </li>
                                            <li><a href="<?php echo $http; ?>compte/playlist/">Ma Playlist</a> </li>
                                            <li><a href="<?php echo $http; ?>logoutAction.php">D�connexion</a> </li>
                                        </ul>
                                        <?
                                    }
                                    ?>
                                </ul>
                                <div class="top_search text-center hidden-sm">
                                    <form action="<?php echo $http; ?>catalogue/" method="POST">
                                        <?php
                                        if ($_SESSION['zone_recherche'] != "" && $_SESSION['zone_recherche'] != "") {

                                            echo'<td><input type="text" name="recherche_top" id="top_recherche" class="input" value="' . $_SESSION['zone_recherche'] . '" /></td>';
                                        } else {

                                            echo'<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="input recherche_gris" value="Chercher un titre" /></td>';
                                        }
                                        ?>

                                                                                                                                                        <!--        <input src="<?php echo $http; ?>images/submit_search.png" type="image" />-->
                                        <input type="submit" value="search" class="button" />
                                    </form>  
                                </div>
                            </div>
                            <div class="top_search hidden-lg">

                                <form action="<?php echo $http; ?>catalogue/" method="POST">
                                    <?php
                                    if ($_SESSION['zone_recherche'] != "" && $_SESSION['zone_recherche'] != "") {

                                        echo'<td><input type="text" name="recherche_top" id="top_recherche" class="input" value="' . $_SESSION['zone_recherche'] . '" /></td>';
                                    } else {

                                        echo'<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="input recherche_gris" value="Chercher un titre" /></td>';
                                    }
                                    ?>

                                                                                                                                                            <!--        <input src="<?php echo $http; ?>images/submit_search.png" type="image" />-->
                                    <input type="submit" value="search" class="button" />
                                </form>  
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <!--For End Ipad menu-->
                    <!--For mobile menu-->
                    <?php
                    if ($page = "player") {
                        
                    } else {
                        ?>

                        <div class="hidden-lg hidden-md hidden-sm posinher pull-right">
                            <div class="responsive-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="open-menu">

                                <ul>
                                    <li><a href="<?php echo $server; ?>catalogue/">VIDEOS</a></li>
                                    <li><a href="<?php echo $server; ?>premium/">PREMIUM</a></li>
                                    <li><a href="<?php echo $server; ?>store/">STORE</a></li>
                                    <?php
                                    $userConnect = UserIsConnect();
                                    if ($userConnect == 0) {
                                        if ($os == 'IOS' || $os == 'Android') {
                                            ?>
                                            <li><a title="Me connecter" href="javascript:void(0);" data-toggle="modal" data-target="#myIosLoginModal"  id="identifier">S'identifier</a></li>
                                            <li><a title="Créer un compte" href="javascript:void(0);" data-toggle="modal" data-target="#myIosRegisterModal"  id="registerrr" style="text-align: right">Créer un compte</a></li>

                                        <?php } else { ?>
                                            <li><a title="Me connecter" href="javascript:void(0);" data-toggle="modal" data-target="#myLoginModal"  id="identifier">S'identifier</a></li>
                                            <li><a title="Créer un compte" href="javascript:void(0);" data-toggle="modal" data-target="#myRegisterModal"  id="register" style="text-align: right">Créer un compte</a></li>
                                            <?php
                                        }
                                    } else {
                                        if (in_array($_SESSION['subscription'], $array_dis)) {
                                            $abo_statut = '[Découverte]';
                                        } elseif (in_array($_SESSION['subscription'], $array_pri)) {
                                            $abo_statut = '[Privilège]';
                                        } else {
                                            $abo_statut = '';
                                        }
                                        ?>

                                        <ul>
                                            <li><a href="#"><?php echo $_SESSION['username'] . " " . $abo_statut; ?>&nbsp;&nbsp;</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo $http; ?>compte/abonnement/">Mon Abonnement</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/parrainage/">Mon Parrainage</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/profil/">Mon Profil</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/paiements/">Mes Paiements</a></li>
                                                    <li><a href="<?php echo $http; ?>compte/telechargements/">Mes Téléchargements</a></li>
                                                </ul> </li>
                                            <li><a href="<?php echo $http; ?>compte/playlist/">Ma Playlist</a> </li>
                                            <li><a href="<?php echo $http; ?>logoutAction.php">D�connexion</a> </li>
                                        </ul>
                                        <?
                                    }
                                    ?>
                                </ul>

                                <div class="top_search text-center">
                                    <form action="<?php echo $http; ?>catalogue/" method="POST">
                                        <?php
                                        if ($_SESSION['zone_recherche'] != "" && $_SESSION['zone_recherche'] != "") {

                                            echo'<td><input type="text" name="recherche_top" id="top_recherche" class="input" value="' . $_SESSION['zone_recherche'] . '" /></td>';
                                        } else {

                                            echo'<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="input recherche_gris" value="Chercher un titre" /></td>';
                                        }
                                        ?>

                                                                                                                                                            <!--        <input src="<?php echo $http; ?>images/submit_search.png" type="image" />-->
                                        <input type="submit" value="search" class="button" />
                                    </form>  
                                </div>

                            </div>
                        </div>
                        <!--For End Mobile menu-->

                        <?php
                    }
                    ?>

                </div>
            </div>
        </header>
        <!--End Header-->
        <?php
        $msiecheck = $_SERVER['HTTP_USER_AGENT'];
        $msie7 = strpos($msiecheck, 'MSIE 7.0');
        $msie6 = strpos($msiecheck, 'MSIE 6.0');
        if ($msie7 != false || $msie6 != false) {
            $trident = strpos($msiecheck, 'Trident');
            if ($trident == false) {
                ?>
                <!--No Vedio found section ->
                
                
                
                
                <?php
            }
        }
        ?>
