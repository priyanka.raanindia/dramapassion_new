<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$dramaID = (int)cleanup($_GET['dramaID']);
	$userConnect = UserIsConnect();
	$titre_com = cleanup($_GET['titre_com']);
	$texte_com = cleanup($_GET['texte_com']);
	$erreur = cleanup($_GET['erreur']);
	
	if($erreur == 1){
		$erreur = "Votre commentaire est vide !";
	}elseif($erreur == 2){
		$erreur = "Le titre est vide !";
	}
	
	
		$texte_area = 'Merci d\'éviter les spoilers !';
		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_coms.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<style>
#legende_coms{
	font-size: 10px;
    color: black;
    margin-top: 3px;
    float:right;
}
#char_black{
    color: black;

}
::-webkit-input-placeholder { font-style: italic; }
::-moz-placeholder { font-style: italic; } /* firefox 19+ */
:-ms-input-placeholder { font-style: italic; } /* ie */
input:-moz-placeholder { font-style: italic; }
</style>
</head>
<body style="width:600px;">
	<div id="cont_iframe_signaler" style="width:600px;">
		<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
			<div id="cont_iframe_tot">
			<div id="titre_iframe_signaler">
			&nbsp;<span>Ajouter un commentaire</span>
			</div>
			<div id="logo_iframe_signaler">
			<img src="<?php echo $http ;?>images/pop_logo.png">
			</div>
			</div>
		</div>
	
<?php if($userConnect == 0){  
$header = 'Location: '.$http.'connexion.php?variable=2' ;
header($header);

}else{?>		
<div id="cont_cont_iframe">
	<div id="cont_coms" style="width:600px;">
	<div class="div_center" style="width:500px;">
	<span class="fancy_erreur"><?php echo $erreur ; ?></span>
	<form method="POST" action="<?php echo $http ; ?>ajout_coms_traitement.php" id="form_to_send">
	<div class="cont_liste" style="display:block;width:500px;clear:both;">
		<div>
		<div style="width:500px;margin-top:15px;text-transform: uppercase;font-weight: bold;font-family: Arial, Tahoma;font-size: 12px;">
		<p>Titre :</p>
		</div>
		<div style="width:500px;margin-top:3px;">
		<input type="text" style="width:494px;height:20px;" name="titre_com" id="titre_com" value="<?php echo $titre_com ; ?>" />
		</div>
		</div>
		<div
		<div style="width:500px;margin-top:15px;">
		<div><span style="float:left;text-transform: uppercase;font-weight: bold;font-family: Arial, Tahoma;font-size: 12px;">Commentaire : </span><spand id="legende_coms">max 500 caractères</span></div>
		</div>
		<div style="clear:both;"></div>
		<div style="width:500px;"><br />
		<p style="font-size:10px;font-style:italic;color:grey;">Cette case est exclusivement réservée aux commentaires sur le drama mis en ligne par Dramapassion. Tout renvoi vers d’autres sites internet sera automatiquement supprimé.<br />
En cas de liens répétitifs, l'accès à votre compte Dramapassion pourra être bloqué.<br />
Merci de votre compréhension.</p><br />
		<textarea rows="9" style="width:494px;font-size:12px;" name="texte_com" id="texte_com" placeholder="<?php echo $texte_area ;?>"><?php echo $texte_com ; ?></textarea>
		</div>
		</div>
<div style="clear:both;"></div>			
		
	
		</div>
		<input type="hidden" name="dramaID" value="<?php echo $dramaID ; ?>" />
		<div class="cont_send_bt" style="display:block;width:500px;margin-top:15px;margin-left:53px;text-align:right;">
		<a href="javascript:void();" onclick="fermer();" ><img src="<?php echo $http ;?>images/annuler.png" ></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" onclick="send_form();" ><img src="<?php echo $http ;?>images/envoyer_coms.png" ></a>
		</div>
	</form>
	</div>
	</div>
</div>		

			

	
<?php } ?>   	
	
	</div>
	
</form>
<script>
function fermer(){
	window.parent.$.fancybox.close();
}
 $("#texte_com").keyup(function(limit) {
                                                            
                                                            //Définir la limite à atteindre
                                                            var limit = "500";
                                                            
                                                            //Récupérer le nombre de caractères dans la zone de texte
                                                            var currlength = $(this).val().length;
                                                            
                                                            //Afficher un texte de légende en fonction du nombre de caractères
                                                            if(currlength >= limit){
                                                            var tot_char = limit - currlength ;
                                                            if(tot_char < 0 ){
                                                            tot_char = 0;
                                                            }
                                                            $("#legende_coms").html(tot_char + " <span id='char_black' >caractère restant</span>");
                                                            char_limit = $(this).val();
                                                            char_limit = char_limit.slice(0,char_limit.length-1);
                                                            $("#texte_com").val(char_limit);
                                                            }
                                                            else{
                                                            var tot_char = limit - currlength ;
                                                            if(tot_char < 0 ){
                                                            tot_char = 0;
                                                            }
                                                            $("#legende_coms").html(tot_char + " <span id='char_black' >caractères restants</span>");
                                                            }
                                                            
                                                            }); 

function send_form(){
	nb_caractere = $("#texte_com").val().length;
	
	if(nb_caractere > 501){
		alert('Vous avez dépassé le nombre de caractères autorisés \n nombre autorisé : 500 \n nombre écrit : '+nb_caractere);
	}else{
		$('#form_to_send').submit();
	}
	
}
</script>
</body>
</html>