<?php
class UserManager{
	private $_db;
	
	public function __construct($db){
    	$this->setDb($db);
	}
	
	public function get($id){
		$id = (int) $id;
		$donnees = array();
		if($id > 0){
		$q = $this->_db->query('SELECT * FROM t_dp_user WHERE UserID = '.$id);
		$donnees = $q->fetch(PDO::FETCH_ASSOC);
		}
		return new User($donnees);
	}
	
	
	public function setDb(PDO $db){
    	$this->_db = $db;
	}
}

?>