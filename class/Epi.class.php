<?php
class Epi{

	private $_EpisodeID;
	private $_DramaID;
	private $_EpisodeNumber;
	private $_EpisodeAdded;
	private $_ReleaseDate;
	private $_ReleaseDatePre;
	private $_ReleaseDateDec;
	private $_ReleaseDateFree;
	private $_ActifPre;
	private $_ActifDec;
	private $_ActifFree;
	private $_ActifMaint;
	private $_TimeEpi;
	private $_Part;
	private $_DatePriv;
	private $_DateDec;
	private $_DateFree;
	private $_DatePrivTime;
	private $_DateDecTime;
	private $_DateFreeTime;
	private $_HashDlPriv;
	private $_HashDlDec;
	private $_DramaShortcut;
	private $_DramaTitle;
	private $Secret = "DramaPassion2009";


	public function __construct(array $donnees){
    	$this->hydrate($donnees);
    	$this->DateAff();
    	$this->Hash_dl();
	}
	
	
	public function hydrate(array $donnees){
		  foreach ($donnees as $key => $value){
		    $method = 'set'.$key;
		         
		    if (method_exists($this, $method)){
		      $this->$method($value);
		    }
		  }
	}

	
	public function EpisodeID(){ return $this->_EpisodeID;}
	public function DramaID(){ return $this->_DramaID;}
	public function EpisodeNumber(){ return $this->_EpisodeNumber;}
	public function EpisodeAdded(){ return $this->_EpisodeAdded;}
	public function ReleaseDate(){ return $this->_ReleaseDate;}
	public function ReleaseDatePre(){ return $this->_ReleaseDatePre;}
	public function ReleaseDateDec(){ return $this->_ReleaseDateDec;}
	public function ReleaseDateFree(){ return $this->_ReleaseDateFree;}
	public function ActifPre(){ return $this->_ActifPre;}
	public function ActifDec(){ return $this->_ActifDec;}
	public function ActifFree(){ return $this->_ActifFree;}
	public function ActifMaint(){ return $this->_ActifMaint;}
	public function TimeEpi(){ return $this->_TimeEpi;}
	public function Part(){ return $this->_Part;}
	public function DramaShortcut(){ return $this->_DramaShortcut;}
	public function DramaTitle(){ return $this->_DramaTitle;}
	public function DatePriv(){return $this->_DatePriv;}
	public function DateDec(){return $this->_DateDec;}
	public function DateFree(){return $this->_DateFree;}
	public function DatePrivTime(){return $this->_DatePrivTime;}
	public function DateDecTime(){return $this->_DateDecTime;}
	public function DateFreeTime(){return $this->_DateFreeTime;}
	public function HashDlPriv(){return $this->_HashDlPriv;}
	public function HashDlDec(){return $this->_HashDlDec;}
	public function Secret(){return $this->_Secret;}
	
	
	public function Hash_dl(){
		$t = time();
		
		if($this->EpisodeNumber() < 10){
			$i_norm = "0".$this->EpisodeNumber() ;
		}else{
			$i_norm = $this->EpisodeNumber() ;
		}
		
		$f1 = "/".$this->DramaShortcut()."/".$this->DramaShortcut().$i_norm."-hd-dl.f4v#".$this->DramaTitle()."#".$i_norm;
		$f2 = "/".$this->DramaShortcut()."/".$this->DramaShortcut().$i_norm."-sd-dl.f4v#".$this->DramaTitle()."#".$i_norm;
		
		$hash_dl = base64_encode($t);
		$hash_dl = base64_encode($hash_dl);
	
		$hash_temp_hd = $hash_dl.";".$f1;
		$hash_temp_hd = $hash_temp_hd.";".md5($this->Secret());
		$hash_dl2_hd = base64_encode($hash_temp_hd);
		
		$hash_temp_sd = $hash_dl.";".$f2;
		$hash_temp_sd = $hash_temp_sd.";".md5($this->Secret());
		$hash_dl2_sd = base64_encode($hash_temp_sd);
	
		$sortie[1] = $hash_dl;
		$sortie[2] = $hash_dl2;
		$sortie[3] = $epi_id;
		$sortie[4] = $type;
		
		
	}
	
	public function DateAff(){
		$this->_DatePriv =  date("d-m-Y", strtotime($this->ReleaseDatePre()));
		$this->_DateDec =  date("d-m-Y", strtotime($this->ReleaseDateDec()));
		$this->_DateFree =  date("d-m-Y", strtotime($this->ReleaseDateFree()));
		$this->_DatePrivTime =  strtotime($this->ReleaseDatePre());
		$this->_DateDecTime = strtotime($this->ReleaseDateDec());
		$this->_DateFreeTime = strtotime($this->ReleaseDateFree());
		
	}
	
	public function setEpisodeID($id){
    	$id = (int) $id;
	    if ($id > 0){
	      $this->_EpisodeID = $id;
	    }
	}
	
	public function setDramaID($id){
    	$id = (int) $id;
	    if ($id > 0){
	      $this->_DramaID = $id;
	    }
	}
	
	public function setEpisodeNumber($nb){
    	$nb = (int) $nb;
	    if ($nb > 0){
	      $this->_EpisodeNumber = $nb;
	    }
	}
	
	public function setEpisodeAdded($date){
	    if (is_string($date)){
	      $this->_EpisodeAdded = $date;
	    }
	}
	
	public function setReleaseDate($date){
	    if (is_string($date)){
	      $this->_ReleaseDate = $date;
	    }
	}
	
	public function setReleaseDatePre($date){
	    if (is_string($date)){
	      $this->_ReleaseDatePre = $date;
	    }
	}
	
	public function setReleaseDateDec($date){
	    if (is_string($date)){
	      $this->_ReleaseDateDec = $date;
	    }
	}
	
	public function setReleaseDateFree($date){
	    if (is_string($date)){
	      $this->_ReleaseDateFree = $date;
	    }
	}
	
	public function setActifPre($nb){
    	$nb = (int) $nb;
	    if ($nb > 0){
	      $this->_ActifPre = $nb;
	    }
	}
	
	public function setActifDec($nb){
    	$nb = (int) $nb;
	    if ($nb > 0){
	      $this->_ActifDec = $nb;
	    }
	}
	
	public function setActifFree($nb){
    	$nb = (int) $nb;
	    if ($nb > 0){
	      $this->_ActifFree = $nb;
	    }
	}
	
	public function setActifMaint($nb){
    	$nb = (int) $nb;
	    if ($nb > 0){
	      $this->_ActifMaint = $nb;
	    }
	}
	
	public function setTimeEpi($nb){
    	$nb = (int) $nb;
	    if ($nb > 0){
	      $this->_TimeEpi = $nb;
	    }
	}
	
	public function setPart($nb){
    	$nb = (int) $nb;
	    if ($nb > 0){
	      $this->_Part = $nb;
	    }
	}
	
	public function setDramaShortcut($shortcut){
	    if (is_string($shortcut)){
	      $this->_DramaShortcut = $shortcut;
	    }
	}
	
	public function setDramaTitle($titre){
	    if (is_string($titre)){
	      $this->_DramaTitle = $titre;
	    }
	}
	
	public function AffTabEpi($drama,$user,$epis){
		$typeUserAbo = $user->typeAbo();
		$typeUserOs = $user->os();
		
		
		$sortie = '<table width="720" cellpadding="0" cellspacing="0" class="noir" border="0">'."\n";
		$sortie .= '<tr>';
				$i = 1;
				foreach ($epis as $unEpi){
					if($unEpi->EpisodeNumber() < 10){
						$i_norm = "0".$unEpi->EpisodeNumber() ;
					}else{
						$i_norm = $unEpi->EpisodeNumber() ;
					}
				
					if(($i %2) !=0 ){
						$style_modulo = 'style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF"';
					}else{
						$style_modulo = 'style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb"';
					}
					if($os == 'Win' || $os == 'Linux' || $os == 'Mac' || $os == 'non'){
							if($typeUserAbo == 'privilege' || $typeUserAbo == 'decouverte'){
							$sortie .= '<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	$sortie .= '<td valign="middle" width="110"><b>EPISODE</b></td>'."\n";
                        	$sortie .= '<td valign="middle" width="200"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	$sortie .= '<td valign="middle" width="200" class="or"><center><b>STREAMING</b></center></td>'."\n";
                        	$sortie .= '<td valign="middle" width="160" class="or"><center><b>TELECHARGEMENT</b></center></td>'."\n";  
			                $sortie .= '</tr>'."\n";
			                $sortie .= '<tr>'."\n";
			                $sortie .= '<td colspan="8"><img src="'.$drama->http().'images/ligne720.jpg" ></td>'."\n";
							$sortie .= '</tr>'."\n";
					
							$sortie .= '<tr class="tab_blanc"   '.$style_modulo.'>'."\n";
		                    $sortie .= '<td valign="top">&nbsp;</td>'."\n";
		                    $sortie .= '<td height="28" valign="middle">Episode '.$unEpi->EpisodeNumber().'</td>'."\n";
		                    
		                   
		                   
		                    $affDate = '';
		                    if($unEpi->DatePrivTime() < 1306000){
		                    	
		                    }else{
			                 	$affDate = $unEpi->DatePriv();
		                    }
							
							$sortie .= '<td height="25" valign="middle"><center>'.$affDate.'</center></td>'."\n";
							
							
							$affStreaming = '';
							$affDl = '';
							if($unEpi->DatePrivTime() < time() && $unEpi->DatePrivTime() >1306000 ){
								if($typeUserAbo == 'privilege'){
									$affStreaming = '<td height="25" valign="middle"><center><a href="'.$drama->http().'drama/'.$drama->DramaID().'/'.$drama->linkurl().'/'.$i_norm.'/" ><img  src="'.$drama->http().'images/play.png" border="0"></a></center></td>'."\n";
									$affDl = '<td height="25" valign="middle"><center><a target="_blank" href=""><img src="'.$drama->http().'images/bt_dl.png" border="0"></a></center></td>'."\n";
								}else{
									$affDl = '<td height="25" valign="middle"><center><a href="'.$drama->http().'premium/" title="Réservé aux abonnés Privilège"><img src="'.$drama->http().'images/bt_dl_gris.png" border="0"></a></center></td>'."\n";
									if($unEpi->DatePrivTime() < time() && $unEpi->DatePrivTime() >1306000 && $unEpi->EpisodeNumber() == 1){
										$affStreaming = '<td height="25" valign="middle"><center><a href="'.$drama->http().'drama/'.$drama->DramaID().'/'.$drama->linkurl().'/'.$i_norm.'/" ><img  src="'.$drama->http().'images/play.png" border="0"></a></center></td>'."\n";
									}else{
										if($unEpi->DateDecTime() < time()){
											$affStreaming = '<td height="25" valign="middle"><center><a href="'.$drama->http().'drama/'.$drama->DramaID().'/'.$drama->linkurl().'/'.$i_norm.'/" ><img  src="'.$drama->http().'images/play.png" border="0"></a></center></td>'."\n";
										}else{
											$affStreaming = '<td height="25" valign="middle"><center>'.$unEpi->DateDec().'</center></td>'."\n";
										}
									}
								}
							}elseif($unEpi->DatePrivTime() <1306000){
									$affStreaming = '<td height="25" valign="middle"><center></center></td>'."\n";
									$affDl = '<td height="25" valign="middle"><center></center></td>'."\n";
							}else{
								if($typeUserAbo == "decouverte"){
										$affStreaming = '<td height="25" valign="middle"><center>'.$unEpi->DateDec().'</center></td>'."\n";
										$affDl = '<td height="25" valign="middle"><center><a href="'.$drama->http().'premium/" title="Réservé aux abonnés Privilège"><img src="'.$drama->http().'images/bt_dl_gris.png" border="0"></a></center></td>'."\n";
								}else{
										$affStreaming = '<td height="25" valign="middle"><center>'.$unEpi->DatePriv().'</center></td>'."\n";
										$affDl = '<td height="25" valign="middle"><center>'.$unEpi->DatePriv().'</center></td>'."\n";
								}
							}	
							$sortie .= 	$affStreaming;
							$sortie .= 	$affDl;
							
							$sortie .=  '</tr>'."\n"; 
					
						
					
							
						}else{
							$sortie .= '<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	$sortie .= '<td valign="middle" width="90"><b>EPISODE</b></td>'."\n";
                        	$sortie .= '<td valign="middle" width="150"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	$sortie .= '<td valign="middle" width="130" class="or"><center><b>STREAMING</b></center></td>'."\n";
                        	$sortie .= '<td valign="middle" width="150" class="or"><center><b>TELECHARGEMENT</b></center></td>'."\n";
                        	$sortie .= '<td valign="middle" width="150" class="rose"><center><b>GRATUIT</b></center></td>'."\n"; 
                        	$sortie .= '</tr>'."\n";
			                $sortie .= '<tr>'."\n";
			                $sortie .= '<td colspan="8"><img src="'.$drama->http().'images/ligne720.jpg" ></td>'."\n";
							$sortie .= '</tr>'."\n";
					
							$sortie .= '<tr class="tab_blanc"   '.$style_modulo.'>'."\n";
		                    $sortie .= '<td valign="top">&nbsp;</td>'."\n";
		                    $sortie .= '<td height="28" valign="middle">Episode '.$unEpi->EpisodeNumber().'</td>'."\n";
		                    $affDate = '';
		                    if($unEpi->DatePrivTime() < 1306000){
		                    	
		                    }else{
			                 	$affDate = $unEpi->DatePriv();
		                    }
							
							$sortie .= '<td height="25" valign="middle"><center>'.$affDate.'</center></td>'."\n";
							$affStreaming = '<td height="25" valign="middle"><center><a href="'.$drama->http().'premium/" title="Réservé aux abonnés Privilège"><img src="'.$drama->http().'images/play_gris.png" border="0"></a></center></td>'."\n";
							$affDl = '<td height="25" valign="middle"><center><a href="'.$drama->http().'premium/" title="Réservé aux abonnés Privilège"><img src="'.$drama->http().'images/bt_dl_gris.png" border="0"></a></center></td>'."\n";
							$sortie .= 	$affStreaming;
							$sortie .= 	$affDl;
							
							if(($unEpi->DateFreeTime()<time() && $unEpi->DateFreeTime()>7872400) || ($unEpi->EpisodeNumber() == "1" && $unEpi->DateFreeTime()>7872400 && $unEpi->DatePrivTime()<time() && $unEpi->DatePrivTime()>1306000  )){
								$sortie .= '<td height="25" valign="middle"><center><a href="'.$drama->http().'drama/'.$drama->DramaID().'/'.$drama->linkurl().'/'.$i_norm.'/" ><img src="'.$drama->http().'images/bt_play_fr.png" border="0" /></a></center></td>'."\n";
							}elseif($unEpi->DateFreeTime()<7872400){
								$sortie .= '<td height="25" valign="middle"><center></center></td>'."\n";
							}else{
								$sortie .= '<td height="25" valign="middle"><center>'.$unEpi->DateFree().'</center></td>'."\n";
							}
							
							$sortie .= '</tr>'."\n";
							
		                    
		                    
						}
					}elseif($os == 'Ps' || $os == 'Wii'){
						$sortie .='<table width="720" cellpadding="0" cellspacing="0" class="noir" border="0">';
                        $sortie .='<tr>';
                        $sortie .='<td valign="middle" width="10">&nbsp;</td>';
                        $sortie .='<td valign="middle" width="90"><b>EPISODE</b></td>';
                        $sortie .='<td valign="middle" width="150"><center><b>DATE DE SORTIE</b></td>';
                        $sortie .='<td valign="middle" width="320" class="or"><center><b>STREAMING</b></center></td>';
                        $sortie .='<td valign="middle" width="150" class="rose"><center><b>GRATUIT</b></center></td>';                
						$sortie .='</tr>';
                        $sortie .='<tr>';
                        $sortie .='<td colspan="8"><img src="'.$drama->http().'images/ligne720.jpg"></td>';
						$sortie .='</tr>';
					}
					$i++;
				}
		
		
	}

	
}
?>