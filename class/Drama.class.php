<?php
class Drama{
	private $_http = "http://www.dramapassion.com/";
	private $_DramaID;
	private $_DramaTitle;
	private $_DramaTitle2;
	private $_DramaTitleFR;
	private $_DramaYear;
	private $_DramaEpisodes;
	private $_ProducerID;
	private $_LicensorID;
	private $_DramaSynopsisFre;
	private $_StatusID;
	private $_ReleaseDate;
	private $_DramaShortcut;
	private $_Categorie;
	private $_HD;
	private $_nb_photo;
	private $_photo_car;
	private $_mineur;
	private $_keyword;
	private $_linkurl;
	private $_imgBig;
	private $_imgThumb;
	private $_imgDetail;
	
	
	public function __construct(array $donnees){
    	$this->hydrate($donnees);
    	$this->setlinkurl();
	    $this->setimgThumb();
	    $this->setimgDetail();
	}
	
	
	public function hydrate(array $donnees){
		  foreach ($donnees as $key => $value){
		    $method = 'set'.$key;
		         
		    if (method_exists($this, $method)){
		      $this->$method($value);
		    }
		  }
	}
	
	
	public function DramaID(){ return $this->_DramaID;}
	public function DramaTitle(){ return $this->_DramaTitle;}
	public function DramaTitle2(){ return $this->_DramaTitle2;}
	public function DramaTitleFR(){ return $this->_DramaTitleFR;}
	public function DramaYear(){ return $this->_DramaYear;}
	public function DramaEpisodes(){ return $this->_DramaEpisodes;}
	public function ProducerID(){ return $this->_ProducerID;}
	public function LicensorID(){ return $this->_LicensorID;}
	public function DramaSynopsisFre(){ return $this->_DramaSynopsisFre;}
	public function StatusID(){ return $this->_StatusID;}
	public function ReleaseDate(){ return $this->_ReleaseDate;}
	public function DramaShortcut(){ return $this->_DramaShortcut;}
	public function Categorie(){ return $this->_Categorie;}
	public function HD(){ return $this->_HD;}
	public function nb_photo(){ return $this->_nb_photo;}
	public function photo_car(){ return $this->_photo_car;}
	public function mineur(){ return $this->_mineur;}
	public function keyword(){ return $this->_keyword;}
	public function linkurl(){ return $this->_linkurl;}
	public function imgThumb(){ return $this->_imgThumb;}
	public function imgDetail(){ return $this->_imgDetail;}
	public function http(){ return $this->_http;}
	
	public function setDramaID($id){
    	$id = (int) $id;
	    if ($id > 0){
	      $this->_DramaID = $id;
	    }
	}
	
	public function setDramaTitle($titre){
	    if (is_string($titre)){
	      $this->_DramaTitle = $titre;
	    }
	}
	
	public function setlinkurl(){
		$t1 = array(' ','\\','!',',','"','.','à',"'");
		$t2 = array('-','','','','','','','');
		$titre = str_replace($t1, $t2, $this->DramaTitle());
		$this->_linkurl = $titre;

	}
	
	public function setDramaTitle2($titre2){
	    if (is_string($titre2)){
	      $this->_DramaTitle2 = $titre2;
	    }
	}
	
	public function setDramaTitleFR($titreFR){
	    if (is_string($titreFR)){
	      $this->_DramaTitleFR = $titreFR;
	    }
	}
	
	public function setDramaYear($year){
	    $year = (int) $year;
	    if ($year > 0){
	      $this->_DramaYear = $year;
	    }
	}
	
	public function setDramaEpisodes($nbEpi){
		$nbEpi = (int) $nbEpi;
	    if ($nbEpi > 0){
	      $this->_DramaEpisodes = $nbEpi;
	    }
	}
	
	public function setProducerID($pid){
		$pid = (int) $pid;
	    if ($pid > 0){
	      $this->_ProducerID = $pid;
	    }
	}
	
	public function setLicensorID($lid){
		$lid = (int) $lid;
	    if ($lid > 0){
	      $this->_LicensorID = $lid;
	    }
	}
	
	public function setDramaSynopsisFre($syno){
		if (is_string($syno)){
	      $this->_DramaSynopsisFre = $syno;
	    }
	}
	
	public function setStatusID($sid){
		$sid = (int) $sid;
	    if ($sid > 0){
	      $this->_StatusID = $sid;
	    }
	}
	
	public function setReleaseDate($rdate){
		if (is_string($rdate)){
	      $this->_ReleaseDate = $rdate;
	    }
	}
	
	public function setDramaShortcut($short){
		if (is_string($short)){
	      $this->_DramaShortcut = $short;
	    }
	}
	
	public function setCategorie($cate){
		if (is_string($cate)){
	      $this->_Categorie = $cate;
	    }
	}
	
	public function setHD($hd){
		$hd = (int) $hd;
	    if ($hd > 0){
	      $this->_HD = $hd;
	    }
	}
	
	public function setnb_photo($nb_photo){
		$nb_photo = (int) $nb_photo;
	    if ($nb_photo > 0){
	      $this->_nb_photo = $nb_photo;
	    }
	}
	
	public function setphoto_car($photo_car){
		$photo_car = (int) $photo_car;
	    if ($photo_car > 0){
	      $this->_photo_car = $photo_car;
	    }
	}
	
	public function setmineur($mineur){
		$mineur = (int) $mineur;
	    if ($mineur >= 0){
	      $this->_mineur = $mineur;
	    }
	}
	
	public function setkeyword($key){
		if (is_string($key)){
	      $this->_keyword = $key;
	    }
	}
	public function imgBig($imgID){
	
		$t1 = array(' ','?');
		$t2 = array('%20','');
		$titre = str_replace($t1, $t2, $this->DramaTitle());
		if($imgID == 0){
			$url = $this->http()."content/dramas/".$titre."_Big.jpg";
		}else{
			$url = $this->http()."content/dramas/".$titre."_Big".$imgID.".jpg";
		}
		$this->_imgBig = $url;
		return $this->_imgBig;
	}
	
	public function setimgThumb(){
		$t1 = array(' ','?');
		$t2 = array('%20','');
		$titre = str_replace($t1, $t2, $this->DramaTitle());
		$url = $this->http()."content/dramas/".$titre."_Thumb.jpg";
		$this->_imgThumb = $url;
	}
	
	public function setimgDetail(){
		$t1 = array(' ','?');
		$t2 = array('%20','');
		$titre = str_replace($t1, $t2, $this->DramaTitle());
		$url = $this->http()."content/dramas/".$titre."_Detail.jpg";
		$this->_imgDetail = $url;
	}

}


?>