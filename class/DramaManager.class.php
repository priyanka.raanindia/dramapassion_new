<?php
class DramaManager{
	private $_db;
	
	public function __construct($db){
    	$this->setDb($db);
	}
	
	public function get($id){
		$id = (int) $id;
 
		$q = $this->_db->query('SELECT * FROM t_dp_drama WHERE DramaID = '.$id);
		$donnees = $q->fetch(PDO::FETCH_ASSOC);
		
		return new Drama($donnees);
	}
	
	public function getList(){
	    $drama = array();
	     
	    $q = $this->_db->query('SELECT * FROM t_dp_drama WHERE StatusID = 1');
	     
	    while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
	      $drama[] = new Drama($donnees);
	    }
	     
	    return $drama;
	}
	
	public function getNewDrama($type){
		$drama = array();
		if($type == "Pay"){	
			$q = $this->_db->query("SELECT d.* FROM t_dp_episode AS e, t_dp_drama AS d WHERE e.ReleaseDatePre <= NOW( ) AND e.DramaID = d.DramaID  GROUP BY e.DramaID ORDER BY DATE_FORMAT( MAX( e.ReleaseDatePre ) ,  '%Y-%m-%d' ) DESC  LIMIT 0 , 3");
			
		}elseif($type == "Free"){
			$q = $this->_db->query("SELECT d.* FROM t_dp_episode AS e, t_dp_drama AS d WHERE e.ReleaseDateFree <= NOW( ) AND e.DramaID = d.DramaID  GROUP BY e.DramaID ORDER BY DATE_FORMAT( MAX( e.ReleaseDateFree ) ,  '%Y-%m-%d' ) DESC  LIMIT 0 , 3");
		}
		
		while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
	      $drama[] = new Drama($donnees);
	    }
	     
	    return $drama;
		
		
	}
	

	
	
	public function setDb(PDO $db){
    	$this->_db = $db;
	}
}

?>