<?php
class EpiManager{
	private $_db;
	
	public function __construct($db){
    	$this->setDb($db);
	}
	
	public function getEpiDrama($drama){
		$id = $drama->DramaID();
		$id = (int) $id;
 
		$q = $this->_db->query('SELECT e.*,d.DramaShortcut,d.DramaTitle FROM t_dp_episode AS e, t_dp_drama AS d WHERE e.DramaID = '.$id.' AND e.DramaID = d.DramaID ORDER BY EpisodeNumber');
		
		while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
	      $epi[] = new Epi($donnees);
	    }
	     
	    return $epi;
		
	}	
	
	public function getLastEpiDrama($drama,$type){
		$id = $drama->DramaID();
		$id = (int) $id;
		if($type == "Pay"){	
			$q = $this->_db->query('SELECT * FROM t_dp_episode WHERE ReleaseDatePre = ( SELECT MAX( ReleaseDatePre ) FROM t_dp_episode WHERE DramaID = '.$id.' ) ORDER BY EpisodeNumber ');
		}elseif($type == 'Free'){
			$q = $this->_db->query('SELECT * FROM t_dp_episode WHERE ReleaseDateFree = ( SELECT MAX( ReleaseDateFree ) FROM t_dp_episode WHERE DramaID = '.$id.' ) ORDER BY EpisodeNumber ');
		}
		
		while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
	      $epis[] = $donnees;
	    }
	    
	    $nb_elem = count($epis);
		$min = $epis[0]['EpisodeNumber'];
		$max_temp = $nb_elem-1;
		$max = $epis[$max_temp]['EpisodeNumber'];
		
		$char = '<span class="new_epi align_logo_titre">Episodes '.$min.' - '.$max.'</span>';
		
		return $char;
		
	}
	
	public function setDb(PDO $db){
    	$this->_db = $db;
	}
}

?>