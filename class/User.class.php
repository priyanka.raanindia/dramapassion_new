<?php
require_once('../includes/settings.inc.php');
class User{
	private $_UserID;
	private $_LevelID;
	private $_UserName;
	private $_UserFname;
	private $_UserLname;
	private $_UserPassword;
	private $_UserEmail;
	private $_UserBalance;
	private $_SexID;
	private $_UserBirthdate;
	private $_CountryID;
	private $_UserProvince;
	private $_UserCity;
	private $_UserAddress1;
	private $_UserAddress2;
	private $_UserZip;
	private $_UserMobile;
	private $_UserAdded;
	private $_Active;
	private $_IP;
	private $_Alias;
	private $_SubscriptionID;
	private $_Next;
	private $_Expiry;
	private $_autorenewal;
	private $_userIDfacebook;
	private $_Paypal;
	private $_Last_connexion;
	private $_player_select;
	private $_newsletter;
	private $_uniqid;
	private $_qual;
	private $_os;
	private $_typeAbo;
	private $array_dis = array("1","2","5","7","8","9");
	private $array_pri = array("3","4","6","10");
	
	
	public function __construct(array $donnees){
    	$this->hydrate($donnees);
    	$this->osUser();
    	$this->typeAboUser();
	}
	
	
	public function hydrate(array $donnees){
		  foreach ($donnees as $key => $value){
		    $method = 'set'.$key;
		         
		    if (method_exists($this, $method)){
		      $this->$method($value);
		    }
		  }
	}
	
	
	public function UserID(){ return $this->_UserID;}
	public function LevelID(){ return $this->_LevelID;}
	public function UserName(){ return $this->_UserName;}
	public function UserFname(){ return $this->_UserFname;}
	public function UserLname(){ return $this->_UserLname;}
	public function UserPassword(){ return $this->_UserPassword;}
	public function UserEmail(){ return $this->_UserEmail;}
	public function UserBalance(){ return $this->_UserBalance;}
	public function SexID(){ return $this->_SexID;}
	public function UserBirthdate(){ return $this->_UserBirthdate;}
	public function CountryID(){ return $this->_CountryID;}
	public function UserProvince(){ return $this->_UserProvince;}
	public function UserCity(){ return $this->_UserCity;}
	public function UserAddress1(){ return $this->_UserAddress1;}
	public function UserAddress2(){ return $this->_UserAddress2;}
	public function UserZip(){ return $this->_UserZip;}
	public function UserMobile(){ return $this->_UserMobile;}
	public function UserAdded(){ return $this->_UserAdded;}
	public function Active(){ return $this->_Active;}
	public function IP(){ return $this->_IP;}
	public function Alias(){ return $this->_Alias;}
	public function SubscriptionID(){ return $this->_SubscriptionID;}
	public function Next(){ return $this->_Next;}
	public function Expiry(){ return $this->_Expiry;}
	public function autorenewal(){ return $this->_autorenewal;}
	public function userIDfacebook(){ return $this->_userIDfacebook;}
	public function Paypal(){ return $this->_Paypal;}
	public function Last_connexion(){ return $this->_Last_connexion;}
	public function player_select(){ return $this->_player_select;}
	public function newsletter(){ return $this->_newsletter;}
	public function uniqid(){ return $this->_uniqid;}
	public function qual(){ return $this->_qual;}
	public function os(){ return $this->_os;}
	public function typeAbo(){ return $this->_typeAbo;}
	
	
	public function osUser(){
		$serOs = $_SERVER['HTTP_USER_AGENT'];
		if(preg_match("#Windows Phone#", $serOs)){
			$os = 'Android';
		}elseif(preg_match("#Windows#", $serOs)){
			$os = 'Win';
		}elseif(preg_match("#iPad#", $serOs) || preg_match("#iPhone#", $serOs) || preg_match("#iPod#", $serOs)){
			$os = 'IOS';
		}elseif(preg_match("#Mac OS#", $serOs)){
			$os = 'Mac';
		}elseif(preg_match("#Android#", $serOs)){
			$os = 'Android';
		}elseif(preg_match("#Linux#", $serOs)){
			$os = 'Linux';
		}elseif(preg_match("#Ubuntu#", $serOs)){
			$os = 'Linux';
		
		}elseif(preg_match("#PLAYSTATION#", $serOs)){ 
			$os = 'Ps';
		}elseif(preg_match("#Wii#", $serOs)){ 
			$os = 'Wii';
		}elseif(preg_match("#PlayBook#", $serOs)){ 
			$os = 'Android';
		}elseif(preg_match("#Bada#", $serOs)){ 
			$os = 'Android';
		}elseif( preg_match('/(bot|spider|yahoo)/i',$serOs)){ 
			$os = 'crawler' ;
		}else{
			$os = 'Non';
		}
		
		$this->_os = $os;
	}
	
	public function typeAboUser(){
		if(in_array($this->SubscriptionID(), $this->array_dis)) {
			$sortie = "decouverte";
		}elseif(in_array($this->SubscriptionID(), $this->array_pri)) {
			$sortie = "privilege";
		}else{
			$sortie = "no";
		}
		$this->_typeAbo = $sortie;
	}
	
	public function setUserID($id){
    	$id = (int) $id;
	    if ($id > 0){
	      $this->_UserID = $id;
	    }
	}
	
	public function setLevelID($id){
	    $id = (int) $id;
	    if ($id > 0){
	      $this->_LevelID = $id;
	    }
	}
	
	public function setUserName($username){
	    if (is_string($username)){
	      $this->_UserName = $username;
	    }
	}
	
	public function setUserFname($userFname){
	    if (is_string($userFname)){
	      $this->_UserFname = $userFname;
	    }
	}
	
	public function setUserLname($userLname){
	    if (is_string($userLname)){
	      $this->_UserLname = $userLname;
	    }
	}
	
	public function setUserPassword($userpassword){
	    if (is_string($userpassword)){
	      $this->_UserPassword = $userpassword;
	    }
	}
	
	public function setUserEmail($useremail){
	    if (is_string($useremail)){
	      $this->_UserEmail = $useremail;
	    }
	}
	
	public function setUserBalance($balance){
    	$balance = (int) $balance;
	    if ($balance >= 0){
	      $this->_UserBalance = $balance;
	    }
	}
	
	public function setSexID($sex){
    	$sex = (int) $sex;
	    if ($sex > 0){
	      $this->_SexID = $sex;
	    }
	}
	
	public function setUserBirthdate($userannif){
	    if (is_string($userannif)){
	      $this->_UserBirthdate = $userannif;
	    }
	}
	
	public function setCountryID($id){
	    $id = (int) $id;
	    if ($id > 0){
	      $this->_CountryID = $id;
	    }
	}
	
	public function setUserProvince($userprovince){
	    if (is_string($userprovince)){
	      $this->_UserProvince = $userprovince;
	    }
	}
	
	public function setUserCity($usercity){
	    if (is_string($usercity)){
	      $this->_UserCity = $usercity;
	    }
	}
	
	public function setUserAddress1($useradd1){
	    if (is_string($useradd1)){
	      $this->_UserAddress1 = $useradd1;
	    }
	}
	
	public function setUserAddress2($useradd2){
	    if (is_string($useradd2)){
	      $this->_UserAddress2 = $useradd2;
	    }
	}
	
	public function setUserZip($zip){
    	$zip = (int) $zip;
	    if ($zip > 0){
	      $this->_UserZip = $zip;
	    }
	}
	
	public function setUserMobile($usermobile){
	    if (is_string($usermobile)){
	      $this->_UserMobile = $usermobile;
	    }
	}
	
	public function setUserAdded($useradd){
	    if (is_string($useradd)){
	      $this->_UserAdded = $useradd;
	    }
	}
	
	public function setActive($active){
    	$active = (int) $active;
	    if ($active >= 0){
	      $this->_Active = $active;
	    }
	}
	
	public function setIP($ip){
	    if (is_string($ip)){
	      $this->_IP = $ip;
	    }
	}
	
	public function setAlias($alias){
	    if (is_string($alias)){
	      $this->_Alias = $alias;
	    }
	}
	
	public function setSubscriptionID($id){
    	$id = (int) $id;
	    if ($id >= 0){
	      $this->_SubscriptionID = $id;
	    }
	}
	
	public function setNext($id){
    	$id = (int) $id;
	    if ($id >= 0){
	      $this->_Next = $id;
	    }
	}
	
	public function setExpiry($expiry){
	    if (is_string($expiry)){
	      $this->_Expiry = $expiry;
	    }
	}
	
	public function setautorenewal($auto){
    	$auto = (int) $auto;
	    if ($auto >= 0){
	      $this->_autorenewal = $auto;
	    }
	}
	
	public function setuserIDfacebook($idFace){
	    if (is_string($idFace)){
	      $this->_userIDfacebook = $idFace;
	    }
	}
	
	public function setPaypal($paypal){
    	$paypal = (int) $paypal;
	    if ($paypal >= 0){
	      $this->_Paypal = $paypal;
	    }
	}
	
	public function setLast_connexion($lastco){
	    if (is_string($lastco)){
	      $this->_Last_connexion = $lastco;
	    }
	}
	
	public function setplayer_select($player){
    	$player = (int) $player;
	    if ($player >= 0){
	      $this->_player_select = $player;
	    }
	}
	
	public function setnewsletter($news){
    	$news = (int) $news;
	    if ($news >= 0){
	      $this->_newsletter = $news;
	    }
	}
	
	public function setuniqid($uniq){
	    if (is_string($uniq)){
	      $this->_uniqid = $uniq;
	    }
	}
	
	public function setqual($qual){
    	$qual = (int) $qual;
	    if ($qual >= 0){
	      $this->_qual = $qual;
	    }
	}
	
	
	
}


?>