<?php
require_once("includes/settings.inc.php");
?>  
<div class="margepied_all_page"><!-- ne pas enlever cette marge et laisser en dernier  --></div>

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <a href="<?php echo $http; ?>" class="footer_logo"><img src="images/footer-logo.png" alt="" /></a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="blanck-area"></div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <ul>
                            <li><a href="<?php echo $http; ?>a-propos-de-DramaPassion/">A propos de Dramapassion</a></li>
                            <li><a href="<?php echo $http; ?>conditions-generales/">Condition d�utilisation et de vente</a></li>
                            <li><a href="<?php echo $http; ?>protection-des-donnees-personnelles/">Protection des donn�es personnelles</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <ul>
                            <li><a href="<?php echo $http; ?>aide-faq/1">Aide - FAQ</a></li>
                            <li><a href="<?php echo $http; ?>contactez-nous/">Contacter-nous</a></li>
                            <li><a href="<?php echo $http; ?>annonceurs">Publicit� - Annonceurs</a></li>
                        </ul>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p><a href="<?php echo $http; ?>" target="_blank">Dramapassion.com</a> est le premier site de VOD en ligne sp�cialis� dans les s�ries cor�ennes en version originale avec sous-titres fran�ais (VOSTFR).</p>
                        <p>&copy; Vlexhan Distribution SPRL, 2017, tous droits r�serv�s.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 text-center">
                <div class="fb-like-box fb_iframe_widget" data-href="https://www.facebook.com/dramapassion" data-width="250" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=344619609607&amp;color_scheme=light&amp;container_width=250&amp;header=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fdramapassion&amp;locale=fr_FR&amp;sdk=joey&amp;show_border=true&amp;show_faces=true&amp;stream=false&amp;width=250">
                    <span style="vertical-align: bottom; width: 250px; height: 230px;">
                        <iframe name="f2c304712283d94" width="250px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like_box Facebook Social Plugin" src="https://www.facebook.com/plugins/like_box.php?app_id=344619609607&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F0F7S7QWJ0Ac.js%3Fversion%3D42%23cb%3Df366446fa498058%26domain%3Dwww.dramapassion.com%26origin%3Dhttp%253A%252F%252Fwww.dramapassion.com%252Ff1c4bf98339a614%26relation%3Dparent.parent&amp;color_scheme=light&amp;container_width=250&amp;header=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fdramapassion&amp;locale=fr_FR&amp;sdk=joey&amp;show_border=true&amp;show_faces=true&amp;stream=false&amp;width=250" style="border: none; visibility: visible; width: 250px; height: 230px;" class="">
                        </iframe></span></div>
<!--                <a href="#"><img src="images/img-facebook.jpg" alt="" /></a>-->
            </div>
        </div>
    </div>
</div>

<!-- Code Google de la balise de remarketing -->
<!--------------------------------------------------
Les balises de remarketing ne peuvent pas être associées aux informations personnelles ou placées sur des pages liées aux catégories à caractère sensible. Pour comprendre et savoir comment configurer la balise, rendez-vous sur la page http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1026690034;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:none;">
    <img height="1" width="1" style="border-style:none;display:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1026690034/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<script>


    $(window).unload(function () {
        //alert("http://www.dramapassion.com/user_track_ajax.php?i=<?php echo base64_encode($_SESSION['id_track_user']); ?>");
        $.ajax({
            type: 'POST',
            url: "http://www.dramapassion.com/user_track_ajax.php?i=<?php echo base64_encode($_SESSION['id_track_user']); ?>",
            async: false
        });
    });
<?php
if ($_SESSION['userid'] == 3) {
    $url_page = $_SERVER['REQUEST_URI'];
    if ($url_page == '/') {
        $url_page_print = '';
    } else {
        $url_page_print = '&url=' . $url_page;
    }
    if ($_SESSION['subscription'] != 0) {
        $url_sub_print = '&a=1';
    }
    ?>

        $(document).click(function (e) {
            var ecran_y = $('body').height();
            var ecran_x = $('body').width();
            $.ajax({
                type: "POST",
                url: "http://www.dramapassion.com/click_track.php?x=" + e.pageX + "&y=" + e.pageY + "&s_x=" + ecran_x + "&s_y=" + ecran_y + '<?php echo $url_sub_print . $url_page_print; ?>',
                success: function () {


                }
            });

        });
        $('#free').mousedown(function (e) {
            var ecran_y = $('body').height();
            var ecran_x = $('body').width();
            var pathname = window.location.pathname;

            $.ajax({
                type: "POST",
                url: "http://www.dramapassion.com/click_track.php?x=" + e.pageX + "&y=" + e.pageY + "&s_x=" + ecran_x + "&s_y=" + ecran_y + '<?php echo $url_sub_print . $url_page_print; ?>',
                success: function () {


                }
            });
        });
        $('#StrobeMediaPlayback').mousedown(function (e) {
            var ecran_y = $('body').height();
            var ecran_x = $('body').width();
            var pathname = window.location.pathname;

            $.ajax({
                type: "POST",
                url: "http://www.dramapassion.com/click_track.php?x=" + e.pageX + "&y=" + e.pageY + "&s_x=" + ecran_x + "&s_y=" + ecran_y + '<?php echo $url_sub_print . $url_page_print; ?>',
                success: function () {


                }
            });
        });
        $('a').click(function (e) {
            if ($(this).hasClass('iframe')) {
            } else {
                e.preventDefault();
                var ecran_y = $('body').height();
                var ecran_x = $('body').width();
                var url = $(this).attr('href');
                $.ajax({
                    type: "POST",
                    url: "http://www.dramapassion.com/click_track.php?x=" + e.pageX + "&y=" + e.pageY + "&s_x=" + ecran_x + "&s_y=" + ecran_y + '<?php echo $url_sub_print . $url_page_print; ?>',
                    success: function () {
                        window.location.replace(url);

                    }
                });
            }
        });
    <?php
}
?>
    $(".tab_blanc").mouseover(function () {
        if ($(this).css("background-color") == 'rgb(170, 139, 51)') {

        } else {
            $(this).css("background-color", "#d6d6d6");
        }
    }).mouseout(function () {
        if ($(this).css("background-color") == 'rgb(170, 139, 51)') {

        } else {
            $(this).css("background-color", "#FFFFFF");
        }
    });
    $(".tab_gris").mouseover(function () {
        if ($(this).css("background-color") == 'rgb(170, 139, 51)') {

        } else {
            $(this).css("background-color", "#d6d6d6");
        }
    }).mouseout(function () {
        if ($(this).css("background-color") == 'rgb(170, 139, 51)') {

        } else {
            $(this).css("background-color", "#ecebeb");
        }
    });

    $(".identifier").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 400,
        'onClosed': function () {
            parent.location.reload(true);
        }

    });
    $("#identifier2").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 300,
        'onClosed': function () {
            //parent.location.reload(true);
            parent.location = "<?php echo $http; ?>";
        }

    });

    $("#register").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 450,
        'onClosed': function () {
            parent.location.reload(true);
        }
    });
    $(".playlist_a").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 200,

    });
    $(".playlist_a2").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 200,

    });
    $("#rec_facebook").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 450,
        'onClosed': function () {
            //parent.location.reload(true);
        }
    });
    $("#rec_tweet").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 450,
        'onClosed': function () {
            //parent.location.reload(true);
        }
    });
    $(".download").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 200,
        'onClosed': function () {
            //parent.location.reload(true);
        }
    });
    $("#test_HD2").fancybox({
        'href': '<?php echo $http; ?>test_video2.php?type=HD',
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 852,
        'height': 480,
        'padding': 0


    });
    $("#test_SD2").fancybox({
        'href': '<?php echo $http; ?>test_video2.php?type=SD',
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 852,
        'height': 480,
        'padding': 0


    });
    $(".player_flash_alert").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 600,
        'height': 200,
        'onClosed': function () {
            //parent.location.reload(true);
        }

    });
    $(".click_pub").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 500,
        'height': 200,
        'overlayShow': false


    });
    $("#pop_up_annonceurs").fancybox({
        'scrolling': 'no',
        'titleShow': false,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'width': 500,
        'height': 600


    });
    $(".playlist_a").mouseover(function () {
        var src = "<?php echo $http; ?>images/playlist_over.png";
        $(this).find(".playlist_img").attr("src", src);
    }).mouseout(function () {
        var src2 = "<?php echo $http; ?>images/playlist.png";
        $(this).find(".playlist_img").attr("src", src2);
    });
    $(".playlist_a2").mouseover(function () {
        var src = "<?php echo $http; ?>images/playlist_over.png";
        $(this).find(".playlist_img").attr("src", src);
    }).mouseout(function () {
        var src2 = "<?php echo $http; ?>images/playlist.png";
        $(this).find(".playlist_img").attr("src", src2);
    });
    function supprimer_gris_recherche() {

        document.getElementById('top_recherche').value = "";
        document.getElementById('top_recherche').style.color = "black";
    }
    function valide(type, nb) {

        if (nb < 10) {
            nb = "0" + nb;
        }
        if (type == 'sd' || type == 'auto_hd' || type == 'auto_sd') {
            type2 = 'hd';
        } else {
            type2 = type;
        }


        var hidden = "hidden_" + type2 + "_" + nb;
        var form = "form_" + type2 + "_" + nb;
        document.getElementById(hidden).value = type;

        document.forms[form].submit();

    }
    function valide2(type, nb, dramaID) {

        if (nb < 10) {
            nb = "0" + nb;
        }
        type2 = 'hd';

        var hidden = "hidden_" + type2 + "_" + dramaID + "_" + nb;

        var form = "form_" + type2 + "_" + dramaID + "_" + nb;

        document.getElementById(hidden).value = type;


        document.forms[form].submit();

    }
<?php
if ($cookiesIp != 'ok') {
    $date_devel = "ip=" . $_SERVER["REMOTE_ADDR"];
    ?>
        $.ajax({
            type: "POST",
            url: "http://devel.dramapassion.com/log_ip.php",
            data: "<?php echo $date_devel; ?>",
        });
    <?php
}
?>
</script>
<?php
if ($paques == 1) {
    $sqlPaquesResult = "SELECT * FROM paques INNER JOIN t_dp_user ON t_dp_user.UserID = paques.userfinded WHERE finded = 1 ORDER BY datefinded DESC";
    $rPaquesResult = mysql_query($sqlPaquesResult);
    $nbPaquesResult = 36 - mysql_num_rows($rPaquesResult);
    ?>
    <div id="paques" <?php
    if (isset($_SESSION['showPop']) && $_SESSION['showPop'] == 0) {
        echo 'class="showPop0"';
    }
    ?>>
        <div id="header-paques">
            <div id="header-paques-title">
                <i class="fa <?php
                if (isset($_SESSION['showPop']) && $_SESSION['showPop'] == 0) {
                    echo 'fa-caret-square-o-up';
                } else {
                    echo 'fa-caret-square-o-down';
                }
                ?>" id="header-paques-x"></i> Concours de Pâques &nbsp;&nbsp;<?php echo $nbPaquesResult; ?> <img src="<?php echo $http; ?>images/paques/Oeuf2.png" style="height: 16px;" /> restants
            </div>
            <img src="<?php echo $http; ?>images/paques/paquesHeader.png" id="imgpaquesheader" />
            <div id="header-paques-explications"><a href="<?php echo $http; ?>paquesExplication/" style="font-weight: bold;font-style: italic;">> Règles du jeu</a></div>
        </div>
        <div id="content-paques">
            <?php
            while ($row = mysql_fetch_array($rPaquesResult)) {
                ?>
                <div class="paques-gagnat">
                    <span style="color: #FF0066;"><?php echo $row['UserName']; ?></span> a gagné <span style="color: #FF0066;"><?php echo $row['Kdo']; ?> 
                </div>	
                <?php
            }
            ?>	
        </div>
    </div>	
    <script>
        $(document).ready(function () {
    <?php
    if (isset($_SESSION['showPop']) && $_SESSION['showPop'] == 0) {
        echo "paquesShow = 0;";
    } else {
        echo "paquesShow = 1;";
    }
    ?>

            $('#header-paques-x').click(function () {
                if (paquesShow == 1) {
                    $('#paques').height("20px");
                    $('#header-paques-x').removeClass("fa-caret-square-o-down").addClass("fa-caret-square-o-up");
                    paquesShow = 0;
                    $.get("<?php echo $http; ?>paques.php?showPop=0", function (data, status) {

                    });
                } else {
                    $('#paques').removeClass("showPop0")
                    $('#paques').css('height', '');
                    $('#header-paques-x').removeClass("fa-caret-square-o-up").addClass("fa-caret-square-o-down");
                    paquesShow = 1;
                    $.get("<?php echo $http; ?>paques.php?showPop=1", function (data, status) {

                    });
                }

            });
    <?php
    $sqlPaquesAll = "SELECT * FROM paques";
    $rPaquesAll = mysql_query($sqlPaquesAll);
    while ($rowAll = mysql_fetch_array($rPaquesAll)) {
        $timeTempRow = strtotime($rowAll['dateshow']);
        if (time() >= $timeTempRow || $_SESSION['userid'] == 3) {
            //isset($_SESSION['userid']) && $_SESSION['userid'] != ""
            if ($rowAll['url'] == 'all' || $rowAll['url'] == $_SERVER['REQUEST_URI']) {
                if (isset($_SESSION['userid']) && $_SESSION['userid'] != "") {
                    $tempID = $rowAll['name'];
                    echo "$('#" . $rowAll['name'] . "').attr('href', '" . $http . "paques.php?d=" . $tempID . "&t=1');";
                    echo "$('#" . $rowAll['name'] . "').fancybox({
                    'scrolling'		: 'no',
                    'titleShow'		: false,
                    'width'			: 600,
                    'height'		: 400
					
            });";
                } else {
                    echo "$('#" . $rowAll['name'] . "').attr('href', '" . $http . "connexionpaques.php');";
                    echo "$('#" . $rowAll['name'] . "').fancybox({
                    'scrolling'		: 'no',
                    'titleShow'		: false,
                    'transitionIn'	:	'elastic',
                    'transitionOut'	:	'elastic',
                    'width'			: 600,
                    'height'		: 400,
					
            });";
                }
            }
        }
    }
    ?>


        });

    </script>
    <?php
}
?>
<div class="clearfix"></div>
<!-- Start Modal -->
<div class="modal fade" id="myLoginModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <?php include_once 'connexion.php'; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--Ios/Android Modal-->
<div class="modal fade" id="myIosLoginModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <?php include_once 'connexion_mobile.php'; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="myRegisterModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!--                <h4 class="modal-title">Modal Header</h4>-->
            </div>
            <div class="modal-body">
                <?php include_once 'register.php' ?>;
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!---Ios Modal-->
<div class="modal fade" id="myIosRegisterModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!--                <h4 class="modal-title">Modal Header</h4>-->
            </div>
            <div class="modal-body">
                <?php include_once 'register_mobile.php' ?>;
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- End Modal -->
</body>
</html>
