<?php
require_once("includes/settings.inc.php");
require_once("includes/dbinfo.inc.php");
require_once("includes/functions.php");
require_once("top.php");
$new_epi_free = New_epi_drama("Free");
$new_epi_pay = New_epi_drama("Pay");
$recent_drama = recent_drama();
//echo "<pre>";print_r($recent_drama);die;
$tab_info_pay = DramaInfo($new_epi_pay[0]['dramaID']);
$tab_info_free = DramaInfo($new_epi_free[0]['dramaID']);

$chaine = $tab_info_pay['synopsis'];
$chaine2 = $tab_info_free['synopsis'];

$lg_max = 200; //nombre de caractère autoriser

if (strlen($chaine) > $lg_max) {
    $chaine = substr($chaine, 0, $lg_max);
    $last_space = strrpos($chaine, " ");
    $chaine = substr($chaine, 0, $last_space) . "...";
}
$synopsis = $chaine;
if (strlen($chaine2) > $lg_max) {
    $chaine2 = substr($chaine2, 0, $lg_max);
    $last_space = strrpos($chaine2, " ");
    $chaine2 = substr($chaine2, 0, $last_space) . "...";
}
$synopsis2 = $chaine2;
//print_r($_SESSION);
$os = getOs();
?>


<!--Start Slider-->
<?php if ($os == 'IOS' || $os == 'Android') {
    ?><div style="width:1000px;margin:auto;">
<?php } else {
    ?>

        <div style="width:1070px;margin:auto;">
        <?php } ?>
        <div id="slider">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="inner">
                            <div class="owl-carousel owl-theme">

                                <?php
                                if ($_SESSION['userid'] == 3) {
                                    Car_aff3();
                                } else {
                                    Car_aff3();
                                }
                                ?>

                                <!--end slide-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Slider-->

    <!--Start Social Media Button-->
    <div class="social_btns">
        <div class="container">
            <ul class="pull-right">
                <?php if ($page == "index") { ?>
                    <!--                    <div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/dramapassion" ></div>-->
                    <li><a href="" ><img src="images/btn-fb.jpg" alt="" /></a></li>
                <?php } else { ?>
                    <li><a href="" ><img src="images/btn-fb.jpg" alt="" /></a></li>

                    <!--                    <div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div>-->
                <?php } ?>

                <li><a href="https://twitter.com/share"><img src="images/btn-twitter.jpg" alt="" /></a><script>!function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//platform.twitter.com/widgets.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, "script", "twitter-wjs");</script></li>
            </ul>
        </div>
    </div>
    <!--End Social Media Button-->

    <!--Start Nouveaut�s Section-->
    <section class="row_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="main-title"><span class="center">Nouveaut�s</span></h2>
                </div>
                <div class="clearfix"></div>
                <?php
                foreach ($recent_drama as $key => $lst_drama) {
                    //print_r($lst_drama);
                    ?>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <a href="drama/<?php echo $lst_drama['dramaID'] . "/" . $lst_drama['link']; ?>" class="img"><img src="<?php echo $lst_drama['img']; ?>" alt="" /> </a>
                    </div>

                    <?php
                }
                ?>

            </div>
        </div>
    </section>
    <!--End Nouveaut�s Section-->

    <!--Start Prochainement  Section-->
    <section class="row_section">
        <div class="container">
            <div class="row">
                <?php comming_soon(); ?>


            </div>
        </div>
    </section>
    <!--End Prochainement Section-->

    <!--Start En ce moment Section-->
    <section class="row_section space-30">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="main-title"><span class="center">En ce moment </span></h2>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- Start Derni�res Sorties-->
                    <div class="latest_releases">
                        <span class="title">Derni�res Sorties <img src="images/icon-premium.png" alt="" class="pull-right" /> </span>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_pay[0]['dramaID'] . "/" . $new_epi_pay[0]['link']; ?>"><img src="<?php echo $http . $new_epi_pay[0]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_pay[0]['dramaID'] . "/" . $new_epi_pay[0]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_pay[0]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_pay[0]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_pay[1]['dramaID'] . "/" . $new_epi_pay[1]['link']; ?>"><img src="<?php echo $http . $new_epi_pay[1]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_pay[1]['dramaID'] . "/" . $new_epi_pay[1]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_pay[1]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_pay[1]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_pay[2]['dramaID'] . "/" . $new_epi_pay[2]['link']; ?>"><img src="<?php echo $http . $new_epi_pay[2]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_pay[2]['dramaID'] . "/" . $new_epi_pay[2]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_pay[2]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_pay[2]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_pay[3]['dramaID'] . "/" . $new_epi_pay[3]['link']; ?>"><img src="<?php echo $http . $new_epi_pay[3]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_pay[3]['dramaID'] . "/" . $new_epi_pay[3]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_pay[3]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_pay[3]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_pay[4]['dramaID'] . "/" . $new_epi_pay[4]['link']; ?>"><img src="<?php echo $http . $new_epi_pay[4]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_pay[4]['dramaID'] . "/" . $new_epi_pay[4]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_pay[4]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_pay[4]['char']; ?></span>
                            </div>
                        </div>


                    </div>
                    <!-- End Derni�res Sorties-->

                    <!-- Start TOP DE LA SEMAINE-->
                    <div class="latest_releases mid">
                        <?php
                        WeekTop10();
                        ?>

                    </div>
                    <!-- End TOP DE LA SEMAINE-->

                    <!-- Start Derni�res Sorties-->
                    <div class="latest_releases gratuit">
                        <span class="title">Derni�res Sorties <img src="images/icon-gratuit.png" alt="" class="pull-right" /> </span>

                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_free[0]['dramaID'] . "/" . $new_epi_free[0]['link']; ?>"><img src="<?php echo $http . $new_epi_free[0]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_free[0]['dramaID'] . "/" . $new_epi_free[0]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_free[0]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_free[0]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_free[1]['dramaID'] . "/" . $new_epi_free[1]['link']; ?>"><img src="<?php echo $http . $new_epi_free[1]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_free[1]['dramaID'] . "/" . $new_epi_free[1]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_free[1]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_free[1]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_free[2]['dramaID'] . "/" . $new_epi_free[2]['link']; ?>"><img src="<?php echo $http . $new_epi_free[2]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_free[2]['dramaID'] . "/" . $new_epi_free[2]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_free[2]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_free[2]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_free[3]['dramaID'] . "/" . $new_epi_free[3]['link']; ?>"><img src="<?php echo $http . $new_epi_free[3]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_free[3]['dramaID'] . "/" . $new_epi_free[3]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_free[3]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_free[3]['char']; ?></span>
                            </div>
                        </div>
                        <div class="post">
                            <div class="img"><a href="drama/<?php echo $new_epi_free[4]['dramaID'] . "/" . $new_epi_free[4]['link']; ?>"><img src="<?php echo $http . $new_epi_free[4]['img']; ?>" alt="" /></a></div>
                            <div class="text">
                                <span class="name"><a href="drama/<?php echo $new_epi_free[4]['dramaID'] . "/" . $new_epi_free[4]['link']; ?>" class="lien_menu_noir"><?php echo $new_epi_free[4]['drama']; ?></a></span>
                                <span class="sub_name"><?php echo $new_epi_free[4]['char']; ?></span>
                            </div>
                        </div>

                    </div>
                    <!-- End Derni�res Sorties-->
                </div>
            </div>
        </div>
    </section>
    <!--End En ce moment Section-->

    <!--Start Disponible sur Dramapassion -->
    <section class="row_section space-15">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <h2 class="main-title">
                        <span class="left">Disponible sur Dramapassion</span>
                        <a href="#" class="viewall">voir tous</a>
                    </h2>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-gong.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-hero.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-big-voice.jpg" alt="" /></a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-second-love.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-doctors.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-awards.jpg" alt="" /></a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-gong.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-hero.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-big-voice.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 text-center">
                    <?php AffPub("9"); ?>
                </div>
            </div>
        </div>
    </section>
    <!--End Disponible sur Dramapassion -->

    <!--Start Collections -->
    <section class="row_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="main-title">
                        <span class="center">Collections</span>
                        <a href="#" class="viewall">voir tous</a>
                    </h2>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img"><img src="images/img-zoom.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img"><img src="images/img-zoom.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="#" class="img"><img src="images/img-zoom.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Collections -->

    <!--Start Recommandations -->
    <section class="row_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="main-title">
                        <span class="center">Recommandations</span>
                    </h2>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <a href="#" class="img"><img src="images/img-big-voice.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-big-voice.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="#" class="img img2"><img src="images/Airport_Big.jpg" alt="" /></a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img-awards.jpg" alt="" /></a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="#" class="img img2"><img src="images/img--k2.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Recommandations -->

    <!--Start Bottom Image -->

    <div class="row_section space-30">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <a href="#"><img src="images/img-free-now.jpg" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <!--End Bottom Image -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo $http; ?>js/placeholders.min.js"></script>
    <script src="<?php echo $http; ?>js/owl.carousel.js"></script>
    <script src="<?php echo $http; ?>js/custom.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo $http; ?>js/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script>
                    $(document).ready(function () {
                        // Using default configuration
                        $('#carrousel_feed').carouFredSel({
                            direction: "left",
                            auto: {
                                timeoutDuration: 5000,
                                pauseOnHover: 'resume'
                            },
                            scroll: {
                                duration: 1000

                            },
                            prev: "#carrousel-prev",
                            next: "#carrousel-next"
                        });

<?php
foreach ($tab_trailer as $key => $value) {


    echo "
                     	
                                                        jwplayer('player_trailer_" . $value . "').setup({
                                                            file: '" . $tab_trailer_url[$value] . "',
                                                            title: 'Trailer - " . $tab_trailer_name[$value] . "',
                                                            width: '100%',
                                                            aspectratio: '16:9',
                                                            skin: 'five',
                                                            width: 0,
                                                            height: 0,
                                                            tracks: [{ 
                                                                        file: '" . $tab_trailer_shortcut[$value] . "', 
                                                                        label: 'Francais',
                                                                        kind: 'captions',
                                                                        'default': true 
                                                                }]
                                                        });
                                                        $('#btn_trailer_" . $value . "').click(function(){
                                                                    drama_id = $( this ).data('id');
                                                                    eta = $( this ).data('eta');
                                                                    btn_trailer = '" . $http . "images/btn_trailer.png';
                                                                    btn_image = '" . $http . "images/btn_affiche.png';
								
                                                                    if(eta == 0){
                                                                            $('#img_drama_'+drama_id).hide();
                                                                            $( this ).data('eta',1);
                                                                            $( this ).attr('src', btn_image);
                                                                            jwplayer('player_trailer_" . $value . "').resize(640,320);
                                                                            jwplayer('player_trailer_" . $value . "').play();
                                                                            $('#carrousel_feed').trigger('stop');
                                                                            jwplayer('player_trailer_" . $value . "').onComplete(function(){
		   
                                                                               setTimeout(function(){ 
                                                                                            $('#img_drama_'+drama_id).show();
                                                                                            $( '#btn_trailer_" . $value . "' ).data('eta',0);
                                                                                            $( '#btn_trailer_" . $value . "' ).attr('src', btn_trailer);
                                                                                            jwplayer('player_trailer_" . $value . "').resize(0,0);
                                                                                            jwplayer('player_trailer_" . $value . "').stop();
                                                                                            $('#carrousel_feed').trigger('play',true); 
                                                                               }, 2000);
									   
                                                                        });
									
                                                                    }else if(eta = 1){
                                                                            $('#img_drama_'+drama_id).show();
                                                                            $( this ).data('eta',0);
                                                                            $( this ).attr('src', btn_trailer);
                                                                            jwplayer('player_trailer_" . $value . "').resize(0,0);
                                                                            jwplayer('player_trailer_" . $value . "').stop();
                                                                            $('#carrousel_feed').trigger('play',true);
									
                                                                    }
								
								
								 
								
                                                            });
						
                            ";
}
?>



                    });
                    /*
                     var Global_src = "test" ;
                     $(".img_play_detail").mouseover(function() {
                     var srcIMG = $(this).find(".img_detail").attr("id");
                     Global_src = $(this).find(".img_detail").attr("src");
                     var src = "image_play.php?img="+srcIMG+"&type=detail";
                     $(this).find(".img_detail").attr("src", src);
                     }).mouseout(function(){
                     var src2 = Global_src;
                     
                     $(this).find(".img_detail").attr("src", src2);
                     });
                     $(".img_play_thumb").mouseover(function() {
                     var srcIMG = $(this).find(".img_thumb").attr("id");
                     Global_src = $(this).find(".img_thumb").attr("src");
                     var src = "image_play.php?img="+srcIMG+"&type=thumb";
                     $(this).find(".img_thumb").attr("src", src);
                     }).mouseout(function(){
                     var src2 = Global_src;
                     
                     $(this).find(".img_thumb").attr("src", src2);
                     });
                     */
                    $(".plus_up").mouseover(function () {
                        var src = "images/plus_up.jpg";
                        $(this).find(".plus_up_img").attr("src", src);
                    }).mouseout(function () {
                        var src2 = "images/plus.jpg";
                        $(this).find(".plus_up_img").attr("src", src2);
                    });
    </script>

    <!--Start Footer -->
    <?php include_once 'bottom.php'; ?>
    <!--End Footer -->

    <!--javascript-->
