<?php session_start();
require_once("includes/settings.inc.php");
include("includes/dbinfo.inc.php");
include("includes/functions.php");
$_SESSION['error']['dpusername'] = "" ;
$_SESSION['error']['dppassword'] = "" ;


if(isset($_POST['login'])){
	
	
	$valid = true;
	
	if(!isset($_POST['dpuser']) || strlen($_POST['dpuser']) == 0){
		$valid = false;
		$_SESSION['error']['dpusername'] = "Pseudo manquant";
		
	}
	if(!isset($_POST['dppassw']) || strlen($_POST['dppassw']) == 0){
		$valid = false;
		$_SESSION['error']['dppassword'] = "Mot de passe manquant";
	}
	$loginuser = mysql_query("SELECT * FROM t_dp_user WHERE UserName ='".cleanup($_POST['dpuser'])."'");
	if(mysql_num_rows($loginuser) != 1){
		$valid = false;
		$_SESSION['error']['dpusername'] = "Pseudo ou mot de passe incorrect";
		
	}else{
		if(md5(cleanup($_POST['dppassw'])) != mysql_result($loginuser,0,"UserPassword")){
			$valid = false;
			$_SESSION['error']['dppassword'] = "Pseudo ou mot de passe incorrect";
			
		}
	}
	
	if($valid){
		$resultuser = mysql_query("SELECT * FROM t_dp_user WHERE UserName = '".cleanup($_POST['dpuser'])."' AND UserPassword = '".md5(cleanup($_POST['dppassw']))."'") or die(mysql_error());
		$userexists = mysql_num_rows($resultuser);
		
		if($userexists == 1){
			if(mysql_result($resultuser,0,"Active") == 0){
				$active_get = 1;
				$_SESSION['un'] = mysql_result($resultuser,0,"UserID");
			}else{
				if(mysql_result($resultuser,0,"LevelID") == 5){
					// USERACCOUNT IS BLOCKED
					$_SESSION['active'] = false;
					$_SESSION['logged'] = false;
					$_SESSION['blocked'] = true;
					header("Location: ".$http."connexion.php");
					exit();
				}elseif(mysql_result($resultuser,0,"LevelID") == 0){
					// USERACCOUNT IS BLOCKED
					$_SESSION['active'] = false;
					$_SESSION['logged'] = false;
					$_SESSION['blocked'] = false;
					$_SESSION['userZero'] = true;
					header("Location: ".$http."connexion.php");
					exit();
				}else{
					$_SESSION['active'] = true;
					$_SESSION['logged'] = true;
					$_SESSION['level'] = mysql_result($resultuser,0,"LevelID");
					$_SESSION['userid'] = mysql_result($resultuser,0,"UserID");
					$_SESSION['username'] = mysql_result($resultuser,0,"UserName");
					$_SESSION['email'] = mysql_result($resultuser,0,"UserEmail");
					$_SESSION['balance'] = mysql_result($resultuser,0,"UserBalance");
					$resultcountry = mysql_query("SELECT CountryCode FROM t_dp_country WHERE CountryCode = '".mysql_result($resultuser,0,"CountryID")."'");
					if(mysql_num_rows($resultcountry)==1){
						$_SESSION['country'] = mysql_result($resultcountry,0,"CountryCode");
					}else{
						$_SESSION['country'] = getIp2Location($_SERVER['REMOTE_ADDR']);
					}
					$_SESSION['expiry'] = mysql_result($resultuser,0,"Expiry");
					if($_SESSION['expiry'] < date("Y-m-d")){
						$_SESSION['subscription'] = 0;
					}else{
						$_SESSION['subscription'] = mysql_result($resultuser,0,"SubscriptionID");
					}
					
					//if(mysql_result($resultuser,0,"IP") == ""){
					mysql_query("UPDATE t_dp_user SET IP = '".$_SERVER['REMOTE_ADDR']."' WHERE UserID = ".mysql_result($resultuser,0,"UserID")." LIMIT 1");
					//}
					
					if(isset($_POST['rememberme']) && $_POST['rememberme'] == 1){
						setcookie("dplogin", mysql_result($resultuser,0,"UserID")."_".md5(cleanup($_POST['dppassw'])), time()+60*60*24*7, "/");
					}else{
						setcookie("dplogin", mysql_result($resultuser,0,"UserID")."_".md5(cleanup($_POST['dppassw'])), time()+60*60*12, "/");
					}
					
					if($_SESSION['language'] == "Fre"){
						$lng = "Fre";	
					}else{
						$lng = "Fre";	
					}
					/*$activepoll = mysql_result(mysql_query("SELECT PollID FROM t_dp_poll_$lng WHERE PollStatus = 1 ORDER BY PollAdded DESC LIMIT 1"),0,"PollID");
					$voted = mysql_query("SELECT * FROM t_dp_pollvote_$lng WHERE PollID = $activepoll AND UserID = ".mysql_result($resultuser,0,"UserID")."");
					if(mysql_num_rows($voted) != 0){
						$_SESSION['voted'] = true;
					}*/
					
					if(mysql_result($resultuser,0,"UserBirthdate") == "" || mysql_result($resultuser,0,"SexID") == "" || mysql_result($resultuser,0,"CountryID") == "" ){
						header("Location: ".$http."firsttime.php");
						exit();
					}else{
							
							header("Location: ".$http."connexionAction.php");
							exit();
						
					}
					
				}
			}
		}else{
			$_SESSION['logged'] = false;
		}
	}else{
		$_SESSION['logged'] = false;
		header("Location: ".$http."connexion.php");
		exit;
	}
	unset($_POST['dppassw']);
}
header("Location: ".$http."connexion.php?active=".$active_get);


?>