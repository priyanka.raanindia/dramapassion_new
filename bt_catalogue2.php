<?php
	if(!session_id()) session_start();
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("bt_header.php");
	require_once("bt_top.php");
	
	
	$recherche = $_SESSION['cata_rech'];
	
	$lien_acteur = $_GET['name'];
	$lien_perso = $_GET['b'];
	$pub = $_GET['pub'];
	$acteur = $_GET['acteur'];
	
	$dramaTab = DramaInfoCata();
	
	$tabGenre = array();
	$genresql = mysql_query("SELECT * FROM t_dp_genrevideo");
	while($rowGenre = mysql_fetch_assoc($genresql)){
		$tabGenre[$rowGenre['GenreVideoID']] = $rowGenre['GenreVideoDescFre'];
	}
	
	$tabActor = array();
	$actorsql = mysql_query("SELECT * FROM `t_dp_actor` ORDER BY `ActorName` ");
	while($rowActor= mysql_fetch_assoc($actorsql)){
		$tabActor[$rowActor['ActorID']]= $rowActor['ActorName'];
	}
	
	
	/*
	echo '<pre>';
	print_r($dramaTab);
	echo '<pre>';
	*/
	
	if(isset($pub)){
		
	}else{
		
	}
	?>
	<div class="row">
		<div class="cata-btn col-xs-12">
		<form class="form-inline" id="form-input">
		  <div class="form-group cata-input" id="cont-cate">
		    <label for="input-cate">Catégorie</label>
		    <select class="form-control" id="input-cate">
			  <option value="all">Tout</option>
			  <option value="com">Comédie, Comédie Romantique</option>
			  <option value="drame">Drame</option>
			  <option value="hist">Historique</option>
			  <option value="act">Action, Thriller, Fantastique</option>
			</select>
		  </div>
		  <div class="form-group cata-input" id="cont-genre">
		    <label for="input-genre">Genre</label>
		    <select class="form-control" id="input-genre">
			  <option value="all">Tout</option>
			  <?php
				foreach($tabGenre as $k => $v){
					echo '<option value="'.$k.'">'.$v.'</option>';
				}  
			  ?>
			</select>
		  </div>
		  <div class="form-group cata-input" id="cont-annee">
		    <label for="input-annee">Année</label>
		    <select class="form-control" id="input-annee">
			  <option value="all">Tout</option>
			  <?php
				for($i=date('Y');$i>2003;$i--){
					echo '<option value="'.$i.'">'.$i.'</option>';
				}
			  ?>
			</select>
		  </div>
		  <div class="form-group cata-input" id="cont-acteur">
		    <label for="input-acteur">Acteur</label>
		    <select class="form-control" id="input-acteur">
			  <option value="all">Tout</option>
			  <?php
				foreach($tabActor as $k => $v){
					echo '<option value="'.$k.'">'.$v.'</option>';
				}  
			  ?>
			</select>
		  </div>
		  <div class="form-group cata-input" id="cont-mot">
		    <label for="input-mot">Mot clé</label>
		    <input type="text" class="form-control" id="input-mot" placeholder="Chercher">
		  </div>
		</form>
		</div>
		<?php
			//images/abonnement_1_mini.png
			//images/abonnement_2_mini.png
			
			foreach($dramaTab as $k => $v){
				echo '<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 cata-content" data-id="'.$k.'" data-titre="'.$v['titre'].'" data-cate="'.$v['cate'].'" data-genre="'.$v['genreID'].'" data-annee="'.$v['annee'].'" data-actor="'.$v['actorID'].'">';
				echo '<img src="'.$v['img_big'].'" class="cata-img" />';
				echo '<div class="cata-title rose">'.$v['titre'].'</div>';
				if($v['free'] == 1){
					echo '<div class="cata-episode new_epi"><img class="cata-logo-dp" src="'.$http.'images/abonnement_2_mini_2.png" /> '.$v['nb_epi'].' épisodes</div>';
				}else{
					echo '<div class="cata-episode new_epi"><img class="cata-logo-dp" src="'.$http.'images/abonnement_1_mini_2.png" /> '.$v['nb_epi'].' épisodes</div>';
				}
				if($v['cate'] != "null"){
					if($v['cate'] == "com"){
						echo '<div class="cata-cate">Comédie, Comédie Romantique</div>';
					}else if($v['cate'] == "drame"){
						echo '<div class="cata-cate">Drame</div>';
					}else if($v['cate'] == "hist"){
						echo '<div class="cata-cate">Historique</div>';
					}else if($v['cate'] == "act"){
						echo '<div class="cata-cate">Action, Thriller, Fantastique</div>';
					}
					
				}
				if($v['genre'] != "null"){
					$tempgenre = explode(";", $v['genre']);
					echo '<div class="cata-genre">';
					for($i=0;$i < count($tempgenre)-1;$i++){
						if($i == count($tempgenre)-2){
							echo $tempgenre[$i];
						}else{
							echo $tempgenre[$i]." , ";
						}
						
					}
					echo '</div>';
					
				}
				
				echo '</div>';
			}
			
			
		?>
		
	</div>
	<?php
	
	
	
?>	

</div>
<script>
$( document ).ready(function() {
	//$("ul").find("[data-slide='" + current + "']");
	checkCate = function(){	
	  $('.cata-content').filter(function(){
		  out = $(this).data('cate') === $( "#input-cate" ).val();
		  if(out == true){
			  out = false;
		  }else{
			   out = true;
		  }
		  
		  return out;
	  }).hide();	
	}
	checkGenre = function(){	
	  $('.cata-content').filter(function(){
		  string = $(this).data('genre');
		  if(string.indexOf($( "#input-genre" ).val()) > -1){
			  out=true;
		  }else{
			  out=false;
		  }
		  
		  
		  if(out == true){
			  out = false;
		  }else{
			   out = true;
		  }
		  
		  return out;
	  }).hide();	
	}
	
	checkAnnee = function(){	
	  $('.cata-content').filter(function(){
		  out = $(this).data('annee') == $( "#input-annee" ).val();
		  if(out == true){
			  out = false;
		  }else{
			   out = true;
		  }
		  
		  return out;
	  }).hide();	
	}
	
	checkActor = function(){	
	  $('.cata-content').filter(function(){
		  string = $(this).data('actor');
		  if(string.indexOf($( "#input-acteur" ).val()) > -1){
			  out=true;
		  }else{
			  out=false;
		  }
		  
		  
		  if(out == true){
			  out = false;
		  }else{
			   out = true;
		  }
		  
		  return out;
	  }).hide();	
	}
	
	checkMot = function(){	
	  $('.cata-content').filter(function(){
		  string = $(this).data('titre').toLowerCase();
		  if(string.indexOf($( "#input-mot" ).val().toLowerCase()) > -1){
			  out=true;
		  }else{
			  out=false;
		  }
		  
		  
		  if(out == true){
			  out = false;
		  }else{
			   out = true;
		  }
		  
		  return out;
	  }).hide();	
	}
	
	
	$( "#input-cate" ).change(function() {	
		checkCate();
	});
	$( "#input-genre" ).change(function() {	
		checkGenre();
	});
	$( "#input-annee" ).change(function() {	
		checkAnnee();
	});
	$( "#input-acteur" ).change(function() {	
		checkActor();
	});
	$( "#input-mot" ).keyup(function() {	
		checkMot();
	});
	
	$('#form-input').on('keyup keypress', function(e) {
	  var keyCode = e.keyCode || e.which;
	  if (keyCode === 13) { 
	    e.preventDefault();
	    return false;
	  }
	});
	
});
</script>
<?php require_once("bt_bottom.php");?>