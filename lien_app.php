<?php
include 'includes/Mobile_Detect.php';
$detect = new Mobile_Detect();

if($detect->isTablet()){
    if($detect->isiOS()){
    	header('Location: https://itunes.apple.com/fr/app/dramapassion/id641121786?mt=8');
    }
    if($detect->isAndroidOS()){
    	header('Location: https://play.google.com/store/apps/details?id=com.dramapassion.dramapassion');
    }
}else{
	header('Location: http://www.dramapassion.com/');
}

?>