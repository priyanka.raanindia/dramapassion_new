<?php 
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect_free.php");

	$hashEpi = Hash_free(10,35,1);	
	$url_free_epi = "http://nf04.dramapassion.com/".$hashEpi;
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>HTML 5 Video test</title>
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <style>
  #my_video_overlay{
	  height: 15px;
	  width: 100%;
	  display: none;
	  background-color: black;
	  color: white;
	  -moz-opacity: 0.50;
		-khtml-opacity: 0.50;
		opacity: 0.50;
		z-index: -1;
  }
  #my_video{
	  margin-top: -15px;
	  width: 640px;
	  height: 360px;
  }
  #cont_my_video{
	  width: 640px;
	  height: 360px;
  }
  </style>
</head>
<body>
<div id="cont_my_video">
<div id="my_video_overlay"></div>
<video id='my_video' controls height="360" width="640" autoplay>
      	<source id='mp4' src="<?php echo $url_free_epi ; ?>" type='video/mp4'>
</video>
</div>

<script src="player.js"></script>
</body>
</html>