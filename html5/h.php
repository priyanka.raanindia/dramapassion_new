<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>HTML5 Video Player</title>
	
	<style type="text/css">
		#my_video {
			display:none; /* NOTICE: the <video> object is hidden until the SDK or DOM is ready */
			height:320px;
			width:567px;
		}
	</style>

	<!-- 
		This example uses jquery to detect when the DOM is ready and dynamic load videoplaza sdk. Videoplaza sdk do not require jquery.
	-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

	<!-- 
		The player configuration (player.js) encapsulates all the code to start the player and initiate videoplaza sdk, it can be written in many ways, you should pick the best framework and pattern that suits your project. Videoplaza will not maintain player.js this file only purpose is to be used (cautiously) as a demo integration between your video player and videoplaza sdk.
	-->
	<script src="js/videoplaza/player.js"></script>

	<!-- 
		Loading videoplaza HTML5 sdk using a time-out, so if videoplaza sdk is too slow to load it will not block your player. I'm using jquery, but the same result can be achieved in many ways. You don't need to do it this way, but your code should be able to handle 404 and time-out requests somehow when loading the sdk.
	-->
	<script>
		// videoplaza sdk, 4 sec time-out, callback function
		loadScript("http://vp-validation.videoplaza.tv/proxy/html5-sdk/1/0.14.4.4-ea/html5-sdk-1.0.14.4.4-ea.min.js",4000,initVideoplaza);
	</script>

	<script>
		// *** START - videoplaza plugin config ***
		vpConfig = {
			vphost : 'http://be-dramapassion.videoplaza.tv',
			category : '',
			contentPartner : 'myContentPartner',
			contentForm : 'shortForm',
			contentId : 'myContentId',
			tags : ['specpost'],
			flags: [],
			playbackPosition: [] // mid-roll cue points, if any.
		}
		// *** END - videoplaza plugin config ***
	</script>

</head>
<body>

	<video id='my_video' controls preload='none' poster="poster.jpg">
      	<source id='mp4' src="http://cdn.videoplaza.tv/resources/media/more_like_trees_640x360.mp4" type='video/mp4'>
      	<source id='webm' src="http://cdn.videoplaza.tv/resources/media/more_like_trees_640x360.webm" type='video/webm'>
    </video>

  	<div id='companionMpu'>companion banner</div>

</body>
</html>