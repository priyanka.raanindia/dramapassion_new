<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");

	
	

	require_once("top.php");
	if(isset($_SESSION['userid'])){
	$os = getOs();
	if($os == "Android"){
		$check = strpos($_SERVER['HTTP_USER_AGENT'], 'Android 2');
		if($check != false){
			$old = true;
		}
	}

	$start = date("Y-m-d H:i:s");
	$startdate = date("Y-m-d");
	$starttime = date("H:i:s");	
	
	//URL security module
	$urlsec = Hash_st_HD(10,17,'hd');

	//URL pear module
	$hash = Hash_dl(10,17,'hd');
	$urlpear = "download.php?d=".$hash[1]."&v=".$hash[2];
	
	//URL streaming low quality
	$urlbd = Hash_st_HD(10,17,'bd');
	$urlbdmob = Hash_epi_IOS(10,17,'pd');
	
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Envoi des données relatives à votre connexion internet</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="contenu" style="text-align:justify;">
<h3>1. Introduction</h3>
<p>Les données recueillies sur cette page sont uniquement utilisées pour analyser la vitesse de connexion de nos utilisateurs.</p>
<p>La collection de ces données peut nous aider à mieux adapter nos services. Nous vous remercions pour votre coopération.</p>
<p>Nous vous recommandons de remplir ce formulaire plusieurs fois à différents moments de la journée.</p>
<br />
<br />
<p><b>Avant de procéder aux tests ci-dessous, veuillez vérifier :</b></p>
<ul>
<li><b>qu'aucun autre programme n'utilise internet en même temps</b></li>
<li><b>qu'aucun autre ordinateur n'utilise la même connexion internet en même temps</b></li>
<li><b>que votre boitier de la télévision numérique soit éteint (si vous utilisez la télévision et l'internet sur la même ligne)</b></li>
</ul>
<br />
<br />
<form name="test" type="get" action="<?php echo $http; ?>test_conn_form_action.php">
<h3>2. Avez-vous des difficultés à regarder les vidéos en streaming ?</h3>
<p>Répondez "oui" si le lancement de la vidéo prend plus de 30 secondes et si la vidéo se met en buffering de manière fréquente.</p>
<br /><p>&nbsp;&nbsp;&nbsp;- En qualité SD : <input name="sd" value="yes" type="radio" checked>&nbsp;Oui&nbsp;&nbsp;&nbsp;<input name="sd" value="no" type="radio">&nbsp;Non&nbsp;&nbsp;&nbsp;<input name="sd" value="?" type="radio">&nbsp;Je ne sais pas</p>
<br /><p>&nbsp;&nbsp;&nbsp;- En qualité HD : <input name="hd" value="yes" type="radio" checked>&nbsp;Oui&nbsp;&nbsp;&nbsp;<input name="hd" value="no" type="radio">&nbsp;Non&nbsp;&nbsp;&nbsp;<input name="hd" value="?" type="radio">&nbsp;Je ne sais pas</p>
<br />
<br />
<h3>3. Test de débit de la connexion internet</h3>
<p>Cliquez sur le bouton "Begin Test" au lien suivant et entrez le résultat dans la case.</p>
<p><i>iPhone/iPad : intallez et utilisez l'appli "DegroupTest". Alternativement, remplissez ce formulaire sur votre ordinateur.</i></p>
<br /><p>&nbsp;&nbsp;&nbsp;- Lien : <a href="http://www.ovh.net" target="_blank" class="lien_bleu"><b>cliquez ici pour le test</b></a>
&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;Résultat "Download Speed" :&nbsp;<input type="text" name="ovh" value="" size="15">&nbsp;Mbps</p>
<br />
<br />
<h3>4. Test de vitesse de téléchargement</h3>
<p>Cliquez sur chacun des liens ci-dessous un par un et entrez la vitesse à laquelle vous téléchargez chaque lien.
Entrez la vitesse de téléchargement 15 secondes après le lancement du téléchargement.
Vous pouvez arrêter le téléchargement après 15 secondes.</p>
<p><i>Android/iPhone/iPad : cette section n'est pas applicable. Alternativement, remplissez ce formulaire sur votre ordinateur.</i></p>
<br /><p>&nbsp;&nbsp;&nbsp;- Premier lien : <a href="<?php echo $server_hd; ?>testfile.f4v" class="lien_bleu"><b>cliquez ici</b></a>
&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;Vitesse de téléchargement :&nbsp;<input type="text" name="dldirect" value="" size="15">&nbsp;&nbsp;<input name="dldirectu" value="ko" type="radio" checked>&nbsp;ko/sec&nbsp;&nbsp;&nbsp;<input name="dldirectu" value="Mo" type="radio">&nbsp;Mo/sec</p>
<br /><p>&nbsp;&nbsp;&nbsp;- Deuxième lien : <a href="<?php echo $server_hd.$urlsec; ?>" class="lien_bleu"><b>cliquez ici</b></a>
&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;Vitesse de téléchargement :&nbsp;<input type="text" name="dlsec" value="" size="15">&nbsp;&nbsp;<input name="dlsecu" value="ko" type="radio" checked>&nbsp;ko/sec&nbsp;&nbsp;&nbsp;<input name="dlsecu" value="Mo" type="radio">&nbsp;Mo/sec</p>
<br /><p>&nbsp;&nbsp;&nbsp;- Troisième lien : <a href="<?php echo $server_hd.$urlpear; ?>" class="lien_bleu"><b>cliquez ici</b></a>
&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;Vitesse de téléchargement :&nbsp;<input type="text" name="dlpear" value="" size="15">&nbsp;&nbsp;<input name="dlpearu" value="ko" type="radio" checked>&nbsp;ko/sec&nbsp;&nbsp;&nbsp;<input name="dlpearu" value="Mo" type="radio">&nbsp;Mo/sec</p>
<br />
<br />
<h3>5. Test streaming</h3>
<p>Cliquez sur le bouton play de chaque vidéo et regardez si vous arrivez à regarder sans buffering fréquent.</p>
<br />
<p>Voyez-vous la vidéo ci-dessous sans buffering (sauf au démarrage) ?&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;<input name="bd" value="yes" type="radio" checked>&nbsp;Oui&nbsp;&nbsp;&nbsp;<input name="bd" value="no" type="radio">&nbsp;Non</p>
<p>Attendez jusqu'à 30 secondes pour le démarrage.</p>
<div style="width:320px;margin-top:5px;">
<?php if($os != "IOS" && $os != "Android"){ ?>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,1" width="320" height="180"> 
							<param name="movie" value="http://ns01.dramapassion.com/smp/StrobeMediaPlayback.swf"></param> 
							<param name="FlashVars" value="src=http://ns01.dramapassion.com/<?php echo $urlbd; ?>"></param>  
							<param name="allowFullScreen" value="true"></param> 
							<param name="allowscriptaccess" value="always"></param> 		
							<embed src="http://ns01.dramapassion.com/smp/StrobeMediaPlayback.swf" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="320" height="180" FlashVars="src=http://ns01.dramapassion.com/<?php echo $urlbd; ?>"> 
							</embed> 
</object>
<?php } else { ?>
<video width="320" height="180" controls="controls" type="video/mp4" src="http://ns01.dramapassion.com/<?php echo $urlbdmob; ?>">
</video>
<?php } ?>
</div>
<br />
<br />
<p>Voyez-vous la vidéo ci-dessous sans buffering (sauf au démarrage) ?&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;<input name="http" value="yes" type="radio" checked>&nbsp;Oui&nbsp;&nbsp;&nbsp;<input name="http" value="no" type="radio">&nbsp;Non</p>
<p>Attendez jusqu'à 30 secondes pour le démarrage.</p>
<div style="width:320px;margin-top:5px;">
<?php if($os != "IOS" && $os != "Android"){ ?>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,1" width="320" height="180"> 
							<param name="movie" value="http://ns04.dramapassion.com:8080/smp/StrobeMediaPlayback.swf"></param> 
							<param name="FlashVars" value="src=http://ns04.dramapassion.com/vod/_definst_/smil:test/test01-hd.smil/manifest.f4m"></param>  
							<param name="allowFullScreen" value="true"></param> 
							<param name="allowscriptaccess" value="always"></param> 		
							<embed src="http://ns04.dramapassion.com:8080/smp/StrobeMediaPlayback.swf" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="320" height="180" FlashVars="src=http://ns04.dramapassion.com/vod/_definst_/smil:test/test01-hd.smil/manifest.f4m"> 
							</embed> 
</object>
<?php } else { if($old == true){ ?>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,1" width="320" height="180"> 
							<param name="movie" value="http://ns04.dramapassion.com:8080/smp/StrobeMediaPlayback.swf"></param> 
							<param name="FlashVars" value="src=http://ns04.dramapassion.com:8080/test/test01-500.mp4"></param>  
							<param name="allowFullScreen" value="true"></param> 
							<param name="allowscriptaccess" value="always"></param> 		
							<embed src="http://ns04.dramapassion.com:8080/smp/StrobeMediaPlayback.swf" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="320" height="180" FlashVars="src=http://ns04.dramapassion.com:8080/test/test01-500.mp4"> 
							</embed> 
</object>
<?php } else { ?> 
<video width="320" height="180" controls="controls" type="video/mp4" src="http://ns04.dramapassion.com/vod/_definst_/smil:test/test01-hd.smil/playlist.m3u8">
</video>
<?php }} ?>
</div>

<br />
<br />
<?php if($os != "IOS" && $os != "Android"){ ?>
<p>Voyez-vous la vidéo ci-dessous sans buffering (sauf au démarrage) ?&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;<input name="rtmp" value="yes" type="radio" checked>&nbsp;Oui&nbsp;&nbsp;&nbsp;<input name="rtmp" value="no" type="radio">&nbsp;Non</p>
<p>Attendez jusqu'à 30 secondes pour le démarrage.</p>

<div style="display:block;width:320px;height:180px;cursor:default;" id="player"></div>

<script>
flowplayer("player", "http://www.dramapassion.com/swf/flowplayer.commercial-3.2.12.swf", {
	key: '#$8d427e6f20cb64d82ba',
	clip: {
       urlResolvers: 'bwcheck',
       provider: 'rtmp',
       autoPlay: false,
       scaling: 'fit',
 
       // available bitrates and the corresponding files. We specify also the video width
       // here, so that the player does not use a too large file. It switches to a
       // file/stream with larger dimensions when going fullscreen if the available bandwidth permits.
       bitrates: [
         { url: "mp4:test/test01-300", width: 224, bitrate: 364 },
	     { url: "mp4:test/test01-500", width: 288, bitrate: 574 },
         { url: "mp4:test/test01-800", width: 360, bitrate: 896 },
         {
            url: "mp4:test/test01-1200", width: 432, bitrate: 1246,
            isDefault: true
         },
         { url: "mp4:test/test01-1800", width: 540, bitrate: 1928 },
         { url: "mp4:test/test01-3200", width: 720, bitrate: 3328 }
       ]
    },
    plugins: {
 
        // bandwidth check plugin
        bwcheck: {
            url: "flowplayer.bwcheck-3.2.10.swf",
 
            // CloudFront uses Adobe FMS servers
            serverType: 'wowza',
 
            // we use dynamic switching, the appropriate bitrate is switched on the fly
            dynamic: true,
 
            netConnectionUrl: 'rtmpt://ns04.dramapassion.com/vod',
			
			qos: {
				bwUp: true,
				bwDown: true,
				frames: true,
				buffer: true,
				screen: true,
				maxUpSwitchesPerStream: 100,
				waitDurationAfterDownSwitch: 10000			
			}
        },
 
        // RTMP streaming plugin
        rtmp: {
            url: "http://www.dramapassion.com/swf/flowplayer.rtmp-3.2.10.swf",
            netConnectionUrl: 'rtmpt://ns04.dramapassion.com/vod'
        }
    }
});
</script>

<?php } else { ?>
<input type="hidden" name="rtmp" value="nav">
<?php } ?>
<br />
<br />
<h3>6. Informations complémentaires</h3>
<span>Votre fournisseur d'accès internet (FAI) : </span>
<select name="FAI">
  <option selected="selected" value ="Free">Free</option>
  <option value="Orange">Orange</option>
  <option value="SFR">SFR</option>
  <option value="Bouygues">Bouygues</option>
  <option value="Numéricable">Numéricable</option>
  <option value="Autre">Autre</option>
</select>
<br />
<p>- Si vous avez sélectionné "Autre", veuillez préciser votre FAI :&nbsp;<input type="text" name="FAI2"></p>
<br />
<p>Vote code postal :&nbsp;<input type="text" name="CP"></p>
<br />
<br />
<br />
<input type="hidden" name="start" value="<?php echo $start; ?>">
<input type="hidden" name="startdate" value="<?php echo $startdate; ?>">
<input type="hidden" name="starttime" value="<?php echo $starttime; ?>">
<input type="hidden" name="os" value="<?php echo $os; ?>">
<input src="<?php echo $http; ?>images/submit_send.png" type="image">

</form>

<br /><br /><br /><br /><br /><br /><br /><br />


</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php 
} else {
?>
<script>
$(document).ready(function() {
  $('#identifier').click();
});
</script>
<?
}
require_once("bottom.php"); 

?>