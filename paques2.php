<?php
	require_once("includes/settings.inc.php");	
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");



	require_once("top.php");
	

		
?>
<tr>
        <td valign="top">  
        <!-- BLOC JQUERY -->
	        <div style="width:1000px;margin:auto;">
	            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
	                <tr>
	                    <td colspan="4" height="10" bgcolor="#FFFFFF"><!-- ZONE BLANCHE --></td>
	                </tr>
	                
	                <tr>
	                    <td width="1000" valign="top">
		                
	                    <div style="width: 1000px;text-align: center;">
							<img src="http://www.dramapassion.com/images/paques/NL_Dramapassion_titre.png" style="width: 1000px;" />
						</div>
	                    </td>
	                </tr>
	                <tr>
	                    <td bgcolor="#FFFFFF" width="1000" height="10"><!-- ZONE BLANCHE --></td>
	                </tr>
	                <tr>
		                <td style="text-align: justify;font-size: 15px;">
			                Pendant deux semaines, Dramapassion met les petits paniers dans les grands pour vous proposer une énorme <span style="font-weight: bold">chasse aux œufs virtuelle</span> ! Le principe est simple : <span style="font-weight: bold">du mardi 22 mars 11h00 au mardi 5 avril 23h59</span>, rendez-vous sur Dramapassion.com (le site uniquement, pas l’application). <br />
Plus d’une <span style="font-weight: bold">trentaine d’œufs</span> comme celui-ci seront dissimulés au fur et à mesure partout sur le site :
							<div style="width: 1000px;text-align: center;">
								<img src="http://www.dramapassion.com/images/paques/160316oeuf.png" style="width: 640px; margin: auto;" />
							</div>
							
							Chaque œuf renferme un cadeau. Dès que vous en voyez un, <span style="font-weight: bold">cliquez simplement dessus et vous remporterez le cadeau correspondant</span>. Dès qu’un œuf est trouvé, les autres utilisateurs ne peuvent plus le remporter. Veillez donc à être <span style="font-weight: bold">le plus rapide</span> ! Un compte à rebours est même présent dans la pop-up en bas à droite de votre écran pour vous faire savoir combien d’œufs ont été trouvés, et sur combien vous pouvez encore vous jeter !<br /><br />

Et ce n’est pas tout ! Un « <span style="font-weight: bold">œuf d’or</span> » unique sera également dissimulé sur le site. Si vous êtes le premier à mettre la main dessus, vous remporterez un an d’abonnement à Dramapassion ainsi que 4 de nos coffrets DVD ! Préparez-vous à ouvrir grand les yeux ! <br /><br />

							<div style="width: 1000px;text-align: center;">
								<img src="http://www.dramapassion.com/images/paques/160316oeufdor.png" style="width: 640px; margin: auto;" />
							</div><br /><br />
							
							<span style="font-weight: bold">
							Sont donc à gagner :<br /><br />
 
-          1 abonnement Privilège d’un an à Dramapassion + 4 coffrets DVD (œuf d’or)<br />
-          10 abonnements Privilège d’un mois<br />
-          20 abonnements Privilège d’une semaine<br />
-          5 OST en partenariat avec <a href="http://www.AsiaWorldMusic.fr" target="_blank" style="text-decoration: none !important;color: #FF0066;">AsiaWorldMusic.fr</a></span><br /><br />

							<div style="width: 1000px;text-align: center;">
								<img src="http://www.dramapassion.com/images/paques/paques2016_dotations.png" style="width: 800px; margin: auto;" />
							</div><br /><br />
					<div style="width: 100%;text-align: center;">
						À vous de jouer !
					</div>		
 
<br /><br />
							
							
							
		                </td>
	                </tr>

	                
	               
	                <tr>
	                    <td bgcolor="#FFFFFF" width="1000"  height="10"><!-- ZONE BLANCHE --></td>
	                </tr>
	                <tr>
	                    <td bgcolor="#FFFFFF" width="1000" height="10"><!-- ZONE BLANCHE --></td>
	                </tr>
	                <tr>
	                    <td bgcolor="#FFFFFF" width="1000" height="10"><!-- ZONE BLANCHE --></td>
	                </tr>
	                
	                
	
				</table>
			</div>
        <!-- BLOC JQUERY -->     
        </td>
</tr>

	
<script>

$("#register2").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});$(".log").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		parent.location.reload(true);
	}
});
</script>


<?php 
	
	require_once("bottom.php"); 

?>