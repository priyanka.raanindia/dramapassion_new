<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- css -->
<link rel="StyleSheet" href="http://www.dramapassion.com/css/my_style.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="http://www.dramapassion.com/css/style_page_free.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="http://www.dramapassion.com/css/style_page_bottom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://www.dramapassion.com/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />

<link rel="stylesheet" href="http://www.dramapassion.com/css/example/example.css" type="text/css">
<link rel="stylesheet" href="http://www.dramapassion.com/css/dropkick.css" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Carter+One&v1' rel='stylesheet' type='text/css'>
  
  
  <style type="text/css">
    .dk_theme_black {
	  background-image: url('http://www.dramapassion.com/images/fond_menu_player.png');
	  background-repeat: repeat-x;
  
  );
}
  .dk_theme_black .dk_toggle,
  .dk_theme_black.dk_open .dk_toggle {
    background-color: transparent;
    color: #fff;
    text-shadow: none;
    text-align: right;
  }
  .dk_theme_black .dk_options a {
    background-color: #333;
    color: #fff;
    text-shadow: none;
  }
    .dk_theme_black .dk_options a:hover,
    .dk_theme_black .dk_option_current a {
      background-color: #969695;
      color: #fff;
      text-shadow: #604A42 0 1px 0;
    }
  </style>

<link rel="shortcut icon" type="image/png" href="http://www.dramapassion.com/logo.png" /> 
<link rel="icon" type="image/png" href="http://www.dramapassion.com/logo.png" /> 

<!-- meta -->
<title>Answer Me 1997 -  en VOSTFR - Episode 1</title>
<meta name="description" lang="fr" content="Tous les épisodes en VOSTFR gratuits - Answer Me 1997 - Sung Shi-Won, scénariste de 33 ans, n’est pas vraiment ce qu’on pourrait appeler un écrivain émérite. Mais qu’importe, ce soir c’est la réunion des anciens élèves de sa promotion. Et c�...">
<meta name="keywords" lang="fr" content="Answer Me 1997, drama coréen, korean drama, k drama, VOSTFR, gratuit, streaming, épisode, télécharger">
<meta name="google-site-verification" content="SbdBgJew61e-YYEHq3Mb5FNdsqAOOO5eY3EmTTutQr0" />
<meta name="author" content="Vlexhan">
<meta name="Publisher" content="Weaby.Be by G1 sprl" />
<meta name="date-creation-ddmmyyyy" content="21022012" />
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="3 days" />
<meta http-equiv="content-language" content="fr-fr" />

<meta property="fb:admins" content="100000544554843" />

<meta property="og:url" content="http://www.dramapassion.com/drama/106/Answer-Me-1997" />
<meta property="og:title" content="Answer Me 1997 -  Episode 01"/>
<meta property="og:type" content="tv_show"/>
<meta property="og:image" content="http://www.dramapassion.com/content/dramas/Answer%20Me%201997_Detail.jpg"/>
<meta property="og:site_name" content="DramaPassion"/>
<meta property="og:description" content="Sung Shi-Won, scénariste de 33 ans, n’est pas vraiment ce qu’on pourrait appeler un écrivain émérite. Mais qu’importe, ce soir c’est la réunion des anciens élèves de sa promotion. Et c�..."/>

<!-- script java -->
<script src="http://www.dramapassion.com//js/scrollbar.js" type="text/javascript"></script>
<script src="http://www.dramapassion.com//js/AC_OETags.js" type="text/javascript"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery-ui.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/functions.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="http://www.dramapassion.com/js/flowplayer-3.2.11.min.js"></script>	
<script type="text/javascript" src="http://www.dramapassion.com/js/flash_detect_min.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/swfobject.js"></script>
<script src="http://www.dramapassion.com/js/jquery.dropkick-1.0.0.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
$(function () {

	$('.custom_dropbox').dropkick({
        theme : 'black',
        change: function (value, label) {
          var url_ajax = "http://www.dramapassion.com/change_player.php?player="+value ;
          
          $.ajax({
	          url: url_ajax
	      }).done(function() {
		      location.reload();
		//window.location = url_location ;
		});
          
        }
    });
        

});
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12509314-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
googletag.cmd.push(function() {
googletag.defineSlot('/20746576/home_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-0').addService(googletag.pubads());
googletag.defineSlot('/20746576/small_rectangle_2', [250, 250], 'div-gpt-ad-1346334604979-1').addService(googletag.pubads());
googletag.defineSlot('/20746576/small_rectangle_all_pages', [250, 250], 'div-gpt-ad-1346334604979-2').addService(googletag.pubads());
googletag.defineSlot('/20746576/video_viewer_right_rectangle', [300, 250], 'div-gpt-ad-1346334604979-3').addService(googletag.pubads());
googletag.defineSlot('/20746576/video_viewer_top_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-4').addService(googletag.pubads());
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="conteneur_all_page">
<div class="contenant_all_page">
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '344619609607', // App ID
      channelUrl : 'http://www.dramapassion.com', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/fr_FR/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "15796249" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=15796249&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- ImageReady Slices (Sans titre-1) -->


<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top" height="93" style="background-image:url(http://www.dramapassion.com/images/header.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER GRIS-->
        
        <div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" height="93" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="368" height="93"><a href="http://www.dramapassion.com/" ><img alt="logo DramaPassion" title="Aller à la page d'accueil" src="http://www.dramapassion.com/images/logo.png" width="368" height="93" alt="Logo Dramapassion" title="Logo Dramapassion" border="0"></a></td>
                    <td width="124" height="93"><a href="http://www.dramapassion.com/catalogue/" ><img alt="catalogue des séries" title="Voir tout le catalogue" src="http://www.dramapassion.com/images/video.png" width="124" height="93" alt="Vidéo on Dramapassion" title="video" border="0" onMouseOver="this.src='http://www.dramapassion.com/images/video_up.png'" onMouseOut="this.src='http://www.dramapassion.com/images/video.png'"></a></td>
                    <td width="141" height="93"><a href="http://www.dramapassion.com/premium/" ><img alt="premium" title="Voir les abonnements" src="http://www.dramapassion.com/images/premium.png" width="141" height="93" alt="Premium account" title="premium" border="0" onMouseOver="this.src='http://www.dramapassion.com/images/premium_up.png'" onMouseOut="this.src='http://www.dramapassion.com/images/premium.png'"></a></td>
                    <td width="90" height="93"><a href="http://www.dramapassion.com/dvd/" ><img alt="DVD" title="Voir les coffrets DVD" src="http://www.dramapassion.com/images/dvd.png" width="90" height="93" alt="DVD" title="dvd" border="0" onMouseOver="this.src='http://www.dramapassion.com/images/dvd_up.png'" onMouseOut="this.src='http://www.dramapassion.com/images/dvd.png'"></a></td>
                    <td width="277" height="93" valign="middle" align="right">
                    	<form action="http://www.dramapassion.com/catalogue/" method="POST">
                        <table height="93" cellpadding="0" cellspacing="0" width="220">
                        <!-- MOTEUR DE RECHERCHE -->
							
                        	<tr>
							
							<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="form_recherche recherche_gris" value="Chercher un titre" /></td>							
                                <td><input src="http://www.dramapassion.com/images/submit_search.png" type="image" /></td>
							
							</tr>
							
		                                     
                        <!-- FIN MOTEUR DE RECHERCHE -->                              
						</table>  
                        </form>                                                          
                    </td>
                </tr>
            </table>
		</div>
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
    
	<tr>
        <td valign="top" height="37" style="background-image:url(http://www.dramapassion.com/images/header2.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				                    					<td class="blanc" width="130">
					<div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div></td>
                    					<td class="blanc" width="122"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
										<td class="blanc" width="763" align="right"><a href="http://www.dramapassion.com/compte/abonnement/" title="Gérer mon compte" class="lien_blanc" style="text-align: right">Oya [Privilège]&nbsp;&nbsp;</a><td>
					<td class="blanc" width="5"><img src="http://www.dramapassion.com/images/barre.png" align="absmiddle"></td>
					<td class="blanc bloc_center" width="70"><a href="http://www.dramapassion.com/compte/playlist/" title="Voir ma playlist" class="lien_blanc">Ma playlist</a><td>
					<td class="blanc" width="5"><img src="http://www.dramapassion.com/images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="70" align="right"><a href="http://www.dramapassion.com/logoutAction.php" title="Me déconnecter" class="lien_blanc" style="text-align: right">Déconnexion</a><td>
					
					
				                                      
                                       
                </tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>

	<tr>
        <td valign="top" height="545" style="background-image:url(http://www.dramapassion.com/images/player_bg.png); background-repeat:repeat-x;">  
		<script type="text/javascript"> 
    if(FlashDetect.versionAtLeast(10, 1)){
		
        
    }else{
        (function($){

                $(function(){
						
                        $(".player_flash_alert").click();
						$(".player_afficher").css({ display: "none"})

                });

        })(jQuery);
    }   
   </script> 
  
<script type="text/javascript" src="http://www.dramapassion.com/js/flowplayer-3.2.11.min.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/functions.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/swfobject.js"></script>
<script type="text/javascript" src="http://www.dramapassion.com/js/ParsedQueryString.js"></script>
<script>
$(function () {
$('.custom_dropbox').change(function() {
	var player = $('.custom_select option:selected').val();
	var url_ajax = "http://www.dramapassion.com/change_player.php?player="+player ;
  	//var url_location = "http://www.dramapassion.com/player_HD2.php?player_type="+player+"&dramaID=106&epiNB=01&type=&user=86";
	$.ajax({
		url: url_ajax
	}).done(function() {
		location.reload();
		//window.location = url_location ;
	});
	
});
});

</script><script type="text/javascript">

        	function loadStrobeMediaPlayback()
        	{
	            // Collect query parameters in an object that we can
	            // forward to SWFObject:
            
	            var pqs = new ParsedQueryString();
	            var parameterNames = pqs.params(false);
	            
	            var parameters = {
	                src: "http://ns209333.ovh.net/hds-vod/mq01.f4m",
	                autoPlay: false,
	                dynamicStreamBufferTime: 9,
					//dvrBufferTime: 12,
					//liveDynamicStreamingBufferTime: 12,
	                controlBarAutoHide: true,
	                playButtonOverlay: true,
	                showVideoInfoOverlayOnStartUp: false,
	                javascriptCallbackFunction: "onJavaScriptBridgeCreated"	
	            };
	            
	            for (var i = 0; i < parameterNames.length; i++) {
	                var parameterName = parameterNames[i];
	                parameters[parameterName] = pqs.param(parameterName) ||
	                parameters[parameterName];
	            }
	            
	          
	            
	            var wmodeValue = "direct";
	            var wmodeOptions = ["direct", "opaque", "transparent", "window"];
	            if (parameters.hasOwnProperty("wmode"))
	            {
	            	if (wmodeOptions.indexOf(parameters.wmode) >= 0)
	            	{
	            		wmodeValue = parameters.wmode;
	            	}	            	
	            	delete parameters.wmode;
	            }
	            
	            // Embed the player SWF:	            
	            swfobject.embedSWF(
					"http://www.dramapassion.com/swf/StrobeMediaPlayback.swf"
					, "StrobeMediaPlayback"
					, 852
					, 480
					, "10.1.0"
					, "swf/expressInstall.swf"
					, parameters
					, {
		                allowFullScreen: "true",
		                wmode: wmodeValue
		            }
					, {
		                name: "StrobeMediaPlayback"
		            }
				);
				
				
			}
			window.onload = loadStrobeMediaPlayback;
			
			var player = null;
			function onJavaScriptBridgeCreated(playerId)
			{
				if (player == null) {
					player = document.getElementById(playerId);
					
					// Add event listeners that will update the 
					player.addEventListener("isDynamicStreamChange", "updateDynamicStreamItems");
					player.addEventListener("switchingChange", "updateDynamicStreamItems");
					player.addEventListener("autoSwitchChange", "updateDynamicStreamItems");
					player.addEventListener("mediaSizeChange", "updateDynamicStreamItems");
				}
			}
			
			var next = 10;
			function updateDynamicStreamItems()
			{
				document.getElementById("dssc").style.display = "block";
				var dynamicStreams = player.getStreamItems();
				var ds = document.getElementById("dssc-items");
				var switchMode = player.getAutoDynamicStreamSwitch() ? "ON" : "OFF"; 
				var currentStreamIndex = player.getCurrentDynamicStreamIndex();
				var bitrates = ["224p","288p","360p","432p","540p","720p"];
				var playing = "";				
				if (currentStreamIndex == next){
					next = 10;
				}

				if (switchMode == "ON"){
					var dsItems = '<a href="#" class="bt_on" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;"><div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</div></a>';
					for (var idx = 0; idx < dynamicStreams.length; idx ++)
					{
						if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "hold"; }					
						dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
					}
										if(dynamicStreams.length < 5 ){
					dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
					dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
					}
									}
				else {
					var dsItems = '<a href="#" class="bt_off" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;"><div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</div></a>';
					if (next == 10)
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "normal"; }
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="' + playing + '" onclick="switchDynamicStreamIndex(' + idx + '); return false;">' + bitrates[idx] + '</a>';				
						}
												if(dynamicStreams.length < 5 ){
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
						}
											}
					else
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else if (next == idx) { playing = "waiting"; } else { playing = "hold"; }					
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
						}
												if(dynamicStreams.length < 5 ){
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
						}
											}
				}
				ds.innerHTML = dsItems;
			}
			
			function switchDynamicStreamIndex(index)
			{
				if (player.getAutoDynamicStreamSwitch())
				{
					player.setAutoDynamicStreamSwitch(false);	
				}
				player.switchDynamicStreamIndex(index);
				next = index;
			}
</script>
<style type="text/css">
        <!--
			.white {
				color: white;
			}
			
			.white:link{text-decoration:none;color: white;}
			.white:visited{text-decoration:none;color: white;}
			.white:hover{text-decoration:none;color: white;}
			.white:active{text-decoration:none;color: white;}
        	.normal {
				color: #969696;
			}
			.normal:link {text-decoration:none;color: #adadad;}
			.normal:visited {text-decoration:none;color: #adadad;}
			.normal:hover {text-decoration:none;color:white;}
			.normal:active {text-decoration:none;color:white;}			
			.waiting {
				color: white;
				text-decoration: blink;
			}					
			.waiting:hover {text-decoration:none;}
			.hold {
				color: #464646;
			}
			
			
			
			.hold:hover {text-decoration:none;}			
			.playing {
				color: #E1D275;
			}
			.playing:link {text-decoration:none;color: #E1D275;}
			.playing:visited {text-decoration:none;color: #E1D275;}
			.playing:hover {text-decoration:none;color: #E1D275;}
			.playing:active {text-decoration:none;color: #E1D275;}		
			
			.bt_on:link {text-decoration:none;color: #E1D275;}
			.bt_on:visited {text-decoration:none;color: #E1D275;}
			.bt_on:hover {text-decoration:none;color:white;}
			.bt_on:active {text-decoration:none;color: #E1D275;}	
			
			.bt_off:link {text-decoration:none;color: #464646;}
			.bt_off:visited {text-decoration:none;color: #464646;}
			.bt_off:hover {text-decoration:none;color: #E1D275;}
			.bt_off:active {text-decoration:none;color: #464646;}	
        -->
        </style>
<div style="width:1000px;margin:auto;border:none;">
<div style="margin-bottom:6px;margin-left:74px;margin-top:15px;border:none;">
<span class="blanc" style="text-transform:uppercase;margin-left:1px;margin-top:15px;font-size:medium;">Answer Me 1997 :</span> <span class="rose" style="text-transform:uppercase;font-size:medium;">Episode 1</span>
</div>
<div style="width:852px;margin-left:74px;">
<div id="StrobeMediaPlayback">
	Alternative content
</div>
<div id="dssc" style="display:none;margin-top:10px;margin-left:20px;width:600px;height:5px;font-size:12px;float:left;">
	<div style="float:left;" class="white"><a target="_blank" href="http://www.dramapassion.com/guide_lecteur" title="Plus d'info" class="white" >Qualité (info) :</a></div>
	<div id="dssc-items" style="float:left;">
		The available qualities will be loaded once the playback starts...
	</div>
</div>

<div style="float:right;margin-top:-10px;font-size:12px;width:300px;" class="white">
<div style="float:right;">
<select class="custom_dropbox" title="Choix du player">
  <option value="1" selected>Flash 1</option>
  <option value="2">Flash 2</option>
</select>
</div>
<div style="float:right;margin-right:5px;margin-top:3px;"><a target="_blank" href="http://www.dramapassion.com/guide_lecteur" title="Plus d'info" class="white" >Choix du lecteur : </a></div>
</div>	
<div style="clear:booth;">
</div>
</div>
</div>

     

	
			</td>
	</tr>
		
		
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0">
                <tr>
                	<td width="680" valign="top">
                    <!-- CADRE DE GAUCHE -->
                    
                    
                          
                          
                        <table width="720" cellpadding="0" cellspacing="0" class="noir" border="0">
<tr><td valign="middle" width="10">&nbsp;</td>
<td valign="middle" width="110"><b>EPISODE</b></td>
<td valign="middle" width="200"><center><b>DATE DE SORTIE</b></td>
<td valign="middle" width="200" class="or"><center><img src="http://www.dramapassion.com/images/pics015.png" style="margin-right:5px;" align="absmiddle"><b>STREAMING</b></center></td>
<td valign="middle" width="200" class="or"><center><img src="http://www.dramapassion.com/images/pics016.png" style="margin-right:5px;" align="absmiddle"><b>TELECHARGEMENT</b></center></td>
</tr>
<tr>
<td colspan="8"><img src="http://www.dramapassion.com/images/ligne720.jpg" ></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 1</td>
<td height="25" valign="middle"><center>07-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/01/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDEtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzAxOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1971&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDEtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzAxOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1971&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 2</td>
<td height="25" valign="middle"><center>07-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/02/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDItc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzAyOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1972&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDItaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzAyOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1972&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 3</td>
<td height="25" valign="middle"><center>07-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/03/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDMtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzAzOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1973&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDMtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzAzOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1973&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 4</td>
<td height="25" valign="middle"><center>07-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/04/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDQtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA0OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1974&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDQtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA0OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1974&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 5</td>
<td height="25" valign="middle"><center>14-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/05/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDUtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA1OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1975&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDUtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA1OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1975&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 6</td>
<td height="25" valign="middle"><center>14-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/06/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDYtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA2OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1976&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDYtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA2OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1976&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 7</td>
<td height="25" valign="middle"><center>14-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/07/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDctc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA3OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1977&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDctaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA3OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1977&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 8</td>
<td height="25" valign="middle"><center>14-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/08/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDgtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA4OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1978&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDgtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA4OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1978&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 9</td>
<td height="25" valign="middle"><center>21-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/09/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDktc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA5OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1979&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMDktaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzA5OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1979&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 10</td>
<td height="25" valign="middle"><center>21-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/10/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTAtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzEwOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1980&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTAtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzEwOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1980&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 11</td>
<td height="25" valign="middle"><center>21-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/11/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTEtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzExOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1981&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTEtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzExOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1981&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 12</td>
<td height="25" valign="middle"><center>21-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/12/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTItc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzEyOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1982&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTItaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzEyOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1982&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 13</td>
<td height="25" valign="middle"><center>28-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/13/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTMtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzEzOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1983&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTMtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzEzOzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1983&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 14</td>
<td height="25" valign="middle"><center>28-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/14/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTQtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzE0OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1984&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTQtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzE0OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1984&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_blanc"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 15</td>
<td height="25" valign="middle"><center>28-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/15/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTUtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzE1OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1985&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTUtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzE1OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1985&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
<tr class="tab_gris"  style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">
<td valign="top">&nbsp;</td>
<td height="28" valign="middle">Episode 16</td>
<td height="25" valign="middle"><center>28-01-2013</center></td>
<td height="25" valign="middle"><center><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997/16/" ><img  src="http://www.dramapassion.com/images/play.png" border="0"></a></center></td>
<td height="25" valign="middle"><center><a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTYtc2QtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzE2OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1986&t=sd"><img src="http://www.dramapassion.com/images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="http://www.dramapassion.com/dl.php?d=TVRNMk16TTFNakEzTUE9PQ==&v=VFZSTk1rMTZUVEZOYWtFelRVRTlQUT09Oy9ybnMvcm5zMTYtaGQtZGwuZjR2I0Fuc3dlciBNZSAxOTk3IzE2OzMzNWYyZjQxMThiMGYwMDAwY2VhODQzOTBmOWYwZWE5&e=1986&t=hd"><img src="http://www.dramapassion.com/images/picto_hd.png" border="0"></a></center></td>
</tr>
</table>
                                                                                                                                        
  

<br><br>

					
                   
 
                    <iframe src="http://www.dramapassion.com/drama_coms.php?dramaID=106&type=0" width="720"  frameborder="0" style="border:none;margin-bottom:40px;" scrolling="no" id="id_iframe" class="noir" border="0" bgcolor="#ecebeb"></iframe>                   
                    
                    <br><br>    
                                        
                    
                    
                   
                     <!-- FIN CADRE DE GAUCHE -->
                    </td>
                    
                    <td width="30">&nbsp;</td>
                    
                    <td width="250" valign="top">
                    <!-- CADRE DE DROITE -->
					
                    <div class="fb-like" data-href="http://www.dramapassion.com/drama/106/Answer-Me-1997" data-send="false" data-width="250" data-show-faces="true"></div>					
                    <br /><br />
                    <div style="border: solid 1px #d8bb43;width: 248px;height: 80px;margin-bottom: 20px;"><p style="text-align:center;margin-top:15px;"><span class="noir" style="font-size:14px;"><b>Comment modifier la qualité ?</b></span><br /><br /><a target="_blank" href="http://www.dramapassion.com/guide_lecteur" style="font-size:14px;" class="lien_bleu"><b>CLIQUEZ ICI</b></a></p></div>					
                    <table cellpadding="0" cellspacing="0" width="250" style="border: solid 1px #f6045b;padding : 5px;padding-bottom: 10px;"><tr><td colspan="2" class="menu_noir" style="font-size:14px;text-align:center;"><div style="height:20px;margin-top:5px;"><div style="display:inline-block;">Séries similaires</div></div></td></tr><tr colspan="2" height="10" ><td> </td></tr><tr height="38"><td width="70" align="left"><a href="http://www.dramapassion.com/drama/3/The-Accidental-Couple"><img title="drama coréen &quot;The Accidental Couple&quot; en vostfr" alt="série coréenne &quot;The Accidental Couple&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/The%20Accidental%20Couple_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/3/The-Accidental-Couple" class="lien_noir">The Accidental Couple</a></td></tr><tr height="38"><td width="70" align="left"><a href="http://www.dramapassion.com/drama/55/The-Vineyard-Man"><img title="drama coréen &quot;The Vineyard Man&quot; en vostfr" alt="série coréenne &quot;The Vineyard Man&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/The%20Vineyard%20Man_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/55/The-Vineyard-Man" class="lien_noir">The Vineyard Man</a></td></tr><tr height="38"><td width="70" align="left"><a href="http://www.dramapassion.com/drama/9/Coffee-Prince"><img title="drama coréen &quot;Coffee Prince&quot; en vostfr" alt="série coréenne &quot;Coffee Prince&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/Coffee%20Prince_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/9/Coffee-Prince" class="lien_noir">Coffee Prince</a></td></tr><tr height="38"><td width="70" align="left"><a href="http://www.dramapassion.com/drama/68/Hate-to-Lose"><img title="drama coréen &quot;Hate to Lose&quot; en vostfr" alt="série coréenne &quot;Hate to Lose&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/Hate%20to%20Lose_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/68/Hate-to-Lose" class="lien_noir">Hate to Lose</a></td></tr><tr height="38"><td width="70" align="left"><a href="http://www.dramapassion.com/drama/93/I-Do-I-Do"><img title="drama coréen &quot;I Do, I Do&quot; en vostfr" alt="série coréenne &quot;I Do, I Do&quot; en vostfr" src="http://www.dramapassion.com/content/dramas/I%20Do,%20I%20Do_Thumb.jpg" width="53" ></a></td><td class="noir" align="left"><a href="http://www.dramapassion.com/drama/93/I-Do-I-Do" class="lien_noir">I Do, I Do</a></td></tr></table>                    <br /><br />
					
                    						<table cellpadding="0" cellspacing="0" width="250">
						
	<tr>
		<td colspan="3" class="menu_noir">Top 10 du moment</td>
	</tr>
	<tr>
		<td colspan="3"><img src="http://www.dramapassion.com/images/ligne250.jpg" width="250" ></td>
	</tr><tr height="20px"><td valign="bottom" class="rose" align="left">1</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/109/May-Queen" class="lien_noir">May Queen</a></td><td valign="bottom" class="postop10new" align="left"><img src="http://www.dramapassion.com/images/top10_new.png" height=9 width=8/></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">2</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/98/Bridal-Mask" class="lien_noir">Bridal Mask</a></td><td valign="bottom" class="postop10neg" align="left"><img src="http://www.dramapassion.com/images/top10_neg.png" height=9 width=8/><span class="noir">1<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">3</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/103/Faith" class="lien_noir">Faith</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">1<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">4</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/108/Nice-Guy" class="lien_noir">Nice Guy</a></td><td valign="bottom" class="postop10neg" align="left"><img src="http://www.dramapassion.com/images/top10_neg.png" height=9 width=8/><span class="noir">2<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">5</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/2/Boys-Over-Flowers" class="lien_noir">Boys Over Flowers</a></td><td valign="bottom" class="postop10egal" align="left"><img src="http://www.dramapassion.com/images/top10_egal.png" height=9 width=8/></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">6</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/105/To-the-Beautiful-You" class="lien_noir">To the Beautiful You</a></td><td valign="bottom" class="postop10neg" align="left"><img src="http://www.dramapassion.com/images/top10_neg.png" height=9 width=8/><span class="noir">3<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">7</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/106/Answer-Me-1997" class="lien_noir">Answer Me 1997</a></td><td valign="bottom" class="postop10neg" align="left"><img src="http://www.dramapassion.com/images/top10_neg.png" height=9 width=8/><span class="noir">1<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">8</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/49/Secret-Garden" class="lien_noir">Secret Garden</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">1<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">9</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/95/The-Queen-and-I" class="lien_noir">The Queen and I</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">11<span class="noir"></td></tr><tr height="20px"><td valign="bottom" class="rose" align="left">10</td><td valign="bottom" class="noir" align="left"><a href="http://www.dramapassion.com/drama/99/Padam-Padam..." class="lien_noir">Padam Padam...</a></td><td valign="bottom" class="postop10pos" align="left"><img src="http://www.dramapassion.com/images/top10_pos.png" height=9 width=8/><span class="noir">2<span class="noir"></td></tr>						<tr><td colspan="3"><br />
												<!-- small_rectangle_all_pages
						<div id='div-gpt-ad-1346273097508-1' style='width:250px; height:250px;'>
						<script type='text/javascript'>
							googletag.cmd.push(function() { googletag.display('div-gpt-ad-1346273097508-1'); });
						</script>
						</div>
						</td>
					</tr>
					</table>  -->
					                         
                    </td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>
    

<script>

$(document).ready(function() {
	 //setTimeout('addphoto()',4000);
	 //$('iframe').iframeAutoHeight();
		 
}) 
 </script>
  <a href="http://www.dramapassion.com/version_flash.php" class="player_flash_alert iframe" style="display:none;" ></a>
</table>
  
<div class="margepied_all_page"><!-- ne pas enlever cette marge et laisser en dernier  --></div>
</div>  
<div id="cont_cont_tab_bottom">
<div style="width:1000px;margin:auto;margin-top: 25px;">
	<div >
		<div style="width:250px;float:left;margin-right:125px;">
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/a-propos-de-DramaPassion/" class="lien_footer">À propos de Dramapassion</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/conditions-generales/" class="lien_footer">Conditions d'utilisation et de vente</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/protection-des-donnees-personnelles/" class="lien_footer">Protection des données personnelles</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
		</div>
		<div style="width:250px;float:left;margin-right:125px;">
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/aide-faq/1" class="lien_footer">Aide - FAQ</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/contactez-nous/" class="lien_footer">Contactez-nous</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
		</div>
		<div style="width:250px;float:left;">
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/annonceurs" class="lien_footer">Publicités/Annonceurs</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
			<span class="span_lien_footer"><a href="http://www.dramapassion.com/partenaires" class="lien_footer">Partenaires</a><span class="souligner"><img src="http://www.dramapassion.com/images/soulignement.png" / ></span></span>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
</div>
<div id="cont_cont_tab_bottom2">
<div style="width:1000px;margin:auto;margin-top : 25px;">	
	<div >
		<span style="display:block;width:320px;margin:auto;">
			<img src="http://www.dramapassion.com/images/pics008.png">
			<img src="http://www.dramapassion.com/images/pics009.png">
			<img src="http://www.dramapassion.com/images/pics010.png">
			<img src="http://www.dramapassion.com/images/pics011.png">
			<img src="http://www.dramapassion.com/images/pics012.png">
			<img src="http://www.dramapassion.com/images/pics013.png"> 
		</span>
		<div style="margin-top : 25px;">
			<span style="text-align:center;font-style:italic;font-size:11px;color: #888888;display:block;" >Dramapassion.com est le premier site de VOD en ligne spécialisé dans les séries coréennes en version originale avec sous-titres français (VOSTFR).<br />Dramapassion.com propose un service gratuit et premium, en streaming et en téléchargement temporaire.</span>
			<span style="color: #ffffff;text-align:center;font-size:11px;display:block;margin-top:15px;">&copy; Vlexhan Distribution SPRL, 2013, tous droits réservés.</span>
		</div>
	</div>
</div>
</div>   
<script>

$(".tab_blanc").mouseover(function() {
    $(this).css("background-color", "#d6d6d6");
  }).mouseout(function(){
	$(this).css("background-color", "#FFFFFF");
  });
$(".tab_gris").mouseover(function() {
    $(this).css("background-color", "#d6d6d6");
  }).mouseout(function(){
	$(this).css("background-color", "#ecebeb");
  });
  
  $(".identifier").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 400,
	'onClosed': function() {
		parent.location.reload(true);
	}
	
});
 $("#identifier2").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 300,
	'onClosed': function() {
		//parent.location.reload(true);
		parent.location = "http://www.dramapassion.com/";
	}
	
});

$("#register").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
  $(".playlist_a").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	
	
});
$(".playlist_a2").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	
	
});
$("#rec_facebook").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
$("#rec_tweet").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
$(".download").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	'onClosed': function() {
		//parent.location.reload(true);
	}
});
$("#test_HD2").fancybox({
	'href'			: 'http://www.dramapassion.com/test_video2.php?type=HD',
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 852,
	'height'		: 480,
	'padding'		: 0
	
	
});
$("#test_SD2").fancybox({
	'href'			: 'http://www.dramapassion.com/test_video2.php?type=SD',
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 852,
	'height'		: 480,
	'padding'		: 0
	
	
});
$(".player_flash_alert").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 200,
	'onClosed': function() {
		//parent.location.reload(true);
	}
	
});
  $(".click_pub").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 500,
	'height'		: 200,
	'overlayShow'	:	false
	
	
});
$("#pop_up_annonceurs").fancybox({
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 500,
	'height'		: 600
	
	
});
$(".playlist_a").mouseover(function(){
	var src = "http://www.dramapassion.com/images/playlist_over.png";
    $(this).find(".playlist_img").attr("src", src);
}).mouseout(function(){
	var src2 = "http://www.dramapassion.com/images/playlist.png";
    $(this).find(".playlist_img").attr("src", src2);
});
$(".playlist_a2").mouseover(function(){
	var src = "http://www.dramapassion.com/images/playlist_over.png";
    $(this).find(".playlist_img").attr("src", src);
}).mouseout(function(){
	var src2 = "http://www.dramapassion.com/images/playlist.png";
    $(this).find(".playlist_img").attr("src", src2);
}); 
function supprimer_gris_recherche(){
	
	document.getElementById('top_recherche').value = "";
	document.getElementById('top_recherche').style.color = "black";
}
 function valide(type,nb){
	
	if(nb < 10){
		nb = "0"+nb;
	}
	if(type == 'sd' || type == 'auto_hd'  || type == 'auto_sd'){
		type2 = 'hd';
	}else{
		type2 = type;
	}
	
	var hidden = "hidden_"+type2+"_"+nb;
	var form = "form_"+type2+"_"+nb;
	document.getElementById(hidden).value = type;
	
	document.forms[form].submit(); 
	
 }  
 function valide2(type,nb,dramaID){
	
	if(nb < 10){
		nb = "0"+nb;
	}
	type2 = 'hd';
	
	var hidden = "hidden_"+type2+"_"+dramaID+"_"+nb;
	
	var form = "form_"+type2+"_"+dramaID+"_"+nb;
	
	document.getElementById(hidden).value = type;
	
	
	document.forms[form].submit(); 
	
 } 
  
</script>
</body>
</html>