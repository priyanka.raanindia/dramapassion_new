<?php
$_SESSION['ipcountry'] = getIp2Location($_SERVER['REMOTE_ADDR']);
if(isset($_REQUEST['lng'])){
	if($_REQUEST['lng'] == "Fre"){
		$_SESSION['language'] = "Fre";
		$_SESSION['langfile'] = "languages/fr.php";
		$_SESSION['weblang'] = 2;
		require_once("languages/fr.php");
	}else if($_REQUEST['lng'] == "Eng"){
		$_SESSION['language'] = "Eng";
		$_SESSION['langfile'] = "languages/en.php";
		$_SESSION['weblang'] = 1;
		require_once("languages/en.php");
	}else{
		$_SESSION['language'] = "Fre";
		$_SESSION['langfile'] = "languages/fr.php";
		$_SESSION['weblang'] = 2;
		require_once("languages/fr.php");
	}
	setcookie("lang",$_REQUEST['lng'],time()+60*60*24*30,"/");
}else{	
	if(isset($_COOKIE['lang'])) {
		setcookie("lang",$_COOKIE['lang'],time()+60*60*24*30,"/");
		$_SESSION['language'] = $_COOKIE['lang'];
		if($_COOKIE['lang'] == "Eng"){
			$_SESSION['langfile'] = "languages/en.php";
			require_once("languages/en.php");
		}else if($_COOKIE['lang'] == "Fre"){
			$_SESSION['langfile'] = "languages/fr.php";
			require_once("languages/fr.php");
		}else{
			$_SESSION['langfile'] = "languages/fr.php";
			require_once("languages/fr.php");
		}
	} else {
		$array_en = array("UK","NL");
		$array_fr = array("LU","FR","BE");
		if(in_array($_SESSION['ipcountry'],$array_en)){
			$_SESSION['language'] = "Eng";
			$_SESSION['langfile'] = "languages/en.php";
			require_once("languages/en.php");
		}else if(in_array($_SESSION['ipcountry'],$array_fr)){
			$_SESSION['language'] = "Fre";
			$_SESSION['langfile'] = "languages/fr.php";
			require_once("languages/fr.php");
		}else{
			//COUNTRY NOT IN ARRAY
			$_SESSION['language'] = "fr";
			$_SESSION['langfile'] = "languages/fr.php";
			require_once("languages/fr.php");
		}
		setcookie("lang",$_SESSION['language'],time()+60*60*24*30,"/");
	}
}
if(isset($_SESSION['weblang'])){ $weblang = $_SESSION['weblang']; }else{ $weblang = 1; $_SESSION['weblang'] = 1; }
?>