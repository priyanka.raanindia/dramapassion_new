1
00:00:10,588 --> 00:00:12,746
[Une unité sous couverture]

2
00:00:13,160 --> 00:00:16,459
La mission d'aujourd'hui
est de trouver la cible et de l'escorter.

3
00:00:19,561 --> 00:00:23,110
Pour attraper les criminels,
nous devons rester inconnus.

4
00:00:24,760 --> 00:00:28,983
Oui, chef Jang !
J'ai identifié notre cible.

5
00:00:28,988 --> 00:00:31,102
Agent Jang, suivez le plan.

6
00:00:32,402 --> 00:00:33,774
Bienvenue.

7
00:00:34,781 --> 00:00:38,345
Pour avoir une couverture parfaite,

8
00:00:38,377 --> 00:00:41,866
il faut une légère imperfection
qui rend unique.

9
00:00:43,090 --> 00:00:45,833
Tu veux travailler pour moi ?

10
00:00:46,085 --> 00:00:47,706
Temps que ça paie bien.

11
00:00:48,388 --> 00:00:49,951
Tu te fiches des conséquences ?

12
00:00:50,005 --> 00:00:52,514
[Des agents sous couverture]

13
00:00:52,873 --> 00:00:57,476
T'es où ?
Ne t'avise pas de faire ça.

14
00:01:12,799 --> 00:01:14,822
Ne pas se laisser atteindre

15
00:01:15,063 --> 00:01:17,174
Gun-Woo, attends !

16
00:01:21,800 --> 00:01:26,997
Un nouveau thriller d'action :
Hidden Identity

