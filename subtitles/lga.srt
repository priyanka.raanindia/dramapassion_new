1
00:00:04,062 --> 00:00:07,233
Est-ce si mal
de faire confiance aux autres ?

2
00:00:07,872 --> 00:00:11,295
Quand de l'argent est en jeu,
on montre sa vraie nature.

3
00:00:11,332 --> 00:00:13,203
Pourquoi tu ne peux pas ?

4
00:00:13,233 --> 00:00:15,554
Pour faire confiance,
il faut d'abord douter.

5
00:00:20,349 --> 00:00:24,340
Entre trahisons et révélations,
il faut en sortir vainqueur.

6
00:00:27,302 --> 00:00:29,999
Un thriller psychologique :
LIAR GAME.

