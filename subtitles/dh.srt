﻿1
00:00:00,801 --> 00:00:03,793
Ceux qui ne peuvent pas rêver
ne peuvent pas vivre.

2
00:00:06,913 --> 00:00:10,428
L'espoir est un rêve éveillé.

3
00:00:16,182 --> 00:00:17,432
Un rêve déchu

4
00:00:19,462 --> 00:00:20,793
Un rêve qui continue

5
00:00:22,762 --> 00:00:23,956
Un rêve volé

6
00:00:25,622 --> 00:00:26,678
Un rêve dérobé

7
00:00:29,227 --> 00:00:30,796
Une sonate pour deux

