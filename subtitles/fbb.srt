﻿1
00:00:00,410 --> 00:00:02,008
John Lennon a dit un jour :

2
00:00:02,034 --> 00:00:04,451
« Les Beatles sont
plus populaires que Jésus ».

3
00:00:04,689 --> 00:00:08,563
On n'est ni populaires ni spéciaux.
On fait juste ce qu'on a envie de faire.

4
00:00:12,993 --> 00:00:15,549
On se fiche de savoir
de quoi demain sera fait.

5
00:00:16,192 --> 00:00:19,144
On s'éclate juste
en faisant de la musique entre amis.

6
00:00:20,041 --> 00:00:23,363
Je crois que je n'ai jamais autant donné.

7
00:00:25,632 --> 00:00:27,824
La ferme, c'est parti !

8
00:00:27,847 --> 00:00:30,118
Shut Up, Flower Boy Band!

