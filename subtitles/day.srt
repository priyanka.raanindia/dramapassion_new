﻿1
00:00:04,138 --> 00:00:09,115
La cote de popularité du président
n'a jamais été aussi faible...

2
00:00:09,201 --> 00:00:10,806
Je dois parler au Président.

3
00:00:10,824 --> 00:00:13,759
Le Président doit mourir,
pour que nous vivions.

4
00:00:13,782 --> 00:00:16,092
[Risque d'assassinat]
- Des innocents sont morts.

5
00:00:16,123 --> 00:00:19,901
J'ai beaucoup de sang sur les mains.
- La politique, c'est des compromis.

6
00:00:19,934 --> 00:00:22,744
[Révéler la conspiration]
- L'un de nous doit mourir.

7
00:00:22,782 --> 00:00:26,272
Il faut prévenir les autorités.
- Il ne reste plus beaucoup de temps.

8
00:00:26,572 --> 00:00:30,521
3 jours, 72 heures, 4320 minutes
- Agent Han Tae-Kyung...

9
00:00:31,698 --> 00:00:34,045
Vous pouvez me protéger ?

10
00:00:34,142 --> 00:00:35,367
Le protéger à tout prix

11
00:00:35,421 --> 00:00:38,998
Pour tuer le Président,
il faudra d'abord me passer sur le corps.

12
00:00:39,361 --> 00:00:43,561
3 Days

