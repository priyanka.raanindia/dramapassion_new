﻿1
00:00:02,331 --> 00:00:04,103
À la fin de l'ère Joseon,

2
00:00:06,067 --> 00:00:07,777
un nouveau fusil apparaît.

3
00:00:15,268 --> 00:00:17,926
Aucune arme à Joseon
ne peut vaincre ce fusil.

4
00:00:20,301 --> 00:00:24,778
Abandonner l'épée,
ou mourir avec l'épée...

5
00:00:25,331 --> 00:00:26,220
L'ère...

6
00:00:26,980 --> 00:00:27,790
des épées...

7
00:00:28,575 --> 00:00:29,416
est révolue.

8
00:00:31,052 --> 00:00:34,968
The Joseon Shooter

