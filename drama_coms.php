<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$dramaID = (int)cleanup($_GET['dramaID']);
	$drama_tab = DramaInfo($dramaID);
	$type = cleanup($_GET['type']);
	
	$limit_min = (cleanup($_GET['limit_min']));
	
	if($limit_min == 0){
		$limit_min = 0;
		$page = 1;
	}else{
		$page = $limit_min ;
		$limit_min = $limit_min -1;
		$limit_min = $limit_min *10 ;
		
	}
	
	$limit_max = $limit_min+10;
	if($limit_min != 0){
	$limit_min = $limit_min;
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_coms.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>

</head>
<body onLoad="resize();">
<div id="taille">
<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0">
                    <tr>
                    	<td width="100" height="30" bgcolor="#ecebeb" id="titre_info"><a href="<?php echo $http ; ?>drama_coms.php?dramaID=<?php echo $dramaID ; ?>&type=0" id="a_info"><center><b>INFOS</b></center></a></td>
                        <td height="30" width="7"></td>
                        <td width="150" height="30"  id="titre_coms"><a href="<?php echo $http ; ?>drama_coms.php?dramaID=<?php echo $dramaID ; ?>&type=1" id="a_coms" ><center><b>COMMENTAIRES</b></center></a></td>
                        <td height="30"></td>
                    </tr>
                    </table>
                   
<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0"  bgcolor="#ecebeb" id="info">
                    <tr>
                    	<td width="30">&nbsp;</td>
                    	<td width="620">
							<br>
							<h2>Synopsis</h2>
							<p style="text-align:justify;"><?php echo $drama_tab['synopsis'] ; ?></p>
							<br />
							<?php if($dramaID == 210){}else{ ?>
							<img src="<?php echo $http ; ?>images/ligne620.png" style="margin-top:2px;margin-bottom:2px;">
							<?php } ?>
						</td>
                    	<td width="30">&nbsp;</td>                                                      
                    </tr>
                    <?php if($dramaID == 210){}else{ ?>
					<tr>
                    	<td></td>
                        <td>
							<h2>Acteurs</h2>
							<?php ActreurDrama($dramaID) ; ?>
						
                        </td>
                        <td></td>
                    </tr>  
                    <?php } ?>
                    <tr>
                    	<td colspan="4"><br><br></td>
                    </tr>  
</table>  
<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0" bgcolor="#ecebeb" id="coms">
<tr>
                    	<td width="30">&nbsp;</td>
                    	<td width="620">
<?php

	
							
	$req_coms = "SELECT * FROM t_dp_review WHERE (Visible=1 AND WebLangID=2 AND ProductID='".$dramaID."') ORDER BY ReviewAdded DESC LIMIT 0,$limit_max ";
	
	$sql_coms = mysql_query($req_coms);
	$req_coms_tot = "SELECT * FROM t_dp_review WHERE (Visible=1 AND WebLangID=2 AND ProductID='".$dramaID."') ORDER BY ReviewAdded ";
	$sql_coms_tot = mysql_query($req_coms_tot);
	
	$nb_coms_tot = mysql_num_rows($sql_coms_tot);
	$tot2 = mysql_num_rows($sql_coms);
	
	
	if(($nb_coms_tot % 10) == 0){
		$nb_tot_page = intval($nb_coms_tot/10);
	}else{
		$nb_tot_page = intval($nb_coms_tot/10)+1;
	}
	if($page ==1){
		$limit_min_m = 0;
	}else{
		$limit_min_m = $page -1;
	}
	if($page == $nb_tot_page){
		$limit_max_p = $nb_tot_page;
	}else{
		$limit_max_p = $page+1;
	}
	echo "<div class='cont_all'>";
	echo "<div class='cont_titre'>";
		echo '<a href="javascript:void();" class="iframe a_ajouter_coms"><img src="'.$http.'images/pics018.png" width="21" valign="middle"><span class="coms_rose">Ajouter un commentaire</span></a><span class="titre_droite">'.$nb_coms_tot.' commentaire';
		if($nb_coms_tot > 1 ){echo 's';}
		echo '</span>';
		echo '<br />';
	if($nb_tot_page == 1){
	
	}else{
		echo "<div class='cont_page_nav2'>";
		echo'<div class="nav_page_img"><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min=0&type=1" ><img border="0" src="'.$http.'images/nav_gauche2.png" width="15"></a><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min='.$limit_min_m.'&type=1" ><img border="0" src="'.$http.'images/nav_gauche.png" width="15"></a></div>';
		echo'<div class="nav_page">&nbsp;<span class="navPage"><a href="javascript:void();" class="selected">'.$page.' </a></span> sur '.$nb_tot_page.'&nbsp;</div>';
		echo'<div class="nav_page_img"><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min='.$limit_max_p.'&type=1" ><img border="0" src="images/nav_droit.png" width="15"></a><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min='.$nb_tot_page.'&type=1" ><img border="0" src="images/nav_droit2.png" width="15"></a></div>';
	echo '</div>';
	}
	echo '</div>';
		echo '<img src="'.$http.'images/ligne660.png" >';
	echo "</div>";
	$nb_temp = 1;
	while($sql_row_coms=mysql_fetch_array($sql_coms)){
			$req_nom = "SELECT * FROM t_dp_user WHERE UserID='".$sql_row_coms['UserID']."'";
			$sql_nom = mysql_query($req_nom);
			$pseudo = mysql_result($sql_nom,0,"UserName");				
		if($limit_min == 0){						
			
								
		echo "<div class='cont_coms'>";
				echo "<div class='cont_info_perso' style='float:left'>";
					echo "<span class='pseudo'>".$pseudo."</span>";
				echo "</div><div class='cont_text_coms' style='float:left'>";
				echo "<h3>".$sql_row_coms['ReviewTitle']."</h3>";
				echo "<span class='texte'><p>".$sql_row_coms['ReviewContent']."</p></span>";
				
			echo "</div>";
			//echo '<div style="clear:both;"></div>';
			echo '<div class="signaler" style="float:left" ><a href="javascript:void();" onclick="signaler_click('.$sql_row_coms['ReviewID'].')" class="iframe a_signaler">Signaler</a></div>';
		echo "</div>";
		echo '<div style="clear:both;"></div>';
		}else{
			if(!($nb_temp < ($limit_min + 1))){
				echo "<div class='cont_coms'>";
				echo "<div class='cont_info_perso' style='float:left'>";
					echo "<span class='pseudo'>".$pseudo."</span>";
				echo "</div><div class='cont_text_coms' style='float:left'>";
				echo "<h3>".$sql_row_coms['ReviewTitle']."</h3>";
				echo "<span class='texte'><p>".$sql_row_coms['ReviewContent']."</p></span>";
				
			echo "</div>";
			//echo '<div style="clear:both;"></div>';
			echo '<div class="signaler" style="float:left" ><a href="javascript:void();" onclick="signaler_click('.$sql_row_coms['ReviewID'].')" class="iframe a_signaler">Signaler</a></div>';
		echo "</div>";
		echo '<div style="clear:both;"></div>';
			}
			
			
			}
		$nb_temp++;
								
	}
	
	if($nb_tot_page == 1){
	
	}else{
	echo "<br />";	
	echo "<div class='cont_page_nav2'>";
		echo'<div class="nav_page_img"><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min=0&type=1" ><img src="'.$http.'images/nav_gauche2.png" width="15"></a><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min='.$limit_min_m.'&type=1" ><img src="'.$http.'images/nav_gauche.png" width="15"></a></div>';
		echo'<div class="nav_page">&nbsp;<span class="navPage"><a href="javascript:void();" class="selected">'.$page.' </a></span> sur '.$nb_tot_page.'&nbsp;</div>';
		echo'<div class="nav_page_img"><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min='.$limit_max_p.'&type=1" ><img src="images/nav_droit.png" width="15"></a><a href="'.$http.'drama_coms.php?dramaID='.$dramaID.'&limit_min='.$nb_tot_page.'&type=1" ><img src="images/nav_droit2.png" width="15"></a></div>';
	echo '</div>';
	}
	echo '</div>';
?>
</td>
<td width="30">&nbsp;</td> 
<tr>
    <td colspan="4"><br><br></td>
</tr>  
</table>
</div>
<script type="text/javascript" >

$(document).ready(function() { 
	<?php if($type == 0){ ?>
	hide('coms');
	show_hide('info');
	<?php }elseif($type == 1){ ?>
	//hide('info');
	show_hide('coms');
	<?php } ?>
	 //document.getElementById("titre_coms").style.backgroundImage =  'url("<?php echo $http ; ?>images/bg_bouton.jpg")';
	 //document.getElementById("titre_coms").style.color = '#FFFFFF';
});
function hide(name){
document.getElementById(name).style.display = 'none';
}
function show(name){
document.getElementById(name).style.display = 'block';
}
function resize(){
	// var taille = pageDim().h ;
	 var taille = jQuery("#taille").height();
	
	 
	 IframeStruct("id_iframe",taille);
}
function show_hide(name){
	if(name == "coms"){
		//if(document.getElementById(name).style.display == "none"){
			hide("info");
			show("coms");
			
			document.getElementById("titre_info").style.backgroundImage =  'url("<?php echo $http ; ?>images/bg_bouton.jpg")';
			document.getElementById("titre_info").style.color = '#FFFFFF';
			document.getElementById("a_info").style.color = '#FFFFFF';
			document.getElementById("titre_coms").style.backgroundImage =  'none';
			document.getElementById("titre_coms").style.backgroundColor ='#ecebeb';
			document.getElementById("titre_coms").style.color = '#000000';
			document.getElementById("a_coms").style.color = '#000000';
			//}
	}
	if(name == "info"){
		//if(document.getElementById(name).style.display == "none"){
			hide("coms");
			show("info");
			document.getElementById("titre_coms").style.backgroundImage =  'url("<?php echo $http ; ?>images/bg_bouton.jpg")';
			document.getElementById("titre_coms").style.color = '#FFFFFF';
			document.getElementById("a_coms").style.color = '#FFFFFF';
			document.getElementById("titre_info").style.backgroundImage =  'none';
			document.getElementById("titre_info").style.backgroundColor ='#ecebeb';
			document.getElementById("titre_info").style.color = '#000000';
			document.getElementById("a_info").style.color = '#000000';
			//}
	}
}
function signaler_click(com_id){
	
	window.parent.$.fancybox({
	'href' : '<?php echo $http ; ?>signaler_coms.php?dramaID=<?php echo $dramaID ;?>&com_id='+com_id,
	'type' : 'iframe',
	//'<iframe class="iframe_fancy" src="<?php echo $http ; ?>signaler_coms.php" frameborder="0" scrolling="no" style="border:0" width="600" height="300">',
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 220,
	'onClosed': function() {
		location.reload(true);
	}
	
	});

};
$('.a_ajouter_coms').click(function(){
	
	window.parent.$.fancybox({
	'href' : '<?php echo $http ; ?>ajout_coms.php?dramaID=<?php echo $dramaID ;?>',
	'type' : 'iframe',
	//'<iframe class="iframe_fancy" src="<?php echo $http ; ?>signaler_coms.php" frameborder="0" scrolling="no" style="border:0" width="600" height="300">',
	'scrolling'		: 'no',
	'titleShow'		: false,
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'width'			: 600,
	'height'		: 450,
	'onClosed': function() {
		location.reload(true);
	}
	
	});
});
</script>
</body>
</html>