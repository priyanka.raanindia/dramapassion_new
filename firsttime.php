<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$select_homme = "";
	$select_femme = "";
	
	$tab_user = InfoUser($_SESSION['userid']);
	if($tab_user['sexe'] == "Homme"){
		$select_homme = 'checked';
	}
	if($tab_user['sexe'] == "Femme"){
		$select_femme = 'checked';
	}
	
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<link type="text/css" href="<?php echo $http ;?>css/custom-theme/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<style type="text/css">
.ui-datepicker {
font-size:12px; 
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (Sans titre-1) -->
<?php 
?>
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Veuillez compléter le formulaire</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                            
                    </td>
                </tr>
			
			<tr>
			<form method="post" action="<?php echo $http ; ?>firsttime_action.php">
				<table width="600" border="0" cellpadding="0" cellspacing="0">
			
				
			

			
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"></td>
				</tr>
			
			
				<tr>
				<td width="123" height="35" >&nbsp;</td>
				
				<td width="150"  align="right">Pays :</td>
				<td width="17">&nbsp;</td>
				<td width="319"><SELECT style="margin-left:-0.5px;" id="pays" name="pays">
														<?php
														
															$req_pays = "SELECT * FROM t_dp_countryship";
															$sql_pays = mysql_query($req_pays);
															$pays_actuel = $tab_user['pays_fr'];
															while($sql_row_pays=mysql_fetch_array($sql_pays)){
															
																if($pays_actuel == $sql_row_pays['CountryNameFre']){
																	$pays_selected = 'selected="selected"';
																	echo'<OPTION VALUE="'.$sql_row_pays['CountryID'].'" '.$pays_selected.'>'.$sql_row_pays['CountryNameFre'].'</OPTION>';
																}else{
																	echo'<OPTION VALUE="'.$sql_row_pays['CountryID'].'">'.$sql_row_pays['CountryNameFre'].'</OPTION>';
																}
															
																
															}?>
														</SELECT></td>
				</tr>
			
				<tr>
				<td height="35" >&nbsp;</td>
				<td align="right">Date de naissance :</td>
				<td>&nbsp;</td>
				<td><input type="text" id="datepicker" name="annif" style="width:190px;" value="<?php echo $tab_user['annif'] ; ?>" /></td>
				</tr>
				
				
				<tr>
				<td height="35"></td>
				<td align="right">Sexe :</td>
				<td >&nbsp;</td>
				<td><input type="radio" name="sexe" <?php echo $select_femme ; ?> value="2"/> Femme &nbsp; <input type="radio" <?php echo $select_homme ; ?> name="sexe" value="1"/> Homme</td>
				</tr>
			
				<tr>
				<td></td>
				<td></td>
				<td height="50" >&nbsp;</td>
				<td><input src="<?php echo $http ; ?>images/submit_send.png" type="image"></td>
				</tr>
				</table>
				
				<input type="hidden" name="register" />
			</form>
			
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>

 <script type="text/javascript">
 <?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
	
	if (w > 0) window.parent.$('.iframe_fancy').css({ width: w+"px"});
    if (h > 0) window.parent.$('.iframe_fancy').css({ height: h+"px"});
	
    if (w > 0) window.parent.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php }else{ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php } ?>
fb_resize(600,240);

function fermer(){
	window.parent.$.fancybox.close();
}
</script>       
<script>
$(function() {
	var d = new Date();
	var n = d.getFullYear();
	var dataBack = n-80;
		$( "#datepicker" ).datepicker({
            changeYear: true,
			changeMonth: true,
			yearRange: dataBack+':'+n,
			
		});
		
	});
	
	jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
});

function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}

fb_resize(600,350);

function fermer(){
	window.parent.$.fancybox.close();
}

</script>             
</body>
</html>