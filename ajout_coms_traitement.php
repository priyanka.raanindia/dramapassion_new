<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$dramaID = (int)cleanup($_POST['dramaID']);
	$userConnect = UserIsConnect();
	$titre_com = cleanup($_POST['titre_com']);
	$texte_com = cleanup($_POST['texte_com']);
	
	$verif_1 = preg_match("#http://#i", $titre_com);
	$verif_2 = preg_match("#télécharger#i", $titre_com);
	$verif_3 = preg_match("#streaming#i", $texte_com);
	$verif_4 = preg_match("#www.#i", $texte_com);
	$verif_5 = preg_match("#viki#i", $texte_com);
	$verif_6 = preg_match("#dramafever#i", $texte_com);
	$verif_7 = preg_match("#viki#i", $titre_com);
	$verif_8 = preg_match("#dramafever#i", $titre_com);
	
	$verif_user = 0;
	$array_user = array(108660);
	
	if(in_array($_SESSION['userid'], $array_user)){
		$verif_user = 1;
	}
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_coms.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />


</head>
<body>
	<div id="cont_iframe_signaler">
		<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
			<div id="cont_iframe_tot">
			<div id="titre_iframe_signaler">
			<span>Ajouter un commentaire</span>
			</div>
			<div id="logo_iframe_signaler">
			<img src="<?php echo $http ;?>images/pop_logo.png">
			</div>
			</div>
		</div>
<?php if($userConnect == 0){  
$header = 'Location: '.$http.'connexion.php?variable=2' ;
header($header);

}elseif($texte_com == ""){
$header = 'Location: '.$http.'ajout_coms.php?dramaID='.$dramaID.'&titre_com='.$titre_com.'&erreur=1' ;
header($header);
}elseif($titre_com == ""){
$header = 'Location: '.$http.'ajout_coms.php?dramaID='.$dramaID.'&texte_com='.$texte_com.'&erreur=2' ;
header($header);

}else{

$count_texte = strlen($texte_com);
if($count_texte > 600){
	$texte_com = substr($texte_com,0,600);
}
if($verif_1 == 1 || $verif_2 == 1 || $verif_3 == 1 || $verif_4 == 1){
	$req_coms = "INSERT INTO t_dp_review (ReviewContent,ReviewTitle,UserID,ProductID,ContentTypeID,ReviewAdded,WebLangID,Visible,Signaler,Sign_type_lien) VALUES ('".$texte_com."','".$titre_com."','".$_SESSION['userid']."','".$dramaID."','2',now(),'2','1','1','1')";	
	
}elseif($verif_5 == 1 || $verif_6 == 1 || $verif_7 == 1 || $verif_8 == 1){
	$req_coms = "INSERT INTO t_dp_review (ReviewContent,ReviewTitle,UserID,ProductID,ContentTypeID,ReviewAdded,WebLangID,Visible,Signaler,Sign_type_lien) VALUES ('".$texte_com."','".$titre_com."','".$_SESSION['userid']."','".$dramaID."','2',now(),'2','0','20','1')";	
}else if($verif_user == 1){
	$req_coms = "INSERT INTO t_dp_review (ReviewContent,ReviewTitle,UserID,ProductID,ContentTypeID,ReviewAdded,WebLangID,Visible,Signaler,Sign_type_lien) VALUES ('".$texte_com."','".$titre_com."','".$_SESSION['userid']."','".$dramaID."','2',now(),'2','0','20','1')";
}else{
$req_coms = "INSERT INTO t_dp_review (ReviewContent,ReviewTitle,UserID,ProductID,ContentTypeID,ReviewAdded,WebLangID,Visible) VALUES ('".$texte_com."','".$titre_com."','".$_SESSION['userid']."','".$dramaID."','2',now(),'2','1')";
}
$sql_coms = mysql_query($req_coms);




if($verif_5 == 1 || $verif_6 == 1 || $verif_7 == 1 || $verif_8 == 1){
	?>
		<div id="cont_coms" style="width:600px;">
		<div class="div_center" style="width:400px;">
		<br />
		<p style="text-align:center;">Nous vous informons que votre commentaire a été supprimé car vous y faisiez référence à un autre site et encouragiez les internautes à ne pas rester sur Dramapassion. Afin d’éviter une suspension de votre compte, merci d'éviter ce type de renvois. Nous vous remercions de votre compréhension.<br /><br />
		<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a></p>
		</div>
		</div>
	<?php
}elseif($verif_user == 1){
	?>
		<div id="cont_coms" style="width:600px;">
		<div class="div_center" style="width:400px;">
		<br />
		<p style="text-align:center;"><br /><br />
		<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a></p>
		</div>
		</div>
	<?php
}else{
	?>
		<div id="cont_coms" style="width:600px;">
		<div class="div_center" style="width:400px;">
		<br />
		<p style="text-align:center;">Votre commentaire à bien été enregistré.<br /><br />
		<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a></p>
		</div>
		</div>
	<?php
}

?>		

	
<?php }
 ?>   	
	
	</div>
	
</form>
<script>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
fb_resize(600,300);
function fermer(){
	window.parent.$.fancybox.close();
}
</script>
</body>
</html>