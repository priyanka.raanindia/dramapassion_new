<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	require_once("top.php");
	
	$id_acteur = cleanup($_GET['id_acteur']);
	
	$req_acteur = "SELECT * FROM t_dp_actor AS actor, t_dp_actor_detail AS detail WHERE detail.ActorID = actor.ActorID AND actor.ActorID = ".$id_acteur;
	$sql_acteur = mysql_query($req_acteur);
	
	$name = mysql_result($sql_acteur,0, 'ActorName');
	
?>
<link rel="stylesheet" href="<?php echo $server; ?>css/acteur.css" type="text/css">
<tr>
	<td>
		<div id="acteur_content">
			<div id="acteur_photo_fond1" class="acteur_photo_fond_click" style="background: url('content/actors_background/fond_acteur_01.jpg');">
				<div id="acteur_content_center1">
					<div id="acteur_name">
					<?php echo $name ; ?>
					</div>
					<div id="acteur_photo_thumb">
					<?php
						$file_dos = "content/actors_fotos/".$id_acteur."/*.jpg";
						$files = glob($file_dos); 
						$id_photo_count = 1;
						foreach($files as $key => $value){
							echo '<div id="acteur_photo_thumb_'.$id_photo_count.'" class="acteur_photo_thumb_mini"><img src="'.$value.'" height="100px" /></div>';
							$id_photo_count++ ;
						}
					?>
					</div>
				</div>
			</div>
			<div id="acteur_photo_show">
				<div id="acteur_photo_show_top"></div>
				<div id="acteur_photo_show_cont">
					<div id="acteur_photo_show_cont_photo">
						<img id="acteur_photo_show_cont_url" src="" />
					</div>
				</div>
				<div id="acteur_photo_show_bottom"></div>
			</div>
			<div id="acteur_photo_fond2" class="acteur_photo_fond_click" style="background: url('content/actors_background/fond_acteur_02.jpg');">
				<div id="acteur_content_center2">
					<div id="acteur_phrase_mythique_cont">
						<div id="acteur_phrase_mythique_titre">Phrase Mythiques :</div>
						<div id="acteur_phrase_mythique_phrase">
							<div class="acteur_phrase_mythique_texte">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque magna nisl, ornare at fringilla non, hendrerit vel mauris. Duis pretium tortor in ligula viverra facilisis.</div>
							<div class="acteur_phrase_mythique_texte">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque magna nisl, ornare at fringilla non, hendrerit vel mauris. Duis pretium tortor in ligula viverra facilisis.</div>
						</div>
						<div id="acteur_phrase_mythique_ajout">
							<div id="acteur_phrase_mythique_ajout_titre"><div id="bulle_rose" ><img src="images/bulle_rose.png" /></div><div id="ajout_txt" >Ajouter une phrase mythique</div></div>
						</div>
						<div class="ligne_separation"><img src="images/ligne720_bl.png" width="720"></div>
					</div>
					<div id="acteur_profil">
						<div id="acteur_profil_onglet">
							<div id="acteur_profil_onglet_profil"><img src="content/actors_background/PROFIL.jpg" /></div>
							<?php
							$s1 = mysql_result($sql_acteur,0, 's1_c'); 
							$s2 = mysql_result($sql_acteur,0, 's2_c'); 
							$s3 = mysql_result($sql_acteur,0, 's3_c');
							$s1_q = mysql_result($sql_acteur,0, 's1_t'); 
							$s2_q = mysql_result($sql_acteur,0, 's2_t'); 
							$s3_q = mysql_result($sql_acteur,0, 's3_t');
							
							if(($s1 != '' && $s1 != ' ') || ($s2 != '' && $s2 != ' ') || ($s3 != '' && $s3 != ' ')){
							?>
							<div id="acteur_profil_onglet_savier"><img src="content/actors_background/Le-saviez-vous.jpg" /></div>
							<?php
							}
							?>
						</div>
						<div id="acteur_profil_contenu">
							<div id="acteur_profil_cont_profil">
								<table class="table_acteur">
									<tr><td width="200" height="30"> </td><td></td></tr>
								<?php
									$nom_ori = mysql_result($sql_acteur,0, 'nom_kr');
									$surnom = mysql_result($sql_acteur,0, 'surnom');
									$profession = mysql_result($sql_acteur,0, 'profession');
									$date_naissance = mysql_result($sql_acteur,0, 'naissance');
									$lieu_naissance = mysql_result($sql_acteur,0, 'lieu_naissance');
									$taille = mysql_result($sql_acteur,0, 'taille');
									$poid = mysql_result($sql_acteur,0, 'poid');
									$signe_astro = mysql_result($sql_acteur,0, 'singe_astro');
									$signe_chinois = mysql_result($sql_acteur,0, 'signe_chinois');
									$groupe_s = mysql_result($sql_acteur,0, 'groupe_sanguin');
									$autre = mysql_result($sql_acteur,0, 'p1_c');
									$autre_q = mysql_result($sql_acteur,0, 'p1_t');
									$surnom_kr = mysql_result($sql_acteur,0, 'surnom_kr');
									
									if($nom_ori != '' && $nom_ori != ' '){
										echo '<tr><td>Nom original :</td><td>'.$nom_ori.'</td></tr>';
									}
									if($surnom != '' && $surnom != ' ' ){
										if($surnom_kr != '' && $surnom_kr != ' '){
											echo '<tr><td>Surnom :</td><td>'.$surnom.' - '.$surnom_kr.'</td></tr>';
										}else{
										echo '<tr><td>Surnom :</td><td>'.$surnom.'</td></tr>';
										}
									}
									if(($surnom == '' || $surnom == ' ' ) && ($surnom_kr != '' && $surnom_kr != ' ')){
										echo '<tr><td>Surnom :</td><td>'.$surnom_kr.'</td></tr>';
									}
									if($profession != '' && $profession != ' '){
										echo '<tr><td>Profession : </td><td>'.$profession.'</td></tr>';
									}
									if($date_naissance != '' && $date_naissance != ' '){
										$date_naissance_aff = date('d-m-Y',$date_naissance);
										echo '<tr><td>Date de naissance :</td><td>'.$date_naissance_aff.'</td></tr>';
									}
									if($lieu_naissance != '' && $lieu_naissance != ' '){
										echo '<tr><td>Lieu de naissance :</td><td>'.$lieu_naissance.'</td></tr>';
									}
									if($taille != '' && $taille != ' '){
										$taille_aff = substr($taille, -3, 1).'m'.substr($taille, -2);
										echo '<tr><td>Taille :</td><td>'.$taille_aff.'</td></tr>';
									}
									if($poid != '' && $poid != ' '){
										$poid_aff = $poid.' Kg';
										echo '<tr><td>Poids :</td><td>'.$poid_aff.'</td></tr>';
									}
									if($signe_astro != '' && $signe_astro != ' ' && $signe_astro != 0){
										$tab_astro[1] = 'Bélier';
										$tab_astro[2] = 'Taureau';
										$tab_astro[3] = 'Gémeaux';
										$tab_astro[4] = 'Cancer';
										$tab_astro[5] = 'Lion';
										$tab_astro[6] = 'Vierge';
										$tab_astro[7] = 'Balance';
										$tab_astro[8] = 'Scorpion';
										$tab_astro[9] = 'Sagittaire';
										$tab_astro[10] = 'Capricorne';
										$tab_astro[11] = 'Verseau';
										$tab_astro[12] = 'Poissons';
										
										
										echo '<tr><td>Signe astrologique :</td><td>'.$tab_astro[$signe_astro].'</td></tr>';
									}
									if($signe_chinois != '' && $signe_chinois != ' ' && $signe_chinois != 0){
										$tab_chinois[1] = 'rat';
										$tab_chinois[2] = 'buffle';
										$tab_chinois[3] = 'tigre';
										$tab_chinois[4] = 'lapin';
										$tab_chinois[5] = 'dragon';
										$tab_chinois[6] = 'serpent';
										$tab_chinois[7] = 'cheval';
										$tab_chinois[8] = 'chèvre';
										$tab_chinois[9] = 'singe';
										$tab_chinois[10] = 'coq';
										$tab_chinois[11] = 'chien';
										$tab_chinois[12] = 'cochon';
										
										
										echo '<tr><td>Signe chinois :</td><td>'.$tab_chinois[$signe_chinois].'</td></tr>';
									}
									if($groupe_s != '' && $groupe_s != ' ' && $groupe_s != 0){
										$tab_groupe_s[1] = 'O';
										$tab_groupe_s[3] = 'A';
										$tab_groupe_s[5] = 'B';
										$tab_groupe_s[7] = 'AB';
										
										echo '<tr><td>Lieu de naissance :</td><td>'.$tab_groupe_s[$groupe_s].'</td></tr>';
									}
									if($autre != '' && $autre != ' '){
										echo '<tr><td>'.$autre_q.' :</td><td>'.$autre.'</td></tr>';
									}
								?>
								<tr><td width="200" height="30"> </td><td></td></tr>
								</table>
							</div>
							<div id="acteur_profil_cont_savier">
								<table class="table_acteur">
								<tr><td width="200" height="30"> </td><td></td></tr>
								<?php
									if($s1 != '' && $s1 != ' '){
										echo '<tr><td>'.$s1_q.' :</td><td>'.$s1.'</td></tr>';
									}
									if($s2 != '' && $s2 != ' '){
										echo '<tr><td>'.$s2_q.' :</td><td>'.$s2.'</td></tr>';
									}
									if($s3 != '' && $s3 != ' '){
										echo '<tr><td>'.$s3_q.' :</td><td>'.$s3.'</td></tr>';
									}
								?>
								<tr><td width="200" height="30"> </td><td></td></tr>
								</table>
							</div>
						</div>
						<?php
							$req_autre_serie = "SELECT * FROM t_dp_dramaactorlink AS link, t_dp_drama AS drama WHERE link.DramaID = drama.DramaID AND ActorID =".$id_acteur ;
							$sql_autre_serie = mysql_query($req_autre_serie);
							$verif_autre_serie = mysql_num_rows($sql_autre_serie);
							
							if($verif_autre_serie > 0){
								?>
								<div id="acteur_serie_autre_titre">Série disponible sur Dramapassion</div>
								<div id="acteur_serie_autre">
									<?php
									$count_serie = 1;
									while($row_autre_serie=mysql_fetch_assoc($sql_autre_serie)){
										if(($count_serie % 3) == 0){
											echo '<div class="serie_autre_last"><div class="serie_img"><img src="content/dramas/'.$row_autre_serie['DramaTitle'].'_Thumb.jpg" /></div><div class="serie_name">'.$row_autre_serie['DramaTitle'].'</div><div class="serie_sortie">'.$row_autre_serie['DramaYear'].'</div></div><br />';
										}else{
											echo '<div class="serie_autre"><div class="serie_img"><img src="content/dramas/'.$row_autre_serie['DramaTitle'].'_Thumb.jpg" /></div><div class="serie_name">'.$row_autre_serie['DramaTitle'].'</div><div class="serie_sortie">'.$row_autre_serie['DramaYear'].'</div></div>';
										}
										$count_serie++ ;
										
									}
									
									?>
								</div>
								<?php
							}
						?>
					</div>
					<div id="pub">
						<?php AffPub("9") ; ?><br />
						<?php AffPub("2") ; ?>
					</div>
				</div>
			</div>
		</div>

	</td>
</tr>
<script>
$('.contenant_all_page').css('paddingBottom','330px');
$('.acteur_photo_thumb_mini').click(function() {
	verif = 1;
	url_photo = $(this).find('img').attr('src');
  $('#acteur_photo_show').show();
  $('#acteur_photo_show_cont_url').attr('src',url_photo);
  height_img = $('#acteur_photo_show_cont_url').height();
	if(height_img == 580){
		$('#acteur_photo_show_cont_url').css('margin-top','10px');
	}else{
		height_final = (600 - height_img)  /2;
		$('#acteur_photo_show_cont_url').css('margin-top',height_final);
	}
  $('#acteur_photo_show').animate({
    height: '610px'
	}, 1000, function() {
    // Animation complete.
    verif = 0;
  });
});
$('.acteur_photo_fond_click').click(function() {
if(verif == 1){

}else{
	$('#acteur_photo_show').animate({
    height: '0px'
	}, 1000, function() {
    // Animation complete.
    $('#acteur_photo_show').hide();
  });
}
});
$('#acteur_profil_onglet_profil').click(function(){
	$('#acteur_profil_onglet_profil').find('img').attr('src','content/actors_background/PROFIL.jpg');
	$('#acteur_profil_onglet_savier').find('img').attr('src','content/actors_background/Le-saviez-vous.jpg');
	$('#acteur_profil_cont_savier').hide();
	$('#acteur_profil_cont_profil').show();
});
$('#acteur_profil_onglet_savier').click(function(){
	$('#acteur_profil_onglet_profil').find('img').attr('src','content/actors_background/PROFIL_r.jpg');
	$('#acteur_profil_onglet_savier').find('img').attr('src','content/actors_background/Le-saviez-vous_r.jpg');
	$('#acteur_profil_cont_savier').show();
	$('#acteur_profil_cont_profil').hide();
});

</script>
<?php require_once("bottom.php"); ?>