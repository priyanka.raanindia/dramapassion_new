<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("includes/serverselect.php");
?>
<html>
<head>
<script src="http://www.dramapassion.com/js/flowplayer-3.2.11.min.js"></script>
</head>
<body>		
<?php 
	$hashEpi = Hash_free(10,35,1);	
	$url_free_epi = $server_free.$hashEpi;
?>
<div id="free_video">
	<div style="display:block;width:600px;height:340px;cursor:default;" id="free"></div>
</div>
<script>
$f("free", "<?php echo $http; ?>swf/flowplayer.commercial-3.2.15.swf", {
   key: '#$8d427e6f20cb64d82ba',
   wmode:'transparent',
   clip: {
        // the manifest file
        url: 'rns/smil:rns01-hd.smil/manifest.f4m',
 
        // we need 2 urlResolvers
        urlResolvers: ['f4m','bwcheck'],
 
        // use the httpstreaming plugin
        provider: 'httpstreaming',
 
        // directory where the manifest and video fragments are stored
        baseUrl: 'http://ns04.dramapassion.com/vod/_definst_/',
 
        autoPlay: false,
        scaling: 'fit'
    },
    canvas: {
	    backgroundColor: "#000000",
	    backgroundGradient: "none"
	},
    plugins: {
        f4m: {
            url: '<?php echo $http; ?>swf/flowplayer.f4m-3.2.9.swf'
        },
        httpstreaming: {
            url: '<?php echo $http; ?>swf/flowplayer.httpstreaming-3.2.9.swf'
        },
        bwcheck: {
            url: '<?php echo $http; ?>swf/flowplayer.bwcheck-httpstreaming-3.2.11.swf',
            dynamic: true,
            //dynamicbuffer: true,
            serverType: 'wowza',
            switchOnFullscreen: false,
            qos: {frames: true, screen: false},
            rememberBitrate: true,
            // show the selected file in the content box
            // usually omitted in production
            onStreamSwitchBegin: function (newItem, currentItem) {
                var content = $f('free').getPlugin('content');
                var message = 'Will switch to: ' +
                               newItem.streamName +
                                ' from ' +
                                currentItem.streamName;
                content.setHtml(message);
            },
            onStreamSwitch: function (newItem) {
                var content = $f('free').getPlugin('content');
                var message = 'Switched to: ' + newItem.streamName;
                content.setHtml(message);
            }
        },
 
        // a content box to display the selected bitrate
        // usually omitted in production
        content: {
            url: '<?php echo $http; ?>swf/flowplayer.content-3.2.8.swf',
            bottom: 30,
            left: 0,
            width: 400,
            height: 150,
            backgroundColor: 'transparent',
            backgroundGradient: 'none',
            border: 0,
            textDecoration: 'outline',
            style: {
                body: {
                    fontSize: 14,
                    fontFamily: 'Arial',
                    textAlign: 'center',
                    color: '#ffffff'
                }
            }
        }
    }
});
</script>
</body>
</html>