<?php 
include("wowza/settings.php");
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <title>FLV player with chapter navigation using Flowplayer + wowza</title>
  <link rel="stylesheet" type="text/css" href="wowza/fp.min.css" />
  <script type="text/javascript" src="wowza/flowplayer-3.2.6.min.js"></script>
  <script type="text/javascript">
// <![CDATA[ */
  window.onload = function () {


    player = $f("player", "wowza/flowplayer-3.2.7.swf", {
      plugins: {
        rtmp: {
        url: "wowza/flowplayer.rtmp-3.2.3.swf",
		netConnectionUrl: "rtmp://ec.playmedia.fr/gong-base"
		  
		  
        }
      },
      clip: {
		url: "rtmp://ec.playmedia.fr/gong-base", //replace 'videoname' by the FLV filename (without the file extension)
        provider: "rtmp",
        scaling: "fit"
      }
    });

  };
  // ]]>
  </script>
</head>

<body>
 
    <p>&nbsp;</p>
<div id="player"></div>

</body></html>
