<?php
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect.php");
?>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="flowplayer-3.2.11.min.js"></script>
	<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>

	
	<!-- some minimal styling, can be removed -->
	<link rel="stylesheet" type="text/css" href="style.css">
	
	<!-- page title -->
	<title>Minimal Flowplayer setup</title>
<style type="text/css">
        <!--
        	body{
	        	background-color: black;
        	}
			.white {
				color: white;
			}
			
			.white:link{text-decoration:none;color: white;}
			.white:visited{text-decoration:none;color: white;}
			.white:hover{text-decoration:none;color: white;}
			.white:active{text-decoration:none;color: white;}
        	.normal {
				color: #969696;
			}
			.normal:link {text-decoration:none;color: #464646;}
			.normal:visited {text-decoration:none;color: #464646;}
			.normal:hover {text-decoration:none;color:white;}
			.normal:active {text-decoration:none;color:white;}			
			.waiting {
				color: white;
				text-decoration: blink;
			}					
			.waiting:hover {text-decoration:none;}
			.hold {
				color: #464646;
			}
			
			
			
			.hold:hover {text-decoration:none;}			
			.playing {
				color: #E1D275;
			}
			.playing:link {text-decoration:none;color: #E1D275;}
			.playing:visited {text-decoration:none;color: #E1D275;}
			.playing:hover {text-decoration:none;color: #E1D275;}
			.playing:active {text-decoration:none;color: #E1D275;}		
			
			.bt_on:link {text-decoration:none;color: #E1D275;}
			.bt_on:visited {text-decoration:none;color: #E1D275;}
			.bt_on:hover {text-decoration:none;color:white;}
			.bt_on:active {text-decoration:none;color: #E1D275;}	
			
			.bt_off:link {text-decoration:none;color: #464646;}
			.bt_off:visited {text-decoration:none;color: #464646;}
			.bt_off:hover {text-decoration:none;color: #E1D275;}
			.bt_off:active {text-decoration:none;color: #464646;}	
        -->
        </style>
</head><body>

<div style="width:852px;margin-left:74px;">
<div id="httpstreaming-dynamic" style="display:block;width:852px;height:480px;cursor:default;">
</div>
<div id="dssc" style="margin-top:10px;margin-left:20px;width:980px;height:5px;font-size:12px;">
	<div style="float:left;" class="white"><a target="_blank" href="<?php echo $http ; ?>guide_lecteur" title="Plus d'info" class="white" >Qualité (info) :</a></div>
	<div id="dssc-items" style="float:left;">
		<div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="bt_on" id="btn_change" onclick="changeDynamic()">Auto (ON)</a></div>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" id="bitrate_355">224p</span>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" id="bitrate_560">288p</span>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" id="bitrate_875">360p</span>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" id="bitrate_1167">432p</span>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" id="bitrate_1687">540p</span>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" id="bitrate_2664">720p</span>
		
	</div>
</div>	
</div>
</div>

	
<script>
flowplayer('httpstreaming-dynamic', '<?php echo $http; ?>swf/flowplayer.commercial-3.2.15.swf', {
	key: '#$8d427e6f20cb64d82ba',
	clip: {
        url: "rns/smil:rns02-hd.smil/manifest.f4m",
        urlResolvers: ['f4m','bwcheck'],
        provider: 'httpstreaming',
        baseUrl: 'http://ns01.dramapassion.com/vod/_definst_/',
        autoPlay: true,
        scaling: 'fit'
    },
    plugins: {
        f4m: {
            url: "flowplayer.f4m-3.2.9.swf",
        },
        httpstreaming: {
            url: "flowplayer.httpstreaming-3.2.9.swf"
        },
         bwcheck: {
            url: 'flowplayer.bwcheck-httpstreaming-3.2.11.swf',
            dynamic: true,
            dynamicbuffer: true,
            serverType: 'wowza',
            switchOnFullscreen: false,
            // show the selected file in the content box
            // usually omitted in production
            qos : {
            	frames: false, 
            	screen: false
            },
            onStreamSwitch: function (newItem) {
                
                $('.playing').attr('class', 'hold');
                var id = '#bitrate_'+newItem.bitrate;
                $('#bitr3').html(id);
                $(id).attr('class', 'playing');
                
            },
            onStreamSwitchBegin: function (newItem, currentItem) {
                var message = 'Will switch to: ' +
                               newItem.streamName +
                                ' from ' +
                                currentItem.streamName;
                $('#bitr4').html(message);                
            },
            onBwDone: function(info, bitrate){
	           $('#bitr').html(bitrate);
	           
            },
            rememberBitrate: true,
            netConnectionUrl: 'http://ns01.dramapassion.com/bwcheck'
        },
    },
    canvas: {
	    backgroundColor: "#000000",
	    backgroundGradient: "none"
	},
    log: {
        filter: 'debug',
        level: 'org.flowplayer.bwcheck.*'
    }
});

change_streaming = 1;
function changeDynamic(){
	
	if(change_streaming == 1){	
		$f('httpstreaming-dynamic').getPlugin("bwcheck").enableDynamic(0);
		html_bt = '<div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="bt_on" id="btn_change" onclick="changeDynamic()">Auto (ON)</a></div>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="normal" onclick="changeBitrate(355)" id="bitrate_355">224p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="normal" onclick="changeBitrate(560)" id="bitrate_560">288p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="normal" onclick="changeBitrate(875)" id="bitrate_875">360p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="normal" onclick="changeBitrate(1167)" id="bitrate_1167">432p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="normal" onclick="changeBitrate(1687)" id="bitrate_1687">540p</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void();" class="normal" onclick="changeBitrate(2664)" id="bitrate_2664">720p</a>';
		$('#dssc-items').html(html_bt);
		$('#btn_change').html('Auto (OFF)');
		$('#btn_change').attr('class', 'bt_off');
		change_streaming = 0 ;
	}else{
		$('#btn_change').html('Auto (ON)');
		$('#btn_change').attr('class', 'bt_on');
		$f('httpstreaming-dynamic').getPlugin("bwcheck").enableDynamic(1);
		change_streaming = 1
	}
	//$f('httpstreaming-dynamic').enableDynamic(disables);
}

function changeBitrate(bitrate){
	var id = '#bitrate_'+bitrate;
	$('.waiting').attr('class', 'normal');
	$(id).attr('class', 'waiting');
	$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(bitrate);
}
function chg_auto(bitrate){
	$f("httpstreaming-dynamic").getPlugin("bwcheck").checkBandwidth();
}
function chg_bwd(bitrate){
			  var bit1 = 355 ;
	          var bit2 = 560;
	          var bit3 = 875;
	          var bit4 = 1167;
	          var bit5 = 1687;
	          var bit6 = 2664;
	          
	          var change_bit11 = 355 ;
	          var change_bit21 = 560;
	          var change_bit31 = 875;
	          var change_bit41 = 1167;
	          var change_bit51 = 1687;
	          var change_bit61 = 2664;
	          
	          $('#bitr').html(bitrate);
	          
	          if(bitrate <= (bit2 * 1.1)){
		          	$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(change_bit11);
		          	$('#bitr2').html(bit1);
	          }else if(bitrate <= (bit3 * 1.1) &&  bitrate > (bit2 * 1.1)){
	          		$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(change_bit21);
	          		$('#bitr2').html(bit2);
	          }else if(bitrate <= (bit4 * 1.1) &&  bitrate > (bit3 * 1.1)){
	          		$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(change_bit31);
	          		$('#bitr2').html(bit3);
	          }else if(bitrate <= (bit5 * 1.1) &&  bitrate > (bit4 * 1.1)){
	          		$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(change_bit41);
	          		$('#bitr2').html(bit4);
	          }else if(bitrate <= (bit6 * 1.1) &&  bitrate > (bit5 * 1.1)){
	          		$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(change_bit51);
	          		$('#bitr2').html(bit5);
	          }else if(bitrate > (bit6 * 1.1)){
	          		$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(change_bit61);
	          		$('#bitr2').html(bit6);
	          }
}
//var int=self.setInterval(function(){chg_auto()},1000);
</script>	
<br />
<a href="#" onclick="chg_auto()" >ici</a>
<div style="color:white;" id='bitr'></div><div  style="color:white;" id='bitr2'></div><div  style="color:white;" id='bitr3'></div><div  style="color:white;" id='bitr4'></div>
</body></html>