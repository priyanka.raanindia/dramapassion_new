<?php
	require_once("../includes/settings.inc.php");
	require_once("../includes/dbinfo.inc.php");
	require_once("../includes/functions.php");
	require_once("../includes/serverselect.php");
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="flowplayer-3.2.11.min.js"></script>
	<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
	
	<!-- some minimal styling, can be removed -->
	<link rel="stylesheet" type="text/css" href="style.css">
	
	<!-- page title -->
	<title>Minimal Flowplayer setup</title>
</head>
<body>

<div style="width:852px;margin-left:74px;">
<div id="httpstreaming-dynamic" style="display:block;width:852px;height:480px;cursor:default;">
</div>
	
<script>
flowplayer('httpstreaming-dynamic', '<?php echo $http; ?>swf/flowplayer.commercial-3.2.15.swf', {
	key: '#$8d427e6f20cb64d82ba',
	clip: {
        url: "rns/smil:rns02-hd.smil/manifest.f4m",
        urlResolvers: ['f4m','bwcheck'],
        provider: 'httpstreaming',
        baseUrl: 'http://ns01.dramapassion.com/vod/_definst_/',
        autoPlay: true,
        scaling: 'fit'
    },
    plugins: {
        f4m: {
            url: "flowplayer.f4m-3.2.9.swf",
        },
        httpstreaming: {
            url: "flowplayer.httpstreaming-3.2.9.swf"
        },
         bwcheck: {
            url: 'flowplayer.bwcheck-httpstreaming-3.2.11.swf',
            dynamic: true,
            dynamicbuffer: true,
            serverType: 'wowza',
            qos : {
            	frames: false, 
            	screen: false
            },
            onStreamSwitch: function (newItem) {
                
                $('.playing').attr('class', 'hold');
                var id = '#bitrate_'+newItem.bitrate;
                $('#bitr3').html(id);
                $(id).attr('class', 'playing');
                
            },
            onStreamSwitchBegin: function (newItem, currentItem) {
                var message = 'Will switch to: ' +
                               newItem.bitrate +
                                ' from ' +
                                currentItem.bitrate;
                $('#bitr4').html(message);                
            },
            onBwDone: function(info, bitrate){
	           $('#bitr').html(bitrate);
	           
            },
            rememberBitrate: true,
            netConnectionUrl: 'http://ns01.dramapassion.com/bwcheck'
        },
    },
    canvas: {
	    backgroundColor: "#000000",
	    backgroundGradient: "none"
	},
    log: {
        filter: 'debug',
        level: 'org.flowplayer.bwcheck.*'
    }
});

function changeBitrate(bitrate){
	var id = '#bitrate_'+bitrate;
	$('.waiting').attr('class', 'normal');
	$(id).attr('class', 'waiting');
	$f('httpstreaming-dynamic').getPlugin("bwcheck").setBitrate(bitrate);
}
</script>	
<br />
Detected bandwidth : <div id='bitr'></div><br />
Playing bitrate : <div id='bitr3'></div><br />
Switch : <div id='bitr4'></div><br />
Manual switch :<br />
<a href="javascript:void();" class="normal" onclick="changeBitrate(355)" id="bitrate_355">355</a><br />
<a href="javascript:void();" class="normal" onclick="changeBitrate(560)" id="bitrate_560">560</a><br />
<a href="javascript:void();" class="normal" onclick="changeBitrate(875)" id="bitrate_875">875</a><br />
<a href="javascript:void();" class="normal" onclick="changeBitrate(1167)" id="bitrate_1167">1167</a><br />
<a href="javascript:void();" class="normal" onclick="changeBitrate(1687)" id="bitrate_1687">1687</a><br />
<a href="javascript:void();" class="normal" onclick="changeBitrate(2664)" id="bitrate_2664">2664</a><br />
</body>
</html>