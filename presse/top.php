<?php
if(!session_id()) session_start();

if(!isset($_COOKIE['__trk'])){
	$unique = uniqid();
	$cookie = md5($unique.$_SERVER['REMOTE_ADDR']);
	setcookie("__trk", $cookie, time()+15552000, "/");
}else{
	setcookie("__trk", $_COOKIE['__trk'], time()+15552000, "/");
}

require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("../includes/functions.php");
require_once("../logincheck.php");
require_once("../langue/fr.php");
//require_once("locationcheck.php"); 

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- css -->
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/partenaires.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_free.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_bottom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<link type="text/css" href="<?php echo $server ;?>css/custom-theme/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<link href="jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />




<!-- meta -->
<title>Sources graphiques</title>

<!-- script java -->
<script src="<?php echo $server; ?>/js/scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>/js/AC_OETags.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/js/flowplayer-3.2.4.min.js"></script>
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.ibox.js"></script>

	<!-- Get Google CDN's jQuery and jQuery UI with fallback to local -->
	

	<!-- mousewheel plugin -->
	<script src="jquery.mousewheel.min.js"></script>
	<!-- custom scrollbars plugin -->
	<script src="jquery.mCustomScrollbar.js"></script>


<style>
.iframe{
	font-size: 12px;
	color: blue;
}
.iframe:hover{
	font-size: 12px;
	color: red;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="conteneur_all_page">
<div class="contenant_all_page">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top" height="93" style="background-image:url(<?php echo $http ; ?>images/header.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER GRIS-->
        
        <div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" height="93" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="368" height="93"><a href="<?php echo $http ; ?>presse/" ><img src="<?php echo $http ; ?>images/logo.png" width="368" height="93" alt="Logo Dramapassion" title="Logo Dramapassion" border="0"></a></td>
                   <td width="368" height="93"><h1></h1></td>
               
                </tr>
            </table>
		</div>
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
    
	<tr>
        <td valign="top" height="37" style="background-image:url(<?php echo $http ; ?>images/header2.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:1000px;margin:auto;">
             <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				<?php 
				$userConnect = UserIsConnect();
				
				if($userConnect == 0){ 
				
				
				
				
				?>
                    
					<td class="blanc" width="130"></td>
					<td class="blanc" width="685"></td>
               
				
					<td class="blanc" width="20"><a title="Me connecter" href="<?php echo $http ; ?>connexion.php" class="iframe lien_blanc identifier" id="identifier1"><img src="<?php echo $http ; ?>images/account.png" align="absmiddle" alt="S'identifier" title="Me connecter"></a></td>
					<td class="blanc bloc_center" width="70"><a title="Me connecter" href="<?php echo $http ; ?>connexion.php" class="iframe lien_blanc identifier" id="identifier">S'identifier</a><td>
					<td class="blanc" width="5"></td>
					<td class="blanc" width="95" align="right"><td>
				
				
				<?php }else{ ?>
					<td class="blanc" width="130"></td>
					<td class="blanc" width="685"></td>
					<td class="blanc" width="763" align="right"><?php echo $_SESSION['username']." ".$abo_statut ; ?>&nbsp;&nbsp;</a><td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc bloc_center" width="70"><td>
					<td class="blanc" width="5"></td>
					<td class="blanc" width="70" align="right"><a href="<?php echo $http ; ?>logoutAction.php" title="Me déconnecter" class="lien_blanc" style="text-align: right">Déconnexion</a><td>
					
					
				<?php } ?>
                                      
                                       
                </tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>