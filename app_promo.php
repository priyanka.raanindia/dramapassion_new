<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	include 'includes/Mobile_Detect.php';
	$detect = new Mobile_Detect();
	
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<link type="text/css" href="<?php echo $http ;?>css/custom-theme/jquery-ui-1.8.19.custom.css" rel="stylesheet" />

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (Sans titre-1) -->
<?php 
?>
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span style="margin-left:3px;">Téléchargez notre application tablette !</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                            
                    </td>
                </tr>
			
			<tr><td>
				<div style="height:250px;">
				<a href="lien_app.php"><img src="<?php echo $http ;?>images/250_250.jpg" style="float:left;margin-right:20px;" /></a>
				<p style="margin-bottom: 40px;text-align:center;"><br />L'application Dramapassion est disponible
				<?php if($detect->isiOS()){ ?>
					dans l'App Store.<br /><br />Voulez-vous la télécharger ?</p>
				<?php }
					  if($detect->isAndroidOS()){?>
					  sur Google Play.<br /><br />Voulez-vous la télécharger ?</p>
				<?php } ?>
				<?php if($detect->isiOS()){ ?>
				<div style="margin-left:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="https://itunes.apple.com/fr/app/dramapassion/id641121786?mt=8"><img alt="Disponible dans l'App Store" src="<?php echo $http ; ?>images/apple.png" height="40"/></a></div>
				<?php }
					  if($detect->isAndroidOS()){?>
					  <div style="margin-left:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://play.google.com/store/apps/details?id=com.dramapassion.dramapassion"><img alt="Disponible sur le Google Play" height="40" src="<?php echo $http ; ?>images/android.png" /></a></div>
				<?php } ?>
				<div style="margin-left:50px;margin-top:20px;">
					<form action="app_promo_action.php" method="post" id="target">
						
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="$('#target').submit();" style="color:#8bbafb;" >Ne plus afficher ce message</a><br /><br />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="never" id="never" /> Retenir mon choix
					</form>
				</div>				
				</div>
			</td></tr>
			</tr>
			
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>              
</body>
</html>