<?php
	/*****************************************************************************
	Auteur du script : Gilbert Soueidy.
	Soci�t� : Neosurf Cards SAS.
	Date : 24/08/2004
	Version : 1.0
	Description :	Fichier de configuration n�cessaire aux APIs marchand et serveur 
	*****************************************************************************/
	
	$NEOSURF_SRV_URL = "https://www.neosurf.info/public/fr/purchase.php";
	

	$NEOSURF_GENERAL_ERRORS = array(
		'InvalidRequestKey' => 1000, // Erreur quand l'input array du marchand contient une mauvaise cl�
		'InvalidResponseKey' => 2000, // Erreur quand l'input array du serveur contient une mauvaise cl�
		'HashError' => 500, // Erreur quand le hash de la requ�te n'est pas correct
		'InvalidDataFormat' => 1001, // Erreur quand le nb de variables envoy�s par le marchand est < 2 ou quand l'URL a un mauvais format
		'RequestMechantIdError' => 1002 // Erreur quand l'Id du marchand contenu dans l'URL a �t� modifi�
	);

	/*
	 * PARAMETRES DE LA REQUETE DE PAIEMENT
	 */
	 
	// L'array contenant le nom des variables utilis�e dans la requ�te de paiement
	$NEOSURF_REQVAR = array ( 'IDMerchant', 'IDService', 'amount', 'Type', 'IDTransaction', 'URLReturn', 'URLConfirmation');


	// le tableau contenant la taille max des champs de la requ�te
	$NEOSURF_MAXLEN_REQVAR = array(
		'IDMerchant' => 6 ,
		'IDService' => 4,
		'amount' => 7,
		'Type' => 1,
		'IDTransaction' => 255,
		'URLReturn' => 255,
		'URLConfirmation' => 255
	);

	// Les erreurs retourn�s quand un champ est trop long 
	$NEOSURF_MAXLEN_REQVAR_ERRNO = array(
		'IDMerchant' => 1020,
		'IDService' => 1021,
		'amount' => 1022,
		'Type' => 1023,
		'IDTransaction' => 1024,
		'URLReturn' => 1025,
		'URLConfirmation' => 1026
	);


	// The maximum length of transaction fields
	$NEOSURF_TYPES_REQVAR = array(
		'IDMerchant' => 'N' ,
		'IDService' => 'N',
		'amount' => 'N',
		'Type' => 'N',
		'IDTransaction' => 'A',
		'URLReturn' => 'A',
		'URLConfirmation' => 'A'
	);


	// Les erreurs retourn�s quand un champ a un mauvais type
	$NEOSURF_TYPES_REQVAR_ERRNO = array(
		'IDMerchant' => 1040,
		'IDService' => 1041,
		'amount' => 1042,
		'Type' => 1043,
		'IDTransaction' => 1044,
		'URLReturn' => 1045,
		'URLConfirmation' => 1046
	);

	// Les erreurs retournes quand un champ manque
	$NEOSURF_MISSING_REQVAR_ERRNO = array(
		'IDMerchant' => 1060,
		'IDService' => 1061,
		'amount' => 1062,
		'Type' => 1063,
		'IDTransaction' => 1064,
		'URLReturn' => 1065,
		'URLConfirmation' => 1066
	);


	/*
	 * PARAMETRES DE LA REPONSE
	 */
	 
	 // L'array contenant le nom des variables utilis�e dans la r�ponse
	 $NEOSURF_RESVAR = array ('IDTransaction', 'ReponseNeosurf');

	// le tableau contenant la taille max des champs de la requ�te
	$NEOSURF_MAXLEN_RESVAR = array(
		'IDTransaction' => 255,
		'ReponseNeosurf' => 2
	);

	// Les erreurs retourn�s quand un champ est trop long 
	$NEOSURF_MAXLEN_RESVAR_ERRNO = array(
		'IDTransaction' => 2020,
		'ReponseNeosurf' => 2021
	);


	// The maximum length of transaction fields
	$NEOSURF_TYPES_RESVAR = array(
		'IDTransaction' => 'A',
		'ReponseNeosurf' => 'N'
	);


	// Les erreurs retourn�s quand un champ a un mauvais type
	$NEOSURF_TYPES_RESVAR_ERRNO = array(
		'IDTransaction' => 2040,
		'ReponseNeosurf' => 2041
	);

	// Les erreurs retournes quand un champ manque
	$NEOSURF_MISSING_RESVAR_ERRNO = array(
		'IDTransaction' => 2060,
		'ReponseNeosurf' => 2061
	);
?>