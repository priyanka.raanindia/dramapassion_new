<?php
require_once("settings.inc.php");
$http = $http;





function upordown($url) {
	$cs = curl_init();
    curl_setopt($cs, CURLOPT_URL, $url);
	curl_setopt($cs, CURLOPT_NOBODY, true);
	curl_setopt($cs, CURLOPT_FOLLOWLOCATION, true);
   	curl_setopt($cs, CURLOPT_CONNECTTIMEOUT, 10);
	curl_exec($cs);
	$status_code = curl_getinfo($cs, CURLINFO_HTTP_CODE);
	return $status_code;
}

function urlproof($string){
	$string = str_replace(" ","-",$string);
	$string = str_replace(",","",$string);	
	$string = str_replace("!","",$string);	
	$string = str_replace("'","",$string);
	$string = str_replace(".","",$string);
	$string = rtrim($string,"-");
	return $string;
}
//Calculates the difference in seconds between to timestamps
function date_difference($start,$end){
	$s = strtotime($start); // Convert datetime to UNIX seconds
	$e = strtotime($end);	// Convert datetime to UNIX seconds
	$difference = $e - $s;	// Subtract endtime with starttime
	return $difference;		// Return amount of seconds between two times
}

function getImage($filename,$folder,$w,$h,$server){
	$imagepath = "";
	$imagepath = "<img src=\"".$server."content/".$folder."/".$filename.".jpg\" width=$w height=$h ";
	if($folder != "actors"){
		$imagepath.="alt='".str_replace("%%title%%",substr(str_replace("'"," ",$filename),0,strpos($filename,"_")),_allalt_)."' title='".str_replace("%%title%%",substr(str_replace("'"," ",$filename),0,strpos($filename,"_")),_alltitle_)."'";
	}
	$imagepath.= ">";
	echo $imagepath;
}
function createRandomPassword() {
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '' ;
    while ($i <= 7) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}
function getexpirydate($expiry){
	$expirydate = array();
	$expirydif = date_difference(date("Y-m-d H:i:s"),$expiry) - 3600;
	if($expirydif <= (48*60*60) && $expirydif >= 0){
		$expirydate[0] = sec2hms($expirydif);
		$expirydate[1] = true;
	}else if($expirydif < 0){
		$expirydate[0] = $expiry;
		$expirydate[1] = false;
	}else{
		$expirydate[0] = round($expirydif / 60 / 60 / 24) ." days";
		$expirydate[1] = false;
	}
	return $expirydate;
}

function cleanup($data) {
   $data = mysql_real_escape_string(trim(strip_tags($data)));
   return $data;
}

//Converts a number of seconds in H:m:s format
function sec2hms ($sec, $padHours = false) {
    $hms = "";
    $hours = intval(intval($sec) / 3600);					// Calculate hours
    $hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'		// Add divider 'h ' if leading 0 is set true
          : $hours. ':';									// Add divider 'h ' if leading 0 is set false
    $minutes = intval(($sec / 60) % 60); 					// Calculate remaining minutes
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ':';	// Add divider 'm '
    $seconds = intval($sec % 60); 							// Calculate remaining seconds
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT). '';	// Add divider 's '
    return $hms; 											// Return #h #m #s
}

//Converts a number of seconds in d u m format
function sec2dhms ($sec, $padHours = false) {
    $hms = "";
	$days = intval(intval($sec) / (3600 * 24));
	$sec = $sec - ($days * 24 * 3600);
	$hms .= str_pad($days, 2, "0", STR_PAD_LEFT). 'd ';
    $hours = intval(intval($sec) / 3600);					// Calculate hours
    $hms .= str_pad($hours, 2, "0", STR_PAD_LEFT). 'u ';	// Add divider 'h ' if leading 0 is set true
    $minutes = intval(($sec / 60) % 60); 					// Calculate remaining minutes
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). 'm ';	// Add divider 'm '
    $seconds = intval($sec % 60); 							// Calculate remaining seconds
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT). 's';	// Add divider 's '
    return $hms; 											// Return #h #m #s
}

function valid_email($email){
	// First, we check that there's one @ symbol, and that the lengths are right
	if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
		return false;
	}
	// Split it into sections to make life easier
	$email_array = explode("@", $email);
	$local_array = explode(".", $email_array[0]);
	for ($i = 0; $i < sizeof($local_array); $i++) {
		if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
			return false;
		}
	}
	if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
		$domain_array = explode(".", $email_array[1]);
		if (sizeof($domain_array) < 2) {
			return false; // Not enough parts to domain
		}
		for ($i = 0; $i < sizeof($domain_array); $i++) {
			if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
				return false;
			}
		}
	}
	return true;
}
function valid_url($uri){
	if($uri == "" || $uri == "http://"){
		return true;
	}else{
		if( preg_match('/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}'.'((:[0-9]{1,5})?\/.*)?$/i' ,$uri)){
			return true;
		} else {
			return false;
		}
	}
}
function browser_detection( $which_test ) {
	// initialize the variables
	$browser = '';
	$dom_browser = '';

	// set to lower case to avoid errors, check to see if http_user_agent is set
	$navigator_user_agent = ( isset( $_SERVER['HTTP_USER_AGENT'] ) ) ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) : '';

	// run through the main browser possibilities, assign them to the main $browser variable
	if (stristr($navigator_user_agent, "opera")){
		$browser = 'opera';
		$dom_browser = true;
	}elseif (stristr($navigator_user_agent, "msie 4")){
		$browser = 'msie4'; 
		$dom_browser = false;
	}elseif (stristr($navigator_user_agent, "msie")){
		$browser = 'msie'; 
		$dom_browser = true;
	}elseif ((stristr($navigator_user_agent, "konqueror")) || (stristr($navigator_user_agent, "safari"))) 	{
		$browser = 'safari'; 
		$dom_browser = true;
	}elseif (stristr($navigator_user_agent, "gecko")) 	{
		$browser = 'mozilla';
		$dom_browser = true;
	}elseif (stristr($navigator_user_agent, "mozilla/4")) 	{
		$browser = 'ns4';
		$dom_browser = false;
	}else 	{
		$dom_browser = false;
		$browser = false;
	}

	// return the test result you want
	if ( $which_test == 'browser' ){
		return $browser;
	}elseif ( $which_test == 'dom' ){
		return $dom_browser;
		//  note: $dom_browser is a boolean value, true/false, so you can just test if
		// it's true or not.
	}
}
function os_detection($p_sAgent = NULL){
    ($p_sAgent===NULL ?    $sAgent = strtolower($_SERVER['HTTP_USER_AGENT']) :    $sAgent = strtolower($p_sAgent));
    $sOs = '';
        switch(true){
			case strpos($sAgent,'windows nt 6.0') : $sOs = 'Windows Vista';
			break;
			case strpos($sAgent,'windows nt 5.2') : $sOs = 'Windows 2003 server';
			break;
			case strpos($sAgent,'windows nt 5.1') : $sOs = 'Windows XP';
			break;
			case strpos($sAgent,'windows nt 5.0') : $sOs = 'Windows 2000';
			break;
			case strpos($sAgent,'windows nt') : $sOs = 'Windows NT';
			break; 
			case strpos($sAgent,'windows 98') : $sOs = 'Windows 98';
			break;
			case strpos($sAgent,'win 9x 4.90') : $sOs = 'Windows ME';
			break;
			case strpos($sAgent,'win me') :    $sOs = 'Windows ME';
			break;
			case strpos($sAgent,'win ce') : $sOs = 'Windows CE';
			break;
			case strpos($sAgent,'ubuntu') : $sOs = 'Ubuntu';
			break;
			case strpos($sAgent,'freebsd') : $sOs = 'Free BSD';
			break;
			case strpos($sAgent,'symbian') : $sOs = 'Symbian';
			break;
			case strpos($sAgent,'mac os x') : $sOs = 'Mac OS X';
			break;
			case strpos($sAgent,'macintosh') : $sOs = 'Macintosh';
			break;
			case strpos($sAgent,'linux') : $sOs = 'Linux';
			break;
			default: $sOs = 'Onbekend';
        }
	    return $sOs;
}

function os_detect(){

if (ereg("Win", getenv("HTTP_USER_AGENT")))
  $os = "Windows";
elseif ((ereg("Mac", getenv("HTTP_USER_AGENT"))) || (ereg("PPC", getenv("HTTP_USER_AGENT"))))
  $os = "Mac";
elseif (ereg("Linux", getenv("HTTP_USER_AGENT")))
  $os = "Linux";
elseif (ereg("FreeBSD", getenv("HTTP_USER_AGENT")))
  $os = "FreeBSD";
elseif (ereg("SunOS", getenv("HTTP_USER_AGENT")))
  $os = "SunOS";
elseif (ereg("IRIX", getenv("HTTP_USER_AGENT")))
  $os = "IRIX";
elseif (ereg("BeOS", getenv("HTTP_USER_AGENT")))
  $os = "BeOS";
elseif (ereg("OS/2", getenv("HTTP_USER_AGENT")))
  $os = "OS/2";
elseif (ereg("AIX", getenv("HTTP_USER_AGENT")))
  $os = "AIX";
else
  $os = "Autre";
// Après on fait ce qu'on souhaite de l'information :
// affichage, stockage dans une base de données ...

//afiche le système d'exploitation du client

return $os;

}

function getIp2Location($ip){
	$ipa = array();
	$ipa = explode(".",$ip);
	$ipno = ($ipa[0] * (256*256*256)) + ($ipa[1] * (256*256)) + ($ipa[2] * (256)) + $ipa[3];
	$rescountry = mysql_query("SELECT * FROM ipcountry WHERE IPFROM <= ".$ipno." AND IPTO >= ".$ipno."");
	$countrycode = mysql_result($rescountry,0,"COUNTRYSHORT");
	return $countrycode;
}
function generateReferral($length=16){
  $referral = "";
  $possible = "0123456789abcdfghjkmnpqrstvwxyzABCDFGHJKMNPQRSTVWXYZ";  
  $i = 0; 
  while ($i < $length) { 
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
    if (!strstr($referral, $char)) { 
      $referral .= $char;
      $i++;
    }
  }
  return $referral;
}
function rewriteCountry($country){
	if(strpos($country,',')){
		$parts = explode(',',$country);
		$country = $parts[1]." ".$parts[0];
	}
	return $country;
}
function yearOptions($endYear,$startYear,$selection){
	if($endYear == 0) $endYear = date('Y');
	for ($i=$endYear;$i >= $startYear;$i--){
		$selected = "";
		if($selection == $i){
			$selected = " SELECTED";
		}
		echo "<option value='".$i."'$selected>".$i."</option>";
	}
}
function monthOptions($lang,$selection){
	if($lang == 1){
		$months = array( 1 => "Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre" );
	}
	if($lang == 2){
		$months = array( 1 => "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" );
	}
	
	foreach ( $months as $monthNo => $month ){
		$selected = "";
		if($selection == $monthNo){
			$selected = " SELECTED";
		}
		echo '<option value="'.$monthNo.'"'.$selected.'>'.$month.'</option>';
	}
}
function dayOptions($selection){
	for ( $i = 1; $i <= 31; $i++ ){
		$selected = "";
		if($selection == $i){
			$selected = " SELECTED";
		}
		echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
	}
}

class mailsending {
	function sendingemail_phpmailer ($var_array,$template,$phpmailer,$FromName,$From,$to,$Subject){
		if (!is_array($var_array)){
			echo "first variable should be an array. ITS NOT !";
			exit;
		}
	
		require_once($phpmailer);
		// I changed this to require_once because i found that when i trued to look the class for multiple emails, the phpmailer class was recelared and hence caused issue. SO MADE THIS as require_once.
	
		$mail = new PHPMailer();

		$mail->FromName = $FromName;
		$mail->From = $From;
		$mail->AddAddress($to);
		$mail->Subject = $Subject;
		$mail->IsHTML(true); 
		
		$filename = $template;
		$fd = fopen ($filename, "r");
		$mailcontent = fread ($fd, filesize ($filename));
								
		foreach ($var_array as $key=>$value){
			$mailcontent = str_replace("%%$value[0]%%", $value[1],$mailcontent );
		}
								
		$mailcontent = stripslashes($mailcontent);
		
		fclose ($fd);
		$mail->Body=$mailcontent;
		if(!$mail->Send()){
		   echo $mail->ErrorInfo;
		   exit;
		}
	}
}
class mailsending2 {
	function sendingemail_phpmailer ($template,$phpmailer,$FromName,$From,$to,$Subject){
	
		require_once($phpmailer);
		// I changed this to require_once because i found that when i trued to look the class for multiple emails, the phpmailer class was recelared and hence caused issue. SO MADE THIS as require_once.
	
		$mail = new PHPMailer();

		$mail->FromName = $FromName;
		$mail->From = $From;
		$mail->AddAddress($to);
		$mail->Subject = $Subject;
		$mail->IsHTML(true); 
								
		$mailcontent = stripslashes($template);
		
		fclose ($fd);
		$mail->Body=$mailcontent;
		if(!$mail->Send()){
		   echo $mail->ErrorInfo;
		   exit;
		}
	}
}
function hms2sec ($hms) {
	list($h, $m, $s) = explode (":", $hms);
	$seconds = 0;
	$seconds += (intval($h) * 3600);
	$seconds += (intval($m) * 60);
	$seconds += (intval($s));
	return $seconds;
}

function checkFacebookConnect(){
	//uses the PHP SDK.  Download from https://github.com/facebook/php-sdk
	require 'facebook.php';
	$facebook = new Facebook(array(
	'appId'  => YOUR_APP_ID,
	'secret' => 'fdf05a16880bcfe68230c50be56cd1c7',));
	$userId = $facebook->getUser();
	
	if ($userId) { 
		$userInfo = $facebook->api('/' + $userId); 
	  
	}else{
		$userInfo = False;
	}
	return $userInfo;

}

function UserIsConnect(){
	$rep = 0;
	if(!isset($_SESSION['logged']) || $_SESSION['logged'] == 0){
		$rep = 0;
	}
	if($_SESSION['logged'] == 1){
		$rep = 1;
		
	}
	//$userInfo = checkFacebookConnect();
	
	if ($userInfo != false) { 
    
	  
	  $userIDfacebook = $userInfo['id'] ;
	  $userEMAILfacebook = $userInfo['email'] ;
	  
	  $userExist = mysql_query("SELECT * FROM t_dp_user WHERE (userIDfacebook = '".$userIDfacebook."' AND UserEmail = '".$userEMAILfacebook."') ");
	  $userExist_true = mysql_fetch_row($userExist);
	  
	  if(mysql_num_rows($userExist) == 0){
			$rep = 0;
	  }else{
			$rep = 1;
	  }
	}
	
	return $rep;

}

function RondomCata ($exept1, $exept2, $genre, $type){
	
	if($type == 'All'){
	$nb_drama_sql = mysql_query("SELECT * FROM t_dp_drama WHERE (ReleaseDate <='".date("Y-m-d")."' AND Categorie ='".$genre."' AND StatusID = 1)");
	$nb_drama = mysql_num_rows($nb_drama_sql);
	
	
	for($i=1; $i <= $nb_drama ; $i++){
		$tab_drama[$i] = mysql_result($nb_drama_sql,($i-1),"DramaID");
	
	}
	
	}
	
	if($type == "Pay"){
	$nb_epi_sql = mysql_query("SELECT * FROM t_dp_episode WHERE (ReleaseDateFree >='".date("Y-m-d")."' AND EpisodeNumber='2')");
	$nb_epi = mysql_num_rows($nb_epi_sql);
	for($i=1; $i <= $nb_epi ; $i++){
		$dramaID = mysql_result($nb_epi_sql,($i-1),"DramaID");
		$nb_drama_sql = mysql_query("SELECT * FROM t_dp_drama WHERE (DramaID ='".$dramaID."' AND Categorie ='".$genre."' AND StatusID = 1)");
		
		if(mysql_num_rows($nb_drama_sql) !=0){
			$tab_drama[$i] = mysql_result($nb_drama_sql,0,'DramaID');
			
		}
	}
	}
	
	if($type == "Free"){
	
	$nb_epi_sql = mysql_query("SELECT * FROM t_dp_episode WHERE (ReleaseDateFree <='".date("Y-m-d")."' AND EpisodeNumber='2')");
	$nb_epi = mysql_num_rows($nb_epi_sql);
	for($i=1; $i <= $nb_epi ; $i++){
		$dramaID = mysql_result($nb_epi_sql,($i-1),"DramaID");
		$nb_drama_sql = mysql_query("SELECT * FROM t_dp_drama WHERE (DramaID ='".$dramaID."' AND Categorie ='".$genre."' AND StatusID = 1)");
		
		if(mysql_num_rows($nb_drama_sql) !=0){
			$tab_drama[$i] = mysql_result($nb_drama_sql,0,'DramaID');
			
		}
	}
	}
	
	
	if($exept1 != 0){
	$unset_num = array_search($exept1, $tab_drama);
	unset($tab_drama[$unset_num]);
   }
   if($exept2 != 0){
   $unset_num2 = array_search($exept2, $tab_drama);
   unset($tab_drama[$unset_num2]);
   }
	
	$rep = $rand_keys = array_rand($tab_drama,1);

	
	return $tab_drama[$rep];
	

}

function CatalogueAff ($genre){
if($genre == "CataCom"){
$genre_env = "com";

}
if($genre == "CataMelo"){
$genre_env = "drame";

}
if($genre == "CataAct"){
$genre_env = "act";

}
if($genre == "CataHist"){
$genre_env = "hist";

}
	$nom_genre1 = $genre."1"; 
	$nom_genre2 = $genre."2";
	$nom_genre3 = $genre."3";
	
	$genre1 = mysql_query("SELECT * FROM t_dp_catalogue_home WHERE CataGenre ='".$nom_genre1."'");
	$genre2 = mysql_query("SELECT * FROM t_dp_catalogue_home WHERE CataGenre ='".$nom_genre2."'");
	$genre3 = mysql_query("SELECT * FROM t_dp_catalogue_home WHERE CataGenre ='".$nom_genre3."'");
	
	$resultgenre1 = mysql_result($genre1,0,"CataChoix");
	$resultgenre2 = mysql_result($genre2,0,"CataChoix");
	$resultgenre3 = mysql_result($genre3,0,"CataChoix");
	$exept = array(0,0,0,0);
	
	
	if($resultgenre1  != "Pay" || $resultgenre1  != "Free" ||$resultgenre1  != "All"){
		
		$dramaID1 = mysql_result($genre1,0,"DramaID");
		$exept[1] = $dramaID1;
	}
	if($resultgenre2  != "Pay" || $resultgenre2  != "Free" ||$resultgenre2  != "All"){
		
		$dramaID2 = mysql_result($genre2,0,"DramaID");
		$exept[2] = $dramaID2;
	}
	if($resultgenre3  != "Pay" || $resultgenre3  != "Free" ||$resultgenre3  != "All"){
		
		$dramaID3 = mysql_result($genre3,0,"DramaID");
		$exept[3] = $dramaID3;
	}

	if($exept[1] == 0){
		$exept[1] = RondomCata($exept[2], $exept[3] , $genre_env, $resultgenre1);
	}
	if($exept[2] == 0){
		$exept[2] = RondomCata($exept[1], $exept[3] , $genre_env, $resultgenre2);
	}
	if($exept[3] == 0){
		$exept[3] = RondomCata($exept[1], $exept[2] , $genre_env, $resultgenre3);
	}
	
	return $exept;
}
function CatalogueImg($tab){
	$imgsql1 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$tab[1]."'");
	$imgsql2 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$tab[2]."'");
	$imgsql3 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$tab[3]."'");
	
	$imgnom1 = mysql_result($imgsql1,0,"DramaTitle");
	$imgnom2 = mysql_result($imgsql2,0,"DramaTitle");
	$imgnom3 = mysql_result($imgsql3,0,"DramaTitle");
	
	$img[1] = "content/dramas/".$imgnom1."_Thumb.jpg";
	$img[2] = "content/dramas/".$imgnom2."_Thumb.jpg";
	$img[3] = "content/dramas/".$imgnom3."_Thumb.jpg";
	$img[1] = str_replace(' ', '%20' ,$img[1]);
	$img[2] = str_replace(' ', '%20' ,$img[2]);
	$img[3] = str_replace(' ', '%20' ,$img[3]);
	return $img;
}
function NomTypeDrama($tab){
	global $http;
	$allsql1 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$tab[1]."'");
	$allsql2 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$tab[2]."'");
	$allsql3 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$tab[3]."'");
	$allsql11 = mysql_query("SELECT * FROM t_dp_episode WHERE (DramaID ='".$tab[1]."' AND  EpisodeNumber=1)");
	$allsql21 = mysql_query("SELECT * FROM t_dp_episode WHERE (DramaID ='".$tab[2]."' AND  EpisodeNumber=1)");
	$allsql31 = mysql_query("SELECT * FROM t_dp_episode WHERE (DramaID ='".$tab[3]."' AND  EpisodeNumber=1)");
	
	$all[10] = mysql_result($allsql1,0,"DramaTitle");
	$all[20] = mysql_result($allsql2,0,"DramaTitle");
	$all[30] = mysql_result($allsql3,0,"DramaTitle");
	
	$all[15] = str_replace(' ', '%20' ,$all[10]); 
	$all[25] = str_replace(' ', '%20' ,$all[20]);
	$all[35] = str_replace(' ', '%20' ,$all[30]);
	
	$timestamp1 = mysql_result($allsql11,0,"ReleaseDateFree");
	$explode1 = explode('-', $timestamp1);
	$year1 = $explode1[0];
	$month1 = $explode1[1];
	$day1 = $explode1[2];
	$timestamp11 = mktime(0, 0, 0, $month1, $day1, $year1);
	
	$timestamp2 = mysql_result($allsql21,0,"ReleaseDateFree");
	$explode2 = explode('-', $timestamp2);
	$year2 = $explode2[0];
	$month2 = $explode2[1];
	$day2 = $explode2[2];
	$timestamp21 = mktime(0, 0, 0, $month2, $day2, $year2);
	
	$timestamp3 = mysql_result($allsql31,0,"ReleaseDateFree");
	$explode3 = explode('-', $timestamp3);
	$year3 = $explode3[0];
	$month3 = $explode3[1];
	$day3 = $explode3[2];
	$timestamp31 = mktime(0, 0, 0, $month3, $day3, $year3);
	
	
	if($timestamp11 > time()){
		$all[11] = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_1_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql1,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}else{
		$all[11] = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_2_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql1,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}
	if($timestamp21 > time()){
		$all[21] = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_1_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql2,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}else{
		$all[21] = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_2_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql2,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}
	if($timestamp31 > time()){
		$all[31] = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_1_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql3,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}else{
		$all[31] = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_2_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql3,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}
	
	
	return $all;

}


function NomTypeDramaSolo($id){
	global $http;
	$allsql1 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$id."'");
	$allsql11 = mysql_query("SELECT * FROM t_dp_episode WHERE (DramaID ='".$id."' AND  EpisodeNumber=1)");

	
	$dramaTitre = mysql_result($allsql1,0,"DramaTitle");

	
	$dramaTitre_link= str_replace(' ', '%20' ,$dramaTitre); 

	
	$timestamp1 = mysql_result($allsql11,0,"ReleaseDateFree");
	$explode_date = explode('-', $timestamp1);
	$year1 = $explode_date[0];
	$month1 = $explode_date[1];
	$day1 = $explode_date[2];
	$timestamp11 = mktime(0, 0, 0, $month1, $day1, $year1);
	

	
	
	if($timestamp11 > time()){
		$sortie = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_1_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql1,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}else{
		$sortie = '<div class="cont_align_logo"><div class="align_logo"><img src="'.$http.'images/abonnement_2_mini.png" style="width:17px"></div><div class="titre_logo_align"><span class="new_epi align_logo_titre">'.mysql_result($allsql1,0,"DramaEpisodes").' Episodes</span></div><div style="clear:both;"></div></div>';
	}

	
	
	return $sortie;

}
function lookCollection(){
	$collection = mysql_query("SELECT * FROM `t_dp_collection` WHERE `show` = 1 AND `show` = 1");
	$out = false;
	$i = 0;
	while($col=mysql_fetch_assoc($collection)){
		$out[$i]['name'] = $col['name'];
		$out[$i]['img'] = $col['img'];
		$out[$i]['short'] = $col['short'];
		$out[$i]['drama'] = $col['drama'];
		$i++;
	}
	
	return $out;
	
}
function AffTop10New(){
	global $http ;
	global $paques;
	$top10 = mysql_query("SELECT * FROM t_dp_top10 WHERE PosNow < 11");
	$name_drama = mysql_query("SELECT DramaID,DramaTitle FROM t_dp_drama");
	while($drama=mysql_fetch_assoc($name_drama)){
		$tab_name_drama[$drama['DramaID']] = $drama['DramaTitle']; 
	}
	
	
	while($data = mysql_fetch_assoc($top10)) 
    { 
		$pos_tab = $data['PosNow']*10;
		$tab_all[$pos_tab] = $tab_name_drama[$data['DramaID']];
		$tab_all[$pos_tab+1] = $data['DramaID'];
		$name_tirer[$pos_tab] = str_replace(' ', '-' ,$tab_all[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace(',','',$name_tirer[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace('\'','',$name_tirer[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace('!','',$name_tirer[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace('à','a',$name_tirer[$pos_tab]);

		
		if($data['PosPas'] != 0){
			$diff = $data['PosPas']-$data['PosNow'] ;
			if($diff < 0){
				$tab_all[$pos_tab+2] = 'neg';
				$tab_all[$pos_tab+3] = $diff * -1;
			}else if($diff > 0){
				$tab_all[$pos_tab+2] = 'pos';
				$tab_all[$pos_tab+3] = $diff;
			}else if($diff == 0){
				$tab_all[$pos_tab+2] = 'egal';
				$tab_all[$pos_tab+3] = 0;
			}
		}else{
			$tab_all[$pos_tab+2] = 'new';
			$tab_all[$pos_tab+3] = 0;
		}
		
		
	
    }
    
	
	for($i=1; $i<=10; $i++){
		$j = $i*10;
		$out[$i]['name']= $tab_all[$j] ;	
		$out[$i]['link']= $http.'drama/'.$tab_all[$j+1].'/'.$name_tirer[$j] ;
		
		
	}
	return $out;
	
	
	
}
function AffTop10 (){
	global $http ;
	global $paques;
	$top10 = mysql_query("SELECT * FROM t_dp_top10 WHERE PosNow < 11");
	$name_drama = mysql_query("SELECT DramaID,DramaTitle FROM t_dp_drama");
	while($drama=mysql_fetch_assoc($name_drama)){
		$tab_name_drama[$drama['DramaID']] = $drama['DramaTitle']; 
	}
	
	
	while($data = mysql_fetch_assoc($top10)) 
    { 
		$pos_tab = $data['PosNow']*10;
		$tab_all[$pos_tab] = $tab_name_drama[$data['DramaID']];
		$tab_all[$pos_tab+1] = $data['DramaID'];
		$name_tirer[$pos_tab] = str_replace(' ', '-' ,$tab_all[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace(',','',$name_tirer[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace('\'','',$name_tirer[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace('!','',$name_tirer[$pos_tab]);
		$name_tirer[$pos_tab] = str_replace('à','a',$name_tirer[$pos_tab]);

		
		if($data['PosPas'] != 0){
			$diff = $data['PosPas']-$data['PosNow'] ;
			if($diff < 0){
				$tab_all[$pos_tab+2] = 'neg';
				$tab_all[$pos_tab+3] = $diff * -1;
			}else if($diff > 0){
				$tab_all[$pos_tab+2] = 'pos';
				$tab_all[$pos_tab+3] = $diff;
			}else if($diff == 0){
				$tab_all[$pos_tab+2] = 'egal';
				$tab_all[$pos_tab+3] = 0;
			}
		}else{
			$tab_all[$pos_tab+2] = 'new';
			$tab_all[$pos_tab+3] = 0;
		}
		
		
	
    }
    if($paques == 1){
		$dbQpaques = "SELECT * FROM paques WHERE id = 4";
		$sqlQpaques = mysql_query($dbQpaques);
		$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
		$idPaques = mysql_result($sqlQpaques,0,"name");
		$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
		if(time()>$dateshow){	
			echo '<tr>
				<td colspan="3" class="menu_noir">Top 1<img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:20px;"> du moment</td>
			</tr>';
		}else{
			echo '<tr>
				<td colspan="3" class="menu_noir">Top 10 du moment</td>
			</tr>';
		}
	}else{
		echo '<tr>
				<td colspan="3" class="menu_noir">Top 10 du moment</td>
			</tr>';
	}
	echo '
	<tr>
		<td colspan="3"><img src="'.$http.'images/ligne250.jpg" width="250" ></td>
	</tr>';
	for($i=1; $i<=10; $i++){
							$j = $i*10;
							echo '<tr height="20px">';
							echo '<td valign="bottom" class="rose" align="left">'.$i.'</td>';
							echo '<td valign="bottom" class="noir" align="left"><a href="'.$http.'drama/'.$tab_all[$j+1].'/'.$name_tirer[$j].'" class="lien_noir">'.$tab_all[$j].'</a></td>';
							
							
							if($tab_all[$j] != ""){
							echo '<td valign="bottom" class="postop10'.$tab_all[$j+2].'" align="left"><img src="'.$http.'images/top10_'.$tab_all[$j+2].'.png" height=9 width=8/>';
							}
							if($tab_all[$j+3] != 0){
								echo '<span class="noir">'; 
								echo $tab_all[$j+3];
								echo '<span class="noir">';
							}
							echo '</td>';
							echo '</tr>';
						}
	
	
	
}

function DramaInfo($id){
	global $http;
	$sql = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$id."'");
	$nb_drama = mysql_num_rows($sql);
	$drama = array();
	if($nb_drama > 0){
	$drama['titre'] = mysql_result($sql,0,"DramaTitle");
	$drama['titre_org'] = mysql_result($sql,0,"DramaTitle2");
	$drama['anne_sortie'] = mysql_result($sql,0,"DramaYear");
	$drama['nb_epi'] = mysql_result($sql,0,"DramaEpisodes");
	$drama['synopsis']= mysql_result($sql,0,"DramaSynopsisFre");
	$drama['shortcut'] = mysql_result($sql,0,"DramaShortcut");
	$titre_big = str_replace(':', '' ,$drama['titre']);
	$drama['img_big'] = $http."content/dramas/".$titre_big."_Big.jpg";
	$drama['img_big'] = str_replace(' ', '%20' ,$drama['img_big']);
	$drama['img_thumb'] = "content/dramas/".$drama['titre']."_Thumb.jpg";
	$drama['img_thumb'] = str_replace(' ', '%20' ,$drama['img_thumb']);
	$drama['img_thumb'] = str_replace('?', '' ,$drama['img_thumb']);
	$drama['img_thumb'] = str_replace(':', '-' ,$drama['img_thumb']);
	$drama['img_thumb'] = $http.$drama['img_thumb'];
	$drama['cate'] = mysql_result($sql,0,"Categorie");
	$drama['release'] = mysql_result($sql,0,"ReleaseDate");
	$drama['releaseFR'] = date('d / m / Y',strtotime($drama['release']));
	$drama['HD'] = mysql_result($sql,0,"HD");
	$drama['nb_photo'] = mysql_result($sql,0,"nb_photo");
	$drama['photo_car'] = mysql_result($sql,0,"photo_car");
	$drama['LicensorID'] = mysql_result($sql,0, "LicensorID");
	$drama['mineur'] = mysql_result($sql,0,"mineur");
	$drama['trailer'] = mysql_result($sql,0,"Trailer");
	$drama['trailerUrl'] = mysql_result($sql,0,"TrailerUrl");
	$drama['hd'] = mysql_result($sql,0,"HD");
	
	//______________________________
	//$drama['group'] = 0;
	
		$drama['group'] = mysql_result($sql,0,"group");
	
	
	
	//______________________________
	
	$drama['img_detail'] = $http."content/dramas/".$titre_big."_Detail.jpg";
	$drama['img_detail'] = str_replace(' ', '%20' ,$drama['img_detail']);
	
	
	if($drama['nb_photo'] == 1){
		$drama['img_facebook'] = $http."content/dramas/".$titre_big."_Big.jpg";
	}else{
		$drama['img_facebook'] = $http."content/dramas/".$titre_big."_Big".$drama['nb_photo'].".jpg";
	}
	
	
	$drama['chaine_temp'] = mysql_result($sql,0,"ProducerID");
	$sql_chaine = mysql_query("SELECT * FROM t_dp_producer WHERE ProducerID ='".$drama['chaine_temp']."'");
	$nb_chaine = mysql_num_rows($sql_chaine);
	if($nb_chaine > 0){
	$drama['chaine'] = mysql_result($sql_chaine,0,"ProducerName");
	$drama['type_stream'] = mysql_result($sql, 0, 'type_stream');
	$drama['keyword'] = mysql_result($sql, 0, 'keyword');
	$drama['StatusID'] = mysql_result($sql,0, 'StatusID');
	}
	}
	return $drama ;
}
function DramaInfoCata(){
	global $http;
	$sql = mysql_query("SELECT * FROM t_dp_drama WHERE StatusID = 1 AND DramaID != 110 ORDER BY ReleaseDate DESC");
	$drama = array();
	
	while($row=mysql_fetch_array($sql)){
		
		$checkFreeReq = "SELECT * FROM t_dp_episode WHERE DramaID = ".$row["DramaID"]." AND EpisodeNumber = 2 LIMIT 0,1";
		$checkFreeSql = mysql_query($checkFreeReq);
		$checkFree = 0;
		
		if( time() >= strtotime(mysql_result($checkFreeSql,0, 'ReleaseDateFree')) ){
			$checkFree = 1;
		}
		$drama[$row["DramaID"]]['free'] = $checkFree;
		$drama[$row["DramaID"]]['genre'] = "";
		
		
		
		$drama[$row["DramaID"]]['titre'] = $row["DramaTitle"];
		$drama[$row["DramaID"]]['annee'] = $row["DramaYear"];
		$drama[$row["DramaID"]]['titre_org'] = $row["DramaTitle2"];
		$drama[$row["DramaID"]]['anne_sortie'] = $row["DramaYear"];
		$drama[$row["DramaID"]]['nb_epi'] = $row["DramaEpisodes"];
		$drama[$row["DramaID"]]['synopsis']= $row["DramaSynopsisFre"];
		$drama[$row["DramaID"]]['shortcut'] = $row["DramaShortcut"];
		$titre_big = str_replace(':', '' ,$drama[$row["DramaID"]]['titre']);
		$drama[$row["DramaID"]]['img_big'] = $http."content/dramas/".$titre_big."_Big.jpg";
		$drama[$row["DramaID"]]['img_big'] = str_replace(' ', '%20' ,$drama[$row["DramaID"]]['img_big']);
		$drama[$row["DramaID"]]['img_thumb'] = "content/dramas/".$drama[$row["DramaID"]]['titre']."_Thumb.jpg";
		$drama[$row["DramaID"]]['img_thumb'] = str_replace(' ', '%20' ,$drama[$row["DramaID"]]['img_thumb']);
		$drama[$row["DramaID"]]['img_thumb'] = str_replace('?', '' ,$drama[$row["DramaID"]]['img_thumb']);
		$drama[$row["DramaID"]]['img_thumb'] = str_replace(':', '-' ,$drama[$row["DramaID"]]['img_thumb']);
		$drama[$row["DramaID"]]['img_thumb'] = $http.$drama[$row["DramaID"]]['img_thumb'];
		$drama[$row["DramaID"]]['cate'] = $row["Categorie"];
		$drama[$row["DramaID"]]['release'] = $row["ReleaseDate"];
		$drama[$row["DramaID"]]['releaseFR'] = date('d / m / Y',strtotime($drama[$row["DramaID"]]['release']));
		$drama[$row["DramaID"]]['HD'] = $row["HD"];
		$drama[$row["DramaID"]]['nb_photo'] = $row["nb_photo"];
		$drama[$row["DramaID"]]['photo_car'] = $row["photo_car"];
		$drama[$row["DramaID"]]['LicensorID'] = $row["LicensorID"];
		$drama[$row["DramaID"]]['mineur'] = $row["mineur"];
		$drama[$row["DramaID"]]['trailer'] = $row["Trailer"];
		$drama[$row["DramaID"]]['trailerUrl'] = $row["TrailerUrl"];
		$drama[$row["DramaID"]]['hd'] = $row["HD"];
		
		//______________________________
		//$drama['group'] = 0;
		
			$drama[$row["DramaID"]]['group'] = $row["group"];
		
		
		
		//______________________________
		
		$drama[$row["DramaID"]]['img_detail'] = $http."content/dramas/".$titre_big."_Detail.jpg";
		$drama[$row["DramaID"]]['img_detail'] = str_replace(' ', '%20' ,$drama[$row["DramaID"]]['img_detail']);
		
		
		
		$reqGenre = "SELECT * FROM t_dp_dramagenrelink INNER JOIN t_dp_genrevideo ON t_dp_dramagenrelink.GenreVideoID = t_dp_genrevideo.GenreVideoID WHERE t_dp_dramagenrelink.DramaID = ".$row["DramaID"];
		$sqlGenre = mysql_query($reqGenre);
		
		while($rowGenre=mysql_fetch_array($sqlGenre)){
			$drama[$row["DramaID"]]['genre'] = $drama[$row["DramaID"]]['genre'].$rowGenre['GenreVideoDescFre'].";";
			$drama[$row["DramaID"]]['genreID'] = $drama[$row["DramaID"]]['genreID'].$rowGenre['GenreVideoID'].";";
		}
		
		
		$reqActor = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID = ".$row["DramaID"];
		$sqlActor = mysql_query($reqActor);
		
		while($rowActor=mysql_fetch_array($sqlActor)){
			$drama[$row["DramaID"]]['actorID'] = $drama[$row["DramaID"]]['actorID'].$rowActor['ActorID'].";";
		}
		
	}
	return $drama ;
}
function EpiInfo($dramaID,$Epinum){

	$sql_epi = mysql_query("SELECT * FROM t_dp_episode WHERE (DramaID ='".$dramaID."'AND EpisodeNumber=".$Epinum.")");
	$epi['Num'] = mysql_result($sql_epi,0,"EpisodeNumber");
	$epi['ReleaseDatePre'] = mysql_result($sql_epi,0,"ReleaseDatePre");
	$epi['ReleaseDateDec'] = mysql_result($sql_epi,0,"ReleaseDateDec");
	$epi['ReleaseDateFree'] = mysql_result($sql_epi,0,"ReleaseDateFree");
	$epi['ActifDec'] = mysql_result($sql_epi,0,"ActifDec");
	$epi['ActifPre']= mysql_result($sql_epi,0,"ActifPre");
	$epi['ActifFree']= mysql_result($sql_epi,0,"ActifFree");
	$epi['ActifMaint']= mysql_result($sql_epi,0,"ActifMaint");
	$epi['ReleaseDatePre_ord'] =  date("d-m-Y", strtotime($epi['ReleaseDatePre']));
	$epi['ReleaseDateDec_ord'] = date("d-m-Y", strtotime($epi['ReleaseDateDec']));
	$epi['ReleaseDateFree_ord'] = date("d-m-Y", strtotime($epi['ReleaseDateFree']));
	$epi['ReleaseDatePre_time'] =  strtotime($epi['ReleaseDatePre']);
	$epi['ReleaseDateDec_time'] = strtotime($epi['ReleaseDateDec']);
	$epi['ReleaseDateFree_time'] = strtotime($epi['ReleaseDateFree']);
	$epi['TimeEpi'] = mysql_result($sql_epi,0,"TimeEpi");
	$epi['EpiID'] = mysql_result($sql_epi,0,"EpisodeID");
	$epi['Part'] = mysql_result($sql_epi,0,"Part");


	
	return $epi ;
}
function GetCleanLinkDrama($name_drama){
	$name_drama = str_replace(' ', '-' ,$name_drama);
	$name_drama = str_replace('\'','', $name_drama);
	$name_drama = str_replace('!','', $name_drama);
	$name_drama = str_replace(',','', $name_drama);
	$name_drama = str_replace('"','', $name_drama);
	$name_drama = str_replace('.','', $name_drama);
	$name_drama = str_replace('à','a', $name_drama);
	$name_drama = str_replace(':','', $name_drama);
	return $name_drama ;
}

function AffTapEpiNew($DramaID){
	global $http;
	global $paques;
	
	$lvl_user = abo_user($_SESSION['userid']);
	$drama_tab = DramaInfo($DramaID);
	$name_drama = $drama_tab['titre'];
	$name_drama = str_replace(' ', '-' ,$name_drama);
	$name_drama = str_replace('\'','', $name_drama);
	$name_drama = str_replace('!','', $name_drama);
	$name_drama = str_replace(',','', $name_drama);
	$name_drama = str_replace('"','', $name_drama);
	$name_drama = str_replace('.','', $name_drama);
	$name_drama = str_replace('à','a', $name_drama);
	$name_drama = str_replace(':','-', $name_drama);
	
	$verif_hd = $drama_tab['HD'];
	//$os = 'IOS';
	
	for($epinb=1; $epinb <= $drama_tab['nb_epi'] ; $epinb++){
	
		$epi_tab[$epinb] = EpiInfo($DramaID,$epinb);
	}
	
	for($i=1; $i <= $drama_tab['nb_epi'] ; $i++){
		if($i < 10){
			$i_norm = "0".$i ;
		}else{
			$i_norm = $i ;
		}
		echo '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-epi-mini">';
			
			if($epi_tab[$i]["ReleaseDatePre_time"]>1306000){
				// date de sortie ? $epi_tab[$i]["ReleaseDatePre_ord"]	
				echo '<img src="http://nf04.dramapassion.com/thumb/'.$drama_tab['shortcut'].'/'.$drama_tab['shortcut'].$i_norm.'-2_thumb.jpg" class="img-responsive" />';
           	}else{
				echo '<img src="'.$http.'images/no-dispo.png" class="img-responsive" />';
			}
			echo 'Episode '.$i.'<br />';
			
			if($epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000){
				if($lvl_user == "privilege" || $lvl_user == "decouverte"){
					// play premium <a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a>
					echo '<a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" role="button" class="btn btn-gold btn-sm"><i class="fa fa-play-circle" aria-hidden="true"></i> Premium</a>';
					
				}else{
					// gris <a href="'.$http.'premium/" title="Réservé aux abonnés"><img src="'.$http.'images/play_gris" border="0"></a>
					echo '<a href="'.$http.'premium/" role="button" class="btn btn-gold btn-sm" disabled="disabled"><i class="fa fa-play-circle" aria-hidden="true"></i> Premium</a>';
						
				}
			}elseif($epi_tab[$i]["ReleaseDatePre_time"]<1306000){
				// rien pas dispo
				echo 'Bientôt disponible';
			}else{
				// afficher date premium sortie $epi_tab[$i]["ReleaseDatePre_ord"]
				echo 'Disponible : '.$epi_tab[$i]["ReleaseDatePre_ord"];
			}
			
			if(($lvl_user == "privilege" || $lvl_user == "decouverte") && ($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2)){
			}else{
			if(($epi_tab[$i]["ReleaseDateFree_time"]<time() && $epi_tab[$i]["ReleaseDateFree_time"]>7872400) || (($i_norm == "1" && $DramaID != 210 ) && $epi_tab[$i]["ReleaseDateFree_time"]>7872400 && $epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000  )){
				// dipo free <a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/"><img src="'.$http.'images/bt_play_fr.png" border="0" /></a>
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" role="button" class="btn btn-pink btn-sm"><i class="fa fa-play-circle" aria-hidden="true"></i> Gratuit</a>';
				
			}elseif($epi_tab[$i]["ReleaseDateFree_time"]<7872400){
				// pas dispo free et pas de date 
			}else{
				// pas dispo free et date $epi_tab[$i]["ReleaseDateFree_ord"]
				echo '<br />Gratuit : '.$epi_tab[$i]["ReleaseDatePre_ord"];
			}
			}
			
			
			
		echo '</div>';
	}
	

}


function AffTabEpi($DramaID,$os){
	global $http;
	global $paques;
	
	$lvl_user = abo_user($_SESSION['userid']);
	$drama_tab = DramaInfo($DramaID);
	$name_drama = $drama_tab['titre'];
	$name_drama = str_replace(' ', '-' ,$name_drama);
	$name_drama = str_replace('\'','', $name_drama);
	$name_drama = str_replace('!','', $name_drama);
	$name_drama = str_replace(',','', $name_drama);
	$name_drama = str_replace('"','', $name_drama);
	$name_drama = str_replace('.','', $name_drama);
	$name_drama = str_replace('à','a', $name_drama);
	$name_drama = str_replace(':','-', $name_drama);
	
	$verif_hd = $drama_tab['HD'];
	//$os = 'IOS';
	
	for($epinb=1; $epinb <= $drama_tab['nb_epi'] ; $epinb++){
	
		$epi_tab[$epinb] = EpiInfo($DramaID,$epinb);
	}
	
	if($os == 'Win' || $os == 'Linux' || $os == 'Mac' || $os == 'non'){
	echo'<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0">'."\n";
                        echo'<tr>';
                        if(($lvl_user == "privilege" || $lvl_user == "decouverte") && ($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2)){
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="110"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="280"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="280" class="or"><center><b>STREAMING</b></center></td>'."\n";

						}else{
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="90"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="150"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="215" class="or"><center><b>STREAMING</b></center></td>'."\n";

                        	echo'<td valign="middle" width="215" class="rose"><center><b>GRATUIT</b></center></td>'."\n";   
                        }             
						echo'</tr>'."\n";
                        echo'<tr>'."\n";
                        	echo'<td colspan="8"><img src="'.$http.'images/ligne680.jpg" ></td>'."\n";
						echo'</tr>'."\n";

 for($i=1; $i <= $drama_tab['nb_epi'] ; $i++){
	
		if($i < 10){
			$i_norm = "0".$i ;
		}else{
			$i_norm = $i ;
		}
		$tab_dl_hd = Hash_dl($i_norm,$DramaID,'hd');
		$tab_dl_sd = Hash_dl($i_norm,$DramaID,'sd');
				if(($i %2) !=0 ){
					
					echo '<tr class="tab_blanc" id="tab_'.$i_norm.'"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 1){
	                    echo '<td height="28" valign="middle">Concert uniquement (2h)</td>'."\n";
                    }else if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
	                    if($paques == 1 && $DramaID == 130 && $i == 9){
							$dbQpaques = "SELECT * FROM paques WHERE id = 5";
							$sqlQpaques = mysql_query($dbQpaques);
							$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
							$idPaques = mysql_result($sqlQpaques,0,"name");
							$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
							if(time()>$dateshow){	
								echo '<td height="28" valign="middle">Episode <img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:18px;"></td>'."\n";
							}else{
								echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
							}
						}else{
							echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
						}
                    	
                    }
				}else{	
					
					echo '<tr class="tab_gris" id="tab_'.$i_norm.'" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}
                    
						
						if($epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
                       	}else{
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}
						
						
						if($epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							if($lvl_user == "privilege" || $lvl_user == "decouverte"){
								echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a></center></td>'."\n";
								
							}else{
								echo '<td height="25" valign="middle"><center><a href="'.$http.'premium/" title="Réservé aux abonnés"><img src="'.$http.'images/play_gris" border="0"></a></center></td>'."\n";
								
							}
						}elseif($epi_tab[$i]["ReleaseDatePre_time"]<1306000){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
						}
						
						
						if(($lvl_user == "privilege" || $lvl_user == "decouverte") && ($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2)){
						}else{
						if(($epi_tab[$i]["ReleaseDateFree_time"]<time() && $epi_tab[$i]["ReleaseDateFree_time"]>7872400) || (($i_norm == "1" && $DramaID != 210 ) && $epi_tab[$i]["ReleaseDateFree_time"]>7872400 && $epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000  )){
							echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/"><img src="'.$http.'images/bt_play_fr.png" border="0" /></a></center></td>'."\n";
						}elseif($epi_tab[$i]["ReleaseDateFree_time"]<7872400){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDateFree_ord"].'</center></td>'."\n";
						}
						}
						
						
					echo '</tr>'."\n"; 
			 
		  } //echo '<td height="25" valign="middle"><center><a href="#" class="lien_fushia">02.02.12</a></center></td>'; 
	echo "</table>"."\n";
	}elseif($os == 'Ps' || $os == 'Wii'){
		echo'<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0">';
                        echo'<tr>';
                        	echo'<td valign="middle" width="10">&nbsp;</td>';
                        	echo'<td valign="middle" width="90"><b>EPISODE</b></td>';
                        	echo'<td valign="middle" width="150"><center><b>DATE DE SORTIE</b></td>';
                        	echo'<td valign="middle" width="280" class="or"><center><b>STREAMING</b></center></td>';
                        	echo'<td valign="middle" width="150" class="rose"><center><b>GRATUIT</b></center></td>';                
						echo'</tr>';
                        echo'<tr>';
                        	echo'<td colspan="8"><img src="'.$http.'images/ligne680.jpg"></td>';
						echo'</tr>';

 for($i=1; $i <= $drama_tab['nb_epi'] ; $i++){
		if($i < 10){
			$i_norm = "0".$i ;
		}else{
			$i_norm = $i ;
		}
			
				if(($i %2) !=0 ){
					
		
					echo '<tr class="tab_blanc"  id="tab_'.$i_norm.'"  style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">';
                    echo '<td valign="top">&nbsp;</td>';
                    if($DramaID == 210 && $i == 1){
	                    echo '<td height="28" valign="middle">Concert uniquement (2h)</td>'."\n";
                    }else if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}else{	
					
					echo '<tr class="tab_gris"  id="tab_'.$i_norm.'" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">';
                    echo '<td valign="top">&nbsp;</td>';
                    if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}
                    
						
						if($epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>';
                       	}else{
							echo '<td height="25" valign="middle"><center></center></td>';
						}
						
						
						if($epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000){
						if($drama_tab['type_stream'] == 0){
							if($verif_hd == 1){
							if($lvl_user == "privilege"){
								echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0)" onclick="valide(\'sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="valide(\'hd\','.$i_norm.')"><img src="'.$http.'images/picto_hd.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
							}elseif($lvl_user == "decouverte"){
								$time_epi_disc = $epi_tab[$i]["ReleaseDateDec_time"];
								
								if($time_epi_disc < time()){
									echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0)" onclick="valide(\'sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)"><img src="'.$http.'images/picto_hd_gris.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
								}else{
									echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/picto_plus.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)"><img src="'.$http.'images/picto_hd_gris.png" border="0"></a></center></td>';
								}
							}else{
								echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/bt_dl_gris.png" border="0"></a></center></td>';
							}
						}else{
							if($lvl_user == "privilege"){
								echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0);" onclick="valide(\'sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
							}elseif($lvl_user == "decouverte"){
								$time_epi_disc = $epi_tab[$i]["ReleaseDateDec_time"];
								
								if($time_epi_disc < time()){
									echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0);" onclick="valide(\'sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
								}else{
									echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/picto_plus.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);"><img src="'.$http.'images/picto_hd_gris.png" border="0"></a></center></td>';
								}
							}else{
								echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/bt_dl_gris.png" border="0"></a></center></td>';
							}
						}						
						}elseif($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2){
						if($verif_hd == 1){
							if($lvl_user == "privilege"){
								echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0)" onclick="valide(\'auto_sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="valide(\'auto_hd\','.$i_norm.')"><img src="'.$http.'images/picto_hd.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
							}elseif($lvl_user == "decouverte"){
								$time_epi_disc = $epi_tab[$i]["ReleaseDateDec_time"];
								
								if($time_epi_disc < time()){
									echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0);" onclick="valide(\'auto_sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)"><img src="'.$http.'images/picto_hd_gris.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
								}else{
									echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/picto_plus.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);"><img src="'.$http.'images/picto_hd_gris.png" border="0"></a></center></td>';
								}
							}else{
								echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/bt_dl_gris.png" border="0"></a></center></td>';
							}
						}else{
							if($lvl_user == "privilege"){
								echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0);" onclick="valide(\'auto_sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
							}elseif($lvl_user == "decouverte"){
								$time_epi_disc = $epi_tab[$i]["ReleaseDateDec_time"];
								
								if($time_epi_disc < time()){
									echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0);" onclick="valide(\'auto_sd\','.$i_norm.')"><img src="'.$http.'images/picto_sd.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
								}else{
									echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/picto_plus.png" border="0"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);"><img src="'.$http.'images/picto_hd_gris.png" border="0"></a></center></td>';
								}
							}else{
								echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/bt_dl_gris.png" border="0"></a></center></td>';
							}

						}
						}
						}elseif($epi_tab[$i]["ReleaseDatePre_time"]<1306000){
							echo '<td height="25" valign="middle"><center></center></td>';
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>';
						}
						
						
						
						
						if(($epi_tab[$i]["ReleaseDateFree_time"]<time() && $epi_tab[$i]["ReleaseDateFree_time"]>7872400) || (($i_norm == "1" && $DramaID != 210 ) && $epi_tab[$i]["ReleaseDateFree_time"]>7872400)){
							echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_free_'.$i_norm.'"><a href="javascript:void(0)" onclick="valide(\'free\','.$i_norm.')"><img src="'.$http.'images/bt_play_fr.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_free_'.$i_norm.'" value=""/></form></center></td>';
						}elseif($epi_tab[$i]["ReleaseDateFree_time"]<7872400){
							echo '<td height="25" valign="middle"><center></center></td>';
						}else{
							echo '<td height="25" valign="middle"><center><a href="#" class="lien_fushia">'.$epi_tab[$i]["ReleaseDateFree_ord"].'</a></center></td>';
						}
					
					echo '</tr>'; 
			 
		  } //echo '<td height="25" valign="middle"><center><a href="#" class="lien_fushia">02.02.12</a></center></td>'; 
	echo "</table>";
	
	}elseif($os == 'RT'){
	echo'<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0">'."\n";
                        echo'<tr>';
                        if($lvl_user == "privilege" || $lvl_user == "decouverte"){
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="110"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="200"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="360" class="or"><center><b>STREAMING</b></center></td>'."\n"; 
						}else{
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="90"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="150"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="215" class="or"><center><b>PRIVILEGE</b></center></td>'."\n";
                        	echo'<td valign="middle" width="215" class="rose"><center><b>GRATUIT</b></center></td>'."\n";   
                        }             
						echo'</tr>'."\n";
                        echo'<tr>'."\n";
                        	echo'<td colspan="8"><img src="'.$http.'images/ligne680.jpg" ></td>'."\n";
						echo'</tr>'."\n";

 for($i=1; $i <= $drama_tab['nb_epi'] ; $i++){
	
		if($i < 10){
			$i_norm = "0".$i ;
		}else{
			$i_norm = $i ;
		}
				if(($i %2) !=0 ){
					
					echo '<tr class="tab_blanc" id="tab_'.$i_norm.'"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 1){
	                    echo '<td height="28" valign="middle">Concert uniquement (2h)</td>'."\n";
                    }else if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}else{	
					
					echo '<tr class="tab_gris" id="tab_'.$i_norm.'" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}
                    
						
						if($epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
                       	}else{
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}
						
						
						if($epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							if($lvl_user == "privilege"){
									
									echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a></center></td>'."\n";			

							}elseif($lvl_user == "decouverte"){
								$time_epi_disc = $epi_tab[$i]["ReleaseDateDec_time"];
								
								if($i == 1&& $epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000 ){
									
									echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a></center></td>'."\n";			
										
									
								
								}else{
									if($time_epi_disc < time()){
											
												echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a></center></td>'."\n";			
										
											
										
									}else{
										echo '<td height="25" valign="middle"><center>'.date('j-m-Y',$epi_tab[$i]["ReleaseDateDec_time"]).'</center></td>'."\n";
									}
								}
							}else{
									
										echo '<td height="25" valign="middle"><center><a href="'.$http.'premium/" title="Réservé aux abonnés"><img src="'.$http.'images/play_gris" border="0"></a></center></td>'."\n";
									
								
								
							}
						}elseif($epi_tab[$i]["ReleaseDatePre_time"]<1306000){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{
							if($lvl_user == "decouverte"){
								echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDateDec_ord"].'</center></td>'."\n";
							}else{
								echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
							}
						}
						
						
						
						if($lvl_user == "privilege" || $lvl_user == "decouverte"){
						}else{
						if(($epi_tab[$i]["ReleaseDateFree_time"]<time() && $epi_tab[$i]["ReleaseDateFree_time"]>7872400) || (($i_norm == "1" && $DramaID != 210 ) && $epi_tab[$i]["ReleaseDateFree_time"]>7872400 && $epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000  )){
							echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/"><img src="'.$http.'images/bt_play_fr.png" border="0" /></a></center></td>'."\n";
						}elseif($epi_tab[$i]["ReleaseDateFree_time"]<7872400){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDateFree_ord"].'</center></td>'."\n";
						}
						}
						
						
					echo '</tr>'."\n"; 
			 
		  } //echo '<td height="25" valign="middle"><center><a href="#" class="lien_fushia">02.02.12</a></center></td>'; 
	echo "</table>"."\n";
	}elseif($os == 'Android'){
	echo'<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0">'."\n";
                        echo'<tr>';
                        if($lvl_user == "privilege" || $_SESSION['userid'] == 3 || $_SESSION['userid'] == 86){
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="110"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="200"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="360" class="or"><center><b>STREAMING</b></center></td>'."\n"; 
						}else{
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="90"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="150"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="215" class="or"><center><b>PRIVILEGE</b></center></td>'."\n";
                        	echo'<td valign="middle" width="215" class="rose"><center><b>GRATUIT</b></center></td>'."\n";   
                        }             
						echo'</tr>'."\n";
                        echo'<tr>'."\n";
                        	echo'<td colspan="8"><img src="'.$http.'images/ligne680.jpg" ></td>'."\n";
						echo'</tr>'."\n";

 for($i=1; $i <= $drama_tab['nb_epi'] ; $i++){
	
		if($i < 10){
			$i_norm = "0".$i ;
		}else{
			$i_norm = $i ;
		}
				if(($i %2) !=0 ){
					
					echo '<tr class="tab_blanc" id="tab_'.$i_norm.'"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 1){
	                    echo '<td height="28" valign="middle">Concert uniquement (2h)</td>'."\n";
                    }else if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}else{	
					
					echo '<tr class="tab_gris" id="tab_'.$i_norm.'" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}
                    
						
						if($epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
                       	}else{
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}
						
						
						if($epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							if($lvl_user == "privilege" || $_SESSION['userid'] == 3 || $_SESSION['userid'] == 86){
									
									echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a></center></td>'."\n";			

							}else{
									
										echo '<td height="25" valign="middle"><center><a href="'.$http.'premium/" title="Réservé aux abonnés"><img src="'.$http.'images/play_gris" border="0"></a></center></td>'."\n";
									
								
								
							}
						}elseif($epi_tab[$i]["ReleaseDatePre_time"]<1306000){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{

							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
							
						}
						
						
						
						if($lvl_user == "privilege" || $_SESSION['userid'] == 3 || $_SESSION['userid'] == 86){
						}else{
						if(($epi_tab[$i]["ReleaseDateFree_time"]<time() && $epi_tab[$i]["ReleaseDateFree_time"]>7872400) || (($i_norm == "1" && $DramaID != 210 ) && $epi_tab[$i]["ReleaseDateFree_time"]>7872400 && $epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000  )){
							echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/"><img src="'.$http.'images/bt_play_fr.png" border="0" /></a></center></td>'."\n";
						}elseif($epi_tab[$i]["ReleaseDateFree_time"]<7872400){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDateFree_ord"].'</center></td>'."\n";
						}
						}
						
						
					echo '</tr>'."\n"; 
			 
		  } //echo '<td height="25" valign="middle"><center><a href="#" class="lien_fushia">02.02.12</a></center></td>'; 
	echo "</table>"."\n";
	}elseif($os == 'IOS' || $os == 'IOS_ipad'){
	
		echo'<table width="720" cellpadding="0" cellspacing="0" class="noir" border="0">';
                        echo'<tr>';
                        	echo'<td valign="middle" width="40">&nbsp;</td>';
                        	echo'<td valign="middle" width="145"><b>EPISODE</b></td>';
                        	echo'<td valign="middle" width="175"><center><b>DATE DE SORTIE</b></td>';
                        if($drama_tab['type_stream'] == 0){
							echo'<td valign="middle" width="175" class="or"><center><img src="'.$http.'images/pics015.png" style="margin-right:5px;" align="absmiddle"><b>BASSE QUALITE</b></center></td>';
                        	echo'<td valign="middle" width="185" class="or"><center><img src="'.$http.'images/pics015.png" style="margin-right:5px;" align="absmiddle"><b>HAUTE QUALITE</b></center></td>';
                        }elseif($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2){
                        	echo'<td valign="middle" width="360" class="or"><center><img src="'.$http.'images/pics015.png" style="margin-right:5px;" align="absmiddle"><b>STREAMING</b></center></td>';
                        }
                                
						echo'</tr>';
                        echo'<tr>';
                        	echo'<td colspan="8"><img src="'.$http.'images/ligne720.jpg" ></td>';
						echo'</tr>';

 for($i=1; $i <= $drama_tab['nb_epi'] ; $i++){
		if($i < 10){
			$i_norm = "0".$i ;
		}else{
			$i_norm = $i ;
		}
			
				if(($i %2) !=0 ){
					
		
					echo '<tr class="tab_blanc" height="45"  id="tab_'.$i_norm.'" style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">';
                    echo '<td valign="top">&nbsp;</td>';
                    if($DramaID == 210 && $i == 1){
	                    echo '<td height="28" valign="middle">Concert uniquement (2h)</td>'."\n";
                    }else if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}else{	
					
					echo '<tr class="tab_gris" height="45" id="tab_'.$i_norm.'" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">';
                    echo '<td valign="top">&nbsp;</td>';
                    if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}
                    
						
						if($epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>';
                       	}else{
							echo '<td height="25" valign="middle"><center></center></td>';
						}
						
						
					
						
						
						if($epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000){
	
							if($lvl_user == "privilege" || $_SESSION['userid'] == 3 || $_SESSION['userid'] == 86){
								if($drama_tab['type_stream'] == 0){
									echo '<td height="25" valign="middle"><center><form action="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" method="POST" name="form_hd_'.$i_norm.'"><a href="javascript:void(0)" onclick="valide(\'sd\','.$i_norm.')"><img src="'.$http.'images/pics019.png" border="0"></a></center></td>';
									echo '<td height="25" valign="middle"><center><a href="javascript:void(0)" onclick="valide(\'hd\','.$i_norm.')"><img src="'.$http.'images/pics019.png" border="0"></a><input type="hidden" name="hidden_type" id="hidden_hd_'.$i_norm.'" value=""/></form></center></td>';
								}elseif($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2){
									echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a></center></td>';
								}
							}else{
								if($drama_tab['type_stream'] == 0){
									echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/bt_ios_gris.png" border="0"></a></center></td>';
									echo '<td height="25" valign="middle"><center><a href="javascript:void(0)"><img src="'.$http.'images/bt_ios_gris.png" border="0"></a></center></td>';
								}elseif($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2){
									echo '<td height="25" valign="middle"><center><a href="'.$http.'premium/"><img src="'.$http.'images/play_gris.png" border="0"></a></center></td>';
								}
							}
						}elseif($epi_tab[$i]["ReleaseDatePre_time"]<1306000){
							echo '<td height="25" valign="middle"><center></center></td>';
							echo '<td height="25" valign="middle"><center></center></td>';
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>';
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>';
						}
						
						
					
					echo '</tr>'; 
			 
		  } //echo '<td height="25" valign="middle"><center><a href="#" class="lien_fushia">02.02.12</a></center></td>'; 
	echo "</table>";
	
	
	}else{
	echo'<table width="680" cellpadding="0" cellspacing="0" class="noir" border="0">'."\n";
                        echo'<tr>';
                        if(($lvl_user == "privilege" || $lvl_user == "decouverte") && ($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2)){
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="110"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="200"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="200" class="or"><center><b>STREAMING</b></center></td>'."\n";

						}else{
                        	echo'<td valign="middle" width="10">&nbsp;</td>'."\n";
                        	echo'<td valign="middle" width="90"><b>EPISODE</b></td>'."\n";
                        	echo'<td valign="middle" width="150"><center><b>DATE DE SORTIE</b></td>'."\n";
                        	echo'<td valign="middle" width="130" class="or"><center><b>STREAMING</b></center></td>'."\n";

                        	echo'<td valign="middle" width="150" class="rose"><center><b>GRATUIT</b></center></td>'."\n";   
                        }             
						echo'</tr>'."\n";
                        echo'<tr>'."\n";
                        	echo'<td colspan="8"><img src="'.$http.'images/ligne680.jpg" ></td>'."\n";
						echo'</tr>'."\n";

 for($i=1; $i <= $drama_tab['nb_epi'] ; $i++){
	
		if($i < 10){
			$i_norm = "0".$i ;
		}else{
			$i_norm = $i ;
		}
		$tab_dl_hd = Hash_dl($i_norm,$DramaID,'hd');
		$tab_dl_sd = Hash_dl($i_norm,$DramaID,'sd');
				if(($i %2) !=0 ){
					
					echo '<tr class="tab_blanc" id="tab_'.$i_norm.'"   style="padding-bottom:3px; padding-top:3px; background-color:#FFFFFF">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 1){
	                    echo '<td height="28" valign="middle">Concert uniquement (2h)</td>'."\n";
                    }else if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
	                    if($paques == 1 && $DramaID == 130 && $i == 9){
							$dbQpaques = "SELECT * FROM paques WHERE id = 5";
							$sqlQpaques = mysql_query($dbQpaques);
							$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
							$idPaques = mysql_result($sqlQpaques,0,"name");
							$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
							if(time()>$dateshow){	
								echo '<td height="28" valign="middle">Episode <img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:18px;"></td>'."\n";
							}else{
								echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
							}
						}else{
							echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
						}
                    	
                    }
				}else{	
					
					echo '<tr class="tab_gris" id="tab_'.$i_norm.'" style="padding-bottom:3px; padding-top:3px; background-color:#ecebeb">'."\n";
                    echo '<td valign="top">&nbsp;</td>'."\n";
                    if($DramaID == 210 && $i == 2){
	                    echo '<td height="28" valign="middle">Concert + remise des prix (4h)</td>'."\n";
                    }else{
                    	echo '<td height="28" valign="middle">Episode '.$i.'</td>'."\n";
                    }
				}
                    
						
						if($epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
                       	}else{
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}
						
						
						if($epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000){
							if($lvl_user == "privilege" || $lvl_user == "decouverte"){
								echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/" ><img  src="'.$http.'images/play.png" border="0"></a></center></td>'."\n";
								
							}else{
								echo '<td height="25" valign="middle"><center><a href="'.$http.'premium/" title="Réservé aux abonnés"><img src="'.$http.'images/play_gris" border="0"></a></center></td>'."\n";
								
							}
						}elseif($epi_tab[$i]["ReleaseDatePre_time"]<1306000){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDatePre_ord"].'</center></td>'."\n";
						}
						
						
						if(($lvl_user == "privilege" || $lvl_user == "decouverte") && ($drama_tab['type_stream'] == 1 || $drama_tab['type_stream'] == 2)){
						}else{
						if(($epi_tab[$i]["ReleaseDateFree_time"]<time() && $epi_tab[$i]["ReleaseDateFree_time"]>7872400) || (($i_norm == "1" && $DramaID != 210 ) && $epi_tab[$i]["ReleaseDateFree_time"]>7872400 && $epi_tab[$i]["ReleaseDatePre_time"]<time() && $epi_tab[$i]["ReleaseDatePre_time"]>1306000  )){
							echo '<td height="25" valign="middle"><center><a href="'.$http.'drama/'.$DramaID.'/'.$name_drama.'/'.$i_norm.'/"><img src="'.$http.'images/bt_play_fr.png" border="0" /></a></center></td>'."\n";
						}elseif($epi_tab[$i]["ReleaseDateFree_time"]<7872400){
							echo '<td height="25" valign="middle"><center></center></td>'."\n";
						}else{
							echo '<td height="25" valign="middle"><center>'.$epi_tab[$i]["ReleaseDateFree_ord"].'</center></td>'."\n";
						}
						}
						
						
					echo '</tr>'."\n"; 
			 
		  } //echo '<td height="25" valign="middle"><center><a href="#" class="lien_fushia">02.02.12</a></center></td>'; 
	echo "</table>"."\n";
	}
}

function Hash_st_HD($epiNB,$dramaID,$type){
	$msql1 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql1,0,"DramaShortcut");
	$secret = "DramaPassion2009";
	$uri_prefix = "vid/";

	$ipLimitation = true;                 // Same as AuthTokenLimitByIp
	$hexTime = dechex(time());             // Time in Hexadecimal

	$f1 = "/".$shortcut."/".$shortcut.$epiNB."-".$type."-st.f4v";

	$token = md5($secret . $f1 . $hexTime . $_SERVER['REMOTE_ADDR']);
	$hash1 = $uri_prefix . $token. "/" . $hexTime . $f1;
	
	return $hash1;
}
function Hash_st_HD_Console($epiNB,$dramaID,$type){
	$msql1 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql1,0,"DramaShortcut");
	$secret = "DramaPassion2009";
	$uri_prefix = "vid/";

	$ipLimitation = true;                 // Same as AuthTokenLimitByIp
	$hexTime = dechex(time());             // Time in Hexadecimal

	$f1 = "/".$shortcut."/".$shortcut.$epiNB."-".$type."-ps.f4v";

	$token = md5($secret . $f1 . $hexTime . $_SERVER['REMOTE_ADDR']);
	$hash1 = $uri_prefix . $token. "/" . $hexTime . $f1;

	return $hash1;
}
function Hash_st_HD_Mobile($epiNB,$dramaID,$type){
	$msql1 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql1,0,"DramaShortcut");
	$secret = "DramaPassion2009";
	$uri_prefix = "vid/";

	$ipLimitation = true;                 // Same as AuthTokenLimitByIp
	$hexTime = dechex(time());             // Time in Hexadecimal

	$f1 = "/".$shortcut."/".$shortcut.$epiNB."-pd.mp4";

	$token = md5($secret . $f1 . $hexTime . $_SERVER['REMOTE_ADDR']);
	$hash1 = $uri_prefix . $token. "/" . $hexTime . $f1;

	return $hash1;
}
function Hash_epi_IOS($epiNB,$dramaID,$type){
	$msql1 = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql1,0,"DramaShortcut");
	$secret = "DramaPassion2009";
	$uri_prefix = "vid/";

	$ipLimitation = true;                 // Same as AuthTokenLimitByIp
	$hexTime = dechex(time());             // Time in Hexadecimal

	$f1 = "/".$shortcut."/".$shortcut.$epiNB."-pd.mp4";

	$token = md5($secret . $f1 . $hexTime . $_SERVER['REMOTE_ADDR']);
	$hash1 = $uri_prefix . $token. "/" . $hexTime . $f1;

	return $hash1;
}
function Hash_free($epiNB,$dramaID,$part){
	$msql = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql,0,"DramaShortcut");
	$secret = "DramaPassion2009";
	$uri_prefix = "vid/";

	$ipLimitation = true;                 // Same as AuthTokenLimitByIp
	$hexTime = dechex(time());             // Time in Hexadecimal

	$f1 = "/".$shortcut."/".$shortcut.$epiNB."-fd".$part.".mp4";

	$token = md5($secret . $f1 . $hexTime . $_SERVER['REMOTE_ADDR']);
	$hash1 = $uri_prefix . $token. "/" . $hexTime . $f1;
	
	return $hash1;

}
function Hash_free_base($epiNB,$dramaID,$part){
	$msql = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql,0,"DramaShortcut");
	$secret = "DramaPassion2009";
	$uri_prefix = "vid/";

	$ipLimitation = true;                 // Same as AuthTokenLimitByIp
	$hexTime = dechex(time());             // Time in Hexadecimal

	$f1 = $shortcut.$epiNB."-fd".$part.".mp4";
	$f2 = "/".$shortcut."/".$shortcut.$epiNB."-fd".$part.".mp4";

	$token = md5($secret . $f2 . $hexTime . $_SERVER['REMOTE_ADDR']);
	$sortie[1] = $uri_prefix . $token. "/" . $hexTime . "/" . $shortcut;
	$sortie[2] = $f1;
	
	return $sortie;

}
function Hash_ltpd($epiNB,$dramaID){
	$msql = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql,0,"DramaShortcut");
	$secret = "DramaPassion2009";
	$uri_prefix = "vid/";
	$f1 = "/".$shortcut."/".$shortcut.$epiNB."-fd.mp4";
	$t = time();
	$t_hex = sprintf("%08x", $t);
	$m1 = md5($secret.$f1.$t_hex);
	$hash1 = sprintf('%s%s/%s%s', $uri_prefix, $m1, $t_hex, $f1);

	return $hash1;

}
function Hash_dl($epiNB,$dramaID,$type){
	$msql_dl = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$dramaID."'");
	$shortcut = mysql_result($msql_dl,0,"DramaShortcut");
	$name = mysql_result($msql_dl,0,"DramaTitle");
	$msql2 = mysql_query("SELECT EpisodeID FROM t_dp_episode WHERE DramaID ='".$dramaID."' AND EpisodeNumber = '".$epiNB."'");
	$epi_id = mysql_result($msql2,0,"EpisodeID");
	$secret = "DramaPassion2009";
	$t = time();

	$f1 = "/".$shortcut."/".$shortcut.$epiNB."-".$type."-dl.f4v#".$name."#".$epiNB;
	
	$hash_dl = base64_encode($t);
	$hash_dl = base64_encode($hash_dl);

	$hash_temp = $hash_dl.";".$f1;
	$hash_temp = $hash_temp.";".md5($secret);
	$hash_dl2 = base64_encode($hash_temp);

	$sortie[1] = $hash_dl;
	$sortie[2] = $hash_dl2;
	$sortie[3] = $epi_id;
	$sortie[4] = $type;
	
	return $sortie;
}
/*
function Titre_drama($id){
	$sql_nom = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$id."'");
	$nom = mysql_result($sql_nom,0,"DramaTitle");
	
	return $nom;

}
*/
function Shortcut_drama($id){
	$msql_short = mysql_query("SELECT * FROM t_dp_drama WHERE DramaID ='".$id."'");
	$shortcut = mysql_result($msql_short,0,"DramaShortcut");
	
	return $shortcut;
}
function Afficher_slider($id,$id_carr){
	if($id > 1){
	$tab_all = DramaInfo($id);
	$nom = $tab_all['titre'];
	$nb_photo = $tab_all['nb_photo'];
	$photo_car = $tab_all['photo_car'];
	
	$nom_photo = str_replace(':','', $nom);
	
	if($photo_car == 1){
	$img = "content/dramas/".$nom_photo."_Big.jpg";
	$img = str_replace(' ', '%20' ,$img);
	}else{
	$img = "content/dramas/".$nom_photo."_Big".$photo_car.".jpg";
	$img = str_replace(' ', '%20' ,$img);
	
	}
		
	$name_tiret = dramaLinkClean($nom);
	
	$synopsis = $tab_all['synopsis'];
	$chaine = $synopsis;
	
	$lg_max = 250; //nombre de caractère autoriser

	if (strlen($chaine) > $lg_max){
		$chaine = substr($chaine, 0, $lg_max);
		$last_space = strrpos($chaine, " ");
		$chaine = substr($chaine, 0, $last_space)."...";
	}
	$synopsis = $chaine;
	
	
	
	echo "<li>";
					echo'<table width="1000" height="320" border="0" cellpadding="0" cellspacing="0">';
					echo'<tr>';
                    echo'<td width="360" height="320" style="background-image:url('.$http.'images/jquery_gauche.png); background-repeat:repeat-x;" valign="top" >';
                        echo'<table width="360">';
                            echo'<tr>';
                                echo'<td width="20"></td>';
								echo'<td valign="top">';
                        echo'<br />';
                        
                        echo'<h1 class="menu_blanc">'.$nom.'</h1>';
                        
                        
                        echo'<h2 class="rose">'.$tab_all['nb_epi'].' épisodes</h2>';
                        
                        echo'<p class="blanc" align="justify">'.$synopsis.'</p><br />';
                        
                        echo'<p class="blanc" align="justify" valign="top">';
						AffGenreDrama($id);
						echo '</p>';
                        echo'<br />';
                        echo'<p><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="Regarder '.$nom.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
                                echo'</td>';
                                echo'<td width="20"></td>';
                            echo'</tr>';
                        echo'</table>';                            
                    echo'</td>';
                    echo'<td><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$img.'" width="640" height="320" alt="série coréenne &quot;'.$nom.'&quot; en vostfr" title="drama coréen &quot;'.$nom.'&quot; en vostfr" border="0"></a></td>';
					echo'</tr>';
					echo'</table>';
	echo'</li>';
	
	

	}else{
		if($id > 0){

	echo '<li style="width:1000px;height:320px;">';
		echo '<a href="'.$http.'http://www.dramapassion.com/dvd/detail/5/Coffret-DVD-City-Hunter"><img src="'.$http.'images/slider3.jpg" /></a>';
	echo '</li>';

	
	
		}else{
	$sql_car2 = mysql_query("SELECT * FROM t_dp_carrousel WHERE CarrID = ".$id_carr."");
	$titre = mysql_result($sql_car2,0,"CarrTitre");
	$texte = mysql_result($sql_car2,0,"CarrTexte");
	$bt = mysql_result($sql_car2,0,"CarrBt");
	$photo = mysql_result($sql_car2,0,"CarrPhoto");
	$lien = mysql_result($sql_car2,0,"CarrLien");
	/*
	echo "<li>";
					echo'<table width="1000" height="320" border="0" cellpadding="0" cellspacing="0">';
					echo'<tr>';
                    echo'<td width="360" height="320" style="background-image:url('.$http.'images/jquery_gauche.png); background-repeat:repeat-x;" valign="top" >';
                        echo'<table width="360">';
                            echo'<tr>';
                                echo'<td width="20"></td>';
								echo'<td valign="top">';
                        echo'<br />';
                        
                        echo'<h1 class="menu_blanc">'.$titre.'</h1>';
                        
                        echo'<p class="blanc" align="justify">'.$texte.'</p>';
                        
                        if($bt == 'yes'){
						 echo'<br />';
                        echo'<p><a href="'.$lien.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="'.$titre.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
                        }elseif($bt == 2){
	                        echo'<br />';
                        echo'<p><a href="'.$lien.'"><img src="'.$http.'images/preco.png" width="118" height="31" border="0" title="'.$titre.'"></a></p>';
                        }                
								
								echo'</td>';
                                echo'<td width="20"></td>';
                            echo'</tr>';
                        echo'</table>';                            
                    echo'</td>';
                    echo'<td><a href="'.$lien.'"><img src="'.$photo.'" width="640" height="320" alt="" border="0"></a></td>';
					echo'</tr>';
					echo'</table>';
	echo'</li>';
	*/
	$detect2 = new Mobile_Detect();
	if($detect2->isTablet()){
	echo '<li>';
		echo '<a href="lien_app.php"><img src="'.$http.'images/slider.jpg" /></a>';
	echo '</li>';
	}else{
	echo '<li>';
		echo '<table id="Tableau_01" width="1000" height="320" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<img src="'.$http.'images/slider_01.jpg" width="1000" height="238" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="'.$http.'images/slider_02.jpg" width="108" height="82" alt=""></td>
		<td>
			<a href="https://itunes.apple.com/fr/app/dramapassion/id641121786?mt=8"><img src="'.$http.'images/slider_03.jpg" width="154" height="56" alt=""></a></td>
		<td>
			<a href="https://play.google.com/store/apps/details?id=com.dramapassion.dramapassion"><img src="'.$http.'images/slider_04.jpg" width="155" height="56" alt=""></a></td>
		<td rowspan="2">
			<img src="'.$http.'images/slider_05.jpg" width="583" height="82" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="'.$http.'images/slider_06.jpg" width="309" height="26" alt=""></td>
	</tr>
</table>';

	echo '</li>';
	}
	}
}
	
	
	
	
}
function Afficher_slider2($id,$id_carr){
	if($id > 2){
	$tab_all = DramaInfo($id);
	$nom = $tab_all['titre'];
	$nb_photo = $tab_all['nb_photo'];
	$photo_car = $tab_all['photo_car'];
	
	$nom_photo = str_replace(':','', $nom);
	
	if($photo_car == 1){
	$img = "content/dramas/".$nom_photo."_Big.jpg";
	$img = str_replace(' ', '%20' ,$img);
	}else{
	$img = "content/dramas/".$nom_photo."_Big".$photo_car.".jpg";
	$img = str_replace(' ', '%20' ,$img);
	
	}
		
	$name_tiret = dramaLinkClean($nom);
	
	$synopsis = $tab_all['synopsis'];
	$chaine = $synopsis;
	
	$lg_max = 250; //nombre de caractère autoriser

	if (strlen($chaine) > $lg_max){
		$chaine = substr($chaine, 0, $lg_max);
		$last_space = strrpos($chaine, " ");
		$chaine = substr($chaine, 0, $last_space)."...";
	}
	$synopsis = $chaine;
	
	
	
	echo "<li>";
					echo'<table width="1000" height="320" border="0" cellpadding="0" cellspacing="0">';
					echo'<tr>';
                    echo'<td width="360" height="320" style="background-image:url('.$http.'images/jquery_gauche.png); background-repeat:repeat-x;" valign="top" >';
                        echo'<table width="360">';
                            echo'<tr>';
                                echo'<td width="20"></td>';
								echo'<td valign="top">';
                        echo'<br />';
                        
                        echo'<h1 class="menu_blanc">'.$nom.'</h1>';
                        
                        
                        echo'<h2 class="rose">'.$tab_all['nb_epi'].' épisodes</h2>';
                        
                        echo'<p class="blanc" align="justify">'.$synopsis.'</p><br />';
                        
                        echo'<p class="blanc" align="justify" valign="top">';
						AffGenreDrama($id);
						echo '</p>';
                        echo'<br />';
                        echo'<p><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="Regarder '.$nom.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
                                echo'</td>';
                                echo'<td width="20"></td>';
                            echo'</tr>';
                        echo'</table>';                            
                    echo'</td>';
                    echo'<td><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$img.'" width="640" height="320" alt="série coréenne &quot;'.$nom.'&quot; en vostfr" title="drama coréen &quot;'.$nom.'&quot; en vostfr" border="0"></a></td>';
					echo'</tr>';
					echo'</table>';
	echo'</li>';
	
	

	}else{
		if($id > 0){

	if($id == 2){
	echo '<li style="width:1000px;height:320px;">';
		echo '<a href="'.$http.'http://www.dramapassion.com/concours/"><img src="'.$http.'images/slider7.jpg" /></a>';
	echo '</li>';
	}elseif($id == 1){
	echo '<li style="width:1000px;height:320px;">';
		echo '<a href="'.$http.'http://www.dramapassion.com/store/"><img src="'.$http.'images/slider9.jpg" /></a>';
	echo '</li>';
	}
	
	
		}else{
	$sql_car2 = mysql_query("SELECT * FROM t_dp_carrousel WHERE CarrID = ".$id_carr."");
	$titre = mysql_result($sql_car2,0,"CarrTitre");
	$texte = mysql_result($sql_car2,0,"CarrTexte");
	$bt = mysql_result($sql_car2,0,"CarrBt");
	$photo = mysql_result($sql_car2,0,"CarrPhoto");
	$lien = mysql_result($sql_car2,0,"CarrLien");
	/*
	echo "<li>";
					echo'<table width="1000" height="320" border="0" cellpadding="0" cellspacing="0">';
					echo'<tr>';
                    echo'<td width="360" height="320" style="background-image:url('.$http.'images/jquery_gauche.png); background-repeat:repeat-x;" valign="top" >';
                        echo'<table width="360">';
                            echo'<tr>';
                                echo'<td width="20"></td>';
								echo'<td valign="top">';
                        echo'<br />';
                        
                        echo'<h1 class="menu_blanc">'.$titre.'</h1>';
                        
                        echo'<p class="blanc" align="justify">'.$texte.'</p>';
                        
                        if($bt == 'yes'){
						 echo'<br />';
                        echo'<p><a href="'.$lien.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="'.$titre.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
                        }elseif($bt == 2){
	                        echo'<br />';
                        echo'<p><a href="'.$lien.'"><img src="'.$http.'images/preco.png" width="118" height="31" border="0" title="'.$titre.'"></a></p>';
                        }                
								
								echo'</td>';
                                echo'<td width="20"></td>';
                            echo'</tr>';
                        echo'</table>';                            
                    echo'</td>';
                    echo'<td><a href="'.$lien.'"><img src="'.$photo.'" width="640" height="320" alt="" border="0"></a></td>';
					echo'</tr>';
					echo'</table>';
	echo'</li>';
	*/
	$detect2 = new Mobile_Detect();
	if($detect2->isTablet()){
	echo '<li>';
		echo '<a href="lien_app.php"><img src="'.$http.'images/slider.jpg" /></a>';
	echo '</li>';
	}else{
	echo '<li>';
		echo '<table id="Tableau_01" width="1000" height="320" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<img src="'.$http.'images/slider_01.jpg" width="1000" height="238" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="'.$http.'images/slider_02.jpg" width="108" height="82" alt=""></td>
		<td>
			<a href="https://itunes.apple.com/fr/app/dramapassion/id641121786?mt=8"><img src="'.$http.'images/slider_03.jpg" width="154" height="56" alt=""></a></td>
		<td>
			<a href="https://play.google.com/store/apps/details?id=com.dramapassion.dramapassion"><img src="'.$http.'images/slider_04.jpg" width="155" height="56" alt=""></a></td>
		<td rowspan="2">
			<img src="'.$http.'images/slider_05.jpg" width="583" height="82" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="'.$http.'images/slider_06.jpg" width="309" height="26" alt=""></td>
	</tr>
</table>';

	echo '</li>';
	}
	}
}
	
	
	
	
}
function Afficher_slider3($id,$id_carr){
	global $tab_trailer;
	global $tab_trailer_name;
	global $tab_trailer_url;
	global $tab_trailer_shortcut;
	global $paques;
	if($id > 2){
	$tab_all = DramaInfo($id);
	$nom = $tab_all['titre'];
	$nb_photo = $tab_all['nb_photo'];
	$photo_car = $tab_all['photo_car'];
	
	$nom_photo = str_replace(':','', $nom);
	
	if($photo_car == 1){
	$img = "content/dramas/".$nom_photo."_Big.jpg";
	$img = str_replace(' ', '%20' ,$img);
	}else{
	$img = "content/dramas/".$nom_photo."_Big".$photo_car.".jpg";
	$img = str_replace(' ', '%20' ,$img);
	
	}
		
	$name_tiret = dramaLinkClean($nom);
	
	$synopsis = $tab_all['synopsis'];
	$chaine = $synopsis;
	
	$lg_max = 250; //nombre de caractère autoriser

	if (strlen($chaine) > $lg_max){
		$chaine = substr($chaine, 0, $lg_max);
		$last_space = strrpos($chaine, " ");
		$chaine = substr($chaine, 0, $last_space)."...";
	}
	$synopsis = $chaine;
	
	
	
	echo "<li>";
					echo'<table width="1000" height="320" border="0" cellpadding="0" cellspacing="0">';
					echo'<tr>';
                    echo'<td width="360" height="320" style="background-image:url('.$http.'images/jquery_gauche.png); background-repeat:repeat-x;" valign="top" >';
                        echo'<table width="360">';
                            echo'<tr>';
                                echo'<td width="20"></td>';
								echo'<td valign="top">';
                        echo'<br />';
                        
                        echo'<h1 class="menu_blanc">'.$nom.'</h1>';
                        
                        
                        echo'<h2 class="rose">'.$tab_all['nb_epi'].' épisodes</h2>';
                        
                        echo'<p class="blanc" align="justify">'.$synopsis.'</p><br />';
                        
                        echo'<p class="blanc" align="justify" valign="top">';
						AffGenreDrama($id);
						echo '</p>';
                        echo'<br />';
                        
                        if($tab_all['trailer'] >= 1){
	                        echo'<p><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="Regarder '.$nom.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a>&nbsp;&nbsp;&nbsp;';
	                        if($paques == 1 && $id_carr == 3){
								$dbQpaques = "SELECT * FROM paques WHERE id = 3";
								$sqlQpaques = mysql_query($dbQpaques);
								$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
								$idPaques = mysql_result($sqlQpaques,0,"name");
								$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
								if(time() > $dateshow){	
									echo '<img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:31px;">';
								}
							}
							echo '&nbsp;&nbsp;&nbsp;<img id="btn_trailer_'.$id.'" data-eta="0" data-id="'.$id.'" src="'.$http.'images/btn_trailer.png" />   </p>';
	                        
	                     }else{
		                     echo'<p><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="Regarder '.$nom.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
	                     }
                        
                                echo'</td>';
                                echo'<td width="20"></td>';
                            echo'</tr>';
                        echo'</table>';                            
                    echo'</td>';
                    if($tab_all['trailer'] == 1){
	                     $name_drama = str_replace('\'', ' ' ,$tab_all['titre']);
	                    $tab_trailer[$id] = $id;
	                    $tab_trailer_name[$id] = $name_drama;
	                    $tab_trailer_shortcut[$id] = "http://www.dramapassion.com/subtitles/".$tab_all['shortcut'].".srt";
						$tab_trailer_url[$id] = '//www.youtube.com/watch?v='.$tab_all['trailerUrl'];
	                   
	                 echo'<td>
                    <div  width="640" height="320" style="display:none;" id="player_trailer_'.$id.'" class="player_slider"></div>
                    <div style="height:320px; overflow:hidden"><a href="drama/'.$id.'/'.$name_tiret.'"><img id="img_drama_'.$id.'"  src="'.$img.'" width="640" height="320" alt="série coréenne &quot;'.$nom.'&quot; en vostfr" title="drama coréen &quot;'.$nom.'&quot; en vostfr" border="0"></a></div></td>';
                     
                     }else if($tab_all['trailer'] == 2){
	                     $name_drama = str_replace('\'', ' ' ,$tab_all['titre']);
	                    $tab_trailer[$id] = $id;
	                    $tab_trailer_name[$id] = $name_drama;
	                    $tab_trailer_shortcut[$id] = "http://www.dramapassion.com/subtitles/".$tab_all['shortcut'].".srt";
						$tab_trailer_url[$id] = $tab_all['trailerUrl'];
	                   
	                 echo'<td>
                    <div  width="640" height="320" style="display:none;" id="player_trailer_'.$id.'" class="player_slider"></div>
                    <div style="height:320px; overflow:hidden"><a href="drama/'.$id.'/'.$name_tiret.'"><img id="img_drama_'.$id.'"  src="'.$img.'" width="640" height="320" alt="série coréenne &quot;'.$nom.'&quot; en vostfr" title="drama coréen &quot;'.$nom.'&quot; en vostfr" border="0"></a></div></td>';
                     
                     }else{
	                   echo'<td>
                   
                    <div style="height:320px; overflow:hidden"><a href="drama/'.$id.'/'.$name_tiret.'"><img id="img_drama_'.$id.'"  src="'.$img.'" width="640" height="320" alt="série coréenne &quot;'.$nom.'&quot; en vostfr" title="drama coréen &quot;'.$nom.'&quot; en vostfr" border="0"></a></div></td>';   
                     }
                    
					echo'</tr>';
					echo'</table>';
	echo'</li>';
	
	
	

	}else{
		if($id > 0){

	if($id == 2){
	echo '<li style="width:1000px;height:320px;">';
		echo '<a href="'.$http.'http://www.dramapassion.com/concours/"><img src="'.$http.'images/slider7.jpg" /></a>';
	echo '</li>';
	}elseif($id == 1){
	echo '<li style="width:1000px;height:320px;">';
		echo '<a href="'.$http.'http://www.dramapassion.com/store/"><img src="'.$http.'images/slider10.jpg" /></a>';
	echo '</li>';
	}
	
	
		}else{
	$sql_car2 = mysql_query("SELECT * FROM t_dp_carrousel WHERE CarrID = ".$id_carr."");
	$titre = mysql_result($sql_car2,0,"CarrTitre");
	$texte = mysql_result($sql_car2,0,"CarrTexte");
	$bt = mysql_result($sql_car2,0,"CarrBt");
	$photo = mysql_result($sql_car2,0,"CarrPhoto");
	$lien = mysql_result($sql_car2,0,"CarrLien");
	/*
	echo "<li>";
					echo'<table width="1000" height="320" border="0" cellpadding="0" cellspacing="0">';
					echo'<tr>';
                    echo'<td width="360" height="320" style="background-image:url('.$http.'images/jquery_gauche.png); background-repeat:repeat-x;" valign="top" >';
                        echo'<table width="360">';
                            echo'<tr>';
                                echo'<td width="20"></td>';
								echo'<td valign="top">';
                        echo'<br />';
                        
                        echo'<h1 class="menu_blanc">'.$titre.'</h1>';
                        
                        echo'<p class="blanc" align="justify">'.$texte.'</p>';
                        
                        if($bt == 'yes'){
						 echo'<br />';
                        echo'<p><a href="'.$lien.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="'.$titre.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
                        }elseif($bt == 2){
	                        echo'<br />';
                        echo'<p><a href="'.$lien.'"><img src="'.$http.'images/preco.png" width="118" height="31" border="0" title="'.$titre.'"></a></p>';
                        }                
								
								echo'</td>';
                                echo'<td width="20"></td>';
                            echo'</tr>';
                        echo'</table>';                            
                    echo'</td>';
                    echo'<td><a href="'.$lien.'"><img src="'.$photo.'" width="640" height="320" alt="" border="0"></a></td>';
					echo'</tr>';
					echo'</table>';
	echo'</li>';
	*/
	$detect2 = new Mobile_Detect();
	if($detect2->isTablet()){
	echo '<li>';
		echo '<a href="lien_app.php"><img src="'.$http.'images/slider.jpg" /></a>';
	echo '</li>';
	}else{
	echo '<li>';
		echo '<table id="Tableau_01" width="1000" height="320" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<img src="'.$http.'images/slider_01.jpg" width="1000" height="238" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="'.$http.'images/slider_02.jpg" width="108" height="82" alt=""></td>
		<td>
			<a href="https://itunes.apple.com/fr/app/dramapassion/id641121786?mt=8"><img src="'.$http.'images/slider_03.jpg" width="154" height="56" alt=""></a></td>
		<td>
			<a href="https://play.google.com/store/apps/details?id=com.dramapassion.dramapassion"><img src="'.$http.'images/slider_04.jpg" width="155" height="56" alt=""></a></td>
		<td rowspan="2">
			<img src="'.$http.'images/slider_05.jpg" width="583" height="82" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="'.$http.'images/slider_06.jpg" width="309" height="26" alt=""></td>
	</tr>
</table>';

	echo '</li>';
	}
	}
}
	
		
	
}
function Afficher_sliderNew($id,$id_carr){
	$tab_all = DramaInfo($id);
	$nom = $tab_all['titre'];
	$nb_photo = $tab_all['nb_photo'];
	$photo_car = $tab_all['photo_car'];
	
	$nom_photo = str_replace(':','', $nom);
	
	if($photo_car == 1){
	$img = "content/dramas/".$nom_photo."_Big.jpg";
	$img = str_replace(' ', '%20' ,$img);
	}else{
	$img = "content/dramas/".$nom_photo."_Big".$photo_car.".jpg";
	$img = str_replace(' ', '%20' ,$img);
	
	}
		
	$name_tiret = dramaLinkClean($nom);
	
	$synopsis = $tab_all['synopsis'];
	$chaine = $synopsis;
	
	$lg_max = 200; //nombre de caractère autoriser

	if (strlen($chaine) > $lg_max){
		$chaine = substr($chaine, 0, $lg_max);
		$last_space = strrpos($chaine, " ");
		$chaine = substr($chaine, 0, $last_space)."...";
	}
	$synopsis = $chaine;
	$out = "";
	
	$out .= '<div style="height:auto;position: relative; ">';
	$out .= '<div class="hidden-xs" style="width:32%;float:left;background-color: black;height:100%;position:absolute;padding:20px; ">';
	$out .= '<h1 class="menu_blanc">'.$nom.'</h1>';
	$out .= '<h2 class="rose">'.$tab_all['nb_epi'].' épisodes</h2>';
	$out .= '<p class="blanc hidden-sm hidden-xs" align="justify">'.$synopsis.'</p>';
	$out .= '<p class="blanc hidden-sm hidden-xs" align="justify" valign="top">';
	$out .= AffGenreDrama2($id);
	$out .= '</p>';
    $out .= '<br />';
    $out .= '<p style="text-align:right;"><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="Regarder '.$nom.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
	$out .= '</div>';
	
	$out .= '<div class="img_slider" style="float:right;"><a href="drama/'.$id.'/'.$name_tiret.'"><img id="img_drama_'.$id.'"  src="'.$img.'" width="100%"  alt="série coréenne &quot;'.$nom.'&quot; en vostfr" title="drama coréen &quot;'.$nom.'&quot; en vostfr" border="0"></a></div>'; 
	
	$out .= '<div style="clear: both;"></div>';
	
	$out .= '</div>';
	
	/*
	$out .= '<table width="1000" height="320" border="0" cellpadding="0" cellspacing="0">';
	$out .= '<tr>';
    $out .= '<td width="360" height="320" style="background-image:url('.$http.'images/jquery_gauche.png); background-repeat:repeat-x;" valign="top" >';
    $out .= '<table width="360">';
    $out .= '<tr>';
    $out .= '<td width="20"></td>';
	$out .= '<td valign="top">';
    $out .= '<br />';
                        
    $out .= '<h1 class="menu_blanc">'.$nom.'</h1>';
                        
                        
    $out .= '<h2 class="rose">'.$tab_all['nb_epi'].' épisodes</h2>';
                        
    $out .= '<p class="blanc" align="justify">'.$synopsis.'</p><br />';
                        
    $out .= '<p class="blanc" align="justify" valign="top">';
	$out .= AffGenreDrama2($id);
	$out .= '</p>';
    $out .= '<br />';

	$out .= '<p><a href="drama/'.$id.'/'.$name_tiret.'"><img src="'.$http.'images/regarder.png" width="118" height="31" border="0" title="Regarder '.$nom.'" onMouseOver="this.src=\''.$http.'images/regarder_up.png\'" onMouseOut="this.src=\''.$http.'images/regarder.png\'"></a></p>';
	                     
                        
    $out .= '</td>';
    $out .= '<td width="20"></td>';
    $out .= '</tr>';
    $out .= '</table>';                            
    $out .= '</td>';

	$out .= '<td>
                   
                    <div style="height:320px; overflow:hidden"><a href="drama/'.$id.'/'.$name_tiret.'"><img id="img_drama_'.$id.'"  src="'.$img.'" width="640" height="320" alt="série coréenne &quot;'.$nom.'&quot; en vostfr" title="drama coréen &quot;'.$nom.'&quot; en vostfr" border="0"></a></div></td>';   
                     
                    
	$out .= '</tr>';
	$out .= '</table>';
	*/
	return $out;
	
	
}	

function Afficher_sliderPub(){
	
	/*
	$rand = rand(1, 2);

	if($rand == 1){
		echo '<li style="width:1000px;height:320px;">';
		echo '<a href="http://bit.ly/1CPydsX" target="_blank"><img src="'.$http.'images/gong2.jpg" /></a>';
		echo '</li>';
	}else{
		echo '<li style="width:1000px;height:320px;">';
		echo '<a href="http://apple.co/1RPAkU1" target="_blank"><img src="'.$http.'images/gong1.jpg" /></a>';
		echo '</li>';
	}	
	
	echo '<li style="width:1000px;height:320px;">';
	echo '<a href="http://play.google.com/store/apps/details?id=com.dramapassion.dramapassion" target="_blank"><img src="'.$http.'images/carousel_newandroid.jpg" /></a>';
	echo '</li>';
	*/
	/* PUB ABO
	echo '<li style="width:1000px;height:320px;">';
	echo '<div style="width: 1000px;height: 320px">
	<div style="padding: 0px !important; margin: 0px !important; height: 262px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Slider/carousel_abo_01.jpg" /></div>
	<div style="padding: 0px !important; margin: 0px !important height: 58px !important;overflow: hidden;">
		<div style="float: left;height: 58px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Slider/carousel_abo_02.jpg" /></div>
		<div style="float: left;height: 58px !important;overflow: hidden;"><a href="http://itunes.com/apps/Dramapassion" target="_blank" style="text-decoration: none !important"><img src="http://www.dramapassion.com/images/Slider/carousel_abo_03.jpg" /></a></div>
		<div style="float: left;height: 58px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Slider/carousel_abo_04.jpg" /></div>
		<div style="float: left;height: 58px !important;overflow: hidden;"><a href="http://play.google.com/store/apps/details?id=com.dramapassion.dramapassion" target="_blank" style="text-decoration: none !important"><img src="http://www.dramapassion.com/images/Slider/carousel_abo_05.jpg" /></a></div>
		<div style="float: left;height: 58px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Slider/carousel_abo_06.jpg" /></div>
		<div style="clear: both;"></div>
	</div>
</div>';
*/
	/*
	echo '<li style="width:1000px;height:320px;">';
	echo '<a href="http://www.dramapassion.com/store/#linkTel"><img src="'.$http.'images/Slider/carousel_mai2016v2.jpg" /></a>';
	echo '</li>';	
	*/
	echo '<li style="width:1000px;height:320px;">';
	echo '<a href="http://www.animasia.org/" target="_blank"><img src="'.$http.'images/Slider/animazia.png" /></a>';
	echo '</li>';	
	
	
}
function Afficher_sliderPub2(){
	
	
	echo '<li style="width:1000px;height:320px;">';
	echo '<a href="https://www.facebook.com/dramapassion/" target="_blank"><img src="'.$http.'images/Slider/carousel_sept2016-1.jpg" /></a>';
	echo '</li>';	
	
	
}
function Afficher_sliderPubGong(){

	$tabLink = array(
			0 => "http://bit.ly/20GHyuE",
			1 => "http://bit.ly/1PS35J2",
			2 => "http://bit.ly/1SGQgGH",
			3 => "http://bit.ly/1mdjY9o"
		
	);
	$random = rand(0,3);
	echo '<li style="width:1000px;height:320px;">';
	echo '<a href="'.$tabLink[$random].'" target="_blank"><img src="'.$http.'images/Slider/'.$random.'.jpg" /></a>';
	echo '</li>';
	
	/*
	echo '<li style="width:1000px;height:320px;">';
	echo '<a href="http://www.dramapassion.com/parrainage/explication" target="_blank"><img src="'.$http.'images/Slider/carousel_parrainage.jpg" /></a>';
	echo '</li>';	
	*/
	
	
}
function Afficher_pub_paques(){
	$timeNow = time();
	$t1 = 1459893600;
	if($timeNow < $t1){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="'.$http.'paquesExplication/"><img src="'.$http.'images/paques/slider_paques2016.jpg" /></a>';
			echo '</li>';
		}
}
function Afficher_pub_hemmo(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.gongnetworks.com/concours-concours-hemogloween-avec-dramapassion-104.php" target="_blank"><img src="'.$http.'images/Slider/carousel_oct2016.png" /></a>';
			echo '</li>';
}
function Afficher_pub_famille(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/catalogue/?pub=famille" target="_blank"><img src="'.$http.'images/Slider/carousel_thematique.png" /></a>';
			echo '</li>';
}
function Afficher_pub_halo(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/catalogue/?pub=hemo" target="_blank"><img src="'.$http.'images/Slider/sliderzoomhalloween2016.png" /></a>';
			echo '</li>';
}
function Afficher_pub_webtoon(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/catalogue/?pub=webtoon" target="_blank"><img src="'.$http.'images/Slider/sliderzoomwebtoons2016.png" /></a>';
			echo '</li>';
}
function Afficher_pub_mama(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/mama2016" target="_blank"><img src="'.$http.'images/Slider/carousel_mama2016_direct.jpg" /></a>';
			echo '</li>';
}
function Afficher_pub_pixel(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="https://www.facebook.com/dramapassion" target="_blank"><img src="'.$http.'images/Slider/carousel_dec2016.png" /></a>';
			echo '</li>';
}

function Afficher_pub_seo(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/catalogue/?pub=SeoInGuk" target="_blank"><img src="'.$http.'images/Slider/carousel_thematique.jpg" /></a>';
			echo '</li>';
}
function Afficher_pub_overwatch(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.gongnetworks.com/programme-apex-league-overwatch-220.php" target="_blank"><img src="'.$http.'images/Slider/carousel_overwatch1.jpg" /></a>';
			echo '</li>';
}
function Afficher_pub_gonghyojin(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/catalogue/?pub=gonghyojin" target="_blank"><img src="'.$http.'images/Slider/carousel_gonghyojin.png" /></a>';
			echo '</li>';
}
function Afficher_pub_opefev2017(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/opefev2017" target="_blank"><img src="'.$http.'images/Slider/carousel_opefev2017.png" /></a>';
			echo '</li>';
}
function Afficher_pub_badboy(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.dramapassion.com/catalogue/?pub=meetthebadboys" target="_blank"><img src="'.$http.'images/Slider/carousel_badboy.png" /></a>';
			echo '</li>';
}
function Afficher_pub_fanawards2017(){
			echo '<li style="width:1000px;height:320px;">';
			echo '<a href="http://www.gongnetworks.com/fanawards/" target="_blank"><img src="'.$http.'images/Slider/carousel_fanawards2017.png" /></a>';
			echo '</li>';
}
function Car_aff(){
	$sql_car = mysql_query("SELECT * FROM t_dp_carrousel");
	$nb_drama_car = mysql_num_rows($sql_car);
	
	for($i=0; $i < $nb_drama_car ; $i++){
		$id_drama = mysql_result($sql_car,($i),"CarrDramaID");
		$id_carr = mysql_result($sql_car,($i),"CarrID");
		Afficher_slider($id_drama,$id_carr);
	
	}

}
function Car_aff2(){
	$sql_car = mysql_query("SELECT * FROM t_dp_carrousel");
	$nb_drama_car = mysql_num_rows($sql_car);
	for($i=0; $i < $nb_drama_car ; $i++){
		$id_drama = mysql_result($sql_car,($i),"CarrDramaID");
		$id_carr = mysql_result($sql_car,($i),"CarrID");
		Afficher_slider2($id_drama,$id_carr);
	
	}

}
function Car_aff3(){
	$sql_car = mysql_query("SELECT * FROM t_dp_carrousel");
	$nb_drama_car = mysql_num_rows($sql_car);
	for($i=0; $i < $nb_drama_car ; $i++){
		$id_drama = mysql_result($sql_car,($i),"CarrDramaID");
		$id_carr = mysql_result($sql_car,($i),"CarrID");
		if($i == 0){
			//Afficher_pub_mama();
			//Afficher_sliderPub();
			//Afficher_sliderPub2();
			//Afficher_pub_paques();
			//Afficher_pub_pixel();
			//if(time() <= 1477954799){
			//Afficher_pub_halo();
			//Afficher_pub_hemmo();
			//Afficher_pub_gonghyojin();
			//Afficher_pub_opefev2017();
			Afficher_pub_fanawards2017();
			//}
			
			Afficher_slider3($id_drama,$id_carr);
		}else if($i == 1){
			//Afficher_sliderPub();
			//Afficher_pub_badboy();
			
			Afficher_slider3($id_drama,$id_carr);
		}else if($i == 2){
			//Afficher_pub_famille();
			Afficher_pub_badboy();
			//Afficher_pub_gonghyojin();
			//Afficher_pub_seo();
			Afficher_slider3($id_drama,$id_carr);
		}else if($i == 3){
			
			Afficher_sliderPubGong();
			Afficher_slider3($id_drama,$id_carr);
		}else if($i == 4){
			
			Afficher_slider3($id_drama,$id_carr);
		
		}else if($i == 5){
			
			Afficher_slider3($id_drama,$id_carr);
			Afficher_pub_webtoon();
			//Afficher_pub_overwatch();
		}else{
			Afficher_slider3($id_drama,$id_carr);
		}
		
	
	}

}

function Car_aff_bt(){
	$sql_car = mysql_query("SELECT * FROM t_dp_carrousel_bt ORDER BY CarrID");
	$nb_drama_car = mysql_num_rows($sql_car);
	for($i=0; $i < $nb_drama_car ; $i++){
		$id_drama = mysql_result($sql_car,($i),"CarrDramaID");
		$id_carr = mysql_result($sql_car,($i),"CarrID");
		$type = mysql_result($sql_car,($i),"CarrType");
		if($type == 1){
			$out[$i]  = Afficher_sliderNew($id_drama,$id_carr);
		}else{
			$link = mysql_result($sql_car,($i),"CarrLink");
			$img = mysql_result($sql_car,($i),"CarrImg");
			$out[$i]  = '<div style="width:100%;"><a href="'.$link.'" target="_blank"><img style="width:100%;" src="'.$http.'images/Slider/'.$img.'" /></a></div>';
		}
		
	
	}
	return $out;

}

function Champ_recherche($type){
	global $http;
	if($type == "cate"){
		$tab[0] = "Comédie / Comédie Romantique";
		$tab[1] = "Drame";
		$tab[2] = "Historique";
		$tab[3] = "Action / Thriller / Fantastique";
		
		$tab_val[0]= "com";
		$tab_val[1]= "drame";
		$tab_val[2]= "hist";
		$tab_val[3]= "act";
		
	}
	if($type == "annee"){
		$nb=0;
		$sql_annee = mysql_query("SELECT * FROM t_dp_drama");
		while($sql_row_annee=mysql_fetch_array($sql_annee)){
			$tab[$nb] = $sql_row_annee['DramaYear'];
			$nb++;
		}
		$tab = array_unique ($tab);
		
		rsort($tab);
		$tab_val = $tab;
	}
	if($type == "chaine"){
		$nb=0;
		$sql_chaine = mysql_query("SELECT * FROM t_dp_producer ORDER BY ProducerName");
		while($sql_row_chaine=mysql_fetch_array($sql_chaine)){
			$tab[$nb] = $sql_row_chaine['ProducerName'];
			$tab_val[$nb] = $sql_row_chaine['ProducerID'];
			$nb++;
		}
	}
	if($type == "genre"){
		$nb=0;
		$sql_genre = mysql_query("SELECT * FROM t_dp_genrevideo ORDER BY GenreVideoDescFre");
		while($sql_row_genre=mysql_fetch_array($sql_genre)){
			$tab[$nb]  = $sql_row_genre['GenreVideoDescFre'];
			$tab_val[$nb] = $sql_row_genre['GenreVideoID'];
			$nb++;
		}
	}
	if($type == "acteur"){
		$nb=0;
		$sql_acteur = mysql_query("SELECT * FROM t_dp_actor ORDER BY ActorName");
		while($sql_row_acteur=mysql_fetch_array($sql_acteur)){
			$tab[$nb] = $sql_row_acteur['ActorName'];
			$tab_val[$nb] = $sql_row_acteur['ActorID'];
			$nb++;
		}
	}
			
		
	$nb_tab = count($tab);
	echo'<div class="all_input" onclick="showHideSelect(\'selectID_'.$type.'\')" >';
    echo'<div class="inputsSelect"  >';
	$type_lien = "lien_".$type;
       echo'<p class="selects"  id="select_'.$type.'" >Tout</p>';
	   
       echo'<ul id="selectID_'.$type.'"  >';
			echo'<li><a href="javascript:void(0)" id="'.$type.'_Tout"  onclick="validAndHide(\'Tout\', this, \''.$type_lien.'\', \'select_'.$type.'\')">Tout</a></li>';
	   foreach($tab as $key => $value){
           echo'<li><a href="javascript:void(0)" id="'.$type.'_'.$tab_val[$key].'" onclick="validAndHide(\''.$tab_val[$key].'\', this, \''.$type_lien.'\', \'select_'.$type.'\')">'.$value.'</a></li>';
		}
		
		echo'</ul>';
		
	echo'</div><img src="'.$http.'images/submit_liste.png" style="cursor:pointer;">';
    echo'<input type="hidden" name="'.$type_lien.'" id="'.$type_lien.'" />';
	echo'</div>';

}
function trie_tableau_desc($array, $key)
        {
			
           foreach($array as $cle => $valeur) {
					$req_verif = "SELECT * FROM t_dp_drama WHERE DramaID =".$cle;
					$sql_verif = mysql_query($req_verif);
					$verif = mysql_result($sql_verif,0,'StatusID');
					
					if($verif == 1){
						$sort_values[$cle] = $array[$cle][$key];
					}
           }
           arsort ($sort_values);
           reset ($sort_values);
		   
           while (list ($arr_key, $arr_val) = each ($sort_values)) {
                         $sorted_arr[] = $array[$arr_key];
           }
           return $sorted_arr;
}
function recent_drama(){

	$req_sql = "SELECT max(ReleaseDatePre),DramaID FROM t_dp_episode WHERE ReleaseDatePre < NOW() GROUP BY DramaID";
		
	
	$sql_new_epi = mysql_query($req_sql);
	
	

		while($sql_row=mysql_fetch_array($sql_new_epi)){
			$tab_all[$sql_row['DramaID']] = array("dramaID" =>$sql_row['DramaID'], "date" => $sql_row["max(ReleaseDatePre)"]);
			
		}
	
	
	$tab_ord= trie_tableau_desc($tab_all,"date");
	
	
	
	for($j=0; $j<8 ; $j++){
		$action = "SELECT * FROM t_dp_episode WHERE (ReleaseDatePre = '".$tab_ord[$j]["date"]."' AND DramaID = ".$tab_ord[$j]["dramaID"].")";
		
		$sql_epi_ord = mysql_query($action);
		
		
		
		$nb = 0;
		$tab_inter = array();
		while($sql_row2=mysql_fetch_array($sql_epi_ord)){
			$tab_inter[$nb] = $sql_row2['EpisodeNumber'];
			$nb++;
		}
		sort($tab_inter);
		
		
		$tab_tri_final[$tab_ord[$j]["dramaID"]] = $tab_inter;
		$tab_date_sortie[$tab_ord[$j]["dramaID"]] = $tab_ord[$j]["date"];
		
		
		
		
		 
	}
	
	$nb_tab_sortie = 0;
	foreach($tab_tri_final as $key => $value){
		$nb_elem = count($tab_tri_final[$key]);
		$min = $tab_tri_final[$key][0];
		$max_temp = $nb_elem-1;
		$max = $tab_tri_final[$key][$max_temp];
		$date = $tab_date_sortie[$key];
		
		$char = '<span class="new_epi align_logo_titre">Episodes '.$min.' - '.$max.'</span>';
		
		$action_drama = "SELECT * FROM t_dp_drama WHERE DramaID = '".$key."'";
		$sql_nom_drama = mysql_query($action_drama);
		$nom_drama = mysql_result($sql_nom_drama,0,"DramaTitle");
		
		$nom_drama_def = substr($nom_drama,0,27);
		if(strlen($nom_drama_def) >= 27){
			$nom_drama_def.= "...";	
		}
		
		$drama_link = str_replace(' ', '-' ,$nom_drama);
		$drama_link = str_replace('\'','',$drama_link);
		$drama_link = str_replace(',','',$drama_link);
		$drama_link = str_replace('!','',$drama_link);
		$drama_link = str_replace('.','',$drama_link);
		$drama_link = str_replace('à','a',$drama_link);
		$drama_link = str_replace(':','-',$drama_link);

		
		
		$nom_drama = str_replace(':','',$nom_drama);
		
		$img_drama = "content/dramas/".$nom_drama."_Big.jpg";
	
			
	
		$tab_sortie[$nb_tab_sortie] = array("drama" => $nom_drama_def ,"img" => $img_drama, "char" => $char, "dramaID" => $key, "link" => $drama_link,"date" => $date);
		$nb_tab_sortie++;
	
	}
	return $tab_sortie;
	

}

function recent_aff_pay(){

	$req_sql = "SELECT max(ReleaseDatePre),DramaID FROM t_dp_episode WHERE ReleaseDatePre < NOW() GROUP BY DramaID";
		
	
	$sql_new_epi = mysql_query($req_sql);
	
	

		while($sql_row=mysql_fetch_array($sql_new_epi)){
			$tab_all[$sql_row['DramaID']] = array("dramaID" =>$sql_row['DramaID'], "date" => $sql_row["max(ReleaseDatePre)"]);
			
		}
	
	
	$tab_ord= trie_tableau_desc($tab_all,"date");
	
	
	
	for($j=0; $j<10 ; $j++){
		$action = "SELECT * FROM t_dp_episode WHERE (ReleaseDatePre = '".$tab_ord[$j]["date"]."' AND DramaID = ".$tab_ord[$j]["dramaID"].")";
		
		$sql_epi_ord = mysql_query($action);
		
		
		
		$nb = 0;
		$tab_inter = array();
		while($sql_row2=mysql_fetch_array($sql_epi_ord)){
			$tab_inter[$nb] = $sql_row2['EpisodeNumber'];
			$nb++;
		}
		sort($tab_inter);
		
		
		$tab_tri_final[$tab_ord[$j]["dramaID"]] = $tab_inter;
		$tab_date_sortie[$tab_ord[$j]["dramaID"]] = $tab_ord[$j]["date"];
		
		
		
		
		 
	}
	
	$nb_tab_sortie = 0;
	foreach($tab_tri_final as $key => $value){
		$nb_elem = count($tab_tri_final[$key]);
		$min = $tab_tri_final[$key][0];
		$max_temp = $nb_elem-1;
		$max = $tab_tri_final[$key][$max_temp];
		$date = $tab_date_sortie[$key];
		
		$char = '<span class="new_epi align_logo_titre">Episodes '.$min.' - '.$max.'</span>';
		
		$action_drama = "SELECT * FROM t_dp_drama WHERE DramaID = '".$key."'";
		$sql_nom_drama = mysql_query($action_drama);
		$nom_drama = mysql_result($sql_nom_drama,0,"DramaTitle");
		
		$nom_drama_def = substr($nom_drama,0,27);
		if(strlen($nom_drama_def) >= 27){
			$nom_drama_def.= "...";	
		}
		
		$drama_link = str_replace(' ', '-' ,$nom_drama);
		$drama_link = str_replace('\'','',$drama_link);
		$drama_link = str_replace(',','',$drama_link);
		$drama_link = str_replace('!','',$drama_link);
		$drama_link = str_replace('.','',$drama_link);
		$drama_link = str_replace('à','a',$drama_link);
		$drama_link = str_replace(':','-',$drama_link);

		
		
		$nom_drama = str_replace(':','',$nom_drama);
		
		$img_drama = "content/dramas/".$nom_drama."_Big.jpg";
	
			
	
		$tab_sortie[$nb_tab_sortie] = array("drama" => $nom_drama_def ,"img" => $img_drama, "char" => $char, "dramaID" => $key, "link" => $drama_link,"date" => $date);
		$nb_tab_sortie++;
	
	}
	return $tab_sortie;
	

}

function recent_aff_free(){

	$req_sql = "SELECT max(ReleaseDateFree),DramaID FROM t_dp_episode WHERE ReleaseDateFree < NOW() GROUP BY DramaID";
		
	
	$sql_new_epi = mysql_query($req_sql);
	
	

		while($sql_row=mysql_fetch_array($sql_new_epi)){
			$tab_all[$sql_row['DramaID']] = array("dramaID" =>$sql_row['DramaID'], "date" => $sql_row["max(ReleaseDateFree)"]);
			
		}
	
	
	$tab_ord= trie_tableau_desc($tab_all,"date");
	
	
	
	for($j=0; $j<10 ; $j++){
		$action = "SELECT * FROM t_dp_episode WHERE (ReleaseDateFree = '".$tab_ord[$j]["date"]."' AND DramaID = ".$tab_ord[$j]["dramaID"].")";
		
		$sql_epi_ord = mysql_query($action);
		
		
		
		$nb = 0;
		$tab_inter = array();
		while($sql_row2=mysql_fetch_array($sql_epi_ord)){
			$tab_inter[$nb] = $sql_row2['EpisodeNumber'];
			$nb++;
		}
		sort($tab_inter);
		
		
		$tab_tri_final[$tab_ord[$j]["dramaID"]] = $tab_inter;
		$tab_date_sortie[$tab_ord[$j]["dramaID"]] = $tab_ord[$j]["date"];
		
		
		
		
		 
	}
	
	$nb_tab_sortie = 0;
	foreach($tab_tri_final as $key => $value){
		$nb_elem = count($tab_tri_final[$key]);
		$min = $tab_tri_final[$key][0];
		$max_temp = $nb_elem-1;
		$max = $tab_tri_final[$key][$max_temp];
		$date = $tab_date_sortie[$key];
		
		$char = '<span class="new_epi align_logo_titre">Episodes '.$min.' - '.$max.'</span>';
		
		$action_drama = "SELECT * FROM t_dp_drama WHERE DramaID = '".$key."'";
		$sql_nom_drama = mysql_query($action_drama);
		$nom_drama = mysql_result($sql_nom_drama,0,"DramaTitle");
		
		$nom_drama_def = substr($nom_drama,0,27);
		if(strlen($nom_drama_def) >= 27){
			$nom_drama_def.= "...";	
		}
		
		$drama_link = str_replace(' ', '-' ,$nom_drama);
		$drama_link = str_replace('\'','',$drama_link);
		$drama_link = str_replace(',','',$drama_link);
		$drama_link = str_replace('!','',$drama_link);
		$drama_link = str_replace('.','',$drama_link);
		$drama_link = str_replace('à','a',$drama_link);
		$drama_link = str_replace(':','-',$drama_link);

		
		
		$nom_drama = str_replace(':','',$nom_drama);
		
		$img_drama = "content/dramas/".$nom_drama."_Big.jpg";
	
			
	
		$tab_sortie[$nb_tab_sortie] = array("drama" => $nom_drama_def ,"img" => $img_drama, "char" => $char, "dramaID" => $key, "link" => $drama_link,"date" => $date);
		$nb_tab_sortie++;
	
	}
	return $tab_sortie;
	

}

function New_epi_drama($type){
	if($type == "Pay"){
		$req_sql = "SELECT max(ReleaseDatePre),DramaID FROM t_dp_episode WHERE ReleaseDatePre < NOW() GROUP BY DramaID";
		
	}elseif($type == "Free"){
		$req_sql = "SELECT max(ReleaseDateFree),DramaID FROM t_dp_episode WHERE ReleaseDateFree < NOW() GROUP BY DramaID ";
	}
	$sql_new_epi = mysql_query($req_sql);
	
	
	if($type == "Free"){
		while($sql_row=mysql_fetch_array($sql_new_epi)){
			$tab_all[$sql_row['DramaID']] = array("dramaID" =>$sql_row['DramaID'], "date" => $sql_row["max(ReleaseDateFree)"]);
		}
	}elseif($type == "Pay"){
		while($sql_row=mysql_fetch_array($sql_new_epi)){
			$tab_all[$sql_row['DramaID']] = array("dramaID" =>$sql_row['DramaID'], "date" => $sql_row["max(ReleaseDatePre)"]);
			
		}
	
	}
	$tab_ord= trie_tableau_desc($tab_all,"date");
	
	
	
	for($j=0; $j<3 ; $j++){
		if($type == "Pay"){
			$action = "SELECT * FROM t_dp_episode WHERE (ReleaseDatePre = '".$tab_ord[$j]["date"]."' AND DramaID = ".$tab_ord[$j]["dramaID"].")";
		}elseif($type == "Free"){
			$action = "SELECT * FROM t_dp_episode WHERE (ReleaseDateFree = '".$tab_ord[$j]["date"]."' AND DramaID = ".$tab_ord[$j]["dramaID"].")";
		}
		$sql_epi_ord = mysql_query($action);
		
		
		
		$nb = 0;
		$tab_inter = array();
		while($sql_row2=mysql_fetch_array($sql_epi_ord)){
			$tab_inter[$nb] = $sql_row2['EpisodeNumber'];
			$nb++;
		}
		sort($tab_inter);
		
		
		$tab_tri_final[$tab_ord[$j]["dramaID"]] = $tab_inter;
		$tab_date_sortie[$tab_ord[$j]["dramaID"]] = $tab_ord[$j]["date"];
		
		
		
		
		 
	}
	
	$nb_tab_sortie = 0;
	foreach($tab_tri_final as $key => $value){
		$nb_elem = count($tab_tri_final[$key]);
		$min = $tab_tri_final[$key][0];
		$max_temp = $nb_elem-1;
		$max = $tab_tri_final[$key][$max_temp];
		$date = $tab_date_sortie[$key];
		
		$char = '<span class="new_epi align_logo_titre">Episodes '.$min.' - '.$max.'</span>';
		
		$action_drama = "SELECT * FROM t_dp_drama WHERE DramaID = '".$key."'";
		$sql_nom_drama = mysql_query($action_drama);
		$nom_drama = mysql_result($sql_nom_drama,0,"DramaTitle");
		
		$nom_drama_def = substr($nom_drama,0,27);
		if(strlen($nom_drama_def) >= 27){
			$nom_drama_def.= "...";	
		}
		
		$drama_link = str_replace(' ', '-' ,$nom_drama);
		$drama_link = str_replace('\'','',$drama_link);
		$drama_link = str_replace(',','',$drama_link);
		$drama_link = str_replace('!','',$drama_link);
		$drama_link = str_replace('.','',$drama_link);
		$drama_link = str_replace('à','a',$drama_link);
		$drama_link = str_replace(':','-',$drama_link);

		
		
		$nom_drama = str_replace(':','',$nom_drama);
		
		if($nb_tab_sortie == 0){
			$img_drama = "content/dramas/".$nom_drama."_Detail.jpg";
			$img_drama = str_replace(' ', '%20' ,$img_drama);
			$img_drama = str_replace('\'', '' ,$img_drama);
			
		}else{
			$img_drama = "content/dramas/".$nom_drama."_Thumb.jpg";
			$img_drama = str_replace(' ', '%20' ,$img_drama);
			$img_drama = str_replace('\'', '' ,$img_drama);
			
		}
			
	
		$tab_sortie[$nb_tab_sortie] = array("drama" => $nom_drama_def ,"img" => $img_drama, "char" => $char, "dramaID" => $key, "link" => $drama_link,"date" => $date);
		$nb_tab_sortie++;
	
	}
	return $tab_sortie;
	

}
  function trie_tableau($array, $key)
        {
           for ($i = 0; $i < sizeof($array); $i++) {
                   $sort_values[$i] = $array[$i][$key];
           }
           asort ($sort_values);
           reset ($sort_values);
           while (list ($arr_key, $arr_val) = each ($sort_values)) {
                         $sorted_arr[] = $array[$arr_key];
           }
           return $sorted_arr;
        }
function CataRecherche($cate,$annee,$chaine,$mot,$genre,$acteur,$tri){
	$tab_recherche_genre = array();
	$tab_recherche_annee = array();
	$tab_recherche_chaine = array();
	$tab_recherche_actor = array();
	$tab_recherche_mot = array ();
	$tab_recherche_acteur = array();
	

	$req_drama_all = "SELECT * FROM t_dp_drama WHERE StatusID = 1 AND DramaID != 110";
	$sql_drama_all = mysql_query($req_drama_all);
	
	while($row_drama_all=mysql_fetch_array($sql_drama_all)){
		$tab_inter[$row_drama_all['DramaID']] = "on";
	}

	if($cate != "" && $cate != "Tout"){
		$req_cate = "SELECT * FROM t_dp_drama WHERE (Categorie='".$cate."' AND StatusID = 1 AND DramaID != 110)";
		$sql_cate  = mysql_query($req_cate);
		
		while($sql_row_cate=mysql_fetch_array($sql_cate)){
			$tab_recherche_cate[$sql_row_cate['DramaID']] = "on";
		}
		
	}else{
		$tab_recherche_cate=$tab_inter;
	}
	if($acteur != "" && $acteur != "Tout"){
		$req_acteur = "SELECT * FROM t_dp_dramaactorlink WHERE ActorID='".$acteur."'";
		$sql_acteur  = mysql_query($req_acteur);
		
		while($sql_row_acteur=mysql_fetch_array($sql_acteur)){
			$tab_recherche_acteur[$sql_row_acteur['DramaID']] = "on";
		}
		
	}else{
		$tab_recherche_acteur=$tab_inter;
	}	
	if($annee != "" && $annee != "Tout"){
		$req_annee = "SELECT * FROM t_dp_drama WHERE (DramaYear=".$annee." AND StatusID = 1 AND DramaID != 110)";
		$sql_annee  = mysql_query($req_annee);
		while($sql_row_annee=mysql_fetch_array($sql_annee)){
			$tab_recherche_annee[$sql_row_annee['DramaID']] = "on";
		}
	}else{
		$tab_recherche_annee=$tab_inter;
	}
	if($chaine != "" && $chaine != "Tout"){
		$req_chaine = "SELECT * FROM t_dp_drama WHERE (ProducerID='".$chaine."' AND StatusID = 1 AND DramaID != 110)";
		$sql_chaine  = mysql_query($req_chaine);
		while($sql_row_chaine=mysql_fetch_array($sql_chaine)){
			$tab_recherche_chaine[$sql_row_chaine['DramaID']] = "on";
		}
	}else{
		$tab_recherche_chaine=$tab_inter;
	}
	if($genre != "" && $genre != "Tout"){
		$req_genre = "SELECT * FROM t_dp_dramagenrelink WHERE GenreVideoID='".$genre."'";
		$sql_genre = mysql_query($req_genre);
		while($sql_row_genre=mysql_fetch_array($sql_genre)){
			$tab_recherche_genre[$sql_row_genre['DramaID']] = "on";
		}
	}else{
		$tab_recherche_genre=$tab_inter;
	}
	if($mot != ""){
		
		$req_actor = "SELECT * FROM t_dp_actor WHERE (UPPER(ActorName) LIKE UPPER('%".$mot."%') OR UPPER(ActorOriginalName) LIKE UPPER('%".$mot."%') )";
		$sql_actor = mysql_query($req_actor);
		
		while($sql_row=mysql_fetch_array($sql_actor)){
			$req_actor_link = "SELECT * FROM t_dp_dramaactorlink WHERE ActorID = ".$sql_row['ActorID'] ;
			$sql_actor_link = mysql_query($req_actor_link);
			
			while($sql_row2=mysql_fetch_array($sql_actor_link)){
				$tab_recherche_mot[$sql_row2['DramaID']] = "on";
			}
			
		}
		
		$req_mot = "SELECT * FROM t_dp_drama WHERE ((UPPER(DramaTitle) LIKE UPPER('%".$mot."%') OR UPPER(DramaTitle2) LIKE UPPER('%".$mot."%') OR UPPER(DramaTitleFR) LIKE UPPER('%".$mot."%'))AND StatusID = 1 AND DramaID != 110)";
		$sql_mot  = mysql_query($req_mot);
		
		$req_mot2 = "SELECT * FROM t_dp_drama WHERE (UPPER(keyword) LIKE UPPER('%".$mot."%'))AND StatusID = 1 AND DramaID != 110";
		$sql_mot2 = mysql_query($req_mot2);
		
		
		while($sql_row_mot2=mysql_fetch_assoc($sql_mot2)){
					
			$chaine = $sql_row_mot2['keyword'];
			$chaine = str_replace(",", "", $chaine);
			$tab_mot = explode(" ",$chaine);
			
			foreach($tab_mot as $key => $value){
				$regex = "#^".$mot."$#i";
				
				if(preg_match($regex, $value)){
					$tab_recherche_mot[$sql_row_mot2['DramaID']] = "on";
				}
			}
		}
		
		while($sql_row_mot=mysql_fetch_array($sql_mot)){
			$tab_recherche_mot[$sql_row_mot['DramaID']] = "on";
		}
	}
	if($cate != ""){
		$tab_inter = array_intersect_assoc($tab_inter,$tab_recherche_cate);
	}
	if($annee != ""){
		$tab_inter = array_intersect_assoc($tab_inter,$tab_recherche_annee);
	}
	if($chaine != ""){
		$tab_inter = array_intersect_assoc($tab_inter,$tab_recherche_chaine);
	}
	if($genre != ""){
		$tab_inter = array_intersect_assoc($tab_inter,$tab_recherche_genre);
	}
	if($mot != ""){
		$tab_inter = array_intersect_assoc($tab_inter,$tab_recherche_mot);
	}
	if($acteur != ""){
		$tab_inter = array_intersect_assoc($tab_inter,$tab_recherche_acteur);
	}
	foreach($tab_inter as $key => $value){
		if($tri == "alpha"){
			$tab_drama_info = DramaInfo($key);
			$tab_inter_final[$key] = $tab_drama_info['titre'];
			
		}elseif($tri == "date"){
			$tab_drama_info = DramaInfo($key);
			$annee_unix = strtotime($tab_drama_info['release']);
			$tab_inter_final[$key] = $annee_unix;
			//$tab_inter_final[$key] = array("annee" => $annee_unix, "nom" => $tab_drama_info['titre'],"dramaID" => $key);
			
		}
	}
	if($tri == "alpha"){
		asort($tab_inter_final);
	}elseif($tri == "date"){
		arsort($tab_inter_final);
	/*
		foreach ($tab_inter_final as $key => $row) {
			$annee[$key]  = $row['annee'];
			$nom[$key] = $row['nom'];
			$annee_unix[$key] = strtotime($annee[$key]);
		}
	
	array_multisort($annee_unix, SORT_DESC, $nom, SORT_ASC, $tab_inter_final);
	echo '<pre>';
	print_r($tab_inter_final);
	echo '</pre>';
	foreach($tab_inter_final as $cle => $valeur){
		$table_temp[$valeur['dramaID']] = "on";
		
	}
	$tab_inter_final = $table_temp ;
	*/
	}
	return $tab_inter_final;
	
}

function EpiDispo($idEpi,$idDrama,$typeEpi,$typeUser){
	$rep = true;
	$pays = getIp2Location($_SERVER["REMOTE_ADDR"]);
	
	
	$req_sql = "SELECT * FROM t_dp_episode WHERE (DramaID=".$idDrama." AND EpisodeNumber=".$idEpi.")";
	$sql_epi = mysql_query($req_sql);
		$lvl_user = $typeUser;
	
	
	
	if($typeEpi == "free"){
		$dateEpi = mysql_result($sql_epi,0,"ReleaseDateFree");
	}
	elseif($typeEpi == "hd" && ($lvl_user == "privilege")){
		$dateEpi = mysql_result($sql_epi,0,"ReleaseDatePre");
		
		
	}elseif($typeEpi == "hd" && ($lvl_user != "privilege")){
		$rep = false;
		
	}
	elseif($typeEpi == "sd"){
		if($lvl_user == "privilege"){
			$dateEpi = mysql_result($sql_epi,0,"ReleaseDatePre");
		}elseif($lvl_user == "decouverte"){
			$dateEpi = mysql_result($sql_epi,0,"ReleaseDateDec");
		}else{
			$rep = false;
		}
		
	}
	
	if($rep != false){
		
		$time = strtotime($dateEpi);
		
		if($time <= time()){
			if($time > 7872400){
				$rep =true;
			}else{
				$rep = false;
			}
			
		}else{
			$rep = false;
		}
	}
	
	return $rep;
			
		
}
/*
function VerifEpiErreur($idEpi,$idDrama,$iduser,$pays){
	$erreur = 0;
	
	if($iduser == ""){
		$type_abo = "no";
	}else{
	
	$type_abo = abo_user($iduser);
	}
	$req_sql = "SELECT * FROM t_dp_episode WHERE (DramaID=".$idDrama." AND EpisodeNumber=".$idEpi.")";
	$sql_epi = mysql_query($req_sql);
	$nb_epi = mysql_num_rows($sql_epi);

	global $array_fr;
	if($nb_epi != 0){
	if(in_array($pays, $array_fr)|| in_array($iduser,$array_id)){
	
	
	$dateEpi_pre = mysql_result($sql_epi,0,"ReleaseDatePre");
	$time_pre = strtotime($dateEpi_pre);
		
	if($time_pre <= time()){
		if($time_pre > 7872400){
				if($type_abo == "privilege"){
				
				}elseif($type_abo == "decouverte"){
					$dateEpi_dec = mysql_result($sql_epi,0,"ReleaseDateDec");
					$time_dec = strtotime($dateEpi_dec);
					if($time_dec <= time() || $idEpi == 1 ){
					
					}else{
						$erreur = 4;
					}
				}else{
					$dateEpi_fre = mysql_result($sql_epi,0,"ReleaseDateFree");
					$time_fre = strtotime($dateEpi_fre);
					if($time_fre <= time() || $idEpi == 1 ){
					
					}else{
						$erreur = 5;
					}
				}
					
			}else{
				$erreur = 3;
			}
			
		}else{
			$erreur = 3;
		}
	
	
	
	}else{
		$crawler = 0;
		if ( preg_match('/(bot|spider|yahoo)/i', $_SERVER[ "HTTP_USER_AGENT" ] )) $crawler = 1 ;
		if($crawler == 1){
			
		}else{
		$erreur = 2;
		}
	}
	}else{
		$erreur = 1;
	}
	return $erreur;
}

function VerifEtatDrama($dramaID){
	$req = "SELECT * FROM t_dp_drama WHERE dramaID = ".$dramaID;
	$sql = mysql_query($req);
	$verif = mysql_result($sql,0,'StatusID');
	return $verif ;
}	
*/
function ActeurDramaNew($dramaID){
	global $http;
	$req_acteur = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID=".$dramaID." ORDER BY Rank";
	$sql_acteur  = mysql_query($req_acteur);
	$nb = 0;
	$tab_exeption = array(363 => 'Lee Min-Ho1993',799 => 'Lee Hye-Young2', 933 => 'Joo Jin-Mo933');
	$tab_exept_name = array(1139);
	echo '<div class="row">';
	
	while($sql_row_acteur=mysql_fetch_array($sql_acteur)){
		$id_actorlink = $sql_row_acteur['DramaActorLinkID'];
		$req_acteur_name = "SELECT * FROM t_dp_actor WHERE ActorID='".$sql_row_acteur['ActorID']."'";
		$sql_acteur_name = mysql_query($req_acteur_name);
		$acteur_name = mysql_result($sql_acteur_name,0,"ActorName");
		$acteur_id = mysql_result($sql_acteur_name,0,"ActorID");
		$acteur_role = $sql_row_acteur['RoleName'];
		
		$acteur_role = str_replace("/", "<br /><i>Et</i> ", $acteur_role);
		
		
		if($tab_exeption[$acteur_id] == ''){
		$acteur_img = "content/actors/".$acteur_name.".jpg";
		$acteur_img = str_replace(' ', '%20' ,$acteur_img);

	
		
		echo '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">';
			echo'<div class="photo_acteur">';
				echo'<a href="javascript:void();" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();"><img src="'.$http.$acteur_img.'" /></a>';
			echo'</div>';
			echo'<div class="texte_acteur">';
				echo '<form method="post" name="acteur_'.$acteur_id.'" action="'.$http.'catalogue/" target="_parent"><input type="hidden" name="lien_acteurd" value="'.$acteur_id.'"><a href="javascript:void();" class="lien_noir" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();">'.$acteur_name.'</a></form>';
				if ($id_actorlink == 1139) {
				echo '<i>dans le rôle du</i><br /> '.$acteur_role ;
				}else{
				echo '<i>dans le rôle de</i><br /> '.$acteur_role ;
				}
			echo'</div>';
		echo'</div>';
		}else{
			$acteur_img = "content/actors/".$tab_exeption[$acteur_id].".jpg";
		$acteur_img = str_replace(' ', '%20' ,$acteur_img);
		
		
		echo '<div class="cont_acteur">';
			echo'<div class="photo_acteur">';
			echo'<a href="javascript:void();" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();"><img src="'.$http.$acteur_img.'" /></a>';
			echo'</div>';
			echo'<div class="texte_acteur">';
				echo '<form method="post" name="acteur_'.$acteur_id.'" action="'.$http.'catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="'.$acteur_id.'"><a href="javascript:void();" class="lien_noir" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();">'.$acteur_name.'</a></form>';
				echo '<br />';
				if ($id_actorlink == 1139) {
				echo '<i>dans le rôle du</i><br /> '.$acteur_role ;
				}else{
				echo '<i>dans le rôle de</i><br /> '.$acteur_role ;
				}
			echo'</div>';
		echo'</div>';
		}
		
		
		
	}
	
	echo'</div>';

}
function ActreurDrama($dramaID){
	global $http;
	$req_acteur = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID=".$dramaID." ORDER BY Rank";
	$sql_acteur  = mysql_query($req_acteur);
	$nb = 0;
	$tab_exeption = array(363 => 'Lee Min-Ho1993',799 => 'Lee Hye-Young2', 933 => 'Joo Jin-Mo933');
	$tab_exept_name = array(1139);
	echo '<div class="cont_acteur_all">';
	
	while($sql_row_acteur=mysql_fetch_array($sql_acteur)){
		$id_actorlink = $sql_row_acteur['DramaActorLinkID'];
		$req_acteur_name = "SELECT * FROM t_dp_actor WHERE ActorID='".$sql_row_acteur['ActorID']."'";
		$sql_acteur_name = mysql_query($req_acteur_name);
		$acteur_name = mysql_result($sql_acteur_name,0,"ActorName");
		$acteur_id = mysql_result($sql_acteur_name,0,"ActorID");
		$acteur_role = $sql_row_acteur['RoleName'];
		
		$acteur_role = str_replace("/", "<br /><i>Et</i> ", $acteur_role);
		
		
		if($tab_exeption[$acteur_id] == ''){
		$acteur_img = "content/actors/".$acteur_name.".jpg";
		$acteur_img = str_replace(' ', '%20' ,$acteur_img);

	
		
		echo '<div class="cont_acteur">';
			echo'<div class="photo_acteur">';
				echo'<a href="javascript:void();" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();"><img src="'.$http.$acteur_img.'" /></a>';
			echo'</div>';
			echo'<div class="texte_acteur">';
				echo '<form method="post" name="acteur_'.$acteur_id.'" action="'.$http.'catalogue/" target="_parent"><input type="hidden" name="lien_acteurd" value="'.$acteur_id.'"><a href="javascript:void();" class="lien_noir" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();">'.$acteur_name.'</a></form>';
				echo '<br />';
				if ($id_actorlink == 1139) {
				echo '<i>dans le rôle du</i><br /> '.$acteur_role ;
				}else{
				echo '<i>dans le rôle de</i><br /> '.$acteur_role ;
				}
			echo'</div>';
		echo'</div>';
		}else{
			$acteur_img = "content/actors/".$tab_exeption[$acteur_id].".jpg";
		$acteur_img = str_replace(' ', '%20' ,$acteur_img);
		
		
		echo '<div class="cont_acteur">';
			echo'<div class="photo_acteur">';
			echo'<a href="javascript:void();" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();"><img src="'.$http.$acteur_img.'" /></a>';
			echo'</div>';
			echo'<div class="texte_acteur">';
				echo '<form method="post" name="acteur_'.$acteur_id.'" action="'.$http.'catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="'.$acteur_id.'"><a href="javascript:void();" class="lien_noir" title="Voir tous les dramas avec '.$acteur_name.'" onclick="document.forms[\'acteur_'.$acteur_id.'\'].submit();">'.$acteur_name.'</a></form>';
				echo '<br />';
				if ($id_actorlink == 1139) {
				echo '<i>dans le rôle du</i><br /> '.$acteur_role ;
				}else{
				echo '<i>dans le rôle de</i><br /> '.$acteur_role ;
				}
			echo'</div>';
		echo'</div>';
		}
		
		
		
	}
	
	echo'</div>';

}

function DramaSimi($DramaID){
	$tab_drama = DramaInfo($DramaID);
	$drama_cate = $tab_drama['cate'];
}

function AffPub($type){
	global $http;
	if($type == "2"){
		echo '<script type="text/javascript"><!-- 
		google_ad_client = "ca-pub-0924014780852025";
/* small box */
google_ad_slot = "4344353117";
google_ad_width = 250;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script><br /><br />';
	}
	if($type == "11"){
		echo '<a href="'.$http.'store/"><img src="'.$http.'images/DramaAutreDL.jpg" border="0" width="250"></a><br /><br />';
	}
	if($type == "1"){
		echo '<a href="'.$http.'premium/"><img src="'.$http.'images/abonnement.jpg" border="0" width="250"></a><br /><br />';
	}
	if($type == "3"){
		//$rand = rand (1, 2);
		$rand = 1;
		if($rand == 1){
			echo '<a href="'.$http.'dvd/detail/4/Coffret-DVD-Vampire-Prosecutor"><img width="250" title="Le coffret DVD &quot;Vampire Prosecutor &quot;" alt="DVD &quot;Vampire Prosecutor&quot;"  src="'.$http.'content/dvd/vpdvdsmall.png" border="0"></a><br /><br /><br />';
			//echo '<a href="'.$http.'dvd/detail/1/Coffret-DVD-Boys-over-Flowers"><img width="250" title="Le coffret DVD &quot;Boys over Flowers&quot;" alt="DVD &quot;Boys over Flowers&quot;" src="'.$http.'content/dvd/bofdvdsmall.jpg" border="0"></a><br /><br />';
		}else{
			echo '<a href="'.$http.'dvd/detail/1/Coffret-DVD-Boys-over-Flowers"><img width="250" title="Le coffret DVD &quot;Boys over Flowers&quot;" alt="DVD &quot;Boys over Flowers&quot;" src="'.$http.'content/dvd/bofdvdsmall.jpg" border="0"></a><br /><br />';
			//echo '<a href="'.$http.'dvd/detail/2/Coffret-DVD-The-1st-Shop-of-Coffee-Prince"><img width="250" title="Le coffret DVD &quot;Coffee Prince&quot;" alt="DVD &quot;Coffee Prince&quot;"  src="'.$http.'content/dvd/cpdvdsmall.jpg" border="0"></a><br /><br /><br />';
		}
	}
	if($type == "5"){
	/* 728x90, created 1/8/11 */
	
/*	 echo '<script type="text/javascript"><!--

google_ad_client = "pub-0924014780852025";



google_ad_slot = "7024655727";

google_ad_width = 728;

google_ad_height = 90;

//-->

</script> 

	

<script type="text/javascript"

src="http://pagead2.googlesyndication.com/pagead/show_ads.js">

</script>';   */

echo "<script type='text/javascript'>
<!--//<![CDATA[
   document.MAX_ct0 ='';
   var m3_u = (location.protocol=='https:'?'https://cas.criteo.com/delivery/ajs.php?':'http://cas.criteo.com/delivery/ajs.php?');
   var m3_r = Math.floor(Math.random()*99999999999);
   document.write (\"<scr\"+\"ipt type='text/javascript' src='\"+m3_u);
   document.write (\"zoneid=158911\");document.write(\"&amp;nodis=1\");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write (\"&amp;exclude=\" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write (\"&amp;loc=\" + escape(window.location));
   if (document.referrer) document.write (\"&amp;referer=\" + escape(document.referrer));
   if (document.context) document.write (\"&context=\" + escape(document.context));
   if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
       document.write (\"&amp;ct0=\" + escape(document.MAX_ct0));
   }
   if (document.mmm_fo) document.write (\"&amp;mmm_fo=1\");
   document.write (\"'></scr\"+\"ipt>\");
//]]>--></script>";

	}
	if($type == "4"){
	/* 300x250, created 1/8/11 */
/*	echo '<script type="text/javascript"><!--

google_ad_client = "pub-0924014780852025";



google_ad_slot = "4174838447";

google_ad_width = 300;

google_ad_height = 250;

//-->

</script> 

			  

<script type="text/javascript"

src="http://pagead2.googlesyndication.com/pagead/show_ads.js">

</script>
';   */

echo "<script type='text/javascript'>
<!--//<![CDATA[
   document.MAX_ct0 ='';
   var m3_u = (location.protocol=='https:'?'https://cas.criteo.com/delivery/ajs.php?':'http://cas.criteo.com/delivery/ajs.php?');
   var m3_r = Math.floor(Math.random()*99999999999);
   document.write (\"<scr\"+\"ipt type='text/javascript' src='\"+m3_u);
   document.write (\"zoneid=158868\");document.write(\"&amp;nodis=1\");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write (\"&amp;exclude=\" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write (\"&amp;loc=\" + escape(window.location));
   if (document.referrer) document.write (\"&amp;referer=\" + escape(document.referrer));
   if (document.context) document.write (\"&context=\" + escape(document.context));
   if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
       document.write (\"&amp;ct0=\" + escape(document.MAX_ct0));
   }
   if (document.mmm_fo) document.write (\"&amp;mmm_fo=1\");
   document.write (\"'></scr\"+\"ipt>\");
//]]>--></script>";
	
	}
	if($type == "6"){
	echo '<div style="border: solid 1px #d8bb43;width: 248px;height: 198px;margin-bottom: 20px;">';
		echo '<a href="javascript:void();" class="iframe" id="test_HD2"><img src="'.$http.'images/offre2_HD.jpg" style="margin-left:45px;" /></a><br /><a href="javascript:void();" class="iframe" id="test_SD2"><img src="'.$http.'images/offre2_SD.jpg" style="margin-left:45px;margin-top:-20px;" /></a><br />';
		echo '<p style="text-align:center;"><a href="'.$http.'premium/" style="font-size:12px;" class="lien_bleu">Cliquez ici pour plus d\'info</a></p>';
	echo '</div>';
	
	}
	if($type == "7"){
	echo '<div style="border: solid 1px #d8bb43;width: 248px;height: 80px;margin-bottom: 20px;">';
		echo '<p style="text-align:center;margin-top:15px;"><span class="noir" style="font-size:14px;"><b>Problèmes de lenteur ?</b></span><br /><br /><a href="'.$http.'test-connexion/" style="font-size:14px;" class="lien_bleu"><b>CLIQUEZ ICI</b></a></p>';
	echo '</div>';
	
	}
	if($type == "8"){
	echo '<div style="border: solid 1px #d8bb43;width: 248px;height: 80px;margin-bottom: 20px;">';
		echo '<p style="text-align:center;margin-top:15px;"><span class="noir" style="font-size:14px;"><b>Comment modifier la qualité ?</b></span><br /><br /><a target="_blank" href="'.$http.'guide_lecteur" style="font-size:14px;" class="lien_bleu"><b>CLIQUEZ ICI</b></a></p>';
	echo '</div>';
	
	}
	if($type == "9"){
	echo '<div style="width: 250px;height: 454px">
	<div style="padding: 0px !important; margin: 0px !important; height: 360px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Aside/250_xv3_01.jpg" /></div>
	<div style="padding: 0px !important; margin: 0px !important height: 41px !important;overflow: hidden;">
		<div style="float: left;height: 41px !important;overflow: hidden;"><a href="http://itunes.com/apps/Dramapassion" target="_blank" style="text-decoration: none !important"><img src="http://www.dramapassion.com/images/Aside/250_xv3_02.jpg" /></a></div>
		<div style="float: left;height: 41px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Aside/250_xv3_03.jpg" /></div>
		<div style="clear: both;"></div>
	</div>
	<div style="padding: 0px !important; margin: 0px !important height: 53px !important;overflow: hidden;">
		<div style="float: left;height: 53px !important;overflow: hidden;"><a href="http://play.google.com/store/apps/details?id=com.dramapassion.dramapassion" target="_blank" style="text-decoration: none !important"><img src="http://www.dramapassion.com/images/Aside/250_xv3_04.jpg" /></a></div>
		<div style="float: left;height: 53px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Aside/250_xv3_05.jpg" /></div>
		<div style="clear: both;"></div>
	</div>
</div>';
	}
	if($type == "10"){
		$arrayGong = array("a","b","c","d","e","f","g","h");
		$arrayGongRandom = shuffle($arrayGong);
		$random = $arrayGong[0];
		echo '<div style="width: 250px;height: 377px">';
		echo '<div style="padding: 0px !important; margin: 0px !important; height: 330px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Aside/pub_mygong_v4'.$random.'_01.png" /></div>';
		echo '<div style="padding: 0px !important; margin: 0px !important height: 29px !important;overflow: hidden;">';
			echo '<div style="float: left;height: 29px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Aside/pub_mygong_v4a_02.png" /></div>';
			echo '<div style="float: left;height: 29px !important;overflow: hidden;"><a href="http://play.google.com/store/apps/details?id=com.gong.appmygong" target="_blank" style="text-decoration: none !important"><img src="http://www.dramapassion.com/images/Aside/pub_mygong_v4a_03.png" /></a></div>';
			echo '<div style="float: left;height: 29px !important;overflow: hidden;"><a href="https://itunes.apple.com/fr/app/mygong/id989963163?mt=8" target="_blank" style="text-decoration: none !important"><img src="http://www.dramapassion.com/images/Aside/pub_mygong_v4a_04.png" /></a></div>';
			echo '<div style="clear: both;"></div>';
		echo '</div>';
		echo '<div style="padding: 0px !important; margin: 0px !important; height: 18px !important;overflow: hidden;"><img src="http://www.dramapassion.com/images/Aside/pub_mygong_v4a_05.png" /></div>';
	}
}
function AffCateDrama($DramaID){
	$rep = DramaInfo($DramaID);
	$cat = $rep['cate'];
	
if($cat == "com"){
$cat = "Comédie / Comédie Romantique";

}
if($cat == "drame"){
$cat = "Drame";

}
if($cat == "act"){
$cat = "Action / Thriller / Fantastique";

}
if($cat == "hist"){
$cat = "Historique";

}
echo $cat;
}
function AffGenreDrama($DramaID){
	$req = "SELECT * FROM t_dp_dramagenrelink WHERE DramaID = '".$DramaID."'";
	$sql = mysql_query($req);
	$nb = 0;
	$nb_neg =0;
	$tab_all= array();
	
	while($sql_row=mysql_fetch_array($sql)){
	
	$req_genre = "SELECT * FROM t_dp_genrevideo WHERE GenreVideoID='".$sql_row['GenreVideoID']."'";
	$sql_genre = mysql_query($req_genre);
	$tab_all[$nb] = mysql_result($sql_genre,0,"GenreVideoDescFre");
	
	$nb++;
	}
	sort($tab_all);
	$nb_genre = count($tab_all);

	for($i=1 ; $i <= $nb_genre; $i++){
		
		if($i == 1 ){
			$sortie = $tab_all[$i-1];
			
		}else{
			$sortie =  $sortie.", ".$tab_all[$i-1];
		}
	}
	
	$rep = DramaInfo($DramaID);
	$cat = $rep['cate'];
	
if($cat == "com"){
$cat = "Comédie, Comédie Romantique";

}
if($cat == "drame"){
$cat = "Drame";

}
if($cat == "act"){
$cat = "Action, Thriller, Fantastique";

}
if($cat == "hist"){
$cat = "Historique";

}
	
	if($cat == $sortie){
		$sortie_full = $cat;
	}else{
		$sortie_full = $cat.', '.$sortie;
	}
	echo $sortie_full ;
}
function AffGenreDrama2($DramaID){
	$req = "SELECT * FROM t_dp_dramagenrelink WHERE DramaID = '".$DramaID."'";
	$sql = mysql_query($req);
	$nb = 0;
	$nb_neg =0;
	$tab_all= array();
	
	while($sql_row=mysql_fetch_array($sql)){
	
	$req_genre = "SELECT * FROM t_dp_genrevideo WHERE GenreVideoID='".$sql_row['GenreVideoID']."'";
	$sql_genre = mysql_query($req_genre);
	$tab_all[$nb] = mysql_result($sql_genre,0,"GenreVideoDescFre");
	
	$nb++;
	}
	sort($tab_all);
	$nb_genre = count($tab_all);

	for($i=1 ; $i <= $nb_genre; $i++){
		
		if($i == 1 ){
			$sortie = $tab_all[$i-1];
			
		}else{
			$sortie =  $sortie.", ".$tab_all[$i-1];
		}
	}
	
	$rep = DramaInfo($DramaID);
	$cat = $rep['cate'];
	
if($cat == "com"){
$cat = "Comédie, Comédie Romantique";

}
if($cat == "drame"){
$cat = "Drame";

}
if($cat == "act"){
$cat = "Action, Thriller, Fantastique";

}
if($cat == "hist"){
$cat = "Historique";

}
	
	if($cat == $sortie){
		$sortie_full = $cat;
	}else{
		$sortie_full = $cat.', '.$sortie;
	}
	return $sortie_full ;
}
function GetGenreDrama($DramaID){
	$req = "SELECT * FROM t_dp_dramagenrelink WHERE DramaID = '".$DramaID."'";
	$sql = mysql_query($req);
	$nb_genre = mysql_num_rows($sql);
	if($nb_genre > 0){
		return 'ok';
	}else{
		return 'none';
	}
}
function InfoUser($id){
	$req = "SELECT * FROM t_dp_user WHERE UserID = '".$id."'";
	$sql = mysql_query($req);
	$tab_user['name'] = mysql_result($sql,0,"UserName");
	$tab_user['Fname'] = mysql_result($sql,0,"UserFname");
	$tab_user['Lname'] = mysql_result($sql,0,"UserLname");
	$tab_user['email'] = mysql_result($sql,0,"UserEmail");
	$tab_user['sexe'] = mysql_result($sql,0,"SexID");
	$tab_user['annif'] = mysql_result($sql,0,"UserBirthdate");
	$tab_user['pays'] = mysql_result($sql,0,"CountryID");
	$tab_user['province'] = mysql_result($sql,0,"UserProvince");
	$tab_user['city'] = mysql_result($sql,0,"UserCity");
	$tab_user['addr1'] = mysql_result($sql,0,"UserAddress1");
	$tab_user['addr2'] = mysql_result($sql,0,"UserAddress2");
	$tab_user['zip'] = mysql_result($sql,0,"UserZip");
	$tab_user['gsm'] = mysql_result($sql,0,"UserMobile");
	$tab_user['added'] = mysql_result($sql,0,"UserAdded");
	$tab_user['subs'] = mysql_result($sql,0,"SubscriptionID");
	$tab_user['next'] = mysql_result($sql,0,"Next");
	$tab_user['expire'] = mysql_result($sql,0,"Expiry");
	$tab_user['autorenew'] = mysql_result($sql,0,"autorenewal");
	$tab_user['idFacebook'] = mysql_result($sql,0,"userIDfacebook");
	$tab_user['newsletter'] = mysql_result($sql,0,"newsletter");
	
	$req_pays = "SELECT * FROM t_dp_countryship WHERE CountryID = ".$tab_user['pays'];
	$sql_pays = mysql_query($req_pays);
	$tab_user['pays_fr'] = mysql_result($sql_pays,0,"CountryNameFre");
	
	if($tab_user['sexe'] == 1){
		$tab_user['sexe'] = "Homme";
	}elseif($tab_user['sexe'] == 2){
		$tab_user['sexe'] = "Femme";
	}
	
	//$tab_user['annif']=eregi_replace("([0-9].*)-([0-9].*)-([0-9].*)" ,"\\3-\\2-\\1",$tab_user['annif']);
	$explode_annif = explode("-", $tab_user['annif']);
	$tab_user['annif'] = $explode_annif[2].'-'.$explode_annif[1].'-'.$explode_annif[0];
	
	$req_pays = "SELECT * FROM t_dp_countryship WHERE CountryID = '".$tab_user['pays']."'";
	$sql_pay = mysql_query($req_pays);
	
	$tab_user['pays'] =  mysql_result($sql_pay,0,"CountryNameFre");
	
	return $tab_user;
}

function getOs() {
	$serOs = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match("#Windows Phone#", $serOs)){
		$os = 'Android';
	}elseif(preg_match("#Windows#", $serOs)){
		if(preg_match("#ARM#", $serOs)){
			$os = 'RT';
		}else{
			$os = 'Win';
		}
	}elseif(preg_match("#iPad#", $serOs) || preg_match("#iPhone#", $serOs) || preg_match("#iPod#", $serOs)){
		$os = 'IOS';
	}elseif(preg_match("#Mac OS#", $serOs)){
		$os = 'Mac';
	}elseif(preg_match("#Android#", $serOs)){
		$os = 'Android';
	}elseif(preg_match("#Linux#", $serOs)){
		$os = 'Linux';
	}elseif(preg_match("#Ubuntu#", $serOs)){
		$os = 'Linux';
	
	}elseif(preg_match("#PLAYSTATION#", $serOs)){ 
		$os = 'Ps';
	}elseif(preg_match("#Wii#", $serOs)){ 
		$os = 'Wii';
	}elseif(preg_match("#PlayBook#", $serOs)){ 
		$os = 'Android';
	}elseif(preg_match("#Bada#", $serOs)){ 
		$os = 'Android';
	}else{
		$os = 'Non';
	}
	
	return $os;
	
}
function dramaLinkClean($drama){
	$drama_link = str_replace(' ', '-' ,$drama);
	$drama_link = str_replace('\'','',$drama_link);
	$drama_link = str_replace(',','',$drama_link);
	$drama_link = str_replace('.','',$drama_link);
	$drama_link = str_replace('!','',$drama_link);
	$drama_link = str_replace('?','',$drama_link);
	$drama_link = str_replace('à','a',$drama_link);
	$drama_link = str_replace(':','-',$drama_link);
	
	
	return $drama_link;


}
function playlist_bt($drama_id){
	global $http;
	$drama = DramaInfo($drama_id);
	echo '<a href="'.$http.'playlist_select.php?dramaID='.$drama_id.'" class="iframe playlist_a"><img src="'.$http.'images/playlist.png" class="playlist_img" border="0" alt="Ajouter &quot;'.$drama["titre"].'&quot; à ma playlist" title="Ajouter &quot;'.$drama["titre"].'&quot; à ma playlist" /></a>';

}
function playlist2($drama_id){
	global $http;
	$drama = DramaInfo($drama_id);
	echo '<div style="float:left;"><a href="'.$http.'playlist_select.php?dramaID='.$drama_id.'" class="iframe playlist_a2">Ajouter à ma playlist</div><div style="float:left;margin-top:0px;"><img src="'.$http.'images/playlist.png" border="0" class="playlist_img" alt="Ajouter &quot;'.$drama["titre"].'&quot; à ma playlist" title="Ajouter &quot;'.$drama["titre"].'&quot; à ma playlist" /></div></a>';

}
function comming_soon(){
	global $http;
	$req = "SELECT * FROM t_dp_drama WHERE (StatusID = 2 OR StatusID = 10) ORDER BY ReleaseDate ";
	$sql = mysql_query($req);
	
	
	echo '<table cellpadding="0" cellspacing="0" width="250">';
							echo'<tr>';
								echo'<td colspan="2" class="menu_noir">Prochainement</td>';
							echo'</tr>';
							echo'<tr height="12">';
								echo'<td colspan="2"><img src="images/ligne250.jpg" width="250"></td>';
							echo'</tr>';
							
							
					
	while($sql_row=mysql_fetch_array($sql)){
		$dramaID=$sql_row['DramaID'];
		$drama = DramaInfo($dramaID);
		$link = dramaLinkClean($drama["titre"]);
		if($sql_row['StatusID'] == 10){
		echo'<tr height="38">';
								echo'<td align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'"><img title="drama coréen &quot;'.$drama["titre"].'&quot; en vostfr" alt="série coréenne &quot;'.$drama["titre"].'&quot; en vostfr" src="'.$drama["img_thumb"].'" width="53" ></a></td>';
								echo'<td class="noir" align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'" class="lien_noir">'.$drama["titre"].'</a></td>';
							echo'</tr>';
		}else{
		echo'<tr height="38">';
								echo'<td align="left"><img title="drama coréen &quot;'.$drama["titre"].'&quot; en vostfr" alt="série coréenne &quot;'.$drama["titre"].'&quot; en vostfr" src="'.$drama["img_thumb"].'" width="53" ></td>';
								echo'<td class="noir" align="left">'.$drama["titre"].'</td>';
							echo'</tr>';		
		}
	}
	echo'</table>';

}

function prochainement(){
	global $http;
	$req = "SELECT * FROM t_dp_drama WHERE (StatusID = 2 OR StatusID = 10) ORDER BY ReleaseDate ";
	$sql = mysql_query($req);
							
	$out = false;
	$i = 0;		
					
	while($sql_row=mysql_fetch_array($sql)){
		$dramaID=$sql_row['DramaID'];
		$drama = DramaInfo($dramaID);
		$link = dramaLinkClean($drama["titre"]);
		if($sql_row['StatusID'] == 10){
			
		$out[$i]['link'] = 1;

		}else{
		$out[$i]['link'] = 0;
	
		}
		$out[$i]['img'] = $drama["img_big"];
		$out[$i]['link'] = $http.'drama/'.$dramaID.'/'.$link;
		$i++;
		
	}
	return $out;

}

function abo_user($userID){
	global $array_dis ;
	global $array_pri ;
	$sortie = "no";

	$req = "SELECT * FROM t_dp_user WHERE UserID = '".$userID."'";
	$sql = mysql_query($req);
	$nb_user = mysql_num_rows($sql);
	if($nb_user > 0){
	$sub_user = mysql_result($sql,0,"SubscriptionID");
	
	if(in_array($sub_user, $array_dis)) {
		$sortie = "decouverte";
	}elseif(in_array($sub_user, $array_pri)) {
		$sortie = "privilege";
	}else{
		$sortie = "no";
	}
	}
	return $sortie;
}
function facebook_drama($dramaID){
	$fonction = 2 ;
	
	
	if($fonction == 1){
		echo '<div class="fb-like-box" data-href="'.$dramaID.'" data-width="250" data-height="260" data-show-faces="true" data-stream="false" data-header="true"></div>';
	}elseif($fonction == 2){
		echo '<div class="fb-like" data-href="'.$dramaID.'" data-send="false" data-width="250" data-show-faces="true"></div>';
	}
	
	
}
function recommander_drama($dramaID){
	global $http; 
	global $paques;
	$req_acteur_drama = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID = ".$dramaID." AND Rank <= 4" ;
	$sql_acteur_drama = mysql_query($req_acteur_drama);
	$i = 1 ;
	$actor_count = 0;
	
	
	
	
	while($row = mysql_fetch_assoc($sql_acteur_drama)){
	
		$tab_acteur_drama[$i] = $row['ActorID'];
		$i++;
		
	}
	
	
	$req_acteur_sim = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID != ".$dramaID." AND (ActorID = ";
	foreach($tab_acteur_drama as $key => $value){
		if($key == 1){
			$req_acteur_sim = $req_acteur_sim.$value;
		}else{
			$req_acteur_sim = $req_acteur_sim.' OR ActorID = '.$value;
		}
	}
	$req_acteur_sim = $req_acteur_sim.')';
	$sql_acteur_sim = mysql_query($req_acteur_sim);
	$actor_count = mysql_num_rows($sql_acteur_sim);
	
	if($actor_count > 0){
	$j = 1;
	while($row = mysql_fetch_assoc($sql_acteur_sim)){
	
		$tab_drama[$j] = $row['DramaID'];
		$j++;
	}
	$tab_drama = array_unique($tab_drama);
	
	$req_verif_drama = "SELECT * FROM t_dp_drama WHERE ";
	foreach($tab_drama as $key_v => $value_v){
		if($key_v == 1){
			$req_verif_drama = $req_verif_drama.'(DramaID = '.$value_v;
		}else{
			$req_verif_drama = $req_verif_drama.' OR DramaID = '.$value_v;
		}
	}
	$req_verif_drama = $req_verif_drama.')';
	$sql_verif_drama = mysql_query($req_verif_drama);
	
	$m=0;
	while($row_verif = mysql_fetch_assoc($sql_verif_drama)){
		if($row_verif['StatusID'] == 1 || $row_verif['StatusID'] == 2){
			$tab_drama_verifie[$m] = $row_verif['DramaID'];
			$m++;
		}
		
	}
	
	
	shuffle($tab_drama_verifie);
	$tab_drama_verifie = array_slice($tab_drama_verifie,0,5);
	
	echo '<table cellpadding="0" cellspacing="0" width="250" style="border: solid 1px #f6045b;padding : 5px;padding-bottom: 10px;">';
							echo'<tr>';
								echo'<td colspan="2" class="menu_noir" style="font-size:14px;text-align:center;"><div style="height:20px;margin-top:5px;"><div style="display: inline-block;heihgt:15px;margin-top:1px;margin-right:7px;margin-left: -7px;"><img src="'.$http.'images/coeur.png" /></div><div style="display:inline-block;">Avec les mêmes acteurs...</div><div style="display: inline-block;margin-top:1px;margin-right : -7px;margin-left: 7px;"><img src="'.$http.'images/coeur.png" /></div></div></td>';
							echo'</tr>';
							echo '<tr colspan="2" height="10" ><td> </td></tr>';
							
	$i = 0;						
	foreach($tab_drama_verifie as $key_d => $value_d){				
		$dramaID=$value_d;
		$drama = DramaInfo($dramaID);
		$link = dramaLinkClean($drama["titre"]);
		
		$req_recup_acteur = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID = ".$dramaID.' AND ( ActorID = ' ;
		
		foreach($tab_acteur_drama as $key_a => $value_a){
			if($key_a == 1){
				$req_recup_acteur = $req_recup_acteur.$value_a;
			}else{
				$req_recup_acteur = $req_recup_acteur.' OR ActorID = '.$value_a;
			}
		}
		$req_recup_acteur = $req_recup_acteur.' ) ';
		$sql_recup_acteur = mysql_query($req_recup_acteur);
		$a = 0;
		$tab_aff_acteur = array();
		while($row_a = mysql_fetch_assoc($sql_recup_acteur)){
			$req_nom_acteur = "SELECT * FROM t_dp_actor WHERE ActorID = ".$row_a['ActorID'];
			$sql_nom_acteur = mysql_query($req_nom_acteur);
			$nom_acteur = mysql_result($sql_nom_acteur, 0,'ActorName');
			$id_acteur = mysql_result($sql_nom_acteur, 0,'ActorID');
			$tab_aff_acteur[$a] = $nom_acteur;
			$tab_aff_acteur_id[$a] = $id_acteur;
			$a++;
		}
		echo'<tr height="38">';
								echo'<td width="70" align="left" style="padding-left:8px;"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'"><img title="drama coréen &quot;'.$drama["titre"].'&quot; en vostfr" alt="série coréenne &quot;'.$drama["titre"].'&quot; en vostfr" src="'.$drama["img_thumb"].'" width="53" ></a></td>';
								echo'<td class="noir" align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'" class="lien_noir">'.$drama["titre"].'</a><br /><p style="color:grey;font-size:12px;">';
								
								foreach($tab_aff_acteur as $key_ac => $value_ac){
									//echo $value_ac.' '.'  '.' ';
									echo '<form method="post" name="acteur_'.$tab_aff_acteur_id[$key_ac].'_'.$i.'" action="'.$http.'catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="'.$tab_aff_acteur_id[$key_ac].'"><a href="javascript:void();" title="Voir toutes les séries avec '.$value_ac.'" class="lien_recommander" onclick="document.forms[\'acteur_'.$tab_aff_acteur_id[$key_ac].'_'.$i.'\'].submit();">'.$value_ac.' '.'  '.' '.'</a></form>';
									$i++;
								}
								echo '</p></td>';
							echo'</tr>';
		
	}
	echo'</table>';
	}else{
		$req_drama_cate = "SELECT * FROM t_dp_drama WHERE DramaID = ".$dramaID ;
		$sql_drama_cate = mysql_query($req_drama_cate);
		$cate_drama = mysql_result($sql_drama_cate,0, 'Categorie');
		$req_drama_sim ="SELECT * FROM t_dp_drama WHERE ( Categorie = '".$cate_drama."' AND StatusID = 1 )";
		$sql_drama_sim = mysql_query($req_drama_sim);
		$s = 1;
		while($row = mysql_fetch_assoc($sql_drama_sim)){
			$tab_drama_sim[$s] = $row['DramaID'];
			$s++;
		}
		$tab_drama_sim = array_unique($tab_drama_sim);
		shuffle($tab_drama_sim);
		$tab_drama_sim = array_slice($tab_drama_sim,0,5);
		
		
		echo '<table cellpadding="0" cellspacing="0" width="250" style="border: solid 1px #f6045b;padding : 5px;padding-bottom: 10px;">';
							echo'<tr>';
								echo'<td colspan="2" class="menu_noir" style="font-size:14px;text-align:center;"><div style="height:20px;margin-top:5px;"><div style="display:inline-block;">Séries similaires</div></div></td>';
							echo'</tr>';
							echo '<tr colspan="2" height="10" ><td> </td></tr>';
							
							
		if($paques == 1 && $dramaID == 210){
			$dbQpaques = "SELECT * FROM paques WHERE id = 8";
			$sqlQpaques = mysql_query($dbQpaques);
			$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
			$idPaques = mysql_result($sqlQpaques,0,"name");
			$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
			if(time()>$dateshow || $_SESSION["userid"] == 3){	
				$oeuf = '<tr colspan="2" height="10" ><td style="text-align:center;"><img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:20px;"></td></tr>';
				echo $oeuf;
			}
		}
							
		foreach($tab_drama_sim as $key_s => $value_s){				
		$dramaID=$value_s;
		$drama = DramaInfo($dramaID);
		$link = dramaLinkClean($drama["titre"]);
		
		
		
		echo'<tr height="38">';
								echo'<td width="70" align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'"><img title="drama coréen &quot;'.$drama["titre"].'&quot; en vostfr" alt="série coréenne &quot;'.$drama["titre"].'&quot; en vostfr" src="'.$drama["img_thumb"].'" width="53" ></a></td>';
								echo'<td class="noir" align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'" class="lien_noir">'.$drama["titre"].'</a></td>';
							echo'</tr>';
		
	}
	echo'</table>';
		
		
	}
	
}
function recommander_drama_new($dramaID){
	global $http; 
	global $paques;
	$req_acteur_drama = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID = ".$dramaID." AND Rank <= 4" ;
	$sql_acteur_drama = mysql_query($req_acteur_drama);
	$i = 1 ;
	$actor_count = 0;
	
	
	
	
	while($row = mysql_fetch_assoc($sql_acteur_drama)){
	
		$tab_acteur_drama[$i] = $row['ActorID'];
		$i++;
		
	}
	
	
	$req_acteur_sim = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID != ".$dramaID." AND (ActorID = ";
	foreach($tab_acteur_drama as $key => $value){
		if($key == 1){
			$req_acteur_sim = $req_acteur_sim.$value;
		}else{
			$req_acteur_sim = $req_acteur_sim.' OR ActorID = '.$value;
		}
	}
	$req_acteur_sim = $req_acteur_sim.')';
	$sql_acteur_sim = mysql_query($req_acteur_sim);
	$actor_count = mysql_num_rows($sql_acteur_sim);
	
	if($actor_count > 0){
	$j = 1;
	while($row = mysql_fetch_assoc($sql_acteur_sim)){
	
		$tab_drama[$j] = $row['DramaID'];
		$j++;
	}
	$tab_drama = array_unique($tab_drama);
	
	$req_verif_drama = "SELECT * FROM t_dp_drama WHERE ";
	foreach($tab_drama as $key_v => $value_v){
		if($key_v == 1){
			$req_verif_drama = $req_verif_drama.'(DramaID = '.$value_v;
		}else{
			$req_verif_drama = $req_verif_drama.' OR DramaID = '.$value_v;
		}
	}
	$req_verif_drama = $req_verif_drama.')';
	$sql_verif_drama = mysql_query($req_verif_drama);
	
	$m=0;
	while($row_verif = mysql_fetch_assoc($sql_verif_drama)){
		if($row_verif['StatusID'] == 1 || $row_verif['StatusID'] == 2){
			$tab_drama_verifie[$m] = $row_verif['DramaID'];
			$m++;
		}
		
	}
	
	
	shuffle($tab_drama_verifie);
	$tab_drama_verifie = array_slice($tab_drama_verifie,0,5);
	
	
	
	echo '<table class="table table-same">
			<thead> 
				<tr> 
					<th colspan="2" class="top-table top-10" ><p class="p-top-10">Avec les mêmes acteurs...</p></th>  
				</tr>
			</thead>
			<tbody>';

					$i = 0;						
					foreach($tab_drama_verifie as $key_d => $value_d){				
						$dramaID=$value_d;
						$drama = DramaInfo($dramaID);
						$link = dramaLinkClean($drama["titre"]);
						
						$req_recup_acteur = "SELECT * FROM t_dp_dramaactorlink WHERE DramaID = ".$dramaID.' AND ( ActorID = ' ;
						
						foreach($tab_acteur_drama as $key_a => $value_a){
							if($key_a == 1){
								$req_recup_acteur = $req_recup_acteur.$value_a;
							}else{
								$req_recup_acteur = $req_recup_acteur.' OR ActorID = '.$value_a;
							}
						}
						$req_recup_acteur = $req_recup_acteur.' ) ';
						$sql_recup_acteur = mysql_query($req_recup_acteur);
						$a = 0;
						$tab_aff_acteur = array();
						while($row_a = mysql_fetch_assoc($sql_recup_acteur)){
							$req_nom_acteur = "SELECT * FROM t_dp_actor WHERE ActorID = ".$row_a['ActorID'];
							$sql_nom_acteur = mysql_query($req_nom_acteur);
							$nom_acteur = mysql_result($sql_nom_acteur, 0,'ActorName');
							$id_acteur = mysql_result($sql_nom_acteur, 0,'ActorID');
							$tab_aff_acteur[$a] = $nom_acteur;
							$tab_aff_acteur_id[$a] = $id_acteur;
							$a++;
						}
						echo'<tr height="60">';
												echo'<td " align="left" style="padding-left:8px;"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'"><img height="50" title="drama coréen &quot;'.$drama["titre"].'&quot; en vostfr" alt="série coréenne &quot;'.$drama["titre"].'&quot; en vostfr" src="'.$drama["img_thumb"].'"  ></a></td>';
												echo'<td class="noir" align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'" class="lien_noir">'.$drama["titre"].'</a><br /><p style="color:grey;font-size:12px;">';
												
												foreach($tab_aff_acteur as $key_ac => $value_ac){
													//echo $value_ac.' '.'  '.' ';
													echo '<form method="post" name="acteur_'.$tab_aff_acteur_id[$key_ac].'_'.$i.'" action="'.$http.'catalogue/" target="_parent"><input type="hidden" name="lien_acteur" value="'.$tab_aff_acteur_id[$key_ac].'"><a href="javascript:void();" title="Voir toutes les séries avec '.$value_ac.'" class="lien_recommander" onclick="document.forms[\'acteur_'.$tab_aff_acteur_id[$key_ac].'_'.$i.'\'].submit();">'.$value_ac.' '.'  '.' '.'</a></form>';
													$i++;
												}
												echo '</p></td>';
											echo'</tr>';
						
					}

	echo '		</tbody>
		</table>';

							
	
	}else{
		$req_drama_cate = "SELECT * FROM t_dp_drama WHERE DramaID = ".$dramaID ;
		$sql_drama_cate = mysql_query($req_drama_cate);
		$cate_drama = mysql_result($sql_drama_cate,0, 'Categorie');
		$req_drama_sim ="SELECT * FROM t_dp_drama WHERE ( Categorie = '".$cate_drama."' AND StatusID = 1 )";
		$sql_drama_sim = mysql_query($req_drama_sim);
		$s = 1;
		while($row = mysql_fetch_assoc($sql_drama_sim)){
			$tab_drama_sim[$s] = $row['DramaID'];
			$s++;
		}
		$tab_drama_sim = array_unique($tab_drama_sim);
		shuffle($tab_drama_sim);
		$tab_drama_sim = array_slice($tab_drama_sim,0,5);
		
		echo '<table class="table table-same">
			<thead> 
				<tr> 
					<th colspan="2" class="top-table top-10" ><p class="p-top-10">Séries similaires</p></th>  
				</tr>
			</thead>
			<tbody>';
		

							
		foreach($tab_drama_sim as $key_s => $value_s){				
		$dramaID=$value_s;
		$drama = DramaInfo($dramaID);
		$link = dramaLinkClean($drama["titre"]);
		
		
		
		echo'<tr height="60">';
								echo'<td  align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'"><img height="50" title="drama coréen &quot;'.$drama["titre"].'&quot; en vostfr" alt="série coréenne &quot;'.$drama["titre"].'&quot; en vostfr" src="'.$drama["img_thumb"].'"  ></a></td>';
								echo'<td class="noir" align="left"><a href="'.$http.'drama/'.$dramaID.'/'.$link.'" class="lien_noir">'.$drama["titre"].'</a></td>';
							echo'</tr>';
		
	}
	echo '		</tbody>
		</table>';
		
		
	}
	
}
?>
