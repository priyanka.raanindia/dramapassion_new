<?php

	/*****************************************************************************
	Auteur du script : Gilbert Soueidy.
	Soci�t� : Neosurf Cards SAS.
	Date : 24/08/2004
	Version : 1.0
	Description :	D�finition de fonctions communes aux marchand et serveur.
	*****************************************************************************/


require_once "NeosurfConf.php";

$DBG=0;


function generateDataString($trsdata, $nbparams, $hash) {
	global $DBG;
	$data="";
	
	// Le nombre de parametres est le premier element des donn�es � g�n�rer. Il est sur 2 octets
	$data .= sprintf("%02d", $nbparams);
	// Calculer maintenant la chaine qui contient les longueurs des  diff�rentes variables
	// D'abord la longueur de la signature SHA1
	$fieldsLength = "040";

	// Maintenant les autres variables
	foreach($trsdata as $val) {
	 $fieldsLength .= sprintf("%03d", strlen($val));
	}

	// Ajouter la cha�ne contenant les longueurs des variables au r�sultat global
	$data .= $fieldsLength;

	// Maintenant generer la chaine qui contient les valeurs des variables 
	
	// D'abord la signature SHA1	
	$fieldsValues = $hash;
	// Ensuite les autres variables
	foreach($trsdata as $val) {
		if ($DBG) print "generateDataString: adding $val to data to hash<br>\n";
		$fieldsValues .= $val;
	}

	// Ajouter la cha�ne contenant les valeurs des variables au r�sultat global
	$data .= $fieldsValues;
	if ($DBG) print "generateDataString: unscrumbled data=$data<br>\n";

	// brouiller les donnees
	$scrumbledData = scrumbleData($data);	
	
	return $scrumbledData;
}



 
function parseDataString($sid, $varDefArray) {
	
	global $DBG, $NEOSURF_GENERAL_ERRORS;

	if($DBG) {
		print "\n<br>Entering parseDataString<br>\n";
		print "\n<br>sid=$sid<br>\n";
	}
	
	$tmpArray=null;
	$tmpArray['Errno']=0;
	
 	// un-brouiller l'URL
 	$data = unscrumbleData($sid);
 	
 	if($DBG) {
 		print "parseDataString: unscrumbled data: $data<br>\n";
 	} 		
	
	// Lire les deux premiers caract�res pour d�terminer le nb de variables
	$nbparams = substr($data, 0, 2);
 
	// V�rifier que l'URL contient au moins deux params
	if ( $nbparams < 2 ) {
	 $tmpArray['Errno'] = $NEOSURF_GENERAL_ERRORS['InvalidDataFormat'];
	 return $tmpArray;
	}	
	if ($DBG) {
		print "parseDataString: nb of params: $nbparams<br>\n";
	}
	
	// R�cup�rer la cha�ne contenant les valeurs des variables
	$startindex = (2 + $nbparams*3);
	$fieldsValue = substr($data, $startindex);
	
	// il faut verifier que la longueur de la chaine des donn�es correspond bien � ce qui est mis dans $nbparams

	// Faire une boucle pour lire les valeurs des variables
	$tmpTotalLength=0;

	// il faut v�rifier ici que la lecture s'arr�te � la fin de la cha�ne
	for($i=0; $i<$nbparams; $i++) {
		$tmpFieldLength = substr($data, $i*3 + 2, 3);
		$tmpFieldValue = substr($fieldsValue, $tmpTotalLength, $tmpFieldLength);
		$tmpTotalLength += $tmpFieldLength;
		
		// Affecter la valeur lue � un �l�ment du tableau des resultats
		if ($i == 0) {
			$tmpArray['merchantHash']= $tmpFieldValue;
			if ($DBG) {
				print "parseDataString: merchantHash=" .  $tmpArray['merchantHash']. "<br>\n";
			}
		} else {
			$tmpArray[$varDefArray[$i-1]]=$tmpFieldValue;
			if ($DBG) {
				print "parseDataString: " . $varDefArray[$i-1] . "=" . $tmpArray[$varDefArray[$i-1]] . "<br>\n";
			}
		}
	}
	
	// return the $tmpArray now
	return $tmpArray;	
}




function checkHash($trsdata, $merchantKey, $merchantHash) {
	
	global $DBG, $NEOSURF_GENERAL_ERRORS;
	
	if($DBG) {
		print "\n<br>Entering checkHash<br>\n";
		print "checkHash: merchantHash=$merchantHash merchantKey=$merchantKey<br>\n";
	}
	
	$errno=0;
	
	// D'abord trier le tableau
	ksort($trsdata);
	reset($trsdata);
	$tohash="";
	
	foreach($trsdata as $key=>$val) {
	 $tohash .= $val;
	}
	
	// Ajouter la clef du marchand
	$tohash .= $merchantKey;
	
	$serverHash=sha1($tohash);
	
	if($DBG) {
		print "data to hash: $tohash<br>\n";
		print "checkHash: serverHash=$serverHash<br>\n";
	}
	
	if ( strcmp($serverHash,$merchantHash) ) {
		$errno = $NEOSURF_GENERAL_ERRORS['HashError'];
	}
		
	return $errno;	
}


function computeHash($trsdata, $key) {
	
	// Trier le tableau correctement
	ksort($trsdata);
	reset($trsdata);
	
	$tohash="";
	foreach($trsdata as $val) {
	 $tohash .= $val;
	}
	
	// ajouter la clef du marchand
	$tohash .= $key;
	$hashed=sha1($tohash);
	
	return $hashed;
}


function scrumbleData($data) {
	
	$newData="";
	
	for($i=0; $i<strlen($data);$i++) {
		$newData .= chr(ord($data[$i]) + 5);	
	}
	return $newData;
}


function unscrumbleData($data) {
	$newData="";
	for($i=0; $i<strlen($data); $i++) {
		$newData .= chr(ord($data[$i]) - 5);
	}
	return $newData;
}
 
 
function checkRequestArray($inarr) {
	
	$errno=0;
	
	// check var names
	$errno=checkRequestArrayVarNames($inarr);
	if ($errno != 0) {
		return $errno;
	}
	// check var length
	$errno=checkRequestArrayVarLen($inarr);
	if ($errno != 0) {
		return $errno;
	}
	
	// Verifier que les valeurs sont bien typ�es
	$errno=checkRequestArrayVarTypes($inarr);
	if ($errno != 0) {
		return $errno;
	}
}


function checkRequestArrayVarNames($inarr) {

	global $NEOSURF_REQVAR, $NEOSURF_MISSING_REQVAR_ERRNO, $NEOSURF_GENERAL_ERRORS;

	$errno=0;

	// D'abord v�rifier que l'array pass� par le marchand ne contient pas des clefs invalides
	foreach($inarr as $key => $val) {
	 if (! in_array($key, $NEOSURF_REQVAR) ) {
		 $errno=$NEOSURF_GENERAL_ERRORS['InvalidRequestKey'];
		 break;
	 }
	}
	
	// Maintenant v�rifier que toutes les variables de la transaction sont bien pr�sentes dans  l'array pass� par le marchand
	foreach($NEOSURF_REQVAR as $val) {
	 if (!array_key_exists($val, $inarr)) {
		 $errno=$NEOSURF_MISSING_REQVAR_ERRNO[$val];
		 break;
	 }
	}
	
	return $errno;
}




 function checkRequestArrayVarLen($inarr) {

	 global $NEOSURF_MAXLEN_REQVAR, $NEOSURF_MAXLEN_REQVAR_ERRNO;
	 $errno=0;

	 foreach($inarr as $key => $val) {
		 if (strlen($val) > $NEOSURF_MAXLEN_REQVAR[$key]) {
			 $errno=$NEOSURF_MAXLEN_REQVAR_ERRNO[$key];
			 break;
		 }
	 }

	 return $errno;
 }




 function checkRequestArrayVarTypes($inarr) {

	 global $NEOSURF_TYPES_REQVAR;
	 $errno=0;
	 foreach($inarr as $key => $val) {
		 $tmperrno = checkRequestType($key, $val, $NEOSURF_TYPES_REQVAR[$key]);
		 if ( $tmperrno != 0 ) {
			 $errno=$tmperrno;
			 break;
		 }
	 }

	 return $errno;
 }


 function checkRequestType($key, $val, $type) {

	 global $NEOSURF_TYPES_REQVAR_ERRNO;

	 return ( (($type == 'N') && (!is_numeric($val))) || (($type == 'A') && (!is_string($val))) ) ?
	 	$NEOSURF_TYPES_REQVAR_ERRNO[$key] : 0;
 }


	// Les API qui v�rifient la syntaxe du tableau contenant les r�ponses.
	function checkResponseArray($inarr) {
		$errno=0;
		// check var names
		$errno=checkResponseArrayVarNames($inarr);
		if ($errno != 0) {
			return $errno;
		}
		
		// check var length
		$errno=checkResponseArrayVarLen($inarr);
		if ($errno != 0) {
			return $errno;
		}
		
		// Verifier que les valeurs sont bien typ�es
		$errno=checkResponseArrayVarTypes($inarr);
		if ($errno != 0) {
			return $errno;
		}
	}


 function checkResponseArrayVarNames($inarr) {
	 global $NEOSURF_RESVAR, $NEOSURF_MISSING_RESVAR_ERRNO, $NEOSURF_GENERAL_ERRORS;
	 $errno=0;
	 // D'abord v�rifier que l'array pass� par le marchand ne contient pas des clefs invalides
	 foreach($inarr as $key => $val) {
		 if (! in_array($key, $NEOSURF_RESVAR) ) {
			 $errno=$NEOSURF_GENERAL_ERRORS['InvalidResponseKey'];
			 break;
		 }
	 }

	 // Maintenant v�rifier que toutes les variables de la transaction sont bien pr�sentes dans  l'array pass� par le marchand
	 foreach($NEOSURF_RESVAR as $val) {
		 if (!array_key_exists($val, $inarr)) {
			 $errno=$NEOSURF_MISSING_RESVAR_ERRNO[$val];
			 break;
		 }
	 }

	 return $errno;
 }




 function checkResponseArrayVarLen($inarr) {

	 global $NEOSURF_MAXLEN_RESVAR, $NEOSURF_MAXLEN_RESVAR_ERRNO;
	 $errno=0;

	 foreach($inarr as $key => $val) {
		 if (strlen($val) > $NEOSURF_MAXLEN_RESVAR[$key]) {
			 $errno=$NEOSURF_MAXLEN_RESVAR_ERRNO[$key];
			 break;
		 }
	 }

	 return $errno;
 }




 function checkResponseArrayVarTypes($inarr) {

	 global $NEOSURF_TYPES_RESVAR;
	 $errno=0;
	 foreach($inarr as $key => $val) {
		 $tmperrno = checkResponseType($key, $val, $NEOSURF_TYPES_RESVAR[$key]);
		 if ( $tmperrno != 0 ) {
			 $errno=$tmperrno;
			 break;
		 }
	 }

	 return $errno;
 }


 function checkResponseType($key, $val, $type) {

	 global $NEOSURF_TYPES_RESVAR_ERRNO;

	 return ( (($type == 'N') && (!is_numeric($val))) || (($type == 'A') && (!is_string($val))) ) ?
	 	$NEOSURF_TYPES_RESVAR_ERRNO[$key] : 0;
 }
?>