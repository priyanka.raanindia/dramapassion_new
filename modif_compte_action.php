<?php
if(!session_id()) session_start();
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");	
	require_once("includes/fct_inc_mon_compte.php");
	$tab_user = InfoUser($_SESSION['userid']);
	$mod = cleanup($_POST['modif_type']);
	
	if($mod == "pass"){
		$titre = "Modification de votre mot de passe";
	}elseif($mod == "mail"){
		$titre = "Modification de votre e-mail";
	}elseif($mod == "annif"){
		$titre = "Modification de votre date de naissance";
	}elseif($mod == "sexe"){
		$titre = "Modification de votre sexe";
	}elseif($mod == "pays"){
		$titre = "Modification de votre pays";
	}elseif($mod == "addr"){
		$titre = "Modification de votre adresse";
	}elseif($mod == "newsletter"){
		$type_news = $_POST['modif_news'];
		if($type_news == 1){
			$titre = "Inscription à notre newsletter";
			$titre_newsletter = "Votre compte a bien été modifié pour recevoir notre newsletter";
		}else{
			$titre = "Désinscription à notre newsletter";
			$titre_newsletter = "Votre compte a bien été modifié pour ne plus recevoir notre newsletter";
		}
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<link type="text/css" href="<?php echo $http ;?>css/custom-theme/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<style type="text/css">
.ui-datepicker {
font-size:12px; 
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (Sans titre-1) -->

<form action="<?php echo $http ;?>modif_compte_action.php" METHOD="POST">
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span><?php echo $titre ; ?></span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                               
                    </td>
                </tr>
                <tr>
                	<td>
			<?php 
			
			
			if($mod == "annif"){ 
			
			$annif = cleanup($_POST['annif']);
			$time = strtotime($annif);
			$date = date("Y-m-d",$time);
			$req_annif = "UPDATE t_dp_user SET UserBirthdate ='".$date."' WHERE UserID=".$_SESSION['userid'] ;
			$sql_annif = mysql_query($req_annif);
			
			
			?>
				
                    <table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Votre date de naissance a bien été modifiée</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="javascript:void();" onclick="fermer()" class="fermer_pop" ><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table>   
					
			      		
			<?php }elseif($mod == "newsletter"){
				
				
				$req_annif = "UPDATE t_dp_user SET newsletter ='".$type_news."' WHERE UserID=".$_SESSION['userid'] ;
				$sql_annif = mysql_query($req_annif);
				
				?>
				<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;"><?php echo $titre_newsletter ; ?></p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="javascript:void();" onclick="fermer()" class="fermer_pop" ><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table>  
			
			<?php }elseif($mod == "pass"){ 
			$anc_pass = cleanup($_POST['anc_pass']);
			$new_pass = cleanup($_POST['new_pass']);
			$new_pass_conf = cleanup($_POST['new_pass_conf']);
			$anc_pass_crypt = md5($anc_pass);
			$verif_pass = true;
			
			$dif = strcmp($new_pass,$new_pass_conf);
			
			
			$rec_anc_pass = "SELECT * FROM t_dp_user WHERE ( UserID=".$_SESSION['userid']." AND UserPassword='".$anc_pass_crypt."')"  ;
			$sql_anc_pass = mysql_query($rec_anc_pass);
			$nb_rep = mysql_num_rows($sql_anc_pass);
			
			if($nb_rep == 0){
				$erreur = "Votre ancien mot de passe est incorrecte !";
				$verif_pass = false;
			}
			
			if($dif != 0 && $verif_pass == true){
				$erreur = "Les deux mot de passe entrés sont différents ! ";
				$verif_pass = false;
			}else{
				
			}
			
			if(($new_pass == "" || $new_pass_conf == "" ) && $verif_pass == true){
				$erreur = "Veuillez remplir les deux champs nouveau mot de passe !";
				$verif_pass = false;
			
			}
			
			if($verif_pass == true ){
				$pass_crypt = md5($new_pass);
				$req_pass = "UPDATE t_dp_user SET UserPassword ='".$pass_crypt."' WHERE UserID=".$_SESSION['userid'] ;
				$sql_sexe = mysql_query($req_pass);
				
				
				?>
				<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Votre mot de passe a bien été modifiée</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="javascript:void();" onclick="fermer()" class="fermer_pop" ><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table>
				
				<?php
				}else{?>
			<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;color:red;"><?php echo $erreur; ?></p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="<?php echo $http ; ?>modif_compte.php?mod=pass" class="fermer_pop" ><img src="<?php echo $http ; ?>images/modifier.png"></a>&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table>
			<?php
			
			}
			
			
			
			?>
			   
			<?php }elseif($mod == "mail"){ 
			
			$new_email = cleanup($_POST['new_email']);
			$new_email_conf = cleanup($_POST['new_email_conf']);
			$verif_mail = true;
			$mail_present = "SELECT * FROM t_dp_user WHERE UserEmail ='".$new_email."'";
			$sq_mail_present = mysql_query($mail_present);
			$nb_mail = mysql_num_rows($sq_mail_present);
			
			$dif = strcmp($new_email,$new_email_conf);
			
			if($nb_mail > 0){
				$erreur = "Cette adresse email est déjà prise !";
				$verif_mail = false;
			}
			
			if($dif != 0 && $verif_mail == true){
				$erreur = "Les deux email entrés sont différents !";
				$verif_mail = false;
			}else{
				
			}
			
			if(($new_email == "" || $new_email_conf == "" ) && $verif_mail == true){
				$erreur = "Veuillez remplir les deux champs email !";
				$verif_mail = false;
			
			}
			
			if($verif_mail == true ){
				
				//$req_mail = "UPDATE t_dp_user SET UserEmail ='".$new_email."',Active=0 WHERE UserID=".$_SESSION['userid'] ;
				//$sql_mail = mysql_query($req_mail);
				
				$emailtemp = "emails/email_verification2.htm";
				
				$req_info_user = "SELECT * FROM t_dp_user WHERE UserID=".$_SESSION['userid'] ;
				$sql_info_user = mysql_query($req_info_user);
				$now = mysql_result($sql_info_user,0,'UserAdded');
				$password = mysql_result($sql_info_user,0,'UserPassword');
				$username = mysql_result($sql_info_user,0,'UserName');
				$usermail = $new_email;
				
				
				
				$activationlink = $http."activate2.php?uh=".$password."&us=".base64_encode($now)."&m=".base64_encode($new_email)."";
				$array_content[]=array("link_placeholder", $activationlink);  
				$array_content[]=array("username", $username); 
				$array_content[]=array("useremail", $usermail); 
				 
				
		 
				mailsending::sendingemail_phpmailer($array_content, $emailtemp,"includes/class.phpmailer.php","DramaPassion.com",$emailfrom,$usermail,"Modification de votre adresse email"); 
				
				?>
				<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Un email de validation vous a été envoyé sur votre nouvelle adresse.<br /><br />Veuillez cliquer sur le lien qui s'y trouve.</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="javascript:void();" onclick="fermer()" class="fermer_pop" ><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table>
				
				<?php
				}else{?>
			<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;color:red;"><?php echo $erreur; ?></p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="<?php echo $http ; ?>modif_compte.php?mod=mail" class="fermer_pop" ><img src="<?php echo $http ; ?>images/modifier.png"></a>&nbsp;&nbsp;<a href="javascript:void();" onclick="fermer()"><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table>
			<?php
			
			} 
			 
			
			}elseif($mod == "sexe"){ 
			
			$sexe = cleanup($_POST['new_sexe']);
			
			
			$req_sexe = "UPDATE t_dp_user SET SexID ='".$sexe."' WHERE UserID=".$_SESSION['userid'] ;
			$sql_sexe = mysql_query($req_sexe);
			
			?>
			<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Votre sexe a bien été modifiée</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table> 
			
			<?php }elseif($mod == "addr"){ 
			
			
			$addr1 = cleanup($_POST['addr1']);
			$addr2 = cleanup($_POST['addr2']);
			$zip = cleanup($_POST['zip']);
			$city = cleanup($_POST['city']);
			$province = cleanup($_POST['province']);
			$new_pays = cleanup($_POST['new_pays']);
			
			
			$req_addr = "UPDATE t_dp_user SET UserAddress1 ='".$addr1."',UserAddress2 ='".$addr2."',UserZip ='".$zip."',UserCity ='".$city."',UserProvince ='".$province."',CountryID ='".$new_pays."' WHERE UserID=".$_SESSION['userid'] ;
			$sql_addr = mysql_query($req_addr);
			
			
			?>
			<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Votre adresse a bien été modifiée</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="javascript:void();" onclick="fermer()" class="fermer_pop" ><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table> 
			<?php }elseif($mod == "pays"){ 
			
			$new_pays = cleanup($_POST['pays_new']);
			$req_addr = "UPDATE t_dp_user CountryID ='".$new_pays."' WHERE UserID=".$_SESSION['userid'] ;
			$sql_addr = mysql_query($req_addr);
			?>
			<table width="600" class="texte">
                    	<tr>
                        	<td width="5" rowspan="9">&nbsp;</td>
                        	<td colspan="4"><br></td>
                        	<td width="5" rowspan="9">&nbsp;</td>                            
						</tr>                            
                        <tr>
                        	<td colspan="3" align="center" ><p style="font-size:medium;">Votre Pays a bien été modifiée</p></td></tr>
                        
                    	<tr height="20">
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	
                            <td colspan="3" align="center"><a href="javascript:void();" onclick="fermer()" class="fermer_pop" ><img src="<?php echo $http ; ?>images/fermer.jpg"></a></td>
                            
                            <td></td>
						</tr>           
                        
                    	
					</table> 
			<?php } ?>
                    <br><br>
                    </td>
                </tr>
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>
</form>
<?php if($mod == "annif"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,200);
	
</script>
<?php }elseif($mod == "newsletter" ){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,200);
	
</script>
<?php }elseif($mod == "pass"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,180);
	
</script>
<?php }elseif($mod == "mail"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,200);
	
</script>
<?php }elseif($mod == "sexe"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,200);
	
</script>
<?php }elseif($mod == "pays"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,200);
	
</script>
<?php }elseif($mod == "addr"){ ?>
<script>
				function fb_resize(w, h) {
				if (w > 0 || h > 0) {
				if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
				if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
				$.fancybox.resize();
				}
				}
				fb_resize(600,200);
	
</script>
<?php } ?>
<script>
$(function() {
	var d = new Date();
	var n = d.getFullYear();
	var dataBack = n-80;
		$( "#datepicker" ).datepicker({
            changeYear: true,
			changeMonth: true,
			yearRange: dataBack+':'+n,
			
		});
		
	});
	
	jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
});
function fermer(){
	window.parent.$.fancybox.close();
}
</script>         
</body>
</html>