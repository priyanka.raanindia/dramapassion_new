<?php
	/*****************************************************************************
	Auteur du script : Gilbert Soueidy.
	Soci�t� : Neosurf Cards SAS.
	Date : 24/08/2004
	Version : 1.0
	Description :	APIs de codage et d�codage du marchand. 
	*****************************************************************************/

require_once "includes/NeosurfConf.php";
require_once "includes/NeosurfUtils.php";
require_once "includes/NeosurfMerchantKey.php";
require_once "includes/sha1lib.class.inc.php";

$DBG=0;

function generateRequestURL($trsdata){

	global $DBG, $NEOSURF_SRV_URL, $NEOSURF_REQVAR, $NEOSURF_MERCHANT_KEY ;

	$data="";
	$errno=0;
	$result=null;
	$neosurfURL="";
	
	if ( count($trsdata) >= 1 ) {
		ksort($trsdata);
		reset($trsdata);
	}

	// Verifier ques les cl�s du tableau sont ceux d�finis dans $NEOSURF_REQVAR
	$errno=checkRequestArray($trsdata);
	if ( $errno != 0 ) {
	 $result['Errno'] = $errno;
	 return $result;
	}

	// Recupere le nombre de variables � envoyer (nbre d'�l�ments du tableau + le checksum)
	$nbparams = count($NEOSURF_REQVAR) + 1;

	// Calculer le checksum de toutes les variables; celles-ci sont concatenees par ordre alphabetique
	$merchantHash = computeHash($trsdata, $NEOSURF_MERCHANT_KEY);
	
	// Appeler la fonction de g�n�ration des donn�es cod�es
	$data = generateDataString($trsdata, $nbparams, $merchantHash);
		
	// Generer l'URL contenant les donnees codees
	$neosurfURL = $NEOSURF_SRV_URL . "?sid=" . urlencode($data);

	// Ajouter l'Id Marchand pour obtenir l'URL finale
	$neosurfURL .= "&IDMerchant=" . $trsdata['IDMerchant'];

	$result['Errno']=0;
	$result['RedirectURL']=$neosurfURL;
	
	return $result;
}

// On suppose que les data ont deja et� url-decodee (urldecode) 
function parseResponse($sid) {
	
	global $DBG, $NEOSURF_RESVAR, $NEOSURF_GENERAL_ERRORS, $NEOSURF_MERCHANT_KEY;
	
	if ($DBG) print "\n<br>Entering parseResponse<br>\n";


	// Trier le tableau de reference pour que les valeurs soient correctement copies
 	sort($NEOSURF_RESVAR);
 	reset($NEOSURF_RESVAR);
 	
 	$trsdata=null;
 	 	 	
 	// parser le sid 	
 	$trsdata = parseDataString($sid, $NEOSURF_RESVAR); 	
 	if ( $trsdata['Errno'] != 0 ) {
 		return $trsdata;
 	}
 	
 	// Supprimer les clefs merchantHash et Errno de l'array retourn�. merchantHash sera sauvegard� dans $merchantHash
 	$merchantHash = $trsdata['merchantHash'];
 	$newTrsData = $trsdata;
 	$trsdata=null;
 	foreach($newTrsData as $key=>$val) {
 		if ( strcmp($key, 'merchantHash') && strcmp($key, 'Errno') ) {
 			$trsdata[$key]=$val;
 		} 		
 	}
 	 	
 	// v�rifier le hash
 	$errno = checkHash($trsdata, $NEOSURF_MERCHANT_KEY, $merchantHash);
 	if($DBG){
 		print "parseResponse: checkHash returned $errno<br>\n";
 	}
 	
 	if ( $errno != 0) {
 		$trsdata=null;
 		$trsdata['Errno']=$errno;
 		return $trsdata;
 	}
 	
	// Every thing went ok.
	$trsdata['Errno'] = 0;

	return $trsdata;
}
?>