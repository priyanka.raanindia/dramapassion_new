<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Fonctionnement du lecteur streaming</h1>
<br>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="contenu" style="text-align:justify;">
<h3>1. Introduction</h3>
<br>
<p>Afin de garantir une qualité de visionnage optimalisée pour chacun de nos utilisateurs, Dramapassion vous propose désormais deux lecteurs différents (pour les ordinateurs Windows, Mac et Linux). La performance d'un lecteur pouvant être variable d'un ordinateur à l'autre en fonction de sa configuration matérielle et logicielle, nos utilisateurs peuvent maintenant essayer les différents lecteurs à leur disposition pour choisir le lecteur et le mode de lecture les mieux adaptés à leur ordinateur.</p>
<br>
<p>Par défaut, pour tous les lecteurs, le choix de la qualité du flux vidéo se fait automatiquement et de manière dynamique pendant toute la durée de la lecture. Bien sûr, il est également possible de sélectionner manuellement la qualité du flux.</p>
<br>
<p>Chaque fois que vous changez le type de lecteur, votre choix est enregistré automatiquement. Par conséquent, une fois que vous avez trouvé le lecteur qui vous est le mieux adapté, il ne faudra plus le changer à chaque visionnage.</p>  
<br />
<br>
<h3>2. Ordre d'essai recommandé</h3>
<br>
<p>Nous vous recommandons d'essayer les différents lecteurs et modes de lecture dans l'ordre suivant :<br />
1. Lecteur "Flash 1" en mode automatique<br />
2. Lecteur "Flash 1" en mode manuel<br />
3. Lecteur "Flash 2" en mode automatique<br />
4. Lecteur "Flash 2" en mode manuel<br />
</p>
<br>
<br />
<h3>3. Qu'est-ce que le mode automatique ?</h3>
<br>
<p>En mode automatique (activé par défaut au lancement de la vidéo), le lecteur adapte automatiquement la qualité du flux vidéo en fonction de la vitesse de connexion de l'utilisateur parmi 6 niveaux de qualité différents (4 niveaux pour les abonnés Découverte). Cette adaptation de qualité se fait en temps réel pendant le visionnage. Si votre connexion ralentit, le lecteur va automatiquement basculer vers une qualité plus faible pour éviter une coupure due au buffering. Si votre connexion accélère à nouveau, le lecteur bascule à nouveau vers des qualités supérieures pour vous proposer la meilleure qualité possible étant donné la vitesse de connexion à ce moment précis.</p>
<br>
<p>En résumé, ce système est conçu pour vous offrir la meilleure qualité du flux vidéo permise par votre connexion internet tout au long du visionnage tout en minimisant le risque d'avoir des coupures dues au buffering.</p>
<br />
<p>Le témoin "Auto" s'allume au lancement de la vidéo. Le lecteur va ensuite surveiller la vitesse de connexion en temps réel et adapter la qualité du flux vidéo en fonction de cette vitesse. Les témoins chiffrés (224p à 720p) représentent les différents niveaux de qualité possible et le témoin allumé représente le flux en cours d'utilisation.
</p>
<br />
<p>Il est possible qu'au démarrage, le lecteur commence par la qualité la plus faible pour ensuite progressivement basculer vers des qualités supérieures. Il ne faut donc pas tirer des conclusions après seulement quelques secondes de lecture. Le lecteur retient généralement la qualité la plus adaptée pour les prochains visionnages.
</p>
<br />
<br>
<h3>4. Comment faire pour utiliser le mode manuel ?</h3>
<br>
<p>Pour modifier la qualité manuellement, cliquez d'abord sur le témoin "Auto(ON)". Le témoin s'éteint et change en "Auto(OFF)". Les témoins chiffrés s'éclaircissent pour montrer que la sélection est possible. Attendez environ 10 secondes pour s'assurer que le mode automatique ait bien terminé ses opérations en cours. Cliquez ensuite sur la qualité que vous désirez fixer.<br />
La qualité enregistrée devient blanche (et clignote sur Firefox) en l'attente du basculement. Le changement du flux vidéo peut prendre quelques secondes avant de s'effectuer
et vous ne pouvez pas modifier la qualité pendant ce temps. Une fois le basculement terminé, la qualité que vous avez choisie change du blanc au jaune.</p><br />
<p>Pour retourner en mode automatique, cliquez simplement sur le témoin "Auto(OFF)".</p>
<br />
<br>
<h3>5. Note d'information pour le lecteur "Flash 2"</h3>
<br>
<p>Si vous voyez un message d'erreur de connexion, c'est parce que votre ordinateur ne permet pas la connexion sur le port 1935. Vous devez alors configurer votre ordinateur et modem/routeur pour que votre ordinateur accepte les connexions sur le port 1935.</p>
<br />
<br />
</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>