<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$mail = cleanup($_GET['email']);
	
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (Sans titre-1) -->
<?php 
?>
<form action="<?php echo $http ;?>loginAction.php" METHOD="POST">
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Validation de votre adresse email</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                            
                    </td>
                </tr>
			
			<tr><td>
				<div style="width:570px;text-align:center;margin:auto;">
				<p><br />
					Un email de validation a été envoyé à l'adresse <?php echo $email ; ?><br />
					Veuillez cliquer sur le lien contenu dans cet email pour activer votre compte.<br /><br />
					<span style="color:red;">Attention : vérifiez aussi votre dossier de courrier indésirable.<br /><br />
					
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			                                                                                                                                    
                
			
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>
</form>
 <script type="text/javascript">
 <?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
	
	if (w > 0) window.parent.$('.iframe_fancy').css({ width: w+"px"});
    if (h > 0) window.parent.$('.iframe_fancy').css({ height: h+"px"});
	
    if (w > 0) window.parent.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php }else{ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php } ?>

fb_resize(600,250);


function fermer(){
	window.parent.$.fancybox.close();
}

</script>                 
</body>
</html>