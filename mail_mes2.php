<?php
require 'includes/class.phpmailer2.php';


$message = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Dramapassion - Renouvellement automatique désactivé</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta content="width=device-width">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #DADADA; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#0E7693; font-size:22px; }

    </style>
   
</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;">

    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#DADADA" >
        <tbody>
            <tr>
                <td align="center" bgcolor="#DADADA">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>                            
                            <tr>
                                <td class="w640"  width="640" height="10"></td>
                            </tr>

                            <tr>
                                <td align="center" class="w640"  width="640" height="20"></td>
                            </tr>
                            <tr>
                                <td class="w640"  width="640" height="10"></td>
                            </tr>


                            <!-- entete -->
                            <tr class="pagetoplogo">
                                <td class="w640"  width="640">
                                    <table  class="w640"  width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#F2F0F0" >
                                        <tbody>
                                            <tr>
                                                <td class="w30"  width="30"></td>
                                                <td  class="w580"  width="580" valign="middle" align="left">
                                                    <div class="pagetoplogo-content">
                                                        <img class="w580" style="text-decoration: none; display: block; color:#476688; font-size:30px;" src="http://www.dramapassion.com/images/logo.png" alt="Logo Dramapassion" />
                                                    </div>
                                                </td> 
                                                <td class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!-- separateur horizontal -->
                            <tr>
                                <td  class="w640"  width="640" height="1" bgcolor="#ffffff"></td>
                            </tr>
                             <!-- contenu -->
                            <tr class="content">
                                <td class="w640" class="w640"  width="640" bgcolor="#ffffff">
                                    <table class="w640"  width="640" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td  class="w30"  width="30"></td>
                                                <td  class="w580"  width="580">
                                                    <!-- une zone de contenu -->
                                                    <table class="w580"  width="580" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody> 
                                                        	<tr>
                                                                <td class="w580"  width="580" height="20" ></td>
                                                            </tr>                                                           
                                                            <tr>
                                                                <td class="w580"  width="580">
                                                                    <div align="left" class="article-content">
                                                                        <p><em style="font-size:12px;">Attention : ceci est un message généré automatiquement. Ne répondez pas directement à cet email car votre message ne sera pas lu. Si vous voulez nous contacter, veuillez utiliser le formulaire de contact disponible sur notre site.</em></p><br />
                                                                        <p>'.$pseudo.',<br /><br />
                                                                        	Nous vous remercions d’avoir souscrit un abonnement sur dramapassion.com.<br />Votre paiement a bien été effectué et votre abonnement est désormais actif.<br /><br />
                                                                        	Veuillez trouver ci-dessous les informations relatives à votre paiement.<br /><br />
                                                                        	- Numéro de commande : '.$num_com.'<br />
                                                                        	- Date et heure de la transaction : '.$time.'<br />
																			- Méthode de paiement : '.$methode_pai.'<br />
																			- Montant : '.$montant.' euros<br />
																			- Abonnement : '.$abo.'<br />
																			- Date d’expiration : '.$date_expi.'<br />
																			- Renouvellement automatique : désactivé*<br /><br />
                                                                        	
                                                                        	Pour commencer à regarder les vidéos, veuillez cliquer sur le lien suivant et vous identifier : <a href="http://www.dramapassion.com">http://www.dramapassion.com</a><br /><br />
                                                                        	Si vous rencontrez des problèmes d’utilisation, nous vous invitons à consulter la page d’aide disponible à l\'adresse suivante : <a href="http://www.dramapassion.com/faq/">http://www.dramapassion.com/faq/</a><br /><br />
                                                                        	Si vous ne trouvez pas de réponse à votre question sur la page d’aide, vous pouvez nous contacter en utilisant le formulaire disponible à l\'adresse suivante : <a href="http://www.dramapassion.com/contact/">http://www.dramapassion.com/contact/</a><br /><br /><br />
                                                                        	
                                                                        	Nous vous remercions de la confiance que vous nous témoignez.<br /><br />

                                                                        	Cordialement,<br />
                                                                        	L’équipe Dramapassion.<br /><br /><br />
                                                                        	* Remarque : votre abonnement prendra fin à la date d’expiration mentionnée ci-dessus et ne sera pas renouvelé automatiquement. Vous pourrez alors souscrire à un nouvel abonnement comme vous venez de le faire.<br />Si vous souhaitez un renouvellement automatique, nous vous invitons à payer directement avec votre carte Visa ou Mastercard lors de votre prochain paiement.

                                                                        
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!-- fin zone -->                                                   

                                                   
                                                </td>
                                                <td class="w30" class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!--  separateur horizontal de 15px de  haut-->
                            <tr>
                                <td class="w640"  width="640" height="15" bgcolor="#ffffff"></td>
                            </tr>
                            <tr>
                                <td class="w640"  width="640" height="60"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
';


$mail = new PHPMailer;


$mail->From = 'noreply@dramapassion.com';
$mail->FromName = 'Dramapassion';
$mail->AddAddress($mail_client);   // Add a recipient
$mail->CharSet = "UTF-8";

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
$mail->IsHTML(true);   	                              // Set email format to HTML


$subject = 'Confirmation de votre abonnement';
$subject = utf8_decode($subject);
$subject = '=?iso8859-1?B?'.base64_encode($subject).'?=';


$mail->Subject = $subject;
$mail->Body    = $message;
$mail->AltBody = 'Attention : ceci est un message généré automatiquement. Ne répondez pas directement à cet email car votre message ne sera pas lu. Si vous voulez nous contacter, veuillez utiliser le formulaire de contact disponible sur notre site.\n\n



'.$pseudo.',\n\n

Nous vous remercions d’avoir souscrit un abonnement sur dramapassion.com.\n
Votre paiement a bien été effectué et votre abonnement est désormais actif.\n\n

Veuillez trouver ci-dessous les informations relatives à votre paiement.\n\n

- Numéro de commande : '.$num_com.'\n
- Date et heure de la transaction : '.$time.'\n
- Méthode de paiement : '.$methode_pai.'\n
- Montant : '.$montant.' euros\n
- Abonnement : '.$abo.'\n
- Date d’expiration : '.$date_expi.'\n
- Renouvellement automatique : désactivé*\n\n

Pour commencer à regarder les vidéos, veuillez cliquer sur le lien suivant et vous identifier : http://www.dramapassion.com\n\n

Si vous rencontrez des problèmes d’utilisation, nous vous invitons à consulter la page d’aide disponible à l\'adresse suivante : http://www.dramapassion.com/faq/\n\n

Si vous ne trouvez pas de réponse à votre question sur la page d’aide, vous pouvez nous contacter en utilisant le formulaire disponible à l\'adresse suivante : http://www.dramapassion.com/contact/\n\n

Nous vous remercions pour la confiance que vous nous témoignez.\n\n

Cordialement,\n
L’équipe Dramapassion.\n\n\n


* Remarque : votre abonnement prendra fin à la date d’expiration mentionnée ci-dessus et ne sera pas renouvelé automatiquement. Vous pourrez alors souscrire à un nouvel abonnement comme vous venez de le faire.\n
Si vous souhaitez un renouvellement automatique, nous vous invitons à payer directement avec votre carte Visa ou Mastercard lors de votre prochain paiement.';

if(!$mail->Send()) {
   exit;
}

?>