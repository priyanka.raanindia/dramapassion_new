<?php session_start();
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
	$dramaID = cleanup($_GET['dramaID']);
	$drama_tab = DramaInfo($dramaID);
	$epiNB = cleanup($_GET['epiNB']);
	$type = $_GET['type'];
	$abo_user = abo_user($_SESSION['userid']);
	
	
	$os = getOs();
	
	//$os = 'Android';
	
	$hashEpi = Hash_st_HD($epiNB,$dramaID,$type);
	$HashEpiConsole = Hash_st_HD_Console($epiNB,$dramaID,$type);
	$HashEpiMobile = Hash_st_HD_Mobile($epiNB,$dramaID,$type);
	$HashEpiMobileSD = Hash_free($epiNB,$dramaID);
	$HashEpiMobileIOS = Hash_epi_IOS($epiNB,$dramaID,$type);
	$titre = Titre_drama($dramaID);
	$typeUser = $_SESSION['subscription'];
	$type_abo = abo_user($_SESSION['userid']);

	
	

$epi_tab = EpiInfo($dramaID,$epiNB);
$idEpi = $epi_tab['EpiID'];


//echo $os;
//

$file = $drama_tab['shortcut'].$epiNB;
$dir = $drama_tab['shortcut'];

if($type == 'auto_hd'){
	$smil = 'hd';
}elseif($type == 'auto_sd'){
	$smil = 'sd';
}


if($type_abo == 'privilege' ){
	if($drama_tab['HD'] == 1){
		$smil = 'hd';
	}else{
		$smil = 'sd';
	}
}elseif($type_abo == 'decouverte'){
	$smil = 'sd';
}

	// Choix des serveurs videos
	require_once("includes/serverselect_hds.php");
	require_once("includes/serverselect_dir.php");


//echo $type." @ ".$type_abo." @ ".$smil ;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<?php if($os == 'Mac' || $os == 'Win' || $os == 'Linux' || $os == 'non'){
$today = gmdate("n/j/Y g:i:s A");
$initial_url = $server_wowza.'vod/_definst_/'.$dir.'/smil:'.$file.'-'.$smil.'.smil/manifest.f4m';
$ip = $_SERVER['REMOTE_ADDR'];
$key = "daHD!75H%x";
$validminutes = 180;
$str2hash = $ip.$key.$today.$validminutes;
$md5raw = md5($str2hash, true);
$base64hash = base64_encode($md5raw);
$urlsignature = "server_time=".$today."&hash_value=".$base64hash."&validminutes=$validminutes";
$base64urlsignature = base64_encode($urlsignature);
$signedurlwithvalidinterval = $initial_url . "?wmsAuthSign=$base64urlsignature"; 	
	
	
	
?>
<script type="text/javascript" src="<?php echo $http ; ?>js/flowplayer-3.2.4.min.js"></script>
<script type="text/javascript" src="<?php echo $http ; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $http ; ?>js/swfobject.js"></script>
<script type="text/javascript" src="<?php echo $http ; ?>js/ParsedQueryString.js"></script>
<script type="text/javascript">
        	function loadStrobeMediaPlayback()
        	{
	            // Collect query parameters in an object that we can
	            // forward to SWFObject:
            
	            var pqs = new ParsedQueryString();
	            var parameterNames = pqs.params(false);
	            
	            var parameters = {
	                src: "<?php echo $signedurlwithvalidinterval ;?>",
	                autoPlay: false,
	                <?php if($drama_tab['mineur'] == 16){ ?>
	                poster: "<?php echo $http ; ?>images/poster_16_ans.png",
	                <?php } ?>
					dynamicStreamBufferTime: 12,
					dvrBufferTime: 12,
					liveDynamicStreamingBufferTime: 12,
	                controlBarAutoHide: true,
	                playButtonOverlay: true,
	                showVideoInfoOverlayOnStartUp: false,
	                javascriptCallbackFunction: "onJavaScriptBridgeCreated"	
	            };
	            
	            for (var i = 0; i < parameterNames.length; i++) {
	                var parameterName = parameterNames[i];
	                parameters[parameterName] = pqs.param(parameterName) ||
	                parameters[parameterName];
	            }
	            
	          
	            
	            var wmodeValue = "direct";
	            var wmodeOptions = ["direct", "opaque", "transparent", "window"];
	            if (parameters.hasOwnProperty("wmode"))
	            {
	            	if (wmodeOptions.indexOf(parameters.wmode) >= 0)
	            	{
	            		wmodeValue = parameters.wmode;
	            	}	            	
	            	delete parameters.wmode;
	            }
	            
	            // Embed the player SWF:	            
	            swfobject.embedSWF(
					"swf/StrobeMediaPlayback.swf"
					, "StrobeMediaPlayback"
					, 852
					, 480
					, "10.1.0"
					, "swf/expressInstall.swf"
					, parameters
					, {
		                allowFullScreen: "true",
		                wmode: wmodeValue
		            }
					, {
		                name: "StrobeMediaPlayback"
		            }
				);
				
				
			}
			window.onload = loadStrobeMediaPlayback;
			
			var player = null;
			function onJavaScriptBridgeCreated(playerId)
			{
				if (player == null) {
					player = document.getElementById(playerId);
					
					// Add event listeners that will update the 
					player.addEventListener("isDynamicStreamChange", "updateDynamicStreamItems");
					player.addEventListener("switchingChange", "updateDynamicStreamItems");
					player.addEventListener("autoSwitchChange", "updateDynamicStreamItems");
					player.addEventListener("mediaSizeChange", "updateDynamicStreamItems");
				}
			}
			
			var next = 10;
			function updateDynamicStreamItems()
			{
				document.getElementById("dssc").style.display = "block";
				var dynamicStreams = player.getStreamItems();
				var ds = document.getElementById("dssc-items");
				var switchMode = player.getAutoDynamicStreamSwitch() ? "ON" : "OFF"; 
				var currentStreamIndex = player.getCurrentDynamicStreamIndex();
				var bitrates = ["224p","288p","360p","432p","540p","720p"];
				var playing = "";				
				if (currentStreamIndex == next){
					next = 10;
				}

				if (switchMode == "ON"){
					var dsItems = '<a href="#" class="bt_on" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;"><div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</div></a>';
					for (var idx = 0; idx < dynamicStreams.length; idx ++)
					{
						if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "hold"; }					
						dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
					}
					<?php if($drama_tab['HD'] == 1){?>
					if(dynamicStreams.length < 5 ){
					dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
					dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
					}
					<?php } ?>
				}
				else {
					var dsItems = '<a href="#" class="bt_off" onclick="player.setAutoDynamicStreamSwitch(!player.getAutoDynamicStreamSwitch()); return false;"><div style="width:100px;float:left;" >&nbsp;&nbsp;&nbsp;&nbsp;Auto (' + switchMode + ')</div></a>';
					if (next == 10)
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else { playing = "normal"; }
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="' + playing + '" onclick="switchDynamicStreamIndex(' + idx + '); return false;">' + bitrates[idx] + '</a>';				
						}
						<?php if($drama_tab['HD'] == 1){?>
						if(dynamicStreams.length < 5 ){
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
						}
						<?php } ?>
					}
					else
					{
						for (var idx = 0; idx < dynamicStreams.length; idx ++)
						{
							if (currentStreamIndex == idx){ playing = "playing"; } else if (next == idx) { playing = "waiting"; } else { playing = "hold"; }					
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="' + playing + '">' + bitrates[idx] + '</span>';
						}
						<?php if($drama_tab['HD'] == 1){?>
						if(dynamicStreams.length < 5 ){
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[4] + '</span>';
							dsItems += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hold" title="Réservé aux abonnés Privilège">' + bitrates[5] + '</span>';
						}
						<?php } ?>
					}
				}
				ds.innerHTML = dsItems;
			}
			
			function switchDynamicStreamIndex(index)
			{
				if (player.getAutoDynamicStreamSwitch())
				{
					player.setAutoDynamicStreamSwitch(false);	
				}
				player.switchDynamicStreamIndex(index);
				next = index;
			}
</script>
<style type="text/css">
        <!--
			.white {
				color: white;
			}
			
			.white:link{text-decoration:none;color: white;}
			.white:visited{text-decoration:none;color: white;}
			.white:hover{text-decoration:none;color: white;}
			.white:active{text-decoration:none;color: white;}
        	.normal {
				color: #969696;
			}
			.normal:link {text-decoration:none;color: #464646;}
			.normal:visited {text-decoration:none;color: #464646;}
			.normal:hover {text-decoration:none;color:white;}
			.normal:active {text-decoration:none;color:white;}			
			.waiting {
				color: white;
				text-decoration: blink;
			}					
			.waiting:hover {text-decoration:none;}
			.hold {
				color: #464646;
			}
			
			
			
			.hold:hover {text-decoration:none;}			
			.playing {
				color: #E1D275;
			}
			.playing:link {text-decoration:none;color: #E1D275;}
			.playing:visited {text-decoration:none;color: #E1D275;}
			.playing:hover {text-decoration:none;color: #E1D275;}
			.playing:active {text-decoration:none;color: #E1D275;}		
			
			.bt_on:link {text-decoration:none;color: #E1D275;}
			.bt_on:visited {text-decoration:none;color: #E1D275;}
			.bt_on:hover {text-decoration:none;color:white;}
			.bt_on:active {text-decoration:none;color: #E1D275;}	
			
			.bt_off:link {text-decoration:none;color: #464646;}
			.bt_off:visited {text-decoration:none;color: #464646;}
			.bt_off:hover {text-decoration:none;color: #E1D275;}
			.bt_off:active {text-decoration:none;color: #464646;}	
        -->
        </style>
</head>
<body>
<div width="1000" height="505">
<div style="margin-bottom:6px;margin-left:74px;margin-top:15px;border:none;">
<span class="blanc" style="text-transform:uppercase;margin-left:1px;margin-top:15px;font-size:medium;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase;font-size:medium;">Episode <?php echo preg_replace('`^[0]*`','',$epiNB) ; ?></span>
</div>
<div style="width:852px;margin-left:74px;">
<div id="StrobeMediaPlayback">
	Alternative content
</div>
<div id="dssc" style="display:none;margin-top:10px;margin-left:20px;width:980px;height:5px;font-size:12px;">
	<div style="float:left;" class="white"><a target="_blank" href="<?php echo $http ; ?>guide_lecteur" title="Plus d'info" class="white" >Qualité (info) :</a></div>
	<div id="dssc-items" style="float:left;">
		The available qualities will be loaded once the playback starts...
	</div>
</div>	
</div>
</div>
<?php }elseif($os == 'Android'){
		
		$serOs = $_SERVER['HTTP_USER_AGENT'];
		$os_android = stristr($serOs, 'Android');
		$os_android = substr($os_android, 7, 2);
		if(preg_match("#GT-P7310#", $serOs) || preg_match("#GT-P7510#", $serOs ) || preg_match("#GT-I9000#", $serOs )){
			$base_only = 1 ;
		}else{
			$base_only = 0 ;
		}
		
		
		if($os_android >= 3 && $base_only == 0){
			
			
			if($_SESSION['screen_size_x'] > '432' && $_SESSION['screen_size_y'] > '432' ){
				
			
			?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php echo $server_hd.'video/'.$dir.'/'.$file."-500.mp4"; ?>">
				</video>

				</div>
				<?php
			}elseif($_SESSION['screen_size_x'] > '360' && $_SESSION['screen_size_y'] > '360' ){
				
			
			?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php echo $server_hd.'video/'.$dir.'/'.$file."-500.mp4"; ?>">
				</video>

				</div>
				<?php
				
			}elseif($_SESSION['screen_size_x'] > '288' && $_SESSION['screen_size_y'] > '288' ){
				
			
			?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php echo $server_hd.'video/'.$dir.'/'.$file."-500.mp4"; ?>">
				</video>

				</div>
				<?php
				
				
			}else{
				?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php echo $server_hd.'video/'.$dir.'/'.$file."-500.mp4"; ?>">
				</video>

				</div>
				<?php
			}
		
			
		}else{
			if($type == 'auto_hd'){
							
				?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php echo $server_hd.'video/'.$dir.'/'.$file."-500.mp4"; ?>">
				</video>

				</div>
				<?php
			}elseif($type == 'auto_sd'){
				?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php echo $server_hd.'video/'.$dir.'/'.$file."-300.mp4"; ?>">

				</video>

				</div>
				<?php
			}
			
		}
		
	
	
 ?>
<?php }elseif($os == 'IOS'){
		
		$serOs = $_SERVER['HTTP_USER_AGENT'];
		
		if(preg_match("#iPad#", $serOs)){
			if(($_SESSION['screen_size_x'] > 1200) && ($_SESSION['screen_size_y'] > 1200) ){
		
			?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php if($server_wowza == "http://np03.dramapassion.com/"){echo $server_wowza.'mediacache/_definst_/smil:http/'.$dir.'/'.$file.'-'.$smil.'.smil/playlist.m3u8';} else {echo $server_wowza.'vod/_definst_/'.$dir.'/smil:'.$file.'-'.$smil.'.smil/playlist.m3u8';} ?>">
				</video>

				</div>
			
			<?php
			}else{
				?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php if($server_wowza == "http://np03.dramapassion.com/"){echo $server_wowza.'mediacache/_definst_/smil:http/'.$dir.'/'.$file.'-sd.smil/playlist.m3u8';} else {echo $server_wowza.'vod/_definst_/'.$dir.'/smil:'.$file.'-sd'.'.smil/playlist.m3u8';} ?>">
				</video>

				</div>
			
			<?php
			}
		}elseif(preg_match("#iPhone#", $serOs) || preg_match("#iPod#", $serOs)){
			
			
			
			
			
			
			if(($_SESSION['screen_size_x'] > 400) && ($_SESSION['screen_size_y'] > 400) ){
			
				?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php if($server_wowza == "http://np03.dramapassion.com/"){echo $server_wowza.'mediacache/_definst_/smil:http/'.$dir.'/'.$file.'-'.$smil.'.smil/playlist.m3u8';} else {echo $server_wowza.'vod/_definst_/'.$dir.'/smil:'.$file.'-'.$smil.'.smil/playlist.m3u8';} ?>">
				</video>

				</div>
				<?php
			}else{
				?>
				</head>
				<body>
				<div width="1000" height="505">
				<div style="margin-bottom:6px;margin-left:74px;">
				<span class="blanc" style="text-transform:uppercase;"><?php echo $titre ; ?> :</span> <span class="rose" style="text-transform:uppercase">Episode <?php echo $epiNB ; ?></span>
				</div>
				<div style="width:852;margin-left:74px;">
				<video width="852" height="480" controls="controls" type="video/mp4" src="<?php if($server_wowza == "http://np03.dramapassion.com/"){echo $server_wowza.'mediacache/_definst_/smil:http/'.$dir.'/'.$file.'-md.smil/playlist.m3u8';} else {echo $server_wowza.'vod/_definst_/'.$dir.'/smil:'.$file.'-md'.'.smil/playlist.m3u8'; } ?>">
				</video>

				</div>

				<?php
				
			}
		}
				
		
		
	
	
 ?>

<?php }?>
</body>
</html>
