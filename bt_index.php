<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("bt_header.php");
	require_once("bt_top.php");
	$recent = recent_drama();
	$recentPay = recent_aff_pay();
	$recentFree = recent_aff_free();
	$prochainement = prochainement();
	$top10 = AffTop10New();
	$collection = lookCollection();
	$tabCarr = Car_aff_bt();
	
	//print_r($_SESSION);
	$os = getOs();

?>


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
	  <?php
		$i = 0;
		foreach ($tabCarr as $k => $v) {
		   if($i == 0){
			  echo '<div class="item active">';
			  echo $v; 
			  echo '</div>'; 
		   } else{
			  echo '<div class="item">';
			  echo $v; 
			  echo '</div>'; 
		   }
		   $i++;
		}	
	?>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="row right like-facebook">
	<div class="col-xs-12">
		<div class="inblock block-fb"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/dramapassion" ></div></div>
		<div class="inblock block-tweet"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></div>
	</div>
</div>

<div class="row">
	<h3 class="center">Les nouveautés</h3>
	<?php 
		foreach($recent as $k => $v){
			echo '<div class="col-xs-12 col-sm-6 col-md-3 col-xl-3 img-index"><a href="'.$http.'drama/'.$v['dramaID'].'/'.$v['link'].'"><img src="'.$http.$v['img'].'" class="img-responsive" alt="Responsive image" /></a></div>';
			
		}
	?>
</div>
<?php if($prochainement != false){?>
<div class="row">
	<h3 class="center">Bientôt sur Dramapassion</h3>
	<?php 
		foreach($prochainement as $k => $v){
			if($v['link'] == 1){
				echo '<div class="col-xs-12 col-sm-6 col-md-3 col-xl-3 img-index"><a href="'.$v['link'].'"><img src="'.$v['img'].'" class="img-responsive" alt="Responsive image" /></a></div>';
			}else{
				echo '<div class="col-xs-12 col-sm-6 col-md-3 col-xl-3 img-index"><img src="'.$v['img'].'" class="img-responsive" alt="Responsive image" /></div>';
			}
			
			
		}
	?>
</div>
<?php } ?>
<div class="row">
	<h3 class="center">Que regarder</h3>
	<div class="col-xs-12 col-sm-12 col-md-4 col-xl-4">
		<table class="table ">
			<thead> 
				<tr> 
					<th colspan="2" class="top-table"><p class="p-top">Derniers Épisodes <img class="img-table" src="<?php echo $http ; ?>images/abonnement_1.png"/></p></th>  
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach($recentPay as $k => $v){
						echo "<tr>";
							echo "<td>";
								echo '<a href="'.$v['link'].'" class="lien_noir">'.$v['drama'].'</a>';
							echo "</td>";
							echo '<td class="noir right">';
								echo '<span class="new_epi ">'.$v['char'].'</span>';
							echo "</td>";
						echo "</tr>";
						
						
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-xl-4">
		<table class="table ">
			<thead> 
				<tr> 
					<th colspan="2" class="top-table top-10" ><p class="p-top-10">TOP 10</p></th>  
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach($top10 as $k => $v){
						echo "<tr>";
							echo "<td>";
								echo $k;
							echo "</td>";
							echo '<td class="noir">';
								echo '<a href="'.$v['link'].'" class="lien_noir">'.$v['name'].'</a>';
							echo "</td>";
						echo "</tr>";
						
						
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-xl-4">
		<table class="table ">
			<thead> 
				<tr> 
					<th colspan="2" class="top-table"><p class="p-top">Derniers Épisodes <img class="img-table" src="<?php echo $http ; ?>images/abonnement_2.png"/></p></th>  
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach($recentFree as $k => $v){
						echo "<tr>";
							echo "<td>";
								echo '<a href="'.$v['link'].'" class="lien_noir">'.$v['drama'].'</a>';
							echo "</td>";
							echo '<td class="noir right">';
								echo '<span class="new_epi ">'.$v['char'].'</span>';
							echo "</td>";
						echo "</tr>";
						
						
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<?php if($collection != false){?>
<div class="row">
	<h3 class="center">Collections</h3>
	<?php 
		foreach($collection as $k => $v){
			echo '<div class="col-xs-12 col-sm-6 col-md-6 col-xl-6 img-index"><a href="'.$http.'catalogue/?pub='.$v['short'].'"><img class="img-collection" src="'.$http.'images/collections/'.$v['img'].'"  alt="Responsive image" /></a></div>';	
			
		}
	?>
</div>
<?php } ?>
</div>

<?php require_once("bt_bottom.php");?>