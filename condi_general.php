<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style ="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Conditions Générales d'Utilisation et de Vente</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<p>Dernière mise à jour: le 4 juin 2012.</p>

<br>
<img src="<?php echo $http ; ?>images/ligne720.jpg">    
<br><br>	
<div class="cgv">   
<h3><a name=important>I. CONDITIONS GENERALES</a></h3>


<h4><a name=definitions>I.1. Définitions</a></h4>

<p>Les Parties conviennent de donner à chaque mot commençant par une majuscule dans les présentes Conditions Générales d'Utilisation et de Vente, la signification suivante :</p>

<p><b>Contrôle d'Accès</b> : Technique employée sur le Site pour contrôler dans quelle mesure les conditions d'autorisation et d'identification ont été remplies avant d'accorder aux Utilisateurs l'accès à certaines parties et/ou ressources du Site (par exemple, une telle condition peut être : l'inscription valable d'un Utilisateur sur le Site).</p>

<p><b>Commission Belge de la Protection de la Vie Privée</b> : Organe de contrôle indépendant chargé de veiller à la protection de la vie privée lors du traitement de données à caractère personnel. La Commission a été créée par la Chambre des Représentants de Belgique par application de la loi du 8 décembre 1992.</p>

<p><b>Cookie</b> : (aussi appelé témoin) défini par le protocole de communication http comme étant une pièce d'information envoyée par un serveur http à un client http, que ce dernier retourne lors de chaque interrogation du même serveur http.</p>

<p><b>Conditions Générales</b> : Comprend l'ensemble de cette section I.1. jusqu'à la section I.4 ci-dessous, l'ensemble des Conditions Générales d'Utilisation, la Politique de Protection des Données Personnelles (y compris « l'Opt-In » dans le cadre du Marketing Direct) et les Conditions Générales de Vente.</p>

<p><b>Conditions d'Utilisation</b> : Les conditions faisant partie intégrante des Conditions Générales et qui gouvernent l'emploi du Site pour les Utilisateurs et les Membres Enregistrés. </p>

<p><b>Conditions de Vente</b> : Les conditions publiées sur le Site pouvant être consultées et approuvées par nos Membres Enregistrés et qui régissent la procédure de commande et la partie transactionnelle de l'achat de Produits.</p>

<p><b>Directeur Générale de l'Information et de la protection de la vie privée (D.G.I.)</b> : membre de la direction de Vlexhan Distribution, responsable de la gestion des risques et de l'impact des lois régissant la protection des Données Personnelles sur les activités de Vlexhan Distribution.</p>

<p><b>Données Personnelles</b> : Toute information concernant une personne physique identifiée ou identifiable, désignée ci-après « Personne Concernée ». Est réputée identifiable, une personne qui peut être identifiée, directement ou indirectement, notamment par référence à un numéro d'identification ou à un ou plusieurs éléments spécifiques, propres à son identité physique, physiologique, psychique, économique, culturelle ou sociale.</p>

<p><b>Dramapassion.com</b> ou <b>dramapassion.com</b> : L'URL et tous ses Sites sous-jacents, liés à DramaPassion, qui sont créés et gérés par Vlexhan Distribution.</p>

<p><b>DramaPassion</b> : Les services offerts par Vlexhan Distribution aux Utilisateurs et Membres Enregistrés.</p>

<p><b>Marketing Direct</b> : Dans son sens le plus strict, le Marketing Direct est une technique de communication et de vente qui consiste à diffuser un message personnalisé et instantané vers une cible d'individus ou d'entreprises, dans le but d'obtenir une réaction immédiate et mesurable.</p>

<p><b>Membre(s) Enregistré(s)</b> : Dans la mesure où des Utilisateurs du Site veulent profiter de l'entièreté de DramaPassion et ses fonctionnalités offertes, les Utilisateurs doivent compléter notre formulaire d'enregistrement. Un Utilisateur ne deviendra donc un Membre Enregistré qu'au moment où ce formulaire aura été complété et validé.</p>

<p><b>Mon Compte</b> : Une section faisant partie de Mon Compte sur le Site et qui permettent aux Utilisateurs Enregistrés d'accéder à et éventuellement d'adapter, actualiser, ou corriger une partie des Données Personnelles appartenant aux Utilisateurs Enregistrés même.</p>

<p><b>Opt-in</b> : L'Opt-in est un terme utilisé lorsque quelqu'un reçoit la possibilité de recevoir e-mail de masse, c.-à.-d, un e-mail qui est envoyé à plusieurs personnes à la fois. Typiquement, il s'agit d'une sorte de bulletin d'information ou de publicité électronique. Concrètement l'Opt-in est obtenue aux moments qu'un Utilisateur accepte la Politique de Protection des Données Personnelles de Vlexhan Distribution.</p>

<p><b>Politique de Protection des Données Personnelles ou PPDP</b> : Politique menée par Vlexhan Distribution en matière de collection, traitement et transfert des Données Personnelles obtenues auprès des Utilisateurs et Membres Enregistrés.</p>

<p><b>Personne Concernée</b> : Voir Données Personnelles.</p>

<p><b>Produit(s)</b> : Tous les produits proposés par le Site à ses Utilisateurs, tels que les vidéos, les abonnements et les coffrets DVD.</p>

<p><b>Responsable du Traitement</b> : La personne morale qui, seule ou conjointement avec d'autres, détermine les finalités et les moyens du traitement des Données Personnelles (voir ci-après Vlexhan Distribution - Responsable du Traitement, sous le chapitre PPDP ci-dessous).</p>

<p><b>Services d'hébergement</b> : services qui gèrent des serveurs internet permettant à des organisations de distribuer du contenu sur Internet.</p>

<p><b>Service de Support</b> : Pour des questions liées au Site, les Membres Enregistrés et les Utilisateurs ont la possibilité de contacter Vlexhan Distribution en accédant à la page 'Contactez-nous'. Un formulaire y est mis à disposition grâce auquel les Utilisateurs et Membres Enregistrés peuvent nous communiquer leurs questions liées aux difficultés rencontrées dans le cadre de l'utilisation du Site ou des services offerts par Vlexhan Distribution.</p>

<p><b>Site</b> : Ce site (web) (dont l'URL commence par www.dramapassion.com) comportant aussi bien des informations (passives) que plusieurs applications interactives gérées par Vlexhan Distribution. Ce Site donne également la possibilité aux Membres Enregistrés d'accéder à un volet transactionnel, notamment l'accès et la référence à des services de paiement (p. ex. Ogone) sur d'autres sites. Ces sites-là ne sont pas gérés par Vlexhan Distribution, et sont considérés comme des Sites Liés tels que définis ci-dessous. Vlexhan Distribution utilisera les Données Personnelles recueillies en conformité avec la PPDP ci-dessous.</p>

<p><b>Sites Liés</b> : Sites dont l'apparence est similaire au Site de Vlexhan Distribution, mais dont l'hébergement et le fonctionnement sont contrôlés par des partenaires de DramaPassion ayant un lien contractuel avec Vlexhan Distribution (c.-à-d. les sites dont l'URL ne commence pas par www.dramapassion.com, p. ex. Ogone). L'utilisation des Données Personnelles par des Sites Liés sera soumise à leur propre politique de protection des données et leurs propres conditions d'utilisation, et Vlexhan Distribution rejette toute responsabilité quant à l'utilisation qui pourrait être faite de vos informations et Données Personnelles sur les Sites Liés. Vlexhan Distribution conseille aux Utilisateurs de lire la politique de protection des données et les conditions d'utilisation des Sites Liés avant d'y entrer des informations.</p>

<p><b>Utilisateur(s)</b> : Chaque Personne qui accède à la section publique du Site de Vlexhan Distribution. Lorsqu'un Utilisateur possédant un pseudo et un mot de passe utilise ceux-ci pour accéder à la section exclusivement prévue pour les Membres Enregistrés, Vlexhan Distribution considère cet Utilisateur comme un Membre Enregistré.</p>

<p><b>Vlexhan Distribution</b> : Voir « Données sur l'entreprise » (dans la section « Politique de Protection des Données Personnelles »).</p>

<br />

<h4><a name=welcome>I.2. Bienvenue</a></h4></p>

<p>Bienvenue sur le Site "www.dramapassion.com" de Vlexhan Distribution. Ce Site a été créé et est administré par Vlexhan Distribution dans le but de fournir aux Utilisateurs de ce Site et à nos Membres Enregistrés des services, des informations au sujet de notre entreprise, ainsi que d'autres initiatives prises par Vlexhan Distribution.</p>



<h4><a name=acknowledgment>I.3. Acceptation des Conditions Générales d'Utilisation et de Vente</a></h4>

<p>Pendant l'utilisation du Site, votre accord sur certaines sections des Conditions Générales d'Utilisation et de Vente peut vous être demandé. Certaines de ces sections régissent vos droits (voir ci-après) et nous recommandons aux Utilisateurs et les Membres Enregistres de lire attentivement toutes les sections avant d'accepter d'être tenu(e) par les conditions mentionnées. En utilisant, visitant ou naviguant le Site, vous acceptez d'être tenu(e) par les sections applicables. De plus, les présentes Conditions Générales d'Utilisation et de Vente constituent un contrat valable entre vous et Vlexhan Distribution.</p>

<p>Les présentes Conditions Générales d'Utilisation et de Vente sont disponibles en français uniquement. Vlexhan Distribution conservera une trace des différentes versions des Conditions Générales d'Utilisation et de Vente publiées sur le Site. La version la plus récente sera toujours publiée sur le Site et sera accessible en cliquant sur le lien « Conditions d'Utilisation et de Vente » au bas des pages du Site. La version la plus récente de nos Conditions Générales d'Utilisation et de Vente remplace toujours les versions précédentes.</p>

<p>Notre Politique de Protection des Données Personnelles détermine comment Vlexhan Distribution collecte, utilise et transfère vos Données Personnelles lorsque vous utilisez le Site de Vlexhan Distribution. Au moment où un Utilisateur choisit de devenir un Membre Enregistré, il vous sera demandé de lire et d'accepter la PPDP de Vlexhan Distribution. Si vous, en tant qu'Utilisateur, choisissez de ne pas accepter les conditions mentionnées dans la PPDP de Vlexhan Distribution, vous ne pourrez pas devenir un Membre Enregistré du Site.</p>

<p>Les Conditions Générales d'Utilisation régissent à la fois l'utilisation par des Utilisateurs et des Membres Enregistrés du Site ainsi que DramaPassion. Si vous, en tant qu'Utilisateur n'acceptez pas les Conditions Générales d'Utilisation, vous ne pouvez pas accéder à nos services et vous ne pouvez pas devenir un Membre Enregistré de notre Site.</p>

<p>Les Conditions Générales de Vente régissent les transactions d'achat de Produits proposés par le Site entre Vlexhan Distribution et les Membres Enregistrés.</p>



<h4><a name=changes>I.4. Modifications des Conditions Générales d'Utilisation et de Vente</a></h4>

<p>Ce Site et les activités de Vlexhan Distribution évoluent en permanence. Par conséquent, il est parfois nécessaire pour Vlexhan Distribution de modifier ses Conditions Générales d'Utilisation et de Vente. Vlexhan Distribution se réserve le droit de mettre à jour et de modifier les Conditions Générales d'Utilisation et de Vente à tout moment et sans information préalable. Nous vous demandons donc de réexaminer régulièrement les présentes Conditions Générales d'Utilisation et de Vente. La dernière modification de ces Conditions Générales d'Utilisation et de Vente a été effectuée le 4 juin 2010. Une utilisation du Site postérieure à des modifications ou révisions des Conditions Générales d'Utilisation et de Vente emporte votre acceptation (celui des Utilisateurs et Membres Enregistrés) des Conditions Générales d'Utilisation et de Vente telles que modifiées ou révisées.</p>



<h4><a name=governing>I.5. Droits applicables et autonomie des dispositions contractuelles</a></h4>

<p>Les présentes Conditions Générales d'Utilisation et de Vente sont régies par la loi belge et doivent être interprétées conformément à celle-ci. Tout litige né de l'utilisation du Site sera porté devant les tribunaux de Bruxelles (Belgique). Au cas où une disposition est ou devient illégale, nulle ou qu'elle ne peut être exécutée dans aucune juridiction, cela n'affectera ni la validité ou le caractère exécutoire dans cette juridiction du reste des Conditions Générales d'Utilisation et de Vente, ni la validité ou le caractère exécutoire dans d'autres juridictions de cette disposition ou de toute autre disposition. De plus, la disposition qui est devenue inexécutable sera considérée comme modifiée d'une telle manière qu'elle reflète au mieux l'intention des parties, dans les limites définies par la loi.</p>

<br />

<h3><a name=termsofuse>II. CONDITIONS GENERALES D'UTILISATION</a></h3>


<h4><a name=how>II.1. Comment fonctionne DramaPassion ?</a></h4>

<p>Vlexhan Distribution offre à ses Utilisateurs et Membres Enregistrés plusieurs types de service qui leur permettent de visionner des vidéos
en streaming ou en téléchargement : l'offre gratuite et les offres premium.</p>

<p>L'offre gratuite en streaming est accessible par tous les Utilisateurs et Membres Enregistrés et donne un accès gratuit
à une sélection de vidéos disponibles dans le catalogue du Site. L'offre gratuite propose des vidéos encodées à une qualité
inférieure que celle utilisée pour les offres premium. Contrairement aux offres premium, chaque épisode est coupée en plusieurs
parties. Le visionnage des vidéos peut être précédé par l'affichage d'une publicité vidéo que l'Utilisateur doit regarder
dans son entièreté pour pouvoir lancer la vidéo qu'il souhaite regarder. Une publicité vidéo peut aussi être affichée à la fin d'une vidéo,
dans quel cas l'Utilisateur n'est pas tenu de la regarder dans son entièreté. Vlexhan Distribution se réserve le droit
de décider et de modifier à tout moment et sans information préalable le type et la fréquence
des publicités vidéos pour l'offre gratuite proposée sur le Site.</p>

<p> Le Membre Enregistré peut aussi souscrire aux offres premium payantes proposées sur le Site.
Les offres premium se composent de deux types d'abonnement : Découverte et Premium. Chaque type d'abonnement
donne accès au catalogue de vidéos du Site sous des conditions d'accès spécifiques à chaque type d'abonnement.
Les prix et les conditions d'accès liées à chaque type d'abonnement sont clairement exposées sur la page
<a href="<?php echo $http;?>premium/" class="lien_bleu"><?php echo $http;?>premium/</a> et des informations complémentaires
sont fournies à la page <a href="<?php echo $http;?>aide-faq/2" class="lien_bleu"><?php echo $http;?>aide-faq/2</a>.
Pendant la période de l'abonnement,le Membre Enregistré peut visionner de manière illimitée les vidéos du catalogue du Site,
le cas échéant, avec des restrictions liées au type d'abonnement auquel le Membre Enregistré a souscrit.
Lorsque le paiement est effectué avec une carte Visa ou Mastercard, la reconduction automatique de l'abonnement est activé par défaut.
Le Membre Enregistré a toutefois la possibilité d'annuler la reconduction automatique
à tout moment dans la section « Mon Compte ».</p>

<p>Dans le cas du visionnage en streaming, le temps nécessaire avant le démarrage effectif d'une vidéo dépend
d'un certain nombre de facteurs tels que la largeur de la bande passante disponible à ce moment-là, la vidéo choisie
et les capacités techniques de votre ordinateur ou les paramètres de configuration. Par exemple, des scènes d'action
à grande vitesse ou des effets spéciaux au début d'une vidéo peuvent légèrement en ralentir le lancement. De plus,
il est indispensable que le Membre Enregistré reste connecté à internet jusqu'à ce que l'entièreté de la vidéo
soit transmise à l'ordinateur du Membre Enregistré.</p>

<p>Quant au téléchargement, la vitesse de téléchargement peut varier en fonction de la largeur de la bande bassante
de la connexion internet du Membre Enregistré et le niveau de saturation du serveur. La lecture du fichier téléchargé
requiert le logiciel Adobe Media Player ainsi que le lecteur Flash version 10.1 ou supérieure.
Les fichiers en téléchargement sont protégés par le système de protection Flash Access 2 et requiert une identification
lors de la première ouverture du fichier ainsi qu'après une reconduction automatique ou manuelle de l'abonnement.
Lors de l'identification, l'ordinateur du Membre Enregistré doit être connecté à internet.</p>

<p>En aucun cas, Vlexhan Distribution ne peut être tenu responsable d'un problème de visionnage lié
à une configuration hardware, software ou de connexion internet spécifiques à l'ordinateur du Membre Enregistré.</p>

<p>LES MEMBRES ENREGISTRES NE PEUVENT REGARDER LES VIDEOS QU'EN BELGIQUE, EN FRANCE,
AU LUXEMBOURG, EN SUISSE, AUX PAYS-BAS ET AU ROYAUME-UNI. EN RAISON DES RESTRICTIONS LIEES AUX LICENCES QUI NOUS SONT ACCORDEES,
LES MEMBRES ENREGISTRES NE PEUVENT PAS REGARDER LES VIDEOS EN DEHORS DE CES PAYS.
VLEXHAN DISTRIBUTION SE RÉSERVE LE DROIT D'EMPLOYER CERTAINES TECHNOLOGIES AFIN DE VERIFIER QUE VOUS,
EN TANT QUE MEMBRE ENREGISTRE OU UTILISATEUR, VOUS CONFORMEZ A CETTE REGLE, TOUT EN RESPECTANT NOTRE POLITIQUE DE PROTECTION
DES DONNEES PERSONNELLES. A LA DEMANDE DES FOURNISSEURS DE CONTENU, EN RAISON DE LIMITATIONS TECHNIQUES OU
POUR QUELQUE AUTRE RAISON LAISSEE A NOTRE ENTIERE DISCRETION, CERTAINS OU L'ENSEMBLE DES VIDEOS NE POURRAIENT
ETRE VISIONNES, OU CESSERAIENT D'ETRE VISIONNABLES, POUR TOUS NOS MEMBRES ENREGISTRES ET UTILISATEURS
SANS NOTIFICATION PREALABLE.</p>

<p>Vlexhan Distribution se réserve le droit, à son entière discrétion, d'apporter, de temps à autre,
les changements nécessaires à la gestion de ses affaires sans en avertir les Utilisateurs ou Membres Enregistrés préalablement.
La description de DramaPassion ne doit en aucun cas être interprétée comme une garantie ou une obligation
quant à la manière dont DramaPassion devrait fonctionner. Des changements à DramaPassion sont apportés constamment
et ces changements pourraient ne pas être toujours entièrement couverts dans ces Conditions Générales d'Utilisation et de Vente.</p>



<h4><a name=free>II.2. Vidéos tests, contraintes techniques et contraintes du dispositif</a></h4>

<p>Nous encourageons les Utilisateurs et les Membres Enregistrés à découvrir DramaPassion en visionnant les vidéos test qui se trouvent sur la page de présentation de nos offres premium.
Veuillez noter que pour regarder les vidéos sur votre ordinateur, votre dispositif doit répondre à certaines contraintes techniques.
Les vidéos tests donnent l'occasion aux Membres Enregistrés de contrôler si votre dispositif répond à ces contraintes techniques.
Vlexhan Distribution ne prend aucune responsabilité quant aux pré-requis en matière de résilience,
de qualité ou de bande passante nécessaires pour vous garantir la meilleure expérience de visionnage.</p>



<h4><a name=access>II.3. Contrôle d'Accès</a></h4>

<p>Pour augmenter l'accessibilité du Site de Vlexhan Distribution, celui-ci peut placer un Cookie sur chaque ordinateur qu'un Utilisateur ou un Membre Enregistré emploie pour accéder au Site.
Lorsque vous visitez une nouvelle fois le Site de Vlexhan Distribution, ce Cookie nous permettra de vous reconnaître
en tant que Membre Enregistré et vous permettra d'obtenir un accès direct à notre Site sans devoir entrer
votre mot de passe ou d'autres données d'identification.</p>

<p>Les Membres Enregistrés sont tenus de nous donner des informations correctes liées à votre compte,
ainsi que de mettre les informations de vos comptes à jour. </p>

<p>Comme Membre Enregistrés vous êtes aussi tenus de prendre toutes les précautions nécessaires
pour que votre pseudo et votre mot de passe restent secrets et de faire en sorte que l'accès
à votre ordinateur ou à votre portable soit limité.
Si vous divulguez votre mot de passe ou partagez votre compte en tant qu'un Membre Enregistré
avec une tierce personne, vous serez tenu responsable de vos actes ainsi
que des actes de cette tierce personne. Les utilisateurs d'ordinateurs publics
ou des téléphones portables non protégés doivent se déconnecter à la fin de chaque utilisation du Site.</p>

<p>Si, en tant que Membre Enregistrés, vous êtes victime d'un vol d'identité et des données de votre compte,
vous êtes tenus d'avertir Vlexhan Distribution immédiatement par e-mail.</p>

<p>Vlexhan Distribution se réserve le droit de mettre chaque compte 'en suspens' avec ou sans notification
au Membre Enregistré concerné, pour se protéger de toute activité que Vlexhan Distribution estime frauduleuse.
Vlexhan Distribution n'est pas obligé de créditer ou donner une remise aux Membres Enregistres
dont le compte a été mis 'en suspens' par Vlexhan Distribution ou par une procédure automatisée gérée par le Site.</p>



<h4><a name=member>II.4. Commentaires des Membres Enregistrés </a></h4>

<p>Vous, en tant que Membre Enregistré, avez le droit d'écrire et de publier vos commentaires
sur les vidéos qui sont disponibles sur le Site, dans l'espace réservé à cet effet.
Ces commentaires sont contrôlés régulièrement par Vlexhan Distribution et Vlexhan Distribution
se réserve le droit de rejeter, d'enlever ou d'éditer chaque commentaire à tout moment et cela,
sans avertissement préalable.</p>

<p>Les commentaires que vous, en tant que Membre Enregistré, décidez de publier sur le Site de Vlexhan Distribution
ne doivent en aucun cas inclure un contenu cru ou dur, ou qui inclut un langage discriminatoire, du contenu illégal,
obscène, menaçant, diffamatoire ou autrement répréhensible ; des URLs, des numéros de téléphone, des adresses e-mail,
des attaques personnelles à l'encontre des acteurs ou de toute autre personne impliquée dans la création
et le développement de la série, des informations fallacieuses concernant l'origine du contenu ;
une discussion sur les politiques ou les services de Vlexhan Distribution ; ou des propos encourageant la mise en ligne
et l'utilisation de moyens illégaux pour le visionnage des vidéos protégées par les droits d'auteur.</p>

<p>En soumettant vos commentaires, vous en tant que Membre Enregistré nous autorisez à les publier
sur le Site, accompagnés de votre pseudo comme Membre Enregistré.</p>



<h4><a name=limitations>II.5. Droit d'utilisation limité</a></h4>

<p>Nonobstant toute clause contraire, le contenu disponible sur le Site, y compris le contenu distribué
par Vlexhan Distribution sous forme de flux continu (streaming), ne peut être utilisé par des Membres Enregistrés
qu'à titre personnel et dans un but non commercial. Le droit d'accès au Site vous est accordé uniquement pour cette utilisation limitée. </p>

<p>Un Membre Enregistré n'a pas le droit de télécharger (autrement que dans la mémoire cache lorsque
c'est nécessaire pour votre utilisation personnelle dans les limites géographiques mentionnées
à l'article II.1 ci-dessus, ou dans les autres conditions décrites dans les Conditions d'Utilisation),
modifier, copier, distribuer, transmettre, montrer, exécuter, reproduire, dupliquer, publier, éditer,
licencier, créer des œuvres dérivées à partir du contenu obtenu sur ou à partir du Site
ou proposer à la vente les informations disponibles sur le Site sans notre accord écrit préalable.</p>

<p>Vous n'avez pas le droit d'encadrer ou d'utiliser une technique d'encadrement
qui vous permet de joindre nos marques déposées, le logo ou toute autre information
protégée par des droits de propriété intellectuelle et appartenant à Vlexhan Distribution.
Vous n'avez pas non plus le droit d'employer des 'metatags' ou aucun autre 'texte caché'
qui emploie DramaPassion ou sa marque déposée sans l'accord écrit de Vlexhan Disrtibution.
Toute utilisation non autorisée du Site de Vlexhan Distribution ou des informations disponibles
sur le Site mettra fin à votre droit d'utilisation limité.</p>



<h4><a name=support>II.6. Service de Support</a></h4>

<p>Si vous avez besoin d'aide, vous avez la possibilité de consulter notre page "Aide - FAQ" ou
vous pouvez contacter notre Service de Support (voir définition). </p>



<h4><a name=service>II.7. Tests des services</a></h4>

<p>Il se peut que nous fassions des tests sur certains aspects de nos services.
Vlexhan Distribution se réserve le droit de vous inclure ou de vous exclure de ces tests sans notification préalable.</p>



<h4><a name=cancellation>II.8. Annulation</a></h4>

<p>A tout moment, vous avez le droit d'annuler votre inscription au Site. L'annulation sera immédiate.
Cependant, Vlexhan Distribution ne procédera à aucun remboursement quelle que soit la raison de l'annulation.
Pour annuler votre inscription en tant que Membre Enregistré, veuillez nous envoyer un message
en utilisant le formulaire disponible sur la page « Contactez-nous » en y mentionnant « Annulation d'inscription »
ainsi que votre mot de passe et votre pseudo.</p>



<h4><a name=right>II.9. Résiliation & limitations d'accès</a></h4>

<p>Vlexhan Distribution se réserve le droit de mettre fin ou de limiter l'accès à DramaPassion,
et ceci sans notification préalable envers des Membres Enregistrés pour toute raison
(par ex. suite au non-respect des Conditions Générales d'Utilisation et de Vente) ainsi que sans raison.</p>



<h4><a name=intellectual>II.10. Propriété intellectuelle</a></h4>


<p><a>Actions pour atteinte aux droits d'auteurs.</a><br />

VLEXHAN DISTRIBUTION RESPECTE LES DROITS DE PROPRIETE INTELLECTUELLE QUI LUI SONT ATTRIBUÉS
ET RESPECTE AUSSI LES DROITS DE PROPRIETE INTELLECTUELLE ACCORDÉS À DES TIERS.
VLEXHAN DISTRIBUTION NE PROMEUT, N'ENCOURAGE NI NE TOLÈRE LA COPIE DE CONTENU DIGITAL
OU TOUTE AUTRE CONDUITE VIOLANT CES DROITS. SI VOUS CONNAISSEZ QUELQU'UN QUI UTILISE
SANS AUTORISATION LE CONTENU DU SITE DE VLEXHAN DISTRIBUTION OU QUI DISTRIBUE LE MÊME CONTENU
SUR D'AUTRES SITES WEB, VEUILLEZ NOUS EN INFORMER EN UTILISANT LE LIEN « CONTACTEZ-NOUS »
AU BAS DES PAGES DU SITE DE VLEXHAN DISTRIBUTION EN INCLUANT LES INFORMATIONS SUIVANTES :
(I) LE NOM DE L'ŒUVRE PROTÉGÉE AUX DROITS D'AUTEURS DE LAQUELLE VOUS ALLEGUEZ
QU'UNE INFRACTION A ÉTÉ COMMISE AINSI QUE L'INFORMATION NÉCESSAIRE 
POUR NOUS PERMETTRE DE LOCALISER CETTE OEUVRE;
(II) LE NOM CORRESPONDANT DE L'ŒUVRE SUR LE SITE DE VLEXHAN DISTRIBUTION QUI NE RESPECTERAIT PAS CES DROITS;
(III) L'INFORMATION NÉCESSAIRE NOUS PERMETTANT DE VOUS CONTACTER TELLE QUE VOTRE ADRESSE ET VOTRE NUMÉRO DE TELEPHONE.</p>

<p><a><b>Marques déposées</b></a><br />

"DramaPassion" est une marque déposée appartenant à Vlexhan Distribution.
Le logo et le nom de DramaPassion sont des marques déposées de Vlexhan Distribution.
Les marques déposées ne peuvent être employées ou reproduites qu'avec l'autorisation
écrite de Vlexhan Distribution. Elles ne peuvent pas être associées à des produits
ou des services qui ne sont pas liés à dramapassion.com, ou employées d'une manière qui prêterait
à confusion pour nos Utilisateurs et Membres Enregistrés ou qui déprécierait ou discréditerait
les droits de Vlexhan Distribution. Les autres marques mentionnées sur le Site appartiennent à leurs propriétaires respectifs,
qui peuvent être ou ne pas être affiliés ou associés à, ou sponsorisés par Vlexhan Distribution.
Les images de personnes ou de personnalités affichées sur le Site n'indiquent en rien
le soutien de ces personnes ou personnalités à Vlexhan Distribution ou à d'autres produits
en particulier ou à DramaPassion, sauf s'il en est clairement fait mention.</p>


<p><a><b>Divertissement et contenu du Site</b></a><br />

Vlexhan Distribution distribue des divertissements filmés et Vlexhan Distribution se réserve le droit d'afficher et de promouvoir,
à notre entière discrétion, ces divertissements filmés ou d'autres informations sur notre Site avec les moyens de notre choix.
Par ailleurs, notre Site vous permet ainsi qu'à des tiers, d'afficher des critiques ou commentaires en rapport
aux divertissements que nous distribuons. Les opinions, avis, déclarations, services, offres, ou autres informations
qui font parties du contenu exprimé ou mis à la disposition par des tiers sur notre Site,
appartiennent à leurs auteurs ou producteurs respectifs et non à Vlexhan Distribution.
En aucun cas Vlexhan Distribution ou ses actionnaires, administrateurs,
cadres ou employés ne peuvent être tenus responsables pour les pertes et/ou dommages
causés par la confiance que vous avez accordée à l'information obtenue sur notre Site.
Il est de votre responsabilité d'évaluer les informations, avis, ou tout autre contenu disponible sur le Site.</p>



<h4><a name=links>II.11. Liens et pages</a></h4>

<p>D'autres sites peuvent afficher un lien hypertexte vers notre Site ou notre Site peut afficher des liens hypertextes
vers d'autres sites ou entreprises, y compris les entreprises avec lesquelles nous sommes associés.
Vlexhan Distribution ne vérifie pas ces sites et n'est donc pas responsable des informations
qui s'y trouvent, ni de leur contenu ou de leur politique de traitement des données personnelles.
Les Utilisateurs reconnaissent et conviennent que Vlexhan Distribution n'est pas responsable
et ni ne se porte garant des actions de tierces personnes ou de leurs produits ou du contenu de ces sites.
Veuillez donc attentivement lire les conditions d'utilisations de ces sites.</p>



<h4><a name=deep>II.12. Liens Profonds</a></h4>

<p>Nonobstant toute clause contraire qui vous l'autorise spécifiquement,
vous en tant que Utilisateurs ou Membre Enregistré n'êtes pas autorisé à créer
des « liens profonds » vers notre Site, ce qui signifie que vous ne pouvez pas
créer, afficher, publier ou distribuer des liens vers d'autres pages que les pages du Site
accessibles à tous.</p>



<h4><a name=disclaimers>II.13. Limitation de garantie et de responsabilité</a></h4>

<p>DRAMAPASSION, EN CE COMPRIS LES FONCTIONALITES ASSOCIEES A CELUI-CI,
LE SITE DE VLEXHAN DISTRIBUTION ET SON CONTENU, SONT FOURNIS « TELS QUELS »
ET « SELON LEUR DISPONIBILITE » AVEC TOUS LES DEFAUTS ET SANS AUCUNE GARANTIE.
VLEXHAN DISTRIBUTION NE GARANTIT PAS UNE UTILISATION ININTERROMPUE
OU SANS ERREUR DE DRAMAPASSION. VOUS ACCEPTEZ QUE VLEXHAN DISTRIBUTION PEUT ELIMINER
OU MODIFIER A TOUT MOMENT TOUT OU PARTIE DE DRAMAPASSION, SANS PREVENIR LES UTILISATEURS
OU LES MEMBRES ENREGISTRES. Sans préjudice de ce qui précède, Vlexhan Distribution n'assume pas
de responsabilité pour les éléments suivants :
(i) erreurs et omissions dans le contenu fourni sur le Site de Vlexhan Disribution ;
(ii) recommandations ou conseil du Service de Support ;
(iii) manquements ou interruptions dans la disponibilité du Site de Vlexhan Distribution,
en ce compris la disponibilité des vidéos ;
(iv) la livraison et/ou l'affichage du contenu sur le Site de Vlexhan Distribution ;
et (v) tous les pertes ou dommages engendrés par l'utilisation du contenu fourni sur notre Site.
DANS LA MESURE PERMISE PAR LA LOI, VLEXHAN DISTRIBUTION ET NOS AYANT DROITS N'ACCORDONS AUCUNE GARANTIE,
EN CE COMPRIS, PAR EXEMPLE, LES GARANTIES DE LA QUALITE MARCHANDE ET D'UNE QUALITE SATISFAISANTE,
AINSI QUE DE NON ATTEINTE AUX DROITS DES TIERS.
Par ailleurs, Vlexhan Distribution ne garantit pas que l'information accessible sur le Site de Vlexhan Distribution
est précise, complète et mise à jour. Vlexhan Distribution ne donne pas de garanties sur le contenu des vidéos diffusées
sur le Site de Vlexhan Distribution ou sur la description de ce contenu.
Vlexhan Distribution ne garantit pas que l'utilisation du Site de Vlexhan Distribution soit dépourvue d'interruption,
perte de données, corruption de fichiers, virus, hacking ou d'autres interventions et Vlexhan Distribution
décline toute responsabilité pour ce type de dommages.
L'information orale ou écrite, ainsi que les avis que nous donnons,
ne donne pas lieu à une garantie engageant Vlexhan Distribution.</p>

<p>En aucun cas, Vlexhan Distribution, ses actionnaires, administrateurs, cadres ou employés
ne peuvent être tenus responsables (conjointement ou séparément) envers vous des dommages,
indirects, ou des dommages résultant de la perte d'utilisation, de données ou de bénéfices,
d'interruption de travail ou de dégâts ou pertes commerciales, que nous ayons été prévenus
ou non de la possibilité de survenance de tels dommages, résultant de ou lié à l'utilisation
ou la performance du Site de Vlexhan Distribution, de son contenu, de ses caractéristiques
ou fonctionnalités associées à celle-ci. Dans aucun cas, notre responsabilité totale
pour tous les dommages et les pertes résultants de l'utilisation ou l'impossibilité d'utiliser
le Site de Vlexhan Distribution, son contenu ou les caractéristiques du Site de Vlexhan Distribution
ou les fonctionnalités en rapport avec celui-ci ne peut excéder le montant du dernier abonnement acheté,
sauf si le droit applicable l'impose en cas de lésions corporelles.</p>



<br />

<h3><a name=personal>III. POLITIQUE DE PROTECTION DES DONNEES PERSONNELLES (CI-APRES PPDP)</a></h3>
<p>La PPDP fait partie intégrante des Conditions Générales du Site de Vlexhan Distribution.
Par conséquent, l'acceptation des Conditions Générales implique aussi l'acceptation de la PPDP.
Cliquez sur le lien suivant pour accéder à la page de notre PPDP:<br />

<a href="<?php echo $http; ?>protection-des-donnees-personnelles/" class="lien_bleu"><?php echo $http; ?>protection-des-donnees-personnelles/</a></p>



<br />

<h3><a name=termsofsale>IV. CONDITIONS GENERALES DE VENTE</a></h3>


<h4><a name=order>IV.1. Commande</a></h4>

<p>A chaque instant les Membres Enregistrés peuvent souscrire à un abonnement sur notre Site
(sauf dans le cas d'une notification de temps d'arrêt planifié ou non planifié).
Les Membres Enregistrés peuvent également commander un coffret DVD mis en vente sur le Site.
La vente sera conclue une fois que vous acceptez les Conditions Générales de Vente,
et que Vlexhan Distribution a reçu la confirmation de paiement.</p>



<h4><a name=status>IV.2. Etat de la commande</a></h4>

<p>Dès que vous avez passé commande en tant que Membre Enregistré, le service de paiement (p. ex. Ogone ou Paypal)
vous enverra un e-mail personnalisé comprenant votre numéro d'identification de commande dans un délai maximal d'un jour ouvrable.
Dans le cas d'un abonnement, le compte du Membre Enregistré est adapté au moment de la confirmation du paiement.
Dans le cas d'une commande de coffret DVD, un email de confirmation vous est envoyé par Vlexhan Distribution.
Ensuite, un second email vous est envoyé lorsque votre colis a été expédié.
Vlexhan Distribution se réserve le droit d'annuler votre commande.</p>


<h4><a name=credits>IV.3. Crédits</a></h4>

<p>L'achat des crédits n'est plus possible à partir du 4 juin 2012. Si vous avez encore des crédits non utilisés que vous avez
acheté par le passé, vous avez la possibilité de les utiliser pour les convertir en jours d'abonnement.</p>



<h4><a name=subscription>IV.4. Abonnement</a>
<?php
    if($paques == 1){
		$dbQpaques = "SELECT * FROM paques WHERE id = 25";
		$sqlQpaques = mysql_query($dbQpaques);
		$imgPaques = "images/".mysql_result($sqlQpaques,0,"name").".png";
		$idPaques = mysql_result($sqlQpaques,0,"name");
		$dateshow = strtotime(mysql_result($sqlQpaques,0,"dateshow"));
		if(time()>$dateshow || $_SESSION["userid"] == 3){	
			$oeuf = '<img src="'.$http.$imgPaques.'" id="'.$idPaques.'" style="height:14px;">';
			echo $oeuf;
		}
	}    
       ?>
</h4>

<p>Le début de la période d'abonnement, 7 jours, 1 mois, 3 mois ou 6 mois selon la formule d'abonnement,
prend cours immédiatement après l'acceptation du paiement.
Veuillez noter que Vlexhan Distribution n'offre pas de remboursement pour toute période d'abonnement
non utilisée si l'inscription d'un Membre Enregistré prend fin pour une raison ou pour une autre.
La reconduction de l'abonnement est tacite et automatique lorsque le paiement est effectué par Visa, Mastercard.
Le Membre Enregistré peut à tout instant annuler la reconduction automatique de son abonnement dans la section « Mon Compte ».</p>


<h4><a name=subscription>IV.5. Coffret DVD</a></h4>

<p>Le produit commandé est expédié généralement endéans les 48 heures après l'acceptation du paiement.
Le délai de livraison peut varier d'une région à l'autre et la période de l'année.
La livraison est prise en charge par La Poste (Belgique) et requiert la signature du destinataire au moment de la livraison.
Vlexhan Distribution ne peut en aucun cas être tenu responsable des délais de livraison non respectés.
Si le colis a été endommagé durant la livraison, Vlexhan Distribution s'engage à le remplacer par un nouveau produit et
supporter les coûts de livraison liés à l'échange seulement s'il existe une preuve irréfutable que le colis n'a pas été endommagé
par une faute du Membre Enregistré. Dans le cas d'un retour ou une échange pour toute autre raison,
le Membre Enregistré supportera les charges liées au retour du colis.</p>


<h4><a name=payment>IV.6. Paiement</a></h4>

<p>Les Membres Enregistrés peuvent souscrire à un abonnement ou commander un coffret DVD en utilisant
une carte de crédit (par exemple, Visa ou MasterCard), une carte de débit (par exemple, Bancontact),
un compte PayPal, une carte prépayée Neosurf ou par SMS pour certains produits.
A chaque instant, et à notre entière discrétion, Vlexhan Distribution peut offrir des abonnements gratuits,
des remboursements, réductions ou autres (« gratuités ») à certains de nos Membres Enregistrés
ou à tous nos Membres Enregistrés. Le montant et la forme de ces gratuités et la décision de les donner dépendent
entièrement de la volonté de Vlexhan Distribution. L'offre de gratuités dans un cas ne donne pas droit
à des gratuités dans le futur ou dans des cas similaires, et ne nous oblige en rien à offrir
des gratuités dans le futur, quelles que soient les circonstances.
Si la validité de votre carte de crédit ou sa date d'échéance change,
vous pouvez modifier les informations liées à votre carte de crédit chaque fois que vous placez une nouvelle commande.</p>

<p>Dans la section « Mon Compte », une rubrique « mes transactions » sera bientôt mis à disposition des Membres Enregistrés
où vous pourrez trouver un aperçu de tous vos paiements lors des derniers trois mois.
Si vous voulez recevoir une liste reprenant vos paiements antérieures aux trois mois prévus,
vous pouvez nous envoyer un e-mail au départ de notre page « Contactez-nous ». Des frais additionnels peuvent être dus.</p>



<br /><br />

<a class="lien_bleu" href=#>Retour au sommet de la page</a>

<br /><br /></div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>