<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	
$ip_code = getIp2Location($_SERVER['REMOTE_ADDR']);
$rescountry = mysql_query("SELECT *, CountryName".$_SESSION['language']." AS CountryName FROM t_dp_countryship ORDER BY CountryName ASC");
$countries = mysql_num_rows($rescountry);


?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo $http ;?>css/my_style.css" rel="stylesheet" media="all" type="text/css"> 
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: white;">  
	<div id="fb-root"></div>
  <script type="text/javascript">
// This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    //console.log('statusChangeCallback');
    //console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      UserConnect();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
  
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.

    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '344619609607',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.


  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/fr_FR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function UserConnect() {
    FB.api('/me', function(response) {
	    facebookID = response.id;
	    FacebookEmail = response.email;
	    FacebookUserName = response.first_name;
		urlJson = "<?php echo $http ; ?>_tempFacebook.php?i="+facebookID+"&u="+FacebookUserName+"&e="+FacebookEmail ; 
		//console.log(urlJson);
		
		var jqxhr = $.getJSON( urlJson, function() {
		  
		})
		  .done(function(data) {
		    	window.location.href = "<?php echo $http ; ?>loginFacebook.php?userID="+data.userid;

		  })
		  .fail(function() {
		    
		  });
		  

	}); 
      
      
  }

</script>    
<form method="post" action="registerAction.php" name="registerform" id="registerform">
<center>
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Inscription</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                                 
                    </td>
                </tr>
                <tr>
                	<td>
                    <table width="600" class="texte">
                    	  <?php if($_SESSION['erreur_inscription'] != ""){ ?>
						<tr>
							<td colspan="4"><span style="color:red;"><?php echo $_SESSION['erreur_inscription'] ; ?></span></td>
						</tr>
					<?php } ?>                    
                    	<tr>
                        	<td width="375" valign="top" colspan="2"><b style="text-transform:uppercase">Compte DramaPassion</b></td>
                        	<td width="25"></td>
                        	<td width="200" valign="top"><b style="text-transform:uppercase">Inscription via facebook</b></td>
						</tr>     
						<tr>
							<td colspan="4"></td>
						</tr>
					
                        <tr>
                        	<td>Pseudo</td>
                            <td><input tabindex="1" type="text" name="regusername" class="form" style="width:175px; height:27" value="<?php if(isset($_SESSION['regusername'])){ echo $_SESSION['regusername'];} ?>" <?php if(isset($_SESSION['error']['regusername'])){ echo " class='errorbackground'";} ?>></td>
							
                        	<td width="25" valign="top" style="background-image:url(<?php echo $http ; ?>images/pop_barre.jpg); background-repeat:repeat-y" rowspan="10" align="middle"></td>                            
                            <td style="text-align: center;"><fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button></td>
						</tr> 
                        <tr>
                        	<td>Mot de passe</td>
                            <td><input tabindex="2" type="password" name="regpassword" class="form" style="width:175px; height:27" value="<?php if(isset($_SESSION['regpassword'])){ echo $_SESSION['regpassword'];} ?>" <?php if(isset($_SESSION['error']['regpassword'])){ echo " class='errorbackground'";} ?>></td>
                            
                            <td></td>
						</tr> 
                        <tr>
                        	<td>Confirmez votre mot de passe</td>
                            <td><input tabindex="3" type="password" name="regpasswordc" class="form" style="width:175px; height:27" value="<?php if(isset($_SESSION['regpasswordc'])){ echo $_SESSION['regpasswordc'];} ?>" <?php if(isset($_SESSION['error']['regpasswordc'])){ echo " class='errorbackground'";} ?>></td>
                            
                            <td></td>
						</tr> 
                        <tr>
                        	<td>Adresse e-mail</td>
                            <td><input tabindex="4" type="text" name="regemail" class="form" style="width:175px; height:27" value="<?php if(isset($_SESSION['regemail'])){ echo $_SESSION['regemail'];} ?>" <?php if(isset($_SESSION['error']['regemail'])){ echo " class='errorbackground'";} ?>></td>
                            
                            <td></td>
						</tr> 
                        <tr>
                        	<td>Confirmez votre e-mail</td>
                            <td><input tabindex="5" type="text" name="mailmail2" value="<?php if(isset($_SESSION['mailmail2'])){ echo $_SESSION['mailmail2'];} ?>" class="form" style="width:175px; height:27"></td>
                            
                            <td></td>
						</tr>                                                                                                   
                        <tr>
                        	<td></td>
                            <td><img src="<?php echo $http ; ?>images/captchaCreate.php"></td>
                            
                            <td></td>
						</tr> 
                        <tr>
                        	<td>Code de sécurité</td>
                            <td><input tabindex="6" type="text" name="regseccode" class="form" style="width:175px; height:27" value="<?php if(isset($_SESSION['regseccode'])){ echo $_SESSION['regseccode'];} ?>" <?php if(isset($_SESSION['error']['regseccode'])){ echo " class='errorbackground'";} ?> style="width:135px;"></td>
                            
                            <td></td>
						</tr>                        
                    	<tr>
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                            <td colspan="2"><div style="width:375px"><div style="width:270px;margin:auto;"><div style="float:left;width:20px;"><input tabindex="7" type="checkbox" name="regAgree" ></div><div style="width:250px;float:left;"> J'ai lu et j'accepte <a href="javascript:void();" onclick="condi();" class="lien_noir">les conditions d'utilisation</a> et <a href="javascript:void();" onclick="poli();" class="lien_noir" >la politique de protection des données personnelles</a></div></div></div></td>

						</tr> 
                        <tr>
                            <td><br /><input tabindex="8" style="margin-left:72px;" src="<?php echo $http ; ?>images/submit_send.png" type="image" class="registerbutton<?php if($_SESSION['language'] == "Fre"){ echo "fr"; } ?>"></td>
                            
                            <td></td>
						</tr>                        
					</table>                                                                                                                                        
                    <br><br>
                    </td>
                </tr>
            </table>
		
 <input type="hidden" value="0" name="regconfirm" id="regconfirm" />       
	<input type="hidden" value="register" name="register" />  
	<input type="hidden" name="regcountry" value="<?php echo $ip_code ; ?>" />
  <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>
</center> 
</form>       
<script type="text/javascript">
 <?php if($_GET['variable'] == 1){ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
	
	if (w > 0) window.parent.$('.iframe_fancy').css({ width: w+"px"});
    if (h > 0) window.parent.$('.iframe_fancy').css({ height: h+"px"});
	
    if (w > 0) window.parent.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php }else{ ?>
function fb_resize(w, h) {
  if (w > 0 || h > 0) {
    if (w > 0) window.parent.$('#fancybox-content').css({ width: w+"px"});
    if (h > 0) window.parent.$('#fancybox-content').css({ height: h+"px"});
    $.fancybox.resize();
  }
}
<?php } ?>
fb_resize(600,470);

function poli(){
	parent.document.location='<?php echo $http ; ?>protection-des-donnees-personnelles/';
	window.parent.$.fancybox.close();
}
function condi(){
	
	parent.document.location='<?php echo $http ; ?>conditions-generales/';
	window.parent.$.fancybox.close();
}
function fermer(){
	window.parent.$.fancybox.close();
}
</script>                
</body>
</html>




	
	
	

	
	
	
<?php
$_SESSION['regusername'] = "";
$_SESSION['regpassword'] = "";
$_SESSION['regpasswordc'] = "";
$_SESSION['regemail'] = "";
$_SESSION['mailmail2'] = "";
$_SESSION['regrefusername'] = "";
$_SESSION['regseccode'] = "";
$_SESSION['erreur_inscription'] = "";
?>