<?php
if(!session_id()) session_start();

if(!isset($_COOKIE['__trk'])){
	$unique = uniqid();
	$cookie = md5($unique.$_SERVER['REMOTE_ADDR']);
	setcookie("__trk", $cookie, time()+15552000, "/");
}else{
	setcookie("__trk", $_COOKIE['__trk'], time()+15552000, "/");
}
require_once('includes/Mobile_Detect.php');
require_once("includes/settings.inc.php");
require_once("includes/dbinfo.inc.php");
require_once("includes/functions.php");
require_once("logincheck.php");
require_once("langue/fr.php");
//require_once("locationcheck.php");

$detect_os = new Mobile_Detect();
if($detect_os->isTablet()){

if(isset($_COOKIE['__AppliDramapassion'])){
$cookiez = $_COOKIE['__AppliDramapassion'];
if($cookiez == 1){
		$verif_app = 0;
	}else{
		$verif_app = 1;
	}

}else{
	$verif_app = 1;

}
}

$user_lvl = $_SESSION['subscription'];

$os = getOs();


if(isset($_SESSION['userid']) && $_SESSION['userid'] > 0){
	$_SESSION['balance'] = mysql_result(mysql_query("SELECT UserBalance FROM t_dp_user WHERE UserID = ".$_SESSION['userid']),0,"UserBalance");	
}

$page = str_replace(".php","",basename($_SERVER['PHP_SELF']));

$title = _ttlindex_;
$description = "";
$keywords = "";
$footer = "";
$sociallinks = false;
$og_title = _ttlindex_ ;
$og_img = $http."images/logo_facebook.jpg";
$og_desc = _dscindex_;

if($page == "index"){
	$title = _ttlindex_;
	$description = _dscindex_;
	$keywords = _keyindex_;
	$sociallinks = true ;
	
	$og_title = _ttlindex_ ;
	$og_img = $http."images/logo_facebook.jpg";
	$og_desc = _dscindex_;
	
}elseif($page == "drama2"){
	$dramaID = cleanup($_GET['dramaID']);
	$drama_info_meta = DramaInfo($dramaID);
	
	$title = _ttldrama2_;
	$title = str_replace("%%title%%",$drama_info_meta['titre'],$title);
	
	$description = _dscdrama2_;
	$synopsis = substr($drama_info_meta['synopsis'],0,200);
		if(strlen($drama_info_meta['synopsis']) > 200){
			$synopsis.= "...";	
		}
	$description =  str_replace("%%title%%",$drama_info_meta['titre'],$description);
	$description =  str_replace("%%synopsis%%",$synopsis,$description);
	
	$keywords = _keydrama2_;
	$keyword_titre = $drama_info_meta['titre'].', '.$drama_info_meta['keyword'];
	$keywords = str_replace("%%title%%",$keyword_titre,$keywords);
	
	$sociallinks = true;
	$og_title = $drama_info_meta['titre'];
	$og_img = $drama_info_meta['img_detail'];
	$og_desc = $synopsis;
	
}elseif($page == "player"){
	$dramaID = cleanup($_GET['dramaID']);
	$drama_info_meta = DramaInfo($dramaID);
	$epiNB = cleanup($_GET['epiNB']);
	$type = cleanup($_POST['hidden_type']);
	
	if($epiNB < 10){
		$epiNB2 = $chaine = substr($epiNB,1); 
	}
	
	$title = _ttlplayer_;
	$title = str_replace("%%title%%",$drama_info_meta['titre'],$title);
	$title = str_replace("%%type%%",$type,$title);
	$title = str_replace("%%numEpi%%",$epiNB2,$title);
	$title = str_replace("free","Gratuit",$title);
	$title = str_replace("sd","SD",$title);
	$title = str_replace("hd","HD",$title);
	
	
	$description = _dscplayer_;
	$synopsis = substr($drama_info_meta['synopsis'],0,200);
		if(strlen($drama_info_meta['synopsis']) > 200){
			$synopsis.= "...";	
		}
	$description =  str_replace("%%title%%",$drama_info_meta['titre'],$description);
	$description =  str_replace("%%synopsis%%",$synopsis,$description);
	
	$keywords = _keyplayer_;
	$keyword_titre = $drama_info_meta['titre'].', '.$drama_info_meta['keyword'];
	$keywords = str_replace("%%title%%",$keyword_titre,$keywords);
	
	$sociallinks = true;
	
	$sociallinks = true;
	$og_title = $drama_info_meta['titre']." - ".$type." Episode ".$epiNB;
	$og_title = str_replace("free","Gratuit",$og_title);
	$og_title = str_replace("sd","SD",$og_title);
	$og_title = str_replace("hd","HD",$og_title);
	$og_img = $drama_info_meta['img_detail'];
	$og_desc = $synopsis;
	
}elseif($page == "premium2"){
	$title = _ttlpremium2_;
	$description = _dscpremium2_;
	$keywords = _keypremium2_;
}elseif($page == "catalogue2"){
	$title = _ttlcatalogue2_;
	$description = _dsccatalogue2_;
	$keywords = _keycatalogue2_;
}elseif($page == "dvd2"){
	$title = _ttldvd2_;
	$description = _dscdvd2_;
	$keywords = _keydvd2_;
}elseif($page == "dvd_detail"){
	$title = _ttldvdDetail_;
	$description = _dscdvdDetail_;
	$keywords = _keydvdDetail_;
}elseif($page == "qui_somme_nous"){
	$title = _ttlquiSommeNous_;
	$description = _dscquiSommeNous_;
	$keywords = _keyquiSommeNous_;
}elseif($page == "condi_general"){
	$title = _ttlcondiGeneral_;
	$description = _dsccondiGeneral_;
	$keywords = _keycondiGeneral_;
}elseif($page == "protect_donnee"){
	$title = _ttlprotectDonnee_;
	$description = _dscprotectDonnee_;
	$keywords = _keyprotectDonnee_;
}elseif($page == "faq"){
	$title = _ttlfaq_;
	$description = _dscfaq_;
	$keywords = _keyfaq_;
}elseif($page == "contact"){
	$title = _ttlcontact_;
	$description = _dsccontact_;
	$keywords = _keycontact_;
}elseif($page == "mon_compte"){
	$title = _ttlmonCompte_;
	$description = _dscmonCompte_;
	$keywords = _keymonCompte_;
}elseif($page == "mon_abo"){
	$title = _ttlmonAbo_;
	$description = _dscmonAbo_;
	$keywords = _keymonAbo_;
}elseif($page == "playlist_aff"){
	$title = _ttlplaylistAff_;
	$description = _dscplaylistAff_;
	$keywords = _keyplaylistAff_;
}

$url_serv = $http3.$_SERVER['REQUEST_URI'];
$url_replace = str_replace('/', '%2F' ,$url_serv);
$url_req = "https://www.facebook.com/sharer.php?u=http%3A%2F%2F".$url_replace;


if($page == "player"){
$info = DramaInfo($dramaID);
$titre = $info['titre'];
$titre_clean = dramaLinkClean($titre);
$url_face = $http."drama/".$dramaID."/".$titre_clean;
$facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count" data-href="'.$url_face.'"></div></td>';

}elseif($page == 'index'){
$facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/dramapassion" ></div></td>';

}else{
$facebook_rec = '<td class="blanc" width="130"><div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div></td>';
$url_face = $http2.$_SERVER['REQUEST_URI'];

}


?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<meta charset="utf-8" />
<?php 
if($detect_os->isTablet()){
	if($detect_os->isiOS() ){
		echo '<meta name="viewport" content="width=device-width,target-densitydpi=high-dpi,initial-scale=1.0,maximum-scale=1.4,minimum-scale=0.5">';
	}else{
		echo '<meta name="viewport" content="width=1040,target-densitydpi=device-dpi,initial-scale=1.5,maximum-scale=1.4,minimum-scale=0.5">';
	}

}else{
	echo '<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.4,minimum-scale=0.5">';
}
?>


<!-- css -->
<link rel="StyleSheet" href="<?php echo $server; ?>css/my_style.css?<?php echo date('s'); ?>" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_free.css" type="text/css" media="screen" />
<link rel="StyleSheet" href="<?php echo $server; ?>css/style_page_bottom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.css?<?php echo time() ;?>" type="text/css" media="screen" />

<link rel="stylesheet" href="<?php echo $server; ?>css/example/example.css" type="text/css">
<link rel="stylesheet" href="<?php echo $server; ?>css/dropkick.css" type="text/css">
<link rel="stylesheet" href="<?php echo $server; ?>css/swipebox.css">


<link href='http://fonts.googleapis.com/css?family=Carter+One&v1' rel='stylesheet' type='text/css'>
  
  
  <style type="text/css">
    .dk_theme_black {
	  background-image: url('<?php echo $http ; ?>images/fond_menu_player.png');
	  background-repeat: repeat-x;
  
  );
}
  .dk_theme_black .dk_toggle,
  .dk_theme_black.dk_open .dk_toggle {
    background-color: transparent;
    color: #fff;
    text-shadow: none;
    text-align: right;
  }
  .dk_theme_black .dk_options a {
    background-color: #333;
    color: #fff;
    text-shadow: none;
  }
    .dk_theme_black .dk_options a:hover,
    .dk_theme_black .dk_option_current a {
      background-color: #969695;
      color: #fff;
      text-shadow: #604A42 0 1px 0;
    }
  </style>

<link rel="shortcut icon" type="image/png" href="<?php echo $http ; ?>logo.png" /> 
<link rel="icon" type="image/png" href="<?php echo $http ; ?>logo.png" /> 

<!-- meta -->
<title><?php echo $title;?></title>
<meta name="description" lang="fr" content="<?php echo $description;?>">
<meta name="keywords" lang="fr" content="<?php echo $keywords;?>">
<meta name="google-site-verification" content="SbdBgJew61e-YYEHq3Mb5FNdsqAOOO5eY3EmTTutQr0" />
<meta name="author" content="Vlexhan">
<meta name="Publisher" content="Weaby.Be by G1 sprl" />
<meta name="date-creation-ddmmyyyy" content="21022012" />
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="3 days" />
<meta http-equiv="content-language" content="fr-fr" />

<meta property="fb:admins" content="100000544554843" />

<?php if($sociallinks == true || $page == "index"){ ?>
<meta property="og:url" content="<?php 

if($page == "index"){
echo "http://www.facebook.com/dramapassion" ;
}else{
echo $url_face ; 
}

?>" />
<meta property="og:title" content="<?php echo $og_title ; ?>"/>
<meta property="og:type" content="tv_show"/>
<meta property="og:image" content="<?php echo $og_img ; ?>"/>
<meta property="og:site_name" content="DramaPassion"/>
<meta property="og:description" content="<?php echo $og_desc ; ?>"/>
<?php } 
	
if($page == "player"){
	$crawler = 0;
	if ( preg_match('/(bot|spider|yahoo)/i', $_SERVER[ "HTTP_USER_AGENT" ] )) $crawler = 1 ;
	
}	
	
?>

<!-- script java -->
<script src="<?php echo $server; ?>/js/scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>/js/AC_OETags.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/jquery.iframe-auto-height.plugin.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>js/functions.js"></script>
<script type="text/javascript" src="<?php echo $server; ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<?php if ($os == 'Ps' || $os == 'Android'){ ?>
	<script type="text/javascript" src="<?php echo $http ; ?>js/flowplayer-3.2.4.min.js"></script>		
<?php } else { ?>
	<script type="text/javascript" src="<?php echo $http ; ?>js/flowplayer-3.2.12.min.js"></script>	
<?php } ?>
<script type="text/javascript" src="<?php echo $http ; ?>js/flash_detect_min.js"></script>
<script type="text/javascript" src="<?php echo $http ; ?>js/swfobject.js"></script>
<script src="<?php echo $http ; ?>js/jquery.dropkick-1.0.0.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
$(function () {

	$('.custom_dropbox').dropkick({
        theme : 'black',
        change: function (value, label) {
          var url_ajax = "<?php echo $http ; ?>change_player.php?player="+value ;
          
          $.ajax({
	          url: url_ajax
	      }).done(function() {
		      location.reload();
		//window.location = url_location ;
		});
          
        }
    });
        

});
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12509314-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php if($_SESSION['screen_size_x'] == '' && $_SESSION['screen_size_y'] == ''){
?>
<script language="Javascript">
$(document).ready(function() {
  $.ajax({
   type: "POST",
   url: "temp_screen_size.php",
   data: "x="+screen.width+"&y="+screen.height,
 });
 });
</script>
<?php } ?>
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
googletag.cmd.push(function() {
googletag.defineSlot('/20746576/home_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-0').addService(googletag.pubads());
googletag.defineSlot('/20746576/small_rectangle_2', [250, 250], 'div-gpt-ad-1346334604979-1').addService(googletag.pubads());
googletag.defineSlot('/20746576/small_rectangle_all_pages', [250, 250], 'div-gpt-ad-1346334604979-2').addService(googletag.pubads());
googletag.defineSlot('/20746576/video_viewer_right_rectangle', [300, 250], 'div-gpt-ad-1346334604979-3').addService(googletag.pubads());
googletag.defineSlot('/20746576/video_viewer_top_leaderboard', [728, 90], 'div-gpt-ad-1346334604979-4').addService(googletag.pubads());
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script>
<link type="text/css" rel="stylesheet" href="<?php echo $http ; ?>css/jquery.dropdown.css" />
<script type="text/javascript" src="<?php echo $http ; ?>js/jquery.dropdown.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="conteneur_all_page">
<div class="contenant_all_page">
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '344619609607', // App ID
      channelUrl : 'http://www.dramapassion.com', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/fr_FR/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "15796249" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=15796249&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- ImageReady Slices (Sans titre-1) -->


<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	
        <td valign="top" height="93" style="background-image:url(<?php echo $http ; ?>images/header.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER GRIS-->
        <?php
        if($page == "player"){
        ?>
        <div style="width:960px;margin:auto;height:93px;overflow:hidden;" >
            <table id="Tableau_01" width="960" height="93" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="368" height="93"><a href="<?php echo $http ; ?>" ><img alt="logo DramaPassion" title="Aller à la page d'accueil" src="<?php echo $http ; ?>images/logo.png" width="368" height="93"  border="0"></a></td>
                    <td width="124" height="93"><a href="<?php echo $http ; ?>catalogue/" ><img alt="catalogue des séries" title="Voir tout le catalogue" src="<?php echo $http ; ?>images/video.png" width="124" height="93" alt="Vidéo on Dramapassion" title="video" border="0" onMouseOver="this.src='<?php echo $http ; ?>images/video_up.png'" onMouseOut="this.src='<?php echo $http ; ?>images/video.png'"></a></td>
                    <td width="141" height="93"><a href="<?php echo $http ; ?>premium/" ><img alt="premium" title="Voir les abonnements" src="<?php echo $http ; ?>images/premium.png" width="141" height="93" alt="Premium account" title="premium" border="0" onMouseOver="this.src='<?php echo $http ; ?>images/premium_up.png'" onMouseOut="this.src='<?php echo $http ; ?>images/premium.png'"></a></td>
                    <td width="90" height="93"><a href="<?php echo $http ; ?>dvd/" ><img alt="DVD" title="Voir les coffrets DVD" src="<?php echo $http ; ?>images/dvd.png" width="90" height="93" alt="DVD" title="dvd" border="0" onMouseOver="this.src='<?php echo $http ; ?>images/dvd_up.png'" onMouseOut="this.src='<?php echo $http ; ?>images/dvd.png'"></a></td>
                    <td width="237" height="93" valign="middle" align="right">
                    	<form action="<?php echo $http ;?>catalogue/" method="POST">
                        <table height="93" cellpadding="0" cellspacing="0" width="180">
                        <!-- MOTEUR DE RECHERCHE -->
							
                        	<tr>
						
							<?php if($_SESSION['zone_recherche'] != "" && $_SESSION['zone_recherche'] != ""){
							
                            	echo'<td><input type="text" name="recherche_top" id="top_recherche" class="form_recherche2" value="'.$_SESSION['zone_recherche'].'" /></td>';
							
							}else{
							
								echo'<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="form_recherche2 recherche_gris" value="Chercher un titre" /></td>';
							
							}
							?>
							
                                <td><input src="<?php echo $http ; ?>images/submit_search.png" type="image" /></td>
							
							</tr>
							
		                                     
                        <!-- FIN MOTEUR DE RECHERCHE -->                              
						</table>  
                        </form>                                                          
                    </td>
                </tr>
            </table>
		</div>
        <?php
        }else{
        
        ?>
        <div style="width:1000px;margin:auto;height:93px;overflow:hidden;" >
            <table id="Tableau_01" width="1000" height="93" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="368" height="93"><a href="<?php echo $http ; ?>" ><img alt="logo DramaPassion" title="Aller à la page d'accueil" src="<?php echo $http ; ?>images/logo.png" width="368" height="93"  border="0"></a></td>
                    <td width="124" height="93"><a href="<?php echo $http ; ?>catalogue/" ><img alt="catalogue des séries" title="Voir tout le catalogue" src="<?php echo $http ; ?>images/video.png" width="124" height="93" alt="Vidéo on Dramapassion" title="video" border="0" onMouseOver="this.src='<?php echo $http ; ?>images/video_up.png'" onMouseOut="this.src='<?php echo $http ; ?>images/video.png'"></a></td>
                    <td width="141" height="93"><a href="<?php echo $http ; ?>premium/" ><img alt="premium" title="Voir les abonnements" src="<?php echo $http ; ?>images/premium.png" width="141" height="93" alt="Premium account" title="premium" border="0" onMouseOver="this.src='<?php echo $http ; ?>images/premium_up.png'" onMouseOut="this.src='<?php echo $http ; ?>images/premium.png'"></a></td>
                    <td width="90" height="93"><a href="<?php echo $http ; ?>dvd/" ><img alt="DVD" title="Voir les coffrets DVD" src="<?php echo $http ; ?>images/dvd.png" width="90" height="93" alt="DVD" title="dvd" border="0" onMouseOver="this.src='<?php echo $http ; ?>images/dvd_up.png'" onMouseOut="this.src='<?php echo $http ; ?>images/dvd.png'"></a></td>
                    <td width="277" height="93" valign="middle" align="right">
                    	<form action="<?php echo $http ;?>catalogue/" method="POST">
                        <table height="93" cellpadding="0" cellspacing="0" width="220">
                        <!-- MOTEUR DE RECHERCHE -->
							
                        	<tr>
							
							<?php if($_SESSION['zone_recherche'] != "" && $_SESSION['zone_recherche'] != ""){
							
                            	echo'<td><input type="text" name="recherche_top" id="top_recherche" class="form_recherche" value="'.$_SESSION['zone_recherche'].'" /></td>';
							
							}else{
							
								echo'<td><input type="text" name="recherche_top" id="top_recherche" onclick="supprimer_gris_recherche();" class="form_recherche recherche_gris" value="Chercher un titre" /></td>';
							
							}
							?>
							
                                <td><input src="<?php echo $http ; ?>images/submit_search.png" type="image" /></td>
							
							</tr>
							
		                                     
                        <!-- FIN MOTEUR DE RECHERCHE -->                              
						</table>  
                        </form>                                                          
                    </td>
                </tr>
            </table>
		</div>
        <?php } ?>
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
    	<?php
    	if($page == "player"){
    	?>
    	<tr>
        <td valign="top" height="37" style="background-image:url(<?php echo $http ; ?>images/header2.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:960px;margin:auto;">
            <table id="Tableau_01" width="960" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				<?php 
				$userConnect = UserIsConnect();
				if($userConnect == 0){ 
				
				echo $facebook_rec;
				
				
				?>
                    
					
					<td class="blanc" width="645"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
               
				<?php if($os == 'IOS' || $os == 'Android' ){ ?>
				   <td class="blanc" width="20"><a title="Me connecter" href="<?php echo $http ; ?>connexion_mobile.php" class="lien_blanc" id="identifier1"><img src="<?php echo $http ; ?>images/account.png" align="absmiddle" alt="S'identifier" title="Me connecter"></a></td>
					<td class="blanc bloc_center" width="70"><a title="Me connecter" href="<?php echo $http ; ?>connexion_mobile.php" class="lien_blanc" id="identifier">S'identifier</a><td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="95" align="right"><a title="Créer un compte" href="<?php echo $http ; ?>register_mobile.php" class="lien_blanc" id="registerrr" style="text-align: right">Créer un compte</a><td>
				<?php }else{ ?>
					<td class="blanc" width="20"><a title="Me connecter" href="<?php echo $http ; ?>connexion.php" class="iframe lien_blanc identifier" id="identifier1"><img src="<?php echo $http ; ?>images/account.png" align="absmiddle" alt="S'identifier" title="Me connecter"></a></td>
					<td class="blanc bloc_center" width="70"><a title="Me connecter" href="<?php echo $http ; ?>connexion.php" class="iframe lien_blanc identifier" id="identifier">S'identifier</a><td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="95" align="right"><a title="Créer un compte" href="<?php echo $http ; ?>register.php" class="iframe lien_blanc" id="register" style="text-align: right">Créer un compte</a><td>
				
				<?php } ?>
				<?php }else{ ?>
                    <?php if($page == 'index'){ ?>
					<td class="blanc" width="130">
					<div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/dramapassion" ></div></td>
					<?php }else{ ?>
					<td class="blanc" width="130">
					<div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div></td>
                    <?php } ?>
					<td class="blanc" width="122"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
					<?php if(in_array($_SESSION['subscription'], $array_dis)){ 
						$abo_statut = '[Découverte]';     						
					}
					elseif(in_array($_SESSION['subscription'], $array_pri)){ 
						$abo_statut = '[Privilège]';
					}else{ 
						$abo_statut = '';                   
					} ?>
					<td class="blanc" width="763" align="right"><a href="#" data-dropdown="#dropdown-1" id="dropdown_menu" title="Gérer mon compte" class="lien_blanc" style="text-align: right"><?php echo $_SESSION['username']." ".$abo_statut ; ?>&nbsp;&nbsp;</a>
					<div id="dropdown-1" class="dropdown dropdown-tip">
					    <ul class="dropdown-menu">
					    	<li><a href="<?php echo $http ;?>compte/abonnement/">Mon Abonnement</a></li>
					        <li><a href="<?php echo $http ;?>compte/profil/">Mon Profil</a></li>
					        <li><a href="<?php echo $http ;?>compte/paiements/">Mes Paiements</a></li>
					    </ul>
					</div>
					<td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc bloc_center" width="70"><a href="<?php echo $http ; ?>compte/playlist/" title="Voir ma playlist" class="lien_blanc">Ma playlist</a><td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="70" align="right"><a href="<?php echo $http ; ?>logoutAction.php" title="Me déconnecter" class="lien_blanc" style="text-align: right">Déconnexion</a><td>
					
					
				<?php } ?>
                                      
                                       
                </tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>
    	<?php
    	}else{
	    	
    	?>
	<tr>
        <td valign="top" height="37" style="background-image:url(<?php echo $http ; ?>images/header2.jpg); background-repeat:repeat-x;">  
        <!-- BLOC HEADER ROSE -->
		<div style="width:1000px;margin:auto;">
            <table id="Tableau_01" width="1000" border="0" cellpadding="0" cellspacing="0" height="37">
                <tr>
				<?php 
				$userConnect = UserIsConnect();
				if($userConnect == 0){ 
				
				echo $facebook_rec;
				
				
				?>
                    
					
					<td class="blanc" width="685"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
               
				<?php if($os == 'IOS' || $os == 'Android' ){ ?>
				   <td class="blanc" width="20"><a title="Me connecter" href="<?php echo $http ; ?>connexion_mobile.php" class="lien_blanc" id="identifier1"><img src="<?php echo $http ; ?>images/account.png" align="absmiddle" alt="S'identifier" title="Me connecter"></a></td>
					<td class="blanc bloc_center" width="70"><a title="Me connecter" href="<?php echo $http ; ?>connexion_mobile.php" class="lien_blanc" id="identifier">S'identifier</a><td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="95" align="right"><a title="Créer un compte" href="<?php echo $http ; ?>register_mobile.php" class="lien_blanc" id="registerrr" style="text-align: right">Créer un compte</a><td>
				<?php }else{ ?>
					<td class="blanc" width="20"><a title="Me connecter" href="<?php echo $http ; ?>connexion.php" class="iframe lien_blanc identifier" id="identifier1"><img src="<?php echo $http ; ?>images/account.png" align="absmiddle" alt="S'identifier" title="Me connecter"></a></td>
					<td class="blanc bloc_center" width="70"><a title="Me connecter" href="<?php echo $http ; ?>connexion.php" class="iframe lien_blanc identifier" id="identifier">S'identifier</a><td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="95" align="right"><a title="Créer un compte" href="<?php echo $http ; ?>register.php" class="iframe lien_blanc" id="register" style="text-align: right">Créer un compte</a><td>
				
				<?php } ?>
				<?php }else{ ?>
                    <?php if($page == 'index'){ ?>
					<td class="blanc" width="130">
					<div class="fb-like" data-send="false"  data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/dramapassion" ></div></td>
					<?php }else{ ?>
					<td class="blanc" width="130">
					<div class="fb-like" data-send="false"  data-show-faces="false" data-action="recommend" data-layout="button_count"></div></td>
                    <?php } ?>
					<td class="blanc" width="122"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweeter</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
					<?php if(in_array($_SESSION['subscription'], $array_dis)){ 
						$abo_statut = '[Découverte]';     						
					}
					elseif(in_array($_SESSION['subscription'], $array_pri)){ 
						$abo_statut = '[Privilège]';
					}else{ 
						$abo_statut = '';                   
					} ?>
					<td class="blanc" width="763" align="right"><a href="#" data-dropdown="#dropdown-1" id="dropdown_menu" title="Gérer mon compte" class="lien_blanc" style="text-align: right"><?php echo $_SESSION['username']." ".$abo_statut ; ?>&nbsp;&nbsp;</a>
					<div id="dropdown-1" class="dropdown dropdown-tip">
					    <ul class="dropdown-menu">
					    	<li><a href="<?php echo $http ;?>compte/abonnement/">Mon Abonnement</a></li>
					        <li><a href="<?php echo $http ;?>compte/profil/">Mon Profil</a></li>
					        <li><a href="<?php echo $http ;?>compte/paiements/">Mes Paiements</a></li>
					    </ul>
					</div>
					<td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc bloc_center" width="70"><a href="<?php echo $http ; ?>compte/playlist/" title="Voir ma playlist" class="lien_blanc">Ma playlist</a><td>
					<td class="blanc" width="5"><img src="<?php echo $http ; ?>images/barre.png" align="absmiddle"></td>
					<td class="blanc" width="70" align="right"><a href="<?php echo $http ; ?>logoutAction.php" title="Me déconnecter" class="lien_blanc" style="text-align: right">Déconnexion</a><td>
					
					
				<?php } ?>
                                      
                                       
                </tr>
			</table>
		</div>
        <!-- FIN BLOC HEADER ROSE -->        
        </td>
	</tr>
	<?php } ?>

<?php
$msiecheck = $_SERVER['HTTP_USER_AGENT'];
$msie7 = strpos($msiecheck, 'MSIE 7.0');
$msie6 = strpos($msiecheck, 'MSIE 6.0');
if ($msie7 != false || $msie6 != false) {
	$trident = strpos($msiecheck, 'Trident');
	if ($trident == false){
?>
	<tr>
        <td height="60">  
        <!-- BLOC ALERTE IE -->
		<br /><center>
			<table width="998" border="1" cellpadding="0" cellspacing="0" height="40" bordercolor="red">
				<tr>
					<td valign="middle"><center>
						<a href="<?php echo $http; ?>aide-faq/4#401"><font color="red" size="4">
						ATTENTION : Votre navigateur web n'est pas compatible avec ce site !
						Cliquez ici pour plus d'informations.</font></a>
					</center></td>
				</tr>
			</center></table>
        <!-- FIN BLOC ALERTE IE -->        
        </td>
	</tr>	
<?php }}	 ?>