<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
	
	$urlsec = Hash_st_HD(10,17,'hd');
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Utilisation d'un gestionnaire de téléchargement (Windows)</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="contenu" style="text-align:justify;">
<p><i>Si vous utilisez un ordinateur Mac, veuillez consulter la page suivante :
<a href="<?php echo $http; ?>astuce-telechargement-mac/" class="lien_bleu"><?php echo $http; ?>astuce-telechargement-mac/</i></a></p>
<br />
<h3>1. Introduction</h3>
<p>L'utilisation d'un gestionnaire de téléchargement permet :</p>
<ul>
<li>d'augmenter significativement la vitesse de téléchargement</li>
<li>de programmer une liste de téléchargements qui s'effectue automatiquement même lorsque vous n'êtes pas devant votre ordinateur</li>
</ul>
<p>Vous pouvez donc non seulement réduire le temps de téléchargement, mais aussi optimaliser votre temps en programmant à l'avance
les vidéos que vous désirez télécharger. Par exemple, laissez le gestionnaire télécharger les vidéos pendant la nuit et vous pourrez
regarder les vidéos le lendemain sans vous soucier de l'état de votre connexion internet. De plus, la connexion est en général plus rapide la nuit.</p>
<br />
<p>Si vous n'avez encore jamais téléchargé de fichiers vidéo sur Dramapassion, veuillez d'abord consulter la guide pour le téléchargement au lien suivant : 
<a href="<?php echo $http; ?>guide-telechargement/" class="lien_bleu"><?php echo $http; ?>guide-telechargement/</a></p>
<br />
<h3>2. Installation du logiciel Free Download Manager</h3>
<p>Téléchargez le logiciel au lien suivant et installez le logiciel (double-clic sur le fichier téléchargé) :
<a href="<?php echo $http; ?>software/fdminst-Dramapassion.exe" class="lien_bleu">cliquez ici pour télécharger Free Download Manager</a></p>
<br />
<h3>3. Configuration du logiciel</h3>
<p>Il faut modifier quelques paramètres de configuration pour optimaliser la performance pour notre site. Suivez étape par étape les instructions ci-dessous.</p>
<br />
<p>1. Ouvrez Free Download Manager.</p>
<p>2. Allez dans "Options" dans le menu et ensuite cliquez sur "Paramètres".</p>
<p>3. Dans la fenêtre des paramètres, cliquez sur "Réseau" dans la colonne de gauche.</p>
<p>4. Modifiez les paramètres comme dans l'image suivante.</p>
<div style="margin-top:5px;margin-bottom:5px"><img src="<?php echo $http;?>images/fdm1.png" width="600"></div><br />
<p>5.(Option) Cliquez sur "Limite de temps" dans la colonne de gauche.
Vous pouvez définir ici la plage horaire durant laquelle le logiciel va effectuer les téléchargements.
Ceci est optionnel, mais si vous rencontrez des problèmes de connexion à certaines heures de la journée,
cette fonctionnalité peut s'avérer très utile.</p>
<p>6. Cliquez sur "OK".</p>
<p>7. Cliquez sur "Fichier" dans le menu et sélectionnez "Quitter".</p>
<p>8. Lancez à nouveau Free Download Manager.</p>
<br />
<h3>3. Chargement de la liste de vidéos</h3>
<p>Connectez-vous sur www.dramapassion.com et rendez-vous sur la fiche du drama que vous souhaitez regarder.</p>
<br />
<p>1. Dans la colonne "Téléchargement", cliquez sur le bouton "SD" ou "HD" (les fichiers HD prennent plus de temps à télécharger).</p>
<p>2. Dans la nouvelle fenêtre ou onglet qui s'ouvre, faites un clic droit sur le lien "Cliquez ici pour lancer le téléchargement",
et sélectionnez "copier l'adresse du lien".</p>
<p>3. Dans Free Download Manager, cliquez sur le bouton "+" (premier bouton dans la barre de menu).</p>
<p>4. Cliquez sur "OK" et vous verrez que le lien est ajouté dans la fenêtre principale de Free Download Manager.</p>
<p>5. Répétez les étapes 1 à 4 pour les autres vidéos que vous souhaitez ajouter à la liste de téléchargement.</p>
<br />
<p>Nous vous recommandons de mettre 4 à 5 vidéos pour commencer. Nous vous déconseillons d'en mettre plus de 10.</p>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>