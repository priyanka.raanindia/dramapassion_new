 var owl = $('.owl-carousel');
    owl.owlCarousel({
        margin: 0,
        nav: true,
        autoplay: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

 $("#header .responsive-menu").click(function () {
    $("#header .open-menu").slideToggle(); 
 });

 $(".row_section .latest_releases ul li a").click(function () {
     $(this).next(".img").slideToggle();
     $(this).parent("li").siblings().find(".img").slideUp();
 });



