<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
	

	
?>
<!-- ImageReady Slices (Sans titre-1) -->
<div id="fb-root"></div>
 <script type="text/javascript">
// Load the SDK Asynchronously
(function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script');
		 js.id = id;
		 js.async = true;
         js.src = "//connect.facebook.net/fr_FR/all.js";
         ref.parentNode.insertBefore(js, ref);
		 
}(document));


 window.fbAsyncInit = function() {

        FB.init({
          appId      : '344619609607', // App ID
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true  // parse XFBML
        });
		
		
        function bindEvent(el, eventName, eventHandler) {
			if (el.addEventListener){
				el.addEventListener(eventName, eventHandler, false); 
			} else if (el.attachEvent){
				el.attachEvent('on'+eventName, eventHandler);
			}
		}

        // respond to clicks on the login and logout links
       // document.getElementById('loginlink_face').addEventListener('click', function(){
		bindEvent(document.getElementById('loginlink_face'), 'click', function () {
		
		
		 
		  //document.location.href="http://ns225899.ovh.net/facebook_connect_mobile.php" ;
			FB.getLoginStatus(function(response) {
		  
				if(response.status === 'connected') {
				
					// the user is logged in and has authenticated your
					// app, and response.authResponse supplies
					// the user's ID, a valid access token, a signed
					// request, and the time the access token 
					// and signed request each expire
					var uid = response.authResponse.userID;
					var accessToken = response.authResponse.accessToken;
					var url = "<?php echo $http ; ?>facebook_connect_mobile.php?userid="+uid+"&token="+accessToken ;
					window.location = url ;
	
				}else if(response.status === 'not_authorized') {
					// the user is logged in to Facebook, 
					// but has not authenticated your app
					var url2= "<?php echo $http ; ?>facebook_register_form_mobile.php" ;
					window.location = url2;
	
	
				}else{
					// the user isn't logged in to Facebook.
					//FB.login();
					//window.location.reload();
					// document.location.href="http://ns225899.ovh.net/facebook_login_mobile.php" ;
					FB.Event.subscribe('auth.login', function(resp){
			
						var uid = resp.authResponse.userID;
						var accessToken = resp.authResponse.accessToken;
						var url3 = "<?php echo $http ; ?>facebook_connect_mobile.php?userid="+uid+"&token="+accessToken ;
						window.location = url3 ;
					});
					FB.login();
				}
			});
		  
		  
		  
        });
       
} 

</script>
<?php 
?>

<form action="<?php echo $http ;?>loginAction_mobile.php" METHOD="POST">
<input type="hidden" name="login" value="login">
<table id="Tableau_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
        <td valign="top">  
        <!-- BLOC HEADER GRIS-->
        
        <div style="width:100%">
<div style="width:600px;margin:auto;margin-top:50px;border: solid 1px black">
            <table id="Tableau_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" height="51" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x" valign="bottom">
                    <div id="cont_iframe_signaler">
						<div id="cont_titre_iframe_signaler" style="background-image:url(<?php echo $http ;?>images/pop_bg.jpg); background-repeat:repeat-x">
							<div id="cont_iframe_tot">
								<div id="titre_iframe_signaler">
									<span>Connexion</span>
								</div>
								<div id="logo_iframe_signaler">
									<img src="<?php echo $http ;?>images/pop_logo.png">
								</div>
							</div>
						</div>
					</div>                                            
                    </td>
                </tr>
			<?php if(isset($_GET['active']) && $_GET['active'] == 1){ ?>
			<tr><td>
				<div style="width:570px;text-align:center;">
				<p><br />
					Le compte n'est pas encore activé. Veuillez cliquer sur le lien d'activation qui se trouve dans l'email de validation qui vous a été envoyé.<br />
					<br /><a href="<?php echo $http ; ?>resend_confirmation_mobile.php" class="lien_bleu" style="font-size:medium;">Cliquez ici pour recevoir un nouvel email de validation.</a><br /><br />
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			<?php }elseif(isset($_GET['email_ok']) && $_GET['email_ok'] == 1){ ?>
				<tr><td>
				<div style="width:570px;text-align:center;margin:auto;">
				<p><br />
					Un nouvel email de validation a été envoyé.<br /><br />
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			
			
			<?php
			}elseif(isset($_SESSION['blocked']) && $_SESSION['blocked'] == true){ 
			$block = 1;
			?>
			<tr><td>
				<div style="width:570px;text-align:center;margin:auto;">
				<p><br />
					Votre compte a été bloqué pour non respect des conditions générales d'utilisation.<br /><br />
					Veuillez contacter le support pour plus d'informations.<br /><br />
					<a href="javascript:void();" onclick="fermer()" ><img src="<?php echo $http ;?>images/fermer.jpg" ></a>
				</p>				
				</div>
				</td>
			</tr>
			
			
			<?php
			$_SESSION['blocked'] = "";
			}else{ ?>
                <tr>
                	<td>
                    <table width="600" class="texte">
			<?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){?>
                    	<tr>
                        	<td colspan="2"><?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?><span class="rouge">Vous devez être connecté pour <?php if($_GET['variable'] == 1){ echo "signaler" ;}elseif($_GET['variable'] == 2){ echo "ecrire" ;} ?> un commentaire !</span><?php } ?></td>
                                                
						<td><br /></td>
						</tr>           
			<?php }
			if($_GET['message_pass'] == 1 || $_GET['message_pass'] == 2){?>
                    	<tr>
                        	<td colspan="2"><?php if($_GET['message_pass'] == 1 ){ ?><span class="rouge">Vos identifiants ont été envoyés à votre adresse email.</span><?php } ?></td>
                        	<td><br /></td>
						</tr>       
			<?php }
			if($_SESSION['error']['dpusername'] != ""){ 
			$erreur = $_SESSION['error']['dpusername'];
			?>
                    	<tr>
                        	<td colspan="2"><span class="rouge"><?php echo $erreur ; ?></span></td>
                            <td><br /></td>
						</tr> 
			<?php 
			$_SESSION['error']['dpusername'] = "";
			$_SESSION['error']['dppassword'] = "";
			}elseif($_SESSION['error']['dppassword'] != ""){ ?>
						<tr>
                        	<td colspan="2"><span class="rouge"><?php echo $_SESSION['error']['dppassword'] ; ?></span></td>
                            <td><br /></td>
						</tr> 
			<?php 
			$_SESSION['error']['dpusername'] = "";
			$_SESSION['error']['dppassword'] = "";
			}else{ ?>			
							<tr>
                        	<td colspan="2">&nbsp;</td>
                            <td><br /></td>
						</tr> 		
			<?php } ?>
                    	<tr>
                        	<td width="340" valign="top" colspan="2"><b style="text-transform:uppercase">Compte DRamaPassion</b></td>
                        	<td width="40"></td>
                        	<td width="220" valign="top"><b style="text-transform:uppercase">Connectez-vous via facebook</b></td>
						</tr>
						<tr>
                        	<td width="340" valign="top" colspan="2">&nbsp;</td>
                        	<td width="40">&nbsp;</td>
                        	<td width="220" valign="top">&nbsp;</td>
						</tr>
                        <tr>
                        	<td>Pseudo</td>
                            <td><input type="text" name="dpuser" id="dpuser" class="form" style="width:175px; height:27" tabindex="1"></td>
                        	<td width="50" valign="top" style="background-image:url(<?php echo $http ;?>images/pop_barre.jpg); background-repeat:repeat-y" rowspan="7"></td>                            
                            <td><?php //echo '<a href="javascript:void();"  id="loginlink_face"><img src=" '.$http.'images/facebook_connect.jpg" /></a>' ?>Momentanément désactivé</td>
						</tr>   
                        <tr>
                        	<td>Mot de passe</td>
                            <td><input type="password" name="dppassw" id="dppassw" class="form" style="width:175px; height:27" tabindex="2"></td>
                            
                            <td></td>
						</tr> 
                    	<tr>
                        	<td colspan="2"><br></td>
                            <td><br></td>
						</tr>                          
                        <tr>
                        	<td></td>
                            <td><input src="<?php echo $http ;?>images/pop_connecter.jpg" type="image"></td>
                            
                            <td></td>
						</tr>           
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" class="form" name="rememberme" id="rememberme" tabindex="3"> Garder ma session ouverte</td>
                            <td></td>
						</tr>
						
                    	<tr>
                        	<td colspan="2">
<a href="<?php echo $http ; ?>pass_oubli_mobile.php" class="lien_bleu"><br />Pseudo/mot de passe oublié ?</a><br>
<?php if($_GET['variable'] == 1 || $_GET['variable'] == 2){ ?>
Pas encore de compte? <a href="<?php echo $http ; ?>register_mobile.php?variable=1" class="lien_bleu">Créer un compte maintenant</a><br>
<?php }else{ ?>
Pas encore de compte? <a href="<?php echo $http ; ?>register_mobile.php" class="lien_bleu">Créer un compte maintenant</a><br>
<?php } ?>
                            </td>
                            <td><br></td>
						</tr> 
					</table>                                                                                                                                        
                    <br><br>
                    </td>
                </tr>
			<?php } ?>
			</div>
</div>
            </table>
		
        
        <!-- FIN BLOC HEADER GRIS -->        
        </td>
	</tr>
</table>
</form> 
<script>
 function setFocus()
{
document.getElementById("dpuser").focus();
}
window.onload = setFocus();
</script>
</body>
</html>