<?php

require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("../includes/functions.php");

	$drama =$_GET['d'];
	$epi = $_GET['epi'];
	$url = $_GET['u'];
	
	
	$dramainfo = DramaInfo($drama);
	
		
	
	
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Dramapassion</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type='application/javascript' src='fastclick.js'></script>
        <style type="text/css">
			body {
			    margin: 0px;
			    padding: 0px;
			    background-color: black;
			}
			#header{
				height: 34px;
				width: 100%;
				border-top: solid 1px #fe00c4;
				border-bottom: solid 1px #f00616;
				background-image: url('header2.png');
			}
			#header_left{
				width: 10%;
				display: inline-block;
				margin-left: 5px;
			}
			#header_right{
				width: 10%;
				display: inline-block;
			}
			#header_center{
				width: 79%;
				display: inline-block;
				text-align: center;
			}
			#drama_page_titre {
				text-align: center;
				font-size: 18px;
				color: white !important;
				font-weight: bold;
				vertical-align: 11px;
			}
			#video{
				height: 100%;
				width: 100%;

			}
		</style>
    </head>

    <body>
    	<div id="header">
	    	<div id="header_left">
		    	<img src="btn_back.png" height="34px" id="back"/>
	    	</div>
	    	<div id="header_center">
		    	<img class="img_div_header" src="logo_header.png" height="34px" />
		    	<span id="drama_page_titre" onclick=""><?php echo $dramainfo['titre'].' - Episode '.$epi ; ?></span>
	    	</div>
	    	<div id="header_right"></div>
    	</div>
    	<div id="video">
    	<video id="video_elem" width="100%" height="100%" controls autoplay autobuffer src="<?php echo $url ; ?>" poster="<?php echo $dramainfo['img_big'] ; ?>"></video>
    	</div>
    </body>
    <script>
    	$(function() {
		    FastClick.attach(document.body);
		});
    	$('document').ready(function() {
    		document.getElementById('video_elem').play();
		  taille = $(window).height();
		  taille = taille - 40;
		  $('#video').css('height',taille+'px');
		});
		$( "#back" ).click(function() {
		  history.back();
		});
		$( window ).unload(function() {
			$('#video').html('');
  		});
    </script>
</html>

