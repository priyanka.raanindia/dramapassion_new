<?php 
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("../includes/functions.php");

$req = "SELECT * FROM t_dp_drama as d, t_dp_episode as e WHERE d.StatusID = 1 AND d.mineur = 0 AND e.DramaID = d.DramaID AND e.EpisodeNumber = 1 ORDER BY DramaTitle";
$sql = mysql_query($req);



$i_alpha = 1;
while($row=mysql_fetch_assoc($sql)){
	$t3 = array(' ','\\','!',',','"','.','à',"'");
	$t4 = array('-','','','','','','','');
	$titre2 = str_replace($t3, $t4, $row['DramaTitle']);
	
	$t5 = array('"');
	$t6 = array('');
	$syno = str_replace($t5, $t6, $row['DramaSynopsisFre']);
	
	$date = strtotime($row['ReleaseDatePre']);
	
	if($row['Categorie'] == 'act'){
		$cate_nb = 1;
		$cate_txt = 'Action / Thriller / Fantastique';
	}elseif($row['Categorie'] == 'com'){
		$cate_nb = 2;
		$cate_txt = 'Comédie / Comédie Romantique';
	}elseif($row['Categorie'] == 'drame'){
		$cate_nb = 3;
		$cate_txt = 'Drame';
	}elseif($row['Categorie'] == 'hist'){
		$cate_nb = 4;
		$cate_txt = 'Historique';
	}
	
	$tab[$row['DramaID']]['id'] = $row['DramaID'];
	$tab[$row['DramaID']]['titre'] = $row['DramaTitle'];
	$tab[$row['DramaID']]['alpha'] = $i_alpha;
	$tab[$row['DramaID']]['date'] = $date;
	$tab[$row['DramaID']]['cate_nb'] = $cate_nb;
	$tab[$row['DramaID']]['cate_txt'] = $cate_txt;
	$tab[$row['DramaID']]['syno'] = $syno;
	$tab[$row['DramaID']]['nb_epi'] = $row['DramaEpisodes'];
	$tab[$row['DramaID']]['img'] = $titre2;
	
	$i_alpha++;
}


header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
echo json_encode($tab);

?>