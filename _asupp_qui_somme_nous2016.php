<?php
	require_once("includes/settings.inc.php");
	require_once("includes/dbinfo.inc.php");
	require_once("includes/functions.php");
	require_once("header.php");
	require_once("top.php");
?>
    
       
	<tr>
        <td valign="top" height="100">
        <br />  
        <!-- BLOC CONTENTU-->
        <div style="width:720px;margin:auto;">
            <table id="Tableau_01" width="720" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                	<td width="720" valign="top" class="texte">
                    <!-- CADRE DE GAUCHE -->
<h1 class="menu_noir">Qui Sommes-Nous?</h1>
<img src="<?php echo $http ; ?>images/ligne720.jpg">      
<br><br>
<div class="cgv">   
<p>Dramapassion.com est le premier site francophone de séries coréennes ("dramas coréens" pour les connaisseurs) et propose un service de streaming et téléchargement temporaire <b>entièrement légal</b> pour les européens francophones.</p>

<p>Dramapassion.com permet à tous les adeptes de séries coréennes de suivre les séries coréennes les plus récentes en version originale sous-titrée en français (vostfr) par des traducteurs professionnels. Notre catalogue comprend déjà plus de 170 titres et plus de 3000 heures de vidéos.
 Les séries proposées sur Dramapassion.com sont fournies directement par les principales chaînes coréennes KBS, MBC et SBS, ainsi que les chaînes du câble telles que tvN et jTBC.</p>

<p>Dramapassion.com propose un service gratuit accessible à tous ainsi que des offres premium pour un confort de visionnage de haute qualité. Le service est actuellement disponible en France, en Belgique, en Suisse, au Luxembourg, aux Pays-Bas et au Royaume-Uni.</p>

<p>L'utilisation du présent site est soumis aux <a href="<?php echo $http;?>conditions-generales/" class="lien_bleu">Conditions Générales d'Utilisation et de Vente</a> et ce inclus notre <a href="<?php echo $http;?>protection-des-donnees-personnelles/" class="lien_bleu">Politique de Protection de Données Personnelles</a>.</p>

<p>
<u>Mentions légales :</u><br />
- Dénomination de la société éditrice : Vlexhan Distribution SPRL<br />
- Siège social : Clos Chapelle-aux-Champs 30, B 1.30.30, 1200 Bruxelles, Belgique<br />
- Contact : <a href="http://www.dramapassion.com/contactez-nous/" class="lien_bleu">http://www.dramapassion.com/contactez-nous/</a><br />
- Numéro d'entreprise : TVA BE 0810.081.939<br />
- Organe de contrôle de l'éditeur : <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Conseil supérieur de l'audiovisuel (CSA)<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Boulevard de l'impératrice 13, 1000 Bruxelles, Belgique<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T. +32 2 349 5880<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F. +32 2 349 5897<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E. info@@csa.be<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W. www.csa.be<br />
- Les informations détaillées au sujet de Vlexhan Distribution SPRL et des services édités sont disponibles sur le site du CSA <a href="http://www.csa.be/pluralisme" class="lien_bleu">http://www.csa.be/pluralisme</a>
</p>

<br />
<br />
<br />
<center>
<a href="<?php echo $http;?>premium/"><img src="<?php echo $http;?>images/offre.jpg" width="720"></a>
</center>

<br />
<br />
<br />
</div>
</td>
                </tr>
			</table>
		</div>
        <!-- FIN BLOC CONTENTU -->        
        </td>
	</tr>

<?php require_once("bottom.php"); 

?>