<?php
header ("Content-type: image/jpeg");
require_once("includes/settings.inc.php");
$file_input = $_GET['img'];
$file_type = $_GET['type'] ;



if($file_type == "thumb"){
	
	$file_chemin = $http."content/dramas/".$file_input."_Thumb.jpg";
	$file_chemin = str_replace(' ', '%20' ,$file_chemin);
	$play_chemin = $http."images/play_Thumb.png";
	$image = imagecreatefromjpeg($file_chemin);
	$image_sup = imagecreatefrompng($play_chemin);
	
	//echo $file_chemin." | ".$play_chemin." <br />" ;
	
	$largeur_source = imagesx($image);
	$hauteur_source = imagesy($image);
	
	imagecopymerge($image, $image_sup, 0, 0, 0, 0, $largeur_source, $hauteur_source, 60);
	
	
	
	imagejpeg($image);
}
if($file_type == "detail"){
	
	$file_chemin = $http."content/dramas/".$file_input.".jpg";
	$file_chemin = str_replace(' ', '%20' ,$file_chemin);
	$play_chemin = $http."images/play_Detail.png";
	$image = imagecreatefromjpeg($file_chemin);
	$image_sup = imagecreatefrompng($play_chemin);
	
	//echo $file_chemin." | ".$play_chemin." <br />" ;
	
	$largeur_source = imagesx($image);
	$hauteur_source = imagesy($image);
	
	imagecopymerge($image, $image_sup, 0, 0, 0, 0, $largeur_source, $hauteur_source, 60);
	
	
	
	imagejpeg($image);
}
?>