<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("top.php");


$idActor = $_GET['id'];

$req_acteur = "SELECT * FROM t_dp_actor_detail AS d, t_dp_actor AS a WHERE d.ActorID = a.ActorID AND d.ActorID = ".$idActor;
$sql_acteur = mysql_query($req_acteur);

$nom_original = mysql_result($sql_acteur,0, 'nom_kr');
$surnom = mysql_result($sql_acteur,0, 'surnom');
$surnom_kr = mysql_result($sql_acteur,0, 'surnom_kr');
$profession = mysql_result($sql_acteur,0, 'profession');
$date_naissance = mysql_result($sql_acteur,0, 'naissance');
$date_stamp = strtotime($date_naissance);
$date_j = date('d',$date_stamp);
$date_m = date('m',$date_stamp);
$date_a = date('Y',$date_stamp);
$naissance_j = $date_j;
$naissance_m= $date_m;
$naissance_a = $date_a;
$naissance_l = mysql_result($sql_acteur,0, 'lieu_naissance');
$taille = mysql_result($sql_acteur,0, 'taille');
$poid = mysql_result($sql_acteur,0, 'poid');
$p1_t= mysql_result($sql_acteur,0, 'p1_t');
$p1_c= mysql_result($sql_acteur,0, 'p1_c');

$s1_t= mysql_result($sql_acteur,0, 's1_t');
$s1_c= mysql_result($sql_acteur,0, 's1_c');
$s2_t= mysql_result($sql_acteur,0, 's2_t');
$s2_c= mysql_result($sql_acteur,0, 's2_c');
$s3_t= mysql_result($sql_acteur,0, 's3_t');
$s3_c = mysql_result($sql_acteur,0, 's3_c');
$tab_signe_Astro = array('Bélier','Taureau' ,'Gémeaux' ,'Cancer','Lion','Vierge','Balance','Scorpion','Sagittaire','Capricorne','Verseau','Poissons');
$tab_signe_chinois = array('rat','bœuf ou buffle','tigre','lapin ou lièvre','dragon','serpent','cheval','chèvre','singe','coq','chien','cochon ou sanglier');
$tab_groupe = array('O','O','A','A','B','B','AB','AB');


$signe_astro = mysql_result($sql_acteur,0, 'singe_astro');
$signe_chinois = mysql_result($sql_acteur,0, 'signe_chinois');
$groupe = mysql_result($sql_acteur,0, 'groupe_sanguin');
$groupe = $groupe  - 1;
$signe_astro = $signe_astro - 1;
$signe_chinois = $signe_chinois - 1;


$name = mysql_result($sql_acteur,0, 'ActorName');


$req_cita_acteur = "SELECT * FROM t_dp_actor_cita WHERE id_acteur =".$idActor;
$sql_cita_acteur = mysql_query($req_cita_acteur);

$nb_cita_acteur = mysql_num_rows($sql_cita_acteur);

?>
<tr><td>
<div style="width:1000px;margin:auto;">
	<div>
		<div style="width:55%;float:left;font-size: 18px;Line-Height: 1.4;">
			<h1><?php echo $name ;?></h1>
			<br />
			Phrases Mythiques : <br />
			<?php
			if($nb_cita_acteur > 0){
				while($row_cita=mysql_fetch_assoc($sql_cita_acteur)){
					echo '"'.$row_cita['cita'].'"<br />';
				}
				if($_GET['send'] == 1){
					echo '<br /><br />Merci !<br />Votre citation sera revue avant d\'être éventuellement publiée';
				}else{
					echo '<br /><input type="button" value="Proposer une phrase" id="add_cita" /><br />';
				}
				echo '<form method="post" action="cita_Action.php" id="form_cita" style="display:none">';
			}else{
			?>
			<form method="post" action="cita_Action.php" id="form_cita">
			<?php
			}
			?>
			<input type="hidden" name="id" id="id" value="<?php echo $idActor; ?>" />
			Phrase : <input type="text" name="cita" id="cita" /><br />
			Série : <input type="text" name="cita_serie" id="cita_serie" /><br />
			Episode : <input type="text" name="cita_epi" id="cita_epi" /><br />
			<input type="button" value="Envoyer" id="send_cita" />
			</form>
			<h2>Profil</h2>
			Nom original : <?php echo $nom_original ;?><br />
			Surnom : <?php echo $surnom ;?><?php echo $surnom_kr ;?><br />
			Profession : <?php echo $profession ;?><br />
			Date de naissance : <?php echo $naissance_j.' '.$naissance_m.' '.$naissance_a ;?><br />
			Lieu de naissance : <?php echo $naissance_l ;?><br />
			Taille : <?php echo $taille.' cm' ;?><br />
			Poids : <?php echo $poid.' kg' ;?><br />
			Signe astrologique : <?php echo $tab_signe_Astro[$signe_astro] ;?><br /> 
			Signe chinois : <?php echo $tab_signe_chinois[$signe_chinois] ;?><br /> 
			Groupe sanguin : <?php echo $tab_groupe[$groupe] ;?><br /> 
			<?php echo $p1_t ; ?> : <?php echo $p1_c ;?><br /> 
		</div>
		<div style="width:45%;float:left;">
			<div style="background-color:grey;text-align:center;padding-top: 20px;padding-bottom:20px;margin-top:30px;width100%;height:400px;">
				<img id="cont_img" src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/1.jpg" height="100%" />
			</div>
			<div style="text-align:center;height:100px;margin-top: 20px;">
				<img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/1.jpg" onclick="$('#cont_img').attr('src','http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/1.jpg')" height="100%" />
				<img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/2.jpg" onclick="$('#cont_img').attr('src','http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/2.jpg')" height="100%" />
				<img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/3.jpg" onclick="$('#cont_img').attr('src','http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/3.jpg')" height="100%" />
				<img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/4.jpg" onclick="$('#cont_img').attr('src','http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/4.jpg')" height="100%" />
				<img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/5.jpg" onclick="$('#cont_img').attr('src','http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/5.jpg')" height="100%" />
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div>
		<h2>Série disponible sur Dramapassion</h2>
		<?php
			$req_drama = 'SELECT * FROM t_dp_dramaactorlink AS l, t_dp_drama AS d WHERE l.DramaID = d.DramaID AND l.ActorID ='.$idActor.' ORDER BY d.DramaYear DESC';
			$sql_drama = mysql_query($req_drama);
			while($row=mysql_fetch_assoc($sql_drama)){
				$name_link = $row['DramaTitle'];
				$name_link = str_replace(' ','%20',$name_link);
				echo '<div style="float:left;margin-right: 20px;"><img src="http://www.dramapassion.com/content/dramas/'.$name_link.'_Thumb.jpg" /><br /> '.$row['DramaTitle'].' ('.$row['DramaYear'].')</div>';
			}
		?>
		<div style="clear:both;"></div>
	</div>
	
	<div>
		<h2>Le saviez-vous</h2>
		<?php echo $s1_t.' : '.$s1_c ;?><br />
		<?php echo $s2_t.' : '.$s2_c ;?><br />
		<?php echo $s3_t.' : '.$s3_c ;?><br />
	</div>
</div>


</td></tr>	
<script>
$("#add_cita").click(function() {
  $("#form_cita").show();
  $("#add_cita").hide();
});
$("#send_cita").click(function() {
  phrase = $("#cita").val();
  drama = $("#cita_serie").val();
  epi = $("#cita_epi").val();
  
  if(phrase != '' && drama != '' && epi != ''){
	  $("#form_cita").submit();
  }else{
	  alert('Veuillez remplir entièrement le formulaire !');
  }
  
});
</script>

<?php
//echo '<a href="http://dramapassion.com/admin/genre_drama.php">genre</a>';
require_once("bottom.php");
?>