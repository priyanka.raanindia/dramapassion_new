<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("top.php");

$idActeur = $_POST['actorID'];
$nom_original = $_POST['nom_original'];
$surnom = $_POST['surnom'];
$surnom_kr = $_POST['surnom_kr'];
$profession = $_POST['profession'];
$naissance_j = $_POST['naissance_j'];
$naissance_m = $_POST['naissance_m'];
$naissance_a = $_POST['naissance_a'];
$naissance_l = $_POST['naissance_l'];
$taille = $_POST['taille'];
$poid = $_POST['poid'];
$signe_astro = $_POST['signe_astro'];
$signe_chinois = $_POST['signe_chinois'];
$groupe_sanguin = $_POST['groupe_sanguin'];
$s1_titre = $_POST['s1_titre'];
$s1_cont = $_POST['s1_cont'];
$s2_titre = $_POST['s2_titre'];
$s2_cont = $_POST['s2_cont'];
$s3_titre = $_POST['s3_titre'];
$s3_cont = $_POST['s3_cont'];

$p1_titre = $_POST['p1_titre'];
$p1_cont = $_POST['p1_cont'];

for($i=1 ; $i <= 5 ; $i++){
	$nom_photo = 'photo'.$i;
	
if ($_FILES[$nom_photo]['error']) {    
          switch ($_FILES[$nom_photo]['error']){     
                   case 1: // UPLOAD_ERR_INI_SIZE     
                   echo"La photo ".$i." dépasse la limite autorisée par le serveur (fichier php.ini) !<br />";     
                   break;     
                   case 2: // UPLOAD_ERR_FORM_SIZE     
                   echo "La photo ".$i." dépasse la limite autorisée dans le formulaire HTML !<br />"; 
                   break;     
                   case 3: // UPLOAD_ERR_PARTIAL     
                   echo "L'envoi du fichier a été interrompu pendant le transfert !<br />";     
                   break;     
                   case 4: // UPLOAD_ERR_NO_FILE     
                   echo "La photo ".$i." que vous avez envoyé a une taille nulle !<br />"; 
                   break;     
          }     
}     
else {     
 // $_FILES['nom_du_fichier']['error'] vaut 0 soit UPLOAD_ERR_OK     
 // ce qui signifie qu'il n'y a eu aucune erreur   
 if ((($_FILES[$nom_photo]['error'] == UPLOAD_ERR_OK))) {     
	 $chemin_destination = '/var/www/content/actors_fotos/'.$idActeur.'/';  
	 mkdir ($chemin_destination); 
	 move_uploaded_file($_FILES[$nom_photo]['tmp_name'], $chemin_destination.$i.'.jpg');     
	 }  
}
}

$req_maj = 'UPDATE t_dp_actor_detail SET ';

if(isset($nom_original) && isset($profession) && isset($naissance_j) && isset($naissance_m) && isset($naissance_a) && isset($naissance_l) && isset($taille) && isset($poid) && isset($signe_astro) && isset($signe_chinois) && isset($groupe_sanguin) ){
	if($nom_original != '' && $profession != '' && $naissance_j != '' && $naissance_m != '' && $naissance_a != '' && $naissance_l != '' && $taille != '' && $poid != '' && $signe_astro != '' && $signe_chinois != '' && $groupe_sanguin != ''){
	$actor_complete = 1;
	}else{
		$actor_complete = 2;
	}
}else{
	$actor_complete = 2;
}

if(isset($nom_original) && $nom_original != ''){
	$req_maj = $req_maj.' nom_kr = "'.$nom_original.'" ,';
}
if(isset($profession) && $profession != ''){
	$req_maj = $req_maj.' profession = "'.$profession.'" ,';
}
if(isset($naissance_j) && $naissance_j != '' && isset($naissance_m) && $naissance_m != '' && isset($naissance_a) && $naissance_a != ''){
	$date_naissance = $naissance_a.'-'.$naissance_m.'-'.$naissance_j ; 
	$req_maj = $req_maj.' naissance = "'.$date_naissance.'" ,';
}
if(isset($naissance_l) && $naissance_l != ''){
	$req_maj = $req_maj.' lieu_naissance = "'.$naissance_l.'" ,';
}
if(isset($taille) && $taille != ''){
	$req_maj = $req_maj.' taille	 = "'.$taille.'" ,';
}
if(isset($poid) && $poid != ''){
	$req_maj = $req_maj.' poid = "'.$poid.'" ,';
}
if(isset($signe_astro) && $signe_astro != ''){
	$req_maj = $req_maj.' singe_astro = "'.$signe_astro.'" ,';
}
if(isset($signe_chinois) && $signe_chinois != ''){
	$req_maj = $req_maj.' signe_chinois = "'.$signe_chinois.'" ,';
}
if(isset($groupe_sanguin) && $groupe_sanguin != ''){
	$req_maj = $req_maj.' groupe_sanguin = "'.$groupe_sanguin.'" ,';
}
if(isset($p1_titre) && $p1_titre != '' && isset($p1_cont) && $p1_cont != ''){
	$req_maj = $req_maj.' p1_t = "'.$p1_titre.'" ,';
	$req_maj = $req_maj.' p1_c = "'.$p1_cont.'" ,';
	
}
if(isset($s1_titre) && $s1_titre != '' && isset($s1_cont) && $s1_cont != ''){
	$req_maj = $req_maj.' s1_t = "'.$s1_titre.'" ,';
	$req_maj = $req_maj.' s1_c = "'.$s1_cont.'" ,';
	
}
if(isset($s2_titre) && $s2_titre != '' && isset($s2_cont) && $s2_cont != ''){
	$req_maj = $req_maj.' s2_t = "'.$s2_titre.'" ,';
	$req_maj = $req_maj.' s2_c = "'.$s2_cont.'" ,';
	
}
if(isset($s3_titre) && $s3_titre != '' && isset($s3_cont) && $s3_cont != ''){
	$req_maj = $req_maj.' s3_t = "'.$s3_titre.'" ,';
	$req_maj = $req_maj.' s3_c = "'.$s3_cont.'" ,';
	
}
if(isset($surnom) && $surnom != ''){
	$req_maj = $req_maj.' surnom = "'.$surnom.'" ,';
}
if(isset($surnom_kr) && $surnom_kr != ''){
	$req_maj = $req_maj.' surnom_kr = "'.$surnom_kr.'" ,';
}
$req_maj = $req_maj.' info = "'.$actor_complete.'" ,'; 
$req_maj = substr($req_maj, 0, -1); 
$req_maj = $req_maj.' WHERE ActorID	='.$idActeur;


mysql_query($req_maj);
$url = "http://www.dramapassion.com/admin_acteurs/detail_acteur_page.php?id=".$idActeur;


?>
<a href="<?php echo $url ; ?>">voir résultat</a>

</td></tr>	


<?php
//echo '<a href="http://dramapassion.com/admin/genre_drama.php">genre</a>';
require_once("bottom.php");
?>
