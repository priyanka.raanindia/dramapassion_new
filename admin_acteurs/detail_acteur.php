<?php
require_once("../includes/settings.inc.php");
require_once("../includes/dbinfo.inc.php");
require_once("top.php");


$idActor = $_GET['id'];

$req_acteur = "SELECT * FROM t_dp_actor_detail AS d, t_dp_actor AS a WHERE d.ActorID = a.ActorID AND d.ActorID = ".$idActor;
$sql_acteur = mysql_query($req_acteur);

$nom_original = mysql_result($sql_acteur,0, 'nom_kr');
$surnom = mysql_result($sql_acteur,0, 'surnom');
$surnom_kr = mysql_result($sql_acteur,0, 'surnom_kr');
$profession = mysql_result($sql_acteur,0, 'profession');
$date_naissance = mysql_result($sql_acteur,0, 'naissance');
$date_stamp = strtotime($date_naissance);
$date_j = date('d',$date_stamp);
$date_m = date('m',$date_stamp);
$date_a = date('Y',$date_stamp);
if($date_naissance == '0000-00-00'){
$naissance_j = '';
$naissance_m= '';
$naissance_a = '';
}else{
$naissance_j = $date_j;
$naissance_m= $date_m;
$naissance_a = $date_a;
}
$naissance_l = mysql_result($sql_acteur,0, 'lieu_naissance');
$taille = mysql_result($sql_acteur,0, 'taille');
$poid = mysql_result($sql_acteur,0, 'poid');
$s1_t= mysql_result($sql_acteur,0, 's1_t');
$s1_c= mysql_result($sql_acteur,0, 's1_c');
$s2_t= mysql_result($sql_acteur,0, 's2_t');
$s2_c= mysql_result($sql_acteur,0, 's2_c');
$s3_t= mysql_result($sql_acteur,0, 's3_t');
$s3_c = mysql_result($sql_acteur,0, 's3_c');

$p1_t= mysql_result($sql_acteur,0, 'p1_t');
$p1_c = mysql_result($sql_acteur,0, 'p1_c');


$signe_astro = mysql_result($sql_acteur,0, 'singe_astro');
$signe_chinois = mysql_result($sql_acteur,0, 'signe_chinois');
$groupe = mysql_result($sql_acteur,0, 'groupe_sanguin');

$name = mysql_result($sql_acteur,0, 'ActorName');


?>
<style>
#table_form tr, #table_form input, #table_form select{
	height: 30px;
	font-size: 16px;
}
#table_form input{
	width: 90%;
}
</style>
<tr><td>
<div style="width:1000px;margin:auto;">
<h2><?php echo $name ;?></h2>
<a href="detail_acteur_page.php?id=<?php echo $idActor ; ?>">voir page</a>
<img src="<?php echo $http ; ?>images/ligne1000.jpg" /><br />

<form method="post" action="detail_acteur_Action.php" enctype="multipart/form-data"> 
<input type="hidden" name="actorID" id="actorID" value="<?php echo $idActor ; ?>" />
<table id="table_form" width="100%">
	<tr>
		<td width="50%">Nom original</td>
		<td width="50%"><input type="text" name="nom_original" id="nom_original" style="width:100%;" value="<?php echo $nom_original; ?>" /></td>
	</tr>
	<tr>
		<td width="50%">Surnom</td>
		<td width="50%"><input type="text" name="surnom" id="surnom" style="width:50%;" value="<?php echo $surnom; ?>" />FR<input type="text" name="surnom_kr" id="surnom_kr" style="width:50%;" value="<?php echo $surnom_kr; ?>" />Coréen</td>
	</tr>
	<tr>
		<td>Profession</td>
		<td><input type="text" name="profession" id="profession" style="width:100%;" value="<?php echo $profession; ?>" /></td>
	</tr>
	<tr>
		<td>Date de naissance (JJ-MM-AAAA)</td>
		<td><input type="text" name="naissance_j" id="naissance_j" style="width:30%;" value="<?php echo $naissance_j; ?>" /> - <input type="text" name="naissance_m" id="naissance_m" style="width:30%;" value="<?php echo $naissance_m; ?>" /> - <input type="text" name="naissance_a" id="naissance_a" style="width:30%;" value="<?php echo $naissance_a; ?>" /></td>
	</tr>
	<tr>
		<td>Lieu de naissance</td>
		<td><input type="text" name="naissance_l" id="naissance_l" style="width:100%;" value="<?php echo $naissance_l; ?>" /></td>
	</tr>
	<tr>
		<td>Taille (cm)</td>
		<td><input type="text" name="taille" id="taille" style="width:100%;" value="<?php echo $taille; ?>" /></td>
	</tr>
	<tr>
		<td>poids (kg)</td>
		<td><input type="text" name="poid" id="poid" style="width:100%;" value="<?php echo $poid; ?>" /></td>
	</tr>
	<tr>
		<td>Signe astrologique</td>
		<td><select name="signe_astro" id="signe_astro">
			<option value="0" <?php if($signe_astro == 0){echo 'selected' ;} ?>>Choisir</option>
			<option value="1" <?php if($signe_astro == 1){echo 'selected' ;} ?>>Bélier</option>
			<option value="2" <?php if($signe_astro == 2){echo 'selected' ;} ?>>Taureau</option>
			<option value="3" <?php if($signe_astro == 3){echo 'selected' ;} ?>>Gémeaux</option>
			<option value="4" <?php if($signe_astro == 4){echo 'selected' ;} ?>>Cancer</option>
			<option value="5" <?php if($signe_astro == 5){echo 'selected' ;} ?>>Lion</option>
			<option value="6" <?php if($signe_astro == 6){echo 'selected' ;} ?>>Vierge</option>
			<option value="7" <?php if($signe_astro == 7){echo 'selected' ;} ?>>Balance</option>
			<option value="8" <?php if($signe_astro == 8){echo 'selected' ;} ?>>Scorpion</option>
			<option value="9" <?php if($signe_astro == 9){echo 'selected' ;} ?>>Sagittaire</option>
			<option value="10" <?php if($signe_astro == 10){echo 'selected' ;} ?>>Capricorne</option>
			<option value="11" <?php if($signe_astro == 11){echo 'selected' ;} ?>>Verseau</option>
			<option value="12" <?php if($signe_astro == 12){echo 'selected' ;} ?>>Poissons</option>
			</select></td>
	</tr>
	<tr>
		<td>Signe Chinois</td>
		<td><select name="signe_chinois" id="signe_chinois">
			<option value="0" <?php if($signe_chinois == 0){echo 'selected' ;} ?>>Choisir</option>
			<option value="1" <?php if($signe_chinois == 1){echo 'selected' ;} ?>>rat</option>
			<option value="2" <?php if($signe_chinois == 2){echo 'selected' ;} ?>>bœuf ou buffle</option>
			<option value="3" <?php if($signe_chinois == 3){echo 'selected' ;} ?>>tigre</option>
			<option value="4" <?php if($signe_chinois == 4){echo 'selected' ;} ?>>lapin ou lièvre</option>
			<option value="5" <?php if($signe_chinois == 5){echo 'selected' ;} ?>>dragon</option>
			<option value="6" <?php if($signe_chinois == 6){echo 'selected' ;} ?>>serpent</option>
			<option value="7" <?php if($signe_chinois == 7){echo 'selected' ;} ?>>cheval</option>
			<option value="8" <?php if($signe_chinois == 8){echo 'selected' ;} ?>>chèvre</option>
			<option value="9" <?php if($signe_chinois == 9){echo 'selected' ;} ?>>singe</option>
			<option value="10" <?php if($signe_chinois == 10){echo 'selected' ;} ?>>coq</option>
			<option value="11" <?php if($signe_chinois == 11){echo 'selected' ;} ?>>chien</option>
			<option value="12" <?php if($signe_chinois == 12){echo 'selected' ;} ?>>cochon ou sanglier</option>
			</select></td>
	</tr>
	<tr>
		<td>Groupe sanguin</td>
		<td><select name="groupe_sanguin" id="groupe_sanguin">
			<option value="0" <?php if($groupe == 0){echo 'selected' ;} ?>>choisir</option>
			<option value="1" <?php if($groupe == 1){echo 'selected' ;} ?>>O</option>
			<option value="3" <?php if($groupe == 3){echo 'selected' ;} ?>>A</option>
			<option value="5" <?php if($groupe == 5){echo 'selected' ;} ?>>B</option>
			<option value="7" <?php if($groupe == 7){echo 'selected' ;} ?>>AB</option>
			</select></td>
	</tr>
	<tr>
		<td><input type="text" name="p1_titre" id="p1_titre" value="<?php echo $p1_t; ?>"/></td>
		<td><input type="text" name="p1_cont" id="p1_cont"  value="<?php echo $p1_c; ?>"/></td>
	</tr>
	<tr>
	</tr>
	<tr>
	</tr>
	<tr>
		<td><img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/1.jpg" height="50px" />Photo 1</td>
		<td><input type="hidden" name="MAX_FILE_SIZE" value="2097152">     
          <input type="file" name="photo1" id="photo1"> </td>
	</tr>
	<tr>
		<td><img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/2.jpg" height="50px" />Photo 2</td>
		<td><input type="hidden" name="MAX_FILE_SIZE" value="2097152">     
          <input type="file" name="photo2" id="photo2"> </td>
	</tr>
	<tr>
		<td><img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/3.jpg" height="50px" />Photo 3</td>
		<td><input type="hidden" name="MAX_FILE_SIZE" value="2097152">     
          <input type="file" name="photo3" id="photo3"> </td>
	</tr>
	<tr>
		<td><img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/4.jpg" height="50px" />Photo 4</td>
		<td><input type="hidden" name="MAX_FILE_SIZE" value="2097152">     
          <input type="file" name="photo4" id="photo4"> </td>
	</tr>
	<tr>
		<td><img src="http://www.dramapassion.com/content/actors_fotos/<?php echo $idActor ; ?>/5.jpg" height="50px" />Photo 5</td>
		<td><input type="hidden" name="MAX_FILE_SIZE" value="2097152">     
          <input type="file" name="photo5" id="photo5"> </td>
	</tr>
	<tr></tr>
	<tr></tr>
	<tr>
		<td>Le saviez-vous</td>
		<td></td>
	</tr>
	<tr>
		<td><input type="text" name="s1_titre" id="s1_titre" value="<?php echo $s1_t; ?>"/></td>
		<td><input type="text" name="s1_cont" id="s1_cont"  value="<?php echo $s1_c; ?>"/></td>
	</tr>
	<tr>
		<td><input type="text" name="s2_titre" id="s2_titre" value="<?php echo $s2_t; ?>"/></td>
		<td><input type="text" name="s2_cont" id="s2_cont" value="<?php echo $s2_c; ?>"/></td>
	</tr>
	<tr>
		<td><input type="text" name="s3_titre" id="s3_titre" value="<?php echo $s3_t; ?>"/></td>
		<td><input type="text" name="s3_cont" id="s3_cont"  value="<?php echo $s3_c; ?>"/></td>
	</tr>

</table><br /><br />
<input type="submit" value="Envoyer" />
</form>


</table>
</div>
</td></tr>	


<?php
//echo '<a href="http://dramapassion.com/admin/genre_drama.php">genre</a>';
require_once("bottom.php");
?>